<?php

	include("excelwriter.inc.php");
	include("conexao.php");
	
	$excel=new ExcelWriter("inscritosTreinamento2018.xls");
	if($excel===false){
		echo $excel->error;
	}

	$myArr=array("ID", "Data de Inscri��o","Treinamento","Nome","RG","Cpf","Empresa", "CNPJ", "I.E.","I.M.","Endere�o Comercial","N�mero","Complemento","Bairro", "CEP","Cidade", "UF", "Telefone", "Celular","Email","Interesse","Arquivo","Aprovado");
		
	$excel->writeLine($myArr);

	$select =	"SELECT * FROM treinamento_tecnico";
	$result =	$mysqli->query($select);
	while($dados[] = $result->fetch_array(MYSQLI_ASSOC)){}
	
	foreach( $dados as $dado ){
	
		$excel->writeRow();
		$excel->writeCol(urldecode($dado['id']));
		$excel->writeCol(date('d/m/Y H:i:s', strtotime($dado['dthr_inscricao'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['descricao_treinamento'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['nome'].' '.$dado['sobrenome'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['rg'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['cpf'])));		
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['razao_social'])));		
		$excel->writeCol(str_replace("%2F","/",$dado['cnpj']));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['insc_estadual'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['insc_municipal'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['endereco_comercial'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['numero'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['complemento'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['bairro'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",($dado['cep']))));
		$excel->writeCol(urldecode(str_replace("%2F","/",($dado['cidade']))));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['estado'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['telefone'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['celular_whats'])));
		$excel->writeCol(str_replace("%2F","/",$dado['email']));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['interesse'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",$dado['arquivo'])));
		$excel->writeCol(urldecode(str_replace("%2F","/",($dado['fl_aprovado'] == 1) ? 'Sim' : 'N�o')));
		
	} 

	$excel->close();	

	
?>
	<script type="text/javascript">
		window.open('inscritosTreinamento2018.xls');
	</script>
