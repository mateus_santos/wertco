<?php

class SolicitacaoPecasModel extends CI_Model {
	
	/*public function buscaSolicitacao($chamado_id){
        $sql = "SELECT  s.id, s.descricao, s.endereco_entrega, s.empresa_entrega_id, p.descricao as pecas, si.qtd, s.status_id, s.usuario_id, si.id as solicitacao_itens_id, ss.descricao as status
        		FROM 	solicitacao_pecas s
                LEFT JOIN   solicitacao_pecas_itens si ON s.id = si.solicitacao_peca_id
                INNER JOIN  solicitacao_pecas_status ss ON s.status_id = ss.id
                LEFT JOIN   pecas p on si.peca_id = p.id 
                WHERE   s.chamado_id =".$chamado_id;

        $query  =   $this->db->query($sql);
        return  $query->result_array();  

    }*/

    public function ultimasSolicitacoes() {
        $sql = "SELECT s.chamado_id, s.id, s.descricao, s.dthr_solicitacao, ss.descricao as status, e.razao_social as cliente
                FROM        solicitacao_pecas s                 
                INNER JOIN  solicitacao_pecas_status ss ON s.status_id = ss.id        
                INNER JOIN  chamado c   ON c.id = s.chamado_id
                inner join  empresas e  ON c.cliente_id = e.id
                WHERE       ss.id=1";
        $query  =   $this->db->query($sql);
        return  $query->result_array();  
    }

    public function buscaSolicitacao($chamado_id){
        $sql = "SELECT s.id, s.descricao, s.endereco_entrega, group_concat(p.descricao) as pecas, s.dthr_solicitacao, ss.descricao as status, u.nome as responsavel, ss.id as status_id 
                FROM        solicitacao_pecas s 
                LEFT JOIN   solicitacao_pecas_itens si  ON s.id = si.solicitacao_peca_id 
                INNER JOIN  solicitacao_pecas_status ss ON s.status_id = ss.id 
                LEFT JOIN   pecas p                     ON si.peca_id = p.id 
                INNER JOIN  usuarios u ON s.usuario_id = u.id 
                WHERE       s.chamado_id = ".$chamado_id." GROUP by s.id";

        $query  =   $this->db->query($sql);
        return  $query->result_array();  

    }

    public function buscaSolicitacoes(){
		$sql = "SELECT  s.id, s.chamado_id, s.descricao, s.endereco_entrega, s.empresa_entrega_id, s.status_id, s.usuario_id, ss.descricao as status, concat(e.razao_social,' - ',e.cnpj) as cliente, s.dthr_solicitacao, s.nr_nf, s.arquivo_nfe, s.dthr_envio,  s.rastreio, s.rastreio_entrada, s.comprovante_rastreio, s.comprovante_rastreio_entrada
        		FROM 	solicitacao_pecas s, solicitacao_pecas_status ss, chamado c, empresas e
                WHERE   s.status_id = ss.id and
                		s.chamado_id = c.id and 
                		c.cliente_id = e.id";

        $query  =   $this->db->query($sql);
        return  $query->result_array();

	}

	public function buscaSolicitacaoPorId($id){
        $sql = "SELECT  s.id, s.descricao, s.endereco_entrega, s.empresa_entrega_id, p.descricao as pecas, si.qtd, s.status_id, s.usuario_id, si.id as solicitacao_itens_id, ss.descricao as status, concat(e.razao_social,' - ',e.cnpj) as cliente,  s.dthr_solicitacao, s.nr_nf, s.arquivo_nfe, s.dthr_envio, s.chamado_id, concat(dest.razao_social,' - ',dest.cnpj) as destinatario, si.custo, s.rastreio, ss.id as status_id  
        		FROM 	solicitacao_pecas s
                LEFT JOIN   solicitacao_pecas_itens si ON s.id = si.solicitacao_peca_id
                LEFT JOIN   solicitacao_pecas_status ss ON s.status_id = ss.id 
                LEFT JOIN  pecas p     ON  si.peca_id = p.id 
                INNER JOIN  chamado c   ON  s.chamado_id = c.id
                INNER JOIN  empresas e  ON  e.id = c.cliente_id  
                INNER JOIN  empresas dest  ON  dest.id = s.empresa_entrega_id
                WHERE   s.id =".$id;

        $query  =   $this->db->query($sql);
        return  $query->result_array();  

    }

    public function buscaAndamentos($id){

        $sql = "SELECT      ss.id as status_id, ss.descricao as status, sa.*, u.nome as usuario 
                FROM        solicitacao_pecas s 
                INNER JOIN  solicitacao_pecas_andamento sa  ON sa.solicitacao_pecas_id = s.id 
                INNER JOIN  solicitacao_pecas_status ss     ON sa.status_solicitacao_id = ss.id 
                INNER JOIN  usuarios u ON sa.usuario_id = u.id
                WHERE       s.id = ".$id."
                ORDER BY sa.id asc";

        $query  =   $this->db->query($sql);        
        return  $query->result_array();  

    }

    public function insert($solicitacao_pecas)
	{
        $this->db->insert('solicitacao_pecas', $solicitacao_pecas);
		return $this->db->insert_id();
    }

    public function atualizar($dados){

        $this->db->where('id', $dados['id']);

        if($this->db->update('solicitacao_pecas', $dados)){
            return true;

        }else{
            return false;
        }
    }

    public function insereAndamentos($andamento)
    {
        $this->db->insert('solicitacao_pecas_andamento', $andamento);
        return $this->db->insert_id();
    }

}

?>