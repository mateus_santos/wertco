<?php

class ChamadoModel extends CI_Model {
	
	public function select()
    {
		$this->db->select('c.*');
        $this->db->select("concat(e.id,' | ',e.cnpj,' | ',e.razao_social) as cliente");
		$this->db->select('cs.descricao as status');
		$this->db->select('ct.descricao as tipo');
		$this->db->join('empresas e', 'e.id = c.cliente_id');
		$this->db->join('chamado_status cs', 'cs.id = c.status_id');
		$this->db->join('chamado_tipo ct', 'ct.id = c.tipo_id');
		return $this->db->get('chamado c')->result_array();
    }

    public function selectPorEmpresa($empresa_id)
    {
        $this->db->select('c.*');
        $this->db->select("concat(e.id,' | ',e.cnpj,' | ',e.razao_social) as cliente");
        $this->db->select('cs.descricao as status');
        $this->db->select('ct.descricao as tipo');
        $this->db->join('empresas e', 'e.id = c.cliente_id');
        $this->db->join('chamado_status cs', 'cs.id = c.status_id');
        $this->db->join('chamado_tipo ct', 'ct.id = c.tipo_id');
        $this->db->where('c.cliente_id', $empresa_id);
        return $this->db->get('chamado c')->result_array();
    }

    public function selectPorUsuario($usuario_id)
    {
        $this->db->select('c.*');
        $this->db->select("concat(e.id,' | ',e.cnpj,' | ',e.razao_social) as cliente");
        $this->db->select('cs.descricao as status');
        $this->db->select('ct.descricao as tipo');
        $this->db->join('empresas e', 'e.id = c.cliente_id');
        $this->db->join('chamado_status cs', 'cs.id = c.status_id');
        $this->db->join('chamado_tipo ct', 'ct.id = c.tipo_id');
        $this->db->where('c.usuario_id', $usuario_id);
        return $this->db->get('chamado c')->result_array();
    }
	
	public function insert($chamado)
	{
        $this->db->insert('chamado', $chamado);
		return $this->db->insert_id();
    }
	
	public function find($id)
    {
		$this->db->select('e.razao_social cliente');
        $this->db->select('e.cnpj');
		$this->db->select('u.nome contato');
        $this->db->select('uw.nome usuario');
		$this->db->select('c.*');
		$this->db->join('empresas e', 'e.id = c.cliente_id');
		$this->db->join('usuarios u', 'u.id = c.contato_id');
        $this->db->join('usuarios uw', 'uw.id = c.usuario_id');
        $this->db->where('c.id', $id);
		return $this->db->get('chamado c')->row_array();
    }
	
	public function update($chamado)
    {
		$this->db->where('id', $chamado['id']);
        $this->db->update('chamado', $chamado);        
        return $this->db->affected_rows();
	}

	public function selectTipos(){
		return $this->db->get('chamado_tipo')->result_array();
	}
	
	public function selectStatus(){
        $this->db->order_by("ordem", "asc");
		return $this->db->get('chamado_status')->result_array();
	}
	
	public function selectPrioridades(){
		return $this->db->get('chamado_prioridade')->result_array();
	}

	public function insertAnexo($insert){
		
		return $this->db->insert('chamado_upload', $insert);
	}

	public function excluirAnexo($chamado_id){
		$this->db->where('id', $chamado_id);
        if(	$this->db->delete('chamado_upload') ){
            return true;
        }else{
            return false;
        }
	}

	public function listaAnexos($chamado_id){

		$this->db->select('*');
		$this->db->where('chamado_id', $chamado_id);		
		return $this->db->get('chamado_upload')->result_array();
	}

	
	public function listaTecnicosAtendimentos($chamado_id){	
		$this->db->select('e.razao_social empresa');
		$this->db->select('e.id empresa_id');
		$this->db->select('u.nome tecnico');
		$this->db->select('c.*');
		$this->db->join('chamado_atividade ca', 'ca.chamado_id = c.id');
		$this->db->join('chamado_subatividade cs', 'cs.atividade_id = ca.id');		
		$this->db->join('empresas e', 'e.id = cs.parceiro_id');
		$this->db->join('usuarios u', 'u.id = cs.contato_id');		
        $this->db->where('c.id', $chamado_id);
		return $this->db->get('chamado c')->result_array();
	}

	public function selectTotalStatus(){
         
         $sql = "SELECT count(c.status_id) as valor, s.descricao
                    FROM chamado_status s, chamado c
                    WHERE s.id = c.status_id 
                    GROUP BY c.status_id order by s.id asc";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".$dados['descricao']."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['valor']);
        $return['valor'].=']';
        $return['descricao'] = '['; 
        $return['descricao'].= implode(',',$descricao['descricao']);
        $return['descricao'].=']';
        $return['cor'] = '[';
        $return['cor'].= implode(',',$cor);
        $return['cor'].=']';
        $return['dados'] 	= $retorno;
        $return['total']    = $total;
        $return['cores']    = $cores;
        return $return;
    }

    public function selectTotalTipo(){
         
         $sql = "SELECT count(c.tipo_id) as valor, s.descricao
                    FROM chamado_tipo s, chamado c
                    WHERE s.id = c.tipo_id 
                    GROUP BY c.tipo_id order by s.id asc";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".$dados['descricao']."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['valor']);
        $return['valor'].=']';
        $return['descricao'] = '['; 
        $return['descricao'].= implode(',',$descricao['descricao']);
        $return['descricao'].=']';
        $return['cor'] = '[';
        $return['cor'].= implode(',',$cor);
        $return['cor'].=']';
        $return['dados'] 	= $retorno;
        $return['total']    = $total;
        $return['cores']    = $cores;
        return $return;
    }

    public function selectTotalDefeitos(){
         
         $sql = "SELECT     COUNT(c.id) as valor, cd.descricao 
                    FROM    chamado_atividade ca, chamado c, chamado_defeito cd 
                    WHERE   ca.chamado_id = c.id and 
                            ca.defeito_id = cd.id and 
                            ca.defeito_id <> 1
                    GROUP BY    cd.descricao";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".$dados['descricao']."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['valor']);
        $return['valor'].=']';
        $return['descricao'] = '['; 
        $return['descricao'].= implode(',',$descricao['descricao']);
        $return['descricao'].=']';
        $return['cor'] = '[';
        $return['cor'].= implode(',',$cor);
        $return['cor'].=']';
        $return['dados']    = $retorno;
        $return['total']    = $total;
        $return['cores']    = $cores;
        return $return;
    }

     public function totalChamadoMes(){

        $sql="  SELECT  count(c.id) as total, extract(MONTH FROM c.inicio) AS mes
                FROM    chamado c
                WHERE    extract(YEAR FROM c.inicio) = '".date('Y')."'
                GROUP BY extract(MONTH FROM c.inicio)";
        
        $query = $this->db->query($sql);        
        $retorno = $query->result_array();        
        $total = 0;       

        foreach ($retorno as $dados) {
            $valor['total'][] = $dados['total'];
            $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
            $total = $total+$dados['total'];
            $cor[] = $this->random_color();
        }

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['total']);
        $return['valor'].=']';
        $return['mes'] = '['; 
        $return['mes'].= implode(',',$descricao['mes']);
        $return['mes'].=']';
        $return['cor'] = '[';
        $return['cor'].= implode(',',$cor);
        $return['cor'].=']';
        $return['dados']    = $retorno;
        $return['total']    = $total;
        
        return $return;
    }

     public function totalChamadoEstado(){

        $sql="SELECT  count(*) as valor, emp.estado as descricao
                FROM    chamado c, empresas emp
                WHERE emp.id = c.cliente_id and  extract(YEAR FROM c.inicio) = '".date('Y')."'
                GROUP BY emp.estado";
        
        $query = $this->db->query($sql);        
        $retorno = $query->result_array();        
        $total = 0;       
        
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".$dados['descricao']."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['valor']);
        $return['valor'].=']';
        $return['descricao'] = '['; 
        $return['descricao'].= implode(',',$descricao['descricao']);
        $return['descricao'].=']';
        $return['cor'] = '[';
        $return['cor'].= implode(',',$cor);
        $return['cor'].=']';
        $return['dados']    = $retorno;
        $return['total']    = $total;
        $return['cores']    = $cores;
        
        return $return;

    }

    public function ultimosChamadosRealizados(){
         
        $sql    = "SELECT   distinct c.*, emp.razao_social, emp.cnpj, cs.descricao as status, ct.descricao as tipo 
                    FROM    chamado_atividade ca, chamado c, empresas emp, chamado_status cs, chamado_tipo ct 
                    WHERE   ca.chamado_id = c.id    and 
                            emp.id = c.cliente_id   and 
                            c.status_id = cs.id     and 
                            ct.id = c.tipo_id 
                    ORDER BY c.id DESC Limit 3"; 

        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }

    public function buscaChamadosAtividadesById($id){

        $sql    = "SELECT   distinct c.id,c.inicio, c.fim, concat(emp.cnpj,' | ',emp.razao_social) as cliente, 
                            emp.cidade, emp.estado, emp.cep, cs.descricao as status, ct.descricao as tipo, cd.descricao as defeito,
                            cas.descricao as status_atividade, ca.numero_serie, p.modelo, ca.observacao, ca.usuario_id, u.nome, cto.nome as contato,
                            ca.inicio as inicio_atividade, ca.fim as fim_atividade, c.descricao
                    FROM    chamado c 
                    LEFT JOIN   chamado_atividade ca            ON  ca.chamado_id = c.id
                    INNER JOIN  empresas emp                    ON  emp.id = c.cliente_id
                    INNER JOIN  chamado_status cs               ON  c.status_id = cs.id
                    INNER JOIN  chamado_tipo ct                 ON  ct.id = c.tipo_id
                    LEFT JOIN   chamado_defeito cd              ON  ca.defeito_id = cd.id
                    LEFT JOIN   chamado_atividade_status cas    ON  cas.id = ca.status_id
                    LEFT JOIN   produtos p                      ON  p.id = ca.modelo_id
                    LEFT JOIN   usuarios u                      ON   u.id = ca.usuario_id
                    INNER JOIN  usuarios cto                    ON  c.contato_id = cto.id                     
                    WHERE   c.id = ".$id." ORDER BY `c`.`id` ASC"; 

        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }

    public function buscaEnderecoEntregaPecas($chamado_id){
        $sql = "SELECT DISTINCT c.cliente_id as cliente_id, e.razao_social FROM chamado c, empresas e 
                WHERE   c.cliente_id = e.id and c.id = ".$chamado_id."
                UNION 
                SELECT DISTINCT cs.parceiro_id as cliente_id, e.razao_social FROM chamado c, chamado_atividade ca, chamado_subatividade cs, empresas e 
                WHERE c.id = ca.chamado_id and ca.id = cs.atividade_id and cs.parceiro_id = e.id and c.id = ".$chamado_id;
                
        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }

    public function buscaOrdensDePagamento($chamado_id){

        $sql = "SELECT  op.*, opi.qtd,opi.valor, ops.descricao as status, d.descricao as despesa, emp.razao_social 
                FROM    ordem_pagamento op, ordem_pagamento_itens opi, ordem_pagamento_status ops, despesas d, empresas emp 
                WHERE   op.id = opi.ordem_pagamento_id and 
                        op.status_id = ops.id and 
                        opi.despesa_id = d.id and 
                        emp.id = op.favorecido_id and 
                        op.chamado_id =".$chamado_id;

        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }

    public function buscaTecnicosChamados($chamado_id){

        $sql = "    SELECT  DISTINCT emp.razao_social, emp.cnpj, c.id as chamado 
                    FROM    chamado_subatividade cs, chamado_atividade ca, chamado c, empresas emp
                    WHERE   ca.chamado_id = c.id        and
                            cs.atividade_id = ca.id     and 
                            cs.parceiro_id = emp.id     and 
                            c.id = ". $chamado_id;

        $query  =   $this->db->query($sql);
        return  $query->result_array();                
    }

    public function buscaTodosChamadosPdf(){
        $sql = "SELECT  c.id, cs.descricao as status, ct.descricao as tipo, cont.nome as contato, concat(emp.razao_social,' | ', emp.cnpj) as cliente, c.inicio, c.fim, u.nome as usuario 
                FROM    chamado c, chamado_status cs, chamado_tipo ct, usuarios cont, empresas emp , usuarios u
                WHERE   c.cliente_id = emp.id   and
                        c.contato_id = cont.id  and
                        ct.id = c.tipo_id       and
                        c.usuario_id = u.id     and
                        cs.id = c.status_id     order by 1";

        $query  =   $this->db->query($sql);
        return  $query->result_array();                                   
    }

    public function retornaPecas($term){
        $sql = "SELECT  *
                FROM    pecas
                WHERE   descricao like '%".$term."%'";

        $query  =   $this->db->query($sql);
        return  $query->result_array();                                   
    }

    public function buscaAnexos($chamado_id){
        $sql    =   "   SELECT  ca.arquivo
                        FROM    chamado c, chamado_atividade a, chamado_upload ca
                        WHERE   c.id = a.chamado_id and ca.chamado_id = c.id and c.id = ".$chamado_id."  
                        UNION 
                        SELECT  cau.arquivo
                        FROM    chamado c, chamado_atividade a, chamado_atividade_upload cau
                        WHERE   c.id = a.chamado_id and a.id = cau.atividade_id and c.id = ".$chamado_id;
        
        $query  =   $this->db->query($sql);

        return  $query->result_array();
    }

     public function buscaClienteChamado($id){
         
        $sql    = "SELECT   e.* 
                    FROM    chamado c, empresas e 
                    WHERE   c.cliente_id = e.id and 
                            c.id = ".$id; 
                                                  
        $query  =   $this->db->query($sql);
        return  $query->row_array();
    }

    public function buscaAutorizacaoServico($id){
         
        $sql    = "SELECT   e.*
                    FROM    chamado c, empresas e
                    WHERE   c.cliente_id = e.id and
                            c.id = ".$id;

        $query  =   $this->db->query($sql);
        return  $query->row_array();
    }

    public function chamados(){

        $sql = "SELECT  c.id, cs.descricao as status, ct.descricao as tipo, cont.nome as contato,
                        concat(emp.razao_social,' | ', emp.cnpj) as cliente, c.inicio, c.fim, u.nome as usuario
                FROM    chamado c, chamado_status cs, chamado_tipo ct, usuarios cont, empresas emp , usuarios u
                WHERE   c.cliente_id = emp.id   and
                        c.contato_id = cont.id  and
                        ct.id = c.tipo_id       and
                        c.usuario_id = u.id     and
                        cs.id = c.status_id     order by 1";

        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }


    public function relatorioChamados($params){

        $where = '1=1';
        
        if($params['status_id'] != ''){
            $where.= ' and c.status_id = '.$params['status_id'];
        }
   
        if($params['cliente_id'] != ''){
            $where.= ' and c.cliente_id = '.$params['cliente_id'];
        }
   
        if($params['tecnico_id'] != ''){
            $where.= ' and cs.parceiro_id = '.$params['tecnico_id'];
        }

        $sql = 'SELECT c.id , concat(cliente.id," | ",cliente.cnpj," | ",cliente.razao_social) as clientes, csts.descricao as status, group_concat(tecnico.razao_social) as tecnicos, c.inicio, c.status_id 
                FROM chamado c 
                INNER JOIN empresas cliente         ON  cliente.id = c.cliente_id
                INNER JOIN chamado_status csts      ON  c.status_id = csts.id
                LEFT JOIN chamado_atividade ca      ON  ca.chamado_id = c.id 
                LEFT JOIN chamado_subatividade cs   ON  cs.atividade_id = ca.id 
                LEFT JOIN empresas tecnico          ON  tecnico.id = cs.parceiro_id 
                WHERE '.$where.'
                GROUP BY c.id';

        
        $query  =   $this->db->query($sql);
        return  $query->result_array();          
    }



    private function mes($mes){
        switch ($mes) {
            case 1:
                return 'Janeiro';
                break;
            case 2:
                return 'Fevereiro';
                break;    
            case 3:
                return 'Março';
                break;
            case 4:
                return 'Abril';
                break;    
            case 5:
                return 'Maio';
                break;    
            case 6:
                return 'Junho';
                break;       
            case 7:
                return 'Julho';
                break;         
            case 8:
                return 'Agosto';
                break;    
            case 9:
                return 'Setembro';
                break;        
            case 10:
                return 'Outubro';
                break;        
            case 11:
                return 'Novembro';
                break;        
            case 12:
                return 'Dezembro';
            case 'Janeiro':
                return 01;
            case 'Fevereiro':
                return 02;
            case 'Março':
                return 03;
            case 'Abril':
                return 04;
            case 'Maio':
                return 05;   
            case 'Junho':
                return 06;     
            case 'Julho':
                return 07;
            case 'Agosto':
                return 08;
            case 'Setembro':
                return 09;     
            case 'Outubro':
                return 10;
            case 'Novembro':
                return 11;
            case 'Dezembro':
                return 12;     
                break;                    
        }
    }
    
    private function random_color() {
	    $letters = '0123456789ABCDEF';	    
	    $color = '#';	    
	    for($i = 0; $i < 6; $i++) {
	        $index = rand(0,15);
	        $color .= $letters[$index];
	    }
	    return $color;
    }

    public function buscaChamadosInativos($usuario_id){
        $sql = 'SELECT  DISTINCT ca.chamado_id, DATEDIFF( date(NOW()), date(ca.inicio) ) as diff, u.nome, concat(e.cnpj,"|",e.razao_social) as razao_social, cs.descricao as status 
                    FROM    chamado c, chamado_atividade ca, usuarios u, empresas e,  chamado_status cs  
                    WHERE   cs.id = c.status_id         and 
                            c.cliente_id = e.id         and 
                            u.id = c.usuario_id         and 
                            c.id = ca.chamado_id        and 
                            c.status_id not in (3,4)    and 
                            ca.status_id not in (3,4)   and 
                            DATEDIFF( date(NOW()), date(ca.inicio) ) > 15 
                            and u.id='.$usuario_id.' 
                    GROUP BY ca.chamado_id order by 1';
        
        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }

    public function buscaTodosChamadosInativos(){
        $sql = 'SELECT  DISTINCT ca.chamado_id, DATEDIFF( date(NOW()), date(ca.inicio) ) as diff, u.nome,  cs.descricao as status
                    FROM  chamado c, (SELECT max(ca.id) as id, max(ca.inicio) as inicio, ca.chamado_id FROM chamado_atividade ca, chamado c where ca.chamado_id = c.id GROUP by c.id) ca, usuarios u, chamado_status cs  
                    WHERE   cs.id = c.status_id         and 
                            u.id = c.usuario_id         and 
                            c.id = ca.chamado_id        and 
                            c.status_id not in (3,4)    and                             
                            DATEDIFF( date(NOW()), date(ca.inicio) ) > 15 
                    GROUP BY ca.chamado_id order by 1';
        
        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }

}
?>