<?php

class ChamadoDefeitoModel extends CI_Model {
	
	public function select()
    {
        $this->db->select('cd.*');
		$this->db->select('COUNT(ca.id) total');
		$this->db->join('chamado_atividade ca', 'cd.id = ca.defeito_id', 'left');
		$this->db->order_by('total', 'DESC');
		$this->db->group_by('cd.id');
		return $this->db->get('chamado_defeito cd')->result_array();
    }
	
	public function insert($defeito)
	{
        $this->db->insert('chamado_defeito', $defeito);
		return $this->db->insert_id();
    }
	
	public function findByDescricao($descricao)
	{
		$this->db->where('descricao', $descricao);
        return $this->db->get('chamado_defeito')->row_array();
	}	
	
	
}
?>