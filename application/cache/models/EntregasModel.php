<?php

class EntregasModel extends CI_Model {

	public function add ($data) {
        
	    return $this->db->insert('entregas', $data);		

	}

    public function selectEntregas(){

        $sql =  "SELECT * FROM entregas";
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function getEntregasById($id){

        $sql =  "SELECT * FROM entregas where id =".$id;
        $query = $this->db->query($sql);
        return $query->row();
    }   

    public function excluirEntrega($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('entregas')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaEntregas($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('entregas', $dados)){
            return true;
        }else{
            return false;
        }

    }

}
?>