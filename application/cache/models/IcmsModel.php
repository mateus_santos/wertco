<?php

class IcmsModel extends CI_Model {

	public function insereIcms ($data) {
        
		return $this->db->insert('icms', $data);
	}

    public function getIcms()
    {

    	$sql =  "SELECT i.id, i.valor_tributo, e.nome from icms i, estados e WHERE i.estado_id = e.id";
    	$query = $this->db->query($sql);
    	return $query->result();

    }   

    public function getEstados()
    {
    	
    	$sql =  "SELECT * FROM estados";
    	$query = $this->db->query($sql);
    	return $query->result();
    	
    }   

    public function excluirIcms($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('icms')){
            return true;
        }else{
            return false;
        }
    }

    public function getIcmsPorEstado($estado_id)
    {

    	$sql =  "SELECT count(*) as total from icms WHERE estado_id =".$estado_id;
    	$query = $this->db->query($sql);
    	return $query->result_array();

    }

    public function getIcmsById($id)
    {

    	$sql =  "SELECT i.id, i.valor_tributo,i.estado_id FROM icms i WHERE i.id=".$id;
    	$query = $this->db->query($sql);
    	return $query->result_array();

    } 

    public function atualizaIcms($dados)
    {
        $update = array('valor_tributo' 	=>	str_replace(',','.',$dados['valor_tributo']));
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('icms', $update)){
            return true;
        }else{
            return false;
        }

    }

    public function getIcmsPorUF($uf)
    {
        
        $sql =  "SELECT i.fator, i.valor_tributo FROM icms i, estados e where i.estado_id = e.id and trim(e.uf) = trim('".$uf."')";        
        $query = $this->db->query($sql);
        return $query->result();
        
    }   


}
?>