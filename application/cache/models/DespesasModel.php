<?php

class DespesasModel extends CI_Model {

	public function insereDespesa ($data) {
        
		return $this->db->insert('despesas', $data);
	}

    public function getTipoDespesas()
    {

    	$sql =  "SELECT * FROM tipo_despesas";
    	$query = $this->db->query($sql);
    	return $query->result_array();

    }       

    public function excluirDespesa($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('despesas')){
            return true;
        }else{
            return false;
        }
    }

    public function getDespesas()
    {

    	$sql =  "SELECT d.*,td.descricao as tipo FROM despesas d, tipo_despesas td where td.id = d.tipo_despesa_id order by td.descricao DESC";
    	$query = $this->db->query($sql);
    	return $query->result_array();

    }

    public function getDespesasById($id)
    {

    	$sql =  "SELECT d.*,td.descricao as tipo FROM despesas d, tipo_despesas td where td.id = d.tipo_despesa_id and d.id=".$id;
    	$query = $this->db->query($sql);
    	return $query->row_array();

    } 

    public function atualizaDespesa($dados)
    {
        $this->db->where('id', $dados['id']);
        if($this->db->update('despesas', $dados)){
            return true;
        }else{
            return false;
        }

    }

}
?>