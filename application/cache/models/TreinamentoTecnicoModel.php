<?php

class TreinamentoTecnicoModel extends CI_Model {

	public function add ($data) {
        
	    return $this->db->insert('treinamento_tecnico', $data);		

	}   

	public function addTreinamento ($data) {
        
	    return $this->db->insert('treinamentos', $data);		

	}

	public function getTreinamento($id=null){

		if($id==null){
			$where = '1=1';
		}else{
			$where = "id = ".$id;
		}	

		$sql =  "SELECT * FROM treinamentos where ".$where;
		
        $query = $this->db->query($sql);
        $dados = $query->result_array();

        return  $dados;
	}


	public function verificaCadastro( $cpf ){

		$sql =  "SELECT  u.email,u.nome,u.endereco, u.telefone, e.cnpj, e.razao_social, e.fantasia, e.insc_estadual, e.endereco, e.bairro, e.cep, e.cidade, e.estado, e.credenciamento_crea, e.credenciamento_inmetro, e.representante_legal, u.empresa_id , u.id
				FROM 	usuarios u, empresas e  
				 WHERE 	u.empresa_id = e.id
				 and 	u.cpf ='".$cpf."'";
			
        $query = $this->db->query($sql);
        $dados = $query->row();
        
        return $dados;

	}

	public function verificaInscricao($usuario_id, $treinamento_id){

		$sql = 'SELECT 	count(*) as total FROM treinamento_tecnico
				WHERE 	usuario_id= '.$usuario_id.' and treinamento_id='.$treinamento_id;

		$query = $this->db->query($sql);
        $total = $query->row();

        return $total->total;			
	}

	public function getTotalInscricoes($descricao_evento){

		$sql =  "SELECT count(*) as total FROM treinamento_tecnico where descricao_treinamento = '".$descricao_evento."'";
        $query = $this->db->query($sql);
        $total = $query->row();

        return $total->total;
	}

	public function buscaTreinamentosById($id=null){

		if( $id == '' ){
			$where = '1=1';
		}else{
			$where = 'id='.$id;
		}

		$sql = "SELECT * from treinamento_tecnico where ".$where;
		$query = $this->db->query($sql);
        
        return $query->result();		
	}

	public function buscaTreinamentos($treinamento_id){

		$sql = "SELECT 	tt.*,t.descricao,t.total_vagas, t.id as treinamento_id from treinamento_tecnico tt
				LEFT JOIN treinamentos t on t.id = tt.treinamento_id		  
				WHERE  	tt.treinamento_id ='".$treinamento_id."'";
				
		$query = $this->db->query($sql);
        
        return $query->result();		
	}

	public function buscaTreinamentosDesc(){

		$sql = "SELECT * from treinamentos";
		$query = $this->db->query($sql);
        
        return $query->result();		
	}

	public function atualizaStatus($dados){
		 $update = array(
            'fl_aprovado' => $dados['ativo']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('treinamento_tecnico', $update)){
            return true;
        }else{
            return false;
        }
	}

	public function atualizaTreinamento($dados){
		 
        $this->db->where('id', $dados['id']);

        if($this->db->update('treinamentos', $dados)){
            return true;
        }else{
            return false;
        }
	}

	public function getAprovadosInscritos($treinamento_id){
		$sql =  "SELECT count(*) as total FROM treinamento_tecnico where fl_aprovado = 1 and treinamento_id ='".$treinamento_id."'";
        $query = $this->db->query($sql);
        $total = $query->row();

        return $total->total;
	}

	
   
}

?>