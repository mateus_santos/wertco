<?php

class ConfiguracaoConcentradorModel extends CI_Model {
	
	public function insert($concentrador)
	{
        $this->db->where('modelo', $concentrador['modelo']);
		$cct = $this->db->get('configuracao_concentrador')->row_array();
		if(count($cct) > 0){
			$retorno = 0;
		} else {
			$this->db->insert('configuracao_concentrador', $concentrador);
			$retorno = $this->db->insert_id();
		}
		return $retorno;
    }
	
	public function select()
    {
		return $this->db->get('configuracao_concentrador')->result_array();
    }
	
	
}
?>