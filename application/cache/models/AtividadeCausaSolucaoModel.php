<?php

class AtividadeCausaSolucaoModel extends CI_Model {
	
	public function insert($causa_solucao)
	{
        $this->db->insert('chamado_atividade_causa_solucao', $causa_solucao);
		return $this->db->insert_id();
    }
	
	public function update($causa_solucao)
    {
		$this->db->where('id', $causa_solucao['id']);       
        return  $this->db->update('chamado_atividade_causa_solucao', $causa_solucao);
	}
	
	public function select($atividade_id)
    {
		$this->db->select('cacs.*');
		$this->db->select('UPPER(cc.descricao) causa');
		$this->db->select('UPPER(cs.descricao) solucao');
		$this->db->join('chamado_causa cc', 'cc.id = cacs.causa_id');
		$this->db->join('chamado_solucao cs', 'cs.id = cacs.solucao_id');
		$this->db->where('cacs.atividade_id', $atividade_id);
		$this->db->where('cacs.status', 1);
        return $this->db->get('chamado_atividade_causa_solucao cacs')->result_array();
    }
	
	public function selectDuplicate($causa_id, $solucao_id, $atividade_id)
    {
		$this->db->where('cacs.causa_id', $causa_id);
		$this->db->where('cacs.solucao_id', $solucao_id);
		$this->db->where('cacs.atividade_id', $atividade_id);
        return $this->db->get('chamado_atividade_causa_solucao cacs')->row_array();
    }

}
?>