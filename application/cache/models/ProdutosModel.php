<?php

class ProdutosModel extends CI_Model {
	
 	public function select() {
        
        $sql =  "select p.*,tp.descricao as tipo_produto from produtos p, tipo_produtos tp WHERE p.tipo_produto_id = tp.id and p.tipo_produto_id != 5";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function tipoProdutos() {
        
        $sql =  "select * from tipo_produtos";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getProduto($id) {
        
        $sql =  "select * from produtos where id=".$id;
        $query = $this->db->query($sql);

        return $query->row_array();
    }

    public function insereProduto($data){

        $dados_insere =  array( 'codigo'            =>  $data['codigo'],
                                'modelo'            =>  $data['modelo'],
                                'modelo_tecnico'    =>  $data['modelo_tecnico'],
                                'descricao'         =>  $data['descricao'],
                                'valor_unitario'    =>  $data['valor_unitario'],                                
                                'tipo_produto_id'   =>  $data['tipo_produto_id'],
                                'vazao'             =>  $data['vazao'],
                                'nr_rotativa'       =>  $data['nr_rotativa']);

        if($this->db->insert('produtos', $dados_insere)){
            return true;
        }else{
            return false;
        }

    }

    public function excluirProduto($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('produtos')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaProduto($dados)
    {
        $update = array('id'                =>  $dados['id'],
                        'codigo'            =>  $dados['codigo'],
                        'descricao'         =>  $dados['descricao'],
                        'modelo'            =>  $dados['modelo'],
                        'modelo_tecnico'    =>  $dados['modelo_tecnico'],
                        'tipo_produto_id'   =>  $dados['tipo_produto_id'],
                        'valor_unitario'    =>  $dados['valor_unitario'],
                        'vazao'             =>  $dados['vazao'],
                        'nr_rotativa'       =>  $dados['nr_rotativa']);

        $this->db->where('id', $dados['id']);

        if($this->db->update('produtos', $update)){
            return true;
        }else{
            return false;
        }
    }
}
?>