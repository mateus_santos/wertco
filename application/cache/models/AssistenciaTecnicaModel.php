<?php

class AssistenciaTecnicaModel extends CI_Model {
	
	public function selectChamados()
    {
        /*$sql = "SELECT  a.*, s.descricao as status, t.descricao as tipo, assu.descricao as assunto, 
                        concat(u.nome,'|',tec.cnpj,'|',tec.razao_social) as tecnico, concat(e.cnpj,'-',e.razao_social) as cliente 
                FROM    assistencia_tecnica a, status_assist s, assist_tipo t, assist_assunto assu, usuarios u, empresas e, empresas tec
                where   a.assist_tipo_id = t.id 
                and     a.assist_assunto_id = assu.id
                and     a.status_assist_id = s.id 
                and     a.tecnico_id    =   u.id
                and     a.cliente_id    =   e.id 
                and     u.empresa_id    =   tec.id";*/
				
		$sql = "SELECT * FROM chamado";

        $query = $this->db->query($sql);
        return $query->result_array();

    }

	
	public function insereChamados ($data) {
        
		return $this->db->insert('assistencia_tecnica', $data);
	}

   
    public function verificaChamados($sql)
    {
    	
    	$query = $this->db->query($sql);
    	return $query->row_array();

    }

    public function verificaQtd($id)
    {
        $sql = "SELECT qtd,observacao FROM assistencia_tecnica WHERE id = ".$id;
        $query = $this->db->query($sql);
        return $query->row_array();

    }

    
    public function excluirAssistenciaTecnica($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('assistencia_tecnica')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaQtdObs($dados)
    {
        $update = array('qtd' 	        =>	$dados['qtd'],
                        'observacao'    =>  $dados['observacao']);
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('assistencia_tecnica', $update)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizarOrdemProducao($dados)
    { 
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('assistencia_tecnica', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function verificaAssistenciaTecnciaFechadaPorPedido($pedido_id){
        
        $sql = "SELECT count(*) AS total FROM assistencia_tecnica WHERE status_op_id != 3 and pedido_id=".$pedido_id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getOpsByPedido($pedido_id){
        
        $sql = "SELECT * FROM assistencia_tecnica WHERE pedido_id=".$pedido_id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function selectTipos(){

        $sql = "SELECT * FROM assist_tipo";
        $query = $this->db->query($sql);
        return $query->result_array();   

    }

    public function selectAssuntos(){

        $sql = "SELECT * FROM assist_assunto";
        $query = $this->db->query($sql);
        return $query->result_array();     

    }

    public function selectStatus(){

        $sql = "SELECT * FROM status_assist ORDER BY ordem";
        $query = $this->db->query($sql);
        return $query->result_array();     
           
    }
  
}
?>