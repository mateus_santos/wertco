<?php

class OrcamentosModel extends CI_Model {
	
 	public function select() {
        
        $sql =  "select p.*,tp.descricao as tipo_produto from produtos p, tipo_produtos tp WHERE p.tipo_produto_id = tp.id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function selectStatus() {
        
        $sql =  "select * from status_orcamentos";
        $query = $this->db->query($sql);
        return $query->result_array();
    }    

    public function atualizaOrcamento($update)
    {
        $this->db->where('id', $update['id']);
        
        if($this->db->update('orcamentos', $update)){

            return true;
        }else{

            return false;
        }
    }

    public function getOrcamento($id) {
        
        $sql =  "select * from orcamentos where id=".$id;
        $query = $this->db->query($sql);

        $row = $query->result();
        foreach($row as $retorno){}

        return $retorno;
    }

    public function insereOrcamento($data){

        if($this->db->insert('orcamentos', $data)){            
            return true;
        }else{
            return false;
        }

    }

    public function insereOrcamentoProdutos($data)
    {
        
        if($this->db->insert('orcamento_produtos', $data)){
            return true;
        }else{
            return false;
        }
    }

    public function listarProdutos($orcamento_id){
        $sql =  "   SELECT  p.*, op.qtd, op.valor 
                    FROM    orcamentos o, orcamento_produtos op, produtos p 
                    WHERE   op.orcamento_id = o.id  and 
                            p.id = op.produto_id    and     op.orcamento_id = ".$orcamento_id;
        $query = $this->db->query($sql);         
        return $query->result_array();

    }

    public function listarEmpresaOrcamento($orcamento_id)
    {
        $sql =  "SELECT e.*,o.emissao, o.valor_orcamento, u.nome, u.email as email_responsavel
                    FROM orcamentos o, empresas e, zona_atuacao z, usuarios u 
                    WHERE   o.empresa_id = e.id and 
                            z.estado = e.estado and
                            z.usuario_id = u.id and                              
                            o.id = ".$orcamento_id;
        $query = $this->db->query($sql);
        $row = $query->result_array();
        foreach($row as $retorno){}
        return $retorno;
    }
    
    public function buscaOrcamentos($dados) 
    {
            $aColumns = array('orcamento_id', 'solicitante', 'emissao', 'status','andamento', 'responsavel', 'origem', 'acoes');            
            $sLimit = "";
            if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
                $sLimit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] );
                $inicial = intval( $_POST['start'] );
                $final = intval( $_POST['length'] );
            }
            $sOrder = "ORDER BY o.id desc";

            if ( isset( $_POST['order'] ) ) {
                $sOrder = " ORDER BY ";
                for ( $i=0 ; $i< sizeof( $_POST['order'] ) ; $i++ ) {
                    if ( $_POST['columns'][$i]['orderable'] == "true" ) {
                        $sOrder .= $aColumns[ intval( $_POST['order'][$i]['column'] ) ]." ".($_POST['order'][$i]['dir']==='asc' ? 'asc' : 'desc') .", ";
                    }
                }
                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" ) {
                    $sOrder = " ORDER BY {$sIndexColumn} DESC";
                }
            }
            $sWhere = '';
            if ( isset($_POST['search']) && $_POST['search']['value'] != "" ) {
                
                for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
                    if ( isset($_POST['columns'][$i]['searchable']) && $_POST['columns'][$i]['searchable'] == "true" ) {
                        switch ($aColumns[$i]) {
                            case 'orcamento_id':
                                $sWhere .= " AND (o.id LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'solicitante':
                                $sWhere .= " concat(e.cnpj,' ',e.razao_social,' ',e.id) LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'emissao':
                                $sWhere .= " o.emissao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'status':
                                $sWhere .= " s.descricao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'responsavel':
                                $sWhere .= " usu.nome LIKE '%". $_POST['search']['value'] ."%' OR" ;
                                break;
                            case 'origem':
                                $sWhere .= " o.origem LIKE '%". $_POST['search']['value'] ."%')";
                                break;
                        }
                    }
                }
            }

            $sql =  "SELECT     e.*, concat(e.id,' - ',e.cnpj,' - ',e.razao_social) as solicitante, date_format(o.emissao,'%d/%m/%Y') as emissao, o.validade, s.descricao as status,o.id as orcamento_id, o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, o.contato_posto, o.observacao
                        FROM        orcamentos o 
                        INNER JOIN  empresas e                      ON o.empresa_id = e.id 
                        INNER JOIN  status_orcamentos s             ON s.id = o.status_orcamento_id 
                        LEFT  JOIN   orcamento_responsavel as orr    ON orr.orcamento_id = o.id 
                        LEFT  JOIN   usuarios usu                    ON usu.id = orr.usuario_id
                        WHERE       o.status_orcamento_id != 13
                        ".$sWhere." ".$sOrder;

            $query = $this->db->query($sql);
            
            $orcamentos = $query->result_array();
            
            $total      = count($orcamentos);
            
            $orcamentos = array_slice($orcamentos, $inicial, $final);  

            foreach ($orcamentos as $key => $orcamento) {
                if( $orcamento['status'] == "aberto" ){
                    $status = "btn m-btn--pill m-btn--air btn-secondary";
                    $background = '';
                }elseif( $orcamento['status'] == "fechado" ){
                    $status = "btn m-btn--pill m-btn--air btn-success";
                    $background = '';
                }elseif( $orcamento['status'] == "perdido outros" || $orcamento['status'] == "perdido para wayne" || $orcamento['status'] == "perdido para gilbarco" ){
                    $status = "btn m-btn--pill m-btn--air btn-danger";
                    $background = '';
                }elseif( $orcamento['status'] == "cancelado" ){
                    $status = "btn m-btn--pill m-btn--air btn-warning";
                    $background = '';
                }elseif( $orcamento['status'] == "orçamento entregue" ){
                    $status = "btn m-btn--pill m-btn--air btn-info";
                    $background = '';
                }elseif( $orcamento['status'] == "em negociação"){
                    $status = "btn m-btn--pill m-btn--air btn-primary";
                    $background = '';
                }elseif($orcamento['status'] == "Solicitação de Orçamentos - Indicador"){
                    $status = "btn m-btn--pill m-btn--air";
                    $background = "style='background: #ff9800;'";
                }else{
                    $status = "btn m-btn--pill m-btn--air";
                    $background = "style='background: #999;'";
                }

                $orcamentos[$key]['status'] = '<button id="'.$orcamento['orcamento_id'].'" class="status '.$status.'" status="'.$orcamento['status'].'" '. $background.' title="Status do orçamento" onclick="status('.$orcamento['orcamento_id'].');"> '.ucfirst($orcamento['status']).'   </button>';

                $orcamentos[$key]['andamento'] = '<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour"><i title="Andamento do Orçamento" onclick="andamento('.$orcamento['orcamento_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i></button>';
                $orcamentos[$key]['responsavel'] = $orcamento['responsavel'].'<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air responsavel_tour"><i title="Alterar Responsável" onclick="responsavel('.$orcamento['orcamento_id'].', '.$orcamento['responsavel_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-user-ok responsavel" style="cursor: pointer;" ></i>';
                $orcamentos[$key]['acoes'] = '<span style="overflow: visible; width: 110px;" class="">                              
                                                <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill visualizar_orcamento" onclick="visualizar_orcamento('.$orcamento['orcamento_id'].')" title="Visualizar" orcamento_id="'.$orcamento['orcamento_id'].'" target="_blank" >
                                                    <i class="la la-eye visualizar"></i>
                                                </a> 

                                                <button  id="excluir" onclick="excluirOrcamento('.$orcamento['orcamento_id'].');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" orcamento_id="'.$orcamento['orcamento_id'].'">
                                                    <i class="la la-trash"></i>
                                                </button>
                                            </span>';
            }

            $sql  = "SELECT COUNT(*) AS Total FROM orcamentos";
            $query = $this->db->query($sql);
            $iTotal = $query->row_array();
            $output = array("iTotalRecords" => $iTotal['Total'],
                            "iTotalDisplayRecords" => $total,
                            "aaData" => $orcamentos     ); 
            
        return $output;
    
    }

    public function buscaOrcamentosExpirados($dados)  
    {
            $aColumns = array('orcamento_id', 'solicitante', 'emissao', 'status','andamento', 'responsavel', 'origem', 'acoes');
            
            $sLimit = "";
            if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
                $sLimit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] );
                $inicial = intval( $_POST['start'] );
                $final = intval( $_POST['length'] );
            }
            $sOrder = "ORDER BY o.id desc";

            if ( isset( $_POST['order'] ) ) {
                $sOrder = " ORDER BY ";
                for ( $i=0 ; $i< sizeof( $_POST['order'] ) ; $i++ ) {
                    if ( $_POST['columns'][$i]['orderable'] == "true" ) {
                        $sOrder .= $aColumns[ intval( $_POST['order'][$i]['column'] ) ]." ".($_POST['order'][$i]['dir']==='asc' ? 'asc' : 'desc') .", ";
                    }
                }
                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" ) {
                    $sOrder = " ORDER BY {$sIndexColumn} DESC";
                }
            }
            $sWhere = '';
            if ( isset($_POST['search']) && $_POST['search']['value'] != "" ) {
                $sWhere = ""; 
                for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
                    if ( isset($_POST['columns'][$i]['searchable']) && $_POST['columns'][$i]['searchable'] == "true" ) {
                        switch ($aColumns[$i]) {
                            case 'orcamento_id':
                                $sWhere .= " AND (o.id LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'solicitante':
                                $sWhere .= " concat(e.cnpj,' ',e.razao_social,' ',e.id) LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'emissao':
                                $sWhere .= " o.emissao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'status':
                                $sWhere .= " s.descricao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'responsavel':
                                $sWhere .= " usu.nome LIKE '%". $_POST['search']['value'] ."%' OR" ;
                                break;
                            case 'origem':
                                $sWhere .= " o.origem LIKE '%". $_POST['search']['value'] ."%')";
                                break;
                        }
                    }
                }
            }
            $sql =  "SELECT     e.*, concat(e.id,' - ',e.cnpj,' - ',e.razao_social) as solicitante, date_format(o.emissao,'%d/%m/%Y') as emissao, o.validade, s.descricao as status,o.id as orcamento_id, o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, o.contato_posto, o.observacao
                        FROM        orcamentos o 
                        INNER JOIN  empresas e                      ON o.empresa_id = e.id 
                        INNER JOIN  status_orcamentos s             ON s.id = o.status_orcamento_id 
                        LEFT JOIN   orcamento_responsavel as orr    ON orr.orcamento_id = o.id 
                        LEFT JOIN   usuarios usu                    ON usu.id = orr.usuario_id
                        WHERE       o.status_orcamento_id = 13
                        ".$sWhere." ".$sOrder;

            $query = $this->db->query($sql);
            $orcamentos = $query->result_array();
            $total = count($orcamentos);
            $orcamentos = array_slice($orcamentos, $inicial, $final);  
            foreach ($orcamentos as $key => $orcamento) {

                if( $orcamento['status'] == "Expirados" ){
                    $status = "btn m-btn m-btn--pill m-btn--air m-btn--gradient-from-focus m-btn--gradient-to-warning";
                    $background = '';
                }

                $orcamentos[$key]['status'] = '<button id="'.$orcamento['orcamento_id'].'" class="status '.$status.'" status="'.$orcamento['status'].'" '. $background.' title="Status do orçamento" onclick="status('.$orcamento['orcamento_id'].');"> '.ucfirst($orcamento['status']).'   </button>';

                $orcamentos[$key]['andamento'] = '<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour"><i title="Andamento do Orçamento" onclick="andamento('.$orcamento['orcamento_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i></button>';
                $orcamentos[$key]['responsavel'] = $orcamento['responsavel'].'<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air responsavel_tour"><i title="Alterar Responsável" onclick="responsavel('.$orcamento['orcamento_id'].', '.$orcamento['responsavel_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-user-ok responsavel" style="cursor: pointer;" ></i>';
                $orcamentos[$key]['acoes'] = '<span style="overflow: visible; width: 110px;" class="">                              
                                                <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill visualizar_orcamento" onclick="visualizar_orcamento('.$orcamento['orcamento_id'].')" title="Visualizar" orcamento_id="'.$orcamento['orcamento_id'].'" target="_blank" >
                                                    <i class="la la-eye visualizar"></i>
                                                </a> 

                                                <button  id="excluir" onclick="excluirOrcamento('.$orcamento['orcamento_id'].');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" orcamento_id="'.$orcamento['orcamento_id'].'">
                                                    <i class="la la-trash"></i>
                                                </button>
                                            </span>';
            }

            $sql  = "SELECT COUNT(*) AS Total FROM orcamentos";
            $query = $this->db->query($sql);
            $iTotal = $query->row_array();
            $output = array("iTotalRecords" => $iTotal['Total'],
                            "iTotalDisplayRecords" => $total,
                            "aaData" => $orcamentos     ); 
            
        return $output;
    
    }

    public function buscaOrcamentosZona($dados) 
    {
            $aColumns = array('orcamento_id', 'solicitante', 'emissao', 'status','andamento', 'responsavel', 'origem', 'acoes');            
            $sLimit = "";
            if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
                $sLimit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] );
                $inicial = intval( $_POST['start'] );
                $final = intval( $_POST['length'] );
            }
            $sOrder = "ORDER BY o.id desc";

            if ( isset( $_POST['order'] ) ) {
                $sOrder = " ORDER BY ";
                for ( $i=0 ; $i< sizeof( $_POST['order'] ) ; $i++ ) {
                    if ( $_POST['columns'][$i]['orderable'] == "true" ) {
                        $sOrder .= $aColumns[ intval( $_POST['order'][$i]['column'] ) ]." ".($_POST['order'][$i]['dir']==='asc' ? 'asc' : 'desc') .", ";
                    }
                }
                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" ) {
                    $sOrder = " ORDER BY {$sIndexColumn} DESC";
                }
            }
            $sWhere = '';
            if ( isset($_POST['search']) && $_POST['search']['value'] != "" ) {
                
                for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
                    if ( isset($_POST['columns'][$i]['searchable']) && $_POST['columns'][$i]['searchable'] == "true" ) {
                        switch ($aColumns[$i]) {
                            case 'orcamento_id':
                                $sWhere .= " AND (o.id LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'solicitante':
                                $sWhere .= " concat(e.cnpj,' ',e.razao_social,' ',e.id) LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'emissao':
                                $sWhere .= " o.emissao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'status':
                                $sWhere .= " s.descricao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'responsavel':
                                $sWhere .= " usu.nome LIKE '%". $_POST['search']['value'] ."%' OR" ;
                                break;
                            case 'origem':
                                $sWhere .= " o.origem LIKE '%". $_POST['search']['value'] ."%')";
                                break;
                        }
                    }
                }
            }

            $sql =  "SELECT     e.*, concat(e.id,' - ',e.cnpj,' - ',e.razao_social) as solicitante, date_format(o.emissao,'%d/%m/%Y') as emissao, o.validade, s.descricao as status,o.id as orcamento_id, o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, o.contato_posto, o.observacao
                        FROM        orcamentos o 
                        INNER JOIN  empresas e                      ON o.empresa_id = e.id 
                        INNER JOIN  status_orcamentos s             ON s.id = o.status_orcamento_id 
                        LEFT  JOIN   orcamento_responsavel as orr    ON orr.orcamento_id = o.id 
                        LEFT  JOIN   usuarios usu                    ON usu.id = orr.usuario_id
                        WHERE       o.status_orcamento_id != 13 and e.estado in (select estado from zona_atuacao where usuario_id = ".$dados['usuario_id'].")
                        ".$sWhere." ".$sOrder;

            $query = $this->db->query($sql);
            
            $orcamentos = $query->result_array();
            
            $total      = count($orcamentos);
            
            $orcamentos = array_slice($orcamentos, $inicial, $final);  

            foreach ($orcamentos as $key => $orcamento) {
                if( $orcamento['status'] == "aberto" ){
                    $status = "btn m-btn--pill m-btn--air btn-secondary";
                    $background = '';
                }elseif( $orcamento['status'] == "fechado" ){
                    $status = "btn m-btn--pill m-btn--air btn-success";
                    $background = '';
                }elseif( $orcamento['status'] == "perdido outros" || $orcamento['status'] == "perdido para wayne" || $orcamento['status'] == "perdido para gilbarco" ){
                    $status = "btn m-btn--pill m-btn--air btn-danger";
                    $background = '';
                }elseif( $orcamento['status'] == "cancelado" ){
                    $status = "btn m-btn--pill m-btn--air btn-warning";
                    $background = '';
                }elseif( $orcamento['status'] == "orçamento entregue" ){
                    $status = "btn m-btn--pill m-btn--air btn-info";
                    $background = '';
                }elseif( $orcamento['status'] == "em negociação"){
                    $status = "btn m-btn--pill m-btn--air btn-primary";
                    $background = '';
                }elseif($orcamento['status'] == "Solicitação de Orçamentos - Indicador"){
                    $status = "btn m-btn--pill m-btn--air";
                    $background = "style='background: #ff9800;'";
                }else{
                    $status = "btn m-btn--pill m-btn--air";
                    $background = "style='background: #999;'";
                }

                $orcamentos[$key]['status'] = '<button id="'.$orcamento['orcamento_id'].'" class="status '.$status.'" status="'.$orcamento['status'].'" '. $background.' title="Status do orçamento" onclick="status('.$orcamento['orcamento_id'].');"> '.ucfirst($orcamento['status']).'   </button>';

                $orcamentos[$key]['andamento'] = '<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour"><i title="Andamento do Orçamento" onclick="andamento('.$orcamento['orcamento_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i></button>';
                $orcamentos[$key]['responsavel'] = $orcamento['responsavel'].'<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air responsavel_tour"><i title="Alterar Responsável" onclick="responsavel('.$orcamento['orcamento_id'].', '.$orcamento['responsavel_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-user-ok responsavel" style="cursor: pointer;" ></i>';
                $orcamentos[$key]['acoes'] = '<span style="overflow: visible; width: 110px;" class="">                              
                                                <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill visualizar_orcamento" onclick="visualizar_orcamento('.$orcamento['orcamento_id'].')" title="Visualizar" orcamento_id="'.$orcamento['orcamento_id'].'" target="_blank" >
                                                    <i class="la la-eye visualizar"></i>
                                                </a> 

                                                <button  id="excluir" onclick="excluirOrcamento('.$orcamento['orcamento_id'].');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" orcamento_id="'.$orcamento['orcamento_id'].'">
                                                    <i class="la la-trash"></i>
                                                </button>
                                            </span>';
            }

            $sql  = "SELECT COUNT(*) AS Total FROM orcamentos";
            $query = $this->db->query($sql);
            $iTotal = $query->row_array();
            $output = array("iTotalRecords" => $iTotal['Total'],
                            "iTotalDisplayRecords" => $total,
                            "aaData" => $orcamentos     ); 
            
        return $output;
    
    }

     public function buscaOrcamentosExpiradosZona($dados)  
    {
            $aColumns = array('orcamento_id', 'solicitante', 'emissao', 'status','andamento', 'responsavel', 'origem', 'acoes');
            
            $sLimit = "";
            if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
                $sLimit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] );
                $inicial = intval( $_POST['start'] );
                $final = intval( $_POST['length'] );
            }
            $sOrder = "ORDER BY o.id desc";

            if ( isset( $_POST['order'] ) ) {
                $sOrder = " ORDER BY ";
                for ( $i=0 ; $i< sizeof( $_POST['order'] ) ; $i++ ) {
                    if ( $_POST['columns'][$i]['orderable'] == "true" ) {
                        $sOrder .= $aColumns[ intval( $_POST['order'][$i]['column'] ) ]." ".($_POST['order'][$i]['dir']==='asc' ? 'asc' : 'desc') .", ";
                    }
                }
                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" ) {
                    $sOrder = " ORDER BY {$sIndexColumn} DESC";
                }
            }
            $sWhere = '';
            if ( isset($_POST['search']) && $_POST['search']['value'] != "" ) {
                $sWhere = ""; 
                for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
                    if ( isset($_POST['columns'][$i]['searchable']) && $_POST['columns'][$i]['searchable'] == "true" ) {
                        switch ($aColumns[$i]) {
                            case 'orcamento_id':
                                $sWhere .= " AND (o.id LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'solicitante':
                                $sWhere .= " concat(e.cnpj,' ',e.razao_social,' ',e.id) LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'emissao':
                                $sWhere .= " o.emissao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'status':
                                $sWhere .= " s.descricao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'responsavel':
                                $sWhere .= " usu.nome LIKE '%". $_POST['search']['value'] ."%' OR" ;
                                break;
                            case 'origem':
                                $sWhere .= " o.origem LIKE '%". $_POST['search']['value'] ."%')";
                                break;
                        }
                    }
                }
            }
            $sql =  "SELECT     e.*, concat(e.id,' - ',e.cnpj,' - ',e.razao_social) as solicitante, date_format(o.emissao,'%d/%m/%Y') as emissao, o.validade, s.descricao as status,o.id as orcamento_id, o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, o.contato_posto, o.observacao
                        FROM        orcamentos o 
                        INNER JOIN  empresas e                      ON o.empresa_id = e.id 
                        INNER JOIN  status_orcamentos s             ON s.id = o.status_orcamento_id 
                        LEFT JOIN   orcamento_responsavel as orr    ON orr.orcamento_id = o.id 
                        LEFT JOIN   usuarios usu                    ON usu.id = orr.usuario_id
                        WHERE       o.status_orcamento_id = 13 and e.estado in (select estado from zona_atuacao where usuario_id = ".$dados['usuario_id'].")
                        ".$sWhere." ".$sOrder;

            $query = $this->db->query($sql);
            $orcamentos = $query->result_array();
            $total = count($orcamentos);
            $orcamentos = array_slice($orcamentos, $inicial, $final);  
            foreach ($orcamentos as $key => $orcamento) {

                if( $orcamento['status'] == "Expirados" ){
                    $status = "btn m-btn m-btn--pill m-btn--air m-btn--gradient-from-focus m-btn--gradient-to-warning";
                    $background = '';
                }

                $orcamentos[$key]['status'] = '<button id="'.$orcamento['orcamento_id'].'" class="status '.$status.'" status="'.$orcamento['status'].'" '. $background.' title="Status do orçamento" onclick="status('.$orcamento['orcamento_id'].');"> '.ucfirst($orcamento['status']).'   </button>';

                $orcamentos[$key]['andamento'] = '<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour"><i title="Andamento do Orçamento" onclick="andamento('.$orcamento['orcamento_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i></button>';
                $orcamentos[$key]['responsavel'] = $orcamento['responsavel'].'<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air responsavel_tour"><i title="Alterar Responsável" onclick="responsavel('.$orcamento['orcamento_id'].', '.$orcamento['responsavel_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-user-ok responsavel" style="cursor: pointer;" ></i>';
                $orcamentos[$key]['acoes'] = '<span style="overflow: visible; width: 110px;" class="">                              
                                                <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill visualizar_orcamento" onclick="visualizar_orcamento('.$orcamento['orcamento_id'].')" title="Visualizar" orcamento_id="'.$orcamento['orcamento_id'].'" target="_blank" >
                                                    <i class="la la-eye visualizar"></i>
                                                </a> 

                                                <button  id="excluir" onclick="excluirOrcamento('.$orcamento['orcamento_id'].');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" orcamento_id="'.$orcamento['orcamento_id'].'">
                                                    <i class="la la-trash"></i>
                                                </button>
                                            </span>';
            }

            $sql  = "SELECT COUNT(*) AS Total FROM orcamentos";
            $query = $this->db->query($sql);
            $iTotal = $query->row_array();
            $output = array("iTotalRecords" => $iTotal['Total'],
                            "iTotalDisplayRecords" => $total,
                            "aaData" => $orcamentos     ); 
            
        return $output;
    
    }

    public function buscaOrcamentosRepresentantes($user_id)
    {
        
        $sql =  "SELECT e.*, o.emissao,o.validade, s.descricao as status,o.id as orcamento_id,
                        o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, usu_gera.nome as criado_por  
                    FROM        orcamentos o 
                    INNER JOIN  empresas e                      ON  o.empresa_id = e.id 
                    INNER JOIN  status_orcamentos s             ON  s.id = o.status_orcamento_id 
                    LEFT JOIN   orcamento_responsavel as orr    ON  orr.orcamento_id = o.id 
                    LEFT JOIN   usuarios usu                    ON  usu.id = orr.usuario_id 
                    INNER JOIN  usuarios usu_gera               ON  o.solicitante_id = usu_gera.id
                    INNER JOIN  empresas emp_gera               ON  usu_gera.empresa_id = emp_gera.id
                    WHERE       orr.usuario_id  = ".$user_id."   OR  
                                emp_gera.id     = (select empresa_id from usuarios where id = ".$user_id." ) 
                    ORDER BY  o.id desc";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function insereOrcamentoAndamentos($data)
    {
        if($this->db->insert('orcamento_andamentos', $data))
        {
            
            return $this->db->insert_id();
        }else{
            return false;
        }
    }



    public function buscaAndamentos($orcamento_id)
    {
        $sql =  "SELECT oa.*, COALESCE(u.nome,NULL,'') as usuario FROM orcamento_andamentos oa
                    LEFT JOIN orcamento_responsavel orr ON  orr.orcamento_id = oa.orcamento_id 
                    LEFT JOIN usuarios u                ON  u.id = oa.usuario_id 
                    WHERE  oa.orcamento_id = ".$orcamento_id;


        $query = $this->db->query($sql);
        $row = $query->result_array();
        return $row;
    }

    public function atualizaStatusOrcamento($dados)
    {
         $update = array(
            'status_orcamento_id' => $dados['status_orcamento_id']
        );

        $this->db->where('id', $dados['id']);
        
        if($this->db->update('orcamentos', $update)){

            return true;
        }else{
            return false;
        }
    }

    public function atualizaResponsavelOrcamento($dados)
    {
         $update = array(
            'usuario_id' => $dados['usuario_id']
        );
         
        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_responsavel', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function insereResponsavelOrcamento($data)
    {
        if($this->db->insert('orcamento_responsavel', $data))
        {
            return true;
        }else{
            return false;
        }        
    }

    public function getOrcamentoProdutos($id) {
        
        $sql =  "SELECT op.id as orcamento_produto_id,op.produto_id,op.qtd,op.valor,op.orcamento_id, o.id as id_orcamento,
                        o.empresa_id,
                        o.emissao,
                        o.validade,
                        o.status_orcamento_id,
                        o.origem,
                        o.solicitante_id,
                        o.usuario_id,
                        o.valor_orcamento ,
                        o.frete_id,
                        o.entrega_id , 
                        o.forma_pagto_id,
                        o.indicador,
                        o.indicador_id,
						o.contato_posto,
                        o.cel_contato_posto,
                        u.nome as contato_do_posto,
                        u.celular,
                        o.contato_id,
                        o.observacao,
						p.*, e.*, i.valor_tributo, so.descricao as status, i.fator, o.fl_especial,
                        o.garantia 
            FROM orcamentos o
            LEFT JOIN  orcamento_produtos op ON o.id = op.orcamento_id
            LEFT JOIN  usuarios u ON o.contato_id = u.id
            INNER JOIN  status_orcamentos so ON o.status_orcamento_id = so.id
            LEFT JOIN  produtos p ON p.id = op.produto_id
            INNER JOIN  empresas e ON e.id = o.empresa_id
            LEFT JOIN   estados ON  e.estado = estados.uf
            LEFT JOIN   icms i ON i.estado_id = estados.id                   
                WHERE   o.id=".$id;        

        $query = $this->db->query($sql);
        $retorno = $query->result();
        return $retorno;
    }

    public function getSolicitante($id){

        $sql = "SELECT u.nome, e.razao_social , e.cnpj, e.telefone, u.email, concat(e.cidade,'/', e.estado) as local, e.tp_cadastro  FROM usuarios u, empresas e, orcamentos o WHERE u.empresa_id = e.id and o.solicitante_id = u.id and o.id=".$id;
        
        $query = $this->db->query($sql);
        $retorno = $query->result();
        return $retorno;
    }

    public function getIndicador($id){

        $sql = "SELECT concat(u.nome, ' | ', e.razao_social ,' | ', e.cnpj, ' | ', e.email, ' | ', e.telefone) as indicador, e.telefone, u.email, u.id, o.status_orcamento_id  FROM usuarios u, empresas e, orcamentos o WHERE u.empresa_id = e.id and o.indicador_id = u.id and o.id=".$id;
        
        $query = $this->db->query($sql);
        $retorno = $query->result();        
        return $retorno;
    }

    public function getOrcamentoDesconto($orcamento_id) {

        $sql = "SELECT * FROM orcamento_descontos WHERE orcamento_id = ".$orcamento_id." order by id desc limit 1";
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno;
    }

    public function buscaOrcamentosIndicadores($usuario_id)
    {
        
        $sql =  "SELECT e.*,o.emissao,o.validade, s.descricao as status, o.id as orcamento_id, o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id 
                    FROM        orcamentos o 
                    INNER JOIN  empresas e                      ON o.empresa_id = e.id 
                    INNER JOIN  status_orcamentos s             ON s.id = o.status_orcamento_id 
                    LEFT JOIN   orcamento_responsavel as orr    ON orr.orcamento_id = o.id 
                    LEFT JOIN   usuarios usu                    ON usu.id = orr.usuario_id 
                    WHERE orr.usuario_id = ".$usuario_id."
                    ORDER BY o.status_orcamento_id, orcamento_id ASC";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function insereOrcamentosDesconto($data){        

        if($this->db->insert('orcamento_descontos', $data)){ 
            return true;
        }else{
            return false;
        }

    }

    public function buscaProximosStatusOrcamento($orcamento_id){
        $sql = "SELECT s.id, s.descricao,s.ordem FROM status_orcamentos s 
                WHERE  s.ordem > (SELECT so.ordem FROM status_orcamentos so, orcamentos o WHERE so.id = o.status_orcamento_id and o.id = ".$orcamento_id.") order by s.ordem ASC";

        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno;
    }
    
    public function verificaOrcamento($cnpj){
        $sql        =   "SELECT count(*) as total FROM orcamentos o, empresas e
						  WHERE o.empresa_id = e.id   AND
								o.status_orcamento_id NOT IN (2,3,4) AND 
                                e.cnpj = '".$cnpj."'    ";
        // AND  DATEDIFF(date(now()),date(o.emissao)) <= 15
        $query      =   $this->db->query( $sql );
        $retorno    =   $query->row();
        return $retorno;
    }

    public function verificaOrcamentoConcorrente($cnpj){

        $sql        =   "SELECT count(*) as total FROM orcamentos o, empresas e
                          WHERE o.empresa_id = e.id   AND
                                o.status_orcamento_id NOT IN (2,3,4) AND 
                                e.cnpj = '".$cnpj."' ";
        $query      =   $this->db->query(   $sql    );
        $retorno    =   $query->row();
        return $retorno;
    }

    public function verificaOrcamentoCliente($empresa_id){
        $sql        =   "SELECT count(*) as total FROM orcamentos o, empresas e WHERE o.empresa_id = e.id AND o.status_orcamento_id <> 2 AND o.empresa_id = '".$empresa_id."'";        
        $query      =   $this->db->query( $sql );
        $retorno    =   $query->row();
        return $retorno;
    }

    public function atualizaValorProduto($dados)
    {
         $update = array(
            'valor' => $dados['valor']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_produtos', $update)){
            return true;
        }else{
            return false;
        }
    }


    public function getResponsavel($orcamento_id){
        $sql = "SELECT  u.nome as responsavel, e.razao_social FROM orcamento_responsavel o, usuarios u, empresas e 
                where   e.id = u.empresa_id and o.usuario_id = u.id and o.orcamento_id=".$orcamento_id." 
                order by o.id desc LIMIT 1";
        $query      =   $this->db->query( $sql );
        $retorno    =   $query->row();
        return $retorno;
    }

    public function alterarProdutoOrcamento($dados)
    {
        $update = array(
            'produto_id' => $dados['produto_id']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_produtos', $update)){
            return true;
        }else{
            return false;
        }
    }
    // Alteraração de produtos via ajax no sistema dos representantes
    public function alterarProdutoOrcamentoInd3($dados)
    {
        $update = array(
            'produto_id' => $dados['produto_id'],
            'valor'     =>  $dados['valor']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_produtos', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function alterarQtdProduto($dados)
    {
         $update = array(
            'qtd' => $dados['qtd']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_produtos', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function alterarObservacao($dados)
    {         
        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamentos', $dados)){
            return true;
        }else{
            return false;
        }
    }

    public function excluirProduto($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('orcamento_produtos')){
            return true;
        }else{
            return false;
        }
    }

    public function alterarFormaPagto($dados)
    {
         $update = array(
            'forma_pagto_id' => $dados['forma_pagto_id']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamentos', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function alterarEntrega($dados)
    {
         $update = array(
            'entrega_id' => $dados['entrega_id']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamentos', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function alterarFrete($dados)
    {
         $update = array(
            'frete_id' => $dados['frete_id']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamentos', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function getFreteEntregaPagto($orcamento_id)
    {
        $sql = "SELECT f.descricao as frete, p.descricao as forma_pagto, e.descricao as entrega from orcamentos o, forma_pagto p, entregas e, fretes f WHERE o.frete_id = f.id and o.entrega_id = e.id and o.forma_pagto_id = p.id and o.id =".$orcamento_id;
        $query      =   $this->db->query( $sql );
        $retorno    =   $query->row();
        return $retorno;
    }

    public function selectTotalStatus(){
         $sql = "SELECT count(o.status_orcamento_id) as valor, s.descricao 
                    FROM status_orcamentos s, orcamentos o, status_orcamentos so 
                    WHERE s.id = o.status_orcamento_id and o.status_orcamento_id = so.id 
                    GROUP BY o.status_orcamento_id order by so.ordem asc";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".$dados['descricao']."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['valor']);
        $return['valor'].=']';
        $return['descricao'] = '['; 
        $return['descricao'].= implode(',',$descricao['descricao']);
        $return['descricao'].=']';
        $return['cor'] = '[';
        $return['cor'].= implode(',',$cor);
        $return['cor'].=']';
        $return['dados']    = $retorno;
        $return['total']    = $total;
        $return['cores']    = $cores;
        return $return;
    }

    public function selectTotalStatusPeriodo($periodo){
        $data_subtraida = date('Y-m-d', strtotime('-'.$periodo.' days'));
        $sql = "SELECT count(o.status_orcamento_id) as valor, s.descricao 
                    FROM status_orcamentos s, orcamentos o, status_orcamentos so 
                    WHERE s.id = o.status_orcamento_id and o.status_orcamento_id = so.id and o.emissao between '".$data_subtraida."' and '".date('Y-m-d')."' GROUP BY o.status_orcamento_id order by so.ordem asc";
                 
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;
    }

    public function totalFechadoMes(){
        $sql="  SELECT  sum(op.valor*op.qtd) as total, extract(MONTH FROM oa.dthr_andamento) AS mes from orcamento_produtos op, orcamentos o,
                        orcamento_andamentos oa 
                WHERE   op.orcamento_id = o.id and 
                        oa.orcamento_id = o.id and 
                        o.status_orcamento_id = 2 and
                        oa.status_orcamento_id = 2 and 
                        oa.andamento like '%Orçamento Fechado%' and
                        extract(YEAR FROM oa.dthr_andamento) = '".date('Y')."'
                GROUP BY    extract(MONTH FROM oa.dthr_andamento)";    
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
		
        $total = 0;    
        $return = array(); 
        $valor =  array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $cor[] = $this->random_color();
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= 'Janeiro';
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= '#ffcc00';
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function totalAberto(){
        
        $sql    =   "  SELECT sum(op.valor*op.qtd) as valor, o.id from orcamento_produtos op, orcamentos o WHERE op.orcamento_id = o.id and o.status_orcamento_id in (1,5,6) GROUP by o.id ";    
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        $return['qtd'] = 0;       
        $return['total'] = 0;
        $qtd=1;
        foreach ($retorno as $dados) {
            $return['qtd']      =   $qtd++;
            $return['total']    =   $dados['valor'] + $return['total'];
        }               
        
        return $return;
    }

    public function produtosOrcados(){
        $sql    =   "   SELECT  count( op.produto_id ) as total, p.descricao, p.modelo, p.codigo
                        FROM    orcamento_produtos op, orcamentos o, produtos p
                        WHERE   op.orcamento_id = o.id and
                                    op.produto_id = p.id and 
                                    p.tipo_produto_id in (1,2,3)
                        group by    p.descricao
                        order by    total desc limit 5";

        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['total'][] = $dados['total'];
            $descricao['descricao'][] = "'".$dados['descricao']."'";
            $total = $total+$dados['total'];
        }              

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['total']);
        $return['valor'].=']';
        $return['descricao'] = '['; 
        $return['descricao'].= implode(',',$descricao['descricao']);
        $return['descricao'].=']';
        $return['dados']    = $retorno;
        $return['total']    = $total;
        return $return;
    }

    public function buscaOrcamentosPorStatus($status){
        
        switch ($status) {
            case 'aberto':
                $orcamento_id = 1;
                break;
            case 'fechado':
                $orcamento_id = 2;
                break;
            case 'perdido outros':
                $orcamento_id = 3;
                break;    
            case 'cancelado':
                $orcamento_id = 4;
                break;
            case 'orçamento entregue':
                $orcamento_id = 5;
                break;    
            case 'or%C3%A7amento%20entregue' :          
                $orcamento_id = 5;
                break;
            case 'em negociação':
                $orcamento_id = 6;
                break;
            case 'perdido para wayne':
                $orcamento_id = 10;
                break;
            case 'perdido para gilbarco':
                $orcamento_id = 11;
                break;

        }

        $sql = " SELECT sum(op.qtd*op.valor) as valor_total, o.id, date_format(o.emissao,'%d/%m/%Y %h:%i:%s') as emissao, concat(e.razao_social,'-',e.cnpj) as cliente, e.estado, concat( i.razao_social,'-',i.cnpj ) as indicador, group_concat(p.modelo) as produtos
                        FROM orcamentos o 
                        INNER JOIN orcamento_produtos op ON o.id=op.orcamento_id
                        INNER JOIN produtos p ON p.id = op.produto_id                        
                        INNER JOIN empresas e ON o.empresa_id  = e.id 
                        LEFT JOIN empresas i ON o.indicador_id  = i.id 
                    where o.status_orcamento_id=".$orcamento_id." group by o.id";
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        return $retorno;
    }

    public function buscaOrcamentosFechadosMes($status){
        
        $sql = " SELECT  distinct   sum(op.qtd*op.valor) as valor_total, o.id, date_format(oa.dthr_andamento,'%d/%m/%Y %h:%i:%s') as dthr_andamento, concat(e.razao_social,'-',e.cnpj) as cliente, e.estado, concat( i.razao_social,'-',i.cnpj ) as indicador, o.emissao, group_concat(p.modelo) as produtos
                    FROM    orcamentos o  
                    INNER JOIN orcamento_produtos op ON o.id=op.orcamento_id
                    INNER JOIN produtos p ON p.id = op.produto_id
                    INNER JOIN (SELECT distinct date(dthr_andamento) as dthr_andamento, status_orcamento_id, orcamento_id FROM orcamento_andamentos) oa ON o.id = oa.orcamento_id
                    INNER JOIN empresas e ON o.empresa_id  = e.id 
                    LEFT JOIN empresas i ON o.indicador_id  = i.id
                    WHERE  oa.dthr_andamento between '".date('Y')."-".$this->mes($status)."-01' and '".date('Y')."-".$this->mes($status)."-".date('t')."' and 
                            o.status_orcamento_id = 2 and
                            oa.status_orcamento_id = 2 group by o.id ";
                                
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        return $retorno;
    }

    public function buscaOrcamentosInatividade(){

        $sql = "SELECT  DATEDIFF(CURDATE(),max(oa.dthr_andamento)) as dias_ultima_atu,oa.orcamento_id, concat(e.razao_social,' - ', e.cnpj) as cliente 
                FROM    orcamento_andamentos oa, orcamentos o, empresas e 
                WHERE   oa.orcamento_id = o.id and e.id = o.empresa_id 
                GROUP by oa.orcamento_id 
                ORDER BY dias_ultima_atu DESC LIMIT 10";
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        return $retorno;        
    }

    public function buscaOrcamentosPorStatusPeriodo($status, $periodo){
        $data_subtraida = date('Y-m-d', strtotime('-'.$periodo.' days'));        
        switch ($status) {
            case "'aberto'":
                $status_orcamento_id = 1;
                break;
            case "'fechado'":
                $status_orcamento_id = 2;
                break;
            case "'perdido'":
                $status_orcamento_id = 3;
                break;    
            case "'cancelado'":
                $status_orcamento_id = 4;
                break;
            case "'orçamento entregue'":
                $status_orcamento_id = 5;
                break;          
            case "'em negociação'":
                $status_orcamento_id = 6;
                break;           
        }

        $sql = " SELECT sum(op.qtd*op.valor) as valor_total, o.id, date_format(o.emissao,'%d/%m/%Y %h:%i:%s') as emissao 
                    FROM orcamentos o, orcamento_produtos op where o.id=op.orcamento_id and o.status_orcamento_id=".$status_orcamento_id." and DATE(o.emissao) between '".$data_subtraida."' and '".date('Y-m-d')."' group by o.id";
        
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        return $retorno;
    }

    public function atualizaIndicador($dados)
    {
        
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('orcamentos', $dados)){

            return true;
        }else{
            return false;
        }

    }
    /* ************ Comissao Representante ******************/
    public function atualizaComissao($dados)
    {
        $sql        =   "SELECT COALESCE(count(*),0) as total FROM orcamento_descontos WHERE orcamento_id=".$dados['orcamento_id'];
        $query      =   $this->db->query($sql);
        $resultado  =   $query->row_array();

        if( $resultado['total'] == 0 ){
            
            $inserir = array(
                'orcamento_id'      =>  $dados['orcamento_id'],
                'valor_desconto'    =>  $dados['valor_desconto']
            );

            if($this->db->insert('orcamento_descontos', $inserir)){
                return true;
            }else{
                return false;
            }
        }else{

            $update = array(
                'valor_desconto' => $dados['valor_desconto']
            );

            $this->db->where('orcamento_id', $dados['orcamento_id']);
            
            if($this->db->update('orcamento_descontos', $update)){

                return true;
            }else{
                return false;
            }

        }

    }
    
    public function alterarProdutoOrcRep($dados)
    {
        $update = array(
            'produto_id'    =>  $dados['produto_id'],
            'valor' =>  $dados['valor']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_produtos', $update)){
            return true;
        }else{
            return false;
        }
    }  

    public function buscaIndicador($id){

        $sql = "SELECT concat(u.nome, ' | ', e.razao_social ,' | ', e.cnpj) as indicador, e.telefone, u.email, u.id  FROM usuarios u, empresas e, orcamentos o WHERE u.empresa_id = e.id and o.indicador_id = u.id and o.id=".$id;
        
        $query = $this->db->query($sql);
        $retorno = $query->result();
        
        return $retorno;
    }

    public function retornaOrcamentos($term){

        $sql = "SELECT o.id, concat('#',o.id, ' | ', e.razao_social ,' | ', e.cnpj) as label FROM empresas e, orcamentos o WHERE o.empresa_id = e.id and concat(o.id,e.razao_social, e.cnpj) like '%".$term."%'";
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;

    }

    public function retornaOrcamentosFechados($term){

        $sql = "SELECT o.id, concat('#',o.id, ' | ', e.razao_social ,' | ', e.cnpj) as label, o.frete_id, o.observacao FROM empresas e, orcamentos o WHERE o.empresa_id = e.id and concat(o.id,e.razao_social, e.cnpj) like '%".$term."%' and o.status_orcamento_id in (2)";
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;

    }

    public function retornaOrcamentosFechadosRepresentantes($term, $empresa_id){

        $sql = "SELECT  o.id, concat('#',o.id, ' | ', e.razao_social ,' | ', e.cnpj) as label, o.frete_id, o.observacao
                FROM    empresas e, orcamentos o, empresas sol, usuarios u
                WHERE   o.empresa_id = e.id         and
                        u.id    = o.solicitante_id  and
                        sol.id  = u.empresa_id      and 
                        concat(o.id,e.razao_social, e.cnpj) like '%%%%%' and
                        o.status_orcamento_id in (2) and
                        o.id not in (select orcamento_id from pedidos) and
                        sol.id = ".$empresa_id;
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;

    }

    public function retornaProdutosOrcamentos($id){

        $sql = "SELECT op.*, p.descricao, p.tipo_produto_id, p.modelo, p.codigo, o.contato_posto,o.empresa_id FROM orcamentos o, orcamento_produtos op, produtos p WHERE o.id = op.orcamento_id and p.id = op.produto_id and o.id=".$id." order by p.tipo_produto_id asc";
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;

    }

    public function selectContatoPosto($id){

        $sql = "SELECT  DISTINCT u.id 
                FROM    orcamentos o, usuarios u 
                WHERE   u.nome  =   o.contato_posto and
                        o.id    =   ".$id;

        $query = $this->db->query($sql);

        $retorno = $query->row_array();

        return $retorno;
    }

    private function mes($mes){
        switch ($mes) {
            case 1:
                return 'Janeiro';
                break;
            case 2:
                return 'Fevereiro';
                break;    
            case 3:
                return 'Março';
                break;
            case 4:
                return 'Abril';
                break;    
            case 5:
                return 'Maio';
                break;    
            case 6:
                return 'Junho';
                break;       
            case 7:
                return 'Julho';
                break;         
            case 8:
                return 'Agosto';
                break;    
            case 9:
                return 'Setembro';
                break;        
            case 10:
                return 'Outubro';
                break;        
            case 11:
                return 'Novembro';
                break;        
            case 12:
                return 'Dezembro';
            case 'Janeiro':
                return '01';
            case 'Fevereiro':
                return '02';
            case 'Março':
                return '03';
            case 'Abril':
                return '04';
            case 'Maio':
                return '05';   
            case 'Junho':
                return '06';     
            case 'Julho':
                return '07';
            case 'Agosto':
                return '08';
            case 'Setembro':
                return '09';     
            case 'Outubro':
                return '10';
            case 'Novembro':
                return '11';
            case 'Dezembro':
                return '12';     
                break;                    
        }
    }

    private function random_color() {
        $letters = '0123456789ABCDEF';
        $color = '#';
        for($i = 0; $i < 6; $i++) {
            $index = rand(0,15);
            $color .= $letters[$index];
        }
        return $color;
    }

    public function excluirOrcamento($id){
        
        $this->db->where('orcamento_id', $id);
        if(    $this->db->delete('orcamentos_emissao') ){
            $this->db->where('id', $id);
            if(    $this->db->delete('orcamentos') ){
                return true;
            }else{
                return false;
            }
        }else{
           return false;
        }
    }

    public function getRelatorioOrcamentos($filtros){
        $where="1=1";
        
        if( $filtros['status_id'] != '' ){
            $where.=" and o.status_orcamento_id = ".$filtros['status_id'];
            
        }

        if( $filtros['dt_ini'] != '' && $filtros['dt_fim'] != ''){
            $where.=" and date(o.emissao) between '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."'";
            
        }

        if( $filtros['regiao'] != '' ){
            $where.=" and clientes.estado in ".$filtros['regiao'];
            
        }

        if( $filtros['estado'] != '' ){
            $where.=" and clientes.estado = '".$filtros['estado']."'";
        }

        if( $filtros['usuario_id'] != '' ){
            $where.=" and   clientes.estado in (select estado from zona_atuacao where usuario_id = ".$filtros['usuario_id'].")";
        }

        $sql = "SELECT  op.orcamento_id, sum((op.valor*op.qtd)) as valor_total, 
                        group_concat(concat(p.codigo,' - ',p.modelo,' ' )) as bombas, concat(clientes.cnpj,' - ',clientes.razao_social) as cliente,
                        o.origem, so.descricao as status, o.emissao, concat(emi.cnpj,' - ',emi.razao_social) as emp_emissor, u.nome as solicitante, sum(if(p.tipo_produto_id in (1,2,3), op.qtd,0)) as qtd_bombas  
                FROM orcamentos o, orcamento_produtos op, produtos p, empresas clientes, status_orcamentos so, empresas emi, usuarios u
                WHERE   u.id    =   o.solicitante_id 
                and     emi.id  =   u.empresa_id  
                and     o.id    =   op.orcamento_id 
                and     p.id    =   op.produto_id
                and     clientes.id     =   o.empresa_id
                and     o.status_orcamento_id = so.id
                and     ".$where."
                GROUP BY op.orcamento_id                             
                order by 1 desc";
        
        $query = $this->db->query($sql);
        $retorno['orcamento'] = $query->result_array();    

        return $retorno;
    }

    public function buscaOrcamentosFeira()
    {
        
        $sql =  "SELECT e.*, o.emissao,o.validade, s.descricao as status,o.id as orcamento_id,
                        o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, usu_gera.nome as criado_por,o.contato_posto
                    FROM        orcamentos o 
                    INNER JOIN  empresas e                      ON  o.empresa_id = e.id 
                    INNER JOIN  status_orcamentos s             ON  s.id = o.status_orcamento_id 
                    LEFT JOIN   orcamento_responsavel as orr    ON  orr.orcamento_id = o.id 
                    LEFT JOIN   usuarios usu                    ON  usu.id = orr.usuario_id 
                    INNER JOIN  usuarios usu_gera               ON  o.solicitante_id = usu_gera.id
                    INNER JOIN  empresas emp_gera               ON  usu_gera.empresa_id = emp_gera.id
                    WHERE   o.origem = 'Administrador Feira'
                    ORDER BY  o.id desc";
        echo $sql;die;            
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function buscaOrcamentosAprovacao(){
        $sql =  "SELECT o.id, concat(e.cnpj,'|',e.razao_social) as cliente, sum( op.valor*op.qtd ) as total
                    FROM        orcamentos o 
                    INNER JOIN  empresas e                      ON  o.empresa_id = e.id 
                    INNER JOIN  orcamento_produtos op           ON  op.orcamento_id = o.id
                    WHERE o.status_orcamento_id = 12
                    group by o.id 
                    ORDER BY o.id desc";

        $query = $this->db->query($sql);
        return $query->result_array();   
    }

    public function retornaFormaPagto($orcamento_id) {

        $sql = "SELECT f.* FROM forma_pagto f, orcamentos o WHERE f.id = o.forma_pagto_id and o.id = ".$orcamento_id;
        $query = $this->db->query($sql);
        $retorno = $query->row_array();
        return $retorno;

    }

    public function retornaDiferencaEmissao($orcamento_id) {

        $sql    =   "   SELECT  DATEDIFF(date(now()),date(max(o.dthr_emissao))) as dias, date_format(date(max(o.dthr_emissao)),'%d/%m/%Y') as dthr_emissao 
                        FROM    orcamentos_emissao o
                        WHERE   o.orcamento_id = ".$orcamento_id;

        $query  =   $this->db->query($sql);

        return  $query->row_array();

    }

    public function insereMotivoReemissao($data){

        if($this->db->insert('orcamentos_emissao', $data) ) {
            return true;
        }else{
            return false;
        }

    }

    public function orcamentoExpirados(){

        $sql    =   "   SELECT  oe.orcamento_id 
                        FROM    orcamentos o 
                        INNER JOIN (    SELECT  max(date(dthr_emissao)) as dthr_emissao, id, orcamento_id 
                                        FROM    orcamentos_emissao GROUP BY orcamento_id   ) oe ON o.id = oe.orcamento_id 
                        WHERE   o.status_orcamento_id not in (2,3,4,9,10,11,13) and DATEDIFF(date(now()),date(oe.dthr_emissao)) > 60";
        
        $query = $this->db->query($sql);
        
        return $query->result_array();

    }

    public function orcamentosAExpirar(){

        /*$sql = "SELECT  o.id, e.cnpj,e.razao_social, u.nome as responsavel, (60 - DATEDIFF(date(now()),date(o.emissao))) as dias, tc.descricao 
                FROM    orcamentos o, orcamento_responsavel orr, usuarios u, tipo_cadastros tc, empresas e 
                WHERE   e.id                = o.empresa_id  AND 
                        orr.orcamento_id    = o.id          AND
                        orr.usuario_id      = u.id          AND 
                        tc.id = u.tipo_cadastro_id          AND 
                        DATEDIFF(date(now()),date(o.emissao)) > 45 AND 
                        o.status_orcamento_id not in (2,3,4,9,10,11,13)";*/

         $sql    =   "  SELECT  o.id, e.cnpj,e.razao_social, u.nome as responsavel, (60 - DATEDIFF(date(now()),date(oe.dthr_emissao))) as dias, tc.descricao 
                        FROM    orcamentos o
                        INNER JOIN (    SELECT  max(date(dthr_emissao)) as dthr_emissao, id, orcamento_id 
                                        FROM    orcamentos_emissao GROUP BY orcamento_id   ) oe ON o.id = oe.orcamento_id 
                        INNER JOIN empresas e ON    e.id  = o.empresa_id
                        INNER JOIN orcamento_responsavel orr ON orr.orcamento_id    = o.id
                        INNER JOIN usuarios u ON    orr.usuario_id  = u.id
                        INNER JOIN tipo_cadastros tc ON tc.id = u.tipo_cadastro_id
                        WHERE   o.status_orcamento_id not in (2,3,4,9,10,11,13)     and 
                                DATEDIFF(date(now()),date(oe.dthr_emissao)) > 45    and 
                                DATEDIFF(date(now()),date(oe.dthr_emissao)) < 60  ORDER BY dias ASC";
        
        $query = $this->db->query($sql);
        
        return $query->result_array();

    }

    public function buscaOrcamentoById($orcamento_id){

        $sql = "SELECT concat(e.cnpj,' - ',e.razao_social) as cliente FROM orcamentos o, empresas e where o.empresa_id = e.id and o.id = ".$orcamento_id;

        $query = $this->db->query($sql);
        
        return $query->row_array();

    }

    public function primeiraEmissao($orcamento_id){
        $sql = "SELECT MIN(id) as id, date_format(dthr_emissao,'%d/%m/%Y %H:%i:%s') as dthr_emissao  FROM orcamentos_emissao WHERE orcamento_id = ".$orcamento_id;
        
        $query = $this->db->query($sql);

        return $query->row_array();
    }

    public function insereAlerta($data)
    {
        if($this->db->insert('orcamento_alertas', $data))
        {

            return $this->db->insert_id();
        }else{

            return false;
        }
    }
    
}
?>