<?php

class FretesModel extends CI_Model {

	public function add ($data) {
        
	    return $this->db->insert('fretes', $data);		

	}

    public function selectFretes(){

        $sql =  "SELECT * FROM fretes";
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function getFretesById($id){

        $sql =  "SELECT * FROM fretes where id =".$id;
        $query = $this->db->query($sql);
        return $query->row();
    }   

    public function excluirFrete($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('fretes')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaFrete($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('fretes', $dados)){
            return true;
        }else{
            return false;
        }

    }

}
?>