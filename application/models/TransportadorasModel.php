<?php

class transportadorasModel extends CI_Model {

	public function insere ($data) {
        
		return $this->db->insert('transportadoras', $data);
	}

    public function getTipotransportadoras()
    {

    	$sql =  "SELECT * FROM transportadora_tipo";
    	$query = $this->db->query($sql);
    	return $query->result_array();

    }       

    public function excluir($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('transportadoras')){
            return true;
        }else{
            return false;
        }
    }

    public function getTransportadoras()
    {

    	$sql =  "SELECT d.*,td.descricao as tipo FROM transportadoras d, transportadora_tipo td where td.id = d.tipo_id order by td.descricao DESC";
    	$query = $this->db->query($sql);
    	return $query->result_array();

    }

    public function getTransportadorasById($id)
    {

    	$sql =  "SELECT d.*,td.descricao as tipo FROM transportadoras d, transportadora_tipo td where td.id = d.tipo_id and d.id=".$id;
    	$query = $this->db->query($sql);
    	return $query->row_array();

    } 

    public function atualiza($dados)
    {
        $this->db->where('id', $dados['id']);
        if($this->db->update('transportadoras', $dados)){
            return true;
        }else{
            return false;
        }

    }

}
?>