<?php

class ConfiguracaoIdentificadorModel extends CI_Model {
	
	public function insert($identificador)
	{
        $this->db->where('modelo', $identificador['modelo']);
		$idf = $this->db->get('configuracao_identificador')->row_array();
		if(count($idf) > 0){
			$retorno = 0;
		} else {
			$this->db->insert('configuracao_identificador', $identificador);
			$retorno = $this->db->insert_id();
		}
		return $retorno;
    }
	
	public function Select()
    {
		return $this->db->get('configuracao_identificador')->result_array();
    }
	
}
?>