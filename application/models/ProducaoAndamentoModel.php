<?php
class ProducaoAndamentoModel extends CI_Model {

	public function add($data) {
        return $this->db->insert('producao_andamento', $data);		
    }

    
    public function excluir($id){
        
        $this->db->where('id', $id);
        
        if($this->db->delete('producao_andamento')){
            return true;
        }else{
            return false;
        }

    }

    public function update($dados)
    {

        $this->db->where('id', $dados['id']);

        if($this->db->update('producao_andamento', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function buscaNrSeriePorPedido($pedido_id)
    {
        $sql = "SELECT pedido_id, ordem_producao_id, modelo, codigo, descricao, combustivel, qtd_lacres, dt_afericao,observacoes, dt_inicio_garantia,
                       dt_fim_garantia, fl_estoque, fl_estoque_disp, dthr_geracao, concat(indicador_rtm,' ',id) as id   
                FROM bombas_nr_serie WHERE pedido_id=".$pedido_id."  ORDER BY id DESC";
        return $this->db->query($sql)->result_array();
    }

    public function buscaStatusProducao()
    {
        $sql = "SELECT * FROM status_producao ORDER BY id ASC";
        return $this->db->query($sql)->result_array();
    }

    public function buscaStatusPedidoProducao($pedido_id)
    {
        $sql = "SELECT  pa.id, pa.pedido_id, pa.nr_serie_id, pa.status_producao_id, date_format(pa.dthr_andamento,'%d/%m/%Y %H:%i:%s') as dthr_andamento, pa.descricao, sp.descricao as status 
                FROM    producao_andamento pa
                INNER JOIN (select id, nr_serie_id, max(dthr_andamento) as dthr_andamento from producao_andamento GROUP by nr_serie_id) as op on op.dthr_andamento = pa.dthr_andamento  
                INNER JOIN status_producao sp ON sp.id = pa.status_producao_id 
                WHERE pa.pedido_id = ".$pedido_id."
                GROUP BY nr_serie_id, pedido_id";

        return $this->db->query($sql)->result_array();
    }

}

?>