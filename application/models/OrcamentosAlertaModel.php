<?php

class OrcamentosAlertaModel extends CI_Model {

	public function selectAlertas($usuario_id){
		$sql =  "SELECT al.id as alerta_id, oa.*, al.fl_visualizado, date_format(date(al.dthr_alerta),'%d/%m/%Y') as dthr_alerta_f, al.dthr_alerta, al.usuario_id FROM orcamento_alertas al, orcamento_andamentos oa, orcamentos o 
				WHERE 	al.orcamento_andamento_id = oa.id and  
						oa.orcamento_id = o.id and 
						al.fl_visualizado = 0 and 						
						al.usuario_id = ".$usuario_id." order by dthr_alerta asc";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function selectNovosAlertas($usuario_id){
		
		$sql =  "SELECT count(*) as total_novos FROM orcamento_alertas al, orcamento_andamentos oa, orcamentos o 
				WHERE 	al.orcamento_andamento_id = oa.id and  
						oa.orcamento_id = o.id and 												
						al.usuario_id = ".$usuario_id." and fl_visualizado = 0";
		$query = $this->db->query($sql);
		return $query->row_array();
	
	}

	public function selectTodosAlertas(){
		$sql =  "SELECT al.id as alerta_id, oa.*, al.fl_visualizado, date_format(date(al.dthr_alerta),'%d/%m/%Y') as dthr_alerta_f, al.dthr_alerta, al.usuario_id, u.nome 
				FROM orcamento_alertas al, orcamento_andamentos oa, orcamentos o, usuarios u 
				WHERE 	u.id = al.usuario_id and 
						al.orcamento_andamento_id = oa.id and  
						oa.orcamento_id = o.id  						
				 order by dthr_alerta asc";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function selectTodosNovosAlertas($usuario_id){
		
		$sql =  "SELECT count(*) as total_novos FROM orcamento_alertas al, orcamento_andamentos oa, orcamentos o 
				WHERE 	al.orcamento_andamento_id = oa.id and  
						oa.orcamento_id = o.id ";
		
		$query = $this->db->query($sql);		
		return $query->row_array();
	
	}

	public function atualizar($update){

		$this->db->where('id', $update['id']);

		if($this->db->update('orcamento_alertas', $update)){
            return true;
        }else{
            return false;
        }

	}

	public function selectAlertaDiario($usuario_id){
		$sql =  "SELECT al.id as alerta_id, oa.*, al.fl_visualizado, date_format(date(al.dthr_alerta),'%d/%m/%Y') as dthr_alerta, al.usuario_id FROM orcamento_alertas al, orcamento_andamentos oa, orcamentos o 
				WHERE 	al.orcamento_andamento_id = oa.id and  
						oa.orcamento_id = o.id and 
						al.fl_visualizado = 0 and 
						al.dthr_alerta = date(NOW()) and
						al.usuario_id = ".$usuario_id;

		$query = $this->db->query($sql);
		return $query->result_array();
	}

}

