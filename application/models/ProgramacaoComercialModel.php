<?php

class ProgramacaoComercialModel extends CI_Model {

	public function select(){

    	$sql =  "SELECT id, date_format(dt_abertura_prod,'%d/%m/%Y') as dt_abertura_prod, date_format(dt_semana_atual,'%d/%m/%Y') as dt_semana_atual, nr_bicos_semana, nr_bombas_semana, total_bicos_atual, total_bombas_atual, dt_abertura_prod as data_abertura, dt_semana_atual as data_semana_atual
    			 FROM programacao_producao_regras";
    	$query = $this->db->query($sql);
        return $query->row_array();
	   
    }

    public function buscaOrcamentos()
    {
       $sql =  "SELECT  o.id, e.razao_social 
                FROM    orcamentos o
                INNER JOIN empresas e on o.empresa_id = e.id
                WHERE   o.status_orcamento_id not in (13,4,10,11,9,8,3,14,1) and 
                        o.id not in (select orcamento_id from pedidos) and 
                        o.id not in (select orcamento_id from programacao_comercial ) 
                ORDER BY `o`.`status_orcamento_id`  DESC";
        return $this->db->query($sql)->result_array();
    }

    public function buscaOrcamentosSemana($semana)
    {
        $sql = "SELECT  pc.orcamento_id, pc.nr_bicos, pc.nr_bombas, e.razao_social 
                FROM    programacao_comercial pc 
                inner join orcamentos o ON o.id = pc.orcamento_id
                inner join empresas e ON e.id = o.empresa_id     
                WHERE semana =".$semana;  
                
        return $this->db->query($sql)->result_array();
    }

	public function atualiza($dados)
    {
        $this->db->where('id', $dados['id']);
        if($this->db->update('programacao_producao_regras', $dados)){
            return true;
        }else{
    		return false;
        }

    }

    public function inserirPedido ($data) {
        
        return $this->db->insert('programacao_semanal', $data);     

    }

    public function selectDatasPedidos(){

        $sql    =   "   SELECT  ps.dt_semana_prog, sum(ps.nr_bicos) as bicos, sum(ps.nr_bombas) as bombas
                        FROM    programacao_semanal ps                           
                        GROUP BY ps.dt_semana_prog 
                        ORDER BY ps.dt_semana_prog ASC";

        $query  =   $this->db->query($sql);
        $datas  =   $query->result_array();
        $dataPedidos = array();

        foreach($datas as $data){
                            
            $sql        =   "SELECT     ps.pedido_id, e.razao_social , ps.nr_bicos as bicos, ps.nr_bombas as bombas, ps.id, p.status_pedido_id, s.descricao as status, s.ordem
                            FROM        programacao_semanal ps, pedidos p, orcamentos o, empresas e, status_pedidos s
                            WHERE       p.id                =   ps.pedido_id    and
                                        p.orcamento_id      =   o.id            and
                                        o.empresa_id        =   e.id            and
                                        p.status_pedido_id  =   s.id            and
                                        ps.dt_semana_prog   =   '".$data['dt_semana_prog']."' 
                            order by s.ordem ASC";

            $key                =   $data['dt_semana_prog'].'*'.$data['bicos'].'*'.$data['bombas'];
            $query1             =   $this->db->query($sql);
            $pedidos            =   $query1->result_array();
            $dataPedidos[$key]  =   $pedidos;

        }       
        return $dataPedidos;
    }

    public function atualizaProgramacao($dados)
    {
        $this->db->where('id', $dados['id']);
        if($this->db->update('programacao_semanal', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function reorganizaSemana(){
        $sql = "SELECT  p.dt_semana_prog, sum(p.nr_bicos) as nr_bicos, sum(p.nr_bombas) as nr_bombas 
                FROM    programacao_semanal p 
                WHERE   p.dt_semana_prog = (select pp.dt_semana_atual from programacao_producao_regras pp where id = 1 )";

        $query  =   $this->db->query($sql);
        $datas  =   $query->row_array();

        $updateRegra = array(   'id'                    =>  1,
                                'total_bicos_atual'     =>  $datas['nr_bicos'],
                                'total_bombas_atual'    =>  $datas['nr_bombas']     );

        if($this->atualiza($updateRegra)) {
            return true;
        }else{
            return false;
        }
    }

    public function buscaInfoPedido($pedido_id){
        
        $sql="  SELECT  concat(e.cnpj,'-',e.razao_social) as cliente, concat(e.cidade,'/',e.estado) as cidade, p.modelo, sum(if(p.tipo_produto_id in (1,2,3,6),pi.qtd,0)) qtd, p.codigo, s.descricao as status 
                FROM    pedidos pe, orcamentos o, empresas e, pedido_itens pi, produtos p, status_pedidos s
                WHERE   pe.id           =   pi.pedido_id    and
                        o.id            =   pe.orcamento_id and
                        o.empresa_id    =   e.id    and
                        pi.produto_id   =   p.id    and
                        pe.status_pedido_id = s.id and
                        pe.id           =   ".$pedido_id." and 
                        p.tipo_produto_id in (1,2,3,6)
                GROUP by pe.id, pi.produto_id";
        
        $query  =   $this->db->query($sql);
        
        return  $query->result_array();

    } 

    public function verificaData($dt_alteracao){
        $sql = "    SELECT  COUNT(*) as total 
                    FROM    programacao_semanal 
                    WHERE   dt_semana_prog='".$dt_alteracao."'";

        $query  =   $this->db->query($sql);

        return  $query->row_array();                

    }

    public function retornaBicosBombasSemana($dt_semana_prog){
        $sql    =   "SELECT  ps.dt_semana_prog, sum(ps.nr_bicos) as bicos, sum(ps.nr_bombas) as bombas
                        FROM    programacao_semanal ps
                        WHERE   ps.dt_semana_prog = '".$dt_semana_prog."'
                        GROUP BY ps.dt_semana_prog  ";

        $query = $this->db->query($sql);
        return $query->row_array();

    }

    public function excluirPedido($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('programacao_semanal')){
            return true;
        }else{
            return false;
        }
    }

    public function buscaModelosPedido($pedido_id)
    {
        $sql="  SELECT  distinct pr.modelo, pi.qtd, pr.nr_bicos
                FROM    pedidos p, pedido_itens pi, produtos pr 
                WHERE   p.id = pi.pedido_id     AND 
                        pi.produto_id = pr.id   AND 
                        pr.tipo_produto_id in (1,2,3,6) AND 
                        p.id =".$pedido_id;
        return $this->db->query($sql)->result_array();
    }
    
}

?>