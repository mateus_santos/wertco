<?php

class ChamadoCausaModel extends CI_Model {
	
	public function select($defeito_id)
    {
		$this->db->select('cc.*');
		$this->db->select('COUNT(cacs.id) total');
		$this->db->where('defeito_id', $defeito_id);
		$this->db->join('chamado_atividade_causa_solucao cacs', 'cc.id = cacs.causa_id', 'left');
		$this->db->order_by('total', 'DESC');
		$this->db->group_by('cc.id');
        return $this->db->get('chamado_causa cc')->result_array();
    }
	
	public function insert($causa)
	{
        $this->db->insert('chamado_causa', $causa);
		return $this->db->insert_id();
    }
	
	public function findByDescricao($defeito_id, $descricao)
	{
		$this->db->where('descricao', $descricao);
		$this->db->where('defeito_id', $defeito_id);
        return $this->db->get('chamado_causa')->row_array();
	}

}
?>