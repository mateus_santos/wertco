<?php

class JsonModel extends CI_Model
{

    public function getSimultaneo($familia, $tipo, $ramo, $numBicos, $latfront)
    {

        $sql =  "SELECT abast_simultaneos FROM jsons where familia='$familia' AND tipo='$tipo' 
        AND ramo = '$ramo' AND qnt_bicos= '$numBicos' AND latfront= '$latfront' GROUP BY abast_simultaneos";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getVazao($familia, $tipo, $ramo, $numBicos, $latfront, $simultaneo, $hidraulico)
    {

        $sql =  "SELECT vazao FROM jsons where familia='$familia' AND tipo='$tipo' AND ramo = '$ramo'
         AND qnt_bicos= '$numBicos' AND latfront= '$latfront' AND abast_simultaneos='$simultaneo' AND conjunto_hidraulico = '$hidraulico' GROUP BY vazao";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getHidraulico($familia, $tipo, $ramo, $numBicos, $latfront, $simultaneo)
    {

        $sql =  "SELECT conjunto_hidraulico FROM jsons where familia='$familia' AND tipo='$tipo' AND ramo = '$ramo' 
        AND qnt_bicos= '$numBicos' AND latfront= '$latfront' AND abast_simultaneos='$simultaneo'  GROUP BY conjunto_hidraulico";

        $query = $this->db->query($sql);
        return $query->result();
    }



    public function getModelo($familia, $tipo, $ramo, $numBicos, $latfront, $simultaneo, $vazao, $hidraulico)
    {

        $sql =  "SELECT modelo FROM jsons where familia='$familia' AND tipo='$tipo' AND ramo = '$ramo' 
        AND qnt_bicos= '$numBicos' AND latfront= '$latfront' AND abast_simultaneos='$simultaneo' AND vazao = '$vazao' AND conjunto_hidraulico = '$hidraulico'";

        $query = $this->db->query($sql);
        return $query->result();
    }
}
