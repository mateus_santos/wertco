<?php

class SolicitacaoPecasItensModel extends CI_Model {
	
	public function retornaPecas($term){
        $sql = "SELECT  *
                FROM    solicitacao_pecas
                WHERE   descricao like '%".$term."%'";

        $query  =   $this->db->query($sql);
        return  $query->result_array();                                   
    }

    public function insert($solicitacao_pecas_itens)
	{
		$this->db->insert('solicitacao_pecas_itens', $solicitacao_pecas_itens);
        return $this->db->insert_id();
    }

    public function excluirItem($id){
        $this->db->where('id', $id);
        if( $this->db->delete('solicitacao_pecas_itens') ){
            return true;
        }else{
            return false;
        }
    }

    public function excluirSubItem($id, $dados){
        $this->db->where('id', $dados['id']);

        if($this->db->update('solicitacao_pecas_itens', $dados)){

            $this->db->where('id', $id);
            if( $this->db->delete('solicitacao_pecas_itens_nr') ){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function atualizarCusto($dados){

        $this->db->where('id', $dados['id']);

        if($this->db->update('solicitacao_pecas_itens', $dados)){
            return true;

        }else{
            return false;
        }
    }

}

?>