<?php

class ZonaAtuacaoModel extends CI_Model {
	
	public function adicionar($data){
		
		$this->db->insert('zona_atuacao', $data);
		return $this->db->insert_id();
	}

	public function buscaEstadosDisponiveis(){
        $sql = "	SELECT e.* FROM estados e 
        			WHERE e.uf not in (select estado from zona_atuacao)";
        
        $query  =   $this->db->query($sql);       

        return $query->result_array();
    }

    public function select(){
    	$sql = "	SELECT 	z.id, z.usuario_id, u.nome, group_concat(z.estado) as estados 
    				FROM 	zona_atuacao z, estados e, usuarios u 
    				WHERE 	z.usuario_id = u.id and 
    						z.estado = e.uf 
    				GROUP BY z.usuario_id ";

    	$query  =   $this->db->query($sql);       

        return $query->result_array();
    }

    public function buscaEstadosUsuarios($usuario_id){
        $sql = "	SELECT 	z.estado, e.nome 	FROM zona_atuacao z, estados e
        			WHERE 	z.estado = e.uf 	and  
        					z.usuario_id = ".$usuario_id;
        
        $query  =   $this->db->query($sql);       

        return $query->result_array();
    }

    public function delete($usuario_id){ 
        
        $this->db->where('usuario_id', $usuario_id);
        if(    $this->db->delete('zona_atuacao') ){
            return true;
        }else{
            return false;
        }
    }

    public function buscarEmailResp($estado){
    	$sql = "	SELECT u.email, u.nome FROM orcamentos o, empresas e, zona_atuacao z, usuarios u
    				WHERE 	o.empresa_id = e.id and 
    						z.estado = e.estado and 
    						z.usuario_id = u.id and 
    						z.estado = '".$estado."'";
    	$query  =   $this->db->query($sql);       

        return $query->row_array();

    }

    public function buscaConsultorWertco(){
        
        $sql = "SELECT  DISTINCT z.usuario_id, u.nome 
                FROM    usuarios u, zona_atuacao z 
                WHERE   u.id = z.usuario_id ";
        
        $query  =   $this->db->query($sql);  
        
        return $query->result_array();

    }

    public function getRelatorioRepresentantes($filtros){
        
        $where = "1=1";
               
        if( $filtros['usuario_id'] != '' ){
            $where.= " and z.usuario_id =".$filtros['usuario_id'];
        }

        if( $filtros['dt_fim'] != '' ){
            $where.= " and o.emissao BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."'";
        }

        $sql = "SELECT u.id, upper(u.nome) AS representante, concat(e.cnpj,'-',upper(e.razao_social)) AS empresa, e.estado, count(o.id) AS total, sum(CASE WHEN o.status_orcamento_id = 2 THEN 1 ELSE 0 END) AS fechados, upper(con.nome) AS zona
                FROM    usuarios u
                INNER JOIN  empresas e ON u.empresa_id = e.id
                LEFT  JOIN  (select orr.usuario_id, o.id, o.status_orcamento_id, o.emissao FROM orcamentos o, orcamento_responsavel orr WHERE o.id = orr.orcamento_id) o ON o.usuario_id = u.id
                INNER JOIN  zona_atuacao z  ON  z.estado = e.estado
                INNER JOIN  usuarios con    ON  z.usuario_id = con.id
                WHERE       u.tipo_cadastro_id = 2 AND u.ativo = 1 and ".$where."
                GROUP BY    u.id
                ORDER BY    zona ASC, empresa ASC ";
        
        $query = $this->db->query($sql);
        $retorno['orcamento'] = $query->result_array();
        
        return $retorno;
    
    }

    public function buscaConsultorEstado($estado)
    {
        $sql = "SELECT  DISTINCT z.usuario_id, upper(u.nome) as nome 
                FROM    usuarios u, zona_atuacao z 
                WHERE   u.id = z.usuario_id 
                and     z.estado = '".$estado."' ";
        
        $query  =   $this->db->query($sql);  
        
        return $query->result_array();
    }

    public function buscaEmailResponsavel($usuario_id)
    {
        $sql = "SELECT email, nome FROM usuarios WHERE id =".$usuario_id;
        
        return $this->db->query($sql)->row_array();
    }
}

?>