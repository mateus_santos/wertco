<?php

class ChamadoModel extends CI_Model {
	
	public function select($abertos)
    {
        $where = " cs.id not in (3,4)";   
		if($abertos == 2){
            $where = " cs.id in (3,4)";   
        }

        $sql = " SELECT c.*, concat(e.id,' | ',e.cnpj,' | ',e.razao_social) as cliente, cs.descricao as status,ct.descricao as tipo
                  FROM chamado c   
		          INNER JOIN empresas e ON e.id = c.cliente_id
		          INNER JOIN chamado_status cs ON cs.id = c.status_id
		          INNER JOIN chamado_tipo ct ON ct.id = c.tipo_id 
                  Where ".$where;         
                 
        return $this->db->query($sql)->result_array();
    }

    public function selectFinanceiro($abertos)
    {
        $where = " cs.id not in (3,4)";   
        
        if($abertos == 2){
            $where = " cs.id in (3,4)";   
        }

        $sql = "SELECT c.*, concat(e.id,' | ',e.cnpj,' | ',e.razao_social) as cliente, cs.descricao as status, ct.descricao as tipo, group_concat(tecnico.razao_social) as tecnicos
                FROM chamado c
                INNER JOIN  empresas e on  e.id = c.cliente_id
                inner join  chamado_status cs on cs.id = c.status_id
                inner join  chamado_tipo ct on ct.id = c.tipo_id
                LEFT JOIN chamado_atividade ca      ON  ca.chamado_id = c.id 
                LEFT JOIN chamado_subatividade csu   ON  csu.atividade_id = ca.id 
                LEFT JOIN empresas tecnico          ON  tecnico.id = csu.parceiro_id
                WHERE ".$where."
                GROUP BY c.id ";

        return $this->db->query($sql)->result_array();
    }

    public function selectPorEmpresa($empresa_id)
    {
        $this->db->select('c.*');
        $this->db->select("concat(e.id,' | ',e.cnpj,' | ',e.razao_social) as cliente");
        $this->db->select('cs.descricao as status');
        $this->db->select('ct.descricao as tipo');
        $this->db->join('empresas e', 'e.id = c.cliente_id');
        $this->db->join('chamado_status cs', 'cs.id = c.status_id');
        $this->db->join('chamado_tipo ct', 'ct.id = c.tipo_id');
        $this->db->where('c.cliente_id', $empresa_id);
        return $this->db->get('chamado c')->result_array();
    }

    public function selectPorUsuario($usuario_id)
    {
        $this->db->select('c.*');
        $this->db->select("concat(e.id,' | ',e.cnpj,' | ',e.razao_social) as cliente");
        $this->db->select('cs.descricao as status');
        $this->db->select('ct.descricao as tipo');
        $this->db->join('empresas e', 'e.id = c.cliente_id');
        $this->db->join('chamado_status cs', 'cs.id = c.status_id');
        $this->db->join('chamado_tipo ct', 'ct.id = c.tipo_id');
        $this->db->where('c.usuario_id', $usuario_id);
        return $this->db->get('chamado c')->result_array();
    }
	
	public function insert($chamado)
	{
        $this->db->insert('chamado', $chamado);
		return $this->db->insert_id();
    }
	
	public function find($id)
    {
		$this->db->select('e.razao_social cliente');
        $this->db->select('e.cnpj');
		$this->db->select('u.nome contato');
        $this->db->select('uw.nome usuario');
		$this->db->select('c.*');
		$this->db->join('empresas e', 'e.id = c.cliente_id');
		$this->db->join('usuarios u', 'u.id = c.contato_id');
        $this->db->join('usuarios uw', 'uw.id = c.usuario_id');
        $this->db->where('c.id', $id);
		return $this->db->get('chamado c')->row_array();
    }
	
	public function update($chamado)
    {
		$this->db->where('id', $chamado['id']);
        $this->db->update('chamado', $chamado);        
        return $this->db->affected_rows();
	}

	public function selectTipos(){
		return $this->db->get('chamado_tipo')->result_array();
	}

    public function buscaTiposById($tipo_id){
        $this->db->where('id', $tipo_id);    
        return $this->db->get('chamado_tipo')->row_array();
    }
	
	public function selectStatus(){
        $this->db->order_by("ordem", "asc");
		return $this->db->get('chamado_status')->result_array();
	}

    public function buscaStatusById($status_id){
        $this->db->where("id", $status_id);
        return $this->db->get('chamado_status')->row_array();
    }
	
	public function selectPrioridades(){
		return $this->db->get('chamado_prioridade')->result_array();
	}

	public function insertAnexo($insert){
		
		return $this->db->insert('chamado_upload', $insert);
	}

	public function excluirAnexo($chamado_id){
		$this->db->where('id', $chamado_id);
        if(	$this->db->delete('chamado_upload') ){
            return true;
        }else{
            return false;
        }
	}

	public function listaAnexos($chamado_id){

		$this->db->select('*');
		$this->db->where('chamado_id', $chamado_id);		
		return $this->db->get('chamado_upload')->result_array();
	}

	
	public function listaTecnicosAtendimentos($chamado_id){	
		$this->db->select('e.razao_social empresa');
		$this->db->select('e.id empresa_id');
		$this->db->select('u.nome tecnico');
		$this->db->select('c.*');
		$this->db->join('chamado_atividade ca', 'ca.chamado_id = c.id');
		$this->db->join('chamado_subatividade cs', 'cs.atividade_id = ca.id');		
		$this->db->join('empresas e', 'e.id = cs.parceiro_id');
		$this->db->join('usuarios u', 'u.id = cs.contato_id');		
        $this->db->where('c.id', $chamado_id);
		return $this->db->get('chamado c')->result_array();
	}

	public function selectTotalStatus(){
         
         $sql = "SELECT count(c.status_id) as valor, s.descricao
                    FROM chamado_status s, chamado c
                    WHERE s.id = c.status_id 
                    GROUP BY c.status_id order by s.id asc";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".$dados['descricao']."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['valor']);
        $return['valor'].=']';
        $return['descricao'] = '['; 
        $return['descricao'].= implode(',',$descricao['descricao']);
        $return['descricao'].=']';
        $return['cor'] = '[';
        $return['cor'].= implode(',',$cor);
        $return['cor'].=']';
        $return['dados'] 	= $retorno;
        $return['total']    = $total;
        $return['cores']    = $cores;
        return $return;
    }

    public function selectTotalTipo(){
         
         $sql = "SELECT count(c.tipo_id) as valor, s.descricao
                    FROM chamado_tipo s, chamado c
                    WHERE s.id = c.tipo_id 
                    GROUP BY c.tipo_id order by s.id asc";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".$dados['descricao']."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['valor']);
        $return['valor'].=']';
        $return['descricao'] = '['; 
        $return['descricao'].= implode(',',$descricao['descricao']);
        $return['descricao'].=']';
        $return['cor'] = '[';
        $return['cor'].= implode(',',$cor);
        $return['cor'].=']';
        $return['dados'] 	= $retorno;
        $return['total']    = $total;
        $return['cores']    = $cores;
        return $return;
    }

    public function selectTotalDefeitos(){
         
         $sql = "SELECT     COUNT(c.id) as valor, cd.descricao 
                    FROM    chamado_atividade ca, chamado c, chamado_defeito cd 
                    WHERE   ca.chamado_id = c.id and 
                            ca.defeito_id = cd.id and 
                            ca.defeito_id <> 1 and 
                            cd.descricao != 'Informação'
                    GROUP BY    cd.descricao 
                    order by valor DESC
                    limit 10 ";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".str_replace("'"," ",$dados['descricao'])."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['valor']);
        $return['valor'].=']';
        $return['descricao'] = '['; 
        $return['descricao'].= implode(',',$descricao['descricao']);
        $return['descricao'].=']';
        $return['cor'] = '[';
        $return['cor'].= implode(',',$cor);
        $return['cor'].=']';
        $return['dados']    = $retorno;
        $return['total']    = $total;
        $return['cores']    = $cores;
        return $return;
    }

     public function totalChamadoMes(){

        $sql="  SELECT  count(c.id) as total, extract(MONTH FROM c.inicio) AS mes
                FROM    chamado c
                WHERE    extract(YEAR FROM c.inicio) = '".date('Y')."'
                GROUP BY extract(MONTH FROM c.inicio)";
        
        $query = $this->db->query($sql);        
        $retorno = $query->result_array();        
        $total = 0;       
        
        if(count($retorno) == 0){
            $return['valor'] = '[0]';
            $return['mes'] = "['Janeiro']";             
            $return['cor'] = "['#ffcc00']";
            $return['dados']    = array('total'     =>  '0',
                                            'mes'   =>  'Janeiro');
            $return['total']    = 0;

        }else{

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $cor[] = $this->random_color();
            }            

            $return['valor'] = '['; 
            $return['valor'].= implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
        }
        
        return $return;
    }

     public function totalChamadoEstado(){

        $sql="SELECT  count(*) as valor, emp.estado as descricao
                FROM    chamado c, empresas emp
                WHERE emp.id = c.cliente_id and  extract(YEAR FROM c.inicio) = '".date('Y')."'
                GROUP BY emp.estado";
        
        $query = $this->db->query($sql);        
        $retorno = $query->result_array();        
        $total = 0;       
        
        if(count($retorno) == 0){
            $return['valor'] = '[0]';
            $return['mes'] = "['Janeiro']";             
            $return['cor'] = "['#ffcc00']";
            $return['cor'] = "['#ffcc00']";
            $return['cores'] = "['#ffcc00']";
            $return['dados']    = array('total'     =>  '0',
                                            'mes'   =>  'Janeiro');
            $return['total']    = 0;

        }else{

            foreach ($retorno as $dados) {
                $valor['valor'][] = $dados['valor'];
                $descricao['descricao'][] = "'".$dados['descricao']."'";
                $total = $total+$dados['valor'];
                $cor1 = "'".$this->random_color()."'";
                $cores1 = str_replace("'", "", $cor1);
                $cor[] = $cor1;
                $cores[] = $cores1;
            }

            $return['valor'] = '['; 
            $return['valor'].= implode(',',$valor['valor']);
            $return['valor'].=']';
            $return['descricao'] = '['; 
            $return['descricao'].= implode(',',$descricao['descricao']);
            $return['descricao'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            $return['cores']    = $cores;
        }
        
        return $return;

    }

    public function ultimosChamadosRealizados(){
         
        $sql    = "SELECT   distinct c.*, emp.razao_social, emp.cnpj, cs.descricao as status, ct.descricao as tipo 
                    FROM    chamado_atividade ca, chamado c, empresas emp, chamado_status cs, chamado_tipo ct 
                    WHERE   ca.chamado_id = c.id    and 
                            emp.id = c.cliente_id   and 
                            c.status_id = cs.id     and 
                            ct.id = c.tipo_id 
                    ORDER BY c.id DESC Limit 3"; 

        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }

    public function buscaChamadosAtividadesById($id){

        $sql    = "SELECT   distinct c.id,c.inicio, c.fim, concat(emp.cnpj,' | ',emp.razao_social) as cliente, 
                            emp.cidade, emp.estado, emp.cep, cs.descricao as status, ct.descricao as tipo, cd.descricao as defeito,
                            cas.descricao as status_atividade, ca.numero_serie, p.modelo, ca.observacao, ca.usuario_id, u.nome, cto.nome as contato,
                            ca.inicio as inicio_atividade, ca.fim as fim_atividade, c.descricao, ca.id as atividade_id
                    FROM    chamado c 
                    LEFT JOIN   chamado_atividade ca            ON  ca.chamado_id = c.id
                    INNER JOIN  empresas emp                    ON  emp.id = c.cliente_id
                    INNER JOIN  chamado_status cs               ON  c.status_id = cs.id
                    INNER JOIN  chamado_tipo ct                 ON  ct.id = c.tipo_id
                    LEFT JOIN   chamado_defeito cd              ON  ca.defeito_id = cd.id
                    LEFT JOIN   chamado_atividade_status cas    ON  cas.id = ca.status_id
                    LEFT JOIN   produtos p                      ON  p.id = ca.modelo_id
                    LEFT JOIN   usuarios u                      ON   u.id = ca.usuario_id
                    INNER JOIN  usuarios cto                    ON  c.contato_id = cto.id                     
                    WHERE   c.id = ".$id." ORDER BY `c`.`id` ASC"; 

        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }
	
	public function buscaChamadosAtividadesAtendimentosById($id){

        $sql    = "SELECT 	cs.*, concat(u.nome,' - ',concat(e.cnpj,'-',e.razao_social)) as tecnico 
					FROM 	chamado_subatividade cs, chamado_atividade ca, chamado c, usuarios u, empresas e 
					WHERE 	ca.chamado_id = c.id 	and 
							cs.atividade_id = ca.id and 
							cs.parceiro_id = e.id 	and 
							cs.contato_id = u.id 	and 
							c.id = ".$id; 

        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }

    public function buscaEnderecoEntregaPecas($chamado_id){
        $sql = "SELECT DISTINCT c.cliente_id as cliente_id, e.razao_social FROM chamado c, empresas e 
                WHERE   c.cliente_id = e.id and c.id = ".$chamado_id."
                UNION 
                SELECT DISTINCT cs.parceiro_id as cliente_id, e.razao_social FROM chamado c, chamado_atividade ca, chamado_subatividade cs, empresas e 
                WHERE c.id = ca.chamado_id and ca.id = cs.atividade_id and cs.parceiro_id = e.id and c.id = ".$chamado_id;
                
        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }

    public function buscaOrdensDePagamento($chamado_id){

        $sql = "SELECT  op.*, opi.qtd,opi.valor, ops.descricao as status, d.descricao as despesa, emp.razao_social 
                FROM    ordem_pagamento op, ordem_pagamento_itens opi, ordem_pagamento_status ops, despesas d, empresas emp 
                WHERE   op.id = opi.ordem_pagamento_id and 
                        op.status_id = ops.id and 
                        opi.despesa_id = d.id and 
                        emp.id = op.favorecido_id and 
                        op.chamado_id =".$chamado_id;

        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }

    public function buscaTecnicosChamados($chamado_id){

        $sql = "    SELECT  DISTINCT emp.razao_social, emp.cnpj, c.id as chamado 
                    FROM    chamado_subatividade cs, chamado_atividade ca, chamado c, empresas emp
                    WHERE   ca.chamado_id = c.id        and
                            cs.atividade_id = ca.id     and 
                            cs.parceiro_id = emp.id     and 
                            c.id = ". $chamado_id;

        $query  =   $this->db->query($sql);
        return  $query->result_array();                
    }

    public function buscaTodosChamadosPdf(){
        $sql = "SELECT  c.id, cs.descricao as status, ct.descricao as tipo, cont.nome as contato, concat(emp.razao_social,' | ', emp.cnpj) as cliente, c.inicio, c.fim, u.nome as usuario 
                FROM    chamado c, chamado_status cs, chamado_tipo ct, usuarios cont, empresas emp , usuarios u
                WHERE   c.cliente_id = emp.id   and
                        c.contato_id = cont.id  and
                        ct.id = c.tipo_id       and
                        c.usuario_id = u.id     and
                        cs.id = c.status_id     order by 1";

        $query  =   $this->db->query($sql);
        return  $query->result_array();                                   
    }

    public function retornaPecas($term){
        $sql = "SELECT  *
                FROM    pecas
                WHERE   descricao like '%".$term."%'";

        $query  =   $this->db->query($sql);
        return  $query->result_array();                                   
    }

    public function buscaAnexos($chamado_id){
        
        $sql    =   "   SELECT  ca.arquivo
                        FROM    chamado c, chamado_atividade a, chamado_upload ca
                        WHERE   c.id = a.chamado_id and ca.chamado_id = c.id and c.id = ".$chamado_id."  
                        UNION 
                        SELECT  cau.arquivo
                        FROM    chamado c, chamado_atividade a, chamado_atividade_upload cau
                        WHERE   c.id = a.chamado_id and a.id = cau.atividade_id and c.id = ".$chamado_id;
        
        $query  =   $this->db->query($sql);

        return  $query->result_array();
    }

     public function buscaClienteChamado($id){
         
        $sql    = "SELECT   e.* 
                    FROM    chamado c, empresas e 
                    WHERE   c.cliente_id = e.id and 
                            c.id = ".$id; 
                                                  
        $query  =   $this->db->query($sql);
        return  $query->row_array();
    }

    public function buscaAutorizacaoServico($id){
         
        $sql    = "SELECT   e.*
                    FROM    chamado c, empresas e
                    WHERE   c.cliente_id = e.id and
                            c.id = ".$id;

        $query  =   $this->db->query($sql);
        return  $query->row_array();
    }

    public function chamados(){

        $sql = "SELECT  c.id, cs.descricao as status, ct.descricao as tipo, cont.nome as contato,
                        concat(emp.razao_social,' | ', emp.cnpj) as cliente, c.inicio, c.fim, u.nome as usuario
                FROM    chamado c, chamado_status cs, chamado_tipo ct, usuarios cont, empresas emp , usuarios u
                WHERE   c.cliente_id = emp.id   and
                        c.contato_id = cont.id  and
                        ct.id = c.tipo_id       and
                        c.usuario_id = u.id     and
                        cs.id = c.status_id     order by 1";

        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }


    public function relatorioChamados($params){

        $where = '1=1';
        
        if($params['status_id'] != ''){
            $where.= ' and c.status_id = '.$params['status_id'];
        }
   
        if($params['cliente_id'] != ''){
            $where.= ' and c.cliente_id = '.$params['cliente_id'];
        }
   
        if($params['tecnico_id'] != ''){
            $where.= ' and cs.parceiro_id = '.$params['tecnico_id'];
        }

        $sql = 'SELECT c.id , concat(cliente.id," | ",cliente.cnpj," | ",cliente.razao_social) as clientes, csts.descricao as status, group_concat(tecnico.razao_social) as tecnicos, c.inicio, c.status_id 
                FROM chamado c 
                INNER JOIN empresas cliente         ON  cliente.id = c.cliente_id
                INNER JOIN chamado_status csts      ON  c.status_id = csts.id
                LEFT JOIN chamado_atividade ca      ON  ca.chamado_id = c.id 
                LEFT JOIN chamado_subatividade cs   ON  cs.atividade_id = ca.id 
                LEFT JOIN empresas tecnico          ON  tecnico.id = cs.parceiro_id 
                WHERE '.$where.'
                GROUP BY c.id';

        
        $query  =   $this->db->query($sql);
        return  $query->result_array();          
    }



    private function mes($mes){
        switch ($mes) {
            case 1:
                return 'Janeiro';
                break;
            case 2:
                return 'Fevereiro';
                break;    
            case 3:
                return 'Março';
                break;
            case 4:
                return 'Abril';
                break;    
            case 5:
                return 'Maio';
                break;    
            case 6:
                return 'Junho';
                break;       
            case 7:
                return 'Julho';
                break;         
            case 8:
                return 'Agosto';
                break;    
            case 9:
                return 'Setembro';
                break;        
            case 10:
                return 'Outubro';
                break;        
            case 11:
                return 'Novembro';
                break;        
            case 12:
                return 'Dezembro';
            case 'Janeiro':
                return 01;
            case 'Fevereiro':
                return 02;
            case 'Março':
                return 03;
            case 'Abril':
                return 04;
            case 'Maio':
                return 05;   
            case 'Junho':
                return 06;     
            case 'Julho':
                return 07;
            case 'Agosto':
                return '08';
            case 'Setembro':
                return '09';     
            case 'Outubro':
                return '10';
            case 'Novembro':
                return '11';
            case 'Dezembro':
                return '12';     
                break;                    
        }
    }
    
    private function random_color() {
	    $letters = '0123456789ABCDEF';	    
	    $color = '#';	    
	    for($i = 0; $i < 6; $i++) {
	        $index = rand(0,15);
	        $color .= $letters[$index];
	    }
	    return $color;
    }

    public function buscaChamadosInativos($usuario_id){
        $sql = 'SELECT  DISTINCT ca.chamado_id, DATEDIFF( date(NOW()), date(ca.inicio) ) as diff, u.nome, concat(e.cnpj,"|",e.razao_social) as razao_social, cs.descricao as status 
                    FROM    chamado c,(SELECT cs.atividade_id as id, max(cs.inicio) as inicio, ca.chamado_id FROM chamado_atividade ca, chamado_subatividade cs where cs.atividade_id = ca.id GROUP by ca.chamado_id ) ca, usuarios u, empresas e,  chamado_status cs  
                    WHERE   cs.id = c.status_id         and 
                            c.cliente_id = e.id         and 
                            u.id = c.usuario_id         and 
                            c.id = ca.chamado_id        and 
                            c.status_id not in (3,4)    and                             
                            DATEDIFF( date(NOW()), date(ca.inicio) ) > 15 
                            and u.id='.$usuario_id.' 
                    GROUP BY ca.chamado_id order by 2 DESC';
        
        $query  =   $this->db->query($sql);
        return  $query->result_array();
    }

    public function buscaTodosChamadosInativos(){

        $sql = 'SELECT  DISTINCT ca.chamado_id, DATEDIFF( date(NOW()), date(ca.inicio) ) as diff, u.nome,  cs.descricao as status, cp.descricao as prioridade, cp.id
                    FROM  chamado c, (SELECT cs.atividade_id as id, max(cs.inicio) as inicio, ca.chamado_id FROM chamado_atividade ca, chamado_subatividade cs where cs.atividade_id = ca.id GROUP by ca.chamado_id ) ca, usuarios u, chamado_status cs, chamado_prioridade cp  
                    WHERE   cs.id = c.status_id         and 
                            u.id = c.usuario_id         and 
                            c.id = ca.chamado_id        and 
                            c.status_id not in (3,4)    and
                            cp.id = c.prioridade_id     
                    GROUP BY ca.chamado_id 
                    order by cp.id DESC, diff DESC';
        
        $query  =   $this->db->query($sql);
        
        return  $query->result_array();

    }

    public function buscaTotalChamadosAbertos()
    {
        $sql = "SELECT count(*) as total FROM chamado WHERE status_id not in (3,4) ";
        return $this->db->query($sql)->row_array();
    }

    public function buscaTotalChamadosFechados()
    {
        $sql = "SELECT count(*) as total FROM chamado WHERE status_id = 3 ";
        return $this->db->query($sql)->row_array();
    }

    public function relatorioQualidadeChamados($filtros)
    {
        //$whereFim = ($filtros['id_status'] == 3 || $filtros['id_status'] == 4 ) ? " AND fim BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."'" : " AND 1=1";
        
        $sql="SELECT (select count(id) from chamado where status_id in (3,4) and fim BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."' ) as fechados,
                    (select count(id) from chamado where status_id not in (3,4) and inicio <= '".$filtros['dt_fim']."' and ".$filtros['status_id']." and ".$filtros['tipo_id'].") as abertos,
                    (select count(id) from chamado where inicio BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."' and ".$filtros['status_id']." and ".$filtros['tipo_id'].") as aberto_periodo
                FROM dual";
       
        
        return $this->db->query($sql)->result_array();

    }

    public function relatorioMediaChamado($filtros)
    {
        

        $sql="SELECT (select count(id) from chamado where status_id in (3,4) AND inicio BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."' AND fim BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."') as fechados,

                    (select count(id) from chamado where inicio BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."'  and ".$filtros['tipo_id'].") as abertos,

                    (select count(id) from chamado WHERE inicio BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."' and ".$filtros['tipo_id'].") as aberto_periodo,

                    (SELECT sum(TIMESTAMPDIFF(hour, c.inicio, c.fim) / 24) as diferenca FROM chamado c where c.status_id in (3,4) and c.inicio BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."' AND c.fim BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."') as diferenca                    
                FROM dual";
        /*(SELECT count(id) FROM chamado WHERE inicio BETWEEN '2018-01-01' and '".date('Y-m-d')."') as abertos_geral,
                    (SELECT count(id) FROM chamado WHERE fim is not null OR ( status_id in (3,4,12) ) ) as fechados_geral*/        
        return $this->db->query($sql)->result_array();                 
    }

    public function buscaTodasOrdensPagto($status)
    {
        $sql = "SELECT op.id, c.id as chamado_id, concat(upper(e.razao_social),'-',e.cnpj) as cliente, 
                concat(upper(t.razao_social),'-', t.cnpj) as favorecido, concat('R$',format(op.valor_op,2,'DE_de')) as valor,
                upper(s.descricao) as status, op.dt_emissao, op.descricao, op.comprovante, op.nota_fiscal, op.nr_nf    
                FROM ordem_pagamento op 
                INNER JOIN chamado as c ON c.id = op.chamado_id 
                INNER JOIN empresas e on e.id = c.cliente_id
                INNER JOIN empresas t on t.id = op.favorecido_id  
                INNER JOIN ordem_pagamento_status s on s.id = op.status_id
                WHERE s.id in (".$status.")
                ORDER BY op.id  DESC";
        
        return $this->db->query($sql)->result_array();
    }

    public function buscaChamadosCliente($where)
    {
        $sql = "SELECT concat(e.cnpj,'-',e.razao_social) as cliente, e.cidade, e.estado, e.cep, c.id as chamado, date_format(c.inicio,'%d/%m/%Y %H:%i:%s') as inicio,date_format(c.fim,'%d/%m/%Y %H:%i:%s') as fim, ct.descricao as tipo, cs.descricao as status, p.modelo, ca.numero_serie, ca.inicio as inicio_atividade, ca.fim as fim_atividade, cd.descricao as defeito, cc.descricao as causa, sol.descricao as solucao, chs.observacao, upper(u.nome) as tecnico
                FROM chamado c
                INNER JOIN empresas e on e.id = c.cliente_id
                INNER JOIN chamado_status cs on cs.id = c.status_id
                INNER JOIN chamado_tipo ct on ct.id = c.tipo_id
                LEFT JOIN chamado_atividade ca on ca.chamado_id = c.id
                LEFT JOIN produtos p on p.id = ca.modelo_id
                LEFT JOIN chamado_subatividade chs on chs.atividade_id = ca.id
                LEFT JOIN chamado_defeito cd on cd.id = ca.defeito_id
                LEFT JOIN chamado_causa cc on cc.defeito_id = cd.id
                LEFT JOIN chamado_solucao sol on sol.causa_id = cc.id 
                LEFT JOIN usuarios u on u.id = chs.contato_id
                WHERE c.cliente_id = ".$where['cliente_id']."
                GROUP BY c.id, ca.id, chs.id";
                
        return $this->db->query($sql)->result_array();   
    }

    public function buscaChamado($term)
    {
        $sql = "SELECT c.id, concat('#', c.id, ' - Cliente: ', e.cnpj, ' - ', e.razao_social) as label, concat('#', c.id, ' - Cliente: ', e.cnpj, ' - ', e.razao_social) as value FROM chamado c 
                INNER JOIN empresas e ON e.id = c.cliente_id 
                WHERE concat(c.id,e.razao_social,e.cnpj) like '%".$term."%'";
        
        return $this->db->query($sql)->result_array();   
    }

    public function totalChamadosAbertos($where)
    {
        $sql = "SELECT coalesce(count(*),0) as total, coalesce(sum(if(c.tipo_id = 1,1,0)),0) as startup, coalesce(sum(if(c.tipo_id = 2,1,0)),0) as manutencao, coalesce(sum(if(c.tipo_id = 3,1,0)),0) as informacao 
                FROM chamado c
                WHERE c.tipo_id in(1,2) AND c.inicio BETWEEN '".$where['dt_ini']."' and '".$where['dt_fim']."'";

        return $this->db->query($sql)->row_array();   

    }

    public function buscaChamadosDcs($where)
    {
        $sql="SELECT coalesce(count(cd.id),0) as total, cd.descricao as defeito, cc.descricao as causa, cs.descricao as solucao  
            FROM chamado c
            INNER JOIN chamado_atividade ca on ca.chamado_id = c.id
            INNER JOIN chamado_defeito cd on cd.id = ca.defeito_id
            INNER JOIN chamado_atividade_causa_solucao cacs on cacs.atividade_id = ca.id
            INNER JOIN chamado_causa cc on cc.id = cacs.causa_id
            INNER JOIN chamado_solucao cs on cs.id = cacs.solucao_id
            WHERE c.tipo_id in (1,2) AND cd.id not in (17) AND c.inicio BETWEEN '".$where['dt_ini']."' and '".$where['dt_fim']."' and c.tipo_id = 2
            GROUP BY cd.id
            ORDER BY 1 DESC";

        return $this->db->query($sql)->result_array();  
    }

    public function buscaTotalChamadosDcs($where)
    {
        $sql="SELECT coalesce(count(*),0) as total_geral 
            FROM chamado c
            INNER JOIN chamado_atividade ca on ca.chamado_id = c.id
            INNER JOIN chamado_defeito cd on cd.id = ca.defeito_id
            INNER JOIN chamado_atividade_causa_solucao cacs on cacs.atividade_id = ca.id
            INNER JOIN chamado_causa cc on cc.id = cacs.causa_id
            INNER JOIN chamado_solucao cs on cs.id = cacs.solucao_id
            WHERE c.tipo_id in(1,2) AND cd.id not in (17) AND c.inicio BETWEEN '".$where['dt_ini']."' and '".$where['dt_fim']."' and c.tipo_id = 2            
            ORDER BY 1 DESC";

        return $this->db->query($sql)->row_array();  
    }

    public function buscaStatusAtual($chamado_id)
    {
        $sql = "SELECT c.status_id FROM chamado c WHERE c.id=".$chamado_id;
        return $this->db->query($sql)->row_array();
    }

    public function buscaStatusChamadoId($status_id)
    {
        $sql = "SELECT * FROM chamado_status WHERE id=".$status_id;
        return $this->db->query($sql)->row_array();
    }

    public function insereAndamentoChamado($dados)
    {
        $this->db->insert('chamado_andamentos', $dados);
        return $this->db->insert_id();
    }

    public function buscaSolicitacaoAtendimento($chamado_id)
    {
        $sql = "SELECT coalesce(count(*),0) as retorno FROM chamado_autoriz_servico WHERE chamado_id=".$chamado_id;
        return $this->db->query($sql)->row_array();
    }

    public function updateAutorizServico($dados)
    {   
        $sql = "SELECT id FROM chamado_autoriz_servico WHERE chamado_id = ".$dados['chamado_id']." order by id ASC LIMIT 1";
        $retorno = $this->db->query($sql)->row_array();
        $autoriz = array('dthr_aceite' => $dados['dthr_aceite']);
        $this->db->where('id', $retorno['id']);        
        $this->db->update('chamado_autoriz_servico', $autoriz);        
        return $this->db->affected_rows();
        
    }

    public function buscaEmailParceirosAtraso()
    {
        $sql = "SELECT DISTINCT lower(par.email) as email, par.id as parceiro_id 
                FROM chamado c
                INNER JOIN chamado_status s on s.id = c.status_id
                INNER JOIN empresas e on e.id = c.cliente_id
                INNER JOIN chamado_atividade ca on ca.chamado_id = c.id
                INNER JOIN chamado_subatividade cs on cs.atividade_id = ca.id
                INNER JOIN empresas par on par.id = cs.parceiro_id 
                WHERE c.status_id in (1,7,9,12)";

        return $this->db->query($sql)->result_array();
    }

    public function listaChamadosAtrasadosParceiros($parceiro_id)
    {
        $sql = "SELECT distinct c.id as chamado_id, 
                        concat(e.cnpj,' - ',e.razao_social) as cliente, 
                        upper(e.cidade) as cidade, 
                        e.estado, 
                        s.descricao as status,
                        date_format(c.inicio,'%d/%m/%Y') as inicio
                 FROM chamado c
                inner join chamado_status s on s.id = c.status_id
                inner join empresas e on e.id = c.cliente_id
                inner join chamado_atividade ca on ca.chamado_id = c.id
                inner join chamado_subatividade cs on cs.atividade_id = ca.id
                inner join empresas par on par.id = cs.parceiro_id 
                where c.status_id in (1,7,9,12) and par.id = ".$parceiro_id." group by c.id order by c.id asc";

        return $this->db->query($sql)->result_array();   
    }


    
}
?>