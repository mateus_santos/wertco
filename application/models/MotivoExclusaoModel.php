<?php

class MotivoExclusaoModel extends CI_Model {

	public function add ($data) {
        
	    return $this->db->insert('motivo_exclusao', $data);		

	}

    public function selectMotivoExclusao(){

        $sql =  "SELECT * FROM motivo_exclusao";
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function getmotivo_exclusaoById($id){

        $sql =  "SELECT * FROM motivo_exclusao where id =".$id;
        $query = $this->db->query($sql);
        return $query->row();
    }   

    public function excluir($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('motivo_exclusao')){
            return true;
        }else{
            return false;
        }
    }

    public function atualiza($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('motivo_exclusao', $dados)){
            return true;
        }else{
            return false;
        }

    }

}
?>