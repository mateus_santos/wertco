<?php 

class LogModel extends CI_Model {

	public function insereLog($data){

        if($this->db->insert('logs', $data)){
            
            return true;
        }else{
            return false;
        }

	}

	public function insereLogCampos($data){
		if($this->db->insert('log_campos', $data)){

            return true;
        }else{
        	
            return false;
        }		
	}
}