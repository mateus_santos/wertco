<?php

class UsuariosModel extends CI_Model {

    public function add ($data) {

        $this->db->insert('usuarios', $data);
        return $this->db->insert_id();
    }

    public function select() {
        $this->db->order_by('nome');
        return $this->db->get('usuarios')->result();
    }

    public function findMarca($id){

        $sql =  "   SELECT u.*, concat(e.cnpj,' - ',e.razao_social) as empresa, t.descricao as tipo_cadastro, e.estado FROM usuarios u
                    INNER JOIN empresas e on u.empresa_id = e.id 
                    INNER JOIN tipo_cadastros t on t.id = u.tipo_cadastro_id  
                    WHERE u.id=".$id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getCpf($cpf)
     {

        $sql =  "select count(*) as total from usuarios WHERE cpf='".$cpf."'";        
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function getEmail($email)
    {

        $sql =  "select count(*) as total from usuarios WHERE email='".$email."'";        
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function autenticacao($email,$senha)
    {
        $sql = "SELECT  count(*) as total, u.nome, u.email,u.tipo_cadastro_id, tc.descricao as tipo_acesso, u.id, u.empresa_id, u.foto, e.telefone, u.celular, u.telefone as telefone_usuario, stc.id as subtipo_cadastro, stc.tipo_cadastro_pai, e.razao_social, u.setor_id
                FROM    usuarios u
                INNER JOIN  empresas e ON e.id = u.empresa_id
                INNER JOIN  tipo_cadastros tc ON tc.id = u.tipo_cadastro_id
                LEFT JOIN   subtipo_cadastros stc ON stc.tipo_cadastro_pai = tc.id and stc.id = u.subtipo_cadastro_id
                WHERE u.email = '".$email."' and u.senha = '".md5(md5($senha))."' and u.ativo = 1
                GROUP BY u.nome, u.email, u.tipo_cadastro_id, u.id";
        
        $query = $this->db->query($sql);

        return $query->result();
    } 

    /* Método para verificar se existe usuário  *
     * @params: $nome, primeiro nome do usuário *
     * @params: $email, email do usuário        *
     * @view:   esqueci-minha-senha.php         *
     * @controller: esqueci-minha-senha.php     */
    public function verificaUsuario($cpf,$email)
    {
        $sql = "SELECT count(*) as total, u.id, u.nome FROM usuarios u WHERE email = '".$email."' and cpf = '".$cpf."' group by u.id,u.nome";              
        $query = $this->db->query($sql);

        return $query->result();   
    }

    public function redefinirSenha($id,$senha)
    {
        $dados = array('senha' => md5(md5($senha)));
        
        $this->db->where('id', $id);
        $retorno = $this->db->update('usuarios', $dados);        
        return $retorno;
    }
   
    public function getUsuarios()
    {
        
        $sql    =   "SELECT     u.id,u.nome,u.cpf,u.email,e.razao_social,tp.descricao,u.dthr_cadastro,u.ativo, e.estado, uz.nome as zona, u.tipo_cadastro_id 
                        FROM    usuarios u
                        inner join empresas e on u.empresa_id = e.id
                        inner join  tipo_cadastros tp on u.tipo_cadastro_id = tp.id 
                        left join zona_atuacao z on z.estado = e.estado 
                        left join usuarios uz on  z.usuario_id = uz.id
                        WHERE  u.senha != ''";
        
        $query  =   $this->db->query($sql);
        
        return $query->result();

    }

    public function atualizaStatus($dados)
    {
        $update = array(
            'ativo' => $dados['ativo']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('usuarios', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function getUsuario($id){

        $sql =  "SELECT u.*, tag cartao, ultima_atualizacao, periodo_renovacao periodo, sc.descricao as tipo_funcionario, sc.obs 
                    FROM usuarios u 
                    LEFT JOIN cartao_tecnicos ct    ON  ct.usuario_id           =   u.id 
                    LEFT JOIN subtipo_cadastros sc  ON  u.subtipo_cadastro_id   =   sc.id
                    WHERE u.id=".$id;
        $query = $this->db->query($sql);
        foreach($query->result_array() as $retorno){}
        return $retorno; 
    }

    public function getTiposCadastro()
    {

        $sql        =   "SELECT * FROM tipo_cadastros ORDER BY descricao ASC";
        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();
        return $retorno;
    }

    public function atualizaUsuario($dados)
    {
        $this->db->where('id', $dados['id']);

        if($this->db->update('usuarios', $dados)){
            return true;
        }else{
            return false;
        }
    }

     public function atualizaCliente($dados)
    {
        $update = array('nome'              =>  $dados['nome'],
                        'cpf'               =>  $dados['cpf'],
                        'email'             =>  $dados['email'],
                        'telefone'          =>  $dados['telefone']);

        if(isset($dados['foto'])) {
            $update['foto'] = $dados['foto'];
        }

        $this->db->where('id', $dados['id']);

        if($this->db->update('usuarios', $update)){
            return true;
        }else{
            return false;
        }
    }

    /* Autocomplete - Orçamentos area administrativa */
    public function retornaRepresentante($term){

        $sql =  "SELECT     distinct e.razao_social as label , e.id, e.razao_social, e.cnpj, u.empresa_id 
                    FROM   usuarios u,empresas e WHERE  u.empresa_id = e.id and concat(u.nome,u.cpf,e.razao_social,e.cnpj,e.id,u.id) like '%".$term."%' and (u.tipo_cadastro_id = 2 ) and u.ativo = 1";
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno; 
    }

    /* Autocomplete Relatório de Orçamentos */
    public function retornaRepresentantesAtivosAutoComplete($term)
    {
    
        $sql = "SELECT     distinct concat(u.nome,'-',e.razao_social) as label, u.id, e.razao_social, e.cnpj, u.empresa_id, u.nome, concat(e.cnpj,'-',upper(e.razao_social)) as empresa
                    FROM    usuarios u, empresas e
                    WHERE   u.empresa_id = e.id     and 
                            u.tipo_cadastro_id in (2,12)  and 
                            u.ativo = 1             and 
                            concat(u.nome,u.cpf,e.razao_social,e.cnpj,e.id,u.id) like '%".$term."%' ";

        return $this->db->query($sql)->result_array();

    }

    /* Autocomplete - Orçamentos */
    public function retornaIndicadores($term){

        $sql =  "SELECT concat(u.nome,' | Empresa: ', e.razao_social) as label , u.id, e.razao_social, e.cnpj, u.empresa_id, nome FROM usuarios u,empresas e WHERE  u.empresa_id = e.id and concat(u.nome,u.cpf,e.razao_social,e.cnpj,e.id,u.id) like '%".$term."%' and (u.tipo_cadastro_id <> 1) and u.ativo = 1";
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno; 
    }

    public function retornaIndicadoresAtivos(){

        $sql =  "SELECT concat(u.nome,' | Empresa: ', e.razao_social) as label , u.id, e.razao_social, e.cnpj, u.empresa_id, u.nome 
                    FROM usuarios u,empresas e 
                    WHERE   u.empresa_id = e.id 
                    and     u.tipo_cadastro_id in (4,6,10) 
                    and     u.ativo = 1
                    order by u.nome ASC, e.razao_social";
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno; 
    }

    /* Autocomplete - Cadastro de cartões área administrativa */
    public function retornaTecnicos($term){

        $sql = "SELECT concat(u.id, ' | ', u.nome,' | Empresa: ', e.razao_social) as label , u.id, e.razao_social, e.cnpj, u.email, e.email as email_empresa FROM usuarios u,empresas e WHERE  u.empresa_id = e.id and concat(u.nome,u.cpf,e.razao_social,e.cnpj,e.id,u.id) like '%".$term."%' and (u.tipo_cadastro_id = 4 or u.tipo_cadastro_id = 3 or  u.tipo_cadastro_id = 7 ) and u.ativo = 1 and u.id not in (select usuario_id from cartao_tecnicos)";

        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno;
    }

    /* Autocomplete - Cadastro de cartões área administrativa */
    public function retornaTecnicosPedidos($term){

        $sql = "SELECT concat(u.id, ' | ', u.nome,' | Empresa: ', e.razao_social) as label , u.id, e.razao_social, e.cnpj,u.email, e.email as email_empresa FROM usuarios u,empresas e WHERE  u.empresa_id = e.id and concat(u.nome,u.cpf,e.razao_social,e.cnpj,e.id,u.id) like '%".$term."%' and u.tipo_cadastro_id in (4,3,7,17,9) and u.ativo = 1";

        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno;
    }

     public function retornaClientes($term){

        $sql = "SELECT  e.id, concat(e.cnpj,' - ', e.razao_social) as label , 
                        e.razao_social, e.cnpj, concat(e.endereco,'-',e.bairro,'-',e.cep) as endereco,
                        concat(e.cidade,'/',e.estado) as cidade, e.tipo_cadastro_id, e.fl_inadimplencia 
                FROM    empresas e 
                WHERE   concat(e.razao_social,e.cnpj,e.id) like '%".$term."%' and (e.tipo_cadastro_id = 1 or e.id in (select o.empresa_id from orcamentos o inner join pedidos p on p.orcamento_id = o.id ))";

        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno;
    }

    public function retornaTecnicoAssist($term){

        $sql = "SELECT  concat(u.id, ' | ', u.nome,' | Empresa: ', e.razao_social) as label , u.id, e.razao_social, e.cnpj, 
                        concat(e.endereco,'-',e.bairro,'-',e.cep) as endereco, concat(e.cidade,'/',e.estado) as cidade, c.ultima_atualizacao, c.periodo_renovacao
                FROM    usuarios u,empresas e, cartao_tecnicos c 
                WHERE   c.usuario_id = u.id 
                and     u.empresa_id = e.id 
                and     concat(u.nome,u.cpf,e.razao_social,e.cnpj,e.id,u.id) like '%".$term."%' 
                and     u.tipo_cadastro_id = 4 and u.ativo = 1 ";

        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno;
    }

    public function retornaUsuariosVinculados($empresa_id){

         $sql = "SELECT     u.id, concat(u.nome,' - ', u.cpf) as label 
                    FROM    usuarios u WHERE u.empresa_id=".$empresa_id;                    
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno;
    }
    
    public function getUsuariosEmpresa($empresa_id){
        $this->db->select('u.*');
        $this->db->select("DATE_FORMAT(DATE_ADD(ct.ultima_atualizacao, INTERVAL ct.periodo_renovacao MONTH), '%d/%m/%Y') validade");
        $this->db->select('sc.descricao as tp_acesso');
        $this->db->join('cartao_tecnicos ct',   'ct.usuario_id = u.id', 'left');
        $this->db->join('subtipo_cadastros sc',  'sc.id = u.subtipo_cadastro_id', 'left');
        $this->db->where('u.empresa_id', $empresa_id);
        $this->db->where('u.tipo_cadastro_id', 4);
        return $this->db->get('usuarios u')->result_array();
    }

    public function getTodosUsuariosEmpresa($empresa_id){
        $this->db->distinct();
        $this->db->select('u.*');        
        $this->db->select('COALESCE(cts.id,0) as solicitacao');
        $this->db->select("DATE_FORMAT(DATE_ADD(ct.ultima_atualizacao, INTERVAL ct.periodo_renovacao MONTH), '%d/%m/%Y') validade");
        $this->db->select('sc.descricao as tp_acesso');
        $this->db->join('cartao_tecnicos ct', 'ct.usuario_id = u.id', 'left');
        $this->db->join('cartao_tecnico_solicita cts', 'cts.solicitante_id = u.id', 'left');
        $this->db->join('tipo_cadastros tp', 'tp.id = u.tipo_cadastro_id');
        $this->db->join('subtipo_cadastros sc',  'sc.id = u.subtipo_cadastro_id and sc.tipo_cadastro_pai = tp.id', 'left');
        $this->db->where('u.empresa_id', $empresa_id);        
        return $this->db->get('usuarios u')->result_array();
    }

    public function excluirUsuario($id){ 
        
        $this->db->where('id', $id);
        if(    $this->db->delete('usuarios') ){
            return true;
        }else{
            return false;
        }
    }

     public function relatorioTecnicosRegiao($params){

        $where = ' u.tipo_cadastro_id = 4 and u.ativo = 1';
                   
        if($params['regiao'] != ''){
            $where.= ' and e.estado in '.$params['regiao'];
            
        }

        $sql = "SELECT u.*, concat(e.cnpj, ' - ',e.razao_social) as razao_social, e.estado 
                FROM usuarios u 
                INNER JOIN empresas e   ON  e.id = u.empresa_id                
                WHERE ".$where;

        
        $query  =   $this->db->query($sql);
        return  $query->result_array();          
    }

    public function retornaParceirosCartao($term){

        $sql    =   "   SELECT  CONCAT(u.nome,' - ',e.razao_social) as label, ct.* FROM usuarios u, cartao_tecnicos ct, empresas e 
                        WHERE   e.id = u.empresa_id 
                        AND     u.id = ct.usuario_id 
                        AND     CONCAT(e.razao_social,u.nome) like '%".$term."%'";
        $query  =   $this->db->query($sql);
        
        return $query->result_array();;
    }

    public function selectUsuariosComercialFallowUp(){
        $sql    =   "SELECT distinct   u.id, u.email 
                        FROM    usuarios u, orcamento_alertas oa 
                        WHERE   u.id = oa.usuario_id and 
                                u.empresa_id = 2 and 
                                oa.fl_visualizado = 0";

        $query  =   $this->db->query($sql);
        
        return $query->result_array();
    }

    public function buscaUsuariosComercialWertco(){
        $sql = "SELECT * FROM usuarios where tipo_cadastro_id = 5 and empresa_id = 2 and id not in (select usuario_id from zona_atuacao) and ativo = 1";
        
        $query  =   $this->db->query($sql);       

        return $query->result_array();
    }

    public function buscaUsuariosComercialWertcoTodos(){
        $sql = "SELECT * FROM usuarios where tipo_cadastro_id = 5 and empresa_id = 2 and ativo = 1";
        
        $query  =   $this->db->query($sql);       

        return $query->result_array();
    }

    public function retornaRepresentantesAtivos($usuario_id = NULL){

        $where = '1=1';
        
        if( $usuario_id != NULL ){

            $where = 'e.estado in (SELECT estado FROM zona_atuacao WHERE usuario_id = '.$usuario_id.')';
        }

        $sql =  "SELECT     distinct e.razao_social as label, u.id, e.razao_social, e.cnpj, u.empresa_id, u.nome, concat(e.cnpj,'-',upper(e.razao_social)) as empresa
                    FROM    usuarios u, empresas e
                    WHERE   u.empresa_id = e.id     and 
                            u.tipo_cadastro_id = 2  and 
                            u.ativo = 1             and     ".$where." ORDER BY u.nome ASC";

        $query = $this->db->query($sql);

        $retorno = $query->result_array();

        return $retorno; 

    }

    public function buscaSubtipoCadastro()
    {        
        $sql = " SELECT * FROM subtipo_cadastros";
        return $this->db->query($sql)->result_array();
    }


}
?>