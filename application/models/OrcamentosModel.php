<?php

class OrcamentosModel extends CI_Model {
	
 	public function select() {
        
        $sql =  "select p.*,tp.descricao as tipo_produto from produtos p, tipo_produtos tp WHERE p.tipo_produto_id = tp.id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function selectStatus() {
        
        $sql =  "SELECT * FROM status_orcamentos";
        $query = $this->db->query($sql);
        return $query->result_array();
    }    

    public function atualizaOrcamento($update)
    {
        $this->db->where('id', $update['id']);
        
        if($this->db->update('orcamentos', $update)){

            return true;
        }else{

            return false;
        }
    }

    public function getOrcamento($id) {
        
        $sql =  "SELECT * FROM orcamentos where id=".$id;
        $query = $this->db->query($sql);

        $row = $query->result();
        foreach($row as $retorno){}

        return $retorno;
    }

    public function insereOrcamento($data){

        if($this->db->insert('orcamentos', $data)){            
            return true;
        }else{
            return false;
        }

    }

    public function insereOrcamentoProdutos($data)
    {
        
        if($this->db->insert('orcamento_produtos', $data)){
            return true;
        }else{
            return false;
        }
    }

    public function listarProdutos($orcamento_id){
        $sql =  "   SELECT  p.*, op.qtd, op.valor 
                    FROM    orcamentos o, orcamento_produtos op, produtos p 
                    WHERE   op.orcamento_id = o.id  and 
                            p.id = op.produto_id    and     op.orcamento_id = ".$orcamento_id;
        $query = $this->db->query($sql);         
        return $query->result_array();

    }

    public function listarEmpresaOrcamento($orcamento_id)
    {
        $sql =  "SELECT e.*,o.emissao, o.valor_orcamento, u.nome, u.email as email_responsavel
                    FROM orcamentos o, empresas e, zona_atuacao z, usuarios u 
                    WHERE   o.empresa_id = e.id and 
                            z.estado = e.estado and
                            z.usuario_id = u.id and                              
                            o.id = ".$orcamento_id;
        $query = $this->db->query($sql);
        $row = $query->result_array();
        foreach($row as $retorno){}
        return $retorno;
    }
    
    public function buscaOrcamentos($dados) 
    {

            $aColumns = array('orcamento_id', 'solicitante', 'emissao', 'status','andamento', 'responsavel','estado', 'origem', 'acoes');            
            $sLimit = "";
            if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
                $sLimit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] );
                $inicial = intval( $_POST['start'] );
                $final = intval( $_POST['length'] );
            }

            $sOrder = "ORDER BY o.id desc";

            if ( isset( $_POST['order'] ) ) {
                $sOrder = " ORDER BY ";
                for ( $i=0 ; $i< sizeof( $_POST['order'] ) ; $i++ ) {
                    if ( $_POST['columns'][$i]['orderable'] == "true" ) {
                        $sOrder .= $aColumns[ intval( $_POST['order'][$i]['column'] ) ]." ".($_POST['order'][$i]['dir']==='asc' ? 'asc' : 'desc') .", ";
                    }
                }
                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" ) {
                    $sOrder = " ORDER BY {$sIndexColumn} DESC";
                }
            }
            $sWhere = '';
            if ( isset($_POST['search']) && $_POST['search']['value'] != "" ) {
                
                for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
                    if ( isset($_POST['columns'][$i]['searchable']) && $_POST['columns'][$i]['searchable'] == "true" ) {
                        switch ($aColumns[$i]) {
                            case 'orcamento_id':
                                $sWhere .= " AND (o.id LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'solicitante':
                                $sWhere .= " concat(e.cnpj,' ',e.razao_social,' ',e.id) LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'emissao':
                                $sWhere .= " o.emissao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'status':
                                $sWhere .= " s.descricao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'estado':
                                $sWhere .= " e.estado LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'responsavel':
                                $sWhere .= " usu.nome LIKE '%". $_POST['search']['value'] ."%' OR" ;
                                break;
                            case 'origem':
                                $sWhere .= " o.origem LIKE '%". $_POST['search']['value'] ."%')";
                                break;
                        }
                    }
                }
            }

            $where = '1=1';
            /*if($_POST['zona'] != '' ){
                if($_POST['representante'] != '' || $_POST['estado'] != '' || $_POST['periodo_fim'] != '' || $_POST['indicador_id'] != ''){
                    $where .= ' AND e.estado IN (SELECT estado FROM zona_atuacao WHERE usuario_id = '.$_POST['zona'].')';
                }else{
                    $where = ' e.estado IN (SELECT estado FROM zona_atuacao WHERE usuario_id = '.$_POST['zona'].')';
                }
            }*/

            if($_POST['representante'] != ''){
                
                if( $_POST['estado'] != '' || $_POST['periodo_fim'] != '' || $_POST['indicador_id'] != ''){
                    $where .= ' AND (orr.usuario_id = '.$_POST['representante'].' or o.usuario_id ='.$_POST['representante'].')';
                }else{
                    $where = ' (orr.usuario_id = '.$_POST['representante'].' or o.usuario_id ='.$_POST['representante'].')';
                }

            }

            /*if($_POST['indicadores'] != ''){
                
                if($_POST['estado'] != '' || $_POST['periodo_fim'] != '' || $_POST['representante'] != ''){
                    $where .= ' AND (orr.usuario_id = '.$_POST['indicadores'].' or o.usuario_id ='.$_POST['indicadores'].' or o.indicador_id='.$_POST['indicadores'].')';
                }else{
                    $where = ' (orr.usuario_id = '.$_POST['indicadores'].' or o.usuario_id ='.$_POST['indicadores'].' or o.indicador_id='.$_POST['indicadores'].')';
                }

            }*/

            if($_POST['estado'] != ''){
                
                if($_POST['representante'] != '' || $_POST['periodo_fim'] != '' || $_POST['indicador_id'] != ''){
                    $where .= " AND e.estado = '".$_POST['estado']."'";
                }else{
                    $where = " e.estado = '".$_POST['estado']."'";
                }

            }

            if($_POST['periodo_ini'] != '' && $_POST['periodo_fim'] != ''){
                
                $data_ini = str_replace('/', '-', $_POST['periodo_ini'] );
                $data_ini = date("Y-m-d", strtotime($data_ini));

                $data_fim = str_replace('/', '-', $_POST['periodo_fim'] );
                $data_fim = date("Y-m-d", strtotime($data_fim));
                
                if($_POST['representante'] != '' || $_POST['estado'] != ''){
                    $where .= " AND o.emissao BETWEEN '".$data_ini."' AND '".$data_fim."' ";
                }else{
                    $where = " o.emissao BETWEEN '".$data_ini."' AND '".$data_fim."'";
                }

            }

            if( isset($_POST['status'])){
                if( $_POST['status'] != '' ){
                    $where.=' and o.status_orcamento_id in (';                
                    foreach(    $_POST['status'] as $status     ){                     
                        $where.= $status.',';
                    }
                    $where.='0)';
                }
            }            

            $sql =  "SELECT     e.*, concat(e.id,' - ',e.cnpj,' - ',e.razao_social) as solicitante, date_format(o.emissao,'%d/%m/%Y') as emissao, o.validade, s.descricao as status,o.id as orcamento_id, o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, o.contato_posto, o.observacao,o.dt_previsao, o.dt_previsao_prod
                        FROM    orcamentos o 
                        INNER JOIN   empresas e                     ON  o.empresa_id = e.id
                        INNER JOIN   status_orcamentos s            ON  s.id = o.status_orcamento_id
                        LEFT  JOIN   orcamento_responsavel as orr   ON  orr.orcamento_id = o.id
                        LEFT  JOIN   usuarios usu                   ON  usu.id = orr.usuario_id
                        WHERE       o.status_orcamento_id != 13
                        ".$sWhere." AND  ".$where.' '.$sOrder;
            
            //echo $sql; die;
            
            $query = $this->db->query($sql);
            
            $orcamentos = $query->result_array();
            
            $total      = count($orcamentos);
            
            $orcamentos = array_slice($orcamentos, $inicial, $final);  

            foreach ($orcamentos as $key => $orcamento) {
                if( $orcamento['status'] == "aberto" ){
                    $status = "btn m-btn--pill m-btn--air btn-secondary";
                    $background = '';
                }elseif( $orcamento['status'] == "fechado" ){
                    $status = "btn m-btn--pill m-btn--air btn-success";
                    $background = '';
                }elseif( $orcamento['status'] == "perdido outros" || $orcamento['status'] == "perdido para wayne" || $orcamento['status'] == "perdido para gilbarco" ){
                    $status = "btn m-btn--pill m-btn--air btn-danger";
                    $background = '';
                }elseif( $orcamento['status'] == "cancelado" ){
                    $status = "btn m-btn--pill m-btn--air btn-warning";
                    $background = '';
                }elseif( $orcamento['status'] == "orçamento entregue" ){
                    $status = "btn m-btn--pill m-btn--air btn-info";
                    $background = '';
                }elseif( $orcamento['status'] == "em negociação"){
                    $status = "btn m-btn--pill m-btn--air btn-primary";
                    $background = '';
                }elseif($orcamento['status'] == "Solicitação de Orçamentos - Indicador"){
                    $status = "btn m-btn--pill m-btn--air";
                    $background = "style='background: #ff9800;'";
                }else{
                    $status = "btn m-btn--pill m-btn--air";
                    $background = "style='background: #999;'";
                }

                $orcamentos[$key]['status'] = '<button id="'.$orcamento['orcamento_id'].'" class="status '.$status.'" status="'.$orcamento['status'].'" '. $background.' title="Status do orçamento" onclick="status('.$orcamento['orcamento_id'].',\''.$orcamento['dt_previsao'].'\');"> '.ucfirst($orcamento['status']).'   </button>';

                $orcamentos[$key]['andamento'] = '<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour"><i title="Andamento do Orçamento" onclick="andamento('.$orcamento['orcamento_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i></button>';
                $orcamentos[$key]['responsavel'] = $orcamento['responsavel'].'<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air responsavel_tour"><i title="Alterar Responsável" onclick="responsavel('.$orcamento['orcamento_id'].', '.$orcamento['responsavel_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-user-ok responsavel" style="cursor: pointer;" ></i>';
                $orcamentos[$key]['acoes'] = '<span style="overflow: visible; width: 110px;" class="">                              
                                                <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill visualizar_orcamento" onclick="visualizar_orcamento('.$orcamento['orcamento_id'].','.$this->session->userdata('usuario_id').')" title="Visualizar" orcamento_id="'.$orcamento['orcamento_id'].'" target="_blank" >
                                                    <i class="la la-eye visualizar"></i>
                                                </a> 

                                                <button  id="excluir" onclick="excluirOrcamento('.$orcamento['orcamento_id'].');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" orcamento_id="'.$orcamento['orcamento_id'].'">
                                                    <i class="la la-trash"></i>
                                                </button>
                                            </span>';
            }

            $sql    = "SELECT COUNT(*) AS Total FROM orcamentos";
            $query  = $this->db->query($sql);
            $iTotal = $query->row_array();
            $output = array("iTotalRecords" => $iTotal['Total'],
                            "iTotalDisplayRecords" => $total,
                            "aaData" => $orcamentos     ); 
            
        return $output;
    
    }

    public function buscaOrcamentosExpirados($dados)  
    {
            $aColumns = array('orcamento_id', 'solicitante', 'emissao', 'status','andamento', 'responsavel','estado', 'origem', 'acoes');
            
            $sLimit = "";
            if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
                $sLimit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] );
                $inicial = intval( $_POST['start'] );
                $final = intval( $_POST['length'] );
            }
            $sOrder = "ORDER BY o.id desc";

            if ( isset( $_POST['order'] ) ) {
                $sOrder = " ORDER BY ";
                for ( $i=0 ; $i< sizeof( $_POST['order'] ) ; $i++ ) {
                    if ( $_POST['columns'][$i]['orderable'] == "true" ) {
                        $sOrder .= $aColumns[ intval( $_POST['order'][$i]['column'] ) ]." ".($_POST['order'][$i]['dir']==='asc' ? 'asc' : 'desc') .", ";
                    }
                }
                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" ) {
                    $sOrder = " ORDER BY {$sIndexColumn} DESC";
                }
            }
            $sWhere = '';
            if ( isset($_POST['search']) && $_POST['search']['value'] != "" ) {
                $sWhere = ""; 
                for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
                    if ( isset($_POST['columns'][$i]['searchable']) && $_POST['columns'][$i]['searchable'] == "true" ) {
                        switch ($aColumns[$i]) {
                            case 'orcamento_id':
                                $sWhere .= " AND (o.id LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'solicitante':
                                $sWhere .= " concat(e.cnpj,' ',e.razao_social,' ',e.id) LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'emissao':
                                $sWhere .= " o.emissao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'status':
                                $sWhere .= " s.descricao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'responsavel':
                                $sWhere .= " usu.nome LIKE '%". $_POST['search']['value'] ."%' OR" ;
                                break;
                            case 'origem':
                                $sWhere .= " o.origem LIKE '%". $_POST['search']['value'] ."%')";
                                break;
                            case 'estado':
                                $sWhere .= " e.estado LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                        }
                    }
                }
            }

            $where = '1=1';
            /*if($_POST['zona'] != '' ){
                if($_POST['representante'] != '' || $_POST['estado'] != '' || $_POST['periodo_fim'] != ''){
                    $where .= ' AND e.estado IN (SELECT estado FROM zona_atuacao WHERE usuario_id = '.$_POST['zona'].')';
                }else{
                    $where = ' e.estado IN (SELECT estado FROM zona_atuacao WHERE usuario_id = '.$_POST['zona'].')';
                }
            }*/

            if($_POST['representante'] != ''){
                
                if( $_POST['estado'] != '' || $_POST['periodo_fim'] != ''){
                    $where .= ' AND (orr.usuario_id = '.$_POST['representante'].' or o.usuario_id ='.$_POST['representante'].')';
                }else{
                    $where = ' (orr.usuario_id = '.$_POST['representante'].' or o.usuario_id ='.$_POST['representante'].')';
                }

            }

            if($_POST['estado'] != ''){
                
                if($_POST['representante'] != '' ||  $_POST['periodo_fim'] != ''){
                    $where .= " AND e.estado = '".$_POST['estado']."'";
                }else{
                    $where = " e.estado = '".$_POST['estado']."'";
                }

            }

            if($_POST['periodo_ini'] != '' && $_POST['periodo_fim'] != ''){
                
                $data_ini = str_replace('/', '-', $_POST['periodo_ini'] );
                $data_ini = date("Y-m-d", strtotime($data_ini));

                $data_fim = str_replace('/', '-', $_POST['periodo_fim'] );
                $data_fim = date("Y-m-d", strtotime($data_fim));
                
                if($_POST['representante'] != '' || $_POST['estado'] != ''){
                    $where .= " AND o.emissao BETWEEN '".$data_ini."' AND '".$data_fim."' ";
                }else{
                    $where = " o.emissao BETWEEN '".$data_ini."' AND '".$data_fim."'";
                }

            }
            if( isset($_POST['status']) ){
                if( $_POST['status']    !=  ''  ){
                    $where.=' and o.status_orcamento_id in (';                
                    foreach(    $_POST['status'] as $status     ){                     
                        $where.= $status.',';
                    }
                    $where.='0)';
                }
            }
            $sql =  "SELECT     e.*, concat(e.id,' - ',e.cnpj,' - ',e.razao_social) as solicitante, date_format(o.emissao,'%d/%m/%Y') as emissao, o.validade, s.descricao as status,o.id as orcamento_id, o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, o.contato_posto, o.observacao
                        FROM        orcamentos o 
                        INNER JOIN  empresas e                      ON o.empresa_id = e.id 
                        INNER JOIN  status_orcamentos s             ON s.id = o.status_orcamento_id 
                        LEFT JOIN   orcamento_responsavel as orr    ON orr.orcamento_id = o.id 
                        LEFT JOIN   usuarios usu                    ON usu.id = orr.usuario_id
                        WHERE       o.status_orcamento_id = 13
                        ".$sWhere." AND ".$where." ".$sOrder;

            $query = $this->db->query($sql);
            $orcamentos = $query->result_array();
            $total = count($orcamentos);
            $orcamentos = array_slice($orcamentos, $inicial, $final);
            foreach ($orcamentos as $key => $orcamento) {

                if( $orcamento['status'] == "Expirados" ){
                    $status = "btn m-btn m-btn--pill m-btn--air m-btn--gradient-from-focus m-btn--gradient-to-warning";
                    $background = '';
                }

                $orcamentos[$key]['status'] = '<button id="'.$orcamento['orcamento_id'].'" class="status '.$status.'" status="'.$orcamento['status'].'" '. $background.' title="Status do orçamento" onclick="status('.$orcamento['orcamento_id'].');"> '.ucfirst($orcamento['status']).'   </button>';

                $orcamentos[$key]['andamento'] = '<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour"><i title="Andamento do Orçamento" onclick="andamento('.$orcamento['orcamento_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i></button>';
                $orcamentos[$key]['responsavel'] = $orcamento['responsavel'].'<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air responsavel_tour"><i title="Alterar Responsável" onclick="responsavel('.$orcamento['orcamento_id'].', '.$orcamento['responsavel_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-user-ok responsavel" style="cursor: pointer;" ></i>';
                $orcamentos[$key]['acoes'] = '<span style="overflow: visible; width: 110px;" class="">                              
                                                <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill visualizar_orcamento" onclick="visualizar_orcamento('.$orcamento['orcamento_id'].','.$this->session->userdata('usuario_id').')" title="Visualizar" orcamento_id="'.$orcamento['orcamento_id'].'" target="_blank" >
                                                    <i class="la la-eye visualizar"></i>
                                                </a> 

                                                <button  id="excluir" onclick="excluirOrcamento('.$orcamento['orcamento_id'].');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" orcamento_id="'.$orcamento['orcamento_id'].'">
                                                    <i class="la la-trash"></i>
                                                </button>
                                            </span>';
            }

            $sql  = "SELECT COUNT(*) AS Total FROM orcamentos";
            $query = $this->db->query($sql);
            $iTotal = $query->row_array();
            $output = array("iTotalRecords" => $iTotal['Total'],
                            "iTotalDisplayRecords" => $total,
                            "aaData" => $orcamentos     ); 
            
        return $output;
    
    }

    public function buscaOrcamentosZona($dados) 
    {
        $aColumns = array('orcamento_id', 'solicitante', 'emissao', 'status','andamento', 'responsavel','estado', 'origem', 'acoes');            
        $sLimit = "";
        
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $sLimit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] );
            $inicial = intval( $_POST['start'] );
            $final = intval( $_POST['length'] );
        }

        $sOrder = "ORDER BY o.id desc";

        if ( isset( $_POST['order'] ) ) {
            $sOrder = " ORDER BY ";
            for ( $i=0 ; $i< sizeof( $_POST['order'] ) ; $i++ ) {
                if ( $_POST['columns'][$i]['orderable'] == "true" ) {
                    $sOrder .= $aColumns[ intval( $_POST['order'][$i]['column'] ) ]." ".($_POST['order'][$i]['dir']==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" ) {
                $sOrder = " ORDER BY {$sIndexColumn} DESC";
            }
        }
        $sWhere = '';
        if ( isset($_POST['search']) && $_POST['search']['value'] != "" ) {
            
            for ( $i=0 ; $i<count($aColumns) ; $i++ ) {

                if ( isset($_POST['columns'][$i]['searchable']) && $_POST['columns'][$i]['searchable'] == "true" ) {

                    switch ($aColumns[$i]) {
                        case 'orcamento_id':
                            $sWhere .= " AND (o.id LIKE '%". $_POST['search']['value'] ."%' OR";
                            break;
                        case 'solicitante':
                            $sWhere .= " concat(e.cnpj,' ',e.razao_social,' ',e.id) LIKE '%". $_POST['search']['value'] ."%' OR";
                            break;
                        case 'emissao':
                            $sWhere .= " o.emissao LIKE '%". $_POST['search']['value'] ."%' OR";
                            break;
                        case 'status':
                            $sWhere .= " s.descricao LIKE '%". $_POST['search']['value'] ."%' OR";
                            break;
                        case 'responsavel':
                            $sWhere .= " usu.nome LIKE '%". $_POST['search']['value'] ."%' OR" ;
                            break;
                        case 'origem':
                            $sWhere .= " o.origem LIKE '%". $_POST['search']['value'] ."%')";
                            break;
                        case 'estado':
                            $sWhere .= " e.estado LIKE '%". $_POST['search']['value'] ."%' OR";
                            break;
                    }
                }
            }
        }

        $where = '1=1';       

        if($_POST['representante'] != ''){
            if($_POST['estado'] != '' || $_POST['periodo_fim'] != ''){
                $where .= ' AND (orr.usuario_id = '.$_POST['representante'].' or o.usuario_id ='.$_POST['representante'].')';
            }else{
                $where = ' (orr.usuario_id = '.$_POST['representante'].' or o.usuario_id ='.$_POST['representante'].')';
            }

        }

        if($_POST['estado'] != ''){
            if($_POST['representante'] != '' ||  $_POST['periodo_fim'] != ''){
                $where .= " AND e.estado = '".$_POST['estado']."'";
            }else{
                $where = " e.estado = '".$_POST['estado']."'";
            }

        }

        if($_POST['periodo_ini'] != '' && $_POST['periodo_fim'] != ''){
            
            $data_ini = str_replace('/', '-', $_POST['periodo_ini'] );
            $data_ini = date("Y-m-d", strtotime($data_ini));

            $data_fim = str_replace('/', '-', $_POST['periodo_fim'] );
            $data_fim = date("Y-m-d", strtotime($data_fim));
            
            if($_POST['representante'] != '' || $_POST['estado'] != ''){
                $where .= " AND o.emissao BETWEEN '".$data_ini."' AND '".$data_fim."' ";
            }else{
                $where = " o.emissao BETWEEN '".$data_ini."' AND '".$data_fim."'";
            }

        }
        if( isset($_POST['status']) ){
            if( $_POST['status']    !=  ''  ){
                $where.=' and o.status_orcamento_id in (';                
                foreach(    $_POST['status'] as $status     ){                     
                    $where.= $status.',';
                }
                $where.='0)';
            }
        }
        $sql =  "SELECT     e.*, concat(e.id,' - ',e.cnpj,' - ',e.razao_social) as solicitante, date_format(o.emissao,'%d/%m/%Y') as emissao, o.validade, s.descricao as status,o.id as orcamento_id, o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, o.contato_posto, o.observacao, o.dt_previsao, o.dt_previsao_prod
                    FROM        orcamentos o 
                    INNER JOIN  empresas e                      ON o.empresa_id = e.id 
                    INNER JOIN  status_orcamentos s             ON s.id = o.status_orcamento_id 
                    LEFT  JOIN   orcamento_responsavel as orr    ON orr.orcamento_id = o.id 
                    LEFT  JOIN   usuarios usu                    ON usu.id = orr.usuario_id
                    WHERE       o.status_orcamento_id != 13 and e.estado in (select estado from zona_atuacao where usuario_id = ".$dados['usuario_id'].")
                    ".$sWhere." AND ".$where." ".$sOrder;

        $query = $this->db->query($sql);
        
        $orcamentos = $query->result_array();
        
        $total      = count($orcamentos);
        
        $orcamentos = array_slice($orcamentos, $inicial, $final);  

        foreach ($orcamentos as $key => $orcamento) {
            if( $orcamento['status'] == "aberto" ){
                $status = "btn m-btn--pill m-btn--air btn-secondary";
                $background = '';
            }elseif( $orcamento['status'] == "fechado" ){
                $status = "btn m-btn--pill m-btn--air btn-success";
                $background = '';
            }elseif( $orcamento['status'] == "perdido outros" || $orcamento['status'] == "perdido para wayne" || $orcamento['status'] == "perdido para gilbarco" ){
                $status = "btn m-btn--pill m-btn--air btn-danger";
                $background = '';
            }elseif( $orcamento['status'] == "cancelado" ){
                $status = "btn m-btn--pill m-btn--air btn-warning";
                $background = '';
            }elseif( $orcamento['status'] == "orçamento entregue" ){
                $status = "btn m-btn--pill m-btn--air btn-info";
                $background = '';
            }elseif( $orcamento['status'] == "em negociação"){
                $status = "btn m-btn--pill m-btn--air btn-primary";
                $background = '';
            }elseif($orcamento['status'] == "Solicitação de Orçamentos - Indicador"){
                $status = "btn m-btn--pill m-btn--air";
                $background = "style='background: #ff9800;'";
            }else{
                $status = "btn m-btn--pill m-btn--air";
                $background = "style='background: #999;'";
            }

            $orcamentos[$key]['status'] = '<button id="'.$orcamento['orcamento_id'].'" class="status '.$status.'" status="'.$orcamento['status'].'" '. $background.' title="Status do orçamento" onclick="status('.$orcamento['orcamento_id'].',\''.$orcamento['dt_previsao'].'\');"> '.ucfirst($orcamento['status']).'   </button>';

            $orcamentos[$key]['andamento'] = '<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour"><i title="Andamento do Orçamento" onclick="andamento('.$orcamento['orcamento_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i></button>';
            $orcamentos[$key]['responsavel'] = $orcamento['responsavel'].'<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air responsavel_tour"><i title="Alterar Responsável" onclick="responsavel('.$orcamento['orcamento_id'].', '.$orcamento['responsavel_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-user-ok responsavel" style="cursor: pointer;" ></i>';
            $orcamentos[$key]['acoes'] = '<span style="overflow: visible; width: 110px;" class="">                              
                                            <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill visualizar_orcamento" onclick="visualizar_orcamento('.$orcamento['orcamento_id'].')" title="Visualizar" orcamento_id="'.$orcamento['orcamento_id'].'" target="_blank" >
                                                <i class="la la-eye visualizar"></i>
                                            </a> 

                                            <button  id="excluir" onclick="excluirOrcamento('.$orcamento['orcamento_id'].');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" orcamento_id="'.$orcamento['orcamento_id'].'">
                                                <i class="la la-trash"></i>
                                            </button>
                                        </span>';
        }

        $sql  = "SELECT COUNT(*) AS Total FROM orcamentos";
        $query = $this->db->query($sql);
        $iTotal = $query->row_array();
        $output = array("iTotalRecords" => $iTotal['Total'],
                        "iTotalDisplayRecords" => $total,
                        "aaData" => $orcamentos     ); 
            
        return $output;
    
    }

    public function buscaOrcamentosExpiradosZona($dados)  
    {
            $aColumns = array('orcamento_id', 'solicitante', 'emissao', 'status','andamento', 'responsavel','estado', 'origem', 'acoes');
            
            $sLimit = "";
            if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
                $sLimit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] );
                $inicial = intval( $_POST['start'] );
                $final = intval( $_POST['length'] );
            }
            $sOrder = "ORDER BY o.id desc";

            if ( isset( $_POST['order'] ) ) {
                $sOrder = " ORDER BY ";
                for ( $i=0 ; $i< sizeof( $_POST['order'] ) ; $i++ ) {
                    if ( $_POST['columns'][$i]['orderable'] == "true" ) {
                        $sOrder .= $aColumns[ intval( $_POST['order'][$i]['column'] ) ]." ".($_POST['order'][$i]['dir']==='asc' ? 'asc' : 'desc') .", ";
                    }
                }
                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" ) {
                    $sOrder = " ORDER BY {$sIndexColumn} DESC";
                }
            }
            $sWhere = '';
            if ( isset($_POST['search']) && $_POST['search']['value'] != "" ) {
                $sWhere = ""; 
                for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
                    if ( isset($_POST['columns'][$i]['searchable']) && $_POST['columns'][$i]['searchable'] == "true" ) {
                        switch ($aColumns[$i]) {
                            case 'orcamento_id':
                                $sWhere .= " AND (o.id LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'solicitante':
                                $sWhere .= " concat(e.cnpj,' ',e.razao_social,' ',e.id) LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'emissao':
                                $sWhere .= " o.emissao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'status':
                                $sWhere .= " s.descricao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'responsavel':
                                $sWhere .= " usu.nome LIKE '%". $_POST['search']['value'] ."%' OR" ;
                                break;
                            case 'origem':
                                $sWhere .= " o.origem LIKE '%". $_POST['search']['value'] ."%')";
                                break;
                            case 'estado':
                                $sWhere .= " e.estado LIKE '%". $_POST['search']['value'] ."%' OR";
                            break;
                        }
                    }
                }
            }

        $where = '1=1';

        if($_POST['representante'] != ''){
            if( $_POST['estado'] != ''){
                $where .= ' AND (orr.usuario_id = '.$_POST['representante'].' or o.usuario_id ='.$_POST['representante'].')';
            }else{
                $where = ' (orr.usuario_id = '.$_POST['representante'].' or o.usuario_id ='.$_POST['representante'].')';
            }

        }

        if($_POST['estado'] != ''){
            
            if($_POST['representante'] != ''){
                $where .= " AND e.estado = '".$_POST['estado']."'";
            }else{
                $where = " e.estado = '".$_POST['estado']."'";
            }

        }

        if($_POST['periodo_ini'] != '' && $_POST['periodo_fim'] != ''){
            
            $data_ini = str_replace('/', '-', $_POST['periodo_ini'] );
            $data_ini = date("Y-m-d", strtotime($data_ini));

            $data_fim = str_replace('/', '-', $_POST['periodo_fim'] );
            $data_fim = date("Y-m-d", strtotime($data_fim));
            
            if( $_POST['representante'] != ''    ||  $_POST['estado'] != ''  ){
                $where .= " AND o.emissao BETWEEN '".$data_ini."' AND '".$data_fim."' ";
            }else{
                $where = " o.emissao BETWEEN '".$data_ini."' AND '".$data_fim."'";
            }

        }
        
        if( isset($_POST['status']) ){
            if( $_POST['status']    !=  ''  ){
                $where.=' and o.status_orcamento_id in (';                
                foreach(    $_POST['status'] as $status     ){
                    $where.= $status.',';
                }
                $where.='0)';
            }
        }
            
        $sql =  "SELECT     e.*, concat(e.id,' - ',e.cnpj,' - ',e.razao_social) as solicitante, date_format(o.emissao,'%d/%m/%Y') as emissao, o.validade, s.descricao as status,o.id as orcamento_id, o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, o.contato_posto, o.observacao
                FROM        orcamentos o 
                INNER JOIN  empresas e                      ON  o.empresa_id = e.id 
                INNER JOIN  status_orcamentos s             ON  s.id = o.status_orcamento_id 
                LEFT JOIN   orcamento_responsavel as orr    ON  orr.orcamento_id = o.id 
                LEFT JOIN   usuarios usu                    ON  usu.id = orr.usuario_id
                WHERE       o.status_orcamento_id = 13 and e.estado in (select estado from zona_atuacao where usuario_id = ".$dados['usuario_id'].")
                ".$sWhere." AND ".$where." ".$sOrder;

            $query = $this->db->query($sql);
            $orcamentos = $query->result_array();
            $total = count($orcamentos);
            $orcamentos = array_slice($orcamentos, $inicial, $final);  
            foreach ($orcamentos as $key => $orcamento) {

                if( $orcamento['status'] == "Expirados" ){
                    $status = "btn m-btn m-btn--pill m-btn--air m-btn--gradient-from-focus m-btn--gradient-to-warning";
                    $background = '';
                }

                $orcamentos[$key]['status'] = '<button id="'.$orcamento['orcamento_id'].'" class="status '.$status.'" status="'.$orcamento['status'].'" '. $background.' title="Status do orçamento" onclick="status('.$orcamento['orcamento_id'].');"> '.ucfirst($orcamento['status']).'   </button>';

                $orcamentos[$key]['andamento'] = '<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour"><i title="Andamento do Orçamento" onclick="andamento('.$orcamento['orcamento_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i></button>';
                $orcamentos[$key]['responsavel'] = $orcamento['responsavel'].'<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air responsavel_tour"><i title="Alterar Responsável" onclick="responsavel('.$orcamento['orcamento_id'].', '.$orcamento['responsavel_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-user-ok responsavel" style="cursor: pointer;" ></i>';
                $orcamentos[$key]['acoes'] = '<span style="overflow: visible; width: 110px;" class="">                              
                                                <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill visualizar_orcamento" onclick="visualizar_orcamento('.$orcamento['orcamento_id'].')" title="Visualizar" orcamento_id="'.$orcamento['orcamento_id'].'" target="_blank" >
                                                    <i class="la la-eye visualizar"></i>
                                                </a> 

                                                <button  id="excluir" onclick="excluirOrcamento('.$orcamento['orcamento_id'].');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" orcamento_id="'.$orcamento['orcamento_id'].'">
                                                    <i class="la la-trash"></i>
                                                </button>
                                            </span>';
            }

            $sql  = "SELECT COUNT(*) AS Total FROM orcamentos";
            $query = $this->db->query($sql);
            $iTotal = $query->row_array();
            $output = array("iTotalRecords" => $iTotal['Total'],
                            "iTotalDisplayRecords" => $total,
                            "aaData" => $orcamentos     ); 
            
        return $output;
    
    }

     public function buscaOrcamentosStatus($dados) 
    {

            $aColumns = array('orcamento_id', 'solicitante', 'emissao', 'status','andamento', 'responsavel','estado', 'origem', 'acoes');            
            $sLimit = "";
            if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
                $sLimit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] );
                $inicial = intval( $_POST['start'] );
                $final = intval( $_POST['length'] );
            }

            $sOrder = "ORDER BY o.id desc";

            if ( isset( $_POST['order'] ) ) {
                $sOrder = " ORDER BY ";
                for ( $i=0 ; $i< sizeof( $_POST['order'] ) ; $i++ ) {
                    if ( $_POST['columns'][$i]['orderable'] == "true" ) {
                        $sOrder .= $aColumns[ intval( $_POST['order'][$i]['column'] ) ]." ".($_POST['order'][$i]['dir']==='asc' ? 'asc' : 'desc') .", ";
                    }
                }
                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == " ORDER BY" ) {
                    $sOrder = " ORDER BY {$sIndexColumn} DESC";
                }
            }
            $sWhere = '';
            if ( isset($_POST['search']) && $_POST['search']['value'] != "" ) {
                
                for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
                    if ( isset($_POST['columns'][$i]['searchable']) && $_POST['columns'][$i]['searchable'] == "true" ) {
                        switch ($aColumns[$i]) {
                            case 'orcamento_id':
                                $sWhere .= " AND (o.id LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'solicitante':
                                $sWhere .= " concat(e.cnpj,' ',e.razao_social,' ',e.id) LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'emissao':
                                $sWhere .= " o.emissao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'status':
                                $sWhere .= " s.descricao LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'estado':
                                $sWhere .= " e.estado LIKE '%". $_POST['search']['value'] ."%' OR";
                                break;
                            case 'responsavel':
                                $sWhere .= " usu.nome LIKE '%". $_POST['search']['value'] ."%' OR" ;
                                break;
                            case 'origem':
                                $sWhere .= " o.origem LIKE '%". $_POST['search']['value'] ."%')";
                                break;
                        }
                    }
                }
            }

            $where = '1=1';
          
            if($_POST['representante'] != ''){                
                
                $where .= ' AND o.indicador_id = '.$_POST['representante'];                

            }

           
            if($_POST['estado'] != ''){

                $where .= " AND e.estado = '".$_POST['estado']."'";               

            }

            if($_POST['periodo_ini'] != '' && $_POST['periodo_fim'] != ''){
                
                $data_ini = str_replace('/', '-', $_POST['periodo_ini'] );
                $data_ini = date("Y-m-d", strtotime($data_ini));

                $data_fim = str_replace('/', '-', $_POST['periodo_fim'] );
                $data_fim = date("Y-m-d", strtotime($data_fim));
                
                if($_POST['representante'] != '' || $_POST['estado'] != ''){
                    $where .= " AND o.emissao BETWEEN '".$data_ini."' AND '".$data_fim."' ";
                }else{
                    $where .= " AND o.emissao BETWEEN '".$data_ini."' AND '".$data_fim."'";
                }

            }
             
            if( $dados['status_id'] == 99 ){
                //$sWhere .= ' AND o.status_orcamento_id in (3,4,10,11,14,7,9) AND orr.usuario_id = ' .$dados['usuario_id'];
                $sWhere .= ' AND o.status_orcamento_id in (3,4,10,11,14,7,9)';
            }else{
                //$sWhere .= ' AND o.status_orcamento_id = '.$dados['status_id'].' AND orr.usuario_id = ' .$dados['usuario_id'];
                $sWhere .= ' AND o.status_orcamento_id = '.$dados['status_id'];
            }
            
            $sql =  "SELECT     e.*, concat(e.id,' - ',e.cnpj,' - ',e.razao_social) as solicitante, date_format(o.emissao,'%d/%m/%Y') as emissao, o.validade, s.descricao as status,o.id as orcamento_id, o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, o.contato_posto, o.observacao,o.dt_previsao, o.dt_previsao_prod
                        FROM    orcamentos o 
                        INNER JOIN   empresas e                     ON  o.empresa_id = e.id
                        INNER JOIN   status_orcamentos s            ON  s.id = o.status_orcamento_id
                        LEFT  JOIN   orcamento_responsavel as orr   ON  orr.orcamento_id = o.id
                        LEFT  JOIN   usuarios usu                   ON  usu.id = orr.usuario_id
                        WHERE       o.status_orcamento_id != 13
                        ".$sWhere." AND  ".$where.' '.$sOrder;
            
            //echo $sql; die;
            
            $query = $this->db->query($sql);
            
            $orcamentos = $query->result_array();
            
            $total      = count($orcamentos);
            
            $orcamentos = array_slice($orcamentos, $inicial, $final);  

            foreach ($orcamentos as $key => $orcamento) {
                if( $orcamento['status'] == "aberto" ){
                    $status = "btn m-btn--pill m-btn--air btn-secondary";
                    $background = '';
                }elseif( $orcamento['status'] == "fechado" ){
                    $status = "btn m-btn--pill m-btn--air btn-success";
                    $background = '';
                }elseif( $orcamento['status'] == "perdido outros" || $orcamento['status'] == "perdido para wayne" || $orcamento['status'] == "perdido para gilbarco" ){
                    $status = "btn m-btn--pill m-btn--air btn-danger";
                    $background = '';
                }elseif( $orcamento['status'] == "cancelado" ){
                    $status = "btn m-btn--pill m-btn--air btn-warning";
                    $background = '';
                }elseif( $orcamento['status'] == "orçamento entregue" ){
                    $status = "btn m-btn--pill m-btn--air btn-info";
                    $background = '';
                }elseif( $orcamento['status'] == "em negociação"){
                    $status = "btn m-btn--pill m-btn--air btn-primary";
                    $background = '';
                }elseif($orcamento['status'] == "Solicitação de Orçamentos - Indicador"){
                    $status = "btn m-btn--pill m-btn--air";
                    $background = "style='background: #ff9800;'";
                }else{
                    $status = "btn m-btn--pill m-btn--air";
                    $background = "style='background: #999;'";
                }

                $orcamentos[$key]['status'] = '<button id="'.$orcamento['orcamento_id'].'" class="status '.$status.'" status="'.$orcamento['status'].'" '. $background.' title="Status do orçamento" onclick="status('.$orcamento['orcamento_id'].',\''.$orcamento['dt_previsao'].'\');"> '.ucfirst($orcamento['status']).'   </button>';

                $orcamentos[$key]['andamento'] = '<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour"><i title="Andamento do Orçamento" onclick="andamento('.$orcamento['orcamento_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i></button>';
                $orcamentos[$key]['responsavel'] = $orcamento['responsavel'].'<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air responsavel_tour"><i title="Alterar Responsável" onclick="responsavel('.$orcamento['orcamento_id'].', '.$orcamento['responsavel_id'].','.$orcamento['status_orcamento_id'].');" class="flaticon-user-ok responsavel" style="cursor: pointer;" ></i>';
                $orcamentos[$key]['acoes'] = '<span style="overflow: visible; width: 110px;" class="">                              
                                                <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill visualizar_orcamento" onclick="visualizar_orcamento('.$orcamento['orcamento_id'].','.$this->session->userdata('usuario_id').')" title="Visualizar" orcamento_id="'.$orcamento['orcamento_id'].'" target="_blank" >
                                                    <i class="la la-eye visualizar"></i>
                                                </a> 

                                                <button  id="excluir" onclick="excluirOrcamento('.$orcamento['orcamento_id'].');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" orcamento_id="'.$orcamento['orcamento_id'].'">
                                                    <i class="la la-trash"></i>
                                                </button>
                                            </span>';
            }

            $sql    = "SELECT COUNT(*) AS Total FROM orcamentos";
            $query  = $this->db->query($sql);
            $iTotal = $query->row_array();
            $output = array("iTotalRecords" => $iTotal['Total'],
                            "iTotalDisplayRecords" => $total,
                            "aaData" => $orcamentos     ); 
            
        return $output;
    
    }

    public function buscaOrcamentosRepresentantes($user_id)
    {
        
        $sql =  "SELECT e.*, o.emissao,o.validade, s.descricao as status,o.id as orcamento_id,
                        o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, usu_gera.nome as criado_por  
                    FROM        orcamentos o 
                    INNER JOIN  empresas e                      ON  o.empresa_id = e.id 
                    INNER JOIN  status_orcamentos s             ON  s.id = o.status_orcamento_id 
                    LEFT JOIN   orcamento_responsavel as orr    ON  orr.orcamento_id = o.id 
                    LEFT JOIN   usuarios usu                    ON  usu.id = orr.usuario_id 
                    INNER JOIN  usuarios usu_gera               ON  o.solicitante_id = usu_gera.id
                    INNER JOIN  empresas emp_gera               ON  usu_gera.empresa_id = emp_gera.id
                    WHERE  o.indicador_id in (select us.id from usuarios us where us.empresa_id = (select empresa_id from usuarios where id = ".$user_id.") ) 
                    ORDER BY  o.id desc";

        $query = $this->db->query($sql);
      
        return $query->result();
    
    }

    public function buscaOrcamentosRepresentantesStatus ($user_id,$tipo)
    {
        $where = ($tipo == 1) ? ' and o.status_orcamento_id in (1,6,7)' : ' and o.status_orcamento_id not in (1,6,7)';
        $sql =  "SELECT e.*, o.emissao,o.validade, s.descricao as status,o.id as orcamento_id,
                        o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, usu_gera.nome as criado_por  
                    FROM        orcamentos o 
                    INNER JOIN  empresas e                      ON  o.empresa_id = e.id 
                    INNER JOIN  status_orcamentos s             ON  s.id = o.status_orcamento_id 
                    LEFT JOIN   orcamento_responsavel as orr    ON  orr.orcamento_id = o.id 
                    LEFT JOIN   usuarios usu                    ON  usu.id = orr.usuario_id 
                    INNER JOIN  usuarios usu_gera               ON  o.solicitante_id = usu_gera.id
                    INNER JOIN  empresas emp_gera               ON  usu_gera.empresa_id = emp_gera.id
                    WHERE  o.indicador_id in (select us.id from usuarios us where us.empresa_id = (select empresa_id from usuarios where id = ".$user_id.") )  ".$where."
                    ORDER BY  o.id desc";

        $query = $this->db->query($sql);
      
        return $query->result();
    
    }
    
    public function buscaOrcamentosRepresentantes2($user_id,$empresa_id,$negociacao)
    {
        
        $where = ($negociacao == 1) ? ' and o.status_orcamento_id in (1,6,7)' : ' and o.status_orcamento_id not in (1,6,7)';

        $sql =  "SELECT e.*, o.emissao,o.validade, s.descricao as status,o.id as orcamento_id,
                        o.status_orcamento_id, usu.nome as responsavel, orr.usuario_id as responsavel_id, o.origem, usu_gera.nome as criado_por  
                    FROM        orcamentos o 
                    INNER JOIN  empresas e                      ON  o.empresa_id = e.id 
                    INNER JOIN  status_orcamentos s             ON  s.id = o.status_orcamento_id 
                    INNER JOIN  orcamento_responsavel as orr    ON  orr.orcamento_id = o.id 
                    LEFT JOIN   usuarios usu                    ON  usu.id = orr.usuario_id 
                    INNER JOIN  usuarios usu_gera               ON  o.solicitante_id = usu_gera.id
                    INNER JOIN  empresas emp_gera               ON  usu_gera.empresa_id = emp_gera.id
                    WHERE       (orr.usuario_id  = ".$user_id."   OR  
                                usu_gera.id = ".$user_id." or 
                                o.id in (SELECT orc.id FROM orcamentos orc 
                                        inner join orcamento_responsavel resp on orc.id = resp.orcamento_id
                                        where resp.usuario_id in (select id from usuarios where empresa_id = ".$empresa_id.") 
                                        ) 
                                )
                                ".$where."
                    ORDER BY  o.id desc";

        $query = $this->db->query($sql);
      
        return $query->result();
    
    }

    public function insereOrcamentoAndamentos($data)
    {
        if($this->db->insert('orcamento_andamentos', $data))
        {
            
            return $this->db->insert_id();
        }else{

            return false;
        }
    }



    public function buscaAndamentos($orcamento_id)
    {
        $sql =  "SELECT oa.*, COALESCE(u.nome,NULL,'') as usuario, so.descricao as status, date_format(oa.dthr_andamento, '%d/%m/%Y %H:%i:%s') as dt_andamento
                FROM    orcamento_andamentos oa
                LEFT JOIN orcamento_responsavel orr ON  orr.orcamento_id = oa.orcamento_id 
                LEFT JOIN usuarios u                ON  u.id = oa.usuario_id 
                LEFT JOIN status_orcamentos    so   on  oa.status_orcamento_id = so.id
                WHERE  oa.orcamento_id = ".$orcamento_id;

        $query = $this->db->query($sql);
        $row = $query->result_array();
        return $row;
    }

    public function atualizaStatusOrcamento($dados)
    {       
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('orcamentos', $dados)){

            return true;
        }else{
            return false;
        }
    }

    public function atualizaResponsavelOrcamento($dados)
    {
         $update = array(
            'usuario_id' => $dados['usuario_id']
        );
         
        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_responsavel', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function insereResponsavelOrcamento($data)
    {
        if($this->db->insert('orcamento_responsavel', $data))
        {
            return true;
        }else{
            return false;
        }        
    }

    public function getOrcamentoProdutos($id) {
        
        $sql =  "SELECT op.id as orcamento_produto_id,op.produto_id,op.qtd,op.valor,op.orcamento_id, o.id as id_orcamento,
                        o.empresa_id,
                        o.emissao,
                        o.validade,
                        o.status_orcamento_id,
                        o.origem,
                        o.solicitante_id,
                        o.usuario_id,
                        o.valor_orcamento ,
                        o.frete_id,
                        o.entrega_id , 
                        o.forma_pagto_id,
                        o.indicador,
                        o.indicador_id,
						o.contato_posto,
                        o.cel_contato_posto,
                        u.nome as contato_do_posto,
                        u.celular,
                        o.contato_id,
                        o.observacao,
                        o.dt_previsao,                        
                        o.dt_previsao_prod,
						p.*, e.*, i.valor_tributo, so.descricao as status, i.fator, o.fl_especial,
                        o.garantia 
            FROM orcamentos o
            LEFT JOIN  orcamento_produtos op ON o.id = op.orcamento_id
            LEFT JOIN  usuarios u ON o.contato_id = u.id
            INNER JOIN  status_orcamentos so ON o.status_orcamento_id = so.id
            LEFT JOIN  produtos p ON p.id = op.produto_id
            INNER JOIN  empresas e ON e.id = o.empresa_id
            LEFT JOIN   estados ON  e.estado = estados.uf
            LEFT JOIN   icms i ON i.estado_id = estados.id                   
                WHERE   o.id=".$id;        

        $query = $this->db->query($sql);
        $retorno = $query->result();
        return $retorno;
    }

    public function getSolicitante($id){

        $sql = "SELECT u.nome, e.razao_social , e.cnpj, e.telefone, u.email, concat(e.cidade,'/', e.estado) as local, e.tp_cadastro  FROM usuarios u, empresas e, orcamentos o WHERE u.empresa_id = e.id and o.solicitante_id = u.id and o.id=".$id;
        
        $query = $this->db->query($sql);
        $retorno = $query->result();
        return $retorno;
    }

    public function getIndicador($id){

        $sql = "SELECT concat(u.nome, ' | ', e.razao_social ,' | ', e.cnpj, ' | ', e.email, ' | ', e.telefone) as indicador, e.telefone, u.email, u.id, o.status_orcamento_id  FROM usuarios u, empresas e, orcamentos o WHERE u.empresa_id = e.id and o.indicador_id = u.id and o.id=".$id;
        
        $query = $this->db->query($sql);
        $retorno = $query->result();        
        return $retorno;
    }

    public function getOrcamentoDesconto($orcamento_id) {

        $sql = "SELECT * FROM orcamento_descontos WHERE orcamento_id = ".$orcamento_id." order by id desc limit 1";
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno;
    }

    public function buscaOrcamentosIndicadores($usuario_id)
    {
        
        $sql =  "SELECT e.*,o.emissao,o.validade, s.descricao as status, o.id as orcamento_id, o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id 
                    FROM        orcamentos o 
                    INNER JOIN  empresas e                      ON o.empresa_id = e.id 
                    INNER JOIN  status_orcamentos s             ON s.id = o.status_orcamento_id 
                    LEFT JOIN   orcamento_responsavel as orr    ON orr.orcamento_id = o.id 
                    LEFT JOIN   usuarios usu                    ON usu.id = orr.usuario_id 
                    WHERE o.solicitante_id = ".$usuario_id." or o.indicador_id = ".$usuario_id."
                    ORDER BY o.status_orcamento_id, orcamento_id ASC";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function insereOrcamentosDesconto($data){        

        if($this->db->insert('orcamento_descontos', $data)){ 
            return true;
        }else{
            return false;
        }

    }

    public function buscaProximosStatusOrcamento($orcamento_id){
        $sql = "SELECT s.id, s.descricao,s.ordem FROM status_orcamentos s 
                WHERE  s.ordem > (SELECT so.ordem FROM status_orcamentos so, orcamentos o WHERE so.id = o.status_orcamento_id and o.id = ".$orcamento_id.") order by s.ordem ASC";

        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno;
    }
    
    public function verificaOrcamento($cnpj){
        $sql        =   "SELECT count(*) as total FROM orcamentos o, empresas e
						  WHERE o.empresa_id = e.id   AND
								o.status_orcamento_id NOT IN (2,3,4,10,11,14) AND 
                                e.cnpj = '".$cnpj."'    ";
        // AND  DATEDIFF(date(now()),date(o.emissao)) <= 15
        $query      =   $this->db->query( $sql );
        $retorno    =   $query->row();
        return $retorno;
    }

    public function verificaOrcamentoId($cnpj){
        $sql        =   "SELECT o.id, o.status_orcamento_id FROM orcamentos o, empresas e
                          WHERE o.empresa_id = e.id   AND
                                o.status_orcamento_id NOT IN (2,3,4,10,11,14) AND 
                                e.cnpj = '".$cnpj."'    
                        ORDER BY o.emissao DESC";
        // AND  DATEDIFF(date(now()),date(o.emissao)) <= 15
        $query      =   $this->db->query( $sql );
        $retorno    =   $query->row_array();
        
        return $retorno;
    }

    public function verificaOrcamentoConcorrente($cnpj){

        $sql        =   "SELECT count(*) as total FROM orcamentos o, empresas e
                          WHERE o.empresa_id = e.id   AND
                                o.status_orcamento_id NOT IN (2,3,4) AND 
                                e.cnpj = '".$cnpj."' ";
        $query      =   $this->db->query(   $sql    );
        $retorno    =   $query->row();
        return $retorno;
    }

    public function verificaOrcamentoCliente($empresa_id){
        $sql        =   "SELECT count(*) as total FROM orcamentos o, empresas e WHERE o.empresa_id = e.id AND o.status_orcamento_id <> 2 AND o.empresa_id = '".$empresa_id."'";        
        $query      =   $this->db->query( $sql );
        $retorno    =   $query->row();
        return $retorno;
    }

    public function atualizaValorProduto($dados)
    {
         $update = array(
            'valor' => $dados['valor']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_produtos', $update)){
            return true;
        }else{
            return false;
        }
    }


    public function getResponsavel($orcamento_id){
        $sql = "SELECT  u.nome as responsavel, e.razao_social, u.id FROM orcamento_responsavel o, usuarios u, empresas e 
                where   e.id = u.empresa_id and o.usuario_id = u.id and o.orcamento_id=".$orcamento_id." 
                order by o.id desc LIMIT 1";
        $query      =   $this->db->query( $sql );
        $retorno    =   $query->row();
        return $retorno;
    }

    public function alterarProdutoOrcamento($dados)
    {
        $update = array(
            'produto_id' => $dados['produto_id']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_produtos', $update)){
            return true;
        }else{
            return false;
        }
    }
    // Alteraração de produtos via ajax no sistema dos representantes
    public function alterarProdutoOrcamentoInd3($dados)
    {
        $update = array(
            'produto_id' => $dados['produto_id'],
            'valor'     =>  $dados['valor']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_produtos', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function alterarQtdProduto($dados)
    {
         $update = array(
            'qtd' => $dados['qtd']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_produtos', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function alterarObservacao($dados)
    {         
        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamentos', $dados)){
            return true;
        }else{
            return false;
        }
    }

    public function excluirProduto($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('orcamento_produtos')){
            return true;
        }else{
            return false;
        }
    }

    public function alterarFormaPagto($dados)
    {
         $update = array(
            'forma_pagto_id' => $dados['forma_pagto_id']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamentos', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function alterarEntrega($dados)
    {
         $update = array(
            'entrega_id' => $dados['entrega_id']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamentos', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function alterarFrete($dados)
    {
         $update = array(
            'frete_id' => $dados['frete_id']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamentos', $update)){
            return true;
        }else{
            return false;
        }
    }

    public function getFreteEntregaPagto($orcamento_id)
    {
        $sql = "SELECT f.descricao as frete, p.descricao as forma_pagto, e.descricao as entrega 
                FROM orcamentos o 
                LEFT JOIN forma_pagto p on p.id = o.forma_pagto_id 
                LEFT JOIN entregas e on e.id = o.entrega_id 
                LEFT JOIN fretes f on f.id = o.frete_id WHERE o.id =".$orcamento_id;
        $query      =   $this->db->query( $sql );
        $retorno    =   $query->row();
        return $retorno;
    }

    public function selectTotalStatus(){
         $sql = "SELECT count(o.status_orcamento_id) as valor, s.descricao 
                    FROM status_orcamentos s, orcamentos o, status_orcamentos so 
                    WHERE s.id = o.status_orcamento_id and o.status_orcamento_id = so.id and extract(YEAR FROM o.emissao) = '".date('Y')."'
                    GROUP BY o.status_orcamento_id order by so.ordem asc";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".$dados['descricao']."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }

        if(count($retorno) > 0) {

            $return['valor'] = '['; 
            $return['valor'].= implode(',',$valor['valor']);
            $return['valor'].=']';
            $return['descricao'] = '['; 
            $return['descricao'].= implode(',',$descricao['descricao']);
            $return['descricao'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            $return['cores']    = $cores;
            return $return;
        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['descricao'] = '['; 
            $return['descricao'].= '';
            $return['descricao'].=']';
            $return['cor'] = '[';
            $return['cor'].= "'#ffcc00'";
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
        }
    }

    public function selectPedidoTotalStatus(){
         $sql = "SELECT     count(p.status_pedido_id) as valor, s.descricao 
                FROM        status_pedidos s, pedidos p 
                WHERE       s.id = p.status_pedido_id and extract(YEAR FROM p.dthr_geracao ) = '".date('Y')."'
                GROUP BY    p.status_pedido_id 
                ORDER BY    s.ordem ASC";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".str_replace("'", "",$dados['descricao'])."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['valor']);
        $return['valor'].=']';
        $return['descricao'] = '['; 
        $return['descricao'].= implode(',',$descricao['descricao']);
        $return['descricao'].=']';
        $return['cor'] = '[';
        $return['cor'].= implode(',',$cor);
        $return['cor'].=']';
        $return['dados']    = $retorno;
        $return['total']    = $total;
        $return['cores']    = $cores;
        return $return;
    }

    public function selectTotalStatusPeriodo($periodo){
        $data_subtraida = date('Y-m-d', strtotime('-'.$periodo.' days'));
        $sql = "SELECT count(o.status_orcamento_id) as valor, s.descricao 
                    FROM status_orcamentos s, orcamentos o, status_orcamentos so 
                    WHERE   s.id = o.status_orcamento_id    and 
                            o.status_orcamento_id = so.id   and 
                            o.emissao   between '".$data_subtraida."' and '".date('Y-m-d')."' GROUP BY o.status_orcamento_id order by so.ordem asc";
                 
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;
    }

    public function totalBombas(){
    
        $sql="  SELECT  sum(op.qtd) as total,  sum(op.valor*op.qtd) as valor_total
                FROM    orcamento_produtos op, orcamentos o,produtos p 
                WHERE   p.id = op.produto_id    and 
                        op.orcamento_id = o.id  and 
                        p.tipo_produto_id in (1,2,3,6)  and
                        o.status_orcamento_id = 2   and 
                        p.modelo != 'ML40L' and 
                        o.id  in (select oas.orcamento_id from orcamento_andamentos oas WHERE extract(YEAR FROM oas.dthr_andamento) = '".date('Y')."' and oas.status_orcamento_id = 2) ";
        
        $query = $this->db->query($sql);
        
        return $query->row_array();

    }

    public function totalBombasOpcionais(){
    
        $sql="  SELECT  sum(op.valor*op.qtd) as valor
                FROM    orcamento_produtos op, orcamentos o,produtos p 
                WHERE   p.id = op.produto_id    and 
                        op.orcamento_id = o.id  and 
                        p.tipo_produto_id in (1,2,3,4)  and
                        o.status_orcamento_id = 2   and 
                        o.id  in (select oas.orcamento_id from orcamento_andamentos oas WHERE extract(YEAR FROM oas.dthr_andamento) = '".date('Y')."' and oas.status_orcamento_id = 2) ";
        
        $query = $this->db->query($sql);
        
        return $query->row_array();

    }

    public function totalBombasPed(){
    
        $sql="  SELECT  sum(pi.qtd) as total,  sum(pi.valor*pi.qtd) as valor_total
                FROM    pedidos p, pedido_itens pi,produtos pr 
                WHERE   pr.id = pi.produto_id    and 
                        pi.pedido_id = p.id  and 
                        pr.tipo_produto_id in (1,2,3,6)  and
                        p.status_pedido_id != 7   and 
                        pr.modelo != 'ML40L' and 
                        extract(YEAR FROM p.dthr_geracao) = '".date('Y')."'";
        
        $query = $this->db->query($sql); 
        
        return $query->row_array();

    }

    public function totalBombasOpcionaisPed(){
    
        $sql=" SELECT   sum(pi.valor*pi.qtd) as valor
                FROM    pedidos p, pedido_itens pi,produtos pr 
                WHERE   pr.id = pi.produto_id    and 
                        pi.pedido_id = p.id  and 
                        pr.tipo_produto_id in (1,2,3,6)  and
                        p.status_pedido_id != 7   and                         
                        extract(YEAR FROM p.dthr_geracao) = '".date('Y')."'";
        
        $query = $this->db->query($sql);
        
        return $query->row_array();

    }

    public function totalFechadoMes(){
        $ano = date('Y');
        $sql="  SELECT  sum(op.valor*op.qtd) as total, extract(MONTH FROM oas.dthr_andamento) as mes 
                FROM    orcamento_produtos op, orcamentos o, (select oas.orcamento_id, max( oas.dthr_andamento ) as dthr_andamento from orcamento_andamentos oas WHERE extract(YEAR FROM oas.dthr_andamento) = '".$ano."' and oas.status_orcamento_id = 2 group by oas.orcamento_id) as oas 
                WHERE op.orcamento_id = o.id and o.status_orcamento_id = 2 and o.id in (select oas.orcamento_id from orcamento_andamentos oas WHERE extract(YEAR FROM oas.dthr_andamento) = '".$ano."' and oas.status_orcamento_id = 2) and oas.orcamento_id = o.id group by extract(MONTH FROM oas.dthr_andamento) ORDER BY mes ASC";    

        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();		
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $cor[] = $this->random_color();
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= 'Janeiro';
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= '#ffcc00';
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function totalFechadoMesAnterior(){
        $ano = date('Y')-1;
        $sql="  SELECT  sum(op.valor*op.qtd) as total, extract(MONTH FROM oas.dthr_andamento) as mes 
                FROM    orcamento_produtos op, orcamentos o, (select oas.orcamento_id, max( oas.dthr_andamento ) as dthr_andamento from orcamento_andamentos oas WHERE extract(YEAR FROM oas.dthr_andamento) = '".$ano."' and oas.status_orcamento_id = 2 group by oas.orcamento_id) as oas 
                WHERE op.orcamento_id = o.id and o.status_orcamento_id = 2 and o.id in (select oas.orcamento_id from orcamento_andamentos oas WHERE extract(YEAR FROM oas.dthr_andamento) = '".$ano."' and oas.status_orcamento_id = 2) and oas.orcamento_id = o.id group by extract(MONTH FROM oas.dthr_andamento) ORDER BY mes ASC";    

        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();     
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $cor[] = $this->random_color();
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= 'Janeiro';
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= '#ffcc00';
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function totalFechadoMesPed(){

        $sql="  SELECT  sum(pi.valor*pi.qtd) as total, extract(MONTH FROM p.dthr_geracao) as mes 
                FROM    pedidos p, pedido_itens pi, produtos pr
                WHERE p.id = pi.pedido_id and pi.produto_id = pr.id and extract(YEAR FROM p.dthr_geracao) = '".date('Y')."' and p.status_pedido_id != 7 group by extract(MONTH FROM p.dthr_geracao) ORDER BY mes ASC";    

        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();     
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $cor[] = $this->random_color();
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= 'Janeiro';
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= '#ffcc00';
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function totalAberto(){
        
        $sql    =   "  SELECT sum(op.valor*op.qtd) as valor, o.id from orcamento_produtos op, orcamentos o WHERE op.orcamento_id = o.id and o.status_orcamento_id in (1,5,6) GROUP by o.id ";    
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        $return['qtd'] = 0;       
        $return['total'] = 0;
        $qtd=1;
        foreach ($retorno as $dados) {
            $return['qtd']      =   $qtd++;
            $return['total']    =   $dados['valor'] + $return['total'];
        }               
        
        return $return;
    }

    public function produtosOrcados(){
        $sql    =   "   SELECT  count( op.produto_id ) as total, p.descricao, p.modelo, p.codigo
                        FROM    orcamento_produtos op, orcamentos o, produtos p
                        WHERE   op.orcamento_id = o.id and
                                    op.produto_id = p.id and 
                                    p.tipo_produto_id in (1,2,3) and 
                                    o.emissao between '".date('Y-m-d', strtotime('-1 year'))."' and '".date('Y-m-d')."'
                        group by    p.descricao
                        order by    total desc limit 5";
        
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['total'][] = $dados['total'];
            $descricao['descricao'][] = "'".$dados['descricao']."'";
            $total = $total+$dados['total'];
        }              

        $return['valor'] = '['; 
        $return['valor'].= implode(',',$valor['total']);
        $return['valor'].=']';
        $return['descricao'] = '['; 
        $return['descricao'].= implode(',',$descricao['descricao']);
        $return['descricao'].=']';
        $return['dados']    = $retorno;
        $return['total']    = $total;
        return $return;
    }

    public function buscaOrcamentosPorStatus($status){
        
        switch ($status) {
            case 'aberto':
                $orcamento_id = 1;
                break;
            case 'fechado':
                $orcamento_id = 2;
                break;
            case 'perdido outros':
                $orcamento_id = 3;
                break;    
            case 'cancelado':
                $orcamento_id = 4;
                break;
            case 'orçamento entregue':
                $orcamento_id = 5;
                break;    
            case 'or%C3%A7amento%20entregue' :          
                $orcamento_id = 5;
                break;
            case 'em negociação':
                $orcamento_id = 6;
                break;
            case 'perdido para wayne':
                $orcamento_id = 10;
                break;
            case 'perdido para gilbarco':
                $orcamento_id = 11;
                break;

        }

        $sql = " SELECT sum(op.qtd*op.valor) as valor_total, o.id, date_format(o.emissao,'%d/%m/%Y %h:%i:%s') as emissao, concat(e.razao_social,'-',e.cnpj) as cliente, e.estado, concat( i.razao_social,'-',i.cnpj ) as indicador, group_concat(p.modelo) as produtos
                        FROM orcamentos o 
                        INNER JOIN orcamento_produtos op ON o.id=op.orcamento_id
                        INNER JOIN produtos p ON p.id = op.produto_id                        
                        INNER JOIN empresas e ON o.empresa_id  = e.id 
                        LEFT JOIN empresas i ON o.indicador_id  = i.id 
                    where o.status_orcamento_id=".$orcamento_id." group by o.id";
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        return $retorno;
    }

    public function buscaOrcamentosFechadosMes($status){
        
        $sql = " SELECT  distinct   sum(op.qtd*op.valor) as valor_total, o.id, date_format(oa.dthr_andamento,'%d/%m/%Y %h:%i:%s') as dthr_andamento, concat(e.razao_social,'-',e.cnpj) as cliente, e.estado, concat( i.razao_social,'-',i.cnpj ) as indicador, o.emissao, group_concat(p.modelo) as produtos
                    FROM    orcamentos o  
                    INNER JOIN orcamento_produtos op ON o.id=op.orcamento_id
                    INNER JOIN produtos p ON p.id = op.produto_id
                    INNER JOIN (select oas.orcamento_id, max( oas.dthr_andamento ) as dthr_andamento, oas.status_orcamento_id from orcamento_andamentos oas WHERE extract(YEAR FROM oas.dthr_andamento) = '".date('Y')."' and oas.status_orcamento_id = 2 group by oas.orcamento_id) oa ON o.id = oa.orcamento_id
                    INNER JOIN empresas e ON o.empresa_id  = e.id 
                    LEFT JOIN empresas i ON o.indicador_id  = i.id
                    WHERE  extract(MONTH FROM oa.dthr_andamento) = '".$this->mes($status)."' and 
                            o.status_orcamento_id = 2
                             group by o.id ";
        //echo $sql;die;                                
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        return $retorno;
    }

    public function buscaOrcamentosInatividade(){

        $sql = "SELECT  DATEDIFF(CURDATE(),max(oa.dthr_andamento)) as dias_ultima_atu,oa.orcamento_id, concat(e.razao_social,' - ', e.cnpj) as cliente 
                FROM    orcamento_andamentos oa, orcamentos o, empresas e 
                WHERE   oa.orcamento_id = o.id and e.id = o.empresa_id 
                GROUP by oa.orcamento_id 
                ORDER BY dias_ultima_atu DESC LIMIT 10";
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        return $retorno;        
    }

    public function buscaOrcamentosPorStatusPeriodo($status, $periodo){
        $data_subtraida = date('Y-m-d', strtotime('-'.$periodo.' days'));        
        switch ($status) {
            case "'aberto'":
                $status_orcamento_id = 1;
                break;
            case "'fechado'":
                $status_orcamento_id = 2;
                break;
            case "'perdido'":
                $status_orcamento_id = 3;
                break;    
            case "'cancelado'":
                $status_orcamento_id = 4;
                break;
            case "'orçamento entregue'":
                $status_orcamento_id = 5;
                break;          
            case "'em negociação'":
                $status_orcamento_id = 6;
                break;
            case "'Reta Final'":
                $status_orcamento_id = 16;
                break; 
            case "'expirados'":
                $status_orcamento_id = 13;
                break; 
            case "'perdido outros'":
                $status_orcamento_id = 3;
                break; 
            case "'perdido para wayne'":
                $status_orcamento_id = 10;
                break; 
             case "'perdido para gilbarco'":
                $status_orcamento_id = 11;
                break; 
                          
        }

        $sql = " SELECT sum(op.qtd*op.valor) as valor_total, o.id, date_format(o.emissao,'%d/%m/%Y %h:%i:%s') as emissao 
                    FROM orcamentos o, orcamento_produtos op where o.id=op.orcamento_id and o.status_orcamento_id=".$status_orcamento_id." and DATE(o.emissao) between '".$data_subtraida."' and '".date('Y-m-d')."' group by o.id";
        
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        return $retorno;
    }

    public function atualizaIndicador($dados)
    {
        
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('orcamentos', $dados)){

            return true;
        }else{
            return false;
        }

    }
    /* ************ Comissao Representante ******************/
    public function atualizaComissao($dados)
    {
        $sql        =   "SELECT COALESCE(count(*),0) as total FROM orcamento_descontos WHERE orcamento_id=".$dados['orcamento_id'];
        $query      =   $this->db->query($sql);
        $resultado  =   $query->row_array();

        if( $resultado['total'] == 0 ){
            
            $inserir = array(
                'orcamento_id'      =>  $dados['orcamento_id'],
                'valor_desconto'    =>  $dados['valor_desconto']
            );

            if($this->db->insert('orcamento_descontos', $inserir)){
                return true;
            }else{
                return false;
            }
        }else{

            $update = array(
                'valor_desconto' => $dados['valor_desconto']
            );

            $this->db->where('orcamento_id', $dados['orcamento_id']);
            
            if($this->db->update('orcamento_descontos', $update)){

                return true;
            }else{
                return false;
            }

        }

    }
    
    public function alterarProdutoOrcRep($dados)
    {
        $update = array(
            'produto_id'    =>  $dados['produto_id'],
            'valor' =>  $dados['valor']
        );

        $this->db->where('id', $dados['id']);

        if($this->db->update('orcamento_produtos', $update)){
            return true;
        }else{
            return false;
        }
    }  

    public function buscaIndicador($id){

        $sql = "SELECT concat(u.nome, ' | ', e.razao_social ,' | ', e.cnpj) as indicador, e.telefone, u.email, u.id  FROM usuarios u, empresas e, orcamentos o WHERE u.empresa_id = e.id and o.indicador_id = u.id and o.id=".$id;
        
        $query = $this->db->query($sql);
        $retorno = $query->result();
        
        return $retorno;
    }

    public function retornaOrcamentos($term){

        $sql = "SELECT o.id, concat('#',o.id, ' | ', e.razao_social ,' | ', e.cnpj) as label FROM empresas e, orcamentos o WHERE o.empresa_id = e.id and concat(o.id,e.razao_social, e.cnpj) like '%".$term."%'";
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;

    }

    public function retornaOrcamentosFechados($term){

        $sql = "SELECT o.id, concat('#',o.id, ' | ', e.razao_social ,' | ', e.cnpj) as label, o.frete_id, o.observacao, e.estado FROM empresas e, orcamentos o WHERE o.empresa_id = e.id and concat(o.id,e.razao_social, e.cnpj) like '%".$term."%' and o.status_orcamento_id in (2)";
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;

    }

    public function retornaOrcamentosFechadosRepresentantes($term, $empresa_id){

        $sql = "SELECT  o.id, concat('#',o.id, ' | ', e.razao_social ,' | ', e.cnpj) as label, o.frete_id, o.observacao
                FROM    empresas e, orcamentos o, empresas sol, usuarios u
                WHERE   o.empresa_id = e.id         and
                        u.id    = o.solicitante_id  and
                        sol.id  = u.empresa_id      and 
                        concat(o.id,e.razao_social, e.cnpj) like '%%%%%' and
                        o.status_orcamento_id in (2) and
                        o.id not in (select orcamento_id from pedidos) and
                        sol.id = ".$empresa_id;
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;

    }

    public function retornaProdutosOrcamentos($id){

        $sql = "SELECT  op.*, p.descricao, p.tipo_produto_id, p.modelo, p.codigo, o.contato_posto,o.empresa_id, f.descricao as forma_pagto, p.nr_produtos, p.fl_arla 
                FROM    orcamentos o
                INNER JOIN  orcamento_produtos op ON o.id = op.orcamento_id
                INNER JOIN  produtos p ON p.id = op.produto_id
                LEFT JOIN   forma_pagto f ON o.forma_pagto_id = f.id
                WHERE o.id=".$id." 
                ORDER BY    p.tipo_produto_id ASC";
        
        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        
        return $retorno;
        
    }
    
    public function selectContatoPosto($id){

        $sql = "SELECT  DISTINCT u.id 
                FROM    orcamentos o, usuarios u 
                WHERE   u.nome  =   o.contato_posto and
                        o.id    =   ".$id;

        $query = $this->db->query($sql);

        $retorno = $query->row_array();

        return $retorno;
    }

    private function mes($mes){
        switch ($mes) {
            case 1:
                return 'Janeiro';
                break;
            case 2:
                return 'Fevereiro';
                break;    
            case 3:
                return 'Março';
                break;
            case 4:
                return 'Abril';
                break;    
            case 5:
                return 'Maio';
                break;    
            case 6:
                return 'Junho';
                break;       
            case 7:
                return 'Julho';
                break;         
            case 8:
                return 'Agosto';
                break;    
            case 9:
                return 'Setembro';
                break;        
            case 10:
                return 'Outubro';
                break;        
            case 11:
                return 'Novembro';
                break;        
            case 12:
                return 'Dezembro';
            case 'Janeiro':
                return '01';
            case 'Fevereiro':
                return '02';
            case 'Março':
                return '03';
            case 'Abril':
                return '04';
            case 'Maio':
                return '05';   
            case 'Junho':
                return '06';     
            case 'Julho':
                return '07';
            case 'Agosto':
                return '08';
            case 'Setembro':
                return '09';     
            case 'Outubro':
                return '10';
            case 'Novembro':
                return '11';
            case 'Dezembro':
                return '12';     
                break;                    
        }
    }

    private function random_color() {
        $letters = '0123456789ABCDEF';
        $color = '#';
        for($i = 0; $i < 6; $i++) {
            $index = rand(0,15);
            $color .= $letters[$index];
        }
        return $color;
    }

    public function excluirOrcamento($id){
        
        $this->db->where('orcamento_id', $id);
        if(    $this->db->delete('orcamentos_emissao') ){
            $this->db->where('id', $id);
            if(    $this->db->delete('orcamentos') ){
                return true;
            }else{
                return false;
            }
        }else{
           return false;
        }
    }

    public function getRelatorioOrcamentos($filtros){
        $where="1=1";
        
        if( $filtros['status_id'] != '' ){
            $where.=" and o.status_orcamento_id = ".$filtros['status_id'];
            
        }

        if( $filtros['dt_ini'] != '' && $filtros['dt_fim'] != ''){
            $where.=" and date(o.emissao) between '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."'";
            
        }

        if( $filtros['regiao'] != '' ){
            $where.=" and clientes.estado in ".$filtros['regiao'];
            
        }

        if( $filtros['estado'] != '' ){
            $where.=" and clientes.estado = '".$filtros['estado']."'";
        }
        $having = '';
        if( $filtros['usuario_id'] != '' ){
            //$where.=" and   clientes.estado in (select estado from zona_atuacao where usuario_id = ".$filtros['usuario_id'].")";
            $having =" HAVING consultor_resp_id ='".$filtros['usuario_id']."'";
        }

        if( $filtros['representante_id'] != ''){
            $where.=" and (o.solicitante_id =".$filtros['representante_id']." or indicador_id = ".$filtros['representante_id'].") ";
        }

        $sql = "SELECT  op.orcamento_id, sum((op.valor*op.qtd)) as valor_total, 
                        group_concat(concat(p.codigo,' - ',p.modelo,' ' )) as bombas, concat(clientes.cnpj,' - ',clientes.razao_social) as cliente,
                        clientes.cidade, clientes.estado,clientes.telefone, clientes.email,
                        o.origem, so.descricao as status, o.emissao, concat(emi.cnpj,' - ',emi.razao_social) as emp_emissor, upper(u.nome) as solicitante, sum(if(p.tipo_produto_id in (1,2,3,6), op.qtd,0)) as qtd_bombas,if( (select usuario_id from orcamento_responsavel orr where orr.orcamento_id = o.id ) = 7167, 'Cindiel Tavares', ( select lower(us.nome) from usuarios us inner join zona_atuacao zo on zo.usuario_id = us.id where zo.estado = clientes.estado )  ) as consultor_resp,
                        if( (select usuario_id from orcamento_responsavel orr where orr.orcamento_id = o.id ) = 7167, 7167, ( select zo.usuario_id from usuarios us inner join zona_atuacao zo on zo.usuario_id = us.id where zo.estado = clientes.estado )  ) as consultor_resp_id,
                        concat(indi.nome,' | ',indi_emp.razao_social) as indicador, concat(indi_emp.cidade,'/',indi_emp.estado  ) as indi_cidade, indi.telefone as indi_telefone,
                        contato.nome as contato, contato.telefone as contato_tel, contato.email as contato_email 
                FROM orcamentos o
                INNER JOIN orcamento_produtos op ON o.id    =   op.orcamento_id 
                INNER JOIN produtos p ON p.id    =   op.produto_id
                INNER JOIN empresas clientes ON clientes.id = o.empresa_id 
                INNER JOIN status_orcamentos so ON o.status_orcamento_id = so.id
                INNER JOIN usuarios u on u.id    =   o.solicitante_id 
                INNER JOIN empresas emi ON emi.id  =   u.empresa_id 
                LEFT JOIN usuarios contato on contato.id = o.contato_id  
                LEFT JOIN usuarios indi ON indi.id = o.indicador_id
                left join empresas indi_emp on indi_emp.id = indi.empresa_id
                WHERE ".$where."
                GROUP BY op.orcamento_id ".$having."                            
                order by 1 desc ";
        //echo $sql;die;
        $query = $this->db->query($sql);
        $retorno['orcamento'] = $query->result_array();    

        return $retorno;
    }

    public function buscaOrcamentosFeira()
    {
        
        $sql =  "SELECT e.*, o.emissao,o.validade, s.descricao as status,o.id as orcamento_id,
                        o.status_orcamento_id, usu.nome as responsavel, orr.id as responsavel_id, o.origem, usu_gera.nome as criado_por,o.contato_posto
                    FROM        orcamentos o 
                    INNER JOIN  empresas e                      ON  o.empresa_id = e.id 
                    INNER JOIN  status_orcamentos s             ON  s.id = o.status_orcamento_id 
                    LEFT JOIN   orcamento_responsavel as orr    ON  orr.orcamento_id = o.id 
                    LEFT JOIN   usuarios usu                    ON  usu.id = orr.usuario_id 
                    INNER JOIN  usuarios usu_gera               ON  o.solicitante_id = usu_gera.id
                    INNER JOIN  empresas emp_gera               ON  usu_gera.empresa_id = emp_gera.id
                    WHERE       usu_gera.tipo_cadastro_id  = 11 
                            and o.origem = 'Administrador Feira'
                    ORDER BY  o.id desc";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function buscaOrcamentosAprovacao(){
        $sql =  "SELECT o.id, concat(e.cnpj,'|',e.razao_social) as cliente, sum( op.valor*op.qtd ) as total
                    FROM        orcamentos o 
                    INNER JOIN  empresas e                      ON  o.empresa_id = e.id 
                    INNER JOIN  orcamento_produtos op           ON  op.orcamento_id = o.id
                    WHERE o.status_orcamento_id = 12
                    group by o.id 
                    ORDER BY o.id desc";

        $query = $this->db->query($sql);
        return $query->result_array();   
    }

    public function retornaFormaPagto($orcamento_id) {

        $sql = "SELECT f.* FROM forma_pagto f, orcamentos o WHERE f.id = o.forma_pagto_id and o.id = ".$orcamento_id;
        $query = $this->db->query($sql);
        $retorno = $query->row_array();
        return $retorno;

    }

    public function retornaDiferencaEmissao($orcamento_id) {

        $sql    =   "   SELECT  DATEDIFF(date(now()),date(max(o.dthr_emissao))) as dias, date_format(date(max(o.dthr_emissao)),'%d/%m/%Y') as dthr_emissao 
                        FROM    orcamentos_emissao o
                        WHERE   o.orcamento_id = ".$orcamento_id;

        $query  =   $this->db->query($sql);

        return  $query->row_array();

    }

    public function insereMotivoReemissao($data){

        if($this->db->insert('orcamentos_emissao', $data) ) {
            return true;
        }else{
            return false;
        }

    }

    public function orcamentoExpirados(){

        $sql    =   "   SELECT  oe.orcamento_id 
                        FROM    orcamentos o 
                        INNER JOIN (    SELECT  max(date(dthr_andamento)) as dthr_andamento, id, orcamento_id 
                                        FROM    orcamento_andamentos GROUP BY orcamento_id   ) oe ON o.id = oe.orcamento_id 
                        WHERE   o.status_orcamento_id not in (2,3,4,9,10,11,13,15) and DATEDIFF(date(now()),date(oe.dthr_andamento)) > 60";
        
        $query = $this->db->query($sql);
        
        return $query->result_array();

    }

    public function orcamentosAExpirar(){

        /*$sql = "SELECT  o.id, e.cnpj,e.razao_social, u.nome as responsavel, (60 - DATEDIFF(date(now()),date(o.emissao))) as dias, tc.descricao 
                FROM    orcamentos o, orcamento_responsavel orr, usuarios u, tipo_cadastros tc, empresas e 
                WHERE   e.id                = o.empresa_id  AND 
                        orr.orcamento_id    = o.id          AND
                        orr.usuario_id      = u.id          AND 
                        tc.id = u.tipo_cadastro_id          AND 
                        DATEDIFF(date(now()),date(o.emissao)) > 45 AND 
                        o.status_orcamento_id not in (2,3,4,9,10,11,13)";*/

         $sql    =   "  SELECT  o.id, e.cnpj,e.razao_social, u.nome as responsavel, (60 - DATEDIFF(date(now()),date(oe.dthr_emissao))) as dias, tc.descricao 
                        FROM    orcamentos o
                        INNER JOIN (    SELECT  max(date(dthr_emissao)) as dthr_emissao, id, orcamento_id 
                                        FROM    orcamentos_emissao GROUP BY orcamento_id   ) oe ON o.id = oe.orcamento_id 
                        INNER JOIN empresas e ON    e.id  = o.empresa_id
                        INNER JOIN orcamento_responsavel orr ON orr.orcamento_id    = o.id
                        INNER JOIN usuarios u ON    orr.usuario_id  = u.id
                        INNER JOIN tipo_cadastros tc ON tc.id = u.tipo_cadastro_id
                        WHERE   o.status_orcamento_id not in (2,3,4,9,10,11,13)     and 
                                DATEDIFF(date(now()),date(oe.dthr_emissao)) > 45    and 
                                DATEDIFF(date(now()),date(oe.dthr_emissao)) < 60  ORDER BY dias ASC";
        
        $query = $this->db->query($sql);
        
        return $query->result_array();

    }

    public function buscaOrcamentoById($orcamento_id){

        $sql = "SELECT concat(e.cnpj,' - ',e.razao_social) as cliente FROM orcamentos o, empresas e where o.empresa_id = e.id and o.id = ".$orcamento_id;

        $query = $this->db->query($sql);
        
        return $query->row_array();

    }

    public function primeiraEmissao($orcamento_id){
        $sql = "SELECT MIN(id) as id, date_format(dthr_emissao,'%d/%m/%Y %H:%i:%s') as dthr_emissao  FROM orcamentos_emissao WHERE orcamento_id = ".$orcamento_id;
        
        $query = $this->db->query($sql);

        return $query->row_array();
    }

    public function insereAlerta($data)
    {
        if($this->db->insert('orcamento_alertas', $data))
        {

            return $this->db->insert_id();
        }else{

            return false;
        }
    }

    public function retornaRepIndiAtivos(){
        
        $sql  = "SELECT distinct u.id, u.nome, concat(e.cnpj,'-',upper(e.razao_social)) as empresa, e.estado, upper(zu.nome) as zona, tc.descricao as tipo
                FROM usuarios u
                INNER JOIN empresas e       ON u.empresa_id = e.id
                INNER JOIN zona_atuacao z   ON e.estado = z.estado
                INNER JOIN usuarios zu      ON zu.id = z.usuario_id
                INNER JOIN tipo_cadastros tc ON tc.id = u.tipo_cadastro_id
                WHERE   u.tipo_cadastro_id  IN  (2,6,10,12)
                AND     u.ativo = 1 ";

        $query = $this->db->query($sql);

        return $query->result_array();
    
    }

    public function historicoNegocicoes($filtros)
    {
        $sql = "SELECT  o.id, 
                        date_format(o.emissao, '%d/%m/%Y') as emissao, 
                        p.id as pedido_id, 
                        date_format(p.dthr_geracao,'%d/%m/%Y') as dthr_geracao, 
                        format(sum(op.valor* op.qtd),2,'de_DE')  as valor, 
                        concat(e.cnpj,'-',e.razao_social) as cliente, 
                        so.descricao as orcamento_status,
                        sp.descricao as pedido_status,
                        if( p.status_pedido_id > 1 and  p.status_pedido_id <> 7,'Fechado','Aberto') as situacao
                FROM    orcamentos o
                inner join orcamento_produtos op on op.orcamento_id = o.id
                inner join empresas e on e.id = o.empresa_id
                inner join status_orcamentos so on o.status_orcamento_id = so.id
                left join pedidos p on p.orcamento_id = o.id
                left join status_pedidos sp on sp.id = p.status_pedido_id
                where ".$filtros."                
                group BY o.id";
        
        return $this->db->query($sql)->result_array();
    }

    public function buscaClienteOrcamento($orcamento_id)
    {
        $sql = "SELECT  concat(e.cnpj,' | ', e.razao_social) as cliente, e.cidade, e.estado 
                FROM    empresas e, orcamentos o
                WHERE   e.id = o.empresa_id and 
                        o.id=".$orcamento_id;

        return $this->db->query($sql)->row_array();
    }

    public function selectDatasOrcamentos(){

        $sql    =   "   SELECT o.id, o.dt_previsao_prod, sum(p.nr_bicos*op.qtd) as bicos, sum(op.qtd) as bombas, o.emissao , e.razao_social,
                                o.status_orcamento_id, s.descricao as status, 'orçamento' as tipo 
                        FROM orcamentos o
                        INNER JOIN orcamento_produtos op on o.id = op.orcamento_id
                        INNER JOIN produtos p on p.id = op.produto_id
                        INNER JOIN empresas e on o.empresa_id = e.id 
                        inner join status_orcamentos s on s.id = o.status_orcamento_id
                        WHERE o.status_orcamento_id in (6) and p.tipo_produto_id in (1,2,3,6)
                        GROUP BY o.id 
                        union 
                        SELECT o.id, o.dt_previsao_prod, sum(p.nr_bicos*op.qtd) as bicos, sum(op.qtd) as bombas, o.emissao , e.razao_social,
                                pe.status_pedido_id, s.descricao as status, 'pedido' as tipo 
                        FROM orcamentos o
                        inner join pedidos pe on o.id = pe.orcamento_id
                        INNER JOIN pedido_itens op on pe.id = op.pedido_id
                        INNER JOIN produtos p on p.id = op.produto_id
                        INNER JOIN empresas e on o.empresa_id = e.id 
                        inner join status_pedidos s on s.id = pe.status_pedido_id
                        WHERE pe.status_pedido_id in (1,9,17) and p.tipo_produto_id in (1,2,3,6)
                        GROUP BY pe.id                        
                        ORDER BY 2 ASC";
                    //    echo $sql;die;
        $query  =   $this->db->query($sql);
        $datas  =   $query->result_array();
        $dataPedidos = array();

        foreach($datas as $data){
            if( $data['dt_previsao_prod'] == '0000-00-00' ){
                $update = array( 'id' => $data['id'],
                                'dt_previsao_prod' => date('Y-m-d',strtotime($data['emissao'].' +68 days')) );
                
                $this->atualizaOrcamento($update);
            }

            $key                 =  date('W',strtotime($data['dt_previsao_prod'])) .'*'. date('Y',strtotime($data['dt_previsao_prod'])) .'*'.date("d/m/Y", strtotime('monday this week', strtotime($data['dt_previsao_prod']))).'*'.date("Y-m-d", strtotime('monday this week', strtotime($data['dt_previsao_prod'])));            
            $dataPedidos[$key][] =  array(  'id'            =>  $data['id'],
                                            'razao_social'  =>  $data['razao_social'],
                                            'bicos'         =>  $data['bicos'],
                                            'bombas'        =>  $data['bombas'],
                                            'status_id'     =>  $data['status_orcamento_id'],
                                            'status'        =>  $data['status'],
                                            'tipo'          =>  $data['tipo'],
                                            'dt_previsao_prod'   =>  $data['dt_previsao_prod']);

        }       
        return $dataPedidos;
    }

    public function buscaInfo($orcamento_id){
        
        $sql="  SELECT  concat(e.cnpj,'-',e.razao_social) as cliente, concat(e.cidade,'/',e.estado) as cidade, p.modelo, sum(if(p.tipo_produto_id in (1,2,3,6),op.qtd,0)) qtd, p.codigo, s.descricao as status, s.id as status_id, sum(p.nr_bicos*op.qtd) as bicos, o.id as orcamento_id, sp.descricao as status_pedido, pe.id as pedido_id
                FROM    orcamentos o
                inner join empresas e on  o.empresa_id    =   e.id
                inner join orcamento_produtos op on o.id = op.orcamento_id
                inner join produtos p on p.id = op.produto_id
                inner join  status_orcamentos s on o.status_orcamento_id = s.id
                left join pedidos pe on pe.orcamento_id = o.id
                left join status_pedidos sp on sp.id = pe.status_pedido_id
                WHERE   o.id    =   ".$orcamento_id." and
                        p.tipo_produto_id in (1,2,3,6)
                GROUP by o.id, op.produto_id";
        
        $query  =   $this->db->query($sql);
        
        return  $query->result_array();

    } 

    public function buscaTotalBombasBicosSemana($data)
    {
       
        $sql = "SELECT  sum(if(p.tipo_produto_id in (1,2,3,6),op.qtd,0)) bombas, sum(p.nr_bicos*op.qtd) as bicos, sum(op.qtd*op.valor) as valor FROM orcamentos o
                inner join  orcamento_produtos op on o.id = op.orcamento_id 
                inner join produtos p on p.id = op.produto_id
                where o.dt_previsao_prod between '".$data."' and '".date('Y-m-d', strtotime('+ 5 days ', strtotime($data)))."' ";

        return $this->db->query($sql)->row_array();

    }

    public function buscaTotalOrcamentosAbertosDias($periodo_ini,$periodo_fim)
    {
        $sql = "SELECT count(*) as total 
                FROM orcamentos o                 
                WHERE o.status_orcamento_id = 1 
                 and  (DATEDIFF('".date('Y-m-d')."', o.emissao) >= ".$periodo_ini." and DATEDIFF('".date('Y-m-d')."', o.emissao) <= ".$periodo_fim.")";
        
        return $this->db->query($sql)->row_array();        
    
    }

    public function buscaOrcamentosAbertosDias($periodo_ini,$periodo_fim)
    {
        $sql = "SELECT o.id, concat(e.cnpj,'-',e.razao_social) as cliente, DATEDIFF('".date('Y-m-d')."', o.emissao) as dias, format(sum(op.qtd*op.valor),2,'de_DE') as total, sum(op.qtd*op.valor) as valor, concat(date_format(o.emissao,'%d/%m/%Y'), '-', DATEDIFF('".date('Y-m-d')."', o.emissao), ' dias') as emissao  
                FROM orcamentos o 
                INNER JOIN orcamento_produtos op ON op.orcamento_id = o.id 
                INNER JOIN empresas e ON e.id = o.empresa_id 
                WHERE o.status_orcamento_id = 1 and (DATEDIFF('".date('Y-m-d')."', o.emissao) >= ".$periodo_ini." and DATEDIFF('".date('Y-m-d')."', o.emissao) <= ".$periodo_fim.")
                GROUP BY o.id
                ORDER BY 3 DESC";
        
        return $this->db->query($sql)->result_array();        
    
    }

    public function buscaTotalOrcamentosNegociacaoDias($periodo_ini,$periodo_fim)
    {    
        $sql = "SELECT  count(*) as total
                FROM    orcamentos o                 
                INNER JOIN (select oa.orcamento_id, oa.status_orcamento_id, min(oa.dthr_andamento) as dthr_andamento from orcamento_andamentos oa 
                WHERE   oa.status_orcamento_id = 6 group by oa.orcamento_id) as oas ON oas.orcamento_id = o.id 
                WHERE o.status_orcamento_id = 6 and (DATEDIFF('".date('Y-m-d')."', o.emissao) >= ".$periodo_ini." and DATEDIFF('".date('Y-m-d')."', o.emissao) <= ".$periodo_fim.")";

        return $this->db->query($sql)->row_array();                
    }

    public function buscaOrcamentosNegociacaoDias($periodo_ini,$periodo_fim)
    {    
        $sql = "SELECT  o.id, concat(e.cnpj,'-',e.razao_social) as cliente, DATEDIFF('".date('Y-m-d')."', oas.dthr_andamento) as dias, 
                        format(sum(op.qtd*op.valor),2,'de_DE') as total, sum(op.qtd*op.valor) as valor, concat(date_format(oas.dthr_andamento,'%d/%m/%Y'), '-', DATEDIFF('".date('Y-m-d')."', o.emissao), ' dias') as emissao 
                FROM    orcamentos o 
                INNER JOIN orcamento_produtos op ON op.orcamento_id = o.id 
                INNER JOIN empresas e ON e.id = o.empresa_id 
                INNER JOIN (select oa.orcamento_id, oa.status_orcamento_id, min(oa.dthr_andamento) as dthr_andamento from orcamento_andamentos oa 
                WHERE   oa.status_orcamento_id = 6 group by oa.orcamento_id) as oas ON oas.orcamento_id = o.id 
                WHERE o.status_orcamento_id = 6 and (DATEDIFF('".date('Y-m-d')."', o.emissao) >= ".$periodo_ini." and DATEDIFF('".date('Y-m-d')."', o.emissao) <= ".$periodo_fim.")
                GROUP BY o.id
                ORDER BY 3 DESC";
               
        return $this->db->query($sql)->result_array();                
    }

    public function buscaTotalOrcamentosFechadosDias($periodo_ini,$periodo_fim)
    {    
        $sql = "SELECT  count(*) as total 
                FROM    orcamentos o                 
                INNER JOIN (select oa.orcamento_id, oa.status_orcamento_id, min(oa.dthr_andamento) as dthr_andamento from orcamento_andamentos oa WHERE   oa.status_orcamento_id = 2 group by oa.orcamento_id) as oas ON oas.orcamento_id = o.id 
                WHERE o.status_orcamento_id = 2
                        and (DATEDIFF('".date('Y-m-d')."', o.emissao) >= ".$periodo_ini." and DATEDIFF('".date('Y-m-d')."', o.emissao) <= ".$periodo_fim.") and o.id not in (SELECT orcamento_id FROM pedidos WHERE status_pedido_id not in (1,9,17,7))";

        return $this->db->query($sql)->row_array();                
    }

    public function buscaOrcamentosFechadosDias($periodo_ini,$periodo_fim)
    {    
        $sql = "SELECT  o.id, concat(e.cnpj,'-',e.razao_social) as cliente, DATEDIFF('".date('Y-m-d')."', oas.dthr_andamento) as dias, 
                        format(sum(op.qtd*op.valor),2,'de_DE') as total, sum(op.qtd*op.valor) as valor, concat(date_format(oas.dthr_andamento,'%d/%m/%Y'), '-', DATEDIFF('".date('Y-m-d')."', o.emissao), ' dias') as emissao  
                FROM    orcamentos o 
                INNER JOIN orcamento_produtos op ON op.orcamento_id = o.id 
                INNER JOIN empresas e ON e.id = o.empresa_id 
                INNER JOIN (select oa.orcamento_id, oa.status_orcamento_id, min(oa.dthr_andamento) as dthr_andamento from orcamento_andamentos oa WHERE   oa.status_orcamento_id = 2 group by oa.orcamento_id) as oas ON oas.orcamento_id = o.id 
                WHERE o.status_orcamento_id = 2
                        and (DATEDIFF('".date('Y-m-d')."', o.emissao) >= ".$periodo_ini." and DATEDIFF('".date('Y-m-d')."', o.emissao) <= ".$periodo_fim.") and o.id not in (SELECT orcamento_id FROM pedidos WHERE status_pedido_id not in (1,9,17,7))
                GROUP BY o.id
                ORDER BY 3 DESC ";

        return $this->db->query($sql)->result_array();                
    }

    public function funilVendasPeriodo($mes, $ano)
    {
        $sql = "SELECT (SELECT count(o.id) as fechados from orcamentos o 
                        where EXTRACT(YEAR_MONTH FROM o.emissao) = '".$ano.$mes."') as abertos,
                        (SELECT count(o.id) as negociacao 
                            from orcamentos o 
                                where EXTRACT(YEAR_MONTH FROM o.emissao) = '".$ano.$mes."' and o.id in (select oa.orcamento_id from orcamento_andamentos oa where oa.status_orcamento_id in (2,6) and extract(YEAR_MONTH FROM oa.dthr_andamento ) = '".$ano.$mes."')
                         or (o.id in (select p.orcamento_id from pedidos p 
                                          inner join pedido_andamentos pa on pa.pedido_id = p.id 
                                          WHERE pa.status_pedido_id in (1,9,17,7) and pa.status_pedido_id not in (2,16,11,3,4,18,19,12,13,14,20,15,21,5,6,8) AND EXTRACT(YEAR_MONTH FROM pa.dthr_andamento) = '".$ano.$mes."' GROUP BY pa.pedido_id))) as negociacao,
                        (SELECT COUNT(distinct p.orcamento_id) as fechados
                            FROM pedidos p
                            INNER JOIN orcamentos orc on orc.id = p.orcamento_id
                            WHERE EXTRACT(YEAR_MONTH FROM orc.emissao) = '".$ano.$mes."'
                                and p.id in (select distinct oa.pedido_id from pedido_andamentos oa WHERE extract(YEAR_MONTH FROM oa.dthr_andamento) = '".$ano.$mes."' and oa.status_pedido_id in (2,16)) ) as fechados
                        from DUAL";

        return $this->db->query($sql)->row_array();
    }

    public function funilVendasPeriodoOrcamento($mes, $ano)
    {
        $sql = "SELECT (SELECT count(o.id) as fechados from orcamentos o 
                        where EXTRACT(YEAR_MONTH FROM o.emissao) = '".$ano.$mes."') as abertos,
                        (SELECT count(DISTINCT o.id) as negociacao from orcamentos o 
                            WHERE EXTRACT(YEAR_MONTH FROM o.emissao) = '".$ano.$mes."' and o.id in (select distinct orcamento_id from orcamento_andamentos where extract(YEAR_MONTH FROM dthr_andamento) = '".$ano.$mes."' AND status_orcamento_id = 6 and status_orcamento_id not in (2) ))
                          as negociacao,
                        (SELECT COUNT(distinct o.id) as fechados
                            FROM orcamentos o 
                            WHERE EXTRACT(YEAR_MONTH FROM o.emissao) = '".$ano.$mes."'
                                and o.id in (select distinct oa.orcamento_id from orcamento_andamentos oa WHERE extract(YEAR_MONTH FROM dthr_andamento) = '".$ano.$mes."' and oa.status_orcamento_id in (2))) as fechados
                        from DUAL";
        
        return $this->db->query($sql)->row_array();
    }

    public function funilVendasPeriodoPedidos($mes, $ano)
    {
        $sql = "SELECT (SELECT count(p.id) as abertos from pedidos p
                        where EXTRACT(YEAR_MONTH FROM p.dthr_geracao) = '".$ano.$mes."') as abertos,
                        (SELECT count(DISTINCT p.id) as negociacao from pedidos p
                            WHERE EXTRACT(YEAR_MONTH FROM p.dthr_geracao) = '".$ano.$mes."' and p.id in (select distinct pedido_id from pedido_andamentos where extract(YEAR_MONTH FROM pedido_andamentos.dthr_andamento) = '".$ano.$mes."' AND status_pedido_id in (9,17,7) AND status_pedido_id not in (2,16,11,3,4,18,19,12,13,14,20,15,21,5,6,8) ))
                          as negociacao,
                        (SELECT COUNT(distinct p.id) as fechados
                            FROM pedidos p
                            WHERE EXTRACT(YEAR_MONTH FROM p.dthr_geracao) = '".$ano.$mes."'
                                and p.id in (select distinct oa.pedido_id from pedido_andamentos oa WHERE extract(YEAR_MONTH FROM oa.dthr_andamento) = '".$ano.$mes."' and oa.status_pedido_id in (2,16))) as fechados
                        from DUAL";
        
        return $this->db->query($sql)->row_array();
    }

    public function buscaOrcamentoZona($orcamento_id)
    {
        $sql = "SELECT distinct z.usuario_id FROM orcamentos o 
                    inner join empresas e on e.id = o.empresa_id
                    inner join zona_atuacao z on z.estado = e.estado 
                    where o.id = ". $orcamento_id;
                    
        return $this->db->query($sql)->row_array();

    }

    public function buscaOrcamentosPorCnpj($cnpj)
    {
        $sql = "SELECT o.*, concat(e.cnpj,' - ',e.razao_social) as cliente, so.descricao as status, u.nome as criado_por 
                FROM orcamentos o 
                INNER JOIN empresas e ON e.id = o.empresa_id
                INNER JOIN status_orcamentos so ON so.id = o.status_orcamento_id
                INNER JOIN usuarios u ON u.id = o.usuario_id 
                WHERE e.cnpj = '".$cnpj."' ";

        return $this->db->query($sql)->row();
    }

    public function geraRankingParceiros($filtros)
    {
        $where = "";

        if( $filtros['dt_ini'] != ''){            
            $where.=" and o.emissao between '". $filtros['dt_ini']."' and '". $filtros['dt_fim']."'";
        }

        if($filtros['produto_id'] != ''){
            $where.=" and p.id = ".$filtros['produto_id'];
        }

        if($filtros['parceiro_id'] != ''){
            $where.=" and e.id = ".$filtros['parceiro_id'];
        }elseif($filtros['tp_parceiro'] == 'i' ){
            $where.=" and u.tipo_cadastro_id in (4,6,10)";
        }else{
            $where.=" and u.tipo_cadastro_id in (2,12)";
        } 

        $sql ="SELECT distinct concat(e.cnpj,'-',upper(e.razao_social)) AS empresa, e.estado, t.descricao, count(DISTINCT o.id) AS total, sum(CASE WHEN o.status_orcamento_id = 2 THEN 1 ELSE 0 END) AS fechados, sum(if(p.tipo_produto_id in (1,2,3,6),op.qtd,0)) as total_bombas, sum(if(p.tipo_produto_id in (1,2,3,6) and o.status_orcamento_id = 2,op.qtd,0)) as bombas_fechadas, format(sum( op.qtd*op.valor),2,'DE_de') as valor_total, format(sum( if(o.status_orcamento_id = 2, op.qtd*op.valor,0)),2,'DE_de') as valor_fechado, sum( if(o.status_orcamento_id = 2, op.qtd*op.valor,0)) as valor_s, format(sum(if(p.tipo_produto_id in (1,2,3,6),op.qtd*op.valor,0)),2,'DE_de') as valor_bombas
                FROM orcamentos o
                inner join orcamento_produtos op on op.orcamento_id = o.id
                inner join produtos p on op.produto_id = p.id
                inner JOIN usuarios u on o.indicador_id = u.id
                inner JOIN empresas e on e.id = u.empresa_id
                INNER JOIN tipo_cadastros t on u.tipo_cadastro_id = t.id
                WHERE u.empresa_id <> 2 ".$where." 
                group by e.id  
            ORDER BY valor_s DESC";
        
        return $this->db->query($sql)->result_array();
    }

    public function OrcamentoExpiradosCancelados()
    {
        $sql = "SELECT  oe.orcamento_id 
                FROM    orcamentos o 
                INNER JOIN (    SELECT  max(date(dthr_andamento)) as dthr_andamento, id, orcamento_id 
                                FROM    orcamento_andamentos GROUP BY orcamento_id   ) oe ON o.id = oe.orcamento_id 
                WHERE   o.status_orcamento_id in (13) and DATEDIFF(date(now()),date(oe.dthr_andamento)) > 90  ";

        return $this->db->query($sql)->result_array();
    }
}
?>