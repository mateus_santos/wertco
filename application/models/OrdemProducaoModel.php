<?php

class OrdemProducaoModel extends CI_Model {
	
	public function find($id)
	{
		$this->db->select('op.*');
		$this->db->select('itens.descricao item');
		$this->db->where('op.id', $id);
		$this->db->join('itens', 'op.item_id = itens.id');
		return $this->db->get("ordem_producao op")->row_array();
	}
		
	
	public function selectOps()
    {
        $sql = "SELECT o.*, s.descricao as status FROM ordem_producao o, status_op s, itens i
				where i.id = o.item_id and o.status_op_id = s.id and o.status_op_id <> 3 and o.dthr_geracao between '".date('Y-m-d', strtotime('-90 days', strtotime(date('Y-m-d'))))."' and '".date('Y-m-d')."'";                
        $query = $this->db->query($sql);
        return $query->result_array();

    }

	
	public function insereOp ($data) {
        
		return $this->db->insert('ordem_producao', $data);
	}

   
    public function verificaOp($sql)
    {
    	
    	$query = $this->db->query($sql);
    	return $query->row_array();

    }

    public function verificaQtd($id)
    {
        $sql = "SELECT qtd,observacao,combustivel FROM ordem_producao WHERE id = ".$id;
        $query = $this->db->query($sql);
        return $query->row_array();

    }
    
    public function excluirOrdemProducao($id,$motivo){
        
        $this->db->where('id', $id);

        $update = array(    'status_op_id'          =>  4,
                            'motivo_cancelamento'   =>  $motivo    ) ;

        if($this->db->update('ordem_producao',  $update)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizaQtdObs($dados)
    {
        $update = array('qtd' 	        =>	$dados['qtd'],
                        'observacao'    =>  $dados['observacao'],
                        'combustivel'   =>  $dados['combustivel']);
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('ordem_producao', $update)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizarOrdemProducao($dados)
    { 
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('ordem_producao', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function verificaOpFechadaPorPedido($pedido_id){
        
        $sql = "SELECT count(*) AS total FROM ordem_producao WHERE status_op_id != 3 and pedido_id=".$pedido_id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getOpsByPedido($pedido_id){
        
        $sql = "SELECT o.*, p.arquivo as upload_bb FROM ordem_producao o, pedidos p WHERE o.pedido_id = p.id and o.pedido_id=".$pedido_id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function buscaOpNrSerie( $op_id, $nr_serie )
    {
        $sql = "SELECT      op.descricao, if( b.combustivel = '', op.combustivel, b.combustivel ) as combustivel, op.informacoes 
                FROM        bombas_nr_serie b
                LEFT JOIN   ordem_producao op on b.ordem_producao_id = op.id 
                WHERE       op.id = ".$op_id." and b.id =". $nr_serie;

        return $this->db->query($sql)->row_array();
    }

    public function buscaPedidoOpPorNrSerie( $nr_serie ){
        $sql = "SELECT  b.id as nr_serie,
                        concat(e.razao_social,'-',e.cnpj) as cliente, 
                        b.codigo, 
                        concat(b.ordem_producao_id,'/',b.pedido_id) as pedido, 
                        b.modelo, 
                        p.pintura, 
                        b.combustivel
                FROM    bombas_nr_serie b
                INNER JOIN pedidos p            ON  p.id = b.pedido_id
                INNER JOIN ordem_producao op    ON  b.ordem_producao_id = op.id 
                INNER JOIN orcamentos o         ON  p.orcamento_id = o.id 
                INNER JOIN empresas e           ON  e.id = o.empresa_id 
                WHERE   b.id = ".$nr_serie;

        return $this->db->query($sql)->row_array();
    }

    public function atualizaOpsAvulsas()
    {
        $sql = "UPDATE ordem_producao SET status_op_id = 3 WHERE  date(dthr_geracao) < '".date('Y-m-d',strtotime('-10 days', strtotime(date('Y-m-d'))))."' and status_op_id <> 3 and pedido_id = 0;";        
        

        return $this->db->query($sql);

    }
  
}
?>