<?php

class CarteirasEquipeModel extends CI_Model {
	
	public function adicionar($data){
		
		$this->db->insert('carteiras_equipe', $data);
		return $this->db->insert_id();
	}

	public function buscaIndicadoresDisponiveis(){
        $sql = "	SELECT u.*, concat(e.cnpj,'-', e.razao_social) as empresa 
                    FROM usuarios u 
                    INNER JOIN empresas e on e.id = u.empresa_id                    
        			WHERE u.tipo_cadastro_id in (6,10) and u.ativo = 1 and  u.id not in (select indicador_id from carteiras_equipe)
                    ORDER BY u.nome ASC";
        
        $query  =   $this->db->query($sql);       

        return $query->result_array();
    }

    public function select(){
    	$sql = "	SELECT 	c.id, c.equipe_id, u.nome, group_concat(i.nome) as indicadores
    				FROM    carteiras_equipe c
                    inner join usuarios u on u.id = c.equipe_id
                    inner join usuarios i on i.id = c.indicador_id    					
    				GROUP BY c.equipe_id ";

    	$query  =   $this->db->query($sql);       

        return $query->result_array();
    }

    public function buscaIndicadoresUsuarios($usuario_id){
        $sql = "	SELECT c.indicador_id, u.nome, concat(e.cnpj,'-', e.razao_social) as empresa 
                    FROM carteiras_equipe c 
                    INNER JOIN usuarios u on c.indicador_id = u.id 
                    INNER JOIN empresas e on e.id = u.empresa_id                    
        			WHERE 	c.equipe_id = ".$usuario_id;
        //echo $sql;die;
        $query  =   $this->db->query($sql);       

        return $query->result_array();
    }

    public function delete($equipe_id){ 
        
        $this->db->where('equipe_id', $equipe_id);
        if(    $this->db->delete('carteiras_equipe') ){
            return true;
        }else{
            return false;
        }
    }

    public function buscarEmailResp($estado){
    	$sql = "	SELECT u.email, u.nome FROM orcamentos o, empresas e, zona_atuacao z, usuarios u
    				WHERE 	o.empresa_id = e.id and 
    						z.estado = e.estado and 
    						z.usuario_id = u.id and 
    						z.estado = '".$estado."'";
    	$query  =   $this->db->query($sql);       

        return $query->row_array();

    }

    public function buscaConsultorWertco(){
        
        $sql = "SELECT  DISTINCT z.usuario_id, u.nome 
                FROM    usuarios u, zona_atuacao z 
                WHERE   u.id = z.usuario_id ";
        
        $query  =   $this->db->query($sql);  
        
        return $query->result_array();

    }

    public function getRelatorioRepresentantes($filtros){
        
        $where = "1=1";
               
        if( $filtros['usuario_id'] != '' ){
            $where.= " and z.usuario_id =".$filtros['usuario_id'];
        }

        if( $filtros['dt_fim'] != '' ){
            $where.= " and o.emissao BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."'";
        }

        $sql = "SELECT u.id, upper(u.nome) AS representante, concat(e.cnpj,'-',upper(e.razao_social)) AS empresa, e.estado, count(o.id) AS total, sum(CASE WHEN o.status_orcamento_id = 2 THEN 1 ELSE 0 END) AS fechados, upper(con.nome) AS zona
                FROM    usuarios u
                INNER JOIN  empresas e ON u.empresa_id = e.id
                LEFT  JOIN  (select orr.usuario_id, o.id, o.status_orcamento_id, o.emissao FROM orcamentos o, orcamento_responsavel orr WHERE o.id = orr.orcamento_id) o ON o.usuario_id = u.id
                INNER JOIN  zona_atuacao z  ON  z.estado = e.estado
                INNER JOIN  usuarios con    ON  z.usuario_id = con.id
                WHERE       u.tipo_cadastro_id = 2 AND u.ativo = 1 and ".$where."
                GROUP BY    u.id
                ORDER BY    zona ASC, empresa ASC ";
        
        $query = $this->db->query($sql);
        $retorno['orcamento'] = $query->result_array();
        
        return $retorno;
    
    }

}

?>