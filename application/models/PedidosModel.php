<?php

class PedidosModel extends CI_Model {

	public function add ($data) {

	    return $this->db->insert('pedidos', $data);
	}    

    public function selectPedidos($tipo=1){

        $where = ($tipo == 1) ? ' and p.status_pedido_id not in (6,8,7)' : ' and p.status_pedido_id in (6,8,7)';

        $sql =  "SELECT     p.*, concat(e.id,' | ',e.cnpj, ' | ', e.razao_social) as cliente, s.descricao as status, e.email, o.contato_posto, o.empresa_id, o.id as orcamento_id, s.setor_id, s.ordem, e.fl_inadimplencia   
                    FROM    pedidos p, empresas e, orcamentos o, status_pedidos s 
                    WHERE   o.empresa_id = e.id     and 
                            p.orcamento_id = o.id   and 
                            p.status_pedido_id = s.id" . $where;
        
        $query = $this->db->query($sql);
        
        return $query->result();   
    }

    public function selectPedidosPorEmpresa($empresa_id){

        $sql =  "SELECT     p.*, concat(e.id,' | ',e.cnpj, ' | ', e.razao_social) as cliente, s.descricao as status, e.email, o.contato_posto, o.empresa_id, o.id as orcamento_id   
                    FROM    pedidos p, empresas e, orcamentos o, status_pedidos s 
                    WHERE   o.empresa_id = e.id     and 
                            p.orcamento_id = o.id   and 
                            p.status_pedido_id = s.id and
                            o.empresa_id = e.id and 
                            e.id = ".$empresa_id;
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function getPedidosById($id){

        $sql =  "SELECT     p.*, concat(e.cnpj,' | ',e.razao_social) as cliente, concat(u.nome,' | ',t.cnpj,' | ', t.razao_social) as tecnico, e.email, o.empresa_id, sp.ordem, p.pintura  
                    FROM    pedidos p 
                INNER JOIN  orcamentos o        ON  p.orcamento_id = o.id 
                INNER JOIN  empresas e          ON  o.empresa_id = e.id 
                LEFT JOIN   usuarios u          ON  u.id = p.tecnico_id 
                LEFT JOIN   empresas t          ON  u.empresa_id = t.id 
                LEFT JOIN   status_pedidos sp   ON  p.status_pedido_id = sp.id
                WHERE       p.id = ".$id;
                           
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function excluirPedidos($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('pedidos')){
            return true;
        }else{
            return false;
        }
    }

    public function excluirPedidoItens($id){
        
        $this->db->where('pedido_id', $id);
        if($this->db->delete('pedido_itens')){
            return true;
        }else{
            return false;
        }
    }

    public function excluirPedidoFormPagto($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('pedido_form_pag')){
            return true;
        }else{
            return false;
        }
    }

    public function excluirFormPagtoPorPedido($pedido_id){
        
        $this->db->where('pedido_id', $pedido_id);
        if($this->db->delete('pedido_form_pag')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaPedidos($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('pedidos', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizaStatusPedido($dados)
    {
                        
        $this->db->where('id', $dados['id']);

        if($this->db->update('pedidos', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function pedidosEmpresasPdf($id){

        $sql =  "SELECT     p.*, e.cnpj,e.razao_social, e.insc_estadual, e.telefone, e.endereco,e.bairro,e.cep, e.email,e.cidade, e.estado,
							concat(u.nome,' | ',t.cnpj,' | ', t.razao_social) as tecnico,
							f.descricao as frete, concat(i.nome,' | ',indi_emp.cnpj,' | ', indi_emp.razao_social,' | ',indi_emp.email,' | ',indi_emp.telefone) as indicador, o.cel_contato_posto, contato_p.celular as celular_contato, e.tp_cadastro
                    FROM    pedidos p
                INNER JOIN  orcamentos o        ON  p.orcamento_id  = o.id
                LEFT JOIN   fretes f            ON  o.frete_id      = f.id
                INNER JOIN  empresas e          ON  o.empresa_id    = e.id
                LEFT JOIN   usuarios u          ON  u.id = p.tecnico_id
                LEFT JOIN   usuarios i          ON  (i.id = o.indicador_id or i.id = o.indicador)
                LEFT JOIN   empresas indi_emp   ON  i.empresa_id = indi_emp.id
                LEFT JOIN   usuarios contato_p  ON  contato_p.id = o.contato_id
                LEFT JOIN   empresas t          ON  u.empresa_id = t.id 
                WHERE       p.id = ".$id;
                             
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function buscaIcmsOrcamentoPedido($id) 
    {
         
        $sql =  "SELECT i.valor_tributo
                    FROM        pedidos p
                    INNER JOIN  orcamentos o    ON o.id = p.orcamento_id                
                    INNER JOIN  empresas e      on o.empresa_id = e.id 
                    INNER JOIN  estados es      ON es.uf = e.estado  
                    INNER JOIN  icms i          on i.estado_id = es.id
                    WHERE p.id = ".$id;

        $query = $this->db->query($sql);
        return $query->row();
    }

    public function pedidosProdutos($id){
        $sql =  "SELECT     pro.descricao, pro.modelo, pro.codigo, pi.qtd, pi.produtos, pi.valor, pro.tipo_produto_id, pi.produto_id, pro.modelo_tecnico, pro.nr_produtos, pi.id,pi.valor_stx, pro.fl_arla
                    FROM    pedidos p 
                INNER JOIN  pedido_itens pi ON pi.pedido_id = p.id                                 
                INNER JOIN  produtos pro    ON pro.id = pi.produto_id               
                WHERE p.id = ".$id;
                           
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function pedidosFormaPagto($id){
        $sql =  "SELECT     pfp.*
                    FROM    pedidos p 
                INNER JOIN  pedido_form_pag pfp ON pfp.pedido_id = p.id                                                 
                WHERE p.id = ".$id;
                           
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function insereFormaPagamento ($data) {
        
        return $this->db->insert('pedido_form_pag', $data);     

    }

    public function inserePedidoItens ($data) {
        
        return $this->db->insert('pedido_itens', $data);     

    }
	
	public function retornaClientePorPedido($id){
		$sql = "SELECT concat(e.cnpj,' - ', e.razao_social) as cliente, e.id as cliente_id, e.cidade, e.estado as uf, e.pais FROM empresas e
				INNER JOIN orcamentos o ON o.empresa_id = e.id
				INNER JOIN pedidos p ON o.id = p.orcamento_id
				where p.id=".$id;
				
		$query = $this->db->query($sql);
        return $query->row();
	}
	
	public function buscaOpsPorPedido($pedido_id){
		$sql = "SELECT * FROM ordem_producao WHERE pedido_id = ".$pedido_id." and status_op_id != 4";
		$query = $this->db->query($sql);
         
        return $query->result_array();
	}

    public function selectPedidosFeira(){

        $sql =  "SELECT p.*, concat(e.id,' | ',e.cnpj, ' | ', e.razao_social) as cliente, s.descricao as status, e.email, o.contato_posto, o.empresa_id, o.id as orcamento_id       
                FROM    pedidos p, empresas e, orcamentos o, status_pedidos s, usuarios u
                WHERE   o.empresa_id = e.id and
                         p.orcamento_id = o.id and 
                         p.status_pedido_id = s.id and 
                         u.id = p.usuario_geracao and
                         u.tipo_cadastro_id = 11 and 
                         date(p.dthr_geracao) between '2022-07-26' and '2022-07-29'
                         
                ORDER BY p.id DESC";
        $query = $this->db->query($sql);
        return $query->result();   
    }

    public function buscaStatus(){
        $sql =  "SELECT * FROM status_pedidos ORDER BY ordem ASC";        
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function buscaStatusByid($status_id){
        $sql =  "SELECT * FROM status_pedidos WHERE id=".$status_id;        
        return $this->db->query($sql)->row_array();
    }

    public function getRelatorioPedidos($filtros){
        $where="1=1";
        $andamento="";
        $dthr_andamento = ", DATE_FORMAT(p.dthr_geracao,'%d/%m/%Y') as dthr_andamento, DATEDIFF(date(now()),date(p.dthr_geracao)) as dias, '1' as tipo, '' as dt_status_atual ";
        if( $filtros['status_id'] != '' ){
            $dthr_andamento = ", date_format(pa.dthr_andamento,'%d/%m/%Y') as dthr_andamento, DATEDIFF(date(now()),date(pa.dthr_andamento)) as dias, '2' as tipo, date_format(paw.dthr_andamento,'%d/%m/%Y') as dt_status_atual ";
            $andamento = 'LEFT JOIN (select pas.pedido_id as pedido_id, max(pas.dthr_andamento) as dthr_andamento 
                            FROM pedido_andamentos pas 
                            WHERE pas.status_pedido_id = '.$filtros['status_id'].' 
                            GROUP BY pas.pedido_id) as pa ON pa.pedido_id = p.id 
                         LEFT JOIN (select pas.pedido_id as pedido_id, max(pas.dthr_andamento) as dthr_andamento FROM pedido_andamentos pas 
                            GROUP BY pas.pedido_id) as paw ON paw.pedido_id = p.id';
            $where.=" and p.status_pedido_id = ".$filtros['status_id'];
            
        }

        if( $filtros['dt_ini'] != '' && $filtros['dt_fim'] != ''){
            $where.=" and date(p.dthr_geracao) between '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."'";
            
        }        

        if( $filtros['dt_ini_hist'] != '' && $filtros['dt_fim_hist'] != ''){
            $dthr_andamento = ", date_format(pa.dthr_andamento,'%d/%m/%Y') as dthr_andamento, DATEDIFF(date(now()),date(pa.dthr_andamento)) as dias , '2' as tipo, date_format(paw.dthr_andamento,'%d/%m/%Y') as dt_status_atual ";
            $andamento = 'LEFT JOIN (select pas.pedido_id as pedido_id, max(pas.dthr_andamento) as dthr_andamento
                            FROM pedido_andamentos pas 
                            WHERE pas.status_pedido_id = '.$filtros['status_id'].' 
                            GROUP BY pas.pedido_id) as pa ON pa.pedido_id = p.id
                            LEFT JOIN (select pas.pedido_id as pedido_id, max(pas.dthr_andamento) as dthr_andamento FROM pedido_andamentos pas 
                            GROUP BY pas.pedido_id) as paw ON paw.pedido_id = p.id';
            $where =" date(pa.dthr_andamento) between '".$filtros['dt_ini_hist']."' and '".$filtros['dt_fim_hist']."'";
            
        }

        if( $filtros['indicador_id'] != '' ){
            $where.=" and i.id =".$filtros['indicador_id']." or indic.id=".$filtros['indicador_id'];
            
        }
        $having = '';
        if( $filtros['consultor'] != '' ){
            $having =" HAVING consultor_resp ='".$filtros['consultor']."'";
            
        }

        if( $filtros['faturados'] != ''){

            $where.= ($filtros['faturados'] == 'S') ? " and p.nr_nf <> '' " : " and p.nr_nf = ''";
        }

        if( $filtros['dt_ini_fatur'] != ''){

            $where.= " and p.nr_nf <> '' and dt_emissao_nf BETWEEN '".$filtros['dt_ini_fatur']."' and '".$filtros['dt_fim_fatur']."'" ;
        }

        $sql = "SELECT  p.id, s.descricao as status, concat(e.cnpj, '|',e.razao_social) as cliente, concat(indic.cnpj, '|',indic.razao_social) as indicador, COALESCE(sum( op.qtd * op.valor ), p.valor_total) as valor_total, i.id as indicador_id, sum(if(pro.tipo_produto_id in (1,2,3,6),op.qtd,0) ) as total_bombas, sum(pro.nr_bicos*op.qtd) as total_bicos ".$dthr_andamento.", ps.dt_semana_prog, s.ordem, if(o.origem like '%indicador%' or o.origem like '%representante%', concat('|',i.cnpj, '|',i.razao_social), '' ) as rep_indi_emiss, (select lower(us.nome) from usuarios us inner join zona_atuacao zo on zo.usuario_id = us.id where zo.estado = e.estado) as consultor_resp, e.telefone, e.email, e.cidade, e.estado, p.nr_nf, p.dt_emissao_nf, contato.nome as contato, contato.telefone as contato_tel, contato.email as contato_email   
                  FROM  orcamentos o
                  INNER JOIN    pedidos p               ON  p.orcamento_id      =   o.id   
                  INNER JOIN    pedido_itens     op     ON  p.id                =   op.pedido_id                  
                  LEFT  JOIN    empresas e              ON  o.empresa_id        =   e.id 
                  LEFT  JOIN    usuarios u              ON  o.solicitante_id    =   u.id
                  LEFT  JOIN    empresas i              ON  u.empresa_id        =   i.id 
                  LEFT  JOIN    usuarios ind            ON  o.indicador_id      =   ind.id
                  LEFT  JOIN    usuarios contato        ON  o.contato_id        =   contato.id
                  INNER JOIN    status_pedidos s        ON  s.id                =   p.status_pedido_id 
                  LEFT  JOIN    empresas indic          ON  ind.empresa_id      =   indic.id
                  INNER JOIN    produtos pro            ON op.produto_id        =   pro.id
				  LEFT JOIN 	(SELECT * FROM programacao_semanal GROUP BY pedido_id) ps	ON ps.pedido_id 		= 	p.id
                  ".$andamento."
                  WHERE ".$where."
                  group by p.id  ".$having."
                ORDER BY p.id DESC";
        echo $sql;die;
        $query = $this->db->query($sql);
        $retorno['orcamento'] = $query->result_array();    

        return $retorno;
    }

    public function getRelatorioPrevisaoVendas($filtros)
    {
         
        $where="";
        
        if( $filtros['produto_id'] != '' ){
            $where.=" and pro.id = ".$filtros['produto_id'];
            
        }

        $sql = "SELECT a.modelo, sum(total_bombas) as total_bombas, sum(total_bicos) as total_bicos, a.data_primeiro, a.diff as diferenca, a.total_op, a.id, count(*) as total, sum(a.total_geral) as total_geral
                FROM (SELECT  pro.id, pro.modelo, sum(if(pro.tipo_produto_id in (1,2,3,6),op.qtd,0) ) as total_bombas, sum(pro.nr_bicos*op.qtd) as total_bicos, date_format(min(p.dthr_geracao),'%d/%m/%Y') as data_primeiro, datediff(curdate(),date(p.dthr_geracao)) as diff, count(p.id) as total_op, tot.total as total_geral
                FROM    orcamentos o 
                INNER JOIN pedidos p ON p.orcamento_id = o.id 
                INNER JOIN pedido_itens op ON p.id = op.pedido_id 
                LEFT JOIN empresas e ON o.empresa_id = e.id 
                LEFT JOIN usuarios u ON o.solicitante_id = u.id 
                INNER JOIN status_pedidos s ON s.id = p.status_pedido_id 
                INNER JOIN produtos pro ON op.produto_id = pro.id 
                INNER JOIN (select sum(pei.qtd*pei.valor) as total, ped.id from pedidos ped inner join pedido_itens pei on pei.pedido_id = ped.id GROUP by ped.id ) tot on tot.id = p.id
                WHERE p.status_pedido_id = 1 and pro.tipo_produto_id in (1,2,3,6) ".$where."
                GROUP BY pro.id, p.id
                union 
                SELECT pro.id, pro.modelo, sum(if(pro.tipo_produto_id in (1,2,3,6),op.qtd,0) ) as total_bombas, sum(pro.nr_bicos*op.qtd) as total_bicos, date_format(min(o.emissao),'%d/%m/%Y') as data_primeiro, datediff(curdate(), date(o.emissao)) as diff, count(o.id) as total_op, tot.total as total_geral
                  FROM  orcamentos o
                  INNER JOIN    orcamento_produtos   op ON  o.id                =   op.orcamento_id                  
                  LEFT  JOIN    empresas e              ON  o.empresa_id        =   e.id 
                  LEFT  JOIN    usuarios u              ON  o.solicitante_id    =   u.id
                  LEFT  JOIN    empresas i              ON  u.empresa_id        =   i.id                  
                  INNER JOIN    status_orcamentos s     ON  s.id                =   o.status_orcamento_id                   
                  INNER JOIN    produtos pro            ON op.produto_id        =   pro.id
                  INNER JOIN (select sum(pei.qtd*pei.valor) as total, ped.id from orcamentos ped inner join orcamento_produtos pei on pei.orcamento_id = ped.id GROUP by ped.id ) tot on tot.id = o.id
                  WHERE o.status_orcamento_id = 2 and o.id not in (select orcamento_id from pedidos) and pro.tipo_produto_id in (1,2,3,6) ".$where."
                  group by pro.id, o.id) as a group by a.id
                ORDER BY total_bombas DESC";
        
        $query = $this->db->query($sql);
        $retorno['orcamento'] = $query->result_array();    

        return $retorno;

    } 

    public function buscaOportunidadesPorModelo($produto_id){

        $sql = "SELECT a.tipo, a.id, concat(a.cnpj,'|',a.razao_social) as cliente, a.dt_emissao, a.totalzis as total
                FROM (SELECT 'Pedido' as tipo, p.id, e.razao_social, e.cnpj, sum(op.valor*op.qtd) as total, date_format(p.dthr_geracao,'%d/%m/%Y') as dt_emissao, group_concat(op.produto_id), total.totalzis
                FROM    orcamentos o
                INNER JOIN pedidos p ON p.orcamento_id = o.id 
                INNER JOIN pedido_itens op ON p.id = op.pedido_id 
                LEFT JOIN empresas e ON o.empresa_id = e.id 
                LEFT JOIN usuarios u ON o.solicitante_id = u.id 
                INNER JOIN status_pedidos s ON s.id = p.status_pedido_id   
                INNER JOIN (select sum( pet.qtd*pet.valor )as totalzis, ped.id from pedidos ped inner join pedido_itens pet on pet.pedido_id = ped.id group by ped.id ) total on p.id = total.id               
                WHERE p.status_pedido_id = 1 
                and p.id in (select pp.id from pedidos pp INNER join pedido_itens pi on pi.pedido_id = pp.id where EXISTS (select produtos.id from produtos where op.produto_id = produtos.id and produtos.id = ".$produto_id." ) group by p.id)
                GROUP BY p.id                
                union 
                SELECT 'Orçamento' as tipo, o.id, e.razao_social, e.cnpj, sum(op.valor*op.qtd) as total, date_format(o.emissao,'%d/%m/%Y') as dt_emissao, GROUP_CONCAT(op.produto_id), total.totalzis
                  FROM  orcamentos o
                  LEFT JOIN    orcamento_produtos    op ON  o.id                =   op.orcamento_id                  
                  LEFT  JOIN    empresas e              ON  o.empresa_id        =   e.id 
                  LEFT  JOIN    usuarios u              ON  o.solicitante_id    =   u.id
                  LEFT  JOIN    empresas i              ON  u.empresa_id        =   i.id                  
                  INNER JOIN    status_orcamentos s     ON  s.id                =   o.status_orcamento_id
                  INNER JOIN (select sum( pet.qtd*pet.valor )as totalzis, ped.id from orcamentos ped inner join orcamento_produtos pet on pet.orcamento_id = ped.id group by ped.id ) total on o.id = total.id 
                  WHERE o.status_orcamento_id = 2 and o.id not in (select orcamento_id from pedidos) 
                  and o.id in (select o.id from orcamentos o INNER join orcamento_produtos op on op.orcamento_id = o.id where EXISTS (select produtos.id from produtos where op.produto_id = produtos.id and produtos.id = ".$produto_id." ) group by o.id)    
                  group by o.id) as a group by a.id";
        
        return $this->db->query($sql)->result_array();
    }

    public function sincronismoStx(){
        $sql = 'SELECT COALESCE(max(sincronismo_id),0)+1 as sincronismo_id FROM stx_pedido';
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function insereStxPedido ($data) {
        
        return $this->db->insert('stx_pedido', $data);     

    } 

    public function insereAndamento($dados)
    {
        return $this->db->insert('pedido_andamentos', $dados);
    }

    public function numeroItem($pedido_id){
        $sql    =   "SELECT COALESCE(count(*),0)+1 as total from stx_pedido_item where pedido_internet = ".$pedido_id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getSincronismoStx($pedido_id){
        $sql = 'SELECT sincronismo_id FROM stx_pedido WHERE pedido_internet = '.$pedido_id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function sincronismoStxItem(){
        $sql = 'SELECT COALESCE(max(sincronismo_id),0)+1 as sincronismo_id FROM stx_pedido_item';
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function insereStxPedidoItem ($data) {
        
        return $this->db->insert('stx_pedido_item', $data);     

    }

    public function buscaFormaEntrega($pedido_id){
        
        $sql = "SELECT e.* 
                FROM    entregas e, pedidos p, orcamentos o 
                WHERE   e.id = o.entrega_id and 
                        o.id = p.orcamento_id and 
                        p.id = ".$pedido_id;

        $query = $this->db->query($sql);

        return $query->row_array();
    }

    public function totalPedidosConfirmadosAno(){
    
        $sql = "SELECT  CASE
                            WHEN z.usuario_id = 645 THEN 'A'
                            WHEN z.usuario_id = 10  THEN 'B'
                            ELSE 'C'
                        END zona, count(pedidos.id) as total, format(sum(pedidos.valor_total ),2,'de_DE') as total_valor, sum(pedidos.valor_total) as  valor_total
                FROM    pedidos
                INNER JOIN  orcamentos o on o.id = pedidos.orcamento_id
                INNER JOIN  empresas e on e.id = o.empresa_id
                INNER JOIN  zona_atuacao z ON z.estado = e.estado
                INNER JOIN  (select sum(pee.valor) as total, pee.pedido_id from pedido_itens pee group by pee.pedido_id) pi  on pi.pedido_id = pedidos.id
                WHERE   (pedidos.id in (select op.pedido_id from ordem_producao op where extract(YEAR FROM op.dthr_geracao) = '".date('Y')."') or pedidos.status_pedido_id = 2) and 
                            extract(year from pedidos.dthr_geracao ) = '".date('Y')."'
                GROUP BY    z.usuario_id 
                ORDER BY zona";
        
        $query = $this->db->query($sql);

        return $query->result_array();
    
    }

    public function totalBicosBombas($pedido_id){

        $sql = "    SELECT  sum(p.nr_bicos*pi.qtd) as total_bicos, sum(pi.qtd) as total_bombas 
                    FROM    produtos p, pedidos pe, pedido_itens pi 
                    WHERE   p.id = pi.produto_id and 
                            pe.id = pi.pedido_id and 
                            pe.id = ".$pedido_id." and 
                            p.tipo_produto_id in (1,2,3,6) 
                    GROUP by pe.id";

        $query = $this->db->query($sql);            

        return $query->row_array();

    }

    public function buscaPedidosAprovacao()
    {
        $sql = "SELECT p.*, concat(e.cnpj,'-',e.razao_social) as cliente FROM pedidos p, orcamentos o, empresas e 
                WHERE  p.orcamento_id = o.id and o.empresa_id = e.id and status_pedido_id = 9";
        return $this->db->query($sql)->result_array();
    }

    public function totalFechadoMesPed(){

        $sql="  SELECT  sum(pi.valor*pi.qtd) as total, extract(MONTH FROM p.dthr_geracao) as mes 
                FROM    pedidos p, pedido_itens pi, produtos pr
                WHERE p.id = pi.pedido_id and pi.produto_id = pr.id and extract(YEAR FROM p.dthr_geracao) = '".date('Y')."' and p.status_pedido_id != 7 group by extract(MONTH FROM p.dthr_geracao) ORDER BY mes ASC";    

        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();     
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $cor[] = $this->random_color();
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  "'0.00'";
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= "'Janeiro'";
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= "'#ffcc00'";
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function totalConfirmadosMesPed(){

        $sql="  SELECT  sum( pi.valor * pi.qtd ) as total, extract(MONTH FROM op.dthr_geracao) as mes 
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id ) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        extract( year from op.dthr_geracao ) = '".date('Y')."' 
                GROUP BY extract(MONTH FROM op.dthr_geracao) 
                ORDER BY mes ASC";    

        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();     
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $cor[] = "'".$this->random_color()."'";
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= "'Janeiro'";
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= "'#ffcc00'";
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function totalConfirmadosMesPedAnterior(){

        $sql="  SELECT  sum( pi.valor * pi.qtd ) as total, extract(MONTH FROM op.dthr_geracao) as mes 
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op ON op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        op.dthr_geracao  between '".date('Y',strtotime('-1 year'))."-01-01' and '".date('Y-m-d', strtotime('-1 year'))."' 
                GROUP BY extract(MONTH FROM op.dthr_geracao) 
                ORDER BY mes ASC";    
                
        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();     
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $cor[] = "'".$this->random_color()."'";
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= "'Janeiro'";
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= "'#ffcc00'";
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function totalConfirmadosMesPedAntAnt(){

        $sql="  SELECT  sum( pi.valor * pi.qtd ) as total, extract(MONTH FROM op.dthr_geracao) as mes 
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op ON op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        op.dthr_geracao  between '".date('Y',strtotime('-2 year'))."-01-01' and '".date('Y-m-d', strtotime('-2 year'))."' 
                GROUP BY extract(MONTH FROM op.dthr_geracao) 
                ORDER BY mes ASC";    
                
        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();     
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $cor[] = "'".$this->random_color()."'";
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= "'Janeiro'";
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= "'#ffcc0'";
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function buscaPedidosConfirmadosMes($mes)
    {
        $sql = "SELECT  pe.id, concat(e.cnpj,' - ',e.razao_social) as cliente, 
                        s.descricao as status, concat('R$ ',format(sum( pi.valor * pi.qtd ),2,'de_DE')) as valor
                        FROM    pedidos pe
                        inner join orcamentos o on pe.orcamento_id = o.id
                        inner join empresas e on o.empresa_id = e.id 
                        INNER JOIN status_pedidos s on pe.status_pedido_id = s.id
                        INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                        INNER JOIN produtos p on pi.produto_id = p.id
                        INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op on op.pedido_id = pe.id
                        WHERE   pe.status_pedido_id != 7 and
                                extract( year from op.dthr_geracao ) = '".date('Y')."' AND
                                extract( MONTH from op.dthr_geracao ) = '".$this->mes($mes)."'
                        GROUP BY pe.id";
                        
        return $this->db->query($sql)->result_array();
    }

    public function selectPedidoTotalStatus(){

         $sql = "SELECT     count(p.status_pedido_id) as valor, s.descricao 
                FROM        status_pedidos s, pedidos p 
                WHERE       s.id = p.status_pedido_id and extract(YEAR FROM p.dthr_geracao ) = '".date('Y')."'
                GROUP BY    p.status_pedido_id 
                ORDER BY    s.ordem ASC";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['valor'];
            $descricao['descricao'][] = "'".str_replace("'", "",$dados['descricao'])."'";
            $total = $total+$dados['valor'];
            $cor1 = "'".$this->random_color()."'";
            $cores1 = str_replace("'", "", $cor1);
            $cor[] = $cor1;
            $cores[] = $cores1;
        }
        if(count($retorno) > 0) {
            $return['valor'] = '['; 
            $return['valor'].= implode(',',$valor['valor']);
            $return['valor'].=']';
            $return['descricao'] = '['; 
            $return['descricao'].= implode(',',$descricao['descricao']);
            $return['descricao'].=']';
            $return['cor'] = '[';
            $return['cor'].= implode(',',$cor);
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            $return['cores']    = $cores;
            return $return;
        }else{
            $return['valor'] = '0,00';
            $return['descricao'] = '';
            $return['cor'] = '#ffcc00';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            $return['cores']    = '#000';
        }
    }

    public function totalBombasPed(){
    
        $sql="  SELECT  sum(pi.qtd) as total,  sum(pi.valor*pi.qtd) as valor_total
                FROM    pedidos p, pedido_itens pi,produtos pr 
                WHERE   pr.id = pi.produto_id    and 
                        pi.pedido_id = p.id  and 
                        pr.tipo_produto_id in (1,2,3,6)  and
                        p.status_pedido_id != 7   and 
                        pr.modelo != 'ML40L' and 
                        extract(YEAR FROM p.dthr_geracao) = '".date('Y')."'";
        
        $query = $this->db->query($sql); 
        
        return $query->row_array();

    }

    public function totalBombasOpcionaisPed(){
    
        $sql=" SELECT   sum(pi.valor*pi.qtd) as valor
                FROM    pedidos p, pedido_itens pi,produtos pr 
                WHERE   pr.id = pi.produto_id    and 
                        pi.pedido_id = p.id  and 
                        pr.tipo_produto_id in (1,2,3,6)  and
                        p.status_pedido_id != 7   and                         
                        extract(YEAR FROM p.dthr_geracao) = '".date('Y')."'";
        
        $query = $this->db->query($sql);
        
        return $query->row_array();

    }

    public function totalBombasConfirmadasPed(){
    
        $sql="  SELECT  sum(pi.qtd) as qtd_total, format(sum( pi.valor * pi.qtd ),2,'de_DE') as valor_total, format(sum( pi.valor * pi.qtd ) / sum(pi.qtd),2,'de_DE') as media  
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                inner JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op ON op.pedido_id = pe.id
                where   pe.status_pedido_id != 7 and 
                        p.tipo_produto_id   in (1,2,3,6) and 
                        p.modelo != 'ML40L' and 
                        extract( year from op.dthr_geracao ) = '".date('Y')."'
                ";
        
        $query = $this->db->query($sql); 
        
        return $query->row_array();

    }

    public function totalBombasOpcionaisConfirmadasPed(){
    
        $sql=" SELECT   sum(pi.valor*pi.qtd) as valor
                FROM    pedidos p, 
                        pedido_itens pi,
                        produtos pr, 
                        (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op 
                WHERE   pr.id = pi.produto_id    and 
                        pi.pedido_id = p.id      and 
                        op.pedido_id = p.id      and
                        pr.tipo_produto_id in (1,2,3,6)  and
                        p.status_pedido_id != 7  and                         
                        extract(YEAR FROM p.dthr_geracao) = '".date('Y')."'";
        
        $query = $this->db->query($sql);
        
        return $query->row_array();

    }

    public function relatorioMensalBombas($where="1=1")
    {
        $sql = "SELECT p.modelo, sum(pi.qtd) as qtd_total, format(sum( pi.valor * pi.qtd ),2,'de_DE') as valor_total, 
                       format(sum( pi.valor * pi.qtd ) / sum(pi.qtd),2,'de_DE') as media, extract(MONTH FROM op.dthr_geracao) as mes, concat( extract(MONTH FROM op.dthr_geracao),extract(YEAR FROM op.dthr_geracao) ) as groups_by, extract(YEAR FROM op.dthr_geracao) as ano
                FROM pedidos pe
                INNER JOIN pedido_itens pi ON pe.id = pi.pedido_id
                INNER JOIN produtos p ON pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7    and 
                        p.modelo != 'ML40L'         and 
                        p.tipo_produto_id in (1,2,3,6) and
                        ".$where."
                GROUP BY concat( extract(MONTH FROM op.dthr_geracao),extract(YEAR FROM op.dthr_geracao) ), p.modelo
                order by mes asc";
        //echo $sql;die;        
        return $this->db->query($sql)->result_array();
    
    }

    public function relatorioVendasModelo($where="1=1")
    {
        $sql = "SELECT p.modelo, sum(pi.qtd) as qtd_total, format(sum( pi.valor * pi.qtd ),2,'de_DE') as valor_total, 
                       format(sum( pi.valor * pi.qtd ) / sum(pi.qtd),2,'de_DE') as media, p.descricao 
                FROM pedidos pe
                INNER JOIN pedido_itens pi ON pe.id = pi.pedido_id
                INNER JOIN produtos p ON pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7    and 
                        p.modelo != 'ML40L'         and 
                        p.tipo_produto_id in (1,2,3,6) and 
                        ".$where."
                GROUP BY  p.modelo
                ORDER BY qtd_total DESC";

        return $this->db->query($sql)->result_array();
    }

    public function top5ModelosMaisVendidos()
    {
        $sql = "SELECT  p.modelo, count(p.id) as total 
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        p.tipo_produto_id in (1,2,3) and                       
                        extract( year from op.dthr_geracao ) = '".date('Y')."' 
                GROUP BY p.modelo                  
                ORDER BY total  DESC 
                LIMIT 5";
                
        $query  = $this->db->query($sql);
        $retorno= $query->result_array();

        $total = 0;       
        foreach ($retorno as $dados) {
            $valor['total'][] = $dados['total'];
            $descricao['descricao'][] = "'".$dados['modelo']."'";
            $total = $total+$dados['total'];
        }              

        if(count($retorno) > 0) {

            $return['valor'] = '['; 
            $return['valor'].= implode(',',$valor['total']);
            $return['valor'].=']';
            $return['descricao'] = '['; 
            $return['descricao'].= implode(',',$descricao['descricao']);
            $return['descricao'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }else{
            $return['valor'] = '0,00';
            $return['descricao'] = '';
            $return['cor'] = '#ffcc00';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            $return['cores']    = '#000';
        }
    }

    public function pedidosConfirmados3meses($mes_ini, $mes_fim)
    {
        $sql = "SELECT  sum(pi.qtd) as qtd_total, sum(pi.qtd) / 3 as media 
                FROM    pedidos pe 
                INNER JOIN  pedido_itens pi ON pe.id = pi.pedido_id 
                INNER JOIN  produtos p ON pi.produto_id = p.id 
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op ON op.pedido_id = pe.id 
                WHERE   pe.status_pedido_id != 7 and 
                        p.tipo_produto_id in (1,2,3,6) and  
                        p.modelo != 'ML40L' and 
                         op.dthr_geracao between '".$mes_ini."' AND  '".$mes_fim."'";

        return $this->db->query($sql)->row_array();
    }

    private function mes($mes){
        switch ($mes) {
            case 1:
                return 'Janeiro';
                break;
            case 2:
                return 'Fevereiro';
                break;    
            case 3:
                return 'Março';
                break;
            case 4:
                return 'Abril';
                break;    
            case 5:
                return 'Maio';
                break;    
            case 6:
                return 'Junho';
                break;       
            case 7:
                return 'Julho';
                break;         
            case 8:
                return 'Agosto';
                break;    
            case 9:
                return 'Setembro';
                break;        
            case 10:
                return 'Outubro';
                break;        
            case 11:
                return 'Novembro';
                break;        
            case 12:
                return 'Dezembro';
            case 'Janeiro':
                return '01';
            case 'Fevereiro':
                return '02';
            case 'Março':
                return '03';
            case 'Abril':
                return '04';
            case 'Maio':
                return '05';   
            case 'Junho':
                return '06';     
            case 'Julho':
                return '07';
            case 'Agosto':
                return '08';
            case 'Setembro':
                return '09';     
            case 'Outubro':
                return '10';
            case 'Novembro':
                return '11';
            case 'Dezembro':
                return '12';     
                break;                    
        }
    }

    private function random_color() {
        $letters = '0123456789ABCDEF';
        $color = '#';
        for($i = 0; $i < 6; $i++) {
            $index = rand(0,15);
            $color .= $letters[$index];
        }
        return $color;
    }

    public function buscaAndamentos($pedido_id)
    {
        $sql =  "SELECT pa.id, 
                        date_format(pa.dthr_andamento, '%d/%m/%Y %H:%i:%s') as dt_andamento, 
                        pa.andamento, 
                        u.nome as usuario, 
                        sp.descricao as status
                    FROM pedido_andamentos pa
                    INNER JOIN pedidos p        ON  p.id = pa.pedido_id
                    LEFT JOIN status_pedidos sp ON  pa.status_pedido_id = sp.id
                    LEFT JOIN usuarios u        ON  u.id = pa.usuario_id
                    WHERE  p.id = ".$pedido_id;


        $query = $this->db->query($sql);
        $row = $query->result_array();
        return $row;
    }

    public function buscaEmailSolAprovacao($pedido_id)
    {
        $sql = "SELECT  u.email     FROM pedido_andamentos pa, usuarios u 
                WHERE   pa.usuario_id       =   u.id            and 
                        pa.pedido_id        =   ".$pedido_id."  and 
                        pa.status_pedido_id =   9";

        return $this->db->query($sql)->row_array();
    }

    public function relatorioNrSerieOpPedidos($filtros)
    {
        $sql = "SELECT      b.id, b.modelo, pe.id as pedido_id, op.id as op_id, concat(e.cnpj, '-', e.razao_social) as cliente 
                FROM        pedidos pe 
                LEFT JOIN   bombas_nr_serie b   on  b.pedido_id = pe.id 
                LEFT  JOIN  ordem_producao op   on  b.ordem_producao_id = op.id and pe.id = op.pedido_id
                INNER JOIN  orcamentos o        on  o.id = pe.orcamento_id
                INNER JOIN  empresas e          on  e.id = o.empresa_id  
                WHERE  ".$filtros;

        return $this->db->query($sql)->result_array();
    }

    public function buscaAndamentoPedido($pedido_id)
    {
        $sql="  SELECT  pa.pedido_id, pa.andamento, DATE_FORMAT(pa.dthr_andamento, '%d/%m/%Y %H:%i:%s') as dthr_andamento, 
                        upper(u.nome) as usuario, s.descricao as status 
                FROM    pedido_andamentos pa, usuarios u , status_pedidos s
                WHERE   pa.usuario_id = u.id and s.id = pa.status_pedido_id
                        and pa.pedido_id = ".$pedido_id."
                ORDER BY pa.dthr_andamento DESC ";

        return $this->db->query($sql)->result_array();
    }

    public function relatorioPedidosConfirmados($where)
    {
        $sql = "SELECT  format(sum( pi.valor * pi.qtd ),2,'de_DE') as total, extract(MONTH FROM op.dthr_geracao) as mes, pe.id, sum(if(p.tipo_produto_id in (1,2,3,6),pi.qtd,0) ) as total_bombas, sum(p.nr_bicos*pi.qtd) as total_bicos, concat(e.cnpj,'-',e.razao_social) as cliente 
                FROM    pedidos pe
                INNER JOIN orcamentos o on o.id = pe.id
                INNER JOIN empresas e on e.id = o.empresa_id
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id                
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id ) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        extract( year from op.dthr_geracao ) = '".date('Y')."' ".$where."  
                GROUP BY pe.id 
                ORDER BY mes ASC";
                
        return $this->db->query($sql)->result_array();        
    }

    public function relatorioProgramacaoSemanal($where)
    {
        $condicao = '';
        if($where['dt_ini'] != '' && $where['dt_fim'] != ''){
            $condicao = " and ps.dt_semana_prog BETWEEN '".$where['dt_ini']."' and '".$where['dt_fim']."'";
        }

        $sql = "SELECT      ps.pedido_id, date_format(ps.dt_semana_prog,'%d/%m/%Y') as dt_semana_prog_f , SPLIT_STRING(op.modelo, '-',1) as tipo, if(ps.modelos_quebra != '', concat( ps.modelos_quebra,' - ', ps.nr_bicos,' - ', ps.nr_bombas ), op.modelo) as modelo, op.qtd, op.combustivel, pe.pintura, op.descricao, op.informacoes, if(ps.modelos_quebra != '', ps.nr_bombas,op.qtd)  as nr_quebrados, s.descricao as status, ps.nr_bicos,
                (select sp.descricao from programacao_semanal pse 
                    inner join producao_andamento pa on pa.pedido_id = pse.pedido_id
                    inner join status_producao sp on sp.id = pa.status_producao_id
                    where pse.id = ps.id group by pse.pedido_id ) as status_producao, ps.dt_semana_prog 
                FROM        programacao_semanal ps
                INNER JOIN  pedidos pe on pe.id = ps.pedido_id
                INNER JOIN  ordem_producao op on op.pedido_id = pe.id                
                INNER JOIN  status_pedidos s on pe.status_pedido_id = s.id
                where       op.status_op_id <> 4 ".$condicao."                
                UNION 
                SELECT      ps.pedido_id, date_format(ps.dt_semana_prog,'%d/%m/%Y') as dt_semana_prog_f , SPLIT_STRING(pro.modelo, '-',1) as tipo, if(ps.modelos_quebra != '', concat( ps.modelos_quebra,' - ', ps.nr_bicos,' - ', ps.nr_bombas ), pro.modelo) as modelo, pi.qtd,pi.produtos, pe.pintura, pro.modelo, '', if(ps.modelos_quebra != '', ps.nr_bombas,pi.qtd)  as nr_quebrados, s.descricao as status, ps.nr_bicos,
                (select sp.descricao from programacao_semanal pse 
                    left join producao_andamento pa on pa.pedido_id = pse.pedido_id
                    left join status_producao sp on sp.id = pa.status_producao_id
                    where pse.id = ps.id group by pse.pedido_id ) as status_producao, ps.dt_semana_prog 
                FROM        programacao_semanal ps
                INNER JOIN  pedidos pe on pe.id = ps.pedido_id
                INNER JOIN  pedido_itens pi on pi.pedido_id = pe.id
                inner join  produtos pro on pro.id = pi.produto_id
                INNER JOIN  status_pedidos s on pe.status_pedido_id = s.id
                where       s.ordem < 6 and pro.tipo_produto_id in (1,2,3,6) ".$condicao."  
                GROUP BY pe.id, pro.id
                ORDER BY dt_semana_prog asc, pedido_id asc ";
        
        return $this->db->query($sql)->result_array();
    }

    public function relatorioProgramacaoSemanalPorModelo($where)
    {
        $condicao = '';
        if($where['dt_ini'] != '' && $where['dt_fim'] != ''){
            $condicao = " and ps.dt_semana_prog BETWEEN '".$where['dt_ini']."' and '".$where['dt_fim']."'";
        }

        $sql = "SELECT     p.modelo as modelo, sum(pi.qtd) as qtd, sum(pi.qtd*p.nr_bicos) as bicos 
                FROM        programacao_semanal ps
                INNER JOIN  pedidos pe on pe.id = ps.pedido_id
                INNER JOIN  pedido_itens pi on pi.pedido_id = pe.id                
                INNER JOIN  status_pedidos s on pe.status_pedido_id = s.id
                INNER JOIN   produtos p on pi.produto_id = p.id
                where       p.tipo_produto_id in (1,2,3,6) ".$condicao."  
                group by  p.id
                ORDER BY p.modelo ASC ";
        
        return $this->db->query($sql)->result_array();
    }

    public function insereMotivoReprovacao ($data) {

        return $this->db->insert('motivo_reprovacao_inspecao', $data);
    }

     public function inserirFotos ($data) {

        return $this->db->insert('pedido_fotos', $data);
    }

    public function listarFotosPedido($pedido_id)
    {
        $sql = "SELECT * FROM pedido_fotos WHERE pedido_id = ".$pedido_id;
        return $this->db->query($sql)->result_array();
    }

    public function verificaPedidosPorCliente($empresa_id)
    {
        $sql = "SELECT count(*) as total FROM pedidos p
                inner join orcamentos o ON o.id = p.orcamento_id
                WHERE o.empresa_id = ".$empresa_id;
                
        return $this->db->query($sql)->result_array();
    }

    public function editarCombustiveis($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('pedido_itens', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function verificaCombustiveis($pedido_id)
    {
        $sql = "SELECT count(*) as total from pedido_itens pi 
                INNER JOIN produtos p on pi.produto_id = p.id 
                WHERE pi.pedido_id = ".$pedido_id." and ( upper(pi.produtos) like '%DEFINIR%' OR pi.produtos = '') and p.tipo_produto_id <> 4";
                
        return $this->db->query($sql)->row_array();
    }

    public function insereLogPinturas ($data) {

        return $this->db->insert('log_pinturas', $data);
    }    

    public function buscarValorTotalStatus($status)
    {
        $sql = "SELECT format(sum(pi.valor*pi.qtd),2,'de_DE')  as total FROM pedidos p 
                INNER JOIN pedido_itens pi ON pi.pedido_id = p.id 
                inner join orcamentos o    on o.id = p.orcamento_id
                WHERE  p.status_pedido_id in (".$status.")";         
        return $this->db->query($sql)->row_array();
    }

    public function buscaModelosPedidos()
    {
        $sql = "SELECT  pr.* 
                FROM    pedidos p
                INNER JOIN  pedido_itens pi     on  p.id = pi.pedido_id
                INNER JOIN  produtos pr         on  pi.produto_id = pr.id
                GROUP by pr.id";
        return $this->db->query($sql)->result_array();
    }

    public function buscaProximoStatus($ordem)
    {
        $sql = "SELECT * FROM status_pedidos WHERE ordem = ".$ordem;
        return $this->db->query($sql)->result_array();
    }

    public function atualizaStatusAnterior($where)
    {
        $ordem = ($where['ordem']-1 == 0) ? 1 : $where['ordem']-1;

        $sql    = " SELECT      s.id, u.email, s.descricao, s.setor_id 
                    FROM        status_pedidos s 
                    INNER JOIN  usuarios u on s.setor_id = u.setor_id 
                    WHERE       s.ordem = ".$ordem." ORDER BY s.id DESC";
        $status = $this->db->query($sql)->result_array();
        $this->db->where('id', $where['pedido_id']);
        $update = array('status_pedido_id' => $status[0]['id']);
        if($this->db->update('pedidos', $update)){
            return $status;
        }else{
            return false;
        }
    }

    public function totalConfirmadosAcumuladoAno($ano){

        $sql="  SELECT  sum( pi.valor * pi.qtd ) as total, extract( year from op.dthr_geracao ) as ano
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id ) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        extract( YEAR FROM op.dthr_geracao ) = '".$ano."' and
                         date(op.dthr_geracao) between '".$ano."-01-01' and '".$ano."-".date('m-d')."'
                GROUP BY extract( YEAR FROM op.dthr_geracao )";    

        $query      =   $this->db->query($sql);
        $retorno    =   $query->row_array();     
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(is_array($retorno)){
            

            $return['valor']    =   $retorno['total'];
            $return['ano']      =   $retorno['ano'];
            $return['cor']      =   $cor[] = $this->random_color();
            $return['dados']    =   $retorno['total'];
            $return['total']    =   $retorno['total'];
            return $return;

        }
    }

    public function buscaTotalPedidosAbertosDias($periodo_ini,$periodo_fim)
    {
        $sql = "SELECT count(*) as total
                FROM pedidos p 
                inner join orcamentos o on p.orcamento_id = o.id                 
                WHERE p.status_pedido_id in (1,9,17) and (DATEDIFF('".date('Y-m-d')."', p.dthr_geracao) >= ".$periodo_ini." and DATEDIFF('".date('Y-m-d')."', p.dthr_geracao) <= ".$periodo_fim.")";
        
        return $this->db->query($sql)->row_array();        
    
    }

    public function buscaPedidosAbertosDias($periodo_ini,$periodo_fim)
    {
        $sql = "SELECT o.id, concat(e.cnpj,'-',e.razao_social) as cliente, DATEDIFF('".date('Y-m-d')."', p.dthr_geracao) as dias, format(sum(op.qtd*op.valor),2,'de_DE') as total, sum(op.qtd*op.valor) as valor, concat(date_format(p.dthr_geracao,'%d/%m/%Y'), '-', DATEDIFF('".date('Y-m-d')."', p.dthr_geracao), ' dias') as emissao 
                FROM pedidos p 
                inner join orcamentos o on p.orcamento_id = o.id 
                INNER JOIN pedido_itens op ON op.pedido_id = p.id 
                INNER JOIN empresas e ON e.id = o.empresa_id 
                WHERE p.status_pedido_id in (1,9,17) and (DATEDIFF('".date('Y-m-d')."', p.dthr_geracao) >= ".$periodo_ini." and DATEDIFF('".date('Y-m-d')."', p.dthr_geracao) <= ".$periodo_fim.")
                GROUP BY o.id
                ORDER BY 3 DESC";
        
        return $this->db->query($sql)->result_array();        
    
    }

    public function buscaPedidosPorProduto($pedido_itens_id)
    {
        $sql = "SELECT  pi.*, pr.modelo, pr.codigo, pr.descricao 
                FROM    pedido_itens pi 
                INNER JOIN produtos pr ON pi.produto_id = pr.id 
                where   pi.id = ".$pedido_itens_id;

        return $this->db->query($sql)->row_array();
    }

    public function selectFaturamentoTpPagto()
    {
        $sql = "SELECT * FROM faturamento_tp_pagto";
        return $this->db->query($sql)->result_array();
    }

    public function buscaPedidosFaturamento($cnpj)
    {
        $sql = "SELECT pe.id, format(sum(pi.qtd*pi.valor),2,'DE_de') as valor_total, format(sum(f.valor),2,'DE_de') as valor_pago, ftp.descricao as tipo_pagto, pe.dthr_geracao, pe.nr_parcelas 
                FROM pedidos pe 
                INNER JOIN pedido_itens pi on pi.pedido_id = pe.id 
                INNER JOIN orcamentos o on o.id = pe.orcamento_id 
                INNER JOIN empresas e on e.id = o.empresa_id
                LEFT JOIN faturamento_tp_pagto ftp on ftp.id = pe.faturamento_tp_pagto_id 
                LEFT JOIN faturamento_pagto f on f.pedido_id = pe.id
                WHERE e.cnpj = '".$cnpj."'
                GROUP BY pe.id  
                ORDER BY pe.id  DESC";
        return $this->db->query($sql)->result_array();
    }

    public function buscaPagtoPedido($pedido_id)
    {
        $sql = "SELECT  id, format(valor,2,'DE_de') as valor, dt_pagto, comprovante, pedido_id 
                FROM    faturamento_pagto 
                WHERE   pedido_id =".$pedido_id." 
                ORDER BY dt_pagto";
        return $this->db->query($sql)->result_array();
    }

    public function inserirPagto ($data) {
        
        return $this->db->insert('faturamento_pagto', $data);     

    }

    public function excluirPagamento($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('faturamento_pagto')){
            return true;
        }else{
            return false;
        }
    }

    public function selectTop3Confirmados($dt_ini, $dt_fim){
        $ini = explode('/',$dt_ini);
        $fim = explode('/',$dt_fim);
        $sql = "SELECT  concat(p.modelo, '   ') as descricao, sum(pi.qtd*pi.valor) / sum(pi.qtd) as total, count(p.id) as qtd,if(p.id = 23, '#ffcc00',if(p.id = 56,'#ccc','#000')) as cor  
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        p.tipo_produto_id in (1,2,3) and                       
                        op.dthr_geracao between '".$ini[2]."-".$ini[1]."-".$ini[0]."' and '".$fim[2]."-".$fim[1]."-".$fim[0]."' and 
                        p.id in (23,56,43)
                GROUP BY p.modelo                  
                ORDER BY p.id asc
                LIMIT 3";

        $query = $this->db->query($sql);
        $retorno = $query->result_array(); 
        $total = [];       
        foreach ($retorno as $dados) {
            $valor['valor'][] = $dados['total'];
            $descricao['descricao'][] = "'".$dados['descricao']."'";
            $total[] = $dados['qtd'];                        
            $cores[] = $dados['cor'];
        }

        if(count($retorno) > 0) {

            $return['valor'] = '['; 
            $return['valor'].= implode(',',$valor['valor']);
            $return['valor'].=']';
            $return['descricao'] = '['; 
            $return['descricao'].= implode(',',$descricao['descricao']);
            $return['descricao'].=']';            
            $return['dados']    = $retorno;
            $return['total']    = $total;
            $return['cores']    = $cores;
            return $return;
        }else{
            $return['valor'] = '0,00';
            $return['descricao'] = '';
            $return['cor'] = '#ffcc00';
            $return['dados']    = $retorno;
            $return['total']    = 0;
            $return['cores']    = '#000';
        }
    }

    public function comparativoAnoTop3($dt_ini, $dt_fim, $produto_id){
        $ini = explode('/',$dt_ini);
        $fim = explode('/',$dt_fim);
        $sql="  SELECT  concat(p.modelo, '   ') as descricao, sum(pi.qtd*pi.valor) / sum(pi.qtd) as total, count(p.id) as qtd,if(p.id = 23, '#ffcc00',if(p.id = 56,'#ccc','#000')) as cor, extract(YEAR_MONTH FROM op.dthr_geracao) as mes_ano, extract(MONTH FROM op.dthr_geracao) AS mes, extract(YEAR FROM op.dthr_geracao) as ano
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        p.tipo_produto_id in (1,2,3) and                       
                        op.dthr_geracao between '".$ini[2]."-".$ini[1]."-".$ini[0]."' and '".$fim[2]."-".$fim[1]."-".$fim[0]."' and 
                        p.id in (".$produto_id.")
                GROUP BY p.modelo, extract(YEAR_MONTH FROM op.dthr_geracao)                   
                ORDER BY  extract(YEAR_MONTH FROM op.dthr_geracao) asc, p.modelo";    
            
        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $descricao['cor'] = $this->random_color();
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= "'#ffcc00'";
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= "'Janeiro'";
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= '#ffcc00';
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
    }

    public function comparativoTotalBombas($ano)
    {
        $sql = "SELECT sum(pi.qtd) as total, extract(YEAR_MONTH FROM op.dthr_geracao) as mes_ano, extract(MONTH FROM op.dthr_geracao) AS mes, extract(YEAR FROM op.dthr_geracao) as ano
                FROM    pedidos pe
                INNER JOIN pedido_itens pi on pe.id = pi.pedido_id
                INNER JOIN produtos p on pi.produto_id = p.id
                INNER JOIN (select pa.pedido_id as pedido_id, max(pa.dthr_andamento) as dthr_geracao from pedido_andamentos pa where pa.status_pedido_id = 2 group by pa.pedido_id union SELECT o.pedido_id as pedido_id,max(o.dthr_geracao) as dthr_geracao FROM ordem_producao o where o.pedido_id not in (select paa.pedido_id as pedido_id from pedido_andamentos paa where paa.status_pedido_id = 2 group by paa.pedido_id) group by o.pedido_id) op on op.pedido_id = pe.id
                WHERE   pe.status_pedido_id != 7 and
                        p.tipo_produto_id in (1,2,3,6) and                       
                        op.dthr_geracao between '".$ano."-01-01' and '".$ano."-12-31'
                GROUP BY  extract(YEAR_MONTH FROM op.dthr_geracao)                   
                ORDER BY  extract(YEAR_MONTH FROM op.dthr_geracao) asc";

        $query      =   $this->db->query($sql);
        $retorno    =   $query->result_array();
        $total      =   0;    
        $return     =   array(); 
        $valor      =   array(); 
       
        if(count($retorno) > 0){

            foreach ($retorno as $dados) {
                $valor['total'][] = $dados['total'];
                $descricao['mes'][] = "'".$this->mes($dados['mes'])."'";
                $total = $total+$dados['total'];
                $descricao['cor'] = $this->random_color();
            }

            $return['valor'] = '['; 
            $return['valor'].=  implode(',',$valor['total']);
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= implode(',',$descricao['mes']);
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= "'#ffcc00'";
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;

        }else{
            $return['valor'] = '['; 
            $return['valor'].=  '0.00';
            $return['valor'].=']';
            $return['mes'] = '['; 
            $return['mes'].= "'Janeiro'";
            $return['mes'].=']';
            $return['cor'] = '[';
            $return['cor'].= '#ffcc00';
            $return['cor'].=']';
            $return['dados']    = $retorno;
            $return['total']    = $total;
            return $return;
        }
        
    }

    public function buscaPedidosDtEntrega()
    {
        $sql= "SELECT   p.id as pedido,
                        concat(e.cnpj,' - ',upper(e.razao_social)) as cliente, 
                        GROUP_CONCAT(DISTINCT ' ',pro.modelo) as modelos, 
                        sum(if(pro.tipo_produto_id in (1,2,3,6),pi.qtd,0)) as qtd_bombas,
                        date_format(p.dt_previsao_entrega,'%d/%m/%Y') as dt_prev_entrega,
                        t.descricao as transportadora
                FROM pedidos p
                INNER JOIN pedido_itens pi on pi.pedido_id = p.id
                INNER JOIN produtos pro on pro.id = pi.produto_id
                INNER JOIN orcamentos o on o.id = p.orcamento_id
                INNER JOIN empresas e on e.id = o.empresa_id
                LEFT JOIN transportadoras t on t.id = p.transportadora_id
                WHERE p.dt_previsao_entrega between '".date('Y-m-d',strtotime(date('Y-m-d'). ' -1 days'))."' and '".date('Y-m-d')."' and status_pedido_id not in (6,7,8)
                GROUP BY p.id
                ORDER BY dt_previsao_entrega ASC";

        return $this->db->query($sql)->result_array();
    }

    public function buscaPedidosbyId($pedido_id)
    {
        $sql = "SELECT pe.*, upper(e.razao_social) as razao_social, pe.nome_cliente as contato, e.cnpj,
                        e.insc_estadual, e.telefone, e.endereco, e.bairro, e.cidade, e.email, e.estado, e.cep,
                        sum(pi.qtd) as qtd, pr.modelo, pr.descricao, pi.valor
                        from pedidos pe 
                INNER JOIN orcamentos o on o.id = pe.orcamento_id
                INNER JOIN empresas e on e.id = o.empresa_id 
                INNER JOIN pedido_itens pi on pi.pedido_id = pe.id 
                INNER JOIN produtos pr on pr.id = pi.produto_id
                WHERE pe.id = ".$pedido_id." and pr.tipo_produto_id in (1,2,3,6)
                GROUP BY pr.id";

        return $this->db->query($sql)->result_array();   
    }

    public function valorTotalPedido($pedido_id)
    {
        $sql = "SELECT  sum(pi.qtd*pi.valor) as total 
                from    pedidos pe 
                inner join pedido_itens pi on pi.pedido_id = pe.id 
                where pe.id = ".$pedido_id;

        return $this->db->query($sql)->row_array();   
    }

    public function verificaPedidoComArla($pedido_id)
    {
        $sql = "SELECT COALESCE(COUNT(*),0) AS total FROM pedido_itens pi 
                INNER JOIN pedidos p on p.id = pi.pedido_id 
                INNER JOIN produtos pr on pr.id = pi.produto_id 
                where pr.descricao like '%arla%' and pi.pedido_id = ".$pedido_id;

        return $this->db->query($sql)->row_array();           

    }

}
?>