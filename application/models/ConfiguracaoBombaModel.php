<?php

class ConfiguracaoBombaModel extends CI_Model {
	
	public function save($configuracao_bomba)
	{
		if($configuracao_bomba['id'] != 0){
			$this->db->where('id', $configuracao_bomba['id']);
			$this->db->update('configuracao_bomba', $configuracao_bomba);
			if($this->db->affected_rows() > 0) {
				$retorno = $configuracao_bomba['id'];
			} else {
				$retorno = 0;
			}
		} else {
			unset($configuracao_bomba['id']);
			$this->db->insert('configuracao_bomba', $configuracao_bomba);
			$retorno = $this->db->insert_id();
		}
		return $retorno;
    }
	
	public function selectByEmpresa($empresa_id)
    {
		$this->db->select('p.modelo modelo');
		$this->db->select('ci.modelo identificador');
		$this->db->select('cb.*');
		$this->db->join('configuracao_identificador ci', 'ci.id = cb.identificador_id');
		$this->db->join('produtos p', 'p.id = cb.produto_id');
        $this->db->where('cb.empresa_id', $empresa_id);
		return $this->db->get('configuracao_bomba cb')->result_array();
		
    }
	
	public function selectBombas($empresa_id)
    {
        $this->db->select('p.*');
		$this->db->select('op.qtd quantidade');
		$this->db->join('orcamento_produtos op', 'op.produto_id = p.id');
		$this->db->join('orcamentos o', 'o.id = op.orcamento_id');
		$this->db->join('pedidos pd', 'pd.orcamento_id = o.id');
		$this->db->where_not_in('pd.status_pedido_id', array(1,7));
		$this->db->where('p.tipo_produto_id <', 4);
		$this->db->where('o.empresa_id', $empresa_id);
		$this->db->group_by("p.id, op.qtd");
		return $this->db->get('produtos p')->result_array();
    }
	
	public function delete($configuracao_bomba)
    {
		$this->db->where('id', $configuracao_bomba['id']);
		return $this->db->delete('configuracao_bomba');
    }
}
?>