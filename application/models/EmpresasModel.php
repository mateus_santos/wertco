<?php

class EmpresasModel extends CI_Model {

	public function add ($data) {
        
        $this->db->insert('empresas', $data);		
        return $this->db->insert_id();

	}

 	public function select() {
        
        $this->db->order_by('razao_social');
        return $this->db->get('empresas')->result_array();
    }

    public function getEmpresa($id){

        $sql =  "SELECT * FROM empresas WHERE id=".$id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getClientes(){

        $sql =  "   SELECT e.*  FROM empresas e
                        INNER JOIN  orcamentos o ON o.empresa_id = e.id 
                        INNER JOIN  pedidos p    ON p.orcamento_id = o.id 
                        group by e.id
                    ORDER BY e.fl_inadimplencia desc, e.id desc";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getClientesTecnicos(){

        $sql =  "   SELECT e.*  FROM empresas e
                        INNER JOIN  orcamentos o ON o.empresa_id = e.id 
                        INNER JOIN  pedidos p    ON p.orcamento_id = o.id 
                        group by e.id
                    UNION                     
                        SELECT distinct es.* from usuarios u 
                        INNER JOIN empresas es on es.id = u.empresa_id 
                        WHERE u.ativo = 1 and u.tipo_cadastro_id = 4
                        GROUP by es.id 
                    ORDER BY fl_inadimplencia DESC, id ASC ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getCnpj($cnpj){

        $sql =  "select * from empresas WHERE cnpj='".$cnpj."'";
        $query = $this->db->query($sql);
        return $query->result();   
    }
    
    public function getEmpresas($tipo){
        /*$where = ($tipo == 1) ? " AND e.id in (SELECT o.empresa_id FROM orcamentos o INNER JOIN pedidos p on p.orcamento_id = o.id UNION SELECT e.id FROM empresas e WHERE e.tipo_cadastro_id = 1 and e.id in (SELECT o.empresa_id FROM orcamentos o inner join empresas e on o.empresa_id = e.id where e.tipo_cadastro_id = 1 and o.status_orcamento_id in (1,6,16) ) ) " : "AND t.id = ".$tipo;*/

        $sql =  "SELECT e.*,t.descricao as tipo_cadastro 
                    FROM    empresas e, tipo_cadastros t 
                    WHERE t.id= e.tipo_cadastro_id 
                        AND t.id = ".$tipo;
        $query = $this->db->query($sql);
        return $query->result();       
    }

    public function getEmpresasUsuarios($id){
        
        $sql = "SELECT e.*,t.descricao as tipo_cadastro,u.nome, u.cpf, u.email as email_pessoal, tag cartao, ultima_atualizacao, periodo_renovacao periodo, u.dt_treinamento treinamento, u.tipo_cadastro_id tipo, u.id usuario_id
                FROM empresas e 
                INNER JOIN tipo_cadastros t ON t.id= e.tipo_cadastro_id  
                LEFT JOIN usuarios u ON u.empresa_id = e.id
				LEFT JOIN cartao_tecnicos ct ON u.id = ct.usuario_id
				WHERE e.id = ".$id;
        
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getSubtipoByTipo($tipo_cadastro_pai){
        
        $sql = "SELECT s.* FROM subtipo_cadastros s WHERE s.tipo_cadastro_pai = ".$tipo_cadastro_pai." ORDER BY s.nvl ASC";
        
        $query = $this->db->query($sql);
        return $query->result_array(); 
    }

    public function atualizaEmpresas($dados){

        $this->db->where('id', $dados['id']);

        if($this->db->update('empresas', $dados)){
            return true;

        }else{
            return false;
        }
    }

    public function getEmpresaAutocomplete($term){

        $sql =  "select id, concat(razao_social,' - ',cnpj) as label from empresas WHERE cnpj like '%".$term."%' and tipo_cadastro_id = 1 ";
        //echo $sql;die;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function empresaUsuario($usuario_id){

        $sql = "SELECT e.*, u.nome,u.cpf, u.fl_termos FROM usuarios u, empresas e where u.empresa_id = e.id and u.id=".$usuario_id;
        $query = $this->db->query($sql);
        return $query->row();   

    }
	
	public function retornaParceiros($term){

        $sql = "SELECT e.id, concat(e.cnpj,' - ', e.razao_social) as label, e.razao_social, e.cnpj, concat(e.endereco,'-',e.bairro,'-',e.cep) as endereco, concat(e.cidade,'/',e.estado) as cidade, e.tipo_cadastro_id FROM empresas e WHERE concat(e.razao_social,e.cnpj,e.id) like '%".$term."%' and (e.tipo_cadastro_id in (4,2,6,10,12))";

        $query = $this->db->query($sql);
        $retorno = $query->result_array();
        return $retorno;
    }

    public function todasEmpresasTecnicosClientes(){

        $sql = "SELECT DISTINCT 'chamado' as tipo,                                
                                upper(concat(trim(e.razao_social),' | ', trim(e.cnpj))) as empresa,                                
                                e.telefone,
                                '' as celular,
                                lower(e.email) as email,
                                upper(trim(e.endereco)) as endereco,
                                upper(trim(e.cidade)) as cidade,
                                upper(e.estado) as estado,
                                upper(e.bairro) as bairro,
                                upper(e.pais) as pais,
                                e.cep, 
                                e.credenciamento_inmetro,
                                e.credenciamento_crea,
                                '' as tag,
                                '' as local_treinamento,
                                cs.descricao as status,
                                e.latitude,
                                e.longitude,
                                e.id,
                                IF(cs.descricao is null,'pedido','chamado') as chamado_pedido                
                 from    empresas e                 
                inner JOIN    chamado c           ON  c.cliente_id    =   e.id
                inner JOIN    chamado_status cs   ON  cs.id           =   c.status_id  
                UNION
                SELECT DISTINCT 'tecnicos' as tipo,                                
                                upper(concat(e.razao_social,' | ', e.cnpj)) as empresa,                                
                                e.telefone,
                                u.celular,
                                lower(e.email) as email,
                                upper(e.endereco) as endereco,
                                upper(e.cidade) as cidade,
                                upper(e.estado) as estado,
                                upper(e.bairro) as bairro,
                                upper(e.pais) as pais,
                                e.cep,
                                e.credenciamento_inmetro,
                                e.credenciamento_crea,
                                ct.tag,
                                upper(u.local_treinamento) as local_treinamento,
                                'tecnicos' as status,
                                e.latitude,
                                e.longitude,
                                e.id,
                                'tecnico' as chamado_pedido
                FROM            empresas e
                INNER JOIN      usuarios u          ON  e.id = u.empresa_id
                LEFT JOIN       cartao_tecnicos ct  ON  ct.usuario_id = u.id
                WHERE   u.local_treinamento !=  '' AND 
                        u.tipo_cadastro_id  =   4  AND 
                        u.ativo = 1
                GROUP BY u.empresa_id                
                order by 1, 2, 14";

        $query = $this->db->query($sql);

        return $query->result_array();
        
    }

    public function listarBombasPorCliente(){

        $sql = "    SELECT      o.empresa_id, pro.modelo, sum(pi.qtd) as total
                    FROM        pedidos p, orcamentos o, pedido_itens pi, produtos pro
                    WHERE       p.orcamento_id  =   o.id            and 
                                p.id            =   pi.pedido_id    and 
                                pi.produto_id   =   pro.id          and
                                pro.tipo_produto_id     in  (1,2,3)
                    GROUP BY    o.empresa_id, pi.produto_id
                    ORDER BY    o.empresa_id ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }
	
    public function listarTodosClientesIndRep(){

        $sql = "SELECT DISTINCT 'cliente' as tipo,
                                upper(concat(e.razao_social,' | ', e.cnpj)) as empresa,
                                e.telefone,
                                lower(e.email) as email,
                                upper(e.endereco) as endereco,
                                upper(e.cidade) as cidade,
                                upper(e.estado) as estado,
                                upper(e.bairro) as bairro,
                                upper(e.pais) as pais,
                                e.cep,
                                so.descricao as status,
                                if(e.latitude is null,-1,e.latitude) as latitude,
                                if(e.longitude is null,-1,e.longitude) as longitude,
                                e.id,
                                '' as ativo
                FROM    orcamentos o
                INNER   JOIN    status_orcamentos so    ON  so.id           =   o.status_orcamento_id
                INNER   JOIN    empresas e              ON  o.empresa_id    =   e.id
                WHERE   o.status_orcamento_id in (1,2,5,6,12)                         
                GROUP BY e.id
                UNION
                SELECT DISTINCT tp.descricao as tipo,
                                upper(concat(e.razao_social,' | ', e.cnpj)) as empresa,
                                e.telefone,
                                lower(e.email) as email,
                                upper(e.endereco) as endereco,
                                upper(e.cidade) as cidade,
                                upper(e.estado) as estado,
                                upper(e.bairro) as bairro,
                                upper(e.pais) as pais,
                                e.cep,
                                tp.descricao as status,
                                e.latitude,
                                e.longitude,
                                e.id,
                                sum(u.ativo) as ativo
                FROM            empresas e                                
                INNER JOIN      usuarios u ON u.empresa_id = e.id
                INNER JOIN      tipo_cadastros tp   ON  tp.id = u.tipo_cadastro_id
                WHERE           u.tipo_cadastro_id  IN ( 2,4,6,10,12 ) 
                GROUP BY        e.id    
                HAVING ativo >= 1  ";

        $query = $this->db->query($sql);

        return $query->result_array();
        
    }

    public function listarOrcamentosPorCliente(){

        $sql = "SELECT o.id, o.empresa_id, so.descricao as status FROM orcamentos o, empresas e, status_orcamentos so  WHERE o.empresa_id = e.id and o.status_orcamento_id = so.id";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getRelatorioClientesInadimplentes($filtros)
    {
        $where = "";

        if( $filtros['regiao'] != ''){
            $where.=" and e.estado in ".$filtros['regiao'];
        }

        if( $filtros['estado'] != ''){
            $where.=" and e.estado = '".$filtros['estado']."'";   
        }

        if( $filtros['cliente_id'] != ''){
            $where.=" and e.id = ".$filtros['cliente_id'];   
        }        

        $sql = "SELECT  e.*, group_concat(p.id) as pedidos from empresas e, pedidos p, orcamentos o 
                where   e.id = o.empresa_id     and 
                        p.orcamento_id = o.id   and 
                        e.fl_inadimplencia = 1 
                        ".$where. " 
                group by e.id";
                
        return $this->db->query($sql)->result_array();
    }
    
    public function selectAnexos($empresa_id)
    {
        $sql = "SELECT * FROM empresa_anexos WHERE empresa_id =". $empresa_id;
        return $this->db->query($sql)->result_array();    
    }  

    public function insereAnexo($data) {
        
        return $this->db->insert('empresa_anexos', $data);        

    }

    public function excluirAnexo($anexo_id){
        $this->db->where('id', $anexo_id);
        if( $this->db->delete('empresa_anexos') ){
            return true;
        }else{
            return false;
        }
    }

    public function buscaDadosChamados($nr_serie)
    {
        $sql = "SELECT b.*, upper(e.razao_social) as razao_social, e.cnpj, e.id as empresa_id from bombas_nr_serie b
                INNER JOIN pedidos p on p.id = b.pedido_id
                INNER JOIN orcamentos o on o.id = p.orcamento_id
                INNER JOIN empresas e on e.id = o.empresa_id
                WHERE b.id = ".$nr_serie;

        return $this->db->query($sql)->row_array();
    }

    public function selectObs($id)
    {
        $sql = "SELECT * FROM empresa_observacoes WHERE empresa_id =".$id." ORDER BY id DESC";        
        return $this->db->query($sql)->result_array();
    }

    public function insereObs($data) {
        
        return $this->db->insert('empresa_observacoes', $data);

    }

    public function getCnpjCliente($cnpj)
    {
        $sql = "SELECT b.id as nr_serie, b.modelo, b.combustivel, e.* from empresas e
                INNER JOIN orcamentos o ON o.empresa_id = e.id 
                INNER JOIN pedidos p ON p.orcamento_id = o.id  
                LEFT JOIN bombas_nr_serie b on b.pedido_id = p.id 
                WHERE e.tipo_cadastro_id = 1 AND e.cnpj = '".$cnpj."' ";

        return $this->db->query($sql)->result_array();

    }

    public function getBandeiras()
    {
        $sql = "SELECT * FROM bandeiras";

        return $this->db->query($sql)->result_array();
    }
}
?>