<?php
class BombasNrSerieModel extends CI_Model {

	public function buscaNrSerie(){

        $sql =  "SELECT * FROM bombas_nr_serie"; 
         
        return $this->db->query($sql)->result_array();   
    }

    public function buscaNrSerieId($id)
    {
        $sql =  "SELECT * FROM bombas_nr_serie WHERE id = ".$id;          
        return $this->db->query($sql)->row_array();      
    }

    public function buscaNrSerieSS($dados) 
    {
        $aColumns = array('id', 'modelo', 'combustivel', 'pedido_id','ordem_producao_id', 'cliente','acoes');            
        $sLimit = "";
        if ( isset( $_POST['start'] ) && $_POST['length'] != '-1' ) {
            $sLimit = "LIMIT ".intval( $_POST['start'] ).", ".intval( $_POST['length'] );
            $inicial = intval( $_POST['start'] );
            $final = intval( $_POST['length'] );
        }

        $sOrder = "ORDER BY o.id desc";

        if ( isset( $_POST['order'] ) ) {
            $sOrder = " ORDER BY ";
            for ( $i=0 ; $i< sizeof( $_POST['order'] ) ; $i++ ) {
                if ( $_POST['columns'][$i]['orderable'] == "true" ) {
                    $sOrder .= $aColumns[ intval( $_POST['order'][$i]['column'] ) ]." ".($_POST['order'][$i]['dir']==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == " ORDER BY" ) {
                $sOrder = " ORDER BY {$sIndexColumn} DESC";
            }
        }
        $sWhere = '';
        if ( isset($_POST['search']) && $_POST['search']['value'] != "" ) {
            
            for ( $i=0 ; $i<count($aColumns) ; $i++ ) {
                if ( isset($_POST['columns'][$i]['searchable']) && $_POST['columns'][$i]['searchable'] == "true" ) {
                    switch ($aColumns[$i]) {
                        case 'id':
                            $sWhere .= " AND (b.id LIKE '%". $_POST['search']['value'] ."%' OR";
                            break;
                        case 'modelo':
                            $sWhere .= " b.modelo LIKE '%". $_POST['search']['value'] ."%' OR";
                            break;
                        case 'combustivel':
                            $sWhere .= " b.combustivel LIKE '%". $_POST['search']['value'] ."%' OR";
                            break;
                        case 'pedido_id':
                            $sWhere .= " b.pedido_id LIKE '%". $_POST['search']['value'] ."%' OR";
                            break;
                        case 'ordem_producao_id':
                            $sWhere .= " b.ordem_producao_id LIKE '%". $_POST['search']['value'] ."%' OR";
                            break;
                        case 'cliente':
                            $sWhere .= " concat(e.cnpj,'-',e.razao_social) LIKE '%". $_POST['search']['value'] ."%') " ;
                            break;                            
                    }
                }
            }
        }

        $sql =  "SELECT b.*, concat(e.cnpj,'-',e.razao_social) as cliente  FROM bombas_nr_serie b 
                INNER JOIN pedidos pe on pe.id = b.pedido_id 
                INNER JOIN orcamentos o on o.id = pe.orcamento_id
                INNER JOIN empresas e on e.id = o.empresa_id
                WHERE 1 = 1 ".$sWhere." ".$sOrder;
                    
        $query = $this->db->query($sql);
        
        $nr_serie = $query->result_array();
        
        $total    = count($nr_serie);
        
        $nr_serie = array_slice($nr_serie, $inicial, $final);  

        foreach ($nr_serie as $key => $serie) {
            $nr_serie[$key]['acoes'] = '<span style="overflow: visible; width: 110px;" class="">                              
                                            <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill afericao" onclick="afericao('.$serie['id'].')" title="data aferição" nr_serie_id="'.$serie['id'].'" target="_blank" >
                                                <i class="la la-list-alt"></i>
                                            </a> 
                                            <button  id="excluir" onclick="qtdLacres('.$serie['id'].');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Qtd. de lacres" nr_serie_id="'.$serie['id'].'">
                                                <i class="la la-certificate"></i>
                                            </button>
                                            <button   onclick="observacao('.$serie['id'].');" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Observação" nr_serie_id="'.$serie['id'].'">
                                                <i class="la la-file-text"></i>
                                            </button>
                                        </span>';
        }
        $sql    = "SELECT COUNT(*) AS Total FROM bombas_nr_serie";
        $query  = $this->db->query($sql);
        $iTotal = $query->row_array();
        $output = array("iTotalRecords" => $iTotal['Total'],
                        "iTotalDisplayRecords" => $total,
                        "aaData" => $nr_serie     ); 
            
        return $output;
    
    }

    public function buscaNrSeriePorId($id){

        $sql =  "   SELECT   b.* FROM bombas_nr_serie b
                    inner join pedidos pe ON  b.pedido_id = pe.id
                    inner join orcamentos o ON o.id = pe.orcamento_id
                    inner join empresas cli on cli.id = o.empresa_id
                    left JOIN produtos p  ON p.modelo = b.modelo
                    inner join (SELECT * FROM pedido_andamentos WHERE status_pedido_id = 6) pa on pe.id = pa.pedido_id where b.id =".$id; 
        return $this->db->query($sql)->row_array();  
    }

    public function excluir($id){
        
        $this->db->where('id', $id);
        
        if($this->db->delete('bombas_nr_serie')){
            return true;
        }else{
            return false;
        }

    }

    public function update($dados)
    {
        $this->db->where('id', $dados['id']);
        if($this->db->update('bombas_nr_serie', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function buscaRastreabilidade($filtros)
    {
        $where="1=1";
        if( $filtros['nr_serie'] != '' ){
            $where.= " and b.id=".$filtros['nr_serie']  ;
        } 

        if( $filtros['dt_ini'] != '' && $filtros['dt_fim'] != ''){
          $where.= " and op.dthr_geracao between '".$filtros['dt_ini']."' and  '". $filtros['dt_fim']."'";  
        }

        $sql = "SELECT DISTINCT b.id, b.pedido_id, concat(cli.cnpj,'-', cli.razao_social) as cliente, b.modelo, b.combustivel, pa.dthr_entrega, p.nr_bicos, b.qtd_lacres,date_format(b.dt_afericao,'%d/%m/%Y') as dt_afericao, date_format(c.fim,'%d/%m/%Y') as startup, b.observacoes
                FROM bombas_nr_serie b 
                INNER JOIN pedidos pe ON b.pedido_id = pe.id 
                INNER JOIN orcamentos o ON o.id = pe.orcamento_id 
                INNER JOIN empresas cli on cli.id = o.empresa_id 
                LEFT JOIN produtos p ON p.modelo = b.modelo 
                LEFT JOIN ordem_producao op ON op.id = b.ordem_producao_id
                LEFT JOIN (SELECT pedido_id, max(dthr_andamento ) as dthr_entrega FROM pedido_andamentos WHERE status_pedido_id = 6 group by pedido_id) pa on pe.id = pa.pedido_id 
                LEFT JOIN (SELECT c.fim, ca.numero_serie FROM chamado c, chamado_atividade ca WHERE c.id = ca.chamado_id and c.tipo_id = 1 and c.fim is not null GROUP by c.id ) as c ON c.numero_serie = b.id 
                WHERE ".$where." 
                ORDER BY 1 ASC, 2 ASC";

        return $this->db->query($sql)->result_array();
    }

    public function buscaNrSeriePorPedido($pedido_id){
        
        $sql =  "   SELECT  DISTINCT b.pedido_id, b.ordem_producao_id, b.modelo, b.codigo, b.descricao, b.combustivel, b.qtd_lacres, b.dt_afericao, b.observacoes,
                             b.dt_inicio_garantia,b.dt_fim_garantia, b.fl_estoque, b.fl_estoque_disp, b.dthr_geracao, concat(b.indicador_rtm,b.id) as id, concat(cli.cnpj,'-',upper(cli.razao_social)) as cliente, p.nr_bicos, date_format(sysdate(),'%y') as ano 
                    FROM bombas_nr_serie b
                    inner join pedidos pe ON  b.pedido_id = pe.id
                    inner join orcamentos o ON o.id = pe.orcamento_id
                    inner join empresas cli on cli.id = o.empresa_id
                    left JOIN produtos p  ON p.modelo = b.modelo
                    WHERE b.pedido_id =".$pedido_id;

        return $this->db->query($sql)->result_array();
        
    }

    public function atualizaGarantia($dados)
    {
        $sql = "SELECT id, dt_inicio_garantia, dt_fim_garantia FROM bombas_nr_serie WHERE pedido_id =".$dados['pedido_id'];
        $rows = $this->db->query($sql)->result_array();
        $this->db->where('pedido_id', $dados['pedido_id']);        
        if($this->db->update('bombas_nr_serie', $dados)){
                    
            foreach( $rows as $row){
                $data = array(  'nr_serie_id'           =>  $row['id'],
                                'dt_garantia_anterior'  =>  $row['dt_fim_garantia'],
                                'dt_garantia_nova'      =>  $dados['dt_fim_garantia'],
                                'usuario_id'            =>  $this->session->userdata('usuario_id') );

                $this->db->insert('garantia_log', $data);                    
                
            }
            return true;
        }else{
            return false;
        }

    }

    public function buscaDadosGarantia($pedido_id)
    {
        $sql = "SELECT  b.id as nr_serie, b.pedido_id, b.modelo, b.codigo, b.descricao, p.nr_nf, p.dt_emissao_nf, e.cnpj, e.razao_social, e.id as empresa_id, b.dt_inicio_garantia, b.dt_fim_garantia
                FROM    bombas_nr_serie b 
                INNER JOIN  pedidos p ON p.id = b.pedido_id 
                INNER JOIN  orcamentos o ON p.orcamento_id = o.id 
                INNER JOIN  empresas e ON o.empresa_id = e.id 
                WHERE b.pedido_id = " .$pedido_id;
        return $this->db->query($sql)->result_array();
    }

    public function buscaNrSeriePorCliente($empresa_id)
    {
        $sql = "SELECT  distinct b.modelo, b.id as nr_serie, b.pedido_id
                FROM    bombas_nr_serie b 
                INNER JOIN  pedidos p       ON  b.pedido_id = p.id
                INNER JOIN  orcamentos o    ON  o.id = p.orcamento_id 
                INNER JOIN  pedido_itens pi on  p.id = pi.pedido_id                 
                WHere o.empresa_id = ".$empresa_id;

        return $this->db->query($sql)->result_array();
        
    }

    public function buscaNrSeriePorPedidoAlterar($pedido_id){
        
        $sql =  "   SELECT  DISTINCT b.id, b.modelo, concat(cli.cnpj,'-',upper(cli.razao_social)) as cliente, date_format(sysdate(),'%y') as ano,
                            IF(b.modelo is null, concat(p.modelo,' - ',pi.produtos), concat('Nr. Serie: ', b.id, ' - ', b.modelo,' - ',b.combustivel)) as label_modelo, if(b.id is null, pi.id, 0) as item_id, pe.status_pedido_id
                    FROM pedidos pe
                    LEFT JOIN bombas_nr_serie b ON  b.pedido_id = pe.id                      
                    INNER JOIN orcamentos o ON o.id = pe.orcamento_id
                    INNER JOIN empresas cli ON cli.id = o.empresa_id
                    INNER JOIN pedido_itens pi ON pi.pedido_id = pe.id 
                    LEFT JOIN produtos p  ON p.id = pi.produto_id
                    WHERE p.tipo_produto_id in (1,2,3,6) and pe.id =".$pedido_id;

        return $this->db->query($sql)->result_array();
        
    }

    public function buscaNrSerieEstoqueDisponivel(){
        
        $sql =  "   SELECT  DISTINCT b.*, concat(cli.cnpj,'-',upper(cli.razao_social)) as cliente, p.nr_bicos, date_format(sysdate(),'%y') as ano 
                    FROM bombas_nr_serie b
                    inner join pedidos pe ON  b.pedido_id = pe.id
                    inner join orcamentos o ON o.id = pe.orcamento_id
                    inner join empresas cli on cli.id = o.empresa_id
                    left JOIN produtos p  ON p.modelo = b.modelo
                    WHERE b.fl_estoque = 1 and b.fl_estoque_disp = 1 ORDER BY b.id ASC";

        return $this->db->query($sql)->result_array();
        
    }

    public function insereAlteracaoNrSerie($dados)
    {
        if($this->db->insert('alteracoes_nr_serie', $dados)){                                    
            
            return true;
        }else{
            return false;
        }
    }



}

?>