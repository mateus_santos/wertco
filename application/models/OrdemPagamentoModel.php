<?php

class OrdemPagamentoModel extends CI_Model {
	
	public function find($id)
	{
		$this->db->select('op.*');
        $this->db->select('c.cliente_id, c.contato_id, c.tipo_id,c.prioridade_id, c.status_id,c.inicio,c.fim,c.usuario_id,c.ordem_pagamento_id');
		$this->db->select('d.descricao despesa');
        $this->db->select('itens.*');
        $this->db->select('concat(e.cnpj," - ",e.razao_social) cliente');
        $this->db->select('e.cnpj,e.razao_social');
		$this->db->where('op.id', $id);
		$this->db->join('ordem_pagamento_itens itens', 'op.id = itens.ordem_pagamento_id');
        $this->db->join('despesas d', 'itens.despesa_id = d.id');
        $this->db->join('chamado c', 'op.chamado_id = c.id', 'left');
        $this->db->join('empresas e', 'c.cliente_id = e.id', 'left');
		return $this->db->get("ordem_pagamento op")->result_array();
	}
		
	
	public function getOpsByChamado($id)
    {
       
        $sql = "SELECT  o.*, t.descricao as tipo, i.qtd, d.descricao as despesa, i.valor, i.id as item_id, i.dt_servico, s.descricao as status, e.email as email_tecnico    
                FROM    ordem_pagamento o, ordem_pagamento_status s, ordem_pagamento_itens i, tipo_despesas t, despesas d, empresas e
				WHERE   e.id = o.favorecido_id and
                        o.id = i.ordem_pagamento_id and 
                        i.despesa_id = d.id and
                        o.status_id = s.id and
                        d.tipo_despesa_id = t.id and 
                        o.chamado_id = ".$id."
                ORDER BY   i.dt_servico ASC";

        $query = $this->db->query($sql);
        return $query->result_array();

    }

	
	public function insereOp ($data) {
        
		return $this->db->insert('ordem_pagamento', $data);
	}

    public function insertItemOp ($data) {
        
        return $this->db->insert('ordem_pagamento_itens', $data);
    }

   
    public function verificaOp($sql)
    {
    	
    	$query = $this->db->query($sql);
    	return $query->row_array();

    }

    public function verificaQtd($id)
    {
        $sql = "SELECT qtd,observacao FROM ordem_pagamento WHERE id = ".$id;
        $query = $this->db->query($sql);
        return $query->row_array();

    }

    
    public function excluirOrdemProducao($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('ordem_pagamento')){
            return true;
        }else{
            return false;
        }
    }

    public function excluirItem($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('ordem_pagamento_itens')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaQtdObs($dados)
    {
        $update = array('qtd' 	        =>	$dados['qtd'],
                        'observacao'    =>  $dados['observacao']);
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('ordem_pagamento', $update)){
            return true;
        }else{
            return false;
        }

    }

    public function atualizarOrdemPagamento($dados)
    { 
        
        $this->db->where('id', $dados['id']);

        if($this->db->update('ordem_pagamento', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function insereOrdemPagtoHistorico($dados){

        $sql    =   "SELECT * FROM ordem_pagamento WHERE id=".$dados['id'];
        $query  =   $this->db->query($sql);
        $row    =   $query->row_array();

        $salvar = array(    "favorecido_id"         =>  $row['favorecido_id'], 
                            "valor_op"              =>  $row['valor_op'],
                            "status_id"             =>  2,
                            "descricao"             =>  $row['descricao'], 
                            "usuario_id"            =>  $dados['usuario_id'],
                            "dt_emissao"            =>  date('Y-m-d H:i:s'),
                            "chamado_id"            =>  $row['chamado_id'],
                            "ordem_pagamento_id"    =>  $dados['id'],
                            "email_emissao"         =>  $dados['email_emissao'] );

        if(  $this->db->insert('ordem_pagamento_historico', $salvar)  ){
            return true;
        }else{
            return false;
        }
    
    }

    public function verificaOpFechadaPorPedido($pedido_id){
        
        $sql = "SELECT count(*) AS total FROM ordem_pagamento WHERE status_op_id != 3 and pedido_id=".$pedido_id;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function getOpsByPedido($pedido_id){

        
        $sql = "SELECT  o.*, p.arquivo as upload_bb 
                FROM    ordem_pagamento o, pedidos p 
                WHERE   o.pedido_id = p.id and o.pedido_id=".$pedido_id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function listaHistoricoOrdemPagto($chamado_id){        
        $sql = "SELECT  o.*, date_format(o.dt_emissao,'%d/%m/%Y %H:%i:%s') as data_emis, u.nome, f.razao_social as favorecido
                FROM    ordem_pagamento_historico o, usuarios u, empresas f
                WHERE   o.usuario_id    =   u.id    and 
                        o.favorecido_id =   f.id    and
                        o.chamado_id    =   ".$chamado_id." GROUP BY o.id ";
                 
        $query = $this->db->query($sql);
        return $query->result_array();

    }

    public function buscaValorTotal($ordem_pagamento_id){
        $sql = "SELECT  o.id, sum(i.valor*i.qtd) as valor_total
                FROM    ordem_pagamento o, ordem_pagamento_itens i
                WHERE   o.id = i.ordem_pagamento_id and o.id = ".$ordem_pagamento_id." GROUP BY o.id";
        $query = $this->db->query($sql);
        return $query->row_array();
    
    }

    public function fechaOps($dados)
    { 
        
        $this->db->where('pedido_id', $dados['pedido_id']);        
        if($this->db->update('ordem_pagamento', $dados)){
            return true;
        }else{
            return false;
        }

    }
    public function buscaItensOp($op_id)
    {
        $sql = "SELECT upper(d.descricao) as item, opi.qtd, concat('R$',format(opi.valor,2,'DE,de')) as valor 
                FROM ordem_pagamento_itens opi 
                INNER join despesas d on d.id = opi.despesa_id 
                WHERE opi.ordem_pagamento_id =".$op_id;

        return $this->db->query($sql)->result_array();
    }

    public function relatorioOrdemPagto($filtros)
    {   
        $where = "";
        if($filtros['dt_fim'] != ''){
            $dt_ini = explode('/',$filtros['dt_ini']);            
            $dt_ini = $dt_ini[2].'-'.$dt_ini[1].'-'.$dt_ini[0];
            $dt_fim = explode('/',$filtros['dt_fim']);
            $dt_fim = $dt_fim[2].'-'.$dt_fim[1].'-'.$dt_fim[0];
            $where.=" and op.dt_emissao between '".$dt_ini."' and '".$dt_fim."' ";
        }

        if($filtros['tipo_id'] != ''){
            $where.=" and c.tipo_id =".$filtros['tipo_id'];               
        }

        if($filtros['cliente_id'] != ''){
            $where.=" and cli.id =".$filtros['cliente_id']; 
        }

        if($filtros['favorecido_id'] != ''){
            $where.=" and tec.id =".$filtros['favorecido_id']; 
        }       

        if($filtros['estado'] != ''){
            $where.=" and cli.estado = '".$filtros['estado']."'"; 
        }


        if($filtros['chamado_id'] != ''){
            $where.=" and c.id = ".$filtros['chamado_id']; 
        }

        if($filtros['regiao'] != ''){
            $where.=" and cli.estado in ".$filtros['regiao']; 
        }        

        $sql = "SELECT  c.id as chamado_id, op.id, op.descricao, ct.descricao as tipo_chamado, concat(upper(tec.razao_social),'-',tec.cnpj) as favorecido, concat(upper(cli.razao_social),'-',cli.cnpj) as cliente, tec.estado as estado_tec, cli.estado as estado_cli, GROUP_CONCAT(distinct d.descricao) as despesas, 
                        format(sum(opi.valor*opi.qtd ),2,'DE_de') as valor, sum(opi.valor*opi.qtd ) as valor_n    
                FROM    ordem_pagamento op
                INNER JOIN ordem_pagamento_itens opi on opi.ordem_pagamento_id = op.id
                INNER JOIN despesas d on d.id = opi.despesa_id
                INNER JOIN empresas tec on tec.id = op.favorecido_id
                INNER JOIN chamado c on c.id = op.chamado_id 
                INNER JOIN empresas cli on cli.id = c.cliente_id
                LEFT JOIN chamado_tipo ct on ct.id = c.tipo_id
                WHERE 1=1 ".$where." 
                GROUP by op.id  
                ORDER BY op.id  DESC";

        return $this->db->query($sql)->result_array();
    }

    public function relatorioChamadoOps($filtros)
    {
        $estado = "";
        if($filtros['estado'] != ''){
            $estado = "e.estado in ".$filtros['estado']." and ";
        }

        if($filtros['regiao'] != ''){
            $estado = "e.estado in ".$filtros['regiao']." and ";
        }

        $sql = "SELECT bombas.*, chamados.total as chamados, format(total_op,2,'de_DE') as total_op, format(vlr_deslocamento,2,'de_DE') as vlr_deslocamento FROM 
            (select e.estado, count(distinct pe.id) as pedidos, sum(pi.qtd) as bombas, sum(pi.qtd*pr.nr_bicos) as bicos from pedidos pe 
            inner join pedido_itens pi  on  pe.id = pi.pedido_id 
            inner join produtos pr      on pr.id = pi.produto_id
            inner join orcamentos o     on  o.id = pe.orcamento_id
            inner join empresas e       on  e.id = o.empresa_id 
            where ".$estado." pe.dthr_geracao BETWEEN '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."' and pr.tipo_produto_id in (1,2,3,6) and pe.status_pedido_id <> 7
            group by e.estado) AS bombas 
            /* aqui vão os chamados */
            left join (select e.estado, count(DISTINCT c.id) as total, COALESCE(sum(op.vlr_toral),0) as total_op, COALESCE(sum(op.vl_deslocamento),0) as vlr_deslocamento 
            from chamado c 
            inner join empresas e on e.id = c.cliente_id
            left join (SELECT distinct  op.id, op.chamado_id,  sum(opi.qtd*opi.valor) as vlr_toral, op.valor_op as total, sum( if( opi.despesa_id in (12,16,22,20,14,18,11,15,27,31,33,34,26,30), opi.qtd*opi.valor,0 ) ) as vl_deslocamento from ordem_pagamento op 
            inner join ordem_pagamento_itens opi on opi.ordem_pagamento_id = op.id
            group by op.id ) as op on op.chamado_id = c.id 
            where ".$estado." c.inicio BETWEEN  '".$filtros['dt_ini']."' and '".$filtros['dt_fim']."' and c.status_id <> 4 group by e.estado) as chamados 
            ON bombas.estado = chamados.estado"; 

        return $this->db->query($sql)->result_array();
    
    }

}
?>
