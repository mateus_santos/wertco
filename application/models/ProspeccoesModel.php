<?php

class ProspeccoesModel extends CI_Model {
    
	public function buscaProspecPorUsuario($usuario_id){ 
        $sql = " SELECT p.*, ps.descricao as status, concat(e.cnpj,'-',e.razao_social) as cliente, e.estado, date_format(p.dthr_abertura,'%d/%m/%Y %H:%i:%s') as data_abertura
                 FROM   prospeccoes p, prospeccao_status ps, empresas e
                 WHERE  p.status_id     =   ps.id   and  
                        p.empresa_id    =   e.id    and  
                        p.usuario_id    =   ".$usuario_id." ORDER BY p.status_id ASC";
                         
        $query  =   $this->db->query($sql);
        return  $query->result_array();                                   
    }

    public function buscaProspeccao($prospeccao_id)
    {
        $sql = "SELECT p.*, concat(u.nome,'-',date_format(p.dthr_abertura,'%d/%m/%Y %H%i%s')) as usuario, ps.descricao as status, concat(e.cnpj,'-',e.razao_social) as cliente, e.estado, e.id as cliente_id, u.nome
                 FROM   prospeccoes p, prospeccao_status ps, empresas e, usuarios u
                 WHERE  p.status_id     =   ps.id   and  
                        p.empresa_id    =   e.id    and  
                        p.usuario_id    =   u.id    and 
                        p.id            =   ".$prospeccao_id;

        return $this->db->query($sql)->row_array();

    }

    public function buscaAtividades($prospeccao_id)
    {
        $sql = "SELECT  pa.* 
                FROM    prospeccoes p, prospeccao_atividades pa 
                WHERE   p.id = pa.prospeccao_id and 
                        p.id =". $prospeccao_id;

        return $this->db->query($sql)->result_array();
    
    }

    public function insert($insert)
	{
        $this->db->insert('prospeccoes', $insert);
		return $this->db->insert_id();
    }

    public function update($dados)
    {                
        $this->db->where('id', $dados['id']);
      
        if($this->db->update('prospeccoes', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function buscaStatusProspeccao()
    {
        $sql = "select * from prospeccao_status";
        return $this->db->query($sql)->result_array();
    }

    public function insertAtividade($insert)
    {
        $this->db->insert('prospeccao_atividades', $insert);
        return $this->db->insert_id();
    }

    public function insertStatus($insert)
    {
        $this->db->insert('prospeccao_status', $insert);
        return $this->db->insert_id();
    }

    public function excluirStatusProspeccao($id){
        
        $this->db->where('id', $id);
        if($this->db->delete('prospeccao_status')){
            return true;
        }else{
            return false;
        }
    }

    public function atualizarStatus($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('prospeccao_status', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function excluirStatus($dados)
    {
                
        $this->db->where('id',$dados);        
        if($this->db->delete('prospeccao_status')){
            return true;
        }else{
            return false;
        }

    }    

    public function atualizarAtividade($dados)
    {
                
        $this->db->where('id', $dados['id']);
        
        if($this->db->update('prospeccao_atividades', $dados)){
            return true;
        }else{
            return false;
        }

    }

    public function buscaAtividade($atividade_id)
    {
        $sql="SELECT * FROM prospeccao_atividades WHERE id=".$atividade_id;
        return $this->db->query($sql)->row_array();
    }

    public function buscaProspeccaoStatus()
    {
        $sql="SELECT * FROM prospeccao_status";
        return $this->db->query($sql)->result_array();
    }
    
    public function buscaStatusPorId($id)
    {
        $sql="SELECT * FROM prospeccao_status WHERE id=".$id;
        return $this->db->query($sql)->row_array();
    }

    


}

?>