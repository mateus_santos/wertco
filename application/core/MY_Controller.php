<?php

class MY_Controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('LogModel','logM');
		$this->load->model('orcamentosAlertaModel','orcAlerta');
	}
	
	public function _load_view($page,$data = null) {
	    $data['view'] 		= 	$page;
	    $usuario_id 		= 	$this->session->userdata('usuario_id');
	    if($this->session->userdata('tipo_acesso') == 'administrador geral'){
	    	$data['alertas']	=	$this->orcAlerta->selectTodosAlertas($usuario_id); 
	    	$data['naolidos'] 	= 	$this->orcAlerta->selectTodosNovosAlertas($usuario_id);
	    }else{
	    	$data['alertas']	=	$this->orcAlerta->selectAlertas($usuario_id); 
	    	$data['naolidos'] 	= 	$this->orcAlerta->selectNovosAlertas($usuario_id);
	    }
	    
	    //header
	    $this->load->view('inc/header',$data);
	    //menu
	    $this->load->view('inc/navigation',$data);
	    //view
	    $this->load->view($page,$data);
	    //footer
		$this->load->view('inc/footer',$data);
	
	}

	//Carregar template expopostos
	public function _load_view_feira($page,$data = null) {
	    $data['view'] = $page; //css
	    //header
	    $this->load->view('inc/header_feira',$data); 
	    //menu
	    $this->load->view('inc/navigation',$data); 
	    //view
	    $this->load->view($page,$data); 
	    //footer
	    $this->load->view('inc/footer_feira',$data); 
	}

	public function _is_logged()
    {
        $user = $this->session->userdata('logged_in');
        return isset($user);
    }

    public function makeThumbnail($sourcefile,$max_width, $max_height, $endfile, $type){
		// Takes the sourcefile (path/to/image.jpg) and makes a thumbnail from it
		// and places it at endfile (path/to/thumb.jpg).
		// Load image and get image size.		   		   
		switch($type){
			case'image/png':
				$img = imagecreatefrompng($sourcefile);
				break;
			case'image/jpeg':
				$img = imagecreatefromjpeg($sourcefile);
				break;
			case'image/gif':
				$img = imagecreatefromgif($sourcefile);
				break;
			default : 
			return 'Un supported format';
		}

		$width = imagesx( $img );
		$height = imagesy( $img );

		if ($width > $height) {
		    if($width < $max_width)
				$newwidth = $width;
			
			else
			
		    $newwidth = $max_width;	
			
			
		    $divisor = $width / $newwidth;
		    $newheight = floor( $height / $divisor);
		}
		else {
			
			 if($height < $max_height)
		         $newheight = $height;
		     else
				 $newheight =  $max_height;
			 
		    $divisor = $height / $newheight;
		    $newwidth = floor( $width / $divisor );
		}

		// Create a new temporary image.
		$tmpimg = imagecreatetruecolor( $newwidth, $newheight );

		imagealphablending($tmpimg, false);
		imagesavealpha($tmpimg, true);
			
		// Copy and resize old image into new image.
		$return = imagecopyresampled( $tmpimg, $img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height );

		// Save thumbnail into a file.
		//compressing the file
		switch($type){
			case'image/png':
				imagepng($tmpimg, $endfile, 0);
				break;
			case'image/jpeg':
				imagejpeg($tmpimg, $endfile, 100);
				break;
			case'image/gif':
				imagegif($tmpimg, $endfile, 0);
				break;
		}

		// release the memory
		imagedestroy($tmpimg);
		imagedestroy($img);

		return $return;
	}

	public function log($controller, $model, $action, $usuario_alteracao, $campos_alterados, $valores_alterados, $ip){
		$insertLog = array( 'controller'=>	$controller,
							'model' 	=>	$model,
							'acao'		=>	$action,
							'user_id'	=> 	$usuario_alteracao,
							'ip'		=> 	$ip);

		if($this->logM->insereLog($insertLog)){
			if( is_array($campos_alterados) && is_array($valores_alterados) )
			{
				$log_id  = $this->db->insert_id();
				foreach ($campos_alterados as $key => $value) {
					$insert = array('campo'		=> 	$key,
									'novo_valor' 	=> 	$value,
									'log_id' 	=>	$log_id);

					$this->logM->insereLogCampos($insert);
				}
			}
		}

	}
	
	public function ConsultaCNPJRF($cnpj){
	    $url = 'http://webservice.keyconsultas.net/receita/cnpj/?cnpj='.$cnpj.'&token=BRK018cb-5943-467b-86cc-3fac8033578c';
	            
	    $consulta_str = $this->Executa_URL($url);
	    
	    
	    $consulta = json_decode($consulta_str);
	    if ($consulta)
	    {
	        if($consulta->{'code'} == '1'){
	            $dir = $this->SaveConsultaRF($consulta,$cnpj);
	    
	            $consulta->{'Save_Receita'} = $dir;
	        } else {
	            //sleep(3);
	        }

	        return $consulta;
	    }

	    else return null;
	}

	public function SaveConsultaRF($consulta,$cnpj)
	{
		
	    $dir = getcwd()."/Consultas/Receita_Federal/Receita_" . $cnpj . "_" . date("d-m-Y").".html";
	    //$dir = "C:/xampp/htdocs/wertco-site/Consultas/Receita_Federal/Receita_" . $cnpj . "_" . date("d-m-Y").".html";
	    $file = fopen($dir, "w");
	    fwrite($file, $consulta->{'html'});
	    fclose($file);
	    return $dir;
	}

	public function ConsultaCNPJSintegra($cnpj,$estado){
	    $url = 'http://webservice.keyconsultas.net/sintegra_'.strtolower($estado).'/cnpj/?cnpj='.$cnpj.'&token=BRK018cb-5943-467b-86cc-3fac8033578c';
	            
	    $consulta_str = $this->Executa_URL($url);
	    $consulta = json_decode($consulta_str);
	    
	    $dir = $this->SaveConsultaSintegra($consulta,$cnpj);
	    
	    $consulta->{'Save_Sintegra'} = $dir;
	    
	    return $consulta;
	}

	public function SaveConsultaSintegra($consulta,$cnpj)
	{
	    $dir = base_url("Consultas\Sintegra\Sintegra_" . $cnpj . "_" . date("d-m-Y").".html");
		//$dir = "C:/xampp/htdocs/wertco-site/Consultas/Sintegra/Sintegra_" . $cnpj . "_" . date("d-m-Y");
	    if (isset($consulta->{'data'}->{'certificado_url'}))
	    {
	        $dir = $dir . ".pdf";
	        copy($consulta->{'data'}->{'certificado_url'}, $dir);
	    }
	    else
	    {
	        if (isset($consulta->{'html'})){
	            $dir = $dir . ".html";
	            $file = fopen($dir, "w");
	            fwrite($file, $consulta->{'html'});
	            fclose($file);
	        }
	    }
	    
	    return $dir;
	}

	private function Executa_URL($url)
	{
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $ret_str = curl_exec($ch);
	    curl_close($ch);
	    
	    return $ret_str;
	}


	public function Executa_URL_https($url)
	{
	    $ch = curl_init();
	    curl_reset($ch);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false); // adiciona https
	    $ret_str = curl_exec($ch);	    
	    curl_close($ch);	    
	    return json_decode($ret_str);
	}

}
