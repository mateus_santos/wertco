<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends CI_Controller {

	
	public function wertcoExpopostosConveniencia()
	{
		$this->load->view('noticias/wertco-expopostos-conveniencia');
	}

	public function wertcoIngressaNoMercadoBrasileiro()
	{
		$this->load->view('noticias/wertco-ingressa-no-mercado-brasileiro');
	}

	public function minaspetro()
	{
		$this->load->view('noticias/minaspetro');
	}

	public function wertcoEaNovaEmpresaDeCombustiveisCompanytec()
	{
		$this->load->view('noticias/wertco-e-a-nova-empresa-companytec');
	}

	public function nacs()
	{
		$this->load->view('noticias/nacs');
	}

	public function ascoval()
	{
		$this->load->view('noticias/ascoval');
	}

	public function anpPedeExplicacoesDeDistribuidoras()
	{
		$this->load->view('noticias/anp-pede-explicacoes-de-distribuidoras');
	}

	public function saibaQuaisSaoAsAlteracoesNoRegulamentoTecnico()
	{
		$this->load->view('noticias/inmetro');
	}

	public function etanolRecuaEm15Estados()
	{
		$this->load->view('noticias/etanol-recua-em-15-Estados');
	}	

	public function petrobrasTeraAnoDeDefinicoes()
	{
		$this->load->view('noticias/petrobras-tera-ano-de-definicoes');
	}

	public function petrobrasQuerLancarCartao()
	{
		$this->load->view('noticias/petrobras-quer-lancar-cartao');
	}

	public function anpFazAudienciaPublica()
	{
		$this->load->view('noticias/anp-faz-audiencia-publica');
	}

	public function WertcoeAbiepsIniciam()
	{
		$this->load->view('noticias/wertco-e-abieps-iniciam');
	}

	public function WertcoeCompanytecExpopostos()
	{
		$this->load->view('noticias/wertco-companytec-expopostos');
	}

	public function concursoPostoBonito()
	{
		$this->load->view('noticias/xx-concurso-2019');
	}

	public function brasilTemMaiorNrlacradas()
	{
		$this->load->view('noticias/brasil-tem-maior');
	}

	public function exponline()
	{
		$this->load->view('noticias/exponline');
	}

	public function politicaPrivacidade()
	{
		$this->load->view('noticias/politica-privacidade');
	}	

	public function wertcoSurpreendeMercado()
	{
		$this->load->view('noticias/wertco-surpreende-mercado');
	}

	/* BLOG DE NOTÍCIAS */
	public function blog()
	{
		$this->load->view('noticias/noticias');
	}
	
} 