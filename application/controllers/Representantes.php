<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Representantes extends CI_Controller {
	public function __construct() {
		parent::__construct();

		// carrega a model a ser utilizada neste controller
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->helper('form');

	}

	public function cadastro()
	{
		$this->load->view('representantes/cadastro-representantes');
	}

	public function verifica_cnpj()
	{ 
		
		$result = $this->empresasM->getCnpj($_POST['cnpj']);

		echo json_encode($result);		
	}

	public function verifica_cpf(){

		$result = $this->usuariosM->getCpf($_POST['cpf']);
		
		if( $result[0]->total == 0){
			if( !$this->valida_cpf($_POST['cpf']) ){
				echo json_encode(array('status' => 'erro',
									 'mensagem' => 'Cpf inválido.'));	
			}
			
		}else{
			echo json_encode(array('status' => 'erro',
									'mensagem' => 'Cpf já cadastrado.'));
		}
		
		
	}	

	public function add()
	{

		if( $this->input->post('salvar') == '1'){
			
			$data_explode 		= 	explode('/',$this->input->post('data_constituicao'));
			$data_constituicao 	= 	$data_explode[2].'-'.$data_explode[1].'-'.$data_explode[0]; 
			$dadosEmpresa = array(
		        'razao_social' 			=>	$this->input->post('razao_social'),
				'fantasia' 				=>	$this->input->post('fantasia'),
				'cnpj' 					=>	$this->input->post('cnpj'),
				'telefone' 				=>	$this->input->post('telefone'),
				'endereco' 				=>	$this->input->post('endereco'),
				'email' 				=>	$this->input->post('email'),
				'regiao_atua' 			=>	$this->input->post('regiao_atua'),
				'cidade' 				=>	$this->input->post('cidade'),
				'estado'				=>	$this->input->post('estado'),
				'pais'					=>	$this->input->post('pais'),
				'nr_funcionarios' 		=> 	$this->input->post('nr_funcionarios'),
				'data_constituicao' 	=>	$data_constituicao,
				'representante_legal'	=> 	$this->input->post('representante_legal'),
				'tipo_cadastro_id'		=>	2,
				'bairro'				=> 	$this->input->post('bairro')
		    );
			
			$conteudo = '';
			$conteudo.= "Razão Social: <b>". $this->input->post('razao_social').' </b>| ';
			$conteudo.=	"Fantasia: <b>".$this->input->post('fantasia').' </b>| ';
			$conteudo.=	'CNPJ: <b>'.$this->input->post('cnpj').' </b>| ';
			$conteudo.=	'Telefone: <b>'.	$this->input->post('telefone').' </b>| ';
			$conteudo.=	'Endereço: <b>'.	$this->input->post('endereco').' </b>| ';			
			$conteudo.=	'Cidade: <b>'	.$this->input->post('cidade').' </b>| ';
			$conteudo.=	'Estado: <b>'	.	$this->input->post('estado').' </b>| ';
			$conteudo.=	'País: <b>'.	$this->input->post('pais').' </b>| ';		
			$conteudo.=	'Nº de Funcionários: <b>'.	$this->input->post('nr_funcionarios').' </b>| ';
			$conteudo.=	'Data Constituição: <b>'.$this->input->post('data_constituicao').' </b>| ';
			$conteudo.=	'Repreentante Legal: <b>'.$this->input->post('representante_legal').' </b>| ';
						
			if($this->empresasM->add($dadosEmpresa))
			{		
				$empresa_id = $this->db->insert_id();			
			}
 		}else{
 			$empresa_id = $this->input->post('empresa_id');
 		}

		if($empresa_id != ''){

			$dadosUsuarios = array(
		        'nome' 				=>	$this->input->post('pessoal_nome'),
				'cpf' 				=>	$this->input->post('pessoal_cpf'),
				'telefone' 			=>	$this->input->post('pessoal_telefone'),
				'email' 			=>	$this->input->post('pessoal_email'),
				'endereco' 			=>	$this->input->post('pessoal_endereco'),				
				'empresa_id'		=>	$empresa_id,
				'tipo_cadastro_id'	=>	2,
				'senha'				=>	md5(md5($this->input->post('senha')))
		    );

			if($this->usuariosM->add($dadosUsuarios)){

				$conteudo.=	" <br/>Dados pessoais: <br/>";
				$conteudo.=	'Nome: <b>' 			.	$this->input->post('pessoal_nome').' </b>| ';
				$conteudo.=	'CPF: <b>'				.	$this->input->post('pessoal_cpf').' </b>| ';
				$conteudo.=	'Telefone: <b>'			.	$this->input->post('pessoal_telefone').' </b>| ';
				$conteudo.=	'E-mail Pessoal: <b>'	.	$this->input->post('pessoal_email').' </b>| ';				
				$conteudo.=	'empresa_id: <b>'		.	$empresa_id;
				
				$this->enviaEmail('vendas@wertco.com.br','Cadastro de Representante - Wertco.com.br',$conteudo);
				$this->enviaEmail($this->input->post('pessoal_email'),'WERTCO - Cadastro Efetuado com Sucesso','Neste momento seu cadastro está em análise e assim que possível lhe retornaremos um email, informando a liberação de seu acesso a área restrita da WERTCO!');

				$this->session->set_flashdata('sucesso', 'ok.');
				
			}else{
				$this->session->set_flashdata('erro', 'erro.');
				
			}

		}else{
			$this->session->set_flashdata('erro', 'erro.');
			
		}
		
		$this->load->view('representantes/cadastro-representantes');		
	}

	private function enviaEmail($destinatario, $titulo = NULL, $conteudo = NULL  )
	{
		$this->load->library('email');
		// Get full html:
		$body = '<html>
					<head></head>
					<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
						<div style="width: 80%;padding-top: 215px;position: relative;text-align: center;margin-left: 62px;">
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">'.$titulo.'</h1>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">'.$conteudo.'</p>
						</div>
					</body>
				</html>';
		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    //->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
		    ->to($destinatario)
		    ->subject($titulo)
		    ->message($body)
		    ->send();

		return $result;		
		
	}

	private function valida_cpf($cpf){

 
	    // Verifica se um número foi informado
	    if(empty($cpf)) {
	        return false;
	    }
	 
	    // Elimina possivel mascara
	    $cpf = str_replace('.', '', str_replace('-','',$cpf));	    
	     
	    // Verifica se o numero de digitos informados é igual a 11 
	    if (strlen($cpf) != 11) {
	        return false;
	    }
	    // Verifica se nenhuma das sequências invalidas abaixo 
	    // foi digitada. Caso afirmativo, retorna falso
	    else if ($cpf == '00000000000' || 
	        $cpf == '11111111111' || 
	        $cpf == '22222222222' || 
	        $cpf == '33333333333' || 
	        $cpf == '44444444444' || 
	        $cpf == '55555555555' || 
	        $cpf == '66666666666' || 
	        $cpf == '77777777777' || 
	        $cpf == '88888888888' || 
	        $cpf == '99999999999') {
	        return false;
	     // Calcula os digitos verificadores para verificar se o
	     // CPF é válido
	     } else {   
	         
	        for ($t = 9; $t < 11; $t++) {
	             
	            for ($d = 0, $c = 0; $c < $t; $c++) {
	                $d += $cpf{$c} * (($t + 1) - $c);
	            }
	            $d = ((10 * $d) % 11) % 10;
	            if ($cpf{$c} != $d) {
	                return false;
	            }
	        }
	 
	        return true;
	    }
		
	}	

}