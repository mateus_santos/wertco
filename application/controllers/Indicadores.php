<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indicadores extends CI_Controller {
	public function __construct() {
		parent::__construct();

		// carrega a model a ser utilizada neste controller
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->helper('form');

	}

	public function cadastro()
	{
		$this->load->view('indicadores/cadastro-indicadores');
	}

	public function verifica_cnpj()
	{
		
		$result = $this->empresasM->getCnpj($_POST['cnpj']);

		echo json_encode($result);		
	}

	public function verifica_cpf(){

		$result = $this->usuariosM->getCpf($_POST['cpf']);
		
		if( $result[0]->total == 0){
			if( !$this->valida_cpf($_POST['cpf']) ){
				echo json_encode(array('status' => 'erro',
									 'mensagem' => 'Cpf inválido.'));	
			}
			
		}else{
			echo json_encode(array(	'status' 	=>	'erro',
									'mensagem'	=>	'Cpf já cadastrado.'));
		}
		
		
	}	

	public function verifica_email(){

		$result = $this->usuariosM->getEmail($_POST['email']);
		
		if( $result[0]->total > 0){
			echo json_encode(array(	'status' 	=>	'erro',
									'mensagem'	=>	'E-mail já cadastrado.'));
		
		}
		
		
	}	

	public function add()
	{
		$conteudo = '';
		if( $this->input->post('salvar') == '1'){
			
			$dadosEmpresa = array(
		        'razao_social' 		=>	$this->input->post('razao_social'),
				'fantasia' 			=>	$this->input->post('fantasia'),
				'cnpj' 				=>	$this->input->post('cnpj'),
				'telefone' 			=>	$this->input->post('telefone'),
				'endereco' 			=>	$this->input->post('endereco'),
				'email' 			=>	$this->input->post('email'),				
				'cidade' 			=>	$this->input->post('cidade'),
				'estado' 			=>	$this->input->post('estado'),
				'tipo_cadastro_id'	=> 	6,
				'bairro'			=> 	$this->input->post('bairro'),
				'cep'				=> 	$this->input->post('cep'),
				'cartao_cnpj'		=>	$this->input->post('cartao_cnpj')
		    );
			
			
			$conteudo.= "Razão Social: ". 	$this->input->post('razao_social')." | ";
			$conteudo.=	'Fantasia: '.	$this->input->post('fantasia').' | ';
			$conteudo.=	'CNPJ: '.	$this->input->post('cnpj').' | ';
			$conteudo.=	'Telefone: '.	$this->input->post('telefone').' | ';
			$conteudo.=	'Endereço: '.	$this->input->post('endereco').' | ';
			$conteudo.=	'Cidade: '	.	$this->input->post('cidade').' | ';
			$conteudo.=	'Estado: '	.	$this->input->post('estado').' | ';
			$conteudo.=	'País: '.	$this->input->post('pais').' | ';	

			if($this->empresasM->add($dadosEmpresa))
			{		
				$empresa_id = $this->db->insert_id();			
			}
 		}else{
 			
 			$empresa_id = $this->input->post('empresa_id');
 		}

		if($empresa_id != ''){

			$dadosUsuarios = array(
		        'nome' 				=>	$this->input->post('pessoal_nome'),
				'cpf' 				=>	$this->input->post('pessoal_cpf'),
				'telefone' 			=>	$this->input->post('pessoal_telefone'),
				'celular' 			=>	$this->input->post('pessoal_celular'), 
				'email' 			=>	$this->input->post('pessoal_email'),
				'endereco' 			=>	$this->input->post('pessoal_endereco'),				
				'empresa_id'		=>	$empresa_id,
				'tipo_cadastro_id'	=>	6,
				'senha'				=>	md5(md5($this->input->post('senha')))
		    );

			if($this->usuariosM->add($dadosUsuarios)){

				$conteudo.=	"<br/>Dados pessoais<br/>";
				$conteudo.=	'Nome: ' 			.	$this->input->post('pessoal_nome').' | ';
				$conteudo.=	'CPF: '				.	$this->input->post('pessoal_cpf').' | ';
				$conteudo.=	'Telefone: '			.	$this->input->post('pessoal_telefone').' | ';
				$conteudo.=	'E-mail Pessoal: '	.	$this->input->post('pessoal_email').' | ';
				$conteudo.=	'Endereço: ' 		.	$this->input->post('pessoal_endereco').' | ';
				$conteudo.=	'<p>empresa_id: '		.	$empresa_id;
				
				$this->enviaEmail('vendas1@wertco.com.br','Cadastro de Indicadores - Wertco.com.br',$conteudo);
				$this->enviaEmail('marketing@companytec.com.br','Cadastro de Indicadores - Wertco.com.br',$conteudo);
				$this->enviaEmail($this->input->post('pessoal_email'),'WERTCO - Cadastro Efetuado com Sucesso','Neste momento seu cadastro está em análise e assim que possível lhe retornaremos um email, informando a liberação de seu acesso a área restrita da WERTCO!');

				$this->session->set_flashdata('sucesso', 'ok.');
				
			}else{
				$this->session->set_flashdata('erro', 'erro.');
				
			}

		}else{
			$this->session->set_flashdata('erro', 'erro.');
			
		}
		
		redirect('indicadores/cadastro');		
	}

	private function enviaEmail($destinatario, $titulo = NULL, $conteudo = NULL  )
	{
		$this->load->library('email');
		// Get full html:
		$body = '<html>
					<head></head>
					<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
						<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">'.$titulo.'</h1>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">'.$conteudo.'</p>
						</div>
					</body>
				</html>';

		$result = $this->email
		    ->from('webmaster@wertco.com.br')		    
		    ->to($destinatario)
		    ->subject($titulo)
		    ->message($body)
		    ->send();

		return $result;		
		
	}

	private function valida_cpf($cpf){

 
	    // Verifica se um número foi informado
	    if(empty($cpf)) {
	        return false;
	    }
	 
	    // Elimina possivel mascara
	    $cpf = str_replace('.', '', str_replace('-','',$cpf));	    
	     
	    // Verifica se o numero de digitos informados é igual a 11 
	    if (strlen($cpf) != 11) {
	        return false;
	    }
	    // Verifica se nenhuma das sequências invalidas abaixo 
	    // foi digitada. Caso afirmativo, retorna falso
	    else if ($cpf == '00000000000' || 
	        $cpf == '11111111111' || 
	        $cpf == '22222222222' || 
	        $cpf == '33333333333' || 
	        $cpf == '44444444444' || 
	        $cpf == '55555555555' || 
	        $cpf == '66666666666' || 
	        $cpf == '77777777777' || 
	        $cpf == '88888888888' || 
	        $cpf == '99999999999') {
	        return false;
	     // Calcula os digitos verificadores para verificar se o
	     // CPF é válido
	     } else {   
	         
	        for ($t = 9; $t < 11; $t++) {
	             
	            for ($d = 0, $c = 0; $c < $t; $c++) {
	                $d += $cpf[$c] * (($t + 1) - $c);
	            }
	            $d = ((10 * $d) % 11) % 10;
	            if ($cpf[$c] != $d) {
	                return false;
	            }
	        }
	 
	        return true;
	    }
		
	}	

}