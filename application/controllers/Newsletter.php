<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {
	public function __construct() {
		parent::__construct();

		// carrega a model a ser utilizada neste controller
		$this->load->model('NewsletterModel','newsletterM');

	}

	public function cadastra()
	{
		$dados = $this->newsletterM->getTotalEmail($_POST['email']);

		if($dados[0]->total > 0){
			echo json_encode(array('retorno' => 'E-mail já cadastrado'));
		}else{
			$insere = array('email' => $_POST['email'] );
			if($this->newsletterM->add($insere))
			{

				if($this->enviaEmail('webmaster@companytec.com.br','E-mail cadastrado no site','E-mail '.$_POST['email'].' cadastrado no site.' ))
				{
					echo json_encode(array('retorno' => 'sucesso'));		
				}else{
					echo json_encode(array('retorno' => 'E-mail não enviado'));		
				}
				
			}else{
				echo json_encode(array('retorno' => 'Erro de inserção'));		
			}

		}

	}

	private function enviaEmail($destinatario, $titulo = NULL, $conteudo = NULL  )
	{
		$this->load->library('email');
		// Get full html:
		$body = '<html>
					<head>
						<title>'.$titulo.'</title>
					</head>
				<body background="#ffcc00"> 						

					<img src="http://www.wertco.com.br/wertcofundoescuro.png" />

					<h2>'.$titulo.'</h2>

					<div>

						'.$conteudo.'

					</div>

					</body>

					</html>';
		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    //->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
		    ->to('webmaster@wertco.com.br')
		    ->subject($titulo)
		    ->message($body)
		    ->send();

		return $result;		

		
	}
	
}