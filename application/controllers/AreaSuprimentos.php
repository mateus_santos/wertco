<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class AreaSuprimentos extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'suprimentos' && $this->session->userdata('tipo_acesso') != 'administrador geral' && $this->session->userdata('tipo_acesso') != 'administrador tecnico' && $this->session->userdata('tipo_acesso') != 'suporte' && $this->session->userdata('tipo_acesso') != 'qualidade') ) 
		{
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('ChamadoModel', 'chamadosM');
		$this->load->model('ProdutosModel', 'produtosM');
		$this->load->model('PedidosModel', 'pedidosM');

	}

	public function index()
	{
		$parametros			 	=	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();		
		$parametros['title']	=	"Área de Suprimentos";		
		$this->_load_view('area-suprimentos/index',$parametros);

	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados'] 				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-financeiro/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-financeiro/editar-cliente',	$parametros );	
		}
	
	}

	public function relatorioPrevisaoVendas(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Relatório Pedidos";
		$parametros['modelos']	=	$this->produtosM->select();

		$this->_load_view('area-suprimentos/relatorios/previsao-vendas',$parametros );

    }

    public function gerarRelatoriPrevisaoVendas(){
    	    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-previsao-vendas-pdf.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
	    
	    $filtros = array(	'produto_id' 		=> 		$this->input->post('produto_id')	); 
	    
	    $data['dados']	=	$this->pedidosM->getRelatorioPrevisaoVendas($filtros);
	    $mes_fim = date('Y-m-d');
	    $mes_ini = date('Y-m-d', strtotime('-2 Month'));
		$data['media']	= 	$this->pedidosM->pedidosConfirmados3meses($mes_ini,$mes_fim);		
		$this->html2pdf->html(utf8_decode($this->load->view('/area-suprimentos/relatorios/previsao-vendas-pdf', $data,true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-previsao-vendas-pdf.pdf'));
	    
		}
    }

    public function relatorioProgramacaoSemanalPorModelo(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Relatório da Programação Semanal por Modelo";
		$this->_load_view('area-suprimentos/relatorios/relatorio-bombas-producao-modelo',$parametros );
    }

    public function gerarRelatorioProgramacaoSemanalPorModelo(){
    	    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-bombas-producao-modelo-pdf.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
		
	    $dt_ini = '';
	    $dt_fim = '';
	    $data['semana'] = '';
	    if($this->input->post('dt_ini') != '' && $this->input->post('dt_fim') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_ini'));
	    	$dt_ini 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    	$explodeF 	= 	explode('/',$this->input->post('dt_fim'));	    	
	    	$dt_fim 	= 	$explodeF[2].'-'.$explodeF[1].'-'.$explodeF[0];
	    	//$dt_fim 	=	date('Y-m-d', strtotime('+4 days',strtotime($dt_ini)));
	    	//$fim 		= 	date('d/m/Y', strtotime('+4 days',strtotime($dt_ini)));
	    	$data['semana'] = 'Semana: '.date('W', strtotime($dt_ini) ).' - '. $this->input->post('dt_ini').' a Semana: '.date('W', strtotime($dt_fim) ).' - '.$this->input->post('dt_fim') ;
	    }	    

	    $filtros = array(	'dt_ini'			=> 	$dt_ini,
	    					'dt_fim'			=> 	$dt_fim 	); 
	    
	    $data['dados']	=	$this->pedidosM->relatorioProgramacaoSemanalPorModelo($filtros);
		//$this->load->view('/area-qualidade/relatorios/relatorio-rastreabilidade-pdf', $data);				
		$this->html2pdf->html(utf8_decode($this->load->view('/area-suprimentos/relatorios/relatorio-bombas-producao-modelo-pdf', $data,true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-bombas-producao-modelo-pdf.pdf'));
	    
		}
    }

}