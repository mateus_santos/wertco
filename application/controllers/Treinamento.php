<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Treinamento extends CI_Controller {
	public function __construct() {
		parent::__construct();

		// carrega a model a ser utilizada neste controller		
	    $this->load->helper(array('form', 'url'));
	    $this->load->model('TreinamentoTecnicoModel','treinamentoM');
	    $this->load->model('EmpresasModel','empresasM');
	    $this->load->model('UsuariosModel','usuariosM');

	}

	public function inscricao($id)
	{
		//Limpa sessão
		unset($_SESSION['erro']);
		unset($_SESSION['sucesso']);		
		
		$total_aprovados	=	$this->treinamentoM->getAprovadosInscritos(base64_decode($id));		
		$dados 				= 	$this->treinamentoM->getTreinamento(base64_decode($id)); 
		
		if($dados[0]['fl_externo'] == 0){
			$this->load->view('formularios/treinamento-tecnico',array(	'dados' 			=> 	$dados,
																		'total_aprovados'	=> 	$total_aprovados ));	
		}else{
			$this->load->view('formularios/treinamento-tecnico-externo',array(	'dados' 			=> 	$dados,
																				'total_aprovados'	=> 	$total_aprovados	));	
		}
	}



	public function bahia($id)
	{
		//Limpa sessão
		unset($_SESSION['erro']);
		unset($_SESSION['sucesso']);
		$dados = $this->treinamentoM->getTreinamento($id);
		$this->load->view('formularios/treinamento-tecnico',array('dados' 	=> 	$dados));
	}

	public function verifica_cpf(){

		if( !$this->valida_cpf($_POST['cpf']) ){

			echo json_encode(array(	'status' 	=> 	'erro',
									'mensagem' 	=>	'Cpf inválido.'));				
		}else{

			$dados = $this->treinamentoM->verificaCadastro($_POST['cpf']);	
			
			if( isset($dados->email) ){

				$total = $this->treinamentoM->verificaInscricao($dados->id, $_POST['id']);	
				
				if( $total > 0 ){
					echo json_encode(array(	'status' 	=> 	'erro',
											'mensagem' 	=>	'Cpf Já cadastrado no treinamento.'));						
				}else{
					echo json_encode($dados);	
				}
			}			
			
		}

	}

	public function uploadArquivo(){

		$status = "";
	    $msg = "";
	    $file_element_name = 'arquivo';
	     
	    if ($status != "error")
	    {
	        $config['upload_path'] = './depositos/';
	        $config['allowed_types'] = 'gif|jpg|jpeg|png|docx|doc|pdf';
	        $config['max_size'] = 400000;
	        $config['encrypt_name'] = TRUE;
	 
	        $this->load->library('upload', $config);
	 
	        if (!$this->upload->do_upload($file_element_name))
	        {
	            $status = 'error';
	            $msg = $this->upload->display_errors('', '');
	        }
	        else
	        {
	            $msg = $this->upload->data();

	     		$status = 'sucesso';	       
	            
	        }
	    	@unlink($_FILES[$file_element_name]);
	    }
	    
	    echo json_encode(array('msg' => $msg));
    
	}

	public function add()
	{

		$arquivo = $this->input->post('arquivo_upload');

        //if($arquivo != ''){
		
        	if( $this->input->post('empresa_id') == '' ){
        		
        		$dadosEmpresa = array(

			        'razao_social' 				=>	$this->input->post('razao_social'),
					'fantasia' 					=>	$this->input->post('fantasia'),
					'cnpj' 						=>	$this->input->post('cnpj'),
					'telefone' 					=>	$this->input->post('telefone'),
					'endereco' 					=>	$this->input->post('endereco'),
					'email' 					=>	$this->input->post('email'),					
					'cidade' 					=>	$this->input->post('cidade'),
					'estado'					=>	$this->input->post('estado'),
					'pais'						=>	$this->input->post('pais'),
					'credenciamento_inmetro' 	=>	$this->input->post('inmetro'),
					'credenciamento_crea' 		=>	$this->input->post('crea'),
					'tipo_cadastro_id'			=>	4,
					'insc_estadual' 			=>	$this->input->post('inscricao_estadual'), 
					'bairro'					=>	$this->input->post('bairro'),
					'cep'						=>	$this->input->post('cep'),
					'representante_legal'		=> 	$this->input->post('representante_legal'),
					'pais'						=> 	'Brasil'

		    	);

		    	if($this->empresasM->add($dadosEmpresa))
				{
					$empresa_id = $this->db->insert_id();			
				}else{

					$this->session->set_flashdata('erro', 'Não foi possível salvar entre em contato com a wertco 1.');
					$total_aprovados	=	$this->treinamentoM->getAprovadosInscritos($id);		
					$dados 				= 	$this->treinamentoM->getTreinamento($id);
		
					$this->load->view('formularios/treinamento-tecnico',array(	'dados'				=> 	$dados,
																				'total_aprovados'	=> 	$total_aprovados 	)); 
				}

        	}else{

 				$empresa_id = $this->input->post('empresa_id');

 			}

 			if( $this->input->post('usuario_id') == '' ){

 				$dadosUsuarios = array(
			        'nome' 				=>	$this->input->post('nome').' '.$this->input->post('sobrenome') ,
					'cpf' 				=>	$this->input->post('cpf'),
					'telefone' 			=>	$this->input->post('telefone'),
					'celular' 			=>	$this->input->post('celular'),
					'email' 			=>	$this->input->post('email'),					
					'empresa_id'		=>	$empresa_id,
					'tipo_cadastro_id'	=>	4,
					'senha'				=>	md5(md5($this->input->post('senha')))
		    	);

				if($this->usuariosM->add($dadosUsuarios)){

					$usuario_id = $this->db->insert_id();			

				}else{

					$this->session->set_flashdata('erro', 'Não foi possível salvar entre em contato com a wertco.');
					$total_aprovados	=	$this->treinamentoM->getAprovadosInscritos($id);		
					$dados 				= 	$this->treinamentoM->getTreinamento($id);	
					$this->load->view('formularios/treinamento-tecnico',array(	'dados' 			=> 	$dados,
																				'total_aprovados'	=> 	$total_aprovados)); 
				}

 			}else{
 				$usuario_id = $this->input->post('usuario_id');	

 			}


	        $dadosInsert = array(	'treinamento_id'		=> 	$this->input->post('treinamento_id'),
	        						'usuario_id'			=> 	$usuario_id,
	        						'descricao_treinamento'	=> 	$this->input->post('descricao_treinamento'),
	        						'razao_social' 			=>	$this->input->post('razao_social'),
									'cnpj' 					=>	$this->input->post('cnpj'),
									'telefone' 				=>	$this->input->post('telefone'),
									'celular_whats'				=>	$this->input->post('celular'),
									'endereco_comercial'	=>	$this->input->post('endereco'),
									'complemento'			=>	$this->input->post('complemento'),
									'email' 				=>	$this->input->post('email'),				
									'cidade' 				=>	$this->input->post('cidade'),
									'estado'				=>	$this->input->post('estado'),
									'numero'				=>	$this->input->post('numero'),				
									'bairro'				=> 	$this->input->post('bairro'),
									'cep'					=> 	$this->input->post('cep'),
									'nome'					=> 	$this->input->post('nome'),
									'sobrenome'				=> 	$this->input->post('sobrenome'), 
									'cpf'					=>	$this->input->post('cpf'), 
									'rg'					=>	$this->input->post('rg'), 
									'insc_estadual' 		=>	$this->input->post('inscricao_estadual'), 
									'insc_municipal'		=>	$this->input->post('inscricao_municipal'), 
									'interesse'				=> 	$this->input->post('interesse'),
									'arquivo'				=> 	$arquivo,
									'crea'					=> 	$this->input->post('crea'),
									'inmetro'				=> 	$this->input->post('inmetro') );
	        
			$conteudo = '';
	    	$conteudo.= "<p>Nome: <b>". 			$this->input->post('nome').' '.$this->input->post('sobrenome').' </b>| <p/>';
			$conteudo.=	'<p>CPF: <b>'.				$this->input->post('cpf').					' </b> <p/>';
			$conteudo.=	'<p>RG: <b>'.				$this->input->post('rg').					' </b> <p/>';
			$conteudo.=	'<p>Telefone: <b>'.			$this->input->post('telefone').				' </b> <p/>';
			$conteudo.=	'<p>Celular /Whats: <b>'.	$this->input->post('celular').		' </b> <p/>';
			$conteudo.= "<p>Razão Social: <b>". 	$this->input->post('razao_social').			' </b> <p/>';
			$conteudo.=	"<p>Fantasia: <b>".			$this->input->post('fantasia').				' </b> <p/>';
			$conteudo.=	'<p>CNPJ: <b>'.				$this->input->post('cnpj').					' </b> <p/>';
			$conteudo.=	'<p>Inscrição estadual: <b>'.	$this->input->post('inscricao_estadual').	' </b> <p/>';
			$conteudo.=	'<p>Inscrição municipal: <b>'.	$this->input->post('inscricao_municipal').	' </b> <p/>';
			$conteudo.=	'<p>Endereço: <b>'.			$this->input->post('endereco').				' </b> <p/>';
			$conteudo.=	'<p>Nº: <b>'	.			$this->input->post('numero').				' </b> <p/>';
			$conteudo.=	'<p>Bairro: <b>'	.		$this->input->post('bairro').				' </b> <p/>';
			$conteudo.=	'<p>CEP: <b>'	.			$this->input->post('cep').					' </b> <p/>';
			$conteudo.=	'<p>Complemento: <b>'	.	$this->input->post('complemento').			' </b> <p/>';
			$conteudo.=	'<p>Cidade: <b>'	.		$this->input->post('cidade').				' </b> <p/>';
			$conteudo.=	'<p>Estado: <b>'	.		$this->input->post('estado').				' </b> <p/>';
			$conteudo.=	'<p>Interesse: <b>'	.		$this->input->post('interesse').			' </b> <p/>';
			$conteudo.=	'<p>E-mail: <b>'	.		$this->input->post('email').			' </b> <p/>';
			$conteudo.=	'<p>Crea: <b>'	.			$this->input->post('crea').			' </b> <p/>';
			$conteudo.=	'<p>Inmetro: <b>'	.		$this->input->post('inmetro').			' </b> <p/>';
			$arquivo_email = ($arquivo != '' ) ? $conteudo.='<p>COMPROVANTE DO DEPOSITO: <b><a href="http://www.wertco.com.br/depositos/'.$arquivo.'">Baixar</a> </b> <p/>' : '';

			if($this->treinamentoM->add($dadosInsert)){
				
				$this->enviaEmail('marketing@companytec.com.br',$this->input->post('descricao_treinamento'),$conteudo);
				
				/*if( $this->input->post( 'excessao' ) == 1 ){
					$this->enviaEmail('caducidral5@gmail.com',	$this->input->post('descricao_treinamento'),$conteudo);
				}else{
					$this->enviaEmail('adm@abieps.com.br',	$this->input->post('descricao_treinamento'),$conteudo);
				}*/
				
				$this->enviaEmail('suporte@wertco.com.br',$this->input->post('descricao_treinamento'),$conteudo);
				$this->enviaEmail('webmaster@wertco.com.br',$this->input->post('descricao_treinamento'),$conteudo);

				$this->session->set_flashdata('sucesso', 'ok.');
				$total_aprovados	=	$this->treinamentoM->getAprovadosInscritos($this->input->post('treinamento_id'));		
				$dados 				= 	$this->treinamentoM->getTreinamento($this->input->post('treinamento_id'));	
				$this->load->view('formularios/treinamento-tecnico',array(	'dados' 			=> 	$dados,
																			'total_aprovados'	=> 	$total_aprovados)); 
				
			}else{
				$this->session->set_flashdata('erro', 'Não foi possivel salvar entre em contato com o webmaster Companytec.');
				$total_aprovados	=	$this->treinamentoM->getAprovadosInscritos($this->input->post('treinamento_id'));		
				$dados 				= 	$this->treinamentoM->getTreinamento($this->input->post('treinamento_id'));	
				$this->load->view('formularios/treinamento-tecnico',array(	'dados' 			=> 	$dados,
																			'total_aprovados'	=> 	$total_aprovados)); 

			}

				
		/*}else{
			
			$this->session->set_flashdata('erro', 'Não foi possível enviar seu arquivo, reveja o tamanho e o tipo (.doc,.jpg,.png).');
			$total_aprovados	=	$this->treinamentoM->getAprovadosInscritos($this->input->post('treinamento_id'));		
			$dados 				= 	$this->treinamentoM->getTreinamento($this->input->post('treinamento_id'));	
			$this->load->view('formularios/treinamento-tecnico',array(	'dados' 			=> 	$dados,
																		'total_aprovados'	=> 	$total_aprovados)); 

		}*/

	}

	private function enviaEmail($destinatario, $titulo = NULL, $conteudo = NULL, $anexo = NULL  )
	{ 
		$this->load->library('email');
		// Get full html:
		$body = "<html>
					<head>
						<title>".$titulo."</title>
					</head>
				<body> 
					<img src='http://www.wertco.com.br/wertcofundoescuro.png' style='width: 200px;' />
					<h2>".$titulo."</h2>
					<div>
						".$conteudo."
					</div>
					</body>
					</html>";
		if( $anexo != NULL ){			
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    //->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.			    
			    ->to($destinatario)
			    ->subject($titulo)
			    ->message($body)
			    ->attach($anexo)
			    ->send();
		}else{
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    //->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
			    ->to($destinatario)
			    ->subject($titulo)
			    ->message($body)
			    ->send();
		}

		return $result;		
		
	}

	private function valida_cpf($cpf){

	    // Verifica se um número foi informado
	    if(empty($cpf)) {
	        return false;
	    }
	 
	    // Elimina possivel mascara
	    $cpf = str_replace('.', '', str_replace('-','',$cpf));	    
	     
	    // Verifica se o numero de digitos informados é igual a 11 
	    if (strlen($cpf) != 11) {
	        return false;
	    }
	    // Verifica se nenhuma das sequências invalidas abaixo 
	    // foi digitada. Caso afirmativo, retorna falso
	    else if ($cpf == '00000000000' || 
	        $cpf == '11111111111' || 
	        $cpf == '22222222222' || 
	        $cpf == '33333333333' || 
	        $cpf == '44444444444' || 
	        $cpf == '55555555555' || 
	        $cpf == '66666666666' || 
	        $cpf == '77777777777' || 
	        $cpf == '88888888888' || 
	        $cpf == '99999999999') {
	        return false;
	     // Calcula os digitos verificadores para verificar se o
	     // CPF é válido
	     } else {   
	         
	        for ($t = 9; $t < 11; $t++) {
	             
	            for ($d = 0, $c = 0; $c < $t; $c++) {
	                $d += $cpf[$c] * (($t + 1) - $c);
	            }
	            $d = ((10 * $d) % 11) % 10;
	            if ($cpf[$c] != $d) {
	                return false;
	            }
	        }
	 
	        return true;
	    }
	
	}	 


}