<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	public function __construct() {
		parent::__construct();

		// carrega as models a serem utilizadas neste controller
		$this->load->model('ProdutosModel','produtoM');
		$this->load->model('EmpresasModel','empresasM');
		$this->load->model('OrcamentosModel','orcamentosM');
		$this->load->model('UsuariosModel','usuariosM');
		$this->load->model('TrabalheConoscoModel','trabalheM');
		$this->load->model('AutorizServicoModel','autorizServicoM');		
		$this->load->model('CadastrosExpopostosModel','cadastrosExpopostosM');
		$this->load->model('ChamadoModel','chamadoM');
		$this->load->model('OrcamentosAlertaModel' , 'orcamentosAlertasM');
		$this->load->model('PedidosModel' , 'pedidosM');
		$this->load->model('ProgramacaoProducaoRegrasModel','programacaoProdM');	
		$this->load->model('OrdemProducaoModel','opsM');	

	}

	public function index($lang=null)
	{
		if( $lang == 'eng' ){
			$this->load->view('home/eng');	
		}elseif( $lang == 'esp' ){
			$this->load->view('home/esp');	
		}else{

			$this->load->view('home/index');
		}
	}

	public function area_restrita()
	{
		$this->load->view('home/area-restrita.php');
	}

	public function qualidade()
	{
		$this->load->view('home/qualidade.php');
	}

	public function orcamento()
	{
		
		if( $this->input->post() )
		{
			$erro = 0;
			if( $this->input->post('salvar') == '1'){
				
				$dadosEmpresa = array(
			        'razao_social' 				=>	$this->input->post('razao_social'),
					'fantasia' 					=>	$this->input->post('fantasia'),
					'cnpj' 						=>	$this->input->post('cnpj'),
					'telefone' 					=>	$this->input->post('telefone'),
					'endereco' 					=>	$this->input->post('endereco'),
					'email' 					=>	$this->input->post('email'),				
					'cidade' 					=>	$this->input->post('cidade'),
					'estado'					=>	$this->input->post('estado'),
					'pais'						=>	$this->input->post('pais'),
					'bairro'					=>	$this->input->post('bairro'),
					'cep'						=>	$this->input->post('cep'),
					'insc_estadual'				=>	$this->input->post('insc_estadual'),
					'tipo_cadastro_id'			=>	1,
					'cartao_cnpj'				=> 	$this->input->post('cartao_cnpj'),					
					'codigo_ibge_cidade'		=> 	$this->input->post('codigo_ibge_cidade')

			    );			
							
				if($this->empresasM->add($dadosEmpresa))
				{		
					$empresa_id = $this->db->insert_id();			

				}
	 		}else{
	 			$empresa_id = $this->input->post('empresa_id');
	 		}

	 		$dadosOrcamento = array('empresa_id' 			=> 	$empresa_id,
	 								'status_orcamento_id'	=> 	1,
	 								'usuario_id'			=> 	1,
	 								'origem'				=> 	'Site',
	 								'contato_posto'			=> 	$this->input->post('contato_posto'),
	 								'entrega_id'			=> 	14,
	 								'dt_previsao'			=> 	date('Y-m-d',strtotime('+68 days'))	); 

	 		if( $this->orcamentosM->insereOrcamento($dadosOrcamento) )
	 		{
	 			$orcamento_id  = $this->db->insert_id();
	 		
	 			$dadosContato = array(	'empresa_id' 		=> 	$empresa_id,
	 									'nome'				=> 	$this->input->post('contato_posto'),
	 									'email'				=> 	$this->input->post('email_contato_posto'),
	 									'celular'			=> 	$this->input->post('tel_contato_posto'),
	 									'tipo_cadastro_id'	=> 1);

	 			if( $this->usuariosM->add($dadosContato) )
	 			{
		 			
		 			$andamentoOrcamento = array(	'andamento' 			=> 'Orçamento emitido no site da WERTCO!',
			 										'dthr_andamento'		=> date('Y-m-d H:i:s'),
			 										'orcamento_id'			=> $orcamento_id,
			 										'status_orcamento_id'	=> 1 
			 										);	
		 			if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento))
		 			{	 				
		 			
		 				$orcamentoResp = array(	'orcamento_id' 	=> 	$orcamento_id,
			 									'usuario_id' 	=> 	1);
	 					if($this->orcamentosM->insereResponsavelOrcamento($orcamentoResp))
	 					{
			 		
				 			for ($i=0; $i < count($this->input->post('produto')) ; $i++) 	 			  
				 			{ 
				 				$orcamentoProdutos = array(	'orcamento_id' 	=>	$orcamento_id,
				 											'produto_id'  	=> 	$this->input->post('produto')[$i],
				 											'qtd'			=>	$this->input->post('quantidade')[$i]);	

				 				if($this->orcamentosM->insereOrcamentoProdutos($orcamentoProdutos))
				 				{
				 					$erro = $erro;
								}else{
									$erro++;
								}		 					 			
				 			}
				 		}else{
				 			$erro++;	
				 		}

					}else{
						$erro++;
					}
				}else{
					$erro++;
				}

			}else{
				$erro++;
			}

	 		if( $erro == 0){
	 				
	 			if($this->enviarEmailOrcamento($orcamento_id))
 				{
 					$this->session->set_flashdata('sucesso', 'ok.');
 					$bombas['bombas'] = $this->produtoM->select();		
					$this->load->view('home/orcamento.php',$bombas);		
 				}else{
 					
 					$this->session->set_flashdata('erro', 'erro.');	
 					$bombas['bombas'] = $this->produtoM->select();		
					$this->load->view('home/orcamento.php',$bombas);		
 				}
 			}else{
 				$this->session->set_flashdata('erro', 'erro.');
 				$bombas['bombas'] = $this->produtoM->select();		
				$this->load->view('home/orcamento.php',$bombas);		
 			}	 		
			
		}else{

			$bombas['bombas'] = $this->produtoM->select();		
			$this->load->view('home/orcamento.php',$bombas);
		}
	}

	private function enviarEmailOrcamento($orcamento_id){
		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= new DateTime($empresa['emissao']);				
		$email = '<html>
				<head></head>
				<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
				<div class="content" style="width: 600px; height: 100%;">
				<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/backgroundCabecalho.png); height: 528px; "></div>
				<div style="width: 600px; background-color: #ffcc00; height: 100%">
					<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
						<tbody>
							<tr>
								<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
								<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
								<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
								<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
							</tr>
							<tr>
								<td style="padding-bottom: 18px;font-weight: 700;">Validade:</td>
								<td style="padding-bottom: 18px;">30 Dias</td>
								<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
								<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
							</tr>
							<tr>
								<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
								<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
								<td style=" font-weight: 700; text-align: right;">Telefone:</td>
								<td>'.$empresa['telefone'].'</td>
							</tr>
							<tr>
								<td style=" font-weight: 700;">E-mail:</td>
								<td colspan="3">'.$empresa['email'].'</td>
							</tr>
						</tbody>
					</table>
					<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
					<table style="width: 570px;margin-left: 14px;" cellspacing="0">
						<thead style="height: 55px;">
							<tr><th style="    height: 30px;">Modelo</th>
							<th>Descrição</th>
							<th>Quantidade</th>			
						</tr></thead>
						<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
				$total = 0; foreach($produtos as $produto){
					
					$email.='	<tr>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
								<td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>							
							</tr>';					
					$total = $total + ($produto['valor'] * $produto['qtd']);
				}
				
				$email.='		
						</tbody>
					</table>
					<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
				</div>
			</div>
		</body>
		</html>';	

		$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('webmaster@wertco.com.br')    // Optional, an account where a human being reads.
		    ->to('vendas@wertco.com.br')
		    ->subject('Orçamento realizado no site')
		    ->message($email)
		    ->send();

		return $result;
	}

	public function confirmaOrcamentoGestor($orcamento_id){

		$update = array(	'id' 					=> 	$orcamento_id,
							'status_orcamento_id'	=> 	1	);

		if($this->orcamentosM->atualizaOrcamento($update)){

			$andamentoOrcamento = array(	'andamento' 			=> 	'Orçamento aprovado pelo gestor!',
	 										'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
	 										'orcamento_id'			=> 	$orcamento_id,
	 										'status_orcamento_id'	=> 	1,
	 										'usuario_id' 			=> 	2465	);	

 			if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento)){
 				$this->session->set_flashdata('sucesso', 'ok.');				
				$this->load->view('home/aprovaOrcamento.php');
 			}

		}
	
	}

	public function aprovaConfirmacaoPedido($pedido_id)
	{
		$update = array(	'id' 				=> 	$pedido_id,
							'status_pedido_id' 	=> 	2 	);
		
		$validacao = $this->pedidosM->getPedidosById($pedido_id);
		
		if( $validacao->ordem < 4  ){

			if($this->pedidosM->atualizaPedidos($update)){
				$this->insereAndamentoPedido( $pedido_id, 2 , 3708 );
				$this->insereMapaProducao($pedido_id);
				$conteudo = '<html>
								<head></head>
								<body style="width: 850px; position:absolute;">
									<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
										<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
										<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">⚠️ Atenção! Pedido Confirmado pelo cliente e Liberado Pelo Financeiro ⚠️</h2>
										<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
											Pedido: #'.$pedido_id.'<br/>									
										</p>
									</div>
								</body>
							</html>';
				if($this->enviaEmail('todosvendas@wertco.com.br','⚠️ Atenção! Pedido Confirmado pelo cliente e Liberado Pelo Financeiro ⚠️',$conteudo,NULL,'gestor@wertco.com.br') )
				{
					echo 'Pedido Confirmado pelo cliente e Liberado Pelo Financeiro. Realize o check list e Libere o mesmo para produção! <script>setTimeout(function(){ window.location = "https://www.wertco.com.br/AreaAdministrador/pedidos"; }, 3000);</script>'; 
							
				}
			}
		}else{
			echo 'Pedido Já Confirmado! <script>setTimeout(function(){ window.location = "https://www.wertco.com.br/AreaAdministrador/pedidos"; }, 3000);</script>'; 
		}
	}

	public function trabalheConosco(){
		
		if( $_FILES['curriculo']['name'] != "" ){
			$tipo 		= 	explode('.', $_FILES['curriculo']['name']);
			$arquivo 	=	md5($_FILES['curriculo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
			
			$configuracao = array(
		        'upload_path'   	=> 	'./trabalhe-conosco/',		        
		        'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv',
		        'file_name'     	=> 	$arquivo,
		        'max_size'			=> 	400000
		    );      
			
		    $this->load->library('upload', $configuracao);
		    
		    if ($this->upload->do_upload('curriculo')){
		    	
		    	$erro = 0;
		    	
		    	$insert = array('nome' 		=> 	$_POST['nome'].' '.$_POST['sobrenome'],
		    					'telefone' 	=> 	$_POST['phone_curriculo'],
		    					'mensagem'	=> 	$_POST['message_curriculo'],
		    					'email'		=> 	$_POST['email_curriculo'],
		    					'arquivo'	=> 	$arquivo);

		    	if( $this->trabalheM->insere($insert)  ){

		    		$email='<html>
								<head></head>
								<body style="width: 650px; height: 600px;">
									<div style="text-align: left;margin-left: 62px;">
										<img src="http://www.wertco.com.br/bootstrap/assets/demo/default/media/img/logo/wertcofundoescuro.png" />
										<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Currículo cadastrado</h1>
										<table>
											<tr style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
												<td>Nome:</td>
												<td>'.$_POST['nome'].' '.$_POST['sobrenome'].'</td>
											</tr>
											<tr style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
												<td>Telefone:</td>
												<td>'.$_POST['phone_curriculo'].'</td>
											</tr>
											<tr style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
												<td>E-mail:</td>
												<td>'.$_POST['email_curriculo'].'</td>
											</tr>
											<tr style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
												<td>Mensagem:</td>
												<td>'.$_POST['message_curriculo'].'</td>
											</tr>
											<tr style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
												<td>Currículo:</td>
												<td><a href="http://www.wertco.com.br/trabalhe-conosco/'.$arquivo.'">Baixar</a></td>
											</tr>
										</table>																					 	
											 
										
									</div>
								</body>
							</html>';

					if($this->enviaEmail('rh@wertco.com.br','[SITE WERTCO] - Trabalhe conosco',$email,NULL,'rh@wertco.com.br')){
						$this->enviaEmail('webmaster@wertco.com.br','[SITE WERTCO] - Trabalhe conosco',$email,NULL,'rh@wertco.com.br');
						echo json_encode(array('retorno' => 'sucesso'));
					}else{
						echo json_encode(array('erro' => 'erro'));
					}		    		

		    	}else{

		    		echo json_encode(array('retorno' => 'erro'));
		    	}

		    }else{
		    	
		    	echo json_encode(array('retorno' => 'erro'));
		    }
		}
			
		    	
	}

	public function confirmaRecebimentoTecnico(){

		$update =  array(	'chamado_id' 	=> 	$_GET['chamado_id'],
							'tecnico_id'	=> 	$_GET['tecnico_id'],
							'dthr_aceite'	=> 	date('Y-m-d H:i:s'),
							'fl_aceite'		=>  $_GET['opcao'] );

		if( $this->autorizServicoM->update($update) ){
			$atu_chamado = array('id' 					=> $update['chamado_id'],
								'inicio_atendimento' 	=> $update['dthr_aceite']	);
			$this->chamadoM->update($atu_chamado);
			$this->session->set_flashdata('sucesso', 'ok.');

			$tecnico = $this->autorizServicoM->buscaTecnico($update['chamado_id'],$update['tecnico_id']);
			$opcao = ($_GET['opcao']==0) ? "Negou a Solicitação." : "Aprovou a Solicitação."; 
			$email='<html>
						<head></head>
						<body >
							<div style="text-align: left;margin-left: 62px;">
								<img src="http://www.wertco.com.br/bootstrap/assets/demo/default/media/img/logo/wertcofundoescuro.png" />
								<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Aceite de Autorização de Serviço</h1>
								<table width="100%">
									<tr style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										<td>Chamado:</td>
										<td>#'.$_GET['chamado_id'].'</td>
									</tr>
									<tr style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										<td>Posto:</td>
										<td>'.$tecnico['posto'].' - '.$tecnico['posto_cnpj'].'</td>
									</tr>
									<tr style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										<td>Técnico:</td>
										<td>'.$_GET['tecnico_id'].' - '.$tecnico['nome'].'</td>
									</tr>
									<tr style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										<td>Empresa:</td>
										<td>'.$tecnico['razao_social'].' - '.$tecnico['cnpj'].'</td>
									</tr>
									<tr style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										<td>Resposta do Técnico:</td>
										<td>'.$opcao.'</td>
									</tr>
									<tr style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										<td>DTHR ACEITE:</td>
										<td>'.date('d/m/Y H:i:s', strtotime($update['dthr_aceite'])).'</td>
									</tr>
								</table>																					 	
									 
								
							</div>
						</body>
					</html>';

			if(	$this->enviaEmail('industrial@wertco.com.br',	'Retorno de Solicitação de Serviço - Chamado: '.$_GET['chamado_id'],$email,NULL,'industrial@wertco.com.br')	){
				$this->enviaEmail('suporte@wertco.com.br',	'Retorno de Solicitação de Serviço - Chamado: '.$_GET['chamado_id'],$email,NULL,'suporte@wertco.com.br');
				$this->enviaEmail('suporte2@wertco.com.br',	'Retorno de Solicitação de Serviço - Chamado: '.$_GET['chamado_id'],$email,NULL,'suporte2@wertco.com.br');
				$this->load->view('home/confirma-recebimento-email.php');
			}
		}else{
			$this->session->set_flashdata('error', 'erro'); 
			$this->load->view('home/confirma-recebimento-email.php');
		}
		
	}

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null, $reply_to=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($reply_to)    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)		    
			    ->send();
		}else{
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($reply_to)    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)		    
			    ->send();
		}

		return $result;		
	}

	public function ValidaCnpj(){
    	$cnpj 			= 	$this->input->post('cnpj');		
		$consulta_rf 	= 	$this->ConsultaCNPJRF($cnpj);

        if (isset($consulta_rf) && $consulta_rf->{'code'} == '1')
        {
            if ($consulta_rf->{'data'}->{'situacao_cadastral'} == 'ATIVA')
            {
            	$arr['retorno'] 	= 	$consulta_rf->{'data'};
            	$arr['save'] 		= 	$consulta_rf->{'Save_Receita'};
                $arr['result'] 		= 	true;
                $arr['situacao'] 	= 	$consulta_rf->{'data'}->{'situacao_cadastral'};
            	//echo json_encode($consulta_rf);            	
                
            	$arr['result'] = true;
				$arr['erro'] = 0;

            }else {
            	$arr['erro'] 	= 1;
            	$arr['msg'] 	= '* Situação cadastral '.$consulta_rf->{'data'}->{'situacao_cadastral'}.' na Receita Federal.'; 
            }
		}else {

            if (isset($consulta_rf)){
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 2 - '.$consulta_rf->{'code'}.')';
            }else{
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 3)';
            }

        }

		echo json_encode(array(	'receita' 	=> $arr));
	}

	public function cadastroExpopostos(){

		if($this->input->post()){
			$dadosOrcamento = array( 	'nome' 		=> 	$this->input->post('nome'),
	 								 	'telefone'	=> 	$this->input->post('telefone'),
	 									'email'		=> 	$this->input->post('email'),
	 									'estado'	=> 	$this->input->post('estado')	); 

	 		if( $this->cadastrosExpopostosM->insere($dadosOrcamento) ){
	 			$corpoEmail	=	'<html>
									<head></head>
										<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
										<div class="content" style="width: 600px; height: 100%;">
											<img src="http://www.wertco.com.br/bootstrap/img/confirmacaoExpopostos.png" />
										</div>
									</body>
								</html>';
	 			$this->enviaEmail($this->input->post('email'),'Bombas Wertco - Expoposto 2019',$corpoEmail,'https://www.wertco.com.br/downloads/FolderWertco2019Compactado.pdf','marketing@companytec.com.br');
				$this->session->set_flashdata('sucesso', 'ok.');
				$this->load->view('formularios/cadastro-expopostos.php');
	 		}

		}else{
			$this->load->view('formularios/cadastro-expopostos.php');
		}

	}

	public function enviarEmailChamadoSuporte($usuario_id,$email){
		
			$chamados 		= 	$this->chamadoM->buscaTodosChamadosInativos();
			$totalChamados 	= 	$this->chamadoM->buscaTotalChamadosAbertos();
			$totalFechados 	= 	$this->chamadoM->buscaTotalChamadosFechados();
			
			$message = '
			<html>
			<head>
				<title>Chamados abertos com mais de 15 dias de inatividade</title>
			</head>
			<body>
			<img src="https://www.wertco.com.br/bootstrap/assets/demo/default/media/img/logo/wertcofundoescuro.png" style="width: 120px;"	/>
			<br/><br/>
			<table border="1" style="float: left;">
				<thead>
					<tr >
						<th style="padding: 10px;">Chamado #</th>
						<th style="padding: 10px;">Dias sem atualização</th>
						<th style="padding: 10px;">Status Atual</th>
						<th style="padding: 10px;">Prioridade</th>
						<th style="padding: 10px;">Aberto Por</th>
					</tr>
				</thead>
			<tbody>';
			$i=0;
			$total = 0;
			foreach($chamados as $chamado){
				$bgcolor = "background-color: #ffffff;";
				if( $i%2 == 0 ){
					$bgcolor = "background-color: #f0f0f0;";
				}
				$message.='	<tr>
								<td style="padding: 10px;'.$bgcolor.'">'.$chamado['chamado_id'].'</td>
								<td style="padding: 10px;'.$bgcolor.'">'.$chamado['diff'].'</td>
								<td style="padding: 10px;'.$bgcolor.'">'.$chamado['status'].'</td>
								<td style="padding: 10px;'.$bgcolor.'">'.$chamado['prioridade'].'</td>
								<td style="padding: 10px;'.$bgcolor.'">'.$chamado['nome'].'</td>
							</tr>';
				$i++;
				$total++;
			}
			
			$message.='</tbody>			 
			</table>
			<table border="1">
				<thead>
					<tr>
					 	<th style="padding: 10px;">Dia</th>
					 	<th style="padding: 10px;">Total</th>
					 	<th style="padding: 10px;">Fechados</th>
					</tr>					
				</thead>
				<tbody>
					<tr>
						<td style="padding: 10px;background-color: #f0f0f0; ">'.date('d/m/Y').'</td>
						<td style="padding: 10px;background-color: #f0f0f0; ">'.$totalChamados['total'].'</td>
						<td style="padding: 10px;background-color: #f0f0f0; ">'.$totalFechados['total'].'</td>
					</tr>
				</tbody>
			</table>
			</body> 
			</html>';
			if($this->enviaEmail($email,'Chamados abertos',$message,'','webmaster@wertco.com.br'))
			{
				echo 'foi<br/>';
			}
			
	}

	public function alertaPrevisaoEntrega()
	{
		$dados = $this->pedidosM->buscaPedidosDtEntrega();

		if(!empty($dados)){
			$message = '
				<html>
				<head>
					<title>Atenção! Pedidos com data de entrega próxima</title>
				</head>
				<body>
				<img src="https://www.wertco.com.br/bootstrap/assets/demo/default/media/img/logo/wertcofundoescuro.png" style="width: 220px;"	/>
				<br/><br/>
				<table border="1" style="float: left;">
					<thead>
						<tr >
							<th style="padding: 10px;">Pedido #</th>
							<th style="padding: 10px;">Cliente</th>
							<th style="padding: 10px;">Modelos</th>
							<th style="padding: 10px;">Qtd. Bombas</th>
							<th style="padding: 10px;">Dt. Previsão de Entrega</th>
							<th style="padding: 10px;">Transportadora</th>
						</tr>
					</thead>
				<tbody>';
			$i=0;
			$total = 0;
			foreach($dados as $dado){
				$bgcolor = "background-color: #ffffff;";
				if( $i%2 == 0 ){
					$bgcolor = "background-color: #f0f0f0;";
				}
				$message.='	<tr>
								<td style="padding: 10px;'.$bgcolor.'">'.$dado['pedido'].'</td>
								<td style="padding: 10px;'.$bgcolor.'">'.$dado['cliente'].'</td>
								<td style="padding: 10px;'.$bgcolor.'">'.$dado['modelos'].'</td>
								<td style="padding: 10px;'.$bgcolor.'">'.$dado['qtd_bombas'].'</td>
								<td style="padding: 10px;'.$bgcolor.'">'.$dado['dt_prev_entrega'].'</td>
								<td style="padding: 10px;'.$bgcolor.'">'.$dado['transportadora'].'</td>								
							</tr>';
				$i++;
				$total++;
			}
			
			$message.='</tbody>			 
			</table>			
			</body> 
			</html>';
			if($this->enviaEmail('expedicao@wertco.com.br','⚠️ Pedidos Próximo a entrega ⚠️',$message,'','webmaster@wertco.com.br'))
			{
				$this->enviaEmail('expedicao2@wertco.com.br','⚠️ Pedidos Próximo a entrega ⚠️',$message,'','webmaster@wertco.com.br');
				echo 'foi<br/>';
			}
		}
	
	}

	public function relatorioChamados(){

		$this->enviarEmailChamadoSuporte('12','industrial@wertco.com.br');
		$this->enviarEmailChamadoSuporte('2385','suporte2@wertco.com.br');
		$this->enviarEmailChamadoSuporte('2384','suporte@wertco.com.br');
		$this->enviarEmailChamadoSuporte('12','webmaster@wertco.com.br');
		$this->enviarEmailChamadoSuporte('4414','suporte3@wertco.com.br');
	}

	public function atualizaOrcamentosExpirados(){

		$expirados = $this->orcamentosM->orcamentoExpirados();

		foreach( $expirados as $expirado ){

			$atualiza = array(	'id' 					=> 	$expirado['orcamento_id'],
								'status_orcamento_id' 	=> 	13);

			$this->orcamentosM->atualizaOrcamento($atualiza);
			$andamentoOrcamento = array(	'andamento' 			=> 	'Orçamento expirado por falta de atualização!',
	 										'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
	 										'orcamento_id'			=> 	$expirado['orcamento_id'],
	 										'status_orcamento_id'	=> 	13 	);	

 			$this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento);
		}

		$aexpirar = $this->orcamentosM->orcamentosAExpirar();		

		$email = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="x-apple-disable-message-reformatting" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title></title><style type="text/css">
    /* Resets */
    .ReadMsgBody { width: 100%; background-color: #ebebeb;}
    .ExternalClass {width: 100%; background-color: #ebebeb;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
    a[x-apple-data-detectors]{
        color:inherit !important;
        text-decoration:none !important;
        font-size:inherit !important;
        font-family:inherit !important;
        font-weight:inherit !important;
        line-height:inherit !important;
    }        
    body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
    body {margin:0; padding:0;}
    .yshortcuts a {border-bottom: none !important;}
    .rnb-del-min-width{ min-width: 0 !important; }

    /* Add new outlook css start */
    .templateContainer{
        max-width:700px !important;
        width:auto !important;
    }
    /* Add new outlook css end */

    /* Image width by default for 3 columns */
    img[class="rnb-col-3-img"] {
        max-width:170px;
    }

    /* Image width by default for 2 columns */
    img[class="rnb-col-2-img"] {
        max-width:264px;
    }

    /* Image width by default for 2 columns aside small size */
    img[class="rnb-col-2-img-side-xs"] {
        max-width:180px;
    }

    /* Image width by default for 2 columns aside big size */
    img[class="rnb-col-2-img-side-xl"] {
        max-width:350px;
    }

    /* Image width by default for 1 column */
    img[class="rnb-col-1-img"] {
        max-width:550px;
    }

    /* Image width by default for header */
    img[class="rnb-header-img"] {
        max-width:700px;
    }

    /* Ckeditor line-height spacing */
    .rnb-force-col p, ul, ol{margin:0px!important;}
    .rnb-del-min-width p, ul, ol{margin:0px!important;}

    /* tmpl-2 preview */
    .rnb-tmpl-width{ width:100%!important;}

    /* tmpl-11 preview */
    .rnb-social-width{padding-right:15px!important;}

    /* tmpl-11 preview */
    .rnb-social-align{float:right!important;}

    /* Ul Li outlook extra spacing fix */
    li{mso-margin-top-alt: 0; mso-margin-bottom-alt: 0;}        

    /* Outlook fix */
    table {mso-table-lspace:0pt; mso-table-rspace:0pt;}
    
    /* Outlook fix */
    table, tr, td {border-collapse: collapse;}

    /* Outlook fix */
    p,a,li,blockquote {mso-line-height-rule:exactly;} 

    /* Outlook fix */
    .msib-right-img { mso-padding-alt: 0 !important;}

    /* Fix text line height on preview */
    .content-spacing div {display: list-item; list-style-type: none;}

    @media only screen and (min-width:590px){
        /* mac fix width */
        .templateContainer{width:590px !important;}
    }

    @media screen and (max-width: 360px){
        /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
        .rnb-yahoo-width{ width:360px !important;}
    }

    @media screen and (max-width: 380px){
        /* fix width and font size "tmpl-4 tmpl-6" in mobile preview */
        .element-img-text{ font-size:24px !important;}
        .element-img-text2{ width:230px !important;}
        .content-img-text-tmpl-6{ font-size:24px !important;}
        .content-img-text2-tmpl-6{ width:220px !important;}
    }

    @media screen and (max-width: 480px) {
        td[class="rnb-container-padding"] {
            padding-left: 10px !important;
            padding-right: 10px !important;
        }

        /* force container nav to (horizontal) blocks */
        td.rnb-force-nav {
            display: inherit;
        }

        /* fix text alignment "tmpl-11" in mobile preview */
        .rnb-social-text-left {
            width: 100%;
            text-align: center;
            margin-bottom: 15px;
        }
        .rnb-social-text-right {
            width: 100%;
            text-align: center;
        }
    }

    @media only screen and (max-width: 600px) {

        /* center the address &amp; social icons */
        .rnb-text-center {text-align:center !important;}

        /* force container columns to (horizontal) blocks */
        td.rnb-force-col {
            display: block;
            padding-right: 0 !important;
            padding-left: 0 !important;
            width:100%;
        }

        table.rnb-container {
           width: 100% !important;
       }

       table.rnb-btn-col-content {
        width: 100% !important;
    }
    table.rnb-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
    }

    table.rnb-last-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
    }

    table.rnb-col-2 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
    }

    table.rnb-col-2-noborder-onright {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
    }

    table.rnb-col-2-noborder-onleft {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-top: 10px;
        padding-top: 10px;
    }

    table.rnb-last-col-2 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
    }

    table.rnb-col-1 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
    }

    img.rnb-col-3-img {
        /**max-width:none !important;**/
        width:100% !important;
    }

    img.rnb-col-2-img {
        /**max-width:none !important;**/
        width:100% !important;
    }

    img.rnb-col-2-img-side-xs {
        /**max-width:none !important;**/
        width:100% !important;
    }

    img.rnb-col-2-img-side-xl {
        /**max-width:none !important;**/
        width:100% !important;
    }

    img.rnb-col-1-img {
        /**max-width:none !important;**/
        width:100% !important;
    }

    img.rnb-header-img {
        /**max-width:none !important;**/
        width:100% !important;
        margin:0 auto;
    }

    img.rnb-logo-img {
        /**max-width:none !important;**/
        width:100% !important;
    }

    td.rnb-mbl-float-none {
        float:inherit !important;
    }

    .img-block-center{text-align:center !important;}

    .logo-img-center
    {
        float:inherit !important;
    }

    /* tmpl-11 preview */
    .rnb-social-align{margin:0 auto !important; float:inherit !important;}

    /* tmpl-11 preview */
    .rnb-social-center{display:inline-block;}

    /* tmpl-11 preview */
    .social-text-spacing{margin-bottom:0px !important; padding-bottom:0px !important;}

    /* tmpl-11 preview */
    .social-text-spacing2{padding-top:15px !important;}

}</style><!--[if gte mso 11]><style type="text/css">table{border-spacing: 0; }table td {border-collapse: separate;}</style><![endif]--><!--[if !mso]><!--><style type="text/css">table{border-spacing: 0;} table td {border-collapse: collapse;}</style> <!--<![endif]--><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body>

    <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#fff" style="background-color: rgb(255, 255, 255);">

        <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td> </td></tr><tr>
            <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="700" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="700" style="width:590px;">
                        <![endif]-->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:700px!important; width: 590px;">
                            <tbody><tr>

                                <td align="center" valign="top">

                                    <div style="background-color: rgb(255, 255, 255);">
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="700" style="width:700px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_19" id="Layout_19">
                    <tbody><tr>
                        <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                            <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                                <tbody><tr>
                                    <td valign="top" align="center">
                                        <table cellspacing="0" cellpadding="0" border="0">
                                            <tbody><tr>
                                                <td>
                                                    <div style="display:block; border-radius:0px; width:700;;max-width:700px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                        <div>
                                                            <img ng-if="col.img.source != url" border="0" hspace="0" vspace="0" width="700" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="https://www.wertco.com.br/email-cliente/Orcamento_expirado.png"></div><div style="clear:both;"></div>
                                                        </div></td>
                                                    </tr>
                                                </tbody></table>

                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr></tbody></table>
            <!--[if mso]>
                </td>
            <![endif]-->

                <!--[if mso]>
                </tr>
                </table>
            <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="600px" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_22">
                    <tbody><tr>
                        <td class="rnb-del-min-width" align="center" valign="top">
                            <table width="600px" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">
                                <tbody>
                                <tr>
                                    <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="top" class="rnb-container-padding" align="left">

                                        <table width="600px" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                            <tbody><tr>
                                                <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                    <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                        <tbody><tr>
                                                            <td class="content-spacing" style="font-size:14px; font-family:Lucida Sans Unicode,Lucida Grande,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><table cellpadding="0" cellspacing="0" style="width: 600px !important;" width="600">
                                                               <thead>
                                                                  <tr>
                                                                     <th bgcolor="#ffcc00" height="40" style="text-align: center;" width="50"><span style="font-size:12px;"><span style="color:#000000;">ID</span></span></th>
                                                                     <th bgcolor="#ffcc00" height="40" style="text-align: center;" width="280"><span style="font-size:12px;"><span style="color:#000000;">CLIENTE</span></span></th>
                                                                     <th bgcolor="#ffcc00" height="40" style="text-align: center;" width="200"><span style="font-size:12px;"><span style="color:#000000;">RESPONSÁVEL</span></span></th>
                                                                     <th bgcolor="#ffcc00" height="40" style="text-align: center;" width="50"><span style="font-size:12px;"><span style="color:#000000;">DIAS</span></span></th>
                                                                 </tr>
                                                             </thead>
                                                             <tbody>';
                                                            foreach ($aexpirar as $dados) {
                                                             
                                                             	$email.='
	                                                              <tr>
	                                                                 <td bgcolor="#CCCCCC" height="35" style="text-align: center;"><span style="font-size:12px;">'.$dados['id'].'</span></td>
	                                                                 <td bgcolor="#E6E6E6" height="35" style="text-align: center;"><span style="font-size:12px;">'.$dados['cnpj'].' <br/> '.$dados['razao_social'].'</span></td>
	                                                                 <td bgcolor="#CCCCCC" height="35" style="text-align: center;"><span style="font-size:12px;"> '.mb_strtoupper($dados['responsavel']).' <br/> '.mb_strtoupper($dados['descricao']).'</span></td>
	                                                                 <td bgcolor="#E6E6E6" height="35" style="text-align: center;"><span style="font-size:12px;">'.$dados['dias'].'</span></td>
	                                                             </tr>
	                                                             <tr>
	                                                                <td colspan="4" bgcolor="#ffffff" height="2"></td>
	                                                             </tr>';
                                                             }
                                                            $email.=' 
                                                         </tbody>
                                                     </table>
                                                 </td>
                                             </tr>
                                         </tbody></table>

                                     </td></tr>
                                 </tbody></table></td>
                             </tr>
                             <tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
            <![endif]-->

                <!--[if mso]>
                </tr>
                </table>
            <![endif]-->

        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_20">
                    <tbody><tr>
                        <td class="rnb-del-min-width" align="center" valign="top">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                <tbody><tr>
                                    <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="top" class="rnb-container-padding" align="left">

                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                            <tbody><tr>
                                                <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                    <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                        <tbody><tr>
                                                            <td class="content-spacing" style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"></td>
                                                        </tr>
                                                    </tbody></table>

                                                </td></tr>
                                            </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
            </tbody></table><!--[if mso]>
                </td>
            <![endif]-->

                <!--[if mso]>
                </tr>
                </table>
            <![endif]-->

        </div></td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                    <![endif]-->
                </td>
            </tr>
        </tbody></table>

    </body></html>';
		if($this->enviaEmail('todosvendas@wertco.com.br','Orçamentos a expirar',$email) ){
			echo 'email enviado';
		}
		
	}

	public function dataBoleto(){
		$this->load->view('home/data-boleto.php');
	}

	public function somaDiasData(){
		
		$dt_fim = date('d-m-Y', strtotime("+".$this->input->post('dias').' days', strtotime($this->input->post('dt_ini'))));

		echo json_encode(array('retorno' 	=> 	'sucesso',
								'dt_fim'	=> 	$dt_fim));
	}

	public function campanhaIndicador(){
		$this->load->view('home/campanha-indicar-negocio');
	}

	public function alertasFallowUp(){
		$usuarios = $this->usuariosM->selectUsuariosComercialFallowUp();
		foreach( $usuarios as $usuario ){
			$this->enviaEmailAlerta($usuario['id'], $usuario['email']);
		
		}
	
	}

	private function enviaEmailAlerta($usuario_id, $email_usuario){

		$alertas['alertas'] = $this->orcamentosAlertasM->selectAlertaDiario($usuario_id);

		if(count($alertas['alertas']) >= 1 ){

			$email = $this->load->view('home/email-alerta-fallow-up', $alertas, true);

			if(	$this->enviaEmail($email_usuario, " ⚠️ Atenção! Você tem follow-up's agendados para hoje ⚠️",$email)	){
				echo 'email enviado';
			}
		}
			
	}

	public function confirmarAlerta(){

    	$dadosUpdate = array(	'id' 				=> 	$_GET['alerta_id'],
    							'fl_visualizado'	=> 	1,
    							'dthr_visualizado'	=> 	date('Y-m-d H:i:s')	);

    	if(	$this->orcamentosAlertasM->atualizar($dadosUpdate)	){
    		
    		echo json_encode(array('retorno' => 'sucesso'));
    	}else{

    		echo json_encode(array('retorno' => 'erro'));
		}
    }

    // Método para inserção dos andamentos dos pedidos
	private function insereAndamentoPedido($pedido_id, $status_pedido_id, $usuario_id)
	{
		$andamento = $this->pedidosM->buscaStatusByid($status_pedido_id);

		$insert = array(	'pedido_id' 		=> 	$pedido_id,
							'status_pedido_id' 	=>	$status_pedido_id,
							'andamento'			=> 	$andamento['descricao'].'. Através do e-mail',
							'usuario_id' 		=> 	$usuario_id );

		if( $this->pedidosM->insereAndamento($insert) ){
			return true;
		}else{
			return false;
		}
	}

	public function ativaUsuario($id)
	{
		$dados = array(	'id'	=>	$id,
						'ativo'	=>	1	);
		
		if($this->usuariosM->atualizaStatus($dados)){
			$this->log('Área Administrador | ativa usuário','usuarios','EDIÇÂO', $this->session->userdata('usuario_id'), $dados,$dados,$_SERVER['REMOTE_ADDR']);
			echo 'Usuário Ativado com sucesso! <script>setTimeout(function(){ window.location = "https://www.wertco.com.br/AreaAdministrador/gestaoUsuarios"; }, 3000);</script>';					
			
		}else{

			echo 'erro';
		}
	}

	public function inativaUsuario($id)
	{
		$dados = array(	'id'	=>	$id,
						'ativo'	=>	0	);
		
		if($this->usuariosM->atualizaStatus($dados)){
			$this->log('Área Administrador | ativa usuário','usuarios','EDIÇÂO', $this->session->userdata('usuario_id'), $dados,$dados,$_SERVER['REMOTE_ADDR']);
			echo 'Usuário Inativado com sucesso! <script>setTimeout(function(){ window.location = "https://www.wertco.com.br/AreaAdministrador/gestaoUsuarios"; }, 3000);</script>';					
			
		}else{

			echo 'erro';
		}
	}

	public function fechaSemanaProducao(){		
		 
		if( date('N') == '6'  ){
			$abertura = $this->programacaoProdM->select();

			$dia = new DateTime( $abertura['data_abertura'] );
			$dia->modify( 'next monday' );	
			
			$update = array(	'id'				=> 	1,								
								'dt_abertura_prod'	=> 	$dia->format('Y-m-d') );
						 
			if($this->programacaoProdM->atualiza($update)){
				echo 'Atualizou';
			}else{
				echo 'Não Atualizou';
			}

		}

	}

	public function orcamentosExpiradosParaCancelados()
	{

		$expirados = $this->orcamentosM->OrcamentoExpiradosCancelados();

		foreach( $expirados as $expirado ){

			$atualiza = array(	'id' 					=> 	$expirado['orcamento_id'],
								'status_orcamento_id' 	=> 	4);

			$this->orcamentosM->atualizaOrcamento($atualiza);
			$andamentoOrcamento = array(	'andamento' 			=> 	'Orçamento cancelado por falta de atualização!',
	 										'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
	 										'orcamento_id'			=> 	$expirado['orcamento_id'],
	 										'status_orcamento_id'	=> 	4 	);	

 			$this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento);
		}

	}

	private function insereMapaProducao($id)
    {
    	//verificar a data de produção e verifica a data de entrega do pedido
		$regras = $this->programacaoProdM->select();

		$totalB = $this->pedidosM->totalBicosBombas($id);

		//Caso ainda exista espaço na semana para o pedido ser produzido
		if( ($regras['total_bicos_atual'] + $totalB['total_bicos']) < $regras['nr_bicos_semana'] && ($regras['total_bombas_atual'] + $totalB['total_bombas']) < $regras['nr_bombas_semana'] ){

			$data_semana_atual 	= 	$regras['data_semana_atual'];
			$total_bicos 		= 	$regras['total_bicos_atual'] 	+ 	$totalB['total_bicos'];
			$total_bombas 		= 	$regras['total_bombas_atual'] 	+ 	$totalB['total_bombas'];

			if( strtotime($regras['data_abertura']) > strtotime($regras['data_semana_atual']) ){
				$data_semana_atual 	= 	$regras['data_abertura'];
				$total_bicos 		= 	$regras['total_bicos_atual'];
				$total_bombas 		= 	$regras['total_bombas_atual'];					
			}

			$atuNrbombasBicos = array(	'id' 					=> 	1,
										'total_bicos_atual'		=> 	$total_bicos,
										'total_bombas_atual'	=> 	$total_bombas,
										'dt_semana_atual'		=> 	$data_semana_atual	);

			$inserePedidos = array( 'pedido_id' 			=> 	$id,
									'dt_semana_prog' 		=> 	$data_semana_atual,
									'nr_bicos'				=> 	$totalB['total_bicos'],
									'nr_bombas'				=> 	$totalB['total_bombas'],
									'dt_previsao_entrega' 	=> 	date('Y-m-d',strtotime($data_semana_atual.' +15 days') )	 		 );
		}else{
			date_default_timezone_set('America/Sao_Paulo');

			$dia = new DateTime($regras['data_semana_atual']);
			$dia->modify( 'next monday' );
			$proximaSegunda =  $dia->format('Y-m-d');
			$data_semana_atual = (strtotime($regras['data_abertura']) > strtotime($proximaSegunda)) ? $regras['data_abertura'] : $proximaSegunda;

			$atuNrbombasBicos = array(	'id' 					=> 	1,
										'total_bicos_atual'		=> 	$totalB['total_bicos'],
										'total_bombas_atual'	=> 	$totalB['total_bombas'],
										'dt_semana_atual'		=> 	$data_semana_atual	);

			$inserePedidos = array( 'pedido_id' 			=> 	$id,
									'dt_semana_prog' 		=> 	$data_semana_atual,	
									'nr_bicos'				=> 	$totalB['total_bicos'],
									'nr_bombas'				=> 	$totalB['total_bombas'],
									'dt_previsao_entrega' 	=> 	date('Y-m-d',strtotime($data_semana_atual.' +15 days'))	);

		}

		$this->programacaoProdM->atualiza($atuNrbombasBicos);
		
		$this->programacaoProdM->inserirPedido($inserePedidos);

		return true;
    }

    public function bombasExt()
	{
		$this->load->view('home/bombas-ext');
	}

	public function bombasExtIng()
	{
		$this->load->view('home/bombas-ext-ing');
	}

	public function alertaChamadosAtrasadosParceiros()
	{
		
		//$emails = $this->chamadoM->buscaEmailParceirosAtraso();
		$emails[0]['email'] = 'mateus.santos@gmail.com';
		$emails[0]['parceiro_id'] = 2;
		$emails[1]['email'] = 'suporte@wertco.com.br';
		$emails[1]['parceiro_id'] = 2;
		foreach( $emails as $email ){

		$corpo = '<html>
				<head></head>
				<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
				
				<div style="margin: 0 auto; width: 600px; background-image: url(https://www.wertco.com.br/email-cliente/backgroundCabecalhoChamado.png); height: 528px; "></div>
				<div style="width: 600px; background-color: #ffffff; height: 100%">
					<p>Prezados técnicos,<br/><br/>
					Gostaríamos de lembrar a importância do fechamento dos chamados de serviço em nosso sistema. Como vocês já sabem, a conclusão desses chamados é uma condição fundamental para que possamos acompanhar os atendimentos e efetuar o pagamento pelos serviços prestados.<br/><br/>
					Por isso, solicitamos a sua colaboração para que todos os chamados pendentes abaixo sejam prontamente fechados no sistema.<br/><br/> 
					Qualquer dúvida ou problema, por favor, não hesitem em nos contatar.<br>
					</p>
					<table style="padding-top: 20px;font-size: 12px;width: 100%;border: 1px solid #f0f0f0;">
						<thead>
								<th style="padding-bottom: 5px;font-weight: 700;">Chamado #</th>
								<th style="padding-bottom: 5px;font-weight: 700;">Cliente</th>
								<th style="padding-bottom: 5px;font-weight: 700;">Cidade</th>
								<th style="padding-bottom: 5px;font-weight: 700;">Data</th>
								<th style="padding-bottom: 5px;font-weight: 700;">Status</th>
						</thead>
						<tbody>
							<tr>
								
							</tr>';
		$dados = $this->chamadoM->listaChamadosAtrasadosParceiros($email['parceiro_id']);
			foreach( $dados as $dado ){
				$corpo.='	<tr>
								<td style="">'.$dado['chamado_id'].'</td>
								<td style="">'.$dado['cliente'].'</td>
								<td style="">'.$dado['cidade'].'/'.$dado['estado'].'</td>
								<td style="">'.$dado['inicio'].'</td>
								<td style="">'.$dado['status'].'</td>
							</tr>';
				}			
				$corpo.='</tbody>
					</table>
					</div>					
					</body>
					</html>';	

		
		$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('suporte@wertco.com.br')    // Optional, an account where a human being reads.
		    ->to($email['email'])
		    ->subject('WERTCO - Lista de Chamados Pendentes')
		    ->message($corpo)
		    ->send();

		}	
	}

	public function atualizaOpsAvulsas()
	{
		$this->opsM->atualizaOpsAvulsas();
	}

	public function politicaprivacidadelgpd($value='')
	{
		$this->load->view('home/politica-privacidade');

	}
	
}