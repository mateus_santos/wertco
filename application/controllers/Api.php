<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class Api extends CI_Controller {
	public function __construct() {
		parent::__construct();
		// carrega a model a ser utilizada neste controller
		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('OrdemProducaoModel', 'ordemM');
		$this->load->model('JsonModel', 'jsonM');
	}


	public function comando($c=0){

		$inputJSON = file_get_contents('php://input');
		$input = json_decode($inputJSON); //convert JSON into obj

		if($input != null){
			switch ($c) {
				case 'autenticacao':
				$result = $this->usuariosM->autenticacao( stripslashes($input->{'email'}), stripslashes($input->{'senha'}));
				if(count($result) > 0 ){
					$newdata = array(
						'usuario_id'	=> 	$result[0]->id,
						'nome'  		=>	$result[0]->nome,
						'email'     	=>	$result[0]->email,
						'tipo_acesso'	=>	$result[0]->tipo_acesso,
						'empresa_id'	=>	$result[0]->empresa_id,
						'foto'			=>	$result[0]->foto,
						'logged_in' 	=> 	TRUE);

					echo json_encode($newdata);	
				}else{
					echo "false";
				}
				break;

				case 'esquecisenha':

					$result	= 	$this->usuariosM->verificaUsuario(stripslashes($input->{'email'}, $input->{'cpf'}));

					if( $result[0]->total == 0)
					{
						echo 'Dados inválidos!';
					}
					else{
						$url_encode = base_url('usuarios/resetar/'.base64_encode($result[0]->id).'/'.base64_encode($result[0]->cpf));

						if($this->enviaLinkReset($email, $url_encode, $result[0]->nome )) {

							echo 'Sucesso!';
						}else{
							echo 'Não enviou email!';
						}	
					}			

					break;

					default:
						echo "false";
					break;
				}

			}else{
				echo "false";	
			}

		}	

		public function authWertco(){

			if( $_POST['chave'] == '53dc3e9581738a356b99b13acbb0b55c' ){

				$result = $this->usuariosM->autenticacao( stripslashes($_POST['email']), stripslashes($_POST['senha']));
			
				if(count($result) > 0 ){
					$hash = password_hash('ÉAmaiorP@nturr@doMund0', PASSWORD_DEFAULT);
					$data = array(
						'usuario_id'	=> 	$result[0]->id,
						'nome'  		=>	$result[0]->nome,
						'email'     	=>	$result[0]->email,
						'tipo_acesso'	=>	$result[0]->tipo_acesso,
						'hash'			=> 	$hash );

					echo json_encode($data);	

				}else{
					echo json_encode(array('erro' => 'Login e/ou senha incorretos.'));			
				}

			}else{
				echo json_encode(array('erro' => 'Chave Incorreta.'));
			}

		}

		public function getOpFoto(){
						
			$usuario_id = $_POST['usuario_id'];
			$op			= $_POST['op'];
			$data		= $_POST['data'];			

			$tipo 			= 	explode('.', $_FILES['foto']['name']);
			$arquivo 		=	md5($_FILES['foto']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
			
			$configuracao = array(
		        'upload_path'   	=> 	'./upApp/',		        
		        'allowed_types' 	=> 	'jpg|png',
		        'file_name'     	=> 	$arquivo
		    );      

		    $this->load->library('upload', $configuracao);
		    
		    if ($this->upload->do_upload('foto')){
				
		    	$update = array( 'id' 			=>	$op,
		    					 'foto' 		=> 	$arquivo,
		    					 'usuario_foto'	=>	$usuario_id,
		    					 'data_foto'	=>  date("Y-m-d H:i:s", strtotime($data)),
 		    					 'status_op_id'	=> 	3 );
  
		    	if($this->ordemM->atualizarOrdemProducao($update)){
		    		echo 'true';
		    	}else{
		    		echo 'false';
		    	}


			}else{
				echo 'false';	
			}
		}

		public function teste(){
			var_dump($_POST);
		}

	public function getSimultaneo()
	{

		$linhas = $this->jsonM->getSimultaneo(
			$this->input->post('familia'),
			$this->input->post('tipo'),
			$this->input->post('ramo'),
			$this->input->post('numBicos'),
			$this->input->post('latfront')
		);

		$pr["abast_simultaneo"] = array();

		foreach ($linhas as $saida) {
			$pr["abast_simultaneo"][] =  $saida->abast_simultaneos;
		}
		echo json_encode($pr);
	}

	public function getVazao()
	{
		$linhas = $this->jsonM->getVazao(
			$this->input->post('familia'),
			$this->input->post('tipo'),
			$this->input->post('ramo'),
			$this->input->post('numBicos'),
			$this->input->post('latfront'),
			$this->input->post('absSimultaneo'),
			$this->input->post('hidraulico')
		);

		$pr["vazao"] = array();

		foreach ($linhas as $saida) {
			$pr["vazao"][] =  $saida->vazao;
		}
		echo json_encode($pr);
	}

	public function getHidraulico()
	{
		$linhas = $this->jsonM->getHidraulico(
			$this->input->post('familia'),
			$this->input->post('tipo'),
			$this->input->post('ramo'),
			$this->input->post('numBicos'),
			$this->input->post('latfront'),
			$this->input->post('absSimultaneo'),

		);
		$pr["hidraulico"] = array();

		foreach ($linhas as $saida) {
			$pr["hidraulico"][] =  $saida->conjunto_hidraulico;
		}
		echo json_encode($pr);
	}

	public function getModelo()
	{
		$linhas = $this->jsonM->getModelo(
			$this->input->post('familia'),
			$this->input->post('tipo'),
			$this->input->post('ramo'),
			$this->input->post('numBicos'),
			$this->input->post('latfront'),
			$this->input->post('absSimultaneo'),
			$this->input->post('vazao'),
			$this->input->post('hidraulico')
		);
		$pr["modelo"] = array();

		foreach ($linhas as $saida) {
			$pr["modelo"][] =  $saida->modelo;
		}
		echo json_encode($pr);
	}


	public function runJson()
	{
		$m = $this->input->post('modelo');
		$v = $this->input->post('vazao');

		$command = escapeshellcmd("python3 run.py " . $m . " " . $v . " print");
		$output = shell_exec($command);
		echo $output;
	}

		
}