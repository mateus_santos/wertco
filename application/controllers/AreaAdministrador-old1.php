<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class AreaAdministrador extends MY_Controller { 
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'administrador comercial' && $this->session->userdata('tipo_acesso') != 'administrador tecnico' && $this->session->userdata('tipo_acesso') != 'administrador geral' && $this->session->userdata('tipo_acesso') != 'assistencia tecnica' && $this->session->userdata('tipo_acesso') != 'feira' &&  $this->session->userdata('tipo_acesso') != 'qualidade' && $this->session->userdata('tipo_acesso') != 'suprimentos' && $this->session->userdata('tipo_acesso') != 'almoxarifado' && $this->session->userdata('tipo_acesso') != 'financeiro' && $this->session->userdata('tipo_acesso') != 'suporte' && $this->session->userdata('tipo_acesso') != 'comunicacao' ) ) {
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('ProdutosModel', 'produtosM');
		$this->load->model('OrcamentosModel', 'orcamentosM');
		$this->load->model('IcmsModel', 'icmsM');
		$this->load->model('DownloadsModel', 'downloadsM');
		$this->load->model('CartaoTecnicosModel', 'cartaoM');
		$this->load->model('FretesModel', 'fretesM');
		$this->load->model('EntregasModel', 'entregasM');
		$this->load->model('FormaPagtoModel', 'formaPagtoM');
		$this->load->model('TreinamentoTecnicoModel' , 'treinamentoM');
		$this->load->model('PedidosModel' , 'pedidosM');
		$this->load->model('ItensModel' , 'itensM');
		$this->load->model('OrdemProducaoModel' , 'opM');
		$this->load->model('ChamadoModel' , 'chamadoM');
		$this->load->model('AtividadeModel' , 'atividadeM');
		$this->load->model('SubAtividadeModel' , 'subatividadeM');
		$this->load->model('ChamadoDefeitoModel' , 'defeitoM');
		$this->load->model('ChamadoCausaModel' , 'causaM');
		$this->load->model('ChamadoSolucaoModel' , 'solucaoM');
		$this->load->model('AtividadeCausaSolucaoModel' , 'causa_solucaoM');
		$this->load->model('ConfiguracaoModel' , 'configuracaoM');
		$this->load->model('ConfiguracaoBombaModel' , 'configuracao_bombaM');
		$this->load->model('ConfiguracaoConcentradorModel' , 'configuracao_concentradorM');
		$this->load->model('ConfiguracaoIdentificadorModel' , 'configuracao_identificadorM');
		$this->load->model('DespesasModel' , 'despesasM');
		$this->load->model('OrdemPagamentoModel' , 'ordemPagtoM');
		$this->load->model('SolicitacaoPecasModel' , 'solicitacaoPecasM');
		$this->load->model('PecasModel' , 'pecasM');
		$this->load->model('MotivoExclusaoModel' , 'motivoM');
		$this->load->model('SolicitacaoPecasItensModel' , 'solicitacaoPecasItensM');
		$this->load->model('AutorizServicoModel' , 'autorizServicoM');
		$this->load->model('AreaAtuacaoModel' , 'areaAtuacaoM');
		$this->load->model('MotivoRetornoExpModel' , 'motivoExpM');
		$this->load->model('OrcamentosAlertaModel' , 'orcamentosAlertasM');		
		$this->load->model('ZonaAtuacaoModel' , 'zonaM');		
		$this->load->model('ProgramacaoProducaoRegrasModel','programacaoProdM');
		$this->load->model('ProducaoAndamentoModel','producaoAndamentoM');
		$this->load->model('BombasNrSerieModel','nrSerieM');
		
		$this->load->helper('form');
		$this->load->helper('url');

	}
 
	public function index()
	{
		$parametros						 	=	$this->session->userdata();
		$parametros['dados']			 	=	$this->usuariosM->getUsuarios();		
		$parametros['title']			 	=	"Área do Administrador do sistema";
		/* Orçamentos */
		$parametros['total_status']		 	=	$this->orcamentosM->selectTotalStatus();							
		$parametros['total_valor_mes']	 	= 	$this->orcamentosM->totalFechadoMes();
		$parametros['total_aberto']		 	= 	$this->orcamentosM->totalAberto();
		$parametros['produtos_orcados']	 	=	$this->orcamentosM->produtosOrcados();
		$parametros['orcamento_inativo'] 	=	$this->orcamentosM->buscaOrcamentosInatividade();
		$parametros['total_bombas'] 		=	$this->orcamentosM->totalBombas();
		$parametros['valor_bombas'] 		=	$this->orcamentosM->totalBombasOpcionais();
		/* pedidos */
		$parametros['total_status_ped']	 		=	$this->pedidosM->selectPedidoTotalStatus();					
		$parametros['total_valor_mes_ped']		= 	$this->pedidosM->totalFechadoMesPed();
		$parametros['total_bombas_ped']			=	$this->pedidosM->totalBombasPed();
		$parametros['valor_bombas_ped']			=	$this->pedidosM->totalBombasOpcionaisPed();

		$parametros['total_valor_mes_ped_c']		= 	$this->pedidosM->totalConfirmadosMesPed();
		$parametros['total_bombas_ped_c']			=	$this->pedidosM->totalBombasConfirmadasPed();		
		$parametros['total_valor_mes_ped_ant'] 		= 	$this->pedidosM->totalConfirmadosMesPedAnterior();
		$parametros['total_valor_mes_ped_ant_ant'] 	= 	$this->pedidosM->totalConfirmadosMesPedAntAnt();
		//$parametros['valor_bombas_ped_c']			=	$this->pedidosM->totalBombasOpcionaisConfirmadasPed();
		$parametros['total_pedidos']				= 	$this->pedidosM->totalPedidosConfirmadosAno();
		$parametros['produtos_vendidos'] 			= 	$this->pedidosM->top5ModelosMaisVendidos();
 		$parametros['status_']						=	$this->pedidosM->buscaStatus();

 		$parametros['total_acumulado_atual']    	=	$this->pedidosM->totalConfirmadosAcumuladoAno(date('Y'));
 		$parametros['total_acumulado_ant']    		=	$this->pedidosM->totalConfirmadosAcumuladoAno(date('Y', strtotime('-1 year', strtotime(date('Y')))));
 		$parametros['total_acumulado_ant_ant'] 		=	$this->pedidosM->totalConfirmadosAcumuladoAno(date('Y', strtotime('-2 year', strtotime(date('Y')))));
		if($this->session->userdata('usuario_id') == 2465 || $this->session->userdata('usuario_id') == 2){
			$parametros['orcamento_aguardando'] 	=	$this->orcamentosM->buscaOrcamentosAprovacao();						
			$parametros['pedidos_aguardando'] 		=	$this->pedidosM->buscaPedidosAprovacao();						
		}		
		$this->_load_view('area-administrador/index',$parametros);

	}

	public function relatorioMensalVendas(){
    	    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-mensal-bombas.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');	    
	    
	    $where = " extract( year from op.dthr_geracao ) = '".date('Y')."'";

	    $data['dados']	=	$this->pedidosM->relatorioMensalBombas($where);
				
		$this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/pedidos/relatorio-mensal-bombas-pdf', $data,true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-mensal-bombas.pdf'));
	    
		}
    }

    public function relatorioVendasModelo(){
    	    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-vendas-modelo-bombas.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');	    
	    
	    $where = " extract( year from op.dthr_geracao ) = '".date('Y')."'";

	    $data['dados']	=	$this->pedidosM->relatorioVendasModelo($where);
				
		$this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/pedidos/relatorio-vendas-modelo-bombas-pdf', $data,true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-vendas-modelo-bombas.pdf'));
	    
		}
    }

	public function confirmaOrcamentoGestor(){

		$update = array(	'id' 					=> 	$this->input->post('orcamento_id'),
							'status_orcamento_id'	=> 	1	);

		if($this->orcamentosM->atualizaOrcamento($update)){

			$andamentoOrcamento = array(	'andamento' 			=> 	'Orçamento aprovado pelo gestor!',
	 										'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
	 										'orcamento_id'			=> 	$this->input->post('orcamento_id'),
	 										'status_orcamento_id'	=> 	1 	);	

 			if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento)){
 				echo json_encode(array('retorno' => 'sucesso'));
 			}

		}
	
	}

	public function buscaOrcamentosPorStatus(){

		$retorno = $this->orcamentosM->buscaOrcamentosPorStatus($_POST['status']);		
		echo json_encode($retorno); 

	}

	public function buscaOrcamentosPorPeriodo(){
		$retorno = $this->orcamentosM->selectTotalStatusPeriodo($_POST['periodo']);
		echo json_encode($retorno);
	}

	public function buscaOrcamentosFechadosMes(){

		$retorno = $this->orcamentosM->buscaOrcamentosFechadosMes($_POST['mes']);		
		echo json_encode($retorno);
	}

	public function buscaPedidosConfirmadosMes()
	{
		$retorno = $this->pedidosM->buscaPedidosConfirmadosMes($this->input->post('mes'));
		echo json_encode($retorno);
	}

	public function buscaOrcamentosPorStatusPeriodo(){

		$retorno = $this->orcamentosM->buscaOrcamentosPorStatusPeriodo($_POST['status'],$_POST['periodo']);		
		echo json_encode($retorno);
	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){
			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-administrador/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-administrador/editar-cliente',	$parametros );	
		}
	
	}

	public function gestaoUsuarios()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();		
		$parametros['title']	=	"Gestão de Usuários";
		$this->_load_view('area-administrador/gestao-usuario/gestao-usuario',$parametros);
	}

	/****************************************************************************
	**************** Método Ajax - Alterar status do usuario ********************
	*****************************************************************************/
	public function alteraStatus()
	{
		
		$dados = array(	'id'		=>	$_POST['id'],
						'ativo'	=>	$_POST['ativo']	);

		if($this->usuariosM->atualizaStatus($dados)){
			$this->log('Área Administrador | ativa usuário','usuarios','EDIÇÂO', $this->session->userdata('usuario_id'), $dados,$dados,$_SERVER['REMOTE_ADDR']);
			if($_POST['ativo'] == 1){

				if($this->enviaEmailConfirmacao($_POST['email'])){

					echo json_encode(array('retorno' => 'sucesso'));
				}else{

					echo json_encode(array('retorno' => 'e-mail não enviado'));
				}
			}else{

				echo json_encode(array('retorno' => 'sucesso'));
			}
		}else{

			echo json_encode(array('retorno' => 'erro'));
		}

	}
	

	private function enviaEmailConfirmacao($email_destino){

 		$email = '	<html>
						<head></head>
						<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
							<div class="content" style="width: 600px; height: 100%;">
								<img src="http://www.wertco.com.br/bootstrap/img/RetornoWertco.png" />
							</div>
						</body>
					</html>';	
		
		$this->load->library('email');
		
		$result = $this->email
		    ->from('vendas@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to($email_destino)
		    ->subject('WERTCO - Confirmação de Acesso')
		    ->message($email)
		    ->send();

		return $result;	

 	}

	/****************************************************************************
	*****************************************************************************
	**************** Método Responsável por Editar Usuários	*********************
	*****************************************************************************
	*****************************************************************************/

	public function editarUsuario($id = null){

		if($this->input->post('salvar') == 1){
			
			$update = $this->input->post();			
			unset($update['salvar']);
			unset($update['senha2']);
			unset($update['senha']);
			
			if($this->input->post('dt_treinamento') != null && $this->input->post('dt_treinamento') != ''){
				$data 			= 	explode('/',$this->input->post('dt_treinamento'));
				$dt_treinamento	= 	'20'.$data[2].'-'.$data[1].'-'.$data[0];
				$update['dt_treinamento']	=	$dt_treinamento;
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha')));
			}
			 
			if($this->usuariosM->atualizaUsuario($update)){				
				$this->log('Área Administrador | atualiza usuário','usuarios','EDIÇÂO', $this->session->userdata('usuario_id'), $update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['title']	=	"Gestão de Usuários";
				$parametros['dados']	=	$this->usuariosM->getUsuarios();
				$this->_load_view('area-administrador/gestao-usuario/gestao-usuario',	$parametros );
			}
		}else{
			
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->usuariosM->getUsuario($id);
			$parametros['title']				=	"Editar Usuários";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();
			$parametros['subtipos']				= 	$this->usuariosM->buscaSubtipoCadastro();			
			$this->_load_view('area-administrador/gestao-usuario/editar-usuario',	$parametros );
		}
	}

	public function produtos()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->produtosM->select();		
		$parametros['icms']		=	$this->icmsM->retornaFator();
		$parametros['title']	=	"Gestão de Produtos";
		$this->_load_view('area-administrador/produtos/produtos',$parametros);
	}

	/****************************************************************************
	************* Método Responsável por cadastrar produtos da wertco ***********
	*****************************************************************************/
	public function cadastraProdutos()
	{
		
		if($this->input->post('salvar') == 1){

			$insert = array('id'				=>	$this->input->post('id'),
							'codigo'			=>	$this->input->post('codigo'),
							'descricao'			=>	$this->input->post('descricao'),
							'modelo'			=>	$this->input->post('modelo'),
							'modelo_tecnico'	=>	$this->input->post('modelo_tecnico'),
							'vazao'				=>	$this->input->post('vazao'),
							'nr_rotativa'		=>	$this->input->post('nr_rotativa'),
							'valor_unitario'	=>	str_replace(",", ".", str_replace(".","", $this->input->post('valor_unitario'))),
							'tipo_produto_id'	=>	$this->input->post('tipo_produto_id'),
							'nr_bicos'			=> 	$this->input->post('nr_bicos'),
							'nr_produtos'		=> 	$this->input->post('nr_produtos')	);
						 
			if($this->produtosM->insereProduto($insert)){
				$this->log('Área Administrador | cadastra produtos','produtos','INSERÇÃO', $this->session->userdata('usuario_id'), $insert,$insert,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->produtosM->select();		
				$parametros['title']	=	"Gestão de Produtos";
				$this->_load_view('area-administrador/produtos/produtos',$parametros);
			}else{
				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->produtosM->select();		
				$parametros['title']	=	"Gestão de Produtos";
				$this->_load_view('area-administrador/produtos/produtos',$parametros);
			}

		}else{

			$parametros 						= 	$this->session->userdata();		
			$parametros['dados_tipo_cadastro']	=	$this->produtosM->tipoProdutos();
			$parametros['title']				=	"Gestão de Produtos";
			$this->_load_view('area-administrador/produtos/cadastra-produtos',$parametros);
		}

	}
	/****************************************************************************
	**************** Método Ajax - Excluir Produto ******************************
	*****************************************************************************/
	public function excluirProduto()
	{

		if($this->produtosM->excluirProduto($_POST['id'])){
			$this->log('Área Administrador | excluir produtos','produtos','EXCLUSÃO', $this->session->userdata('usuario_id'), array('id' => $_POST['id']),array('id' => $_POST['id']),$_SERVER['REMOTE_ADDR']);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			
			echo json_encode(array('retorno' => 'erro'));
		}
	
	}
	/****************************************************************************
	************* Método Responsável por editar os produtos da wertco ***********
	*****************************************************************************/
	public function editarProduto($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'codigo'			=>	$this->input->post('codigo'),
							'descricao'			=>	$this->input->post('descricao'),
							'modelo'			=>	$this->input->post('modelo'),
							'modelo_tecnico'	=>	$this->input->post('modelo_tecnico'),
							'vazao'				=>	$this->input->post('vazao'),
							'nr_rotativa'		=>	$this->input->post('nr_rotativa'),
							'tipo_produto_id'	=>	$this->input->post('tipo_produto_id'),
							'valor_unitario'	=>	(strpos($this->input->post('valor_unitario'),',') !== false ) ? str_replace(",", ".", str_replace(".","", $this->input->post('valor_unitario'))) : $this->input->post('valor_unitario'),
							'nr_bicos'			=> 	$this->input->post('nr_bicos'), 
							'nr_produtos'		=> 	$this->input->post('nr_produtos'));
						 
			if($this->produtosM->atualizaProduto($update)){
				$this->log('Área Administrador | atualizar produtos','produtos','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->produtosM->select();		
				$parametros['title']	=	"Gestão de Produtos";
				$this->_load_view('area-administrador/produtos/produtos',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->produtosM->select();		
				$parametros['title']	=	"Gestão de Produtos";
				$this->_load_view('area-administrador/produtos/produtos',$parametros);
			}

		}else{

			$row 								= 	$this->produtosM->getProduto($id);
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Produtos";
			$parametros['dados_tipo_produto']	=	$this->produtosM->tipoProdutos();
			$this->_load_view('area-administrador/produtos/editar-produto',	$parametros );
		}
	}

	public function orcamentos($pesquisa = null)
	{
		if( !$this->_is_logged() || $this->session->userdata('tipo_acesso') != 'administrador geral' ) {
			redirect(base_url('usuarios/login'));
			
		}

		$parametros 					= 	$this->session->userdata();
		$parametros['title']			=	"Orçamentos";
		$parametros['pesquisa']			= 	(strpos($pesquisa,'%20') >= 0) 	? 	str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$parametros['zonas']			= 	$this->zonaM->select();
		$parametros['representantes']	=	$this->usuariosM->retornaRepresentantesAtivos();
		$parametros['indicadores']		=	$this->usuariosM->retornaIndicadoresAtivos();
		$parametros['status']			=	$this->orcamentosM->selectStatus();
		$this->_load_view('area-administrador/orcamentos/orcamentos',$parametros);
	}

	public function orcamentosExpirados($pesquisa = null)
	{
		$parametros 			= 	$this->session->userdata();		
		$parametros['title']	=	"Orçamentos";
		$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$parametros['zonas']			= 	$this->zonaM->select();
		$parametros['representantes']	=	$this->usuariosM->retornaRepresentantesAtivos();
		$parametros['status']			=	$this->orcamentosM->selectStatus();
		$this->_load_view('area-administrador/orcamentos/orcamentos-expirados',$parametros);
	}
	
	public function buscaOrcamentosSS(){

		$dados 		=	$this->input->post();
		$retorno 	=	$this->orcamentosM->buscaOrcamentos($dados);
		echo json_encode($retorno);
	
	}

	public function buscaOrcamentosSSExpirados(){
		$dados 	 = $this->input->post();
		$retorno =	$this->orcamentosM->buscaOrcamentosExpirados($dados);
		echo json_encode($retorno);
	}

	public function buscaProximosStatus()
	{
		$retorno = $this->orcamentosM->buscaProximosStatusOrcamento($_POST['orcamento_id']);			
		echo json_encode($retorno);
		
	}

	public function buscaAndamentos()
	{
		$retorno = $this->orcamentosM->buscaAndamentos($_POST['orcamento_id']);
			
		echo json_encode($retorno);
		
	}
	
	public function alteraStatusOrcamento()
	{

		if( $_POST['indicacao'] == 1  ){
			if($_POST['aprovar_indicacao'] == 0){
				$_POST['status_orcamento'] = 8;
			}
		}

		$dados = array('id'						=>	$_POST['orcamento_id'],
					   'status_orcamento_id'	=>	$_POST['status_orcamento']);

		if($this->orcamentosM->atualizaStatusOrcamento($dados))
		{
			$this->log('Área Administrador | atualizar status orçamento','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
			if( $_POST['indicacao'] == 1  ){
				if($_POST['aprovar_indicacao'] == 1){
	        		$andamento = "Indicação Aprovada pela WERTCO!";
	        	}else{
	        		$andamento = "Indicação não Aprovada pela WERTCO!";
	        	}
			}else{
				
				switch($_POST['status_orcamento']){
					case 1:
				        $andamento = "Orçamento Aberto!";
				        break;
				    case 2:
				        $andamento = "Orçamento Fechado, venda realizada!";
				        break;
				    case 3:
				        $andamento = "Orçamento Perdido.";
				        break;
				    case 4:
				        $andamento = "Orçamento Cancelado.";
				        break;
				    case 5:
				        $andamento = "Orçamento entregue ao cliente.";
				        break;        
				    case 6:
				        $andamento = "Orçamento em negociação.";
				        break;
				     case 9:
				        $andamento = "Bombas em teste no cliente.";
				        break; 
				    case 10:
				        $andamento = "Orçamento Perdido para Wayne.";
				        break;
				    case 11:
				        $andamento = "Orçamento Perdido para Gilbarco.";
				        break;
				    case 14:
				    	$andamento = "Orçamento Sem Retorno.";
				        break;
				}
			}

			$inserirAndamento = array(	'orcamento_id'			=>	$_POST['orcamento_id'],
										'status_orcamento_id'	=> 	$_POST['status_orcamento'],
										'andamento'				=>	$andamento,
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id'));

			if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
			{
				$this->enviaEmailStatusGestor($_POST['orcamento_id'], $_POST['status_orcamento']);
				echo json_encode(array('retorno' => 'sucesso'));	
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}
	
	public function alteraIndicadorOrcamento(){

		$update = array('id' 			=> 	$_POST['orcamento_id'], 
						'indicador_id'	=>	$_POST['indicador'],
						'indicador'		=> 	$_POST['indicador'] );

		$indicador = $this->orcamentosM->getIndicador($_POST['orcamento_id']);

		if( $_POST['indicador'] == '' && isset($indicador[0]->id) ){
			$andamento 				= 'Exclusão do indicador #'.$indicador[0]->id.' ao orçamento.';
			

		}else{
			$andamento = (isset($indicador[0]->id)) ? 'Alteração de Indicador: #'.$indicador[0]->id.' => #'.$_POST['indicador'] : 'Indicador #'.$_POST['indicador'].'  	vinculado ao orçamento.'; 
		}

		$inserirAndamento = array(	'orcamento_id'			=>	$_POST['orcamento_id'],
									'status_orcamento_id'	=> 	(isset($indicador[0]->id)) ? $indicador[0]->status_orcamento_id : $_POST['status_orcamento_id'],
									'andamento'				=>	$andamento,
									'dthr_andamento'		=>	date('Y-m-d H:i:s'),
									'usuario_id'			=> 	$this->session->userdata('usuario_id') 	);

		if($this->orcamentosM->atualizaIndicador($update)){
			$this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{

			echo json_encode(array('retorno' => 'erro'));	
		}
	}

	public function insereAndamentoOrcamento(){

		$inserirAndamento = array(	'orcamento_id'			=>	$_POST['orcamento_id'],
									'status_orcamento_id'	=> 	$_POST['status_orcamento_id'],
									'andamento'				=>	$_POST['andamento'],
									'dthr_andamento'		=>	date('Y-m-d H:i:s'),
									'usuario_id'			=> 	$this->session->userdata('usuario_id')	);

		$last_id = $this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento);

		if($last_id){
			//Insere alerta orçamento!			
			if(	$_POST['dthr_alerta']	!=	''	){
				$data_alerta 	= 	explode('*', $_POST['dthr_alerta']);
				
				$insereAlerta 	= 	array(	'orcamento_andamento_id'	=>	$last_id,
											'dthr_alerta'				=>	$data_alerta[0].'-'.$data_alerta[1].'-'.$data_alerta[2],
											'usuario_id'				=> 	$this->session->userdata('usuario_id')	);		

				$this->orcamentosM->insereAlerta($insereAlerta);

			}

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));	

		}

	}

	public function retornaRepresentantes()
	{
	
		$rows = $this->usuariosM->retornaRepresentante($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'		 =>		$row['label'],
									'id'	 	 =>		$row['id'],
									'value'		 =>		$row['label'],
									'empresa_id' => 	$row['id']	);
				
			}

			echo json_encode($dados);
		}
	}

	public function retornaRepresentantesAtivos()
	{
		$rows = $this->usuariosM->retornaRepresentantesAtivosAutoComplete($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'		 =>		$row['label'],
									'id'	 	 =>		$row['id'],
									'value'		 =>		$row['label'],
									'empresa_id' => 	$row['empresa_id']	);
				
			}

			echo json_encode($dados);
		}	
	}

	public function retornaResponsavelOrcamentos()
	{
	
		$rows = $this->usuariosM->retornaIndicadores($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'		 =>		$row['label'],
									'id'	 	 =>		$row['id'],
									'value'		 =>		$row['label'],
									'empresa_id' => 	$row['empresa_id']	);
				
			}

			echo json_encode($dados);
		}
	}

	public function retornaIndicadores()
	{
		
		$rows = $this->usuariosM->retornaIndicadores($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label' 		=>	$row['label'],
								 	'id'			=>	$row['id'],
								 	'value'			=>	$row['label'],
								 	'empresa_id' 	=> 	$row['empresa_id'] 	);
				
			}

			echo json_encode($dados);
		}
	}

	public function alteraResponsavelOrcamento()
	{
		// SE O ID DA TABELA VIER EM BRANCO, INSERE NOVO REPREENTANTE, SE NÃO ATUALIZA O REPRESENTANTE RESPONSÁVEL
		if( $_POST['orcamento_responsavel_id'] == '' )
		{
			$insereResponsavel =  array('orcamento_id' 	=> 	$_POST['orcamento_id'],
										'usuario_id'	=> 	$_POST['responsavel_id']	);

			if($this->orcamentosM->insereResponsavelOrcamento($insereResponsavel)){

				$this->log('Área Administrador | responsavel_orcamento','orcamentos','INSERÇÃO', $this->session->userdata('usuario_id'), $insereResponsavel,$insereResponsavel,$_SERVER['REMOTE_ADDR']);
				$insereAndamentoOrcamento 	= 	array('orcamento_id' 		=> 	$_POST['orcamento_id'],
													  'andamento'			=> 	'Orçamento enviado para o representante responsável',
													  'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													  'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													  'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);

				if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
				{

					echo json_encode(array('retorno' => 'sucesso'));	
				}else{

					echo json_encode(array('retorno' => 'erro'));	
				}			
			}else{

				echo json_encode(array('retorno' => 'erro'));	
			}

		}else{
			$atualizaResponsavel 	= 	array(	'id'			=>	$_POST['orcamento_responsavel_id'],
					   	   						'usuario_id' 	=>	$_POST['responsavel_id']);

			if( $this->orcamentosM->atualizaResponsavelOrcamento($atualizaResponsavel) )
			{
				$this->log('Área Administrador | responsavel_orcamento','orcamentos','EDIÇÂO', $this->session->userdata('usuario_id'), $atualizaResponsavel,$atualizaResponsavel,$_SERVER['REMOTE_ADDR']);
				$insereAndamentoOrcamento 	= 	array('orcamento_id' 		=> 	$_POST['orcamento_id'],
													  'andamento'			=> 	'Alterado o indicador responsável pelo orçamento.',
													  'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													  'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													  'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
				
				if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
				{

					echo json_encode(array('retorno' => 'sucesso'));	
				}else{

					echo json_encode(array('retorno' => 'erro'));	
				}
			}else{

				echo json_encode(array('retorno' => 'erro'));	
			}
		}
	}

	public function visualizarOrcamento($id,$pesquisa = null)
	{
		if( !$this->_is_logged() || $this->session->userdata('tipo_acesso') != 'administrador geral' ) {
			redirect(base_url('usuarios/login'));
		}

		$parametros 				= 	$this->session->userdata();
		$parametros['dados']		=	$this->orcamentosM->getOrcamentoProdutos($id);
		$parametros['desconto']		=	$this->orcamentosM->getOrcamentoDesconto($id);
		$parametros['solicitante']	=	$this->orcamentosM->getSolicitante($id);
		$parametros['indicador']	=	$this->orcamentosM->getIndicador($id);
		$parametros['bombas'] 		= 	$this->produtosM->select();
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['contato_posto']= 	$this->orcamentosM->selectContatoPosto($id);
		$parametros['title']		=	"Orçamento #".$id;
		$parametros['pesquisa']		= 	htmlentities($pesquisa);
		$this->_load_view('area-administrador/orcamentos/visualiza-orcamento',$parametros);
	}

	public function emitirOrcamento()
	{
		if( $_POST['salvar_desconto'] == "1")
		{
			$dados_desconto = array('orcamento_id' 		=> 	$_POST['orcamento_id'],
									'valor_desconto'	=>	$_POST['valor_desconto'],
									'motivo_desconto'	=> 	$_POST['motivo_desconto'] );

			if( $this->orcamentosM->insereOrcamentosDesconto( $dados_desconto ) )
			{

				
				$status_orcamento_id = $_POST['status_orcamento_id'];
				

				//altera o status para emitido	
				$dados = array('id'						=>	$_POST['orcamento_id'],
					   			'status_orcamento_id'	=>	$status_orcamento_id);

				if($this->orcamentosM->atualizaStatusOrcamento($dados)){

					//orçamento entregue/emitido
					$andamentoOrcamento = array('andamento' 			=> 'Orçamento emitido ao cliente por '.$this->session->userdata('nome').' da WERTCO!',
			 									'dthr_andamento'		=> date('Y-m-d H:i:s'),
			 									'orcamento_id'			=> $_POST['orcamento_id'],
			 									'status_orcamento_id'	=> $status_orcamento_id,
			 									'usuario_id'			=> $this->session->userdata('usuario_id')
			 									);	
		 			if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento)){

						$retorno = $this->enviaEmailEmissaoCliente($_POST['email_destino'],$_POST['orcamento_id'], $_POST['valor_tributo'], $_POST['valor_total'],$_POST['contato_posto']);
						if($retorno){
							$this->enviaEmailEmissaoCliente('vendas@wertco.com.br',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');
							$this->enviaEmailEmissaoCliente('vendas1@wertco.com.br',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');
							$this->enviaEmailEmissaoCliente('vendas2@wertco.com.br',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');
							$this->enviaEmailEmissaoCliente('vendas3@wertco.com.br',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');
							$this->enviaEmailEmissaoCliente('gestor@wertco.com.br',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');
							echo json_encode(array('retorno' => 'sucesso'));
						}else{
							echo json_encode(array('retorno' => 'erro'));	
						}
					}else{
						echo json_encode(array('retorno' => 'erro'));	
					}
				}else{
					echo json_encode(array('retorno' => 'erro'));	
				}	
			}else{
				echo json_encode(array('retorno' => 'erro'));
			}
		}else{			
			
			$status_orcamento_id = $_POST['status_orcamento_id'];
			
			//altera o status para emitido	
			$dados = array('id'						=>	$_POST['orcamento_id'],
				   		   'status_orcamento_id'	=>	$status_orcamento_id);

			if($this->orcamentosM->atualizaStatusOrcamento($dados)){

				//orçamento entregue/emitido
				$andamentoOrcamento = array('andamento' 			=> 'Orçamento emitido ao cliente por '.$this->session->userdata('nome').' da WERTCO!',
		 									'dthr_andamento'		=> date('Y-m-d H:i:s'),
		 									'orcamento_id'			=> $_POST['orcamento_id'],
		 									'status_orcamento_id'	=> $status_orcamento_id,
		 									'usuario_id'			=> $this->session->userdata('usuario_id') );	
	 			if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento)){

					$retorno = $this->enviaEmailEmissaoCliente($_POST['email_destino'],$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total']);
					if($retorno){
						$this->enviaEmailEmissaoCliente('vendas@wertco.com.br',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total']);
						$this->enviaEmailEmissaoCliente('vendas1@wertco.com.br',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total']);
						$this->enviaEmailEmissaoCliente('vendas2@wertco.com.br',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total']);
						$this->enviaEmailEmissaoCliente('vendas3@wertco.com.br',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total']);
						echo json_encode(array('retorno' => 'sucesso'));
					}else{
						echo json_encode(array('retorno' => 'erro'));	
					}
				}else{
					echo json_encode(array('retorno' => 'erro'));	
				}
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}	
		}
	}

	private function enviaEmailEmissaoCliente( $email_destino, $orcamento_id, $valor_tributo, $valor_total, $contato_posto = "" )
	{
		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);
		
		$total_desconto 	= 	"";
		
		$anexo = $this->geraOrcamento($orcamento_id,'email',$contato_posto);

		$email = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title>tst</title><style type="text/css">
        /* Resets */
        .ReadMsgBody { width: 100%; background-color: #ebebeb;}
        .ExternalClass {width: 100%; background-color: #ebebeb;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        a[x-apple-data-detectors]{
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
        }        
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        body {margin:0; padding:0;}
        .yshortcuts a {border-bottom: none !important;}
        .rnb-del-min-width{ min-width: 0 !important; }

        /* Add new outlook css start */
        .templateContainer{
            max-width:590px !important;
            width:auto !important;
        }
        /* Add new outlook css end */

        /* Image width by default for 3 columns */
        img[class="rnb-col-3-img"] {
        max-width:170px;
        }

        /* Image width by default for 2 columns */
        img[class="rnb-col-2-img"] {
        max-width:264px;
        }

        /* Image width by default for 2 columns aside small size */
        img[class="rnb-col-2-img-side-xs"] {
        max-width:180px;
        }

        /* Image width by default for 2 columns aside big size */
        img[class="rnb-col-2-img-side-xl"] {
        max-width:350px;
        }

        /* Image width by default for 1 column */
        img[class="rnb-col-1-img"] {
        max-width:550px;
        }

        /* Image width by default for header */
        img[class="rnb-header-img"] {
        max-width:590px;
        }

        /* Ckeditor line-height spacing */
        .rnb-force-col p, ul, ol{margin:0px!important;}
        .rnb-del-min-width p, ul, ol{margin:0px!important;}

        /* tmpl-2 preview */
        .rnb-tmpl-width{ width:100%!important;}

        /* tmpl-11 preview */
        .rnb-social-width{padding-right:15px!important;}

        /* tmpl-11 preview */
        .rnb-social-align{float:right!important;}

        @media only screen and (min-width:590px){
        /* mac fix width */
        .templateContainer{width:590px !important;}
        }

        @media screen and (max-width: 360px){
        /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
        .rnb-yahoo-width{ width:360px !important;}
        }

        @media screen and (max-width: 380px){
        /* fix width and font size "tmpl-4 tmpl-6" in mobile preview */
        .element-img-text{ font-size:24px !important;}
        .element-img-text2{ width:230px !important;}
        .content-img-text-tmpl-6{ font-size:24px !important;}
        .content-img-text2-tmpl-6{ width:220px !important;}
        }

        @media screen and (max-width: 480px) {
        td[class="rnb-container-padding"] {
        padding-left: 10px !important;
        padding-right: 10px !important;
        }

        /* force container nav to (horizontal) blocks */
        td.rnb-force-nav {
        display: inherit;
        }
        }

        @media only screen and (max-width: 600px) {

        /* center the address &amp; social icons */
        .rnb-text-center {text-align:center !important;}

        /* force container columns to (horizontal) blocks */
        td.rnb-force-col {
        display: block;
        padding-right: 0 !important;
        padding-left: 0 !important;
        width:100%;
        }

        table.rnb-container {
         width: 100% !important;
        }

        table.rnb-btn-col-content {
        width: 100% !important;
        }
        table.rnb-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-last-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table[class~="rnb-col-2"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-col-2-noborder-onright {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        }

        table.rnb-col-2-noborder-onleft {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-top: 10px;
        padding-top: 10px;
        }

        table.rnb-last-col-2 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table.rnb-col-1 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        img.rnb-col-3-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xs {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xl {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-1-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-header-img {
        /**max-width:none !important;**/
        width:100% !important;
        margin:0 auto;
        }

        img.rnb-logo-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        td.rnb-mbl-float-none {
        float:inherit !important;
        }

        .img-block-center{text-align:center !important;}

        .logo-img-center
        {
            float:inherit !important;
        }

        /* tmpl-11 preview */
        .rnb-social-align{margin:0 auto !important; float:inherit !important;}

        /* tmpl-11 preview */
        .rnb-social-center{display:inline-block;}

        /* tmpl-11 preview */
        .social-text-spacing{margin-bottom:0px !important; padding-bottom:0px !important;}

        /* tmpl-11 preview */
        .social-text-spacing2{padding-top:15px !important;}

    }</style><!--[if gte mso 11]><style type="text/css">table{border-spacing: 0; }table td {border-collapse: separate;}</style><![endif]--><!--[if !mso]><!--><style type="text/css">table{border-spacing: 0;} table td {border-collapse: collapse;}</style> <!--<![endif]--><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body>

<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#f9fafc" style="background-color: rgb(249, 250, 252);">

    <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr>
    <tr>
        <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="590" style="width:590px;">
                        <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
        <tbody><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_5" id="Layout_5">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:600px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/backgroundCabecalho.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_6">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div>&nbsp;</div>

<div>
<table style="color: rgb(0, 0, 0); font-family: sans-serif; font-style: normal; padding-top: 20px; font-size: 16px; margin-left: 15px; width: 100%;">
   <tbody>
                    <tr>
                        <td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
                        <td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
                        <td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
                        <td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
                        <td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
                        <td style="padding-bottom: 18px;font-weight: 700;"></td>
                        <td style="padding-bottom: 18px;"></td>                        
                    </tr>
                    <tr>
                        <td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
                        <td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
                        <td style=" font-weight: 700; text-align: right;">Telefone:</td>
                        <td>'.$empresa['telefone'].'</td>
                    </tr>
                    <tr>
                        <td style=" font-weight: 700;">E-mail:</td>
                        <td colspan="3">'.$empresa['email'].'</td>
                    </tr>
                </tbody>
</table>
</div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_7" id="Layout_7">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffcc00" align="center" cellspacing="0" style="background-color: rgb(255, 204, 0);">
                            <tbody><tr>
                                <td valign="top" align="left">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:139;;max-width:139px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="139" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/produtos.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_8">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><table cellspacing="0" style="width: 500px;margin-left: 14px">
    <thead style="height: 55px">
        <tr>
            <th style="height: 30px; text-align: center;">Modelo</th>
            <th style="text-align: center;">Descrição</th>
            <th>Qtd</th>
            <th style="text-align: right;">Valor</th>
        </tr>
    </thead>
    <tbody style="background-color: #fff;margin-left: 10px;font-size: 12px">';
       $total = 0; foreach($produtos as $produto){
            
            $email.='   <tr>
                        <td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
                        <td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
                        <td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>
                        <td style="   text-align: right; padding: 10px;">R$ '.number_format($produto['valor'], 2, ',', '.').'</td>
                    </tr>';                 
            $total = $total + ($produto['valor'] * $produto['qtd']);
        }   
        $email.='   <tr>
                        <td colspan="3" style="border-top: 1px solid #f0f0f0; border-right: 1px solid #f0f0f0;    padding: 10px;text-align: right;">
                        <b>Total (Sub-total +'.$valor_tributo.'% de ICMS + 5% de IPI)</b></td>
                        <td style="border-top: 1px solid #f0f0f0; text-align: right; padding: 10px;"><b>R$ '.number_format($total, 2, ',', '.').'</b></td>
                    </tr>';
        /*          <tr>
                        <td colspan="3" style=" border-top: 1px solid #f0f0f0;border-right: 1px solid #f0f0f0;    padding: 10px;text-align: right;"><b>Total (Sub-Total + '.$valor_tributo.'% de ICMS + 5% de IPI)</b></td>
                        <td style="border-top: 1px solid #f0f0f0; padding: 10px; text-align: right;"><b>R$ '.number_format($valor_total, 2, ',', '.').'</b></td>
                    </tr>';*/
        $email.='      
    </tbody>
</table>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_10" id="Layout_10">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffcc00" align="center" cellspacing="0" style="background-color: rgb(255, 204, 0);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:5px; width:542;;max-width:542px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="542" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 5px; " src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_4" id="Layout_4">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="590" class="rnb-container rnb-yahoo-width" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#f9fafc" style="padding-right: 20px; padding-left: 20px; background-color: rgb(249, 250, 252);">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-size:14px; color:#888888; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
                                    <div>© 2018 wertco</div>
                                </td></tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        </td>
        </tr>
        </tbody></table>

</body></html>';	
		
		$this->load->library('email');
		$result = $this->email
		    ->from('vendas@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to($email_destino)
		    ->subject('Orçamento Wertco')
		    ->message($email)
		    ->attach('./pdf/'.$anexo)
		    ->send();

		return $result;		
	
	}

	/************************************************************ 
	************** GERA PDF DO ORÇAMENTO SOLICITADO *************
	*************************************************************/
	public function geraOrcamento($orcamento_id, $origem_solicitacao,$contato)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-'.$orcamento_id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $dados				=	$this->orcamentosM->getOrcamentoProdutos($orcamento_id);
	    $total_descontos 	= 	$this->orcamentosM->getOrcamentoDesconto($orcamento_id);
	    $responsavel 		= 	$this->orcamentosM->getResponsavel($orcamento_id);
	    $orcamento 			= 	$this->orcamentosM->getFreteEntregaPagto($orcamento_id);
	    $indicador			= 	$this->orcamentosM->getIndicador($orcamento_id);
	    $data['frete']		= 	(is_object($orcamento)) ? $orcamento->frete : ' ';
	    $data['entrega']	= 	(is_object($orcamento)) ? $orcamento->entrega : ' ';
	    $data['formaPagto']	= 	(is_object($orcamento)) ? $orcamento->forma_pagto : ' ';
	    $data['emissao']	= 	$this->orcamentosM->retornaDiferencaEmissao($orcamento_id);
	    $data['primeira_emissao']	= 	$this->orcamentosM->primeiraEmissao($orcamento_id);
	    
	    $data['empresa'] = array(	'razao_social' 			=> 	$dados[0]->razao_social,
    								'fantasia' 				=> 	$dados[0]->fantasia,
									'cnpj' 					=> 	$dados[0]->cnpj,
									'telefone'				=> 	$dados[0]->telefone,
									'email'					=> 	$dados[0]->email,
									'endereco'				=> 	$dados[0]->endereco,
									'cidade'				=> 	$dados[0]->cidade,		
									'pais'					=> 	$dados[0]->pais,
									'estado'				=> 	$dados[0]->estado,
									'orcamento_id'			=> 	$dados[0]->orcamento_id,
									'emissao'				=> 	$dados[0]->emissao,
									'valor_tributo'			=>	$dados[0]->valor_tributo,
									'representante_legal' 	=> 	$dados[0]->representante_legal,
									'inscricao_estadual' 	=> 	$dados[0]->insc_estadual,
									'bairro'				=> 	$dados[0]->bairro,
									'cep'					=> 	$dados[0]->cep,
									'cel_contato_posto'		=> 	$dados[0]->cel_contato_posto,
									'observacao'			=> 	$dados[0]->observacao,									
									'fl_especial' 			=> 	$dados[0]->fl_especial,
									'celular'				=> 	$dados[0]->celular,
									'tp_cadastro'			=> 	$dados[0]->tp_cadastro 	);

	    foreach ($dados as $dado) {
	    	$data['produtos'][] = array('codigo' 			=> 	$dado->codigo,
	    								'descricao'			=>	$dado->descricao,
										'modelo'			=>	$dado->modelo,
	    								'qtd'				=>	$dado->qtd,
	    								'valor_unitario' 	=>	$dado->valor_unitario,
	    								'valor_orcamento'	=> 	$dado->valor_orcamento,
	    								'valor_produto' 	=> 	$dado->valor);
	    }		
	    
	    $data['info']	= 	array(	'nome' 			=> 	$this->session->userdata('nome'),
	    						 	'email'			=> 	$this->session->userdata('email'),
	    						 	'telefone'		=>	$this->session->userdata('telefone_usuario'),	    						 	
	    						 	'celular'		=>	$this->session->userdata('celular_usuario'),
	    						 	'contato'		=> 	( isset($_POST['contato'])) ? $_POST['contato'] : $contato 	);

	    $data['representante'] 	= 	array(	'responsavel' 	=>  $responsavel->responsavel,
								  			'empresa' 		=>	$responsavel->razao_social	);	    
	    foreach( $total_descontos as $total_desconto ){
	    	$data['desconto'] = array('valor_desconto' => $total_desconto['valor_desconto'] );
	    }

	    if(!isset($data['desconto'])){ 

	    	$data['desconto'] = array('valor_desconto' => '' );	    		    	
	    }

	    $data['indicador'] = array(	'indicador'	=>	(count($indicador) > 0) ? $indicador[0]->indicador : '');	    

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-administrador/orcamentos/orcamento-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'orcamento-'.$orcamento_id.'.pdf'));
	    	}else{
	    		return $retorno['retorno'] = 'orcamento-'.$orcamento_id.'.pdf';
	    	}
	    }	    
	    
    }

    public function cadastraOrcamentos($pesquisa = null)
	{
		if( !$this->_is_logged() || $this->session->userdata('tipo_acesso') != 'administrador geral' ) {
			redirect(base_url('usuarios/login'));
		}

		$parametros 				= 	$this->session->userdata();
		$parametros['dados']		=	$this->orcamentosM->buscaOrcamentosIndicadores($parametros['usuario_id']);
		$parametros['bombas'] 		= 	$this->produtosM->select();		
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['title']		=	"Orçamentos";
		$parametros['pesquisa']		= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-administrador/orcamentos/cadastra-orcamento',$parametros);	
	}

	/* FUNÇÃO RESPONSÁVEL POR CADASTRAR ORÇAMENTOS PELOS ADMINISTRADORES COMERCIAIS DA WERTCO 	*/
	public function orcamentosAdm()
	{
		if( !$this->_is_logged() || $this->session->userdata('tipo_acesso') != 'administrador geral' ) {
			redirect(base_url('usuarios/login'));
		}

		if( $this->input->post() )
		{
			
			if( $this->input->post('salvar') == '1')
			{
				$dadosEmpresa = array(
			        'razao_social' 				=>	$this->input->post('razao_social'),
					'fantasia' 					=>	$this->input->post('fantasia'),
					'cnpj' 						=>	$this->input->post('cnpj'),
					'telefone' 					=>	$this->input->post('telefone'),
					'endereco' 					=>	$this->input->post('endereco'),
					'email' 					=>	$this->input->post('email'),				
					'cidade' 					=>	$this->input->post('cidade'),
					'estado'					=>	$this->input->post('estado'),
					'pais'						=>	$this->input->post('pais'),				
					'tipo_cadastro_id'			=>	1,
					'bairro'					=>	$this->input->post('bairro'),
					'cep'						=>	$this->input->post('cep'),
					'insc_estadual'				=> 	$this->input->post('inscricao_estadual'),
					'tp_cadastro'				=> 	$this->input->post('tp_cadastro'),
					'cartao_cnpj'				=> 	$this->input->post('cartao_cnpj'),					
					'codigo_ibge_cidade'		=> 	$this->input->post('codigo_ibge_cidade')
			    );

				if($this->empresasM->add($dadosEmpresa))
				{		
					$empresa_id = $this->db->insert_id();

				}
	 		}else{
	 			$empresa_id = $this->input->post('empresa_id');
	 		}

	 		$dadosOrcamento = array( 	'empresa_id' 			=> 	$empresa_id,
		 								'status_orcamento_id'	=> 	1,
		 								'origem'				=> 	'Administrador',
		 								'solicitante_id'		=> 	$this->session->userdata('usuario_id'),
		 								'usuario_id'			=> 	$this->session->userdata('usuario_id'),
		 								'frete_id'				=> 	$this->input->post('frete'),
		 								'entrega_id'			=> 	$this->input->post('entrega'),
		 								'forma_pagto_id'		=> 	$this->input->post('forma_pagto'),
		 								'indicador'				=> 	$this->input->post('indicador_id'),
		 								'indicador_id'			=> 	$this->input->post('indicador_id'),
		 								'contato_posto'			=> 	$this->input->post('contato_posto'),
	 									'cel_contato_posto'		=> 	$this->input->post('tel_contato_posto'),
	 									'observacao'			=> 	$this->input->post('observacao') 	);

	 		if( $this->orcamentosM->insereOrcamento($dadosOrcamento) )
	 		{
	 			$orcamento_id  = $this->db->insert_id();
	 			$this->log('Área Administrador | novo orçamento','orcamentos','INSERÇÃO', $this->session->userdata('usuario_id'), $dadosOrcamento,$dadosOrcamento,$_SERVER['REMOTE_ADDR']);
	 			$erro = 0;

	 			$dadosContato = array(	'empresa_id' 		=> 	$empresa_id,
	 									'nome'				=> 	$this->input->post('contato_posto'),
	 									'email'				=> 	$this->input->post('email_contato_posto'),
	 									'celular'			=> 	$this->input->post('tel_contato_posto'),
	 									'tipo_cadastro_id'	=> 1);

	 			if( $this->usuariosM->add($dadosContato) )
	 			{

	 				$contato_id = $this->db->insert_id();

	 				$updateContato = array(	'id' 			=> 	$orcamento_id,
	 										'contato_id'	=> 	$contato_id	);

	 				$this->orcamentosM->atualizaOrcamento($updateContato);

		 			$andamentoOrcamento = array(	'andamento' 			=> 'Orçamento realizado pelo '.$this->session->userdata('nome').' da WERTCO!',
			 										'dthr_andamento'		=> date('Y-m-d H:i:s'),
			 										'orcamento_id'			=> $orcamento_id,
			 										'status_orcamento_id'	=> 1,
			 										'usuario_id'			=> $this->session->userdata('usuario_id') 	);	

		 			if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento))
		 			{	 				
		 				$erro = $erro;
		 				$orcamentoResp = array(	'orcamento_id' 	=> 	$orcamento_id,
			 									'usuario_id' 	=> 	$this->session->userdata('usuario_id') );
	 					if($this->orcamentosM->insereResponsavelOrcamento($orcamentoResp))
	 					{
	 						$erro = $erro;
	 					
				 			for ($i=0; $i < count($this->input->post('produto')) ; $i++) 	 			  
				 			{ 
				 				$orcamentoProdutos = array(	'orcamento_id' 	=>	$orcamento_id,
				 											'produto_id'  	=> 	$this->input->post('produto')[$i],
				 											'qtd'			=>	$this->input->post('quantidade')[$i],
				 											'valor'			=> 	str_replace(',','.', str_replace('.','',$this->input->post('valor')[$i]))	);	

				 				if($this->orcamentosM->insereOrcamentoProdutos($orcamentoProdutos))
				 				{
				 					$erro = $erro;
				 					
								}else{
									$erro++;
								}		 					 			
				 			}
				 		}else{
							$erro++;
						}	 					

					}else{
						$erro++;
					}
				}else{
						$erro++;
				}
			}else{
				$erro++;
			}

	 		if( $erro == 0){
	 				
	 			if($this->enviarEmailOrcamento($orcamento_id))
 				{
 				
 					$this->session->set_flashdata('sucesso', 'ok.');
 				}else{
 					
 					$this->session->set_flashdata('erro', 'erro.');	
 				}

 			}else{
 				$this->session->set_flashdata('erro', 'erro.');
 			}
			
			$this->orcamentos();
		}					

	}

	public function insereDescontoOrcamento()
	{
		$dados_desconto = array('orcamento_id' 		=> 	$_POST['orcamento_id'],
								'valor_desconto'	=>	str_replace(',', '.', $_POST['valor_desconto']),
								'motivo_desconto'	=> 	$_POST['motivo_desconto'] );

		if( $this->orcamentosM->insereOrcamentosDesconto( $dados_desconto ) )
		{
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}		

	public function alterarValorProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['orcamento_produto_id'],
							 	'valor'	=>	str_replace(',','.', str_replace('.','',$_POST['valor_produto']))	);
		
		if($this->orcamentosM->atualizaValorProduto($dadosUpdate)){
			$this->log('Área Administrador | atualiza valor orçamento','orcamento_produtos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Valor de um produto foi alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{					

				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}
		}else{
			echo json_encode(array('retorno' => 'erro' ));		
		}

	}

	public function alterarProdutoOrcamento(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['orcamento_produto_id'],
							 	'produto_id'	=>	$_POST['produto_id']	);

		if($this->orcamentosM->alterarProdutoOrcamento($dadosUpdate)){
			$this->log('Área Administrador | atualiza produto','orcamento_produtos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Produto #'.$_POST['produto_nr'].' alterado para ' .$_POST['produto'],
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function insereContatoOrcamento(){
		$update = array(	'id' 			=> 	$_POST['orcamento_id'],
							'contato_id' 	=> 	$_POST['contato_id'] 	);	
		
		if($this->orcamentosM->atualizaOrcamento($update)){
						$this->log('Área Administrador | atualiza contato orcamento','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso' ));						
		}else{
			echo json_encode(array('retorno' => 'erro' ));
		}					

	}

	public function alterarQtdProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['orcamento_produto_id'],
							 	'qtd'	=>	$_POST['qtd']	);

		if($this->orcamentosM->alterarQtdProduto($dadosUpdate)){

			$this->log('Área Administrador | atualiza qtd produto','orcamento_produtos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Quantidade de um produto foi alterada',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 					 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarObservacao(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['id'],
							 	'observacao'	=>	$_POST['observacao']	);

		if($this->orcamentosM->alterarObservacao($dadosUpdate)){
			$this->log('Área Administrador | atualiza observacao','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);			
			echo json_encode(array('retorno' => 'sucesso' ));						 	
			
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}


	public function alterarFormaPagto(){

		$dadosUpdate = array(	'id' 				=> 	$_POST['orcamento_id'],
							 	'forma_pagto_id'	=>	$_POST['valor']	);

		if(	$this->orcamentosM->alterarFormaPagto($dadosUpdate)	)	{
			$this->log('Área Administrador | atualiza forma pagto','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de Pagamento alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarEntrega(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['orcamento_id'],
							 	'entrega_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarEntrega($dadosUpdate)){
			$this->log('Área Administrador | atualiza entrega','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de entrega alterada.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarFrete(){
		$dadosUpdate = array(	'id' 		=> 	$_POST['orcamento_id'],
							 	'frete_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarFrete($dadosUpdate)){
			$this->log('Área Administrador | atualiza frete','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Frete alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	private function enviarEmailOrcamento($orcamento_id){

		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);		 
		$email = '<html>
				<head></head>
				<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
				<div class="content" style="width: 600px; height: 100%;">
				<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/backgroundCabecalho.png); height: 528px; "></div>
				<div style="width: 600px; background-color: #ffcc00; height: 100%">
					<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
						<tbody>
							<tr>
								<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
								<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
								<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
								<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
							</tr>
							<tr>								
								<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
								<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
								<td style="padding-bottom: 18px;font-weight: 700;"></td>
								<td style="padding-bottom: 18px;"></td>
							</tr>
							<tr>
								<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
								<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
								<td style=" font-weight: 700; text-align: right;">Telefone:</td>
								<td>'.$empresa['telefone'].'</td>
							</tr>
							<tr>
								<td style=" font-weight: 700;">E-mail:</td>
								<td colspan="3">'.$empresa['email'].'</td>
							</tr>
						</tbody>
					</table>
					<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
					<table style="width: 570px;margin-left: 14px;" cellspacing="0">
						<thead style="height: 55px;">
							<tr><th style="    height: 30px;">Modelo</th>
							<th>Descrição</th>
							<th>Quantidade</th>			
						</tr></thead>
						<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
				$total = 0; foreach($produtos as $produto){
					
					$email.='	<tr>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
								<td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>							
							</tr>';					
					$total = $total + ($produto['valor'] * $produto['qtd']);
				}
				
				$email.='		
						</tbody>
					</table>
					<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
				</div>
			</div>
		</body>
		</html>';	


		$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to('vendas@wertco.com.br')
		    ->subject('Orçamento realizado no Sistema pelo administrador:'.$this->session->userdata('usuario_id'))
		    ->message($email)
		    ->send();

		return $result;		
	}

	public function insereProdutosNovosOrcamento(){
		$erro = 0;
		foreach ($_POST['dados'] as $dados) {
			if( $dados['name'] == 'produto_id'){
				$insert['produto_id'] = $dados['value'];
				$i=1;
			}
			if( $dados['name'] == 'qtd'){
				$insert['qtd'] = $dados['value'];
				$i=2;
			}
			if( $dados['name'] == 'valor_unitario'){
				$insert['valor'] = str_replace(",", ".", str_replace(".","", $dados['value']));
				$i=3;
				$insert['orcamento_id'] = $_POST['orcamento_id'];
				if($this->orcamentosM->insereOrcamentoProdutos($insert)){
					$this->log('Área Administrador | insere produto novo','orcamento_produtos','INSERÇÃO', $this->session->userdata('usuario_id'),$insert,$insert,$_SERVER['REMOTE_ADDR']);
					$erro = $erro;
				}else{
					$erro++;
				}
			}			
		}

		if($erro == 0){
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Produto Adicionado ao orçamento por '.$this->session->userdata('nome'),
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			$this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Icms ******************************
	*****************************************************************************/
	public function excluirProdutoOrcamento()
	{			
		if($this->orcamentosM->excluirProduto($_POST['orcamento_produto_id'])){
			$this->log('Área Administrador | excluir produto','orcamento_produtos','INSERÇÃO', $this->session->userdata('usuario_id'),array('id' => $_POST['orcamento_produto_id']),array('id' => $_POST['orcamento_produto_id']),$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

    public function gestaoEmpresas($pesquisa = null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->empresasM->getEmpresas();
		$parametros['title']	=	"Gestão de Empresas";
		$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-administrador/empresas/gestao-empresas',$parametros);
	}

	public function visualizarEmpresa($id,$pesquisa=null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->empresasM->getEmpresasUsuarios($id);			
		$parametros['subtipos']	=	$this->empresasM->getSubtipoByTipo($parametros['dados'][0]->tipo_cadastro_id);		
		$parametros['title']	=	"Empresa #".$id;
		$parametros['pesquisa'] = 	$pesquisa;
		$parametros['chamados'] = 	$this->chamadoM->selectPorEmpresa($id);	
		$parametros['pedidos'] 	= 	$this->pedidosM->selectPedidosPorEmpresa($id);
		$parametros['anexos'] 	= 	$this->empresasM->selectAnexos($id);
		$this->_load_view('area-administrador/empresas/visualiza-empresa',$parametros);
	}

	public function anexarDocumentosEmpresa(){
		
		if($_FILES['anexo']['name'] != 'NULL' && $_FILES['anexo']['name'] != '' ){

			$tipo 			= 	explode('.', $_FILES['anexo']['name']);
			$arquivo 		=	md5($_FILES['anexo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
			
			$configuracao = array(
		        'upload_path'   	=> 	'./empresa_anexos/',		        
		        'allowed_types' 	=> 	'gif|jpg|jpeg|png|pdf|doc|docx|xls|csv|txt|xlsx|csv|vnd.ms-outlook|msg|zip|rar|log|html|htm|mp4|mp3|ogg',
		        'file_name'     	=> 	$arquivo
		    );      
			
		    $this->load->library('upload', $configuracao);
		    
		    if ($this->upload->do_upload('anexo')){

		    	$inserir = array( 	'empresa_id'	=> 	$this->input->post('empresa_id'),
		    						'descricao'		=>	$this->input->post('descricao'),
		    						'anexo'			=> 	$arquivo,
		    						'usuario_id'	=> 	$this->session->userdata('usuario_id')	);
		    
		    	if( $this->empresasM->insereAnexo($inserir) ){		    		
		    		$this->session->set_flashdata('sucesso', 'ok'); 
		    		
		    	}else{
		    		$this->session->set_flashdata('erro', 'erro');
		    	}
		    }
		}

	    redirect('areaAdministrador/visualizarEmpresa/'.$this->input->post('empresa_id'));

	}

	public function excluirAnexoEmpresa(){

		if( $this->empresasM->excluirAnexo( $this->input->post('id') ) ){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}

	public function cadastrarEmpresa()
	{
		
		if($this->input->post()){

			$dadosEmpresa = array(
		        'razao_social' 		=>	$this->input->post('razao_social'),
				'fantasia' 			=>	$this->input->post('fantasia'),
				'cnpj' 				=>	$this->input->post('cnpj'),
				'telefone' 			=>	$this->input->post('telefone'),
				'endereco' 			=>	$this->input->post('endereco'),
				'email' 			=>	$this->input->post('email'),
				'cidade' 			=>	$this->input->post('cidade'),
				'estado' 			=>	$this->input->post('estado'),
				'tipo_cadastro_id'	=> 	$this->input->post('tipo_cadastro_id'),
				'insc_estadual'		=> 	$this->input->post('inscricao_estadual'),
				'pais'				=> 	$this->input->post('pais'),
				'bairro'			=> 	$this->input->post('bairro'),
				'cep'				=> 	$this->input->post('cep'),
				'cartao_cnpj'		=>	$this->input->post('cartao_cnpj')
		    );
			
			$conteudo= 	"CNPJ: ". 	$this->input->post('cnpj')." <br/> ";
			$conteudo.= "Razão Social: ". 	$this->input->post('razao_social')." <br/> ";
			$conteudo.=	'Fantasia: '.	$this->input->post('fantasia').' <br/> ';
			$conteudo.=	'CNPJ: '.	$this->input->post('cnpj').' <br/> ';
			$conteudo.=	'Telefone: '.	$this->input->post('telefone').' <br/> ';
			$conteudo.=	'Endereço: '.	$this->input->post('endereco').' <br/> ';
			$conteudo.=	'Cidade:  '	.	$this->input->post('cidade').' <br/> ';
			$conteudo.=	'Estado:  '	.	$this->input->post('estado').' <br/> ';
			$conteudo.=	'País: 	  '.	$this->input->post('pais').' <br/> ';	
			$conteudo.=	'Usuário que cadastrou:	  '.	$this->session->userdata('nome');

			if($this->empresasM->add($dadosEmpresa))
			{
				$this->log('Área Administrador | cadastro empresa','empresas','INSERÇÃO', $this->session->userdata('usuario_id'),$dadosEmpresa,$dadosEmpresa,$_SERVER['REMOTE_ADDR']);
				$this->enviaEmail('webmaster@companytec.com.br','Empresa Cadastrada',$conteudo,$this->input->post('cartao_cnpj'));
				$this->session->set_flashdata('sucesso', 'ok');	
				$this->gestaoEmpresas();			
			}else{
				$this->session->set_flashdata('erro', 'erro');
				$this->gestaoEmpresas();
			}					 
			
		}else{

			$parametros 			= 	$this->session->userdata();		
			$parametros['estados']	=	$this->icmsM->getEstados();
			$parametros['title']	=	"Cadastro de Empresas";
			$this->_load_view('area-administrador/empresas/cadastra-empresa',$parametros);
		}
	}

	public function editarEmpresa($id = null, $pesquisa = null){

		if($this->input->post('salvar') == 1){

			$update = $this->input->post();
			
			unset($update['salvar']);
			unset($update['tipo_acesso_id']);
			$update['tipo_cadastro_id'] = $this->input->post('tipo_acesso_id');

			if($this->input->post('venc_inmetro') != null){
				$data 			= 	explode('/',$this->input->post('venc_inmetro'));
				$dt_vencimento	= 	'20'.$data[2].'-'.$data[1].'-'.$data[0];
				$update['venc_inmetro']	=	$dt_vencimento;
			}

			if($this->empresasM->atualizaEmpresas($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				$this->log('Área Administrador | editar empresa','empresas','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				
				$parametros 			= 	$this->session->userdata();
				$parametros['title']	=	"Gestão de Empresas";
				$parametros['dados']	=	$this->empresasM->getEmpresas();	
				$parametros['pesquisa']	= 	$pesquisa;			 
				$this->_load_view('area-administrador/empresas/gestao-empresas',	$parametros );
			}

		}else{
			$row 								= 	$this->empresasM->getEmpresa($id);		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Empresas";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();			
			$parametros['pesquisa']				= 	$pesquisa;			 
			$this->_load_view('area-administrador/empresas/editar-empresa',	$parametros );	
		}
	}

	public function icms() 
	{		
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->icmsM->getIcms();		
		$parametros['title']	=	"ICMS";
		$this->_load_view('area-administrador/icms/icms',$parametros);
	}

	/****************************************************************************
	************* Método Responsável por cadastrar produtos da wertco ***********
	*****************************************************************************/
	public function cadastraIcms()
	{
		
		if($this->input->post('salvar') == 1){

			$insert = array('estado_id'		=>	$this->input->post('estado_id'),
							'valor_tributo'	=>	str_replace(',','.',$this->input->post('valor_tributo')));
						 
			if($this->icmsM->insereIcms($insert)){
				$this->log('Área Administrador | insere icms','icms','INSERÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->icmsM->getIcms();		
				$parametros['title']	=	"ICMS";
				$this->_load_view('area-administrador/icms/icms',$parametros);
			}else{
				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->icmsM->getIcms();		
				$parametros['title']	=	"ICMS";
				$this->_load_view('area-administrador/icms/icms',$parametros);
			}

		}else{

			$parametros 			= 	$this->session->userdata();		
			$parametros['estados']	=	$this->icmsM->getEstados();
			$parametros['title']	=	"Gestão de ICMS";
			$this->_load_view('area-administrador/icms/cadastra-icms',$parametros);
		}
	}
	/****************************************************************************
	**************** Método Ajax - Excluir Icms ******************************
	*****************************************************************************/
	public function excluirIcms()
	{
		
		if($this->icmsM->excluirIcms($_POST['id'])){
			$this->log('Área Administrador | excluir icms','icms','EXCLUSÃO', $this->session->userdata('usuario_id'),array('id' => $_POST['id'] ),array('id' => $_POST['id'] ),$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	/****************************************************************************
	************* Método Responsável por editar os Icms da wertco ***********
	*****************************************************************************/
	public function editarIcms($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'estado_id'			=>	$this->input->post('estado_id'),
							'valor_tributo'		=>	$this->input->post('valor_tributo'));
						 
			if($this->icmsM->atualizaIcms($update)){
				$this->log('Área Administrador | excluir icms','icms','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->icmsM->getIcms();		
				$parametros['title']	=	"ICMS";
				$this->_load_view('area-administrador/icms/icms',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->icmsM->getIcms();		
				$parametros['title']	=	"ICMS";
				$this->_load_view('area-administrador/icms/icms',$parametros);
			}

		}else{

			$row 					= 	$this->icmsM->getIcmsById($id);
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$row;
			$parametros['title']	=	"Editar ICMS";
			$parametros['estados']	=	$this->icmsM->getEstados();
			$this->_load_view('area-administrador/icms/editar-icms',$parametros );
		}
	}

	public function verificaEstadoIcms()
	{
		$dados = $this->icmsM->getIcmsPorEstado($_POST['estado_id']);
		if( $dados[0]['total'] > 0 ){
			echo json_encode(array('retorno' => 'erro'));
		}else{
			echo json_encode(array('retorno' => 'sucesso'));
		}

	}

	public function downloads()
	{		
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->downloadsM->getDownloads();
		$parametros['title']	=	"Gestão de Downloads";
		$this->_load_view('area-administrador/downloads/gestao-downloads',$parametros);
	}	

	public function cadastraDownloads()
	{
		
		if($this->input->post('salvar') == 1){

			$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
			$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
			
			$configuracao = array(
		        'upload_path'   	=> 	'./downloads/',		        
		        'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv|zip|rar|jpeg',
		        'file_name'     	=> 	$arquivo
		    );      
		    $this->load->library('upload', $configuracao);
		    
		    if ($this->upload->do_upload('nome_arquivo')){

		    	$dadosInsert 	= 	array(	'nome_arquivo' 		=>	$_FILES['nome_arquivo']['name'],
											'arquivo' 			=> 	$arquivo,
											'descricao'			=> 	$this->input->post('descricao')	 );

				if( $this->downloadsM->insereDownloads($dadosInsert) ){
					$download_id	=	$this->db->insert_id();
					
					foreach( $this->input->post('tipo_cadastro') as $cadastro_tipo )
					{
						$dadosTipo  = array('tipo_cadastro_id' 	=> 	$cadastro_tipo,
											'download_id'		=> 	$download_id  );

						$this->downloadsM->insereDownloadCadastros($dadosTipo);
					}
					$this->log('Área Administrador | Insere download','downloads','INSERÇÃO', $this->session->userdata('usuario_id'),$dadosInsert,$dadosInsert,$_SERVER['REMOTE_ADDR']);						
					$this->session->set_flashdata('sucesso', 'ok');

				}else{

					$this->session->set_flashdata('erro', 'erro');					
				}

			}else{
				
		        $this->session->set_flashdata('erro', $this->upload->display_errors()); 
			}

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Downloads";
			$parametros['dados']	=	$this->downloadsM->getDownloads();
			$this->_load_view('area-administrador/downloads/gestao-downloads',$parametros);

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Downloads";
			$this->_load_view('area-administrador/downloads/cadastra-download',$parametros);
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Dowload ******************************
	*****************************************************************************/
	public function excluirDownload()
	{		
		
		if($this->downloadsM->excluirDownloads($_POST['id'])){

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function editarDownload($id = null){

		if($this->input->post('salvar') == 1){
			$erro = 0;	

			if( $_FILES['nome_arquivo']['name'] != "" ){
				$tipo 		= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 	=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$configuracao = array(
			        'upload_path'   	=> 	'./downloads/',		        
			        'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv',
			        'file_name'     	=> 	$arquivo
			    );      

			    $this->load->library('upload', $configuracao);
			    
			    if ($this->upload->do_upload('nome_arquivo')){
			    	$erro = 0;
			    	$update['nome_arquivo'] = 	$_FILES['nome_arquivo']['name'];
			    	$update['arquivo'] 		= 	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];

			    }else{
			    	$erro++;
			    }
			}
			
			if($erro == 0){

				$download_id 			= $this->input->post('download_id');
				$update['id'] 			= $download_id;
				$update['descricao'] 	= $this->input->post('descricao');				

				if( $this->downloadsM->atualizaDownloads($update) ){
					$this->log('Área Administrador | edita download','downloads','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);

					if($this->downloadsM->excluirTipoCadastro($download_id)){
						$erro = $erro;
						
						foreach ($this->input->post('tipo_cadastro') as $tipo_cadastro) {
							
							
							$dadosCadastro = array( 'download_id' 		=> 	$download_id,
													'tipo_cadastro_id' 	=> 	$tipo_cadastro );
							
							if($this->downloadsM->insereDownloadCadastros($dadosCadastro)){
								$erro = $erro;	
							}else{
								$erro++;
							}
							
						}
					}else{
						$erro++;
						
					}
					
					if( $erro > 0 ){
						$this->session->set_flashdata('erro', 'erro');		
					}else{
						$this->session->set_flashdata('sucesso', 'ok');		
					}

					$parametros 			= 	$this->session->userdata();
					$parametros['dados']	=	$this->downloadsM->getDownloads();		
					$parametros['title']	=	"Downloads";
					$this->_load_view('area-administrador/downloads/gestao-downloads',$parametros);
				}else{
					$this->session->set_flashdata('erro', 'erro');	
					$parametros 			= 	$this->session->userdata();
					$parametros['dados']	=	$this->downloadsM->getDownloads();		
					$parametros['title']	=	"Downloads";
					$this->_load_view('area-administrador/downloads/gestao-downloads',$parametros);
				}

				

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->downloadsM->getDownloads();		
				$parametros['title']	=	"Downloads";
				$this->_load_view('area-administrador/downloads/gestao-downloads',$parametros);
			}

		}else{

			$row 					= 	$this->downloadsM->getDownloads($id);			
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$row;
			$parametros['title']	=	"Editar Downloads";			
			$this->_load_view('area-administrador/downloads/editar-download',$parametros );
		}
	}

	public function cartaoTecnicos()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->cartaoM->selectCartoes();		
		$parametros['title']	=	"Gestão dos Cartões Técnicos";
		$this->_load_view('area-administrador/cartao-tecnicos/cartao-tecnicos',$parametros);
	}

	public function cadastraCartao()
	{
		
		if($this->input->post('salvar') == 1){
			$data 			= 	explode('/',$this->input->post('dt_vencimento'));
			$dt_vencimento	= 	'20'.$data[2].'-'.$data[1].'-'.$data[0];

			$dados  = array('usuario_id' 		=> 	$this->input->post('usuario_id'),
							'tag' 				=> 	$this->input->post('tag'),
							'dt_vencimento' 	=> 	$dt_vencimento,
							'ativo' 			=> 	$this->input->post('ativo'),
							'periodo_renovacao'	=> 	$this->input->post('periodo_renovacao'),
							'ultima_atualizacao'=> 	date('Y-m-d'));

			if( $this->cartaoM->add($dados) ){
				$this->log('Área Administrador | cadastra cartão técinico','cartao_tecnicos','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);	
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->cartaoM->selectCartoes();		
			$parametros['title']	=	"Gestão dos Cartões Técnicos";
			$this->_load_view('area-administrador/cartao-tecnicos/cartao-tecnicos',$parametros);

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Cartão de Técnicos";
			$this->_load_view('area-administrador/cartao-tecnicos/cadastra-cartao',$parametros);
		}
	
	}

	 /****************************************************************************
    ************* Método Responsável por editar os Cartões da wertco ************
    *****************************************************************************/
    public function editarCartao($id = null){

        if($this->input->post('salvar') == 1){

            $data             =     explode('/',$this->input->post('dt_vencimento'));

            if( strlen($data[2]) < 4 ){
                $ano = '20'.$data[2];

            }else{
                $ano = $data[2];

            }

            $dt_vencimento    = $ano.'-'.$data[1].'-'.$data[0];    

            $update = array('id'            =>    $this->input->post('cartao_id'),
                            'usuario_id'    =>    $this->input->post('usuario_id'),
                            'tag'            =>    $this->input->post('tag'),
                            'dt_vencimento'    =>    $dt_vencimento,
                            'ativo'            =>    $this->input->post('ativo'));
                         
            if($this->cartaoM->atualizaCartao($update)){
            	$this->log('Área Administrador | ATUALIZA cartão técinico','cartao_tecnicos','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);	
                $this->session->set_flashdata('sucesso', 'ok');
                
                $parametros             =     $this->session->userdata();
                $parametros['dados']    =    $this->cartaoM->selectCartoes();        
                $parametros['title']    =    "Gestão dos Cartões Técnicos";
                $this->_load_view('area-administrador/cartao-tecnicos/cartao-tecnicos',$parametros);

            }else{

                $this->session->set_flashdata('erro', 'erro');

                $parametros             =     $this->session->userdata();
                $parametros['dados']    =    $this->cartaoM->selectCartoes();        
                $parametros['title']    =    "Gestão dos Cartões Técnicos";
                $this->_load_view('area-administrador/cartao-tecnicos/cartao-tecnicos',$parametros);
            }

        }else{

            $row                     =     $this->cartaoM->getCartao($id);            
            $parametros             =     $this->session->userdata();
            $parametros['dados']    =    $row;
            $parametros['title']    =    "Editar Cartão Técnicos";            
            $this->_load_view('area-administrador/cartao-tecnicos/cartao-editar',$parametros );
        }
    }

	
	public function retornaTecnicos()
	{
		
		$rows = $this->usuariosM->retornaTecnicosPedidos($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'		=>	$row['label'],
									'id'		=>	$row['id'],
									'value'		=> 	$row['label'],
									'email'		=> 	$row['email']	);
				
			}
			
			echo json_encode($dados);
		}
	}

	public function retornaTecnicosGeral()
	{
		
		$rows = $this->usuariosM->retornaTecnicosGeral($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'		=>	$row['label'],
									'id'		=>	$row['id'],
									'value'		=> 	$row['label']	);
				
			}
			
			echo json_encode($dados);
		}
	}
	
	/****************************************************************************
	**************** Método Ajax - Alterar status do Cartao *********************
	*****************************************************************************/
	public function alteraStatusCartao()
	{

		$dados = array('id'		=>	$_POST['id'],
						'ativo'	=>	$_POST['ativo']);

		if($this->cartaoM->atualizaStatus($dados)){
			$this->log('Área Administrador | altera status cartão','cartao_tecnicos','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);	
			echo json_encode(array('retorno' => 'sucesso'));

		}else{
			echo json_encode(array('retorno' => 'erro'));
	
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Cartao ******************************
	*****************************************************************************/
	public function excluirCartao()
	{		
		
		if($this->cartaoM->excluirCartao($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	

	/****************************************************************************
	**************** Método Ajax - Verificar Tag *********************
	*****************************************************************************/
	public function verificaTag()
	{	

		$dados 		= 	array('tag'	=>	$_POST['tag']);
		$retorno 	= 	$this->cartaoM->verificaTag($dados);
		echo json_encode($retorno);

	}

	public function fretes()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->fretesM->selectFretes();
		$parametros['title']	=	"Fretes";
		$this->_load_view('area-administrador/fretes/fretes',$parametros);
	}

	public function cadastraFretes()
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados  = array('descricao' 		=> 	$this->input->post('descricao'));

			if( $this->fretesM->add($dados) ){
				$this->log('Área Administrador | insere frete','fretes','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->fretesM->selectFretes();
			$parametros['title']	=	"Fretes";
			$this->_load_view('area-administrador/fretes/fretes',$parametros);

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Cartão de Técnicos";
			$this->_load_view('area-administrador/fretes/cadastra-fretes',$parametros);
		}
	
	}
	
	/****************************************************************************
	************* Método Responsável por editar os Fretes da WERTCO *************
	*****************************************************************************/

	public function editarFretes($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'descricao'			=>	$this->input->post('descricao'));
						 
			if($this->fretesM->atualizaFrete($update)){
				$this->log('Área Administrador | atualizar frete','fretes','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->fretesM->selectFretes();
				$parametros['title']	=	"Fretes";
				$this->_load_view('area-administrador/fretes/fretes',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->fretesM->selectFretes();
				$parametros['title']	=	"Fretes";
				$this->_load_view('area-administrador/fretes/fretes',$parametros);
			}

		}else{

			$row 					= 	$this->fretesM->getFretesById($id);
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$row;
			$parametros['title']	=	"Editar Fretes";			
			$this->_load_view('area-administrador/fretes/editar-fretes',$parametros );
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Fretes ******************************
	*****************************************************************************/
	public function excluirFrete()
	{
																															
		if($this->fretesM->excluirFrete($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));

		}else{
			echo json_encode(array('retorno' => 'erro'));

		}

	}

	
	public function entregas()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->entregasM->selectEntregas();
		$parametros['title']	=	"Fretes";
		$this->_load_view('area-administrador/entregas/entregas',$parametros);
	}

	public function cadastraEntregas()
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados  = array('descricao' 		=> 	$this->input->post('descricao'));

			if( $this->entregasM->add($dados) ){
				$this->log('Área Administrador | cadastra entregas','entregas','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->entregasM->selectEntregas();
			$parametros['title']	=	"Fretes";
			$this->_load_view('area-administrador/entregas/entregas',$parametros);

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Tipo de Entrega da mercadoria";
			$this->_load_view('area-administrador/entregas/cadastra-entregas',$parametros);
		}
	
	}
	
	/****************************************************************************
	************* Método Responsável por editar os tipos de entregas feitas *****
	******************************* pela da WERTCO ******************************
	*****************************************************************************/

	public function editarEntregas($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'descricao'			=>	$this->input->post('descricao'));
						 
			if($this->entregasM->atualizaEntregas($update)){
				$this->log('Área Administrador | editar entregas','entregas','INSERÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);

				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->entregasM->selectEntregas();
				$parametros['title']	=	"Entregas";
				$this->_load_view('area-administrador/entregas/entregas',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->entregasM->selectEntregas();
				$parametros['title']	=	"Fretes";
				$this->_load_view('area-administrador/entregas/entregas',$parametros);
			}

		}else{

			$row 					= 	$this->entregasM->getEntregasById($id);
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$row;
			$parametros['title']	=	"Editar entregas";			
			$this->_load_view('area-administrador/entregas/editar-entregas',$parametros );
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Fretes ******************************
	*****************************************************************************/
	public function excluirEntrega()
	{
																															
		if($this->entregasM->excluirEntrega($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));

		}else{
			echo json_encode(array('retorno' => 'erro'));

		}

	}

	public function formaPagto(){
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->formaPagtoM->selectFormaPagto();
		$parametros['title']	=	"Formas de pagamento";
		$this->_load_view('area-administrador/forma-pagto/forma-pagto',$parametros);
	}

	public function cadastraFormaPagto()
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados  = array(	'descricao' 		=> 	$this->input->post('descricao'),
								'fl_visualiza_rep' 	=> 	$this->input->post('fl_visualiza_rep')	);

			if( $this->formaPagtoM->add($dados) ){
				$this->log('Área Administrador | cadastra formaPagtoM','forma_pagto','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->formaPagtoM->selectFormaPagto();
			$parametros['title']	=	"Formas de pagamento";
			$this->_load_view('area-administrador/forma-pagto/forma-pagto',$parametros);

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro Forma de Pagamento";
			$this->_load_view('area-administrador/forma-pagto/cadastra-forma-pagto',$parametros);
		}
	
	}
	
	/****************************************************************************
	************* Método Responsável por editar os tipos de FormaPagto aceitas **
	******************************* pela da WERTCO ******************************
	*****************************************************************************/

	public function editarFormaPagto($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'descricao'			=>	$this->input->post('descricao'),
							'fl_visualiza_rep'	=> 	$this->input->post('fl_visualiza_rep')	);
						 
			if($this->formaPagtoM->atualizaFormaPagto($update)){
				$this->log('Área Administrador | atualiza formaPagtoM','forma_pagto','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->formaPagtoM->selectFormaPagto();
				$parametros['title']	=	"Formas de pagamento";
				$this->_load_view('area-administrador/forma-pagto/forma-pagto',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->formaPagtoM->selectFormaPagto();
				$parametros['title']	=	"Formas de pagamento";
				$this->_load_view('area-administrador/forma-pagto/forma-pagto',$parametros);
			}

		}else{

			$row 					= 	$this->formaPagtoM->getFormaPagtoById($id);
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$row;
			$parametros['title']	=	"Editar forma de pagto";			
			$this->_load_view('area-administrador/forma-pagto/editar-forma-pagto',$parametros );
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Fretes ******************************
	*****************************************************************************/
	public function excluirFormaPagto()
	{
																															
		if($this->formaPagtoM->excluirFormaPagto($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));

		}else{
			echo json_encode(array('retorno' => 'erro'));
			
		}

	}

	public function treinamentos()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->treinamentoM->getTreinamento();
		$parametros['title']	=	"Treinamentos técnicos";
		$this->_load_view('area-administrador/treinamentos/treinamentos',$parametros);
	}

	public function listaTreinamento()
	{
		$parametros 					= 	$this->session->userdata();
		$parametros['dados']			=	$this->treinamentoM->buscaTreinamentos($this->input->post('treinamento_id'));		
		$parametros['total_aprovados']	=	$this->treinamentoM->getAprovadosInscritos($this->input->post('treinamento_id'));		
		$parametros['treinamento_id'] 	= 	$this->input->post('treinamento_id');
		$parametros['title']			=	"Treinamento técnico";
		$this->_load_view('area-administrador/treinamentos/listar-treinamento',$parametros);
	}

	public function visualizarTreinamento($id)
	{
		$parametros 					= 	$this->session->userdata();
		$parametros['dados']			=	$this->treinamentoM->buscaTreinamentosById($id);
		$parametros['title']			=	"Treinamento técnico";
		$this->_load_view('area-administrador/treinamentos/visualizar-treinamento',$parametros);  
	}

	public function cadastraTreinamento()
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados  = array('descricao' 	=> 	$this->input->post('descricao'),
							'total_vagas'	=> 	$this->input->post('total_vagas'));

			if( $this->treinamentoM->addTreinamento($dados) ){
				$this->log('Área Administrador | insere treinamento','treinamento','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

			}else{

				$this->session->set_flashdata('erro', 'erro');					
			}    

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->treinamentoM->getTreinamento();
			$parametros['title']	=	"Treinamentos";
			$this->_load_view('area-administrador/treinamentos/treinamentos',$parametros);

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Treinamento Técnico";
			$this->_load_view('area-administrador/treinamentos/cadastra-treinamento',$parametros);
		}
	
	}

	public function editarTreinamento($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'			=>	$this->input->post('id'),
							'descricao'		=>	$this->input->post('descricao'),
							'total_vagas'	=> 	$this->input->post('total_vagas'));
						 
			if($this->treinamentoM->atualizaTreinamento($update)){
				$this->log('Área Administrador | atualizar treinamento','treinamento','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);

				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->treinamentoM->getTreinamento();
				$parametros['title']	=	"Treinamentos";
				$this->_load_view('area-administrador/treinamentos/treinamentos',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->treinamentoM->getTreinamento();
				$parametros['title']	=	"Treinamentos";
				$this->_load_view('area-administrador/treinamentos/treinamentos',$parametros);
			}

		}else{
			
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->treinamentoM->getTreinamento($id);
			$parametros['title']	=	"Editar Treinamentos";			
			$this->_load_view('area-administrador/treinamentos/editar-treinamento',$parametros );
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir treinamnetos ******************************
	*****************************************************************************/
	public function excluirTreinamentos()
	{
																															
		if($this->treinamentoM->excluirTreinamento($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));

		}else{
			echo json_encode(array('retorno' => 'erro'));

		}

	}

	/****************************************************************************
	********** Método Ajax - Alterar status do cadastro treinamento *************
	*****************************************************************************/
	public function alteraStatusTreinamento()
	{	

		$dados = array('id'		=>	$_POST['id'],
						'ativo'	=>	$_POST['ativo']);

		if($this->treinamentoM->atualizaStatus($dados)){			
			$this->log('Área Administrador | ativa/inativa cadastro treinamento','treinamentos','EDIÇÂO', $this->session->userdata('usuario_id'), $dados,$dados,$_SERVER['REMOTE_ADDR']);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{

			echo json_encode(array('retorno' => 'erro'));
		}

	}

	public function geraExcelTreinamento(){

		$spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');
        
        $writer = new Xlsx($spreadsheet);
 
        $filename = base_url('depositos/inscritosTreinamento2018.xlsx');
 
        $writer->save($filename);
		
	}

	public function solicitacoesCartaoTecnico()
	{
		$parametros				= 	$this->session->userdata();
		$parametros['dados']	=	$this->cartaoM->getSolicitacoes();
		$parametros['title']	=	"Solicitações de Cartões Técnicos";
		$this->_load_view('area-administrador/cartao-tecnicos/solicitacao',$parametros);
	} 

	public function confirmarEnvio(){
		$dados = array('id'					=>	$_POST['id'],
					 	'usuario_aceite_id'	=>	$_POST['usuario_id'],
					 	'situacao'			=> 	'Cartão Enviado ao Técnico',
					 	'dthr_aceite'		=> 	date('Y-m-d H:i:s')	);

		if($this->cartaoM->confirmarEnvio($dados)){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}

	public function confirmarExclusaoSolicitacaoCartao(){		

		if($this->cartaoM->confirmarExclusaoSolicitacao($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}

	public function pedidos()
	{
	
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->pedidosM->selectPedidos();		
		$parametros['title']	=	"Pedidos";
		$this->_load_view('area-administrador/pedidos/pedidos',$parametros);
	
	}

	public function buscaProximoStatusPedido()
	{
		$status =	$this->pedidosM->buscaProximoStatus($this->input->post('ordem')+1);
		$combo  = 	"";
		//Verifica se tem Status relacionados da qualidade
		if( $this->input->post('status') == 19 ){

			$combo.= "<option value='12'>LIBERADO PARA LACRAÇÃO DO IPEM</option>";
		}elseif($this->input->post('status') == 20){

			$combo.= "<option value='14'>PRODUÇÃO CONCLUÍDA E LIBERADO PARA EMBALAGEM</option>";
		}else{
			foreach($status as $sts){
				$combo.= "<option value='".$sts['id']."'>".$sts['descricao']."</option>";
			}
		}
		$combo.= "<option value='anterior'> <======= VOLTAR PARA O STATUS ANTERIOR</option>";
		echo json_encode(array('retorno' => $combo));
	}
	
	public function retornaStatusAnterior()
	{	
		$where = array(	'pedido_id' => 	$this->input->post('pedido_id'),
						'ordem'		=> 	$this->input->post('ordem') );

		if($emails = $this->pedidosM->atualizaStatusAnterior($where)){
			$this->insereAndamentoPedido( $this->input->post('pedido_id'), $emails[0]['id'], ' Pedido retornado. Motivo: '.mb_strtoupper($this->input->post('motivo')) );
			$html='<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido '.$this->input->post('pedido_id').' retornou para o status de '.$emails[0]['descricao'].'</h2>
								<h4>Motivo do retorno: '.$this->input->post('motivo').'</h4>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Enviado por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>
							</div>
						</body>
					</html>';
			foreach ($emails as $email) {
				//$this->enviaEmail($email['email'],'Pedido '.$this->input->post('pedido_id').' Retornou', $html);
				$this->enviaEmail('webmaster@companytec.com.br','Pedido '.$this->input->post('pedido_id').' Retornou', $html);
			}
	    	
	    	echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
		
	}

	public function cadastraPedidos()
	{
		if($this->input->post() ){
			if($this->input->post('frete_id') == 2 ){
				$frete = 'C';
			}elseif($this->input->post('frete_id') == 3){
				$frete = 'F';
			}else{
				$frete = 'O';
			}
			
			$dados  = array( 	'orcamento_id'		=> 	$this->input->post('orcamento_id'),
								'pintura'			=>	$this->input->post('pintura'),
								'motor'				=>	$this->input->post('motor'),
								'combustiveis'		=> 	$this->input->post('combustiveis'),
								'fl_receber_info'	=> 	$this->input->post('fl_receber_info'),
								'status_pedido_id'	=> 	1,
								'usuario_geracao'	=>	$this->session->userdata('usuario_id'),
								'tecnico_id'		=> 	$this->input->post('tecnico_id'),
								'endereco_entrega'	=> 	$this->input->post('endereco_entrega'),
								'nome_cliente'		=> 	$this->input->post('nome_cliente'),
								'rg_cliente'		=> 	$this->input->post('rg_cliente'),
								'cpf_cliente'		=> 	$this->input->post('cpf_cliente'),
								'pintura_descricao'	=> 	$this->input->post('pintura_descricao'),
								'empresa_id'		=> 	$this->input->post('empresa_id'),
								'valor_total'		=> 	number_format($this->input->post('valor_total'), 6, '.', ''),
								'valor_total_stx'	=> 	number_format($this->input->post('valor_total_stx'), 6, '.', ''),
								'valor_ipi'			=> 	'5.00',
								'frete'				=> 	$frete,
								'observacao'		=> 	$this->input->post('observacao'),
								'testeira'			=> 	$this->input->post('testeira'), 
								'painel_frontal'	=> 	$this->input->post('painel_frontal'),
								'painel_lateral'	=> 	$this->input->post('painel_lateral') );

			$dados['arquivo'] = "";
			if($_FILES['arquivo_bb']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['arquivo_bb']['name']);
				$arquivo 		=	md5($_FILES['arquivo_bb']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$configuracao 	= array(	'upload_path'   	=> 	'./pedidos/',		        
								        	'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv|cdr|zip|rar|jpeg|JPEG',
								        	'file_name'     	=> 	$arquivo 	);      
			    
			    $this->load->library('upload', $configuracao);
			    
			    if ( 	$this->upload->do_upload('arquivo_bb') 	){

			    	$dados['arquivo'] 	= 	$arquivo;
			    }
			}

	   		$aux_produto 	= "";
	   		$aux_produto_id = "";
			$qtd = 1;
							
			for($i=0;$i<count($this->input->post('produto_id'));$i++){
									
				if($this->input->post('produto_id')[$i] == $aux_produto_id && trim(mb_strtoupper($this->input->post('produtos')[$i])) == trim(mb_strtoupper($aux_produto)) ){

					$qtd++;
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array(	'qtd' 		=> 	$qtd,																			'valor'		=> 	$this->input->post('valor')[$i],												'valor_stx'	=> 	$this->input->post('valor_stx')[$i]	);
				}else{
					$qtd =	1;
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array( 'qtd' 		=> 	$qtd,																			'valor'		=> 	$this->input->post('valor')[$i],
								'valor_stx'	=> 	$this->input->post('valor_stx')[$i]	);
				}

				$aux_produto 	= mb_strtoupper($this->input->post('produtos')[$i]);
				$aux_produto_id = $this->input->post('produto_id')[$i];
			}
			
			if( $this->pedidosM->add($dados) ){
				$pedido_id = $this->db->insert_id();
				
				$this->insereAndamentoPedido( $pedido_id, 1 );				
				$insereProd = array();
				$formaPagto = array();
				for($i=0;$i<count($this->input->post('porcentagem'));$i++){
                	$formaPagto = array(    'pedido_id'     =>     	$pedido_id,
                                            'porcentagem' 	=>     	$this->input->post('porcentagem')[$i],
                                            'descricao' 	=>		$this->input->post('descricao')[$i]	);				

					$this->pedidosM->insereFormaPagamento($formaPagto);					
				
				}

				foreach($produtos as $key=>$value)
				{
					$prod = explode('|',$key);
					
					if( $prod[1] == 'OPCIONAL' ){
						$prod[1] = '';
					}

					$insereProd = array('pedido_id' 	=>	$pedido_id,
										'produto_id'	=>	$prod[0],
										'produtos'		=> 	$prod[1],
										'qtd'			=> 	$value['qtd'],
										'valor'			=> 	$value['valor']);
					
					$this->pedidosM->inserePedidoItens($insereProd);
				}

				// caso tenha ocorrido alguma alteração
				if( $this->input->post('edicao_forma_pgto') == 1 || ($this->input->post('edicao_forma_pgto') == 0 && (trim($this->input->post('forma_pagto_orc')) != trim($this->input->post('descricao')[0]) ) ) ){

					$formaPagtoT = '';
					for($i=0;$i<count($this->input->post('porcentagem'));$i++){
	                	$formaPagtoT .= $this->input->post('porcentagem')[$i].'% '.$this->input->post('descricao')[$i].'<br>';		
					}	
					
					$email = '<html>
								<head></head>
								<body style="width: 850px; position:absolute;">
									<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
										<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
										<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">⚠️ Atenção! Forma de Pagamento Alterada ⚠️</h2>
										<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
											Pedido: #'.$pedido_id.'<br/><hr />
											Forma de pagamento orçamento: '.$this->input->post('forma_pagto_orc').'<br/>
											Nova Forma de pagamento: <b>'.$formaPagtoT.'</b><br/><hr /><br/>
											Usuário: '.$this->session->userdata('nome').'
										</p>										
									</div>
								</body>
							</html>';


					$this->enviaEmail('gestor@wertco.com.br','⚠️ ATENÇÃO! FORMA DE PAGTO ALTERADO. PEDIDO '.$pedido_id.'  ⚠️', $email );
					//$this->enviaEmail('webmaster@wertco.com.br','⚠️ ATENÇÃO! FORMA DE PAGTO ALTERADO. PEDIDO '.$pedido_id.'  ⚠️', $email );

					$dados = array(	'id' 				=>	$pedido_id,
					   				'status_pedido_id' 	=>	9 	);

					$this->pedidosM->atualizaStatusPedido($dados);
					$this->insereAndamentoPedido( $pedido_id,9 );

				}

				$this->log('Área Administrador | cadastra pedidos','pedidos','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				$pdf = $this->geraPedidosPdf($pedido_id,'tela');
				$this->session->set_flashdata('pedidoPdf', $pdf);
			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->pedidosM->selectPedidos();				
			$parametros['title']	=	"Pedidos";
			$this->_load_view('area-administrador/pedidos/pedidos',$parametros);

		}else{

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->pedidosM->selectPedidos();
			$parametros['title']	=	"Pedidos";
			$this->_load_view('area-administrador/pedidos/cadastra-pedidos',$parametros);
		}
	}

	public function retornaOrcamentos()
	{
		
		$rows = $this->orcamentosM->retornaOrcamentosFechados($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'			=>	$row['label'],
									'id'			=>	$row['id'],
									'value'			=>	$row['label'],
									'frete_id'		=> 	$row['frete_id'],
									'observacao'	=> 	$row['observacao']	);
				
			}

			echo json_encode($dados);
		}
	}
	
	public function retornaItens()
	{
		
		$rows = $this->itensM->retornaItens($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label'	=>	$row['label'],
								'id'		=>	$row['id'],
								'value'		=>	$row['label']	);
				
			}

			echo json_encode($dados);
		}
	}
	
	public function retornaProdutosAvulsos()
	{
		
		$rows = $this->itensM->retornaProdutosAvulsos($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label'		=>	$row['label'],
								'id'			=>	$row['id'],
								'value'			=>	$row['label'],
								'item_id'		=> 	$row['item_id']	);
				
			}

			echo json_encode($dados);
		}
	}
	
	public function retornaProdutosOrcamentos()
	{	
		$retorno = $this->orcamentosM->retornaProdutosOrcamentos($_POST['orcamento_id']);
		echo json_encode($retorno);
	}

	public function editarCombustiveisPedidos()
	{
		$editar = array(	'id' 		=> 	$this->input->post('produto_pedido_id'),
							'produtos' 	=> 	$this->input->post('produtos'));
		if( $this->pedidosM->editarCombustiveis($editar) ){
			echo json_encode(array('retorno' => 'sucesso'	));
		}else{
			echo json_encode(array('retorno' => 'erro' ));
		}
	}

	public function editarPedidos($id = null){

		if($this->input->post()){
			$update = array();
						
			if($this->input->post('tecnico') == 0){
				$tecnico_id = 0;
			}else{
				$tecnico_id = $this->input->post('tecnico_id');
			}

			$update = array('id'				=>	$this->input->post('id'),
							'orcamento_id'		=> 	$this->input->post('orcamento_id'),
							'pintura'			=>	$this->input->post('pintura'),
							'fl_receber_info'	=> 	$this->input->post('fl_receber_info'),
							'tecnico_id'		=> 	$tecnico_id,
							'endereco_entrega'	=> 	$this->input->post('endereco_entrega'),
							'nome_cliente'		=> 	$this->input->post('nome_cliente'),
							'rg_cliente'		=> 	$this->input->post('rg_cliente'),
							'cpf_cliente'		=> 	$this->input->post('cpf_cliente'),
							'pintura_descricao'	=> 	$this->input->post('pintura_descricao'),
							'observacao'		=> 	$this->input->post('observacao'),
							'testeira'			=> 	$this->input->post('testeira'),	
							'painel_frontal'	=> 	$this->input->post('painel_frontal'),
							'painel_lateral'	=> 	$this->input->post('painel_lateral') 	);

			if($this->input->post('pintura') != 'BANDEIRA_BRANCA' || $this->input->post('pintura') != 'bandeira_branca'){
				$update['arquivo'] 	= 	'';

			}else{
				$tipo 			= 	explode('.', $_FILES['arquivo_bb']['name']);
				$arquivo 		=	md5($_FILES['arquivo_bb']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$configuracao = array(
			        'upload_path'   	=> 	'./pedidos/',		        
			        'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv|cdr|zip|rar|jpeg|JPEG',
			        'file_name'     	=> 	$arquivo
			    );      
			    
			    $this->load->library('upload', $configuracao);
			    
			    if ($this->upload->do_upload('arquivo_bb')){

			    	$update['arquivo'] 	= 	$arquivo;
			    }
			}

			$aux_produto 	= "";
	   		$aux_produto_id = "";
			$qtd = 1;
			
			for($i=0;$i<count($this->input->post('produto_id'));$i++){
									
				if($this->input->post('produto_id')[$i] == $aux_produto_id && trim(mb_strtoupper($this->input->post('produtos')[$i])) == trim(mb_strtoupper($aux_produto)) ){

					$qtd++;
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array(	'qtd' 		=> 	$qtd,																			'valor'		=> 	$this->input->post('valor')[$i],												'valor_stx'	=> 	$this->input->post('valor_stx')[$i]	);
				}else{
					$qtd =	1;
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array( 'qtd' 		=> 	$qtd,																			'valor'		=> 	$this->input->post('valor')[$i],
								'valor_stx'	=> 	$this->input->post('valor_stx')[$i]	);
				}

				$aux_produto 	= mb_strtoupper($this->input->post('produtos')[$i]);
				$aux_produto_id = $this->input->post('produto_id')[$i];
			}	

			if($this->pedidosM->atualizaPedidos($update)){

				$this->log('Área Administrador | atualizar pedidos','pedidos','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);				
				
				for($i=0;$i<count($this->input->post('descricao'));$i++){

                    if($this->input->post('descricao')[$i] != ''){

                        $formaPagto = array(    'pedido_id' 	=>     $this->input->post('id'), 
                        						'porcentagem'   =>     $this->input->post('porcentagem')[$i],
                                                'descricao'  	=>     $this->input->post('descricao')[$i]);
                        $this->pedidosM->insereFormaPagamento($formaPagto);
                    
                    }
                }
				

				$this->pedidosM->excluirPedidoItens($this->input->post('id'));

				foreach($produtos as $key=>$value)
				{
					$prod = explode('|',$key);
					
					if( $prod[1] == 'OPCIONAL' ){
						$prod[1] = '';
					}

					$insereProd = array('pedido_id' 	=>	$this->input->post('id'),
										'produto_id'	=>	$prod[0],
										'produtos'		=> 	$prod[1],
										'qtd'			=> 	$value['qtd'],
										'valor'			=> 	$value['valor']);
					
					$this->pedidosM->inserePedidoItens($insereProd);
				}

				// caso tenha ocorrido alguma alteração
				if( $this->input->post('edicao_forma_pgto') == 1 ){

					$formaPagtoT = '';
					for($i=0;$i<count($this->input->post('porcentagem'));$i++){
	                	$formaPagtoT .= $this->input->post('porcentagem')[$i].'% '.$this->input->post('descricao')[$i].'<br>';

					
				
					}	
					
					$email = '<html>
								<head></head>
								<body style="width: 850px; position:absolute;">
									<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
										<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
										<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">⚠️ Atenção! Forma de Pagamento Alterada ⚠️</h2>
										<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
											Pedido: #'.$this->input->post('id').'<br/><hr />
											Nova Forma de pagamento: <b>'.$formaPagtoT.'</b><br/><hr/>
											Usuário: <b>'.$this->session->userdata('nome').'</b>
										</p>										
									</div>
								</body>
							</html>';
					$this->enviaEmail('gestor@wertco.com.br','⚠️ ATENÇÃO! FORMA DE PAGTO ALTERADO. PEDIDO '.$this->input->post('id').'  ⚠️', $email );

					$dados = array(	'id' 				=>	$this->input->post('id'),
					   				'status_pedido_id' 	=>	9 	);

					$this->pedidosM->atualizaStatusPedido($dados);

				}

				$this->session->set_flashdata('sucesso', 'ok');
				$pdf = $this->geraPedidosPdf($this->input->post('id'),'tela');
				$this->session->set_flashdata('pedidoPdf', $pdf);
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->pedidosM->selectPedidos();				
				$parametros['title']	=	"Pedidos";
				$this->_load_view('area-administrador/pedidos/pedidos',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->pedidosM->selectPedidos();				
				$parametros['title']	=	"Pedidos";
				$this->_load_view('area-administrador/pedidos/pedidos',$parametros);
			}

		}else{

			$row 						= 	$this->pedidosM->getPedidosById($id);
			$parametros 				= 	$this->session->userdata();
			$parametros['dados']		=	$row;
			$parametros['formaPagto'] 	= 	$this->pedidosM->pedidosFormaPagto($id);
			$parametros['produtos']		= 	$this->pedidosM->pedidosProdutos($id);						
			$parametros['title']		=	"Editar Pedidos";			
			$this->_load_view('area-administrador/pedidos/editar-pedidos',	$parametros );
		}
	}

	/************************************************************ 
	************** GERA PDF DO ORÇAMENTO SOLICITADO *************
	*************************************************************/
	public function geraPedidosPdf($pedido_id, $origem_solicitacao)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pedidos/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('pedidos-'.$pedido_id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
		
	    $data['empresa'] 		= 	$this->pedidosM->pedidosEmpresasPdf($pedido_id);
	    $data['produtos'] 		= 	$this->pedidosM->pedidosProdutos($pedido_id);
	    $data['forma_pagto']	= 	$this->pedidosM->pedidosFormaPagto($pedido_id);
	    $data['icms'] 			= 	$this->pedidosM->buscaIcmsOrcamentoPedido($pedido_id);
	    $data['usuario_wertco'] = 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
	    $data['entrega']		= 	$this->pedidosM->buscaFormaEntrega($pedido_id);
	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-administrador/pedidos/pedidos-pdf', $data, true));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'pedidos-'.$pedido_id.'.pdf'));
	    	}else{
	    		return $retorno['retorno'] = 'pedidos-'.$pedido_id.'.pdf';
	    	}
	    }	    
	    
    }

	/************************************************************ 
	************** GERA PDF DAS OP'S GERADAS ********************
	*************************************************************/
	public function geraOpsPdf($pedido_id)
    {
    	//Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pedidos/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('ops-pedidos-'.$pedido_id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
			    
		$data['ops'] 			= 	$this->pedidosM->buscaOpsPorPedido($pedido_id);
	    $data['usuario_wertco'] = 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
	    //Load html view
	    $this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/pedidos/ops-pedidos-pdf', $data, true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded	    	
	    	redirect(base_url('pedidos/ops-pedidos-'.$pedido_id.'.pdf'));
	    	
	    }
	
    }
	
    /****************************************************************************
	********* Método Ajax - Excluir Forma de pagamento Pedidos ******************
	*****************************************************************************/
	public function excluirFormaPagtoPedidos()
	{

		if($this->pedidosM->excluirPedidoFormPagto($_POST['id'])){
			$this->log('Área Administrador | excluir produtos pedidos','pedido_itens','EXCLUSÃO', $this->session->userdata('usuario_id'), array('id' => $_POST['id']),array('id' => $_POST['id']),$_SERVER['REMOTE_ADDR']);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	// Método para inserção dos andamentos dos pedidos
	private function insereAndamentoPedido($pedido_id, $status_pedido_id, $motivo='')
	{
		$andamento = $this->pedidosM->buscaStatusByid($status_pedido_id);

		$insert = array(	'pedido_id' 		=> 	$pedido_id,
							'status_pedido_id' 	=>	$status_pedido_id,
							'andamento'			=> 	$andamento['descricao'].$motivo,
							'usuario_id' 		=> 	$this->session->userdata('usuario_id') );

		if( $this->pedidosM->insereAndamento($insert) ){
			return true;
		}else{
			return false;
		}
	}

	public function alteraStatusPedido()
	{

		$dados = array('id'					=>	$_POST['pedido_id'],
					   'status_pedido_id' 	=>	$_POST['status_pedido']);

		if($this->pedidosM->atualizaStatusPedido($dados))
		{
			$this->insereAndamentoPedido($_POST['pedido_id'], $_POST['status_pedido']);
			if($_POST['status_pedido'] == 2){
			
				$email='<html>
						<head></head>
						<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
							<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
								<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido '.$_POST['pedido_id'].' Confirmado pelo cliente</h1>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Pedido: '.$_POST['pedido_id'].' Confirmado pelo cliente <br/>
									Usuário: '.$this->session->userdata('nome').'
									<br/>
								</p>
							</div>
						</body>
					</html>';
				
	    		$this->enviaEmail('industrial@wertco.com.br','⚠️ Pedido '.$_POST['pedido_id'].' confirmado pelo cliente ⚠️', $email);
	    		$this->enviaEmail('todosvendas@wertco.com.br','⚠️ Pedido '.$_POST['pedido_id'].' confirmado pelo cliente ⚠️', $email);
				
	    	}

	    	//envia e-mail para a PRODUÇÃO/EXPEDIÇÃO avisando sobre a liberação para inspeção final 
	    	if($_POST['status_pedido'] == 18){
			
				$email='<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido <b>#'.$this->input->post('pedido_id').'</b>  LIBERADO PARA ADESIVAÇÃO</h2>																
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Enviado por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>							
							</div>
						</body>
					</html>';	    		
	    		
				$this->enviaEmail('comunicacaovisual@wertco.com.br','⛽ Pedido '.$_POST['pedido_id'].' liberado para adesivação ⛽', $email);
	    	}

	    	//envia e-mail para a qualidade solicitando a inspeção 
	    	if($_POST['status_pedido'] == 4){
			
				$email='<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido #'.$this->input->post('pedido_id').' Liberado para inspeção</h2>																
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Enviado por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>							
							</div>
						</body>
					</html>';
				//ALTERAR PARA	
	    		$this->enviaEmail('qualidade@wertco.com.br','Pedido '.$_POST['pedido_id'].' liberado para inspeção', $email);
	    		$this->enviaEmail('qualidade2@wertco.com.br','Pedido '.$_POST['pedido_id'].' liberado para inspeção', $email);
	    		//Enviar para o financeiro e para o comercial tb
	    		$this->enviaEmail('gestor@wertco.com.br','Pedido '.$_POST['pedido_id'].' liberado para inspeção', $email);
	    		$this->enviaEmail('financeiro@wertco.com.br','Pedido '.$_POST['pedido_id'].' liberado para inspeção', $email);
	    		$this->enviaEmail('vendas3@wertco.com.br','Pedido '.$_POST['pedido_id'].' liberado para inspeção', $email);
	    		
	    	}

	    	//envia e-mail para a produção avisando sobre a liberação para inspeção da qualidade 
	    	if($_POST['status_pedido'] == 12){
			
				$email='<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido <b>#'.$this->input->post('pedido_id').'</b> Liberado para inspeção da qualidade </h2>																
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Enviado por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>							
							</div>
						</body>
					</html>';
				// ALTERAR PARA	
	    		$this->enviaEmail('qualidade@wertco.com.br','Pedido '.$_POST['pedido_id'].' liberado para inspeção da qualidade', $email);
	    		$this->enviaEmail('qualidade2@wertco.com.br','Pedido '.$_POST['pedido_id'].' liberado para inspeção da qualidade', $email);
	    		
	    	}

	    	//envia e-mail para a QUALIDADE avisando sobre a liberação para inspeção final 
	    	if($_POST['status_pedido'] == 13){
			
				$email='<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido <b>#'.$this->input->post('pedido_id').'</b> Liberado para inspeção final</h2>																
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Enviado por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>							
							</div>
						</body>
					</html>';

	    		
	    		$this->enviaEmail('qualidade@wertco.com.br','Pedido '.$_POST['pedido_id'].' liberado para INSPEÇÃO FINAL', $email);
	    		$this->enviaEmail('qualidade2@wertco.com.br','Pedido '.$_POST['pedido_id'].' liberado para INSPEÇÃO FINAL', $email);	    			    		
	    	}

	    	//envia e-mail para a PRODUÇÃO/EXPEDIÇÃO avisando sobre a liberação para expedição 
	    	if($_POST['status_pedido'] == 14){
			
				$email='<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido <b>#'.$this->input->post('pedido_id').'</b>  liberado para embalagem</h2>																
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Enviado por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>							
							</div>
						</body>
					</html>';

	    		
	    		$this->enviaEmail('producao@wertco.com.br','Pedido '.$_POST['pedido_id'].' liberado para embalagem', $email);
				//$this->enviaEmail('todosvendas@wertco.com.br','Pedido '.$_POST['pedido_id'].' liberado para embalagem', $email);				
	    	}    	
	    	
	    	//envia e-mail para o COMERCIAL avisando que o produto foi finalizado e está disponível para ser entregue ao cliente	    	
	    	if($_POST['status_pedido'] == 15){
				
	    		$dados = $this->nrSerieM->buscaNrSeriePorPedido($_POST['pedido_id']);

				$email='<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;"> As bombas do pedido <b>#'.$this->input->post('pedido_id').'</b> abaixo, já estão liberadas para coleta!</h2>																
								<table>
									<thead>
										<th>Nr. Série</th>
										<th>Cliente</th>
										<th>Modelo</th>
										<th>Bicos</th>
									</thead>
									<tbody>';
				foreach ($dados as $dado) {
					$email.=' 			<tr>
											<td>'.$dado['id'].'</td>
											<td>'.$dado['cliente'].'</td>
											<td>'.$dado['modelo'].'</td>
											<td>'.$dado['nr_bicos'].'</td>
										</tr>';
				}
				$email.=		'	</tbody>
								</table>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Enviado por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>
							</div>
						</body>
					</html>';
					
				$update = array('pedido_id' 	=> $this->input->post('pedido_id'),
								'status_op_id' 	=> 3 );	
	    		$this->opM->fechaOps($update);
	    		$this->enviaEmail('todosvendas@wertco.com.br','Bombas do Pedido '.$_POST['pedido_id'].' liberado para coleta', $email);

	    	}

	    	//envia e-mail para a PRODUÇÃO/EXPEDIÇÃO avisando sobre a liberação para expedição 
	    	if($_POST['status_pedido'] == 6){
			
				$email='<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido <b>#'.$this->input->post('pedido_id').'</b>  ENTREGUE</h2>	
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Enviado por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>							
							</div>
						</body>
					</html>';
	    		
				$this->enviaEmail('todossuporte@wertco.com.br','Pedido '.$_POST['pedido_id'].' entregue', $email);				
	    	}

	    	if($_POST['status_pedido'] == 7){

	    		$pedido = $this->pedidosM->getPedidosById($_POST['pedido_id']);
	    		$dados 	= array(	'id' 					=> 	$pedido->orcamento_id,
	    						 	'status_orcamento_id' 	=> 	4 );	 		    		

				if($this->orcamentosM->atualizaStatusOrcamento($dados))
				{
					$this->log('Área Administrador | atualizar status orçamento','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
					

					$inserirAndamento = array(	'orcamento_id'			=>	$pedido->orcamento_id,
												'status_orcamento_id'	=> 4,
												'andamento'				=>	'Pedido cancelado, orçamento cancelado!',
												'dthr_andamento'		=>	date('Y-m-d H:i:s'),
												'usuario_id'			=> 	$this->session->userdata('usuario_id'));

					$this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento);
			    	
			    	
					
					$this->log('Área Administrador | Atualizar status pedido','pedidos','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				
				}
			}

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}
	
	public function alteraStatusOp()
	{

		$dados = array('id'				=>	$_POST['op_id'],
					   'status_op_id' 	=>	$_POST['status_op']);

		if($this->opM->atualizarOrdemProducao($dados))
		{
			$this->log('Área Administrador | atualizar status op','op','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);			
			echo json_encode(array('retorno' => 'sucesso'));	
			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function confirmaPedidoOps($id)
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados = $this->input->post();								
			//echo '<pre>';var_dump($dados);die;
			//verificar a data de produção e verifica a data de entrega do pedido
			$regras = $this->programacaoProdM->select();

			$totalB = $this->pedidosM->totalBicosBombas($id);

			//Caso ainda exista espaço na semana para o pedido ser produzido
			if( ($regras['total_bicos_atual'] + $totalB['total_bicos']) < $regras['nr_bicos_semana'] && ($regras['total_bombas_atual'] + $totalB['total_bombas']) < $regras['nr_bombas_semana'] ){

				$data_semana_atual 	= 	$regras['data_semana_atual'];
				$total_bicos 		= 	$regras['total_bicos_atual'] 	+ 	$totalB['total_bicos'];
				$total_bombas 		= 	$regras['total_bombas_atual'] 	+ 	$totalB['total_bombas'];

				if( strtotime($regras['data_abertura']) > strtotime($regras['data_semana_atual']) ){
					$data_semana_atual 	= 	$regras['data_abertura'];
					$total_bicos 		= 	$regras['total_bicos_atual'];
					$total_bombas 		= 	$regras['total_bombas_atual'];					
				}

				$atuNrbombasBicos = array(	'id' 					=> 	1,
											'total_bicos_atual'		=> 	$total_bicos,
											'total_bombas_atual'	=> 	$total_bombas,
											'dt_semana_atual'		=> 	$data_semana_atual	);

				$inserePedidos = array( 'pedido_id' 			=> 	$id,
										'dt_semana_prog' 		=> 	$data_semana_atual,
										'nr_bicos'				=> 	$totalB['total_bicos'],
										'nr_bombas'				=> 	$totalB['total_bombas'],
										'dt_previsao_entrega' 	=> 	date('Y-m-d',strtotime($data_semana_atual.' +15 days') )	 		 );
			}else{
				date_default_timezone_set('America/Sao_Paulo');

				$dia = new DateTime($regras['data_semana_atual']);
				$dia->modify( 'next monday' );
				$proximaSegunda =  $dia->format('Y-m-d');
				$data_semana_atual = (strtotime($regras['data_abertura']) > strtotime($proximaSegunda)) ? $regras['data_abertura'] : $proximaSegunda;

				$atuNrbombasBicos = array(	'id' 					=> 	1,
											'total_bicos_atual'		=> 	$totalB['total_bicos'],
											'total_bombas_atual'	=> 	$totalB['total_bombas'],
											'dt_semana_atual'		=> 	$data_semana_atual	);

				$inserePedidos = array( 'pedido_id' 			=> 	$id,
										'dt_semana_prog' 		=> 	$data_semana_atual,	
										'nr_bicos'				=> 	$totalB['total_bicos'],
										'nr_bombas'				=> 	$totalB['total_bombas'],
										'dt_previsao_entrega' 	=> 	date('Y-m-d',strtotime($data_semana_atual.' +15 days'))	);

			}

			$this->programacaoProdM->atualiza($atuNrbombasBicos);
			
			$this->programacaoProdM->inserirPedido($inserePedidos);

			$this->importaPedidosStx($id, $dados);
			
			$updatePedido = array(	'id' 				=> 	$id,
								  	'status_pedido_id' 	=>	11	);
			
			$this->pedidosM->atualizaPedidos($updatePedido);			
			$this->insereAndamentoPedido( $id,11 );
			$i = 1;
			$erro = 0;
			
			
			//MONTA SQL PARA VERIFICAR SE EXISTE ITEM			
			foreach ($this->input->post('produto_id') as $produto_id) {
				$whereProduto=$produto_id;
				$monta_where = " i.produto_id = ". $produto_id;
				$j=0;
				$insereItem = array();
				$insereSubItem = array();
				$descricao 	= "";
				$verifica = false;
				$whereIni="";
				$whereFim="";
				$whereNotIn="";				
				$obs_subitem="";
				//echo count($dados['subitem'][$produto_id][$i]) .">= 1 &&". $dados['subitem'][$produto_id][$i][$j]."!= ''";die;
				
				$unico = 0;
				if(count($dados['subitem'][$produto_id][$i]) >= 1 && $dados['subitem'][$produto_id][$i][$j] != ""){
					
					foreach($this->input->post('subitem')[$produto_id][$i] as $subitem){
						
						$subitem_id = explode('+',$subitem);
						
						if($subitem_id[0] != ''){

							$sql = "SELECT count(*) as total FROM itens i, subitens s
							         WHERE i.id = s.item_id and i.produto_id = ". $produto_id."
									 and (s.produto_id=". $subitem_id[0]."
									 and s.qtd=".$this->input->post('subitem_qtd')[$produto_id][$i][$j].")";							
														
							$retorno = $this->itensM->verificaItem($sql);
							
							if($retorno['total'] == 0){
								$verifica = true;									
								
							}else{
								
								/*$sql = "SELECT i.id FROM itens i, subitens s
							         WHERE i.id = s.item_id and i.produto_id = ". $produto_id."
									 and (s.produto_id=". $subitem_id[0]."
									 and s.qtd=".$this->input->post('subitem_qtd')[$produto_id][$i][$j].")";
									 
								$id_item = $this->itensM->verificaItem($sql);*/

								$whereIni.="(select item_id from subitens WHERE item_id in ";
								$whereFim.=" and produto_id = ". $subitem_id[0]." and qtd=".$this->input->post('subitem_qtd')[$produto_id][$i][$j].")";
								$whereNotIn.=" and (produto_id != ". $subitem_id[0]." and qtd != ".$this->input->post('subitem_qtd')[$produto_id][$i][$j].")";
								$whereProduto = $produto_id;
								
							}
							
							$sql_item 	= "select if(id is null,0,id) as id from itens where produto_id =".$subitem_id[0];
							
							$item_filho = $this->itensM->getIdItem($sql_item);  
							
							$insereSubItem[] = array( 'item_filho'	=> 	$item_filho, 
													  'produto_id' 	=> 	$subitem_id[0],
													  'qtd'			=> 	$this->input->post('subitem_qtd')[$produto_id][$i][$j],
													  'descricao'	=> 	$subitem_id[1]);
							
							$descricao .= ', '.$subitem_id[1]; 
							$obs_subitem.= ' '.$this->input->post('subitem_qtd')[$produto_id][$i][$j].' - ' .$subitem_id[1].'. ';
						}
												
						$j++;

					}
					
					/*echo '<pre>';
					var_dump($insereSubItem);
					die;*/
				}else{
					$sql = "SELECT count(*) as total FROM itens i
							left join subitens s on i.id = s.item_id
							 WHERE i.produto_id = ". $produto_id."
							 and s.item_id is null ";
									 
					$retorno = $this->itensM->verificaItem($sql);
					if($retorno['total'] == 0){
						$verifica= true;									
						
					}else{
						$sql = "SELECT i.id FROM itens i
							 WHERE i.produto_id = ". $produto_id ." and i.id not in (select item_id from subitens)";
						
						$verifica= false;
						$unico = 1;
						$id_item = $this->itensM->verificaItem($sql);
					}
				}

				//echo ($verifica) ? 'verifica: true' : 'verifica: false';
				//echo '<br>unico:'.$unico.'<br>';				
				if($verifica){
					
					/***************** INSERE NAS TABELAS ITENS E SUBITENS ***********************/
					$insereItem = array('produto_id' 	=> $produto_id,
										'descricao'		=> $this->input->post('item_descricao')[$produto_id][$i][0].','.$descricao );
					
					//INSERE NA TABELA ITENS
					if(!$this->itensM->insereItens($insereItem)){
						$erro = 1;
					}

					$item_id = $this->db->insert_id();
					
					//INSERE NA TABELA SUBITENS
					foreach( $insereSubItem as $insereSubs ){
						
						if($insereSubs['item_filho'] == '' || $insereSubs['item_filho'] == null){
							$insereSubs['item_filho'] = $produto_id;
						}

						$inserir = array( 	'item_filho' => $insereSubs['item_filho'],	
											'produto_id' => $insereSubs['produto_id'],
											'qtd'	 	 =>	$insereSubs['qtd'],
											'item_id'	 => $item_id 	);

						if(!$this->itensM->insereSubItens($inserir)){
							$erro = 1;
						}

					}

				}else{

					if($unico==0){
						//$item_id = $id_item['id'];
						$sql = "SELECT distinct item_id FROM subitens, itens i WHERE item_id in " .$whereIni." (SELECT item_id FROM `subitens` WHERE 1=1 ".$whereFim.") and item_id not in (SELECT distinct item_id from subitens where 1=1 ".$whereNotIn.")  and i.id = subitens.item_id and i.produto_id = ".$whereProduto;
						
						$id_item = $this->itensM->verificaItem($sql);
						$item_id = $id_item['item_id'];
						
					}else{
						$item_id = $id_item['id'];
						//echo 'item_id: '.$item_id.'<br>';
					}

				}

				
				/*********************************** INSERE NA tabela ORDEM DE PRODUÇÃO ********************************/
				//Caso não tenha item ainda, salva o mesmo				
				if($item_id == ''){
					/***************** INSERE NAS TABELAS ITENS E SUBITENS ***********************/
					$insereItem = array('produto_id' 	=> $produto_id,
										'descricao'		=> $this->input->post('item_descricao')[$produto_id][$i][0].','.$descricao );
					
					//INSERE NA TABELA ITENS
					if(!$this->itensM->insereItens($insereItem)){
						$erro = 1;
					}

					$item_id = $this->db->insert_id();
					
					//INSERE NA TABELA SUBITENS
					foreach( $insereSubItem as $insereSubs ){
						
						if($insereSubs['item_filho'] == '' || $insereSubs['item_filho'] == null){
							$insereSubs['item_filho'] = $produto_id;
						}

						$inserir = array( 	'item_filho' => $insereSubs['item_filho'],	
											'produto_id' => $insereSubs['produto_id'],
											'qtd'	 	 =>	$insereSubs['qtd'],
											'item_id'	 => $item_id 	);

						if(!$this->itensM->insereSubItens($inserir)){
							$erro = 1;
						}

					}
				}

				$sql = "SELECT count(*) as total, op.id 
				 		 FROM  ordem_producao op, itens i 
				 		 WHERE op.item_id = i.id 
				 		 	and i.id = ". $item_id ." and op.pedido_id = ".$id." and op.status_op_id <> 4 group by id";						
				
				$ops = $this->opM->verificaOp($sql);				
				$observacao = "";
				$modelo = explode('-',$this->input->post('modelo')[$produto_id][$i][0]);					
					
				$vazao = '';

				if( count($modelo) == 2 ){
					$vazao = "Vazão Baixa - 40 L/Min";

				}elseif(count($modelo) == 3){
					switch ($modelo[count($modelo)-1]) {
						case 'MV':
							$vazao = "Vazão Média - 75 L/Min";
							break;
						case 'AV':
							$vazao = "Vazão Alta - 125 L/Min";
							break;
						case 'BL':
							$vazao = "Vazão Baixa - 40 L/Min";
							break;
						case 'BF':
							$vazao = "Vazão Baixa - 40 L/Min";
							break;
						
					}
				
				}elseif(count($modelo) == 4){

					if($modelo[count($modelo)-1]=='MV'){
						$vazao = "Média Vazão - 75 L/Min";

					}elseif($modelo[count($modelo)-1]=='AV'){
						$vazao = "Vazão Alta - 130 L/Min";

					}else{
						$vazao = "Vazão Mista - ".$modelo[count($modelo)-1];

					}
				}
				
				if( $ops['total'] == 0)
				{
					$count = 0;
					$combustivel_produtos = '';

					foreach($dados['produtos'][$produto_id][$i] as $produto){
						
						
						$combustivel_produtos = $produto;
						
					}

					$codigo = explode('-', $this->input->post('item_descricao')[$produto_id][$i][0]);
					//$observacao = 'Opcionais: '.$obs_subitem.'Produtos: '.$dados['produtos'][$produto_id][$i][0].', ';
					$observacao = 'Opcionais: '.$obs_subitem.'Produtos: '.$combustivel_produtos.', ';
					$observacao.= 'Pintura: '.$dados['pintura'][$produto_id][$i][0].', ';
					$observacao.= 'Potência: '.$dados['potencia'][$produto_id][$i][0].', ';
					$observacao.= 'Voltagem: '.$dados['voltagem'][$produto_id][$i][0].', ';
					$observacao.= 'Frequência: '.$dados['frequencia'][$produto_id][$i][0].', ';
					$observacao.= 'Marca do Motor: WEG, ';
					$observacao.= 'Motor: '.$dados['motor'][$produto_id][$i][0].', ';
					$observacao.= 'Vazão: '.$vazao.', ';
					$observacao.= 'Mangueira: '.$dados['mangueira'][$produto_id][$i][0].', ';
					$observacao.= 'Bicos: '.$dados['bicos'][$produto_id][$i][0].', ';
					$observacao.= 'Obs: '.$dados['obs'][$produto_id][$i][0].', |#|' ;
					$data_previsao 	= 	explode('/',$this->input->post('dthr_previsao'));

					//insere produtos na ordem de produção


					$insereOp 	= array(	'item_id' 			=> 	$item_id,
										  	'status_op_id'		=> 	1,
										  	'pedido_id' 		=> 	$id,
										  	'observacao'		=> 	mb_strtoupper($observacao),
										  	'qtd' 				=>  1,
										  	'usuario_geracao' 	=> 	mb_strtoupper($this->session->userdata('usuario_id')),
										  	'modelo' 			=> 	mb_strtoupper($this->input->post('modelo')[$produto_id][$i][0]),
										  	'modelo_tecnico'	=> 	mb_strtoupper($this->input->post('modelo_tecnico')[$produto_id][$i][0]),
											'nome_cliente'		=> 	mb_strtoupper($this->input->post('cliente')),
											'codigo'			=> 	$codigo[0],										
											'combustivel'		=>	mb_strtoupper($combustivel_produtos),
											'cliente_id'		=> 	$this->input->post('cliente_id'),	
											'cidade'			=> 	mb_strtoupper($this->input->post('cidade')),
											'uf'				=> 	mb_strtoupper($this->input->post('uf')),
											'pais'				=> 	mb_strtoupper($this->input->post('pais')),
											'descricao'			=> 	mb_strtoupper($codigo[count($codigo)-1].$descricao),
											'dthr_previsao'		=> 	date('Y-m-d',strtotime($data_semana_atual.' +15 days')),
											'informacoes'		=> 	trim(mb_strtoupper($this->input->post('informacoes'))) ) ;

					if(!$this->opM->insereOp($insereOp)){
						$erro = 1;
					}
					
				}else{
					$count = 0;
					$combustivel_produtos = '';
					foreach($dados['produtos'][$produto_id][$i] as $produto){
						
						if($count > 0 && $count < count($dados['produtos'][$produto_id][$i])){
							$combustivel_produtos.= '/';
						}
						$combustivel_produtos.= $produto;
						$count++;
					}
					$codigo = explode('-', $this->input->post('item_descricao')[$produto_id][$i][0]);
					$observacao = 'Opcionais: '.$obs_subitem.'Produtos: '.$combustivel_produtos.', ';
					$observacao.= 'Pintura: '.$dados['pintura'][$produto_id][$i][0].', ';
					$observacao.= 'Potência: '.$dados['potencia'][$produto_id][$i][0].', ';
					$observacao.= 'Voltagem: '.$dados['voltagem'][$produto_id][$i][0].', ';
					$observacao.= 'Frequência: '.$dados['frequencia'][$produto_id][$i][0].', ';
					$observacao.= 'Marca do Motor: WEG, ';
					$observacao.= 'Motor: '.$dados['motor'][$produto_id][$i][0].', ';
					$observacao.= 'Vazão: '.$vazao.', ';
					$observacao.= 'Mangueira: '.$dados['mangueira'][$produto_id][$i][0].', ';
					$observacao.= 'Bicos: '.$dados['bicos'][$produto_id][$i][0].', ';
					$observacao.= 'Obs: '.$dados['obs'][$produto_id][$i][0].', |#|';

					$qtdOp 		= $this->opM->verificaQtd($ops['id']);					
					$obs 		= $qtdOp['observacao'].$observacao;
					$combustivel= $qtdOp['combustivel'].' - '. $combustivel_produtos;
					$updateQtd 	= array('id' 			=> 	$ops['id'],
										'qtd'			=> 	$qtdOp['qtd']+1,
										'observacao' 	=>	mb_strtoupper($obs),
										'combustivel'	=> 	mb_strtoupper($combustivel) );

					if(!$this->opM->atualizaQtdObs($updateQtd)){
						$erro = 1;
					}

				}

				$i++;
				
			}
			
			$ops = $this->opM->getOpsByPedido($id);
			$apiWorkfeed = array(	'acao' => 'agendamento_op',
								 	'versao'=>	'0.0'	);
			foreach( $ops as $op){
				$apiWorkfeed['ops'][] = array(	'op' 		=>	$op['id'],
												'item' 		=> 	$op['item_id'],
												'qtd'		=> 	$op['qtd'],
												'modelo'	=> 	$op['modelo']	);
			}

			//Envia para o workfeed as informações das op's geradas
			/*$url = 'http://177.135.100.198/WorkFeed/Api/ApiWertco.php';			
			$options = array(
			    'http' => array(
			        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			        'method'  => 'POST',
			        'content' => http_build_query($apiWorkfeed)
			    )
			);
			$context  = stream_context_create($options);
			$result = file_get_contents($url, false, $context);
			
			if ($result === FALSE) { 
				$this->session->set_flashdata('erro', 'erro');
			}else{
				//echo $result;die;
				// Envia email para o Miro e para o Marcelo
				//$this->enviaEmailProducao('suprimentos@wertco.com.br','suprimentos');
				//$this->enviaEmailProducao('qualidade@wertco.com.br','producao');
				$this->session->set_flashdata('sucesso', 'ok');
			}	
			*/			
			$this->session->set_flashdata('sucesso', 'ok');
			
			/*
			$this->enviaEmailProducao('suprimentos@wertco.com.br','suprimentos',$id);
			$this->enviaEmailProducao('qualidade@wertco.com.br','producao',$id);
			$this->enviaEmailProducao('comunicacaovisual@wertco.com.br','marcio',$id);
			$this->enviaEmailProducao('webmaster@companytec.com.br','mateus',$id); 
			$this->enviaEmailProducao('producao@wertco.com.br','leomar',$id); 
			*/
			
			$email='<html>
					<head></head>
					<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
						<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido '.$id.' confirmado e ops geradas </h1>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">							
							Pedido: '.$id.'<br/>
							Cliente: '.$this->input->post('cliente').'<br/>
							Usuario: '.$this->session->userdata('nome').' <br/>
							Previsão de entrega: '.date('d/m/Y',strtotime($data_semana_atual.' +15 days')).'
							</p>
						</div>
					</body>
				</html>';
	    	
	    	$this->enviaEmail('webmaster@wertco.com.br','⛽ Pedido:'.$id.' confirmado pelo cliente e ops geradas', $email);						
	    	//$this->enviaEmail('industrial@wertco.com.br','⛽ Pedido:'.$id.' confirmado pelo cliente e ops geradas', $email);						
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->pedidosM->selectPedidos();				
			$parametros['title']	=	"Pedidos";

			$this->_load_view('area-administrador/pedidos/pedidos',$parametros);
				 
		}else{

			$row 						= 	$this->pedidosM->getPedidosById($id);
			$parametros 				= 	$this->session->userdata();
			$parametros['dados']		=	$row;			
			$parametros['produtos']		= 	$this->pedidosM->pedidosProdutos($id);
			$parametros['title']		=	"Confirmação de Pedidos/Geração de Ordem de Produção";
			$parametros['id']			= 	$id;
			$parametros['cliente']		= 	$this->pedidosM->retornaClientePorPedido($id);			
			$this->_load_view('area-administrador/pedidos/confirm-gera-pedidos', $parametros );
		}
	}

	public function confirmaEnviaEmailPedido(){

		$pedido_id 	= $_POST['pedido_id'];
		$email 		= $_POST['email'];

		$update = array('id'				=> 	$pedido_id,
						'status_pedido_id' 	=> 	4);

		if($this->pedidosM->atualizaStatusPedido($update)){
			
			$this->insereAndamentoPedido( $pedido_id,4 );

			if($this->enviaEmailConfirmacaoPedido($pedido_id,$email)) {
				echo json_encode(array('retorno' => 'sucesso'));
			}else{
				echo json_encode(array('retorno' => 'Não foi enviado e-mail'));
			}

		}else{
			echo json_encode(array('retorno' => 'Erro: Não atualizou o pedido'));
		}

	}

	private function enviaEmailConfirmacaoPedido($pedido_id, $email_usuario){
		
		$dados = $this->opM->verificaOpFechadaPorPedido($pedido_id);

		if( $dados['total'] > 0)
		{
			return false;

		}else{

			$ops = $this->opM->getOpsByPedido($pedido_id);

			$email = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title>teste</title><style type="text/css">
        /* Resets */
        .ReadMsgBody { width: 100%; background-color: #ebebeb;}
        .ExternalClass {width: 100%; background-color: #ebebeb;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        a[x-apple-data-detectors]{
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
        }        
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        body {margin:0; padding:0;}
        .yshortcuts a {border-bottom: none !important;}
        .rnb-del-min-width{ min-width: 0 !important; }

        /* Add new outlook css start */
        .templateContainer{
            max-width:590px !important;
            width:auto !important;
        }
        /* Add new outlook css end */

        /* Image width by default for 3 columns */
        img[class="rnb-col-3-img"] {
        max-width:170px;
        }

        /* Image width by default for 2 columns */
        img[class="rnb-col-2-img"] {
        max-width:264px;
        }

        /* Image width by default for 2 columns aside small size */
        img[class="rnb-col-2-img-side-xs"] {
        max-width:180px;
        }

        /* Image width by default for 2 columns aside big size */
        img[class="rnb-col-2-img-side-xl"] {
        max-width:350px;
        }

        /* Image width by default for 1 column */
        img[class="rnb-col-1-img"] {
        max-width:550px;
        }

        /* Image width by default for header */
        img[class="rnb-header-img"] {
        max-width:590px;
        }

        /* Ckeditor line-height spacing */
        .rnb-force-col p, ul, ol{margin:0px!important;}
        .rnb-del-min-width p, ul, ol{margin:0px!important;}

        /* tmpl-2 preview */
        .rnb-tmpl-width{ width:100%!important;}

        /* tmpl-11 preview */
        .rnb-social-width{padding-right:15px!important;}

        /* tmpl-11 preview */
        .rnb-social-align{float:right!important;}

        @media only screen and (min-width:590px){
        /* mac fix width */
        .templateContainer{width:590px !important;}
        }

        @media screen and (max-width: 360px){
        /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
        .rnb-yahoo-width{ width:360px !important;}
        }

        @media screen and (max-width: 380px){
        /* fix width and font size "tmpl-4 tmpl-6" in mobile preview */
        .element-img-text{ font-size:24px !important;}
        .element-img-text2{ width:230px !important;}
        .content-img-text-tmpl-6{ font-size:24px !important;}
        .content-img-text2-tmpl-6{ width:220px !important;}
        }

        @media screen and (max-width: 480px) {
        td[class="rnb-container-padding"] {
        padding-left: 10px !important;
        padding-right: 10px !important;
        }

        /* force container nav to (horizontal) blocks */
        td.rnb-force-nav {
        display: inherit;
        }
        }

        @media only screen and (max-width: 600px) {

        /* center the address &amp; social icons */
        .rnb-text-center {text-align:center !important;}

        /* force container columns to (horizontal) blocks */
        td.rnb-force-col {
        display: block;
        padding-right: 0 !important;
        padding-left: 0 !important;
        width:100%;
        }

        table.rnb-container {
         width: 100% !important;
        }

        table.rnb-btn-col-content {
        width: 100% !important;
        }
        table.rnb-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-last-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table[class~="rnb-col-2"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-col-2-noborder-onright {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        }

        table.rnb-col-2-noborder-onleft {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-top: 10px;
        padding-top: 10px;
        }

        table.rnb-last-col-2 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table.rnb-col-1 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        img.rnb-col-3-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xs {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xl {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-1-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-header-img {
        /**max-width:none !important;**/
        width:100% !important;
        margin:0 auto;
        }

        img.rnb-logo-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        td.rnb-mbl-float-none {
        float:inherit !important;
        }

        .img-block-center{text-align:center !important;}

        .logo-img-center
        {
            float:inherit !important;
        }

        /* tmpl-11 preview */
        .rnb-social-align{margin:0 auto !important; float:inherit !important;}

        /* tmpl-11 preview */
        .rnb-social-center{display:inline-block;}

        /* tmpl-11 preview */
        .social-text-spacing{margin-bottom:0px !important; padding-bottom:0px !important;}

        /* tmpl-11 preview */
        .social-text-spacing2{padding-top:15px !important;}

    }</style><!--[if gte mso 11]><style type="text/css">table{border-spacing: 0; }table td {border-collapse: separate;}</style><![endif]--><!--[if !mso]><!--><style type="text/css">table{border-spacing: 0;} table td {border-collapse: collapse;}</style> <!--<![endif]--><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body>

<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#f9fafc" style="background-color:#f9fafc;">

    <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr>
    <tr>
        <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="590" style="width:590px;">
                        <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
        <tbody><tr>

        <td align="center" valign="top" bgcolor="#f9fafc" style="background-color:#f9fafc;">

            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px; background-color:#f9fafc;" name="Layout_0" id="Layout_0">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" bgcolor="#f9fafc" style="min-width:590px; background-color:#f9fafc;">
                        <table width="100%" cellpadding="0" border="0" height="30" cellspacing="0" bgcolor="#f9fafc" style="background-color:#f9fafc;">
                            <tbody><tr>
                                <td valign="top" height="30">
                                    <img width="20" height="30" style="display:block; max-height:30px; max-width:20px;" alt="" src="http://img.mailinblue.com/new_images/rnb/rnb_space.gif">
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr><tr>

        <td align="center" valign="top" bgcolor="#f9fafc" style="background-color:#f9fafc;">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#f9fafc" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px; background-color:#f9fafc;" name="Layout_5" id="Layout_5">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color:#ffffff;">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius: 0px; width:590;;max-width:698px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/bombaHeader.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top" bgcolor="#f9fafc" style="background-color:#f9fafc;">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#f9fafc" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px; background-color:#f9fafc;" name="Layout_6" id="Layout_6">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color:#ffffff;">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:700px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/upApp/'.$ops[count($ops)-1]['foto'].'">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
    	</div></td>
    </tr><tr>

        <td align="center" valign="top" bgcolor="#f9fafc" style="background-color:#f9fafc;">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#f9fafc" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px; background-color:#f9fafc;" name="Layout_7" id="Layout_7">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color:#ffffff;">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:700px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/bombaFooter.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        </td>
        </tr>
        </tbody></table>

</body></html>';

				$this->load->library('email');

				$result = $this->email
							    ->from('webmaster@companytec.com.br')
							    ->reply_to('webmaster@companytec.com.br')
							    ->to($email_usuario)
						    	->subject('Wertco - Seu pedido está pronto!')
						    	->message($email)
						    	->send();
			return $result;
		}
	
	}

	private function enviaEmailProducao($email_destino,$tipo,$id){
				
		$ops = $this->opM->getOpsByPedido($id);

		$email='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title>tst</title><style type="text/css">.ReadMsgBody{width:100%;background-color:#ebebeb}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}body{-webkit-text-size-adjust:none;-ms-text-size-adjust:none;margin:0;padding:0}.yshortcuts a{border-bottom:none!important}.rnb-del-min-width{min-width:0!important}.templateContainer{max-width:590px!important;width:auto!important}img[class=rnb-col-3-img]{max-width:170px}img[class=rnb-col-2-img]{max-width:264px}img[class=rnb-col-2-img-side-xs]{max-width:180px}img[class=rnb-col-2-img-side-xl]{max-width:350px}img[class=rnb-col-1-img]{max-width:550px}img[class=rnb-header-img]{max-width:590px}.rnb-del-min-width p,.rnb-force-col p,ol,ul{margin:0!important}.rnb-tmpl-width{width:100%!important}.rnb-social-width{padding-right:15px!important}.rnb-social-align{float:right!important}@media only screen and (min-width:590px){.templateContainer{width:590px!important}}@media screen and (max-width:360px){.rnb-yahoo-width{width:360px!important}}@media screen and (max-width:380px){.content-img-text-tmpl-6,.element-img-text{font-size:24px!important}.element-img-text2{width:230px!important}.content-img-text2-tmpl-6{width:220px!important}}@media screen and (max-width:480px){td[class=rnb-container-padding]{padding-left:10px!important;padding-right:10px!important}td.rnb-force-nav{display:inherit}}@media only screen and (max-width:600px){table.rnb-col-1,table.rnb-col-2-noborder-onleft,table.rnb-col-3,table.rnb-last-col-2,table.rnb-last-col-3{float:none!important}.img-block-center,.rnb-text-center{text-align:center!important}td.rnb-force-col{display:block;padding-right:0!important;padding-left:0!important;width:100%}img.rnb-col-1-img,img.rnb-col-2-img,img.rnb-col-2-img-side-xl,img.rnb-col-2-img-side-xs,img.rnb-col-3-img,img.rnb-header-img,img.rnb-logo-img,table.rnb-btn-col-content,table.rnb-col-1,table.rnb-col-2-noborder-onleft,table.rnb-col-3,table.rnb-container,table.rnb-last-col-2,table.rnb-last-col-3{width:100%!important}table.rnb-col-3{margin-bottom:10px;padding-bottom:10px}table.rnb-col-2-noborder-onright,table[class~=rnb-col-2]{float:none!important;width:100%!important;margin-bottom:10px;padding-bottom:10px}table.rnb-col-2-noborder-onleft{margin-top:10px;padding-top:10px}.logo-img-center,.rnb-social-align,td.rnb-mbl-float-none{float:inherit!important}img.rnb-header-img{margin:0 auto}.rnb-social-align{margin:0 auto!important}.rnb-social-center{display:inline-block}.social-text-spacing{margin-bottom:0!important;padding-bottom:0!important}.social-text-spacing2{padding-top:15px!important}}</style><!--[ifgtemso11]><style type="text/css">table,</style><![endif]--><!--[if!mso]><!--><style type="text/css">table{border-spacing:0}table td{border-collapse:collapse}</style> <!--<![endif]--><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body>
<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#f9fafc" style="background-color: rgb(249, 250, 252);">
    <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr>
    <tr>
        <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="590" style="width:590px;">
                        <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
        <tbody><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_5" id="Layout_5">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:600px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/backgroundCabecalhoOps.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">
            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_6">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 0px; padding-right: 0px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px">
	<tbody>
					<tr>
						<td style="padding-bottom: 18px;font-weight: 700;">Pedido:</td>
						<td style="padding-bottom: 18px;">'.$ops[0]['pedido_id'].'</td>
						
					</tr>
					<tr>
						<td style="padding-bottom: 18px;font-weight: 700;">Cliente:</td>
						<td style="padding-bottom: 18px;">'.$ops[0]['nome_cliente'].'</td>						
					</tr>					
				</tbody>
</table>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_7" id="Layout_7">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffcc00" align="center" cellspacing="0" style="background-color: rgb(255, 204, 0);">
                            <tbody><tr>
                                <td valign="top" align="left">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:296;;max-width:296px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="296" class="rnb-header-img" alt="" style="display: block;float: left;border-radius: 0px;margin: 0 15px; " src="http://www.wertco.com.br/email-cliente/Ordens_de_producao.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_9">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 0px; padding-right: 0px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><table cellspacing="0" style="width: 570px;margin-left: 14px;padding-bottom: 25px">
	<thead style="height: 55px">
		<tr>
			<th style="height: 30px">Nr.Op</th>
			<th style="text-align: center;">Modelo - Descrição</th>
			<th>Qtd</th>
		</tr>
	</thead>
	<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;" bgcolor="#ffffff">';
		$total = 0; foreach($ops as $op){
			
			$email.='	<tr>
						<td style="border-right: 1px solid #f0f0f0;    padding: 10px;">'.$op['id'].'</td>
						<td style="border-right: 1px solid #f0f0f0;    padding: 10px;">'.$op['modelo'].' - '.$op['descricao'].'</td>
						<td style="text-align: center;border-right: 1px solid #f0f0f0;   padding: 10px;">'.$op['qtd'].'</td>						
					</tr>';					
			
		}			
		$email.='	</tbody>
</table>
</td>
	</tr>
	</tbody>
	</table>
 	</td></tr>
 		</tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_4" id="Layout_4">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="590" class="rnb-container rnb-yahoo-width" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#f9fafc" style="padding-right: 20px; padding-left: 20px; background-color: rgb(249, 250, 252);">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-size:14px; color:#888888; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
                                    <div>© 2018 wertco</div>
                                </td></tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        </td>
        </tr>
        </tbody></table>

</body></html>';
		$this->load->library('email');

		if( $ops[0]['upload_bb'] != 'NULL'){
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject('⛽ Ordens de produção | pedido: '.$id.' ⛽')
			    ->message($email)
			    ->attach('./pedidos/'.$ops[0]['upload_bb'])
			    ->send();
		}else{
		
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject('⛽ Ordens de produção | pedido: '.$id.' ⛽')
			    ->message($email)			    
			    ->send();
		}

		return $result;
			
	}
	
	public function ops()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->opM->selectOps();				
		$parametros['title']	=	"Ops";
		$this->_load_view('area-administrador/ops/ops',$parametros);
	}
	
	public function cadastraOps()
	{

		if($this->input->post('salvar') == 1){
			
			$descricao = explode('|', 	$this->input->post('itens'));
			$insereOp = array(	'item_id' 			=> 	$this->input->post('item_id'),
								'pedido_id' 		=> 	0,
								'status_op_id' 		=> 	1,
								'observacao_manual' => 	$this->input->post('observacoes_manual'),
								'modelo' 			=>  'Nenhum',
								'qtd'				=> 	$this->input->post('qtd'),
								'nome_cliente'		=> 	'WERTCO IND. COM. E SERVIÇOS DE MANUTENÇÃO EM BOMBAS LTDA. | 27.314.980/0001-53',
								'cliente_id'		=> 	 2,
								'cidade'			=> 	'Arujá',
								'uf'				=> 	'SP',
								'pais'				=> 	'Brasil',
								'codigo'			=> 	'S/Código',
								'descricao'			=> 	$descricao[count($descricao)-1],
								'combustivel'		=> 	'Nenhum',
								'observacao' 		=> 	'Ordem de Produção Avulsa, produza o ítem: '.$descricao[count($descricao)-1]);
			
			if( $this->opM->insereOp($insereOp) ){
				
				$this->session->set_flashdata('sucesso', 'ok');
				
			}else{
				$this->session->set_flashdata('erro', 'erro');
			}
			
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->opM->selectOps();				
			$parametros['title']	=	"Ops";
			$this->_load_view('area-administrador/ops/ops',$parametros);
		}else{
			$parametros 			= 	$this->session->userdata();			
			$parametros['title']	=	"Cadastro de Ops";
			$this->_load_view('area-administrador/ops/cadastra-ops',$parametros);
		}
		
	}
	
	public function editaOps($id)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['op']		=	$this->opM->find($id);				
		$parametros['title']	=	"Edição de Ops";
		$this->_load_view('area-administrador/ops/edita-ops',$parametros);
	}
	
	public function editarOps()
	{
		$op = $this->input->post();
		if($this->opM->atualizarOrdemProducao($op)){
			$this->session->set_flashdata('success', 'ok.');	
		}else{
			$this->session->set_flashdata('erro', 'erro.');
		}
		redirect('areaAdministrador/ops');
	}
	
	public function excluirOps()
	{
		$id 	= 	$this->input->post('id');
		$motivo = 	$this->input->post('motivo');	
		if($this->opM->excluirOrdemProducao($this->input->post('id'),$motivo ) ){
			echo json_encode(array(	'status' 	=> 	'success',
									'titulo' 	=> 	'OP excluida',
									'mensagem' 	=> 	'OP excluida com sucesso')	);
		}else{
			echo json_encode(array(	'status' 	=> 	'error',
									'titulo' 	=> 	'OP não excluida',
									'mensagem' 	=> 	'Não foi possível excluir a OP'));	
		}
	}
	
	public function insereItensSubitens(){
		
		$produto = array('descricao' 		=> 	$_POST['item_desc'],
						'tipo_produto_id'	=>	5,
						'codigo' 			=>  '',
						'modelo'            =>  '',						
						'valor_unitario'    =>  '0.00',
						'modelo_tecnico'	=> '',
						'vazao'				=> '',
						'nr_rotativa'		=> 0);
		
		$this->produtosM->insereProduto($produto);		
		$produto_id = $this->db->insert_id();
		
		$item = array(	'produto_id' 	=> $produto_id,
						'descricao'		=> $_POST['item_desc']);
		
		$this->itensM->insereItens($item);		
		$item_pai = $this->db->insert_id();
		$erro = 0;
				
		$sub_descricao 		= 	json_decode(stripslashes($_POST['sub_descricao']));
		$subitens_item_id 	= 	json_decode(stripslashes($_POST['subitens_item_id']));
		$sub_qtd 			= 	json_decode(stripslashes($_POST['sub_qtd']));

		for($i=0;$i<count($sub_descricao);$i++){
			if($sub_descricao[$i] != ''){
				$insert = array( 'produto_id'	=> 	$sub_descricao[$i],
								 'item_id'		=>	$item_pai,
								 'item_filho'	=>  $subitens_item_id[$i],
								 'qtd'			=> 	$sub_qtd[$i]);
				
				if(!$this->itensM->insereSubItens($insert)){
					$erro++;	
				}
			}
				
		}
		
		if( $erro == 0 ){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
			
		}
	}
	
	

	public function chamados()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->chamadoM->select();	
		$parametros['title']	=	"Chamados";
		$this->_load_view('area-administrador/assistencia-tecnica/chamados',$parametros);
	}

	public function cadastraChamado()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['tipos']		=	$this->chamadoM->selectTipos();
		$parametros['prioridades']		=	$this->chamadoM->selectPrioridades();
		$parametros['title']	=	"Cadastro de Chamado";
		$this->_load_view('area-administrador/assistencia-tecnica/cadastra-chamado',$parametros);
	}
	
	public function editaChamado($id = null, $atividade_id = null)
	{
		$parametros 			=	$this->session->userdata();
		$parametros['chamado'] 	=	$this->chamadoM->find($id);
		if($parametros['chamado']) {
			$parametros['tipos']			=	$this->chamadoM->selectTipos();
			$parametros['status']			=	$this->chamadoM->selectStatus();
			$parametros['prioridades']		=	$this->chamadoM->selectPrioridades();
			$parametros['atividades']		=	$this->atividadeM->select($id);
			$parametros['atividade_status']	=	$this->atividadeM->selectStatus();
			$parametros['defeitos']			=	$this->defeitoM->select();
			$parametros['modelos']			=	$this->atividadeM->selectModelos($parametros['chamado']['cliente_id']);
			$parametros['nr_series']		= 	$this->nrSerieM->buscaNrSeriePorCliente($parametros['chamado']['cliente_id']);
			$parametros['despesas']	 		= 	$this->despesasM->getDespesas();
			$parametros['ordem_pagamento']	= 	$this->ordemPagtoM->getOpsByChamado($id);
			$parametros['tecnicos']			=	$this->chamadoM->listaTecnicosAtendimentos($id);
			$parametros['solicitacao_pecas']= 	$this->solicitacaoPecasM->buscaSolicitacao($id);
			$parametros['cliente']			= 	$this->chamadoM->buscaClienteChamado($id);
			$parametros['autoriz_servico'] 	= 	$this->autorizServicoM->buscaSolicitacaoServico($id);
			$parametros['total_aceite']		= 	$this->autorizServicoM->buscaTotalAceite($id);
			$parametros['endereco_entrega'] = 	$this->chamadoM->buscaEnderecoEntregaPecas($id);			
			$parametros['title']			=	"Edição de Chamado";
			$parametros['ops_enviadas']		= 	$this->ordemPagtoM->listaHistoricoOrdemPagto($id);
			$this->_load_view('area-administrador/assistencia-tecnica/edita-chamado',$parametros);
		} else {

			redirect('areaAdministrador/cadastraChamado');
		}
	}
	
	public function editaAtividadeWS()
	{
		$atividade_id = $this->input->post('atividade_id');
		$atividade = $this->atividadeM->find($atividade_id);
		$atividade['causas_solucoes'] = $this->causa_solucaoM->select($atividade_id);
		$atividade['subatividades'] = $this->subatividadeM->select($atividade_id);
		$atividade['defeitos'] = $this->defeitoM->select();
		$atividade['causas'] = $this->causaM->select($atividade['defeito_id']);
		if(count($atividade['causas']) > 0){
			$atividade['solucoes'] = $this->solucaoM->select($atividade['causas'][0]['id']);
		} else {
			$atividade['causas'] = array();
			$atividade['solucoes'] = array();
		}
		echo json_encode($atividade);
	}
	
	public function editaSubAtividadeWS()
	{
		$subatividade_id = $this->input->post('subatividade_id');
		$subatividades = $this->subatividadeM->find($subatividade_id);
		echo json_encode($subatividades);
	}
	
	public function listarCausasWS()
	{
		$causas = $this->causaM->select($this->input->post('defeito_id'));
		echo json_encode($causas);
	}
	
	public function listarSolucoesWS()
	{
		$solucoes = $this->solucaoM->select($this->input->post('causa_id'));
		echo json_encode($solucoes);
	}
	
	public function retornaParceiros()
	{	
		$rows = $this->empresasM->retornaParceiros($_GET['term']);
			
		if(count($rows) > 0){
			echo json_encode($rows);
		}

	}

	public function retornaParceirosCartao()
	{
		$rows = $this->usuariosM->retornaParceirosCartao($_GET['term']);
			
		if(count($rows) > 0){
			echo json_encode($rows);
		}
		
	}

	public function retornaClienteWS()
	{
		$rows = $this->usuariosM->retornaClientes($_GET['term']);
		if(count($rows) > 0){
			echo json_encode($rows);
		}
	}	

	public function retornaUsuariosEmpresaWS()
	{
		
		$rows = $this->usuariosM->getTodosUsuariosEmpresa($_POST['empresa_id']);
		$dados = array();
		
		foreach ($rows as $row) {
			$dados[] = array(	'label'		=>	mb_strtoupper($row['nome']),
								'id'		=>	$row['id'],
								'validade'  =>  $row['validade']);		
		}
			
		echo json_encode($dados);
	}	


	public function verificaOpsPedido(){

		$rows = $this->pedidosM->buscaOpsPorPedido($_POST['pedido_id']);		
		echo json_encode($rows);

	}

	public function anexarNfe(){
		
		if($_FILES['nfe']['name'] != 'NULL' && $_FILES['nfe']['name'] != '' ){

			$tipo 			= 	explode('.', $_FILES['nfe']['name']);
			$arquivo 		=	md5($_FILES['nfe']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
			
			$configuracao = array(
		        'upload_path'   	=> 	'./nfe/',		        
		        'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv',
		        'file_name'     	=> 	$arquivo
		    );      

		    $this->load->library('upload', $configuracao);
		    
		    if ($this->upload->do_upload('nfe')){

		    	$data = explode('/', $this->input->post('dt_emissao_nf'));

		    	$update = array( 	'id' 				=> 	$this->input->post('pedido_id'),
		    						'nr_nf' 			=>	$this->input->post('nr_nf'),
		    						'arquivo_nfe' 		=> 	$arquivo,
		    						'prazo_entrega'		=> 	$this->input->post('prazo_entrega'),
		    						'status_pedido_id'	=> 	5,
		    						'dt_emissao_nf'		=> 	$data[2].'-'.$data[1].'-'.$data[0]	);
		    
		    	if( $this->pedidosM->atualizaPedidos($update) ){
		    		$atualizaGarantia = array(	'pedido_id' 			=> 	$this->input->post('pedido_id'),
		    									'dt_inicio_garantia'	=> 	$update['dt_emissao_nf'],
		    									'dt_fim_garantia'		=> 	date("Y-m-d", strtotime($update['dt_emissao_nf']." +24 month")));
		    		$this->nrSerieM->atualizaGarantia($atualizaGarantia);
		    		
		    		$this->insereAndamentoPedido( $this->input->post('pedido_id'), 5 );
		    		
		    		$email='<html>
						<head></head>
						<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
							<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
								<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Nota fiscal Vinculada ao Pedido</h1>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
								Nota Fiscal: '.$this->input->post('nr_nf').'<br/>
								cliente: 	'.$this->input->post('cliente').'<br/> 	
								Pedido: '.$this->input->post('pedido_id').'<br/>
								Prazo de entrega: '.$this->input->post('prazo_entrega').'<br/>
								</p>
							</div>
						</body>
					</html>';

		    		if( $this->enviaEmail('industrial@wertco.com.br','⛽ Nota Fiscal Vinculada ao Pedido:'.$this->input->post('pedido_id'), $email,'./nfe/'.$arquivo) ){
		    			$this->session->set_flashdata('sucesso', 'ok'); 
		    		}else{
		    			$this->session->set_flashdata('erro', 'erro');
		    		}
		    	}else{
		    		$this->session->set_flashdata('erro', 'erro');
		    	}
		    }else{
		    	$this->session->set_flashdata('erro', 'erro');
		    }
		}else{
			
			$data = explode('/', $this->input->post('dt_emissao_nf'));

	    	$update = array( 	'id' 				=> 	$this->input->post('pedido_id'),
	    						'nr_nf' 			=>	$this->input->post('nr_nf'),	    						
	    						'prazo_entrega'		=> 	$this->input->post('prazo_entrega'),   						
	    						'dt_emissao_nf'		=> 	$data[2].'-'.$data[1].'-'.$data[0]	);
	    
	    	if( $this->pedidosM->atualizaPedidos($update) ){
	    		$this->session->set_flashdata('sucesso', 'ok'); 
	    	}else{
	    		$this->session->set_flashdata('erro', 'erro');	
	    	}
		}

	    $this->pedidos();

	}

	public function buscaNfePedido()
	{
		echo json_encode($this->pedidosM->getPedidosById($this->input->post('pedido_id')));
	}

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->send();
		}else{
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)
			    ->send();
		}

		return $result;		
	}
		
	public function inserirChamado()
	{
		$chamado = $this->input->post();
		$chamado['status_id'] = 1;
		if(isset($chamado['cliente_id'])){
			unset($chamado['tipo_cadastro_id']);
			if($chamado['cliente_id'] != '' && $chamado['contato_id'] != ''){
				if( $chamado['fl_inadimplencia'] == '1' && $chamado['tipo_id'] = '1' ){
					$this->enviaEmailAlerta($chamado['cliente']);
				}
				unset($chamado['cliente']);
				unset($chamado['fl_inadimplencia']);
				if($id = $this->chamadoM->insert($chamado)){
					redirect('areaAdministrador/editaChamado/'.$id);	
				}
			}
		}
		$this->session->set_flashdata('erro', true);
		redirect('areaAdministrador/cadastraChamado');	
	}
	
	public function editarChamado()
	{
		$chamado = $this->input->post();
		if(isset($chamado['id'])){

			if($chamado['status_id'] == 3 || $chamado['status_id'] == 4){
				$chamado['fim'] = date('Y-m-d H:i:s');
			}
			
			$edicao = $this->chamadoM->update($chamado);
			if($edicao > 0){
				$this->session->set_flashdata('sucesso', true);
				if($_FILES['anexos_chamados']['name'][0] != "") {

					$uploads_realizados = $this->uploadMultiplo($_FILES['anexos_chamados'],'./chamados_atividades/');
			
					foreach ($uploads_realizados as $upload) {

						$dadosInsert = 	array(	'descricao' 	=> $upload['nome'],
												'chamado_id' 	=> $chamado['id'],
												'arquivo'		=> $upload['nome'] 	);

						if(! $this->chamadoM->insertAnexo($dadosInsert) ){
							
						}

					}
				}
			} else if($edicao == 0){
				$erro = 0;
				if($_FILES['anexos_chamados']['name'][0] != "") {

					$uploads_realizados = $this->uploadMultiplo($_FILES['anexos_chamados'],'./chamados_atividades/');
					$erro = 0;
					if($uploads_realizados){
						foreach ($uploads_realizados as $upload) {

							$dadosInsert = 	array(	'descricao' 	=> $upload['nome'],
													'chamado_id' 	=> $chamado['id'],
													'arquivo'		=> $upload['nome'] 	);

							if(! $this->chamadoM->insertAnexo($dadosInsert) ){
								$erro = 1;		
							}

						}
					}else{
						$erro=1;						
					}
				}

				if($erro == 0){
					$this->session->set_flashdata('sucesso', true);
				}else{
					$this->session->set_flashdata('erro', true);
				}

			}  else {
				$this->session->set_flashdata('erro', true);
			}
			redirect('areaAdministrador/editaChamado/'.$chamado['id']);
		}
		$this->session->set_flashdata('erro', true);
		redirect('areaAdministrador/cadastraChamado');	
		
	}
	
	
	public function inserirAtividade()
	{
		$atividade = $this->input->post();
		$atividade['status_id'] = 1;
		if(isset($atividade['chamado_id'])){
			if($id = $this->atividadeM->insert($atividade)){
				$this->session->set_flashdata('sucesso_atividade', true);
				$this->session->set_flashdata('atividade_id', $id);
				redirect('areaAdministrador/editaChamado/'.$atividade['chamado_id']);	
			}
			$this->session->set_flashdata('erro_atividade', true);
			redirect('areaAdministrador/editaChamado/'.$atividade['chamado_id']);
		}
		$this->session->set_flashdata('erro_atividade', true);
		redirect('areaAdministrador/cadastraChamado');
	}
	
	
	
	public function editarAtividade()
	{
		$atividade = $this->input->post();
		unset($atividade['tabela_subatividades_length']);
		if(isset($atividade['id'])){
			$escape = false;
			if($atividade['status_id'] == 3 || $atividade['status_id'] == 4){
				$atividade['fim'] = date('Y-m-d H:i:s');
				if(count($this->causa_solucaoM->select($atividade['id'])) == 0){
					$escape = true;
				}
			}
			if($escape){
				$edicao = -1;
			} else {
				$edicao = $this->atividadeM->update($atividade);
			}

			if($edicao > 0){
				$this->session->set_flashdata('atividade_id',$atividade['id'] );
				$this->session->set_flashdata('edit_sucesso_atividade', true);
			} else if($edicao == 0){
				$this->session->set_flashdata('edit_nenhum_atividade', true);
			}  else {
				$this->session->set_flashdata('edit_erro_atividade', true);
			}
			redirect('areaAdministrador/editaChamado/'.$atividade['chamado_id']);
		}
		$this->session->set_flashdata('edit_erro_atividade', true);
		redirect('areaAdministrador/cadastraChamado');
		
	}
		
	public function inserirSubAtividade()
	{
		$subatividade = $this->input->post();
		if(isset($subatividade['contato_id'])){
			unset($subatividade['tecnico']);
			$chamado_id = $subatividade['chamado_id'];
			unset($subatividade['chamado_id']);
			$subatividade['fim'] = date('Y-m-d H:i:s');
			if($id = $this->subatividadeM->insert($subatividade)){
				$this->session->set_flashdata('sucesso_subatividade', true);
				$this->session->set_flashdata('atividade_id', $subatividade['atividade_id']);
				redirect('areaAdministrador/editaChamado/'.$chamado_id);	
			}
			$this->session->set_flashdata('erro_subatividade', true);
			redirect('areaAdministrador/editaChamado/'.$chamado_id);
		}
		$this->session->set_flashdata('erro_subatividade', true);
		redirect('areaAdministrador/cadastraChamado');
	}
	
	public function editarSubAtividade()
	{
		$subatividade = $this->input->post();
		if(isset($subatividade['contato_id'])){
			unset($subatividade['tecnico']);
			$chamado_id = $subatividade['chamado_id'];
			unset($subatividade['chamado_id']);
			$subatividade['fim'] = date('Y-m-d H:i:s');
			$edicao = $this->subatividadeM->update($subatividade);
			if($edicao > 0){
				$this->session->set_flashdata('atividade_id', $subatividade['atividade_id']);
				$this->session->set_flashdata('edit_sucesso_subatividade', true);
			} else if($edicao == 0){
				$this->session->set_flashdata('atividade_id', $subatividade['atividade_id']);
				$this->session->set_flashdata('edit_nenhum_subatividade', true);
			}  else {

				$this->session->set_flashdata('edit_erro_subatividade', true);
			}
			redirect('areaAdministrador/editaChamado/'.$chamado_id);	
		}
		$this->session->set_flashdata('edit_erro_subatividade', true);
		redirect('areaAdministrador/cadastraChamado');	
	}
	
	public function inserirDefeitoWS()
	{
		$defeito = $this->defeitoM->findByDescricao($this->input->post('descricao'));
		if($defeito){
			echo false;
		} else {
			$defeito = $this->input->post();
			$defeito_id = $this->defeitoM->insert($defeito);
			if($defeito_id) {
				echo $defeito_id;
			} else {
				echo false;
			}
		}
	}
	
	public function inserirCausaWS()
	{
		
		$causa = $this->causaM->findByDescricao($this->input->post('defeito_id'), $this->input->post('descricao'));
		if($causa){
			echo false;
		} else {
			$causa = $this->input->post();
			$causa_id = $this->causaM->insert($causa);
			if($causa_id) {
				echo $causa_id;
			} else {
				echo false;
			}
		}
	}
	
	public function inserirSolucaoWS()
	{
	
		$solucao = $this->causaM->findByDescricao($this->input->post('causa_id'), $this->input->post('descricao'));
		if($solucao){
			echo false;
		} else {
			$solucao = $this->input->post();
			$solucao_id = $this->solucaoM->insert($solucao);
			if($solucao_id) {
				echo $solucao_id;
			} else {
				echo false;
			}
		}
	}
	
	public function inserirCausaSolucaoWS()
	{
		$causa_solucao = $this->causa_solucaoM->selectDuplicate($this->input->post('causa_id'),$this->input->post('solucao_id'), $this->input->post('atividade_id') );
		
		if($causa_solucao){
			$causa_solucao['status'] = 1;
			//var_dump($causa_solucao);die;
			if( $this->causa_solucaoM->update($causa_solucao) ) {
			
				$updateAtividade = 	array(	'id' 			=> 	$this->input->post('atividade_id'),
											'defeito_id'	=>  $this->input->post('defeito_id'));
			
				if( $this->atividadeM->update($updateAtividade) ){
					echo json_encode('true');
				} else {
				
				echo json_encode('false1');
				}

			} else {
				
				echo json_encode('false2');
			}
		}  else {
			
			$updateAtividade = 	array(	'id' 			=> 	$this->input->post('atividade_id'),
										'defeito_id'	=>  $this->input->post('defeito_id'));
			
			
			if( $this->atividadeM->update($updateAtividade) ){

				$causa_solucao = $this->input->post();	
				unset($causa_solucao['defeito_id']);
				
				$causa_solucao_id = $this->causa_solucaoM->insert($causa_solucao);				
				if($causa_solucao_id > 0) {
					
					echo json_encode($causa_solucao_id);
				} else {
					
					echo json_encode('false3');
				}
			}else{
				echo json_encode('false4');
			}
		}
	}
	
	public function removerCausaSolucaoWS()
	{
		$causa_solucao = $this->input->post();
		$causa_solucao['status'] = 0;
		if($edicao = $this->causa_solucaoM->update($causa_solucao)) {
			echo $edicao;
		} else {
			echo false;
		}

	}
	
	public function configuracaoEmpresa($id = null)
	{
		if($id != null) {
			$parametros 			= 	$this->session->userdata();
			$parametros['empresa']	=	$this->empresasM->getEmpresa($id)[0];
			$parametros['configuracao']	=	$this->configuracaoM->findByEmpresa($id);
			$parametros['configuracao_bombas']	=	$this->configuracao_bombaM->selectByEmpresa($id);
			$parametros['concentradores']	=	$this->configuracao_concentradorM->select();
			$parametros['identificadores']	=	$this->configuracao_identificadorM->select();
			$parametros['bombas']		=	$this->configuracao_bombaM->selectBombas($id);
			$parametros['title']	=	"Configuração";
			$this->_load_view('area-administrador/empresas/configuracao-empresa',$parametros);
		} else {
			redirect('areaAdministrador/gestaoEmpresas');
		}
	}
	
	public function salvarConfiguracaoWS()
	{
		$configuracao = $this->input->post();
		if($this->configuracaoM->save($configuracao)){
			echo json_encode(array("titulo" => "Configuração salva",
								   "texto" => "A configuração foi salva com sucesso",
								   "tipo" => "success"));
		}  else {
			echo json_encode(array("titulo" => "Configuração não salva",
								   "texto" => "Não foi possível salvar a configuração",
								   "tipo" => "error"));
		}
	}
	
	public function salvarConfiguracaoBombaWS()
	{
		$configuracao_bomba = $this->input->post();
		$configuracao_bomba_id = $this->configuracao_bombaM->save($configuracao_bomba);
		if($configuracao_bomba_id){
			echo json_encode(array("titulo" => "Configuração salva",
								   "texto" => "A configuração foi salva com sucesso",
								   "tipo" => "success",
								   "id" => $configuracao_bomba_id));
		}  else {
			echo json_encode(array("titulo" => "Configuração não salva",
								   "texto" => "Não foi possível salvar a configuração",
								   "tipo" => "error"));
		}
	}
	
	public function removerConfiguracaoBombaWS()
	{
		$configuracao_bomba = $this->input->post();
		if($this->configuracao_bombaM->delete($configuracao_bomba)){
			echo json_encode(array("titulo" => "Configuração removida",
								   "texto" => "A configuração foi removida com sucesso",
								   "tipo" => "success"));
		}  else {
			echo json_encode(array("titulo" => "Configuração não removida",
								   "texto" => "Não foi possível removida a configuração",
								   "tipo" => "error"));
		}
	}
	
	public function inserirConcentradorWS()
	{
		$concentrador = $this->input->post();
		$concentrador_id = $this->configuracao_concentradorM->insert($concentrador);
		if($concentrador_id){
			echo json_encode(array("titulo" => "Modelo salvo",
								   "texto" => "O modelo de concentrador foi salvo com sucesso",
								   "tipo" => "success",
								   "id" => $concentrador_id));
		} else {
			echo json_encode(array("titulo" => "Modelo não salvo",
								   "texto" => "Não foi possível salvar o modelo do concentrador",
									"tipo" => "error"));
		}
	}
	
	public function inserirIdentificadorWS()
	{
		$identificador = $this->input->post();
		$identificador_id = $this->configuracao_identificadorM->insert($identificador);
		if($identificador_id){
			echo json_encode(array("titulo" => "Modelo salvo",
								   "texto" => "O modelo de identificador foi salvo com sucesso",
								   "tipo" => "success",
								   "id" => $identificador_id));
		} else {
			echo json_encode(array("titulo" => "Modelo não salvo",
								   "texto" => "Não foi possível salvar o modelo do identificador",
									"tipo" => "error"));
		}
	}

	public function uploadAtividades(){
		
		
		$uploads_realizados = $this->uploadMultiplo($_FILES['anexos_atividade'],'./chamados_atividades/');
		
		foreach ($uploads_realizados as $upload) {
			

			$dadosInsert = 	array(	'descricao' 	=> $upload['nome'],
									'atividade_id' 	=> $this->input->post('atividade_id'),
									'arquivo'		=> $upload['nome']);

			if(! $this->atividadeM->insertAnexo($dadosInsert) ){
				$this->session->set_flashdata('upload_atividade_erro', true);

				redirect('areaAdministrador/editaChamado/'.$this->input->post('chamado_id'));
			}

		}
		$this->session->set_flashdata('atividade_id', $this->input->post('atividade_id'));
		$this->session->set_flashdata('upload_atividade', true);
		redirect('areaAdministrador/editaChamado/'.$this->input->post('chamado_id'));
	}

	public function excluirAnexosAtividade(){

		if( $this->atividadeM->excluirAnexo( $this->input->post('id') ) ){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}

	public function excluirAnexosChamado(){

		if( $this->chamadoM->excluirAnexo( $this->input->post('id') ) ){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}

	private function uploadMultiplo($arquivos,$local){
		
		for ($i=0; $i < count($arquivos['name']); $i++) {
			
			if(is_array($arquivos['name'])){
				
				if( count($arquivos['name']) == 1 && array_key_exists('0',$arquivos['name']) ){
					$_FILES['file']['name']     =  	$arquivos['name'][0];
	            	$_FILES['file']['type']     =  	$arquivos['type'][0];
	            	$_FILES['file']['tmp_name'] =   $arquivos['tmp_name'][0];
	            	$_FILES['file']['error']    =   $arquivos['error'][0];
	            	$_FILES['file']['size']     =   $arquivos['size'][0];
            	
				}else{
					
					$_FILES['file']['name']     = (count($arquivos['name']) > 1) ? $arquivos['name'][$i] : $arquivos['name'];
		            $_FILES['file']['type']     = (count($arquivos['name']) > 1) ? $arquivos['type'][$i] : $arquivos['type'];
		            $_FILES['file']['tmp_name'] = (count($arquivos['name']) > 1) ? $arquivos['tmp_name'][$i] : $arquivos['tmp_name'];
		            $_FILES['file']['error']    = (count($arquivos['name']) > 1) ? $arquivos['error'][$i] : $arquivos['error'];
		            $_FILES['file']['size']     = (count($arquivos['name']) > 1) ? $arquivos['size'][$i] : $arquivos['size'];		

		        }
			}else{
				$_FILES['file']['name']     =  	$arquivos['name'];
            	$_FILES['file']['type']     =  	$arquivos['type'];
            	$_FILES['file']['tmp_name'] =   $arquivos['tmp_name'];
            	$_FILES['file']['error']    =   $arquivos['error'];
            	$_FILES['file']['size']     =   $arquivos['size'];
			}
			
            
			$arquivo =	str_replace(' ','',trim(preg_replace( "[^a-zA-Z0-9_]", "", strtr($_FILES['file']['name'], "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"))));

			$configuracao = array(
		        'upload_path'   	=> 	$local,
		        'allowed_types' 	=> 	'gif|jpg|jpeg|png|pdf|doc|docx|xls|csv|vnd.ms-outlook|msg|zip|rar|txt|log|html|htm|mp4|mp3|xlsx|docx|ogg',
		        'file_name'     	=> 	$arquivo,
		        'encrypt_name'		=> 	false
		    );			

		    $this->load->library('upload');

		    $this->upload->initialize($configuracao);
		    
		    if (!$this->upload->do_upload('file')){
		    	return false;
			}else{
		    	$upload[] = array( 	'nome' => $this->upload->data('file_name') 	);
			}

		}
		

	    return $upload;
	}

	public function listaAnexosAtividades(){

		$anexos = $this->atividadeM->listaAnexos($this->input->post('atividade_id'));
		echo json_encode($anexos);

	}

	public function listaAnexosChamado(){

		$anexos = $this->chamadoM->listaAnexos($this->input->post('chamado_id'));
		echo json_encode($anexos);

	}

	public function verificaStatusAtividade(){

		$retorno = $this->atividadeM->verificaStatusAtividade($_POST['chamado_id']);
		echo json_encode($retorno);
	}

	public function despesas() 
	{		
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->despesasM->getDespesas();		
		$parametros['title']	=	"Despesas";
		$this->_load_view('area-administrador/despesas/despesas',$parametros);
	}

	public function cadastraDespesa()
	{
		
		if($this->input->post('salvar') == 1){

			$insert = array(	'tipo_despesa_id'	=>	$this->input->post('tipo_despesa_id'),
								'valor'				=>	str_replace(",", ".", str_replace(".","", $this->input->post('valor'))),
								'descricao'			=> 	$this->input->post('descricao')	);
						 
			if($this->despesasM->insereDespesa($insert)){
				$this->log('Área Administrador | insere despesa','despesas','INSERÇÃO', $this->session->userdata('usuario_id'),$insert,$insert,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				$this->despesas();
			}else{
				$this->session->set_flashdata('erro', 'erro');
				$this->despesas();
			}

		}

		$parametros 					= 	$this->session->userdata();		
		$parametros['tipo_despesas']	=	$this->despesasM->getTipoDespesas();
		$parametros['title']			=	"Cadastro de Despesas";
		$this->_load_view('area-administrador/despesas/cadastra-despesa',$parametros);
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Despesa ******************************
	*****************************************************************************/
	public function excluirDespesa()
	{
		
		if($this->despesasM->excluirDespesa($_POST['id'])){
			$this->log('Área Administrador | excluir despesa','despsas','EXCLUSÃO', $this->session->userdata('usuario_id'),array('id' => $_POST['id'] ),array('id' => $_POST['id'] ),$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}
	
	public function editarDespesa($id){

		if($this->input->post('salvar') == 1){

			$update = array(	'id'				=>	$this->input->post('id'),
								'tipo_despesa_id'	=>	$this->input->post('tipo_despesa_id'),
								'valor'				=>	str_replace(",", ".", str_replace(".","", $this->input->post('valor'))),
								'descricao'			=>	$this->input->post('descricao')	);
						 
			if($this->despesasM->atualizaDespesa($update)){
				$this->log('Área Administrador | excluir despesa','despesa','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');				
				$this->despesas();

			}else{

				$this->session->set_flashdata('erro', 'erro');
				$this->despesas();
			}

		}else{

			$row 							= 	$this->despesasM->getDespesasById($id);
			$parametros 					= 	$this->session->userdata();
			$parametros['dados']			=	$row;
			$parametros['title']			=	"Editar Despesa";
			$parametros['tipo_despesas']	=	$this->despesasM->getTipoDespesas();
			$this->_load_view('area-administrador/despesas/editar-despesa',$parametros );
		}
		
	}

	public function ordemPagtoAssist()
	{
		if($this->input->post('salvar') == 1){
			
			if($this->input->post('ordem_pagamento_id') == 0){

				$insertOp = array(	'favorecido_id' => 	$this->input->post('favorecido_id'),
									'descricao'		=> 	$this->input->post('descricao'),
									'status_id'		=> 	1,
									'usuario_id'	=> 	$this->session->userdata('usuario_id'),
									'chamado_id'	=> 	$this->input->post('chamado_id')	);

				$sucesso = $this->ordemPagtoM->insereOp($insertOp);			
				
				if($sucesso) $ordem_pagamento_id = $this->db->insert_id();


			}else{
				$atu_valor = $this->ordemPagtoM->buscaValorTotal($this->input->post('ordem_pagamento_id'));

				$updateOp = array(	'id'			=> 	$this->input->post('ordem_pagamento_id'),
									'favorecido_id'	=> 	$this->input->post('favorecido_id'),
									'descricao'		=> 	$this->input->post('descricao'),
									'usuario_id'	=> 	$this->session->userdata('usuario_id')	);

				$sucesso = $this->ordemPagtoM->atualizarOrdemPagamento($updateOp);
				
				if($sucesso) $ordem_pagamento_id = $this->input->post('ordem_pagamento_id');

			}

			if( $sucesso ){

				$atu_valor = $this->ordemPagtoM->buscaValorTotal($ordem_pagamento_id);
				$i = 0;
				$total = $atu_valor['valor_total'];
				$erro = 0;
				
				foreach( $this->input->post('despesas') as $despesa ){

					if( $despesa != "" ){

						$data 		= 	explode('/', $this->input->post('dt_servico')[$i] );
						$dt_servico =	$data[2].'-'.$data[1].'-'.$data[0];

						$valor 			= str_replace(",", ".", str_replace(".","", $this->input->post('valor')[$i]));
						$insertItemOp 	= array(	'ordem_pagamento_id' 	=> 	$ordem_pagamento_id,
													'despesa_id'			=> 	$despesa,
													'qtd'					=> 	$this->input->post('qtd')[$i],
													'valor'					=> 	$valor,
													'dt_servico'			=> 	$dt_servico 	);

						if(! $this->ordemPagtoM->insertItemOp($insertItemOp) ){
							$erro = 1;
						}
						$total = $total + ($valor * $this->input->post('qtd')[$i]);
						$i++;
					}

				}

				if($erro == 0){
					$updateOp = array( 'id' 		=> 	$ordem_pagamento_id,
									   'valor_op'  	=> 	$total);

					if($this->ordemPagtoM->atualizarOrdemPagamento($updateOp)){
						
						$this->session->set_flashdata('sucesso_op', 'ok');				
						$this->editaChamado($this->input->post('chamado_id'));
					}
				}else{
					$this->session->set_flashdata('erro_op', 'erro');				
					$this->editaChamado($this->input->post('chamado_id'));
					
				}

			}
		}
		
	}	

	public function excluirItemOp(){
		
		if($this->ordemPagtoM->excluirItem($this->input->post('id'))) {
			$this->log('Área Administrador | Excluir Item OP','ordem de produção','EXCLUSÃO', $this->session->userdata('usuario_id'), array('id' => $_POST['id']),array('id' => $_POST['id']),$_SERVER['REMOTE_ADDR']);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}		
	}

	public function emitirOp(){
		
		$ordem_pagamento 			= 	$this->ordemPagtoM->find($this->input->post('ordem_pagamento_id'));
		$params['ordem_pagamento'] 	= 	$ordem_pagamento;
		$email 		= $this->load->view('area-administrador/assistencia-tecnica/email-ordem-pagamento', $params, true);
		$atu_valor 	= $this->ordemPagtoM->buscaValorTotal($this->input->post('ordem_pagamento_id'));
	    if(	$this->enviaEmail($this->input->post('email_tecnico'),'Autorização de pagamento de serviço Ref.: '.$ordem_pagamento[0]['cliente'], $email) ){

	    	$this->enviaEmail('assistenciatecnica@wertco.com.br','Autorização de pagamento de serviço Ref.: '.$ordem_pagamento[0]['cliente'], $email);
	    	$this->enviaEmail('industrial@wertco.com.br','Autorização de pagamento de serviço Ref.: '.$ordem_pagamento[0]['cliente'], $email);
	    	$this->enviaEmail('webmaster@companytec.com.br','Autorização de pagamento de serviço Ref.: '.$ordem_pagamento[0]['cliente'], $email);

	    	$atualizaStatusOp = array(	'id' 			=> 	$this->input->post('ordem_pagamento_id'),
	    								'status_id'		=>	2,
	    								'dt_emissao'	=> 	date('Y-m-d H:i:s'),
	    								'email_emissao'	=> 	$this->input->post('email_tecnico')	,
	    								'usuario_id'	=> 	$this->session->userdata('usuario_id'),
	    								'valor_op'		=> 	$atu_valor['valor_total'] 	);

	    	if(	$this->ordemPagtoM->atualizarOrdemPagamento($atualizaStatusOp) 	){
	    		$this->ordemPagtoM->insereOrdemPagtoHistorico($atualizaStatusOp);
	    		$this->session->set_flashdata('sucesso_op', 'ok');
				$this->editaChamado($this->input->post('chamado_id'));

	    	}else{
	    		$this->session->set_flashdata('sucesso_op', 'erro');
				$this->editaChamado($this->input->post('chamado_id'));

			}
	    }else{
	    	echo json_encode(array('retorno' => 'erro' ));
	    	
	    }

	}

	public function dashboardChamados(){

		$parametros 					= 	$this->session->userdata();
		$parametros['title']			=	"Chamados Dashboard";
		$parametros['total_status']		= 	$this->chamadoM->selectTotalStatus();
		$parametros['total_defeitos']	= 	$this->chamadoM->selectTotalDefeitos();
		$parametros['total_mes']		= 	$this->chamadoM->totalChamadoMes();
		$parametros['total_estado']		= 	$this->chamadoM->totalChamadoEstado();
		$parametros['ultimos_chamados']	= 	$this->chamadoM->ultimosChamadosRealizados();
		$parametros['dados']			=	$this->chamadoM->select();	
		
		$this->_load_view('area-administrador/assistencia-tecnica/dashboard',$parametros );	

	}

	public function geraChamadoPdf($id, $origem_solicitacao){
		 //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('chamado-'.$id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $data['chamados'] 	= 	$this->chamadoM->buscaChamadosAtividadesById($id);
	    $data['ops'] 		= 	$this->chamadoM->buscaOrdensDePagamento($id);
	    $data['tecnicos'] 	= 	$this->chamadoM->buscaTecnicosChamados($id);

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-administrador/assistencia-tecnica/chamado-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'chamado-'.$id.'.pdf'));
	    	}else{
	    		//return $retorno['retorno'] = 'chamado-'.$id.'.pdf';
	    		redirect(base_url('pdf/chamado-'.$id.'.pdf'));
	    	}
	    }	   	
	}
	
	public function geraChamadoCompletoPdf($id, $origem_solicitacao){
		 //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('chamado-completo-'.$id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $data['chamados'] 		= 	$this->chamadoM->buscaChamadosAtividadesById($id);
		$data['atendimentos'] 	= 	$this->chamadoM->buscaChamadosAtividadesAtendimentosById($id);
	    $data['ops'] 			= 	$this->chamadoM->buscaOrdensDePagamento($id);
	    $data['tecnicos'] 		= 	$this->chamadoM->buscaTecnicosChamados($id);

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-administrador/assistencia-tecnica/chamado-completo-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'chamado-completo-'.$id.'.pdf'));
	    	}else{
	    		//return $retorno['retorno'] = 'chamado-'.$id.'.pdf';
	    		redirect(base_url('pdf/chamado-completo-'.$id.'.pdf'));
	    	}
	    }	   	
	}
	
	public function geraTodosChamadosPdf(){
		 //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('todos-chamados.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $data['chamados'] 	= 	$this->chamadoM->buscaTodosChamadosPdf();	  

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-administrador/assistencia-tecnica/chamados-geral-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded	    		    	
	    	redirect(base_url('pdf/todos-chamados.pdf'));
	    	
	    }	   		

	}

	public function retornaEnderecoEmpresa(){

		$retorno = $this->empresasM->getEmpresa($_POST['empresa_id']);
		echo json_encode($retorno);

	}

	public function retornaPecas()
	{
		
		$rows = $this->chamadoM->retornaPecas($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array( 'label' 	=>	$row['descricao'],
								  'id'		=>	$row['id'],
								  'value'	=>	$row['descricao']	);
				
			}

			echo json_encode($dados);
		}
	}

	public function solicitaPecas(){
		
		$erro = 0;
		$enviaEmail = 0;
		if( $this->input->post('solicitacao_pecas_id') == 0 ){

			$solicitacaoPecas = array(	'descricao' 			=>	$this->input->post('descricao_pecas'),
										'endereco_entrega'		=> 	$this->input->post('endereco_entrega'),
										'empresa_entrega_id'	=>	$this->input->post('empresa_entrega'),
										'chamado_id'			=> 	$this->input->post('chamado_id'),
										'usuario_id'			=> 	$this->input->post('usuario_id'),
										'status_id'				=> 	1,
										'modo_envio'			=> 	$this->input->post('modo_envio')	);

			
			$solicitacao_pecas_id = $this->solicitacaoPecasM->insert($solicitacaoPecas);
			$enviaEmail = 1;
			$inserirAndamento = array(	'solicitacao_pecas_id'	=>	$solicitacao_pecas_id,
										'status_solicitacao_id'		=> 	1,
										'andamento'					=>	'Solicitação de peças realizadas para o chamado #<b>'.$this->input->post('chamado_id').'</b>',
										'dthr_andamento'			=>	date('Y-m-d H:i:s'),
										'usuario_id'				=> 	$this->session->userdata('usuario_id')	);

			if(!$this->solicitacaoPecasM->insereAndamentos($inserirAndamento)){
				$erro = 1;
			}
		}else{

			$solicitacao_pecas_id 	= 	$this->input->post('solicitacao_pecas_id');
			$solicitacaoPecasAtu 	= 	array(	'id'					=> 	$solicitacao_pecas_id,
												'descricao' 			=>	$this->input->post('descricao_pecas'),
												'endereco_entrega'		=> 	$this->input->post('endereco_entrega'),
												'empresa_entrega_id'	=>	$this->input->post('empresa_entrega'),
												'chamado_id'			=> 	$this->input->post('chamado_id'),
												'usuario_id'			=> 	$this->input->post('usuario_id'),
												'status_id'				=> 	1	);		

			if(!$this->solicitacaoPecasM->atualizar($solicitacaoPecasAtu) )
			{
				$erro = 1;
			}
		}	

		if($solicitacao_pecas_id > 0 && $solicitacao_pecas_id != null){
			$i = 0;
			$pecas_email	= '';
			foreach( $this->input->post('peca_id') as $peca_id ){
				if($this->input->post('peca')[$i] != ''){
					//Se não possuir id, a peça não exite e deve ser inserida na tabela peças				
					if($peca_id == 0){
						$pecas 		= 	array('descricao' => $this->input->post('peca')[$i] );
						$peca_id 	= 	$this->pecasM->insert($pecas);
					}

					$solicita_itens = array(	'solicitacao_peca_id' 	=> 	$solicitacao_pecas_id,
												'peca_id'				=> 	$peca_id,
												'qtd'					=> 	$this->input->post('qtd')[$i]	);

					if( $this->solicitacaoPecasItensM->insert($solicita_itens) ){
						$erro=0; 
						$pecas_email.='<tr>
											<td style="padding: 10px;border: 1px solid #f0f0f0;">'.mb_strtoupper($this->input->post('peca')[$i]).'</td>
											<td style="padding: 10px;border: 1px solid #f0f0f0;">'.$this->input->post('qtd')[$i].'</td>		
										</tr>'; 

					}else{
						$erro = 1;
						

					}
				}
				$i++;
			}

			if($erro == 0){
				$email='<html>
					<head></head>
					<body style="" width: 650px; position:absolute;">

						<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
						<img src="http://www.wertco.com.br/bootstrap/assets/demo/default/media/img/logo/wertcofundoescuro.png" /><br/>
							<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">⛽ Solicitação de Peças #'.$solicitacao_pecas_id.' - Chamado: '.$this->input->post('chamado_id').'</h2>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
							<b>Descrição / Observação:</b> '.$this->input->post('descricao_pecas').'<br/>
							<b>Destinatário:</b> '.$this->input->post('destinatario').' <br/>
							<b>Endereço de Entrega:</b> 	'.$this->input->post('endereco_entrega').'<br/>
							<b>Cliente:</b> '.$this->input->post('nome_cliente').'<br/>
							<b>Chamado:</b> '.$this->input->post('chamado_id').'<br/>							
							<b>Modo de Envio:</b> '.$this->input->post('modo_envio').'<br/>
							</p>
							<h2>Peças</h2>
							<table width="100%" style="border: 1px solid #ffcc00;">
								<thead>
									<tr>
										<th style="padding: 10px;border: 1px solid #f0f0f0;">PEÇA</th>
										<th style="padding: 10px;border: 1px solid #f0f0f0;">QTD</th>
									</tr>
								</thead>
								<tbody>
									'.$pecas_email.'
								</tbody>
							</table>

						</div>
					</body>
				</html>';
				
				if( $enviaEmail == 1 ){
					
		    		if( $this->enviaEmail('todossuporte@wertco.com.br','⛽ Solicitação de Peças #'.$solicitacao_pecas_id.' - Chamado: '.$this->input->post('chamado_id'), $email) ){

		    			$this->enviaEmail('almoxarifado@wertco.com.br','⛽ Solicitação de Peças #'.$solicitacao_pecas_id.' - Chamado: '.$this->input->post('chamado_id'), $email);
		    			$this->enviaEmail('almoxarifado2@wertco.com.br','⛽ Solicitação de Peças #'.$solicitacao_pecas_id.' - Chamado: '.$this->input->post('chamado_id'), $email);	
		    			$this->enviaEmail('webmaster@wertco.com.br','⛽ Solicitação de Peças #'.$solicitacao_pecas_id.' - Chamado: '.$this->input->post('chamado_id'), $email); 

		    			$this->session->set_flashdata('sucesso', 'ok');
		    		}else{
		    			$this->session->set_flashdata('erro', 'erro');
		    		}
		    		
				}else{
					$this->session->set_flashdata('sucesso', 'ok'); 
				}	
				
				$this->editaChamado($this->input->post('chamado_id'));	
			}else{
				$this->session->set_flashdata('erro', 'erro');
				$this->editaChamado($this->input->post('chamado_id'));
			}

		}else{

			$this->session->set_flashdata('erro', 'erro');
			$this->editaChamado($this->input->post('chamado_id'));
		}

	}

	public function emitirEmailSolicitacaoPecas(){
		$email='<html>
					<head></head>
					<body style="" width: 650px; position:absolute;">

						<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
						<img src="http://www.wertco.com.br/bootstrap/assets/demo/default/media/img/logo/wertcofundoescuro.png" /><br/>
							<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">⛽ Solicitação de Peças - Chamado: '.$this->input->post('chamado_id').'</h2>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
							<b>Descrição / Observação:</b> '.$this->input->post('descricao_pecas').'<br/>
							<b>Endereço de Entrega:</b> 	'.$this->input->post('endereco_entrega').'<br/>
							<b>Cliente:</b> '.$this->input->post('nome_cliente').'<br/>
							<b>Chamado:</b> '.$this->input->post('chamado_id').'<br/>							
							</p>
							<h2>Peças</h2>
							<table width="100%" style="border: 1px solid #ffcc00;">
								<thead>
									<tr>
										<th style="padding: 10px;border: 1px solid #f0f0f0;">PEÇA</th>
										<th style="padding: 10px;border: 1px solid #f0f0f0;">QTD</th>
									</tr>
								</thead>
								<tbody>
									'.$pecas_email.'
								</tbody>
							</table>

						</div>
					</body>
				</html>';
				
				
	    		if( $this->enviaEmail('todossuporte@wertco.com.br',	'⛽ Solicitação de Peças - Chamado: '.$this->input->post('chamado_id'), $email) ){
	    			$this->enviaEmail('almoxarifado@wertco.com.br',	'⛽ Solicitação de Peças - Chamado: '.$this->input->post('chamado_id'), $email);	    			

	    			$this->enviaEmail('almoxarifado2@wertco.com.br','⛽ Solicitação de Peças - Chamado: '.$this->input->post('chamado_id'), $email);	
	    			//$this->enviaEmail('suporte@wertco.com.br',	'⛽ Solicitação de Peças - Chamado: '.$this->input->post('chamado_id'), $email);
	    			//$this->enviaEmail('suporte2@wertco.com.br',	'⛽ Solicitação de Peças - Chamado: '.$this->input->post('chamado_id'), $email);
	    			
	    			echo json_encode(array('retorno' 	=>	'true'	));
	    		}else{

	    			echo json_encode(array('retorno'	=>	'false'	));
	    		}
		    	
	}

	public function excluirItemSolicitacaoPecas(){
		
		if($this->solicitacaoPecasItensM->excluirItem($this->input->post('id'))) {
			$this->log('Área Administrador | Excluir Item solicitacao','ordem de produção','EXCLUSÃO', $this->session->userdata('usuario_id'), array('id' => $_POST['id']),array('id' => $_POST['id']),$_SERVER['REMOTE_ADDR']);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function pecas(){
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Solicitações de Peças - Assistência Técnica";
		$parametros['situacao']	= 	$this->solicitacaoPecasM->retornaStatusSolicitacao();	
		$parametros['dados'] 	=	$this->solicitacaoPecasM->buscaSolicitacoes();	
		
		$this->_load_view('area-administrador/assistencia-tecnica/pecas',$parametros );	
	}

	public function alteraStatusSolicitacaoPecas()
	{

		$dados = array('id'			=>	$_POST['solicitacao_id'],
					   'status_id' 	=>	$_POST['status_solicitacao']);
		

		if($this->solicitacaoPecasM->atualizar($dados))
		{
			switch( $_POST['status_solicitacao'] ){
				case 1 : 
					$status = 'SOLICITAÇÃO REALIZADA';
					break;
				case 2 :
					$status = 'VERIFICAÇÃO DAS PEÇAS NO ESTOQUE';
					break;
				case 3 :
					$status = 'PEÇAS EM SEPARAÇÃO PARA ENVIO';
					break;
				case 4 :
					$status = 'PEÇAS ENVIADAS';
					break;
				case 5 :
					$status = 'SOLICITAÇÃO CANCELADA';
					break;
				case 6 :
					$status = 'PEÇA(S) ENTREGUE';					
					break;
				default:
					$status = $_POST['status_solicitacao'];
			}
			 
			$email='<html>
					<head></head>
					<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
						<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Solicitação de Peças #'.$_POST['solicitacao_id'].'<br/> Chamado# '.$_POST['chamado_id'].' <br/><hr>Status alterado para:<br/> "'.$status.'".</h1>

							<hr>														
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
								Cliente: '.$_POST['nome_cliente'].'
							
							</p>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
								Alterado pelo Usuário: '.$this->session->userdata('nome').'

							</p>
							
						</div>
					</body>
				</html>';

    		$this->enviaEmail('industrial@wertco.com.br',	'Solicitação de Peças #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'].'- Status alterado',$email );
    		$this->enviaEmail('todossuporte@wertco.com.br',	'Solicitação de Peças #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'].'- Status alterado',$email );
    		//$this->enviaEmail('suporte@wertco.com.br',	'Solicitação de Peças #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'].'- Status alterado',$email );
    		//$this->enviaEmail('suporte2@wertco.com.br',	'Solicitação de Peças #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'].'- Status alterado',$email );
    		$this->enviaEmail('webmaster@wertco.com.br','Solicitação de Peças #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'].'- Status alterado',$email );			
			$this->log('Área Administrador | atualizar status solicitação de peças','solicitação de peças','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);

			echo json_encode(array('retorno' => 'sucesso'));
			
		}else{ 
			echo json_encode(array('retorno' => 'erro'));
		}
	
	}

	public function anexarNfeAssistencia(){
		
		$tipo 			= 	explode('.', $_FILES['nfe']['name']);
		$arquivo 		=	md5($_FILES['nfe']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
		
		$configuracao = array(
	        'upload_path'   	=> 	'./nfe-assistencia/',		        
	        'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv|mp3',
	        'file_name'     	=> 	$arquivo
	    );      
	    $this->load->library('upload', $configuracao);
	    
	    if ($this->upload->do_upload('nfe')){

	    	$update = array( 	'id' 			=> 	$this->input->post('solicitacao_id'),
	    						'nr_nf' 		=>	$this->input->post('nr_nf'),
	    						'arquivo_nfe' 	=> 	$arquivo,
	    						'dthr_envio'	=> 	date('Y-m-d',strtotime($this->input->post('dthr_envio'))),
	    						'status_id'		=>  4 	);
	    	
	    	if( $this->solicitacaoPecasM->atualizar($update) ){

	    		$email='<html>
					<head></head>
					<body style="width: 650px; height: 600px; position:absolute;">
						<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Nota fiscal de emissão/substituição vinculada a solicitação</h1>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
							Nota Fiscal: '.$this->input->post('nr_nf').'<br/>
							cliente: 	'.$this->input->post('cliente').'<br/> 	
							Solicitação de Peças: '.$this->input->post('solicitacao_id').'<br/>
							Data de envio: '.$this->input->post('dthr_envio').'<br/>
							</p>
						</div>
					</body>
				</html>';

	    		if( $this->enviaEmail('industrial@wertco.com.br','⛽ Nota Fiscal de emissão Vinculada a Solicitação:'.$this->input->post('solicitacao_id'), $email,'./nfe-assistencia/'.$arquivo) ){
	    			$this->session->set_flashdata('sucesso', 'ok'); 
	    		}else{
	    			$this->session->set_flashdata('erro', 'erro');
	    		}
	    	}else{
	    		$this->session->set_flashdata('erro', 'erro');
	    	}
	    }

	    $this->pecas();

	}

	public function visualizarSolicitacaoPecas($id)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->solicitacaoPecasM->buscaSolicitacaoPorId($id);
		$parametros['itens']	=	$this->solicitacaoPecasM->buscaItensSolicitacao($id);		
		$parametros['title']	=	"Solicitação de Peças #".$id;
		$this->_load_view('area-administrador/assistencia-tecnica/visualiza-solicitacao',$parametros);
	}

	public function excluirOrcamento()
	{
		if( $this->input->post('enviar_exclusao') == '1' ){
			$insertMotivo = array(	'usuario_id' 	=> $this->session->userdata('usuario_id'),
									'tabela'		=> 	'orcamento',
									'motivo'		=> $this->input->post('motivo_exclusao'),
									'id_exclusao'	=> $this->input->post('orcamento_id') 	);

			if($this->motivoM->add($insertMotivo)){

				if( $this->orcamentosM->excluirOrcamento($this->input->post('orcamento_id')) ){
					
					$this->session->set_flashdata('exclusao', 'sucesso'); 	
				}else{

					$this->session->set_flashdata('erro', 'erro'); 	
				}
				
			}else{
				$this->session->set_flashdata('erro', 'erro'); 
			}

		}

		$this->orcamentos();
	}

	/************************************************************ 
	************** GERA PDF DOS ORÇAMENTOS FECHADOS *************
	*************************************************************/
	public function geraPdfOrcamentosFechados($mes)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-fechado-'.$mes.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
		
	    $data['orcamentos']	= $this->orcamentosM->buscaOrcamentosFechadosMes($mes);
	    $data['mes']	= 	$mes;
	    
	    //Load html view
	    $this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/orcamentos/orcamentos-fechados-pdf', $data, true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded	    	
	    	redirect(base_url('pdf/orcamento-fechado-'.$mes.'.pdf'));
	    	
	    }
	    
    }


    /************************************************************ 
	*********** GERA PDF DOS ORÇAMENTOS POR STATUS **************
	*************************************************************/
	public function geraPdfOrcamentosStatus($status)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-status.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
		
	    $data['orcamentos']	= 	$this->orcamentosM->buscaOrcamentosPorStatus($status);	    
	    $data['status']		= 	$status;
	    
	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-administrador/orcamentos/orcamentos-status-pdf', $data, true));

	    if($this->html2pdf->create('save')) {

	    	//PDF was successfully saved or downloaded	
	    	// Configuramos os headers que serão enviados para o browser
			header('Content-Description: File Transfer');
			header('Content-Disposition: attachment; filename="orcamento-status.pdf"');
			header('Content-Type: application/octet-stream');
			header('Content-Transfer-Encoding: binary');
			//header('Content-Length: ' . filesize(base_url('pdf/orcamento-status.pdf')));
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Expires: 0');
			// Envia o arquivo para o cliente			
			ob_clean();
            flush();
			readfile(base_url('pdf/orcamento-status.pdf'));    		    	
	    	
	    }
	    
    }

    public function gerarFormularioStartup(){
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('formulario-startup-'.$this->input->post('cliente_id').'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
		
	    $data['empresa']	= $this->empresasM->getEmpresa($this->input->post('cliente_id'));	    
	    $data['modelos']	= $this->input->post('modelo');	
	    $data['nr_series']	= $this->input->post('nr_serie');	    
	
		//Load html view		
		$this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/assistencia-tecnica/formulario-startup-pdf', $data,true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/formulario-startup-'.$this->input->post('cliente_id').'.pdf'));
	    
		}
    }

    public function buscaAnexosChamados(){
    	$retorno = $this->chamadoM->buscaAnexos($this->input->post('chamado_id'));		
		echo json_encode($retorno); 
    }

    public function autorizacaoServico(){
    	
    	$upload = '';
		if( $_FILES['anexo']['name'] != '' ){
			$uploads_realizados = $this->uploadMultiplo($_FILES['anexo'],'./chamados_atividades/');			
			$upload 			= $uploads_realizados[0]['nome'];
		}

		$insereAutorizacao = array(		'chamado_id' 		=>	$this->input->post('chamado_id'),
										'cliente_id'		=> 	$this->input->post('cliente_id'),
										'tecnico_id'		=> 	$this->input->post('tecnico_id'),
										'email_tecnico'		=> 	$this->input->post('email_tecnico'),
										'tipo'				=> 	$this->input->post('tipo'),
										'cep_cliente' 		=> 	$this->input->post('cep_cliente'),
										'endereco_cliente' 	=> 	$this->input->post('endereco_cliente'),
										'bairro_cliente' 	=> 	$this->input->post('bairro_cliente'),
										'cidade_cliente' 	=> 	$this->input->post('cidade_cliente'),
										'uf_cliente' 		=> 	$this->input->post('estado_cliente'),
										'solicitacao'		=> 	nl2br($this->input->post('solicitacao')),
										'anexo'				=> 	$upload 	);

		if($this->autorizServicoM->insereAutorizacao($insereAutorizacao)) {
			$params['dados'] 	= 	$this->input->post();
			$email 				= 	$this->load->view('area-administrador/assistencia-tecnica/email-autorizacao-servico', $params, true);
						
			if( $upload != ''){
				$this->enviaEmail($this->input->post('email_tecnico'),'Autorização de Serviço - '.$this->input->post('razao_social').' - '.$this->input->post('cnpj'), $email,'./chamados_atividades/'.$upload);

				$this->enviaEmail('todossuporte@wertco.com.br','Autorização de Serviço - '.$this->input->post('razao_social').' - '.$this->input->post('cnpj'), $email,'./chamados_atividades/'.$upload);
				//$this->enviaEmail('suporte2@wertco.com.br','Autorização de Serviço - '.$this->input->post('razao_social').' - '.$this->input->post('cnpj'), $email,'./chamados_atividades/'.$upload);
				$this->enviaEmail('industrial@wertco.com.br','Autorização de Serviço - '.$this->input->post('razao_social').' - '.$this->input->post('cnpj'), $email,'./chamados_atividades/'.$upload);

			}else{

				$this->enviaEmail($this->input->post('email_tecnico'),'Autorização de Serviço - '.$this->input->post('razao_social').' - '.$this->input->post('cnpj'), $email);
				$this->enviaEmail('todossuporte@wertco.com.br','Autorização de Serviço - '.$this->input->post('razao_social').' - '.$this->input->post('cnpj'), $email,'./chamados_atividades/'.$upload);
				//$this->enviaEmail('suporte2@wertco.com.br','Autorização de Serviço - '.$this->input->post('razao_social').' - '.$this->input->post('cnpj'), $email,'./chamados_atividades/'.$upload);
				$this->enviaEmail('industrial@wertco.com.br','Autorização de Serviço - '.$this->input->post('razao_social').' - '.$this->input->post('cnpj'), $email,'./chamados_atividades/'.$upload);

			}

			$this->session->set_flashdata('sucesso', 'sucesso');			
			$this->editaChamado($this->input->post('chamado_id'));

		}else{
			$this->session->set_flashdata('erro', 'erro');
			$this->editaChamado($this->input->post('chamado_id'));

		}

    }

    public function relatorioChamadosAbertos(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Relatório Chamados";
		$parametros['dados']	=	$this->chamadoM->select();
		$parametros['status']	=	$this->chamadoM->selectStatus();
		
		$this->_load_view('area-administrador/assistencia-tecnica/relatorio-chamados',$parametros );

    }

    public function relatorioChamados(){

    	$parametros 	= 	$this->input->post();
		$data['dados']	= 	$this->chamadoM->relatorioChamados($parametros);
		$retorno 		= 	$this->load->view('/area-administrador/assistencia-tecnica/relatorio-chamado-ajax', $data, true);

		echo json_encode(array('retorno'	=> 	$retorno ));    
    }

    public function relatorioOrcamentos(){
    	
		$parametros 				= 	$this->session->userdata();
		$parametros['title']		=	"Relatório Orçamentos";		
		$parametros['status']		=	$this->orcamentosM->selectStatus();
		$parametros['consultores'] 	= 	$this->zonaM->buscaConsultorWertco();		
		$this->_load_view('area-administrador/orcamentos/relatorio-orcamentos',$parametros );	
	}

    public function gerarRelatorioOrcamentos(){
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-orcamentos-pdf.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');		

	    $dt_ini = '';
	    if($this->input->post('dt_ini') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_ini'));	    	
	    	$dt_ini 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    }
	    $dt_fim = '';
	    if($this->input->post('dt_fim') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_fim'));	    	
	    	$dt_fim 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    }

	    $filtros = array(	'status_id' 	=> 	$this->input->post('status_id'),
	    					'dt_ini'		=> 	$dt_ini,
	    					'dt_fim'		=> 	$dt_fim,
	    					'estado'		=> 	$this->input->post('estado'),
	    					'usuario_id'	=> 	$this->input->post('consultor'),
	    					'representante_id' => 	$this->input->post('representante_id')	); 


	    switch ($this->input->post('regiao')) {
	     	case 'NORTE':
	     		$filtros['regiao'] = "('TO', 'AC', 'PA', 'RO', 'RR', 'AP', 'AM')";
	     		break;
	     	case 'NORDESTE':
	     		$filtros['regiao'] = "('BA', 'SE', 'AL', 'PB', 'PE', 'RN', 'CE', 'PI', 'MA')";
	     		break;
	     	case 'CENTRO-OESTE':
	     		$filtros['regiao'] = "('MT', 'MS', 'GO', 'DF')";
	     		break;
	     	case 'SUDESTE':
	     		$filtros['regiao'] = "('ES', 'RJ', 'MG', 'SP')";
	     		break;
	     	case 'SUL':
	     		$filtros['regiao'] = "('PR', 'SC', 'RS')";
	     		break;
	     	default :
	     		$filtros['regiao'] 	= 	'';
	     		break;	     	
	    }
	    
	    $data['dados']	=	$this->orcamentosM->getRelatorioOrcamentos($filtros);
		//$this->load->view('/area-administrador/orcamentos/relatorio-orcamentos-pdf', $data,true);
	    //Load html view		
		$this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/orcamentos/relatorio-orcamentos-pdf', $data,true)));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-orcamentos-pdf.pdf'));
	    
		}
    }

    public function relatorioPedidos(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Relatório Pedidos";		
		$parametros['status']	=	$this->pedidosM->buscaStatus();

		$this->_load_view('area-administrador/pedidos/relatorio-pedidos',$parametros );

    }

    public function gerarRelatorioPedidos(){
    	    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-pedidos-pdf.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');

	    $dt_ini = '';
	    $dt_fim = '';
	    $data['periodo'] = '';
	    $data['status'] = '';
	    if($this->input->post('dt_ini') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_ini'));	    	
	    	$dt_ini 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];

	    	$data['periodo'] = $this->input->post('dt_ini');
	    }

	    if($this->input->post('dt_fim') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_fim'));	    	
	    	$dt_fim 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    	$data['periodo'].= ' a '.$this->input->post('dt_fim');
	    }

	    $dt_ini_hist = '';
	    $dt_fim_hist = '';
	    $data['periodo'] = '';
	    if($this->input->post('dt_ini_hist') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_ini_hist'));	    	
	    	$dt_ini_hist 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];

	    	$data['periodo'] = $this->input->post('dt_ini_hist');
	    }

	    if($this->input->post('dt_fim_hist') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_fim_hist'));	    	
	    	$dt_fim_hist 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    	$data['periodo'].= ' a '.$this->input->post('dt_fim_hist'); 
	    }

	    if($this->input->post('status_id') != '')
	    {
	    	$data['status'] = $this->input->post('status_id');
	    }

	    $filtros = array(	'status_id' 		=> 	$this->input->post('status_id'),
	    					'dt_ini'			=> 	$dt_ini,
	    					'dt_fim'			=> 	$dt_fim,
	    					'indicador_id'		=> 	$this->input->post('indicador_id'),
	    					'dt_ini_hist'		=> 	$dt_ini_hist,
	    					'dt_fim_hist'		=> 	$dt_fim_hist ); 
	    $data['status_desc'] 	= 	$this->input->post('status_desc');
	    $data['status_ordem'] 	= 	$this->input->post('status_ordem');
	    $data['tipo'] 	= 	$this->input->post('tp');
	    $data['dados']	=	$this->pedidosM->getRelatorioPedidos($filtros);		
		$this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/pedidos/relatorio-pedidos-pdf', $data,true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-pedidos-pdf.pdf'));
	    
		}

    }

    public function relatorioTecnicosRegiao(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Relatório Técnicos por Região";	
		
		$this->_load_view('area-administrador/orcamentos/relatorio-tecnicos-regiao',$parametros );

    }

    public function relatorioTecnicosRegiaoAjax(){

    	$parametros 	= 	$this->input->post();
    	switch ($parametros['regiao']) {
	     	case 'NORTE':
	     		$filtros['regiao'] = "('TO', 'AC', 'PA', 'RO', 'RR', 'AP', 'AM')";
	     		break;
	     	case 'NORDESTE':
	     		$filtros['regiao'] = "('BA', 'SE', 'AL', 'PB', 'PE', 'RN', 'CE', 'PI', 'MA')";
	     		break;
	     	case 'CENTRO-OESTE':
	     		$filtros['regiao'] = "('MT', 'MS', 'GO', 'DF')";
	     		break;
	     	case 'SUDESTE':
	     		$filtros['regiao'] = "('ES', 'RJ', 'MG', 'SP')";
	     		break;
	     	case 'SUL':
	     		$filtros['regiao'] = "('PR', 'SC', 'RS')";
	     		break;
	     	default :
	     		$filtros['regiao'] 	= 	'';
	     		break;	     	
	    }
	    
		$data['dados']	= 	$this->usuariosM->relatorioTecnicosRegiao($filtros);
		$retorno 		= 	$this->load->view('/area-administrador/orcamentos/relatorio-tecnicos-regiao-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno ));

    }

    public function listaPecasSolicitacao()
    {

    	$pecas = $this->solicitacaoPecasM->buscaSolicitacaoPorId($_POST['pecas_id']);
		echo json_encode($pecas);
    }

    public function ValidaCnpj(){
    	$cnpj 			= 	$this->input->post('cnpj');		
		$consulta_rf 	= 	$this->ConsultaCNPJRF($cnpj);

        if (isset($consulta_rf) && $consulta_rf->{'code'} == '1')
        {
            if ($consulta_rf->{'data'}->{'situacao_cadastral'} == 'ATIVA')
            {
            	$arr['retorno'] 	= 	$consulta_rf->{'data'};
            	$arr['save'] 		= 	$consulta_rf->{'Save_Receita'};
                $arr['result'] 		= 	true;
                $arr['situacao'] 	= 	$consulta_rf->{'data'}->{'situacao_cadastral'};
            	//echo json_encode($consulta_rf);            	
                
            	$arr['result'] = true;
				$arr['erro'] = 0;

            }else {
            	$arr['erro'] 	= 1;
            	$arr['msg'] 	= '* Situação cadastral '.$consulta_rf->{'data'}->{'situacao_cadastral'}.' na Receita Federal.'; 
            }
		}else {

            if (isset($consulta_rf)){
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 2 - '.$consulta_rf->{'code'}.')';
            }else{
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 3)';
            }

        }

		echo json_encode(array(	'receita' 	=> $arr));
	}

	public function atualizaValidadeCartao()
	{
		$parametros 			= 	$this->session->userdata();			
		//$parametros['dados']	=	$this->cartaoM->getDadosCartao($this->session->userdata('usuario_id'));						
		$parametros['title']	=	"Atualiza Cartão Técnico";

		

		$this->_load_view('area-administrador/assistencia-tecnica/atualiza-cartao-admin',$parametros);

	}

	public function atualizaCartaoTecnico(){
		
		$data = explode('/',$_POST['ultima_atualizacao']);		

		$dados = array(	'id'					=>	$_POST['id'],
						'tag'					=>	$_POST['tag'],
						'ultima_atualizacao'	=>	'20'.$data['2'].'-'.$data['1'].'-'.$data['0'],
						'ultima_senha'			=>	$_POST['senha'] );

		if($this->cartaoM->atualizaCartao($dados)){

			$dadosLog = array( 'usuario_id' 		=> $this->session->userdata('usuario_id'),
							   'cartao_tecnico_id' 	=> 	$_POST['id']);
			
			$this->cartaoM->insereLogCartao($dadosLog);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{

			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function retornaDadosCartao(){

		$data_atual 					=	new DateTime(date('Y-m-d'));				
		$ultima_atualizacao 			= 	new DateTime($_POST['ultima_atualizacao']);		 	 	
		$dt_vencimento 					= 	new DateTime($_POST['dt_vencimento']);
		$diferenca						=	$data_atual->diff($ultima_atualizacao);
		$nova_data 						= 	'+'.$_POST['periodo_renovacao'].' month';
		$retorno['vencimento'] 			=  	date('d/m/y',strtotime($nova_data, strtotime($_POST['ultima_atualizacao'])));		

		if(	$dt_vencimento > date('Y-m-d',strtotime($nova_data, strtotime($_POST['ultima_atualizacao']))))
		{
			$retorno['data_renovacao_vencimento'] = 'MAIOR';
		}else{
			$retorno['data_renovacao_vencimento'] = 'MENOR';
		}

		$retorno['maior_vencimento'] 	= 	(strtotime(date('Y-m-d',strtotime($_POST['ultima_atualizacao']))) < strtotime(date('Y-m-d'))) ? 0 : 1;
		$retorno['diferenca_dias']		= 	$diferenca->d;
		$retorno['diferenca_mes']		= 	$diferenca->m;
		$retorno['diferenca_ano']		= 	$diferenca->y;
		$retorno['ultima_atualizacao'] 	= 	date('d/m/Y', strtotime($_POST['ultima_atualizacao']));

		echo json_encode($retorno);
	}


	public function alteraStatusChamado()
	{

		$dados = array(	'id'		=>	$_POST['id'],
						'status_id'	=>	$_POST['status_id']);

		if($this->chamadoM->update($dados)){
				echo json_encode(array('retorno' => 'sucesso'));			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}

	public function areaAtuacaoAdm(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Áreas de atuação dos representantes";
		$this->_load_view('area-administrador/area-atuacao/area-atuacao-adm',$parametros	);
	}

	public function areaAtuacaoAdmAjax(){

    	$parametros 	= 	$this->input->post();
		$data['dados']	= 	$this->areaAtuacaoM->select($parametros['empresa_id']);
		$retorno 		= 	$this->load->view('/area-administrador/area-atuacao/area-atuacao-adm-ajax', $data, true);
		echo json_encode(array('retorno'	=> 	$retorno ));

    }

    public function atualizaAreaAtuacao(){
    	$parametros =	$this->input->post();
    	$dados 		= 	array(	'id'		=>	$_POST['id'],
								'fl_ativo'	=>	$_POST['fl_ativo']	);

		if($this->areaAtuacaoM->update($dados)){
				echo json_encode(array('retorno' => 'sucesso'));			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
    }

    public function mapaUsuarios(){
    	$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Mapa dos indicadores e Representantes";
		$this->_load_view('area-administrador/assistencia-tecnica/mapa-tecnicos-clientes',$parametros	);
			
    }

    public function ConvertCepToLatLng($cep,$estado,$pais){

        $url = 'https://maps.google.com/maps/api/geocode/json?address='.$cep.','.$estado.','.$pais.'&key=AIzaSyAwBgnWcjYCWMKx5PfokswWZ5oKAo71gqk';
            
        $consulta_str = $this->Executa_URL_https($url);        
        
        $ret = array();
        
        if ($consulta_str->status == 'OK'){
            $ret['lat'] = $consulta_str->results[0]->geometry->location->lat;
            $ret['lng'] = $consulta_str->results[0]->geometry->location->lng;
            $ret['status'] = $consulta_str->status;
            return $ret;
        }else return $consulta_str;

    }

    public function todosTecnicosClientesMap(){

    	$contatos 	= 	$this->empresasM->todasEmpresasTecnicosClientes();   	
    	$modelos 	= 	$this->empresasM->listarBombasPorCliente();    	
	    $retorno 	= 	array();
	    $retorno['sucesso'] = true;
	    for ($i=0; $i < count($contatos); $i++){
	        $contatos[$i]['razao'] 					= utf8_encode($contatos[$i]['empresa']);	        
	        $contatos[$i]['grupo'] 					= $contatos[$i]['tipo'];
	        $contatos[$i]['cep'] 					= $contatos[$i]['cep'];
	        $contatos[$i]['endereco'] 				= $contatos[$i]['endereco'];
	        $contatos[$i]['bairro'] 				= $contatos[$i]['bairro'];
	        $contatos[$i]['estado'] 				= $contatos[$i]['estado'];
	        $contatos[$i]['cidade'] 				= $contatos[$i]['cidade'];	        
	        $contatos[$i]['telefone'] 				= $contatos[$i]['telefone'];
	        $contatos[$i]['credenciamento_inmetro'] = $contatos[$i]['credenciamento_inmetro'];
	        $contatos[$i]['credenciamento_crea'] 	= $contatos[$i]['credenciamento_crea'];	        
	        $contatos[$i]['tag'] 					= $contatos[$i]['tag'];
	        $contatos[$i]['local_treinamento'] 		= $contatos[$i]['local_treinamento'];
	        $contatos[$i]['email'] 					= $contatos[$i]['email'];
	        $contatos[$i]['status'] 				= utf8_encode($contatos[$i]['status']);	        
	        $contatos[$i]['lng'] 					= $contatos[$i]['longitude'];
	        $contatos[$i]['lat'] 					= $contatos[$i]['latitude'];           	        
	        $contatos[$i]['chamado_pedido']			= $contatos[$i]['chamado_pedido'];           

			$contatos[$i]['modelos'] = '';
	        foreach( $modelos  as $modelo){
				if( $contatos[$i]['id'] == $modelo['empresa_id'] ){
					$contatos[$i]['modelos'].= $modelo['modelo'].' - QTD.:'.$modelo['total'].' <br/> ';
					
				}
	        }

            if ($contatos[$i]['longitude'] == ''){
            	if( $contatos[$i]['cep'] != '' &&  $contatos[$i]['estado'] != '' &&  $contatos[$i]['pais'] != ''){
                	$geoloc = $this->ConvertCepToLatLng($contatos[$i]['cep'],$contatos[$i]['estado'],'BRASIL');
                	
                	if(!is_object($geoloc) ){
		                if ($geoloc['status'] == 'OK'){
		                    $update = array(  'id' 			=>	$contatos[$i]['id'],
		                    				  'latitude'	=> 	$geoloc['lat'],
		                    				  'longitude'	=> 	$geoloc['lng']	);


		                    $this->empresasM->atualizaEmpresas($update);
		                }else{
		                    
		                    $update = array( 'id' 			=>	$contatos[$i]['id'],
		                    				  'latitude'	=> 	-1,
		                    				  'longitude'	=> 	-1	);


		                    $this->empresasM->atualizaEmpresas($update);
		                    // salva mesmo assim para não ficar em loop buscando....
		               	}
	               	}else{
	               		$update = array( 	'id' 			=>	$contatos[$i]['id'],
		                    				'latitude'		=> 	-1,
		                    				'longitude'		=> 	-1	);


	                    $this->empresasM->atualizaEmpresas($update);
	                    // salva mesmo assim para não ficar em loop buscando....		
	               	}                
	            }else {
	        		$update = array( 	'id' 		=>	$contatos[$i]['id'],
	                				 	'latitude'	=> 	-1,
	                				  	'longitude'	=> 	-1	);


	                $this->empresasM->atualizaEmpresas($update);
	                // salva mesmo assim para não ficar em loop buscando....
	            }
            
            }else {
                $contatos[$i]['lat'] =  $contatos[$i]['latitude'];
                $contatos[$i]['lng'] = 	$contatos[$i]['longitude'];
            }
	        
	    }

	    echo json_encode($contatos);
    
    }

    public function importaPedidosStx($pedido_id, $itens){

    	$pedido 		= $this->pedidosM->getPedidosById($pedido_id);
    	$sincronismo_id = $this->pedidosM->sincronismoStx();
    	//inserir valor_stx    	
    	$inserePedido 	= array(	'pedido_internet' 		=> 	$pedido_id,
									'tipo_pedido'			=> 	'P',
									'codigo_cliente'		=>	$pedido->empresa_id,
									'pedido_cliente'		=> 	$pedido_id,									
									'data_emissao'			=> 	date('Y-m-d'),
									'desconto_tabela'		=> 	'',
									'codigo_vendedor'		=> 	'SITE',
									'valor_frete'			=> 	'',
									'valor_mercadoria'		=> 	'',
									'valor_ipi'				=> 	$pedido->valor_ipi,
									'valor_total'			=> 	$pedido->valor_total_stx,
									'codigo_transportadora' => '',
									'contato'				=> 	$pedido->nome_cliente,
									'tipo_pagamento_frete'	=> 	$pedido->frete,
									'observacoes'			=> 	$pedido->observacao,
									'pedido_mercantil'		=>  1,
									'status_pedido'			=> 	'P',
									'ultima_atualizacao'	=> 	date('Y-m-d'),
									'cliente_internet'		=> 	$pedido->empresa_id,
									'tipo_cliente'			=> 	'C',
									'sincronismo_id'		=> 	$sincronismo_id['sincronismo_id'],
									'sincronismo_data'		=> 	date('Y-m-d'),
									'sincronismo_situacao'	=> 	3	);

    	if( $this->pedidosM->insereStxPedido($inserePedido) ){
 			
 			foreach( $itens['subitem'] as $key=>$item ){
 				
 				foreach( $itens['subitem'][$key] as $chave=>$linha){

 					$numeroItem 	= 	$this->pedidosM->numeroItem($pedido_id);
 					$sincronismo 	= 	$this->pedidosM->getSincronismoStx($pedido_id);
 					$sincronismo_id = 	$this->pedidosM->sincronismoStxItem();
 					//inseri item pai						 					
	 				$insereItemPai 	= 	array(	'pedido_internet' 			=> 	$pedido_id,
		 										'id_internet'				=> 	'',
		 										'tipo_pedido'				=> 	'C',
		 										'codigo_produto'			=> 	'',
		 										'codigo_produto_internet'	=> 	$itens['codigo'][$key][$chave][0],
		 										'numero_item'				=> 	$numeroItem['total'],		 										
												'quantidade_produto'		=> 	1,												
												'preco_unitario'			=> 	$itens['valor'][$key][$chave][0],
												'perc_desconto'				=> 	'',
												'aliquota_ipi'				=> 	'5.00',
												'ultima_atualizacao'		=> 	date('Y-m-d'),
												'sincronismo_id_pedido'		=> 	$sincronismo['sincronismo_id'],
												'sincronismo_id'			=> 	$sincronismo_id['sincronismo_id'],
												'sincronismo_data'			=> 	date('Y-m-d'),
												'sincronismo_situacao'		=>	3,
												'numero_item_pai' 			=> 	''	);
	 				
	 				$this->pedidosM->insereStxPedidoItem($insereItemPai);
	 				$numeroItemPai 	= $numeroItem['total'];
	 				$codigo_pai 	= $itens['codigo'][$key][$chave][0];	

 					foreach($itens['subitem'][$key][$chave] as $indice=>$dados){
 						$sincronismo_id = 	$this->pedidosM->sincronismoStxItem();
			 			if(strstr($dados, '+')){
			 				$numeroItem 	= 	$this->pedidosM->numeroItem($pedido_id);
			 				$opcional 		= 	explode('+', $dados);
			 				//echo '<pre>'; var_dump($opcional);
				 			$insereItemPai 	= 	array(	'pedido_internet' 			=> 	$pedido_id,
				 										'id_internet'				=> 	'',
				 										'tipo_pedido'				=> 	'C',
				 										'codigo_produto'			=> 	'',
				 										'codigo_produto_internet'	=> 	$opcional[2],
				 										'numero_item'				=> 	$numeroItem['total'],				 										
														'quantidade_produto'		=> 	$itens['subitem_qtd'][$key][$chave][0],		
														'preco_unitario'			=> 	$opcional[3],
														'perc_desconto'				=> 	'',
														'aliquota_ipi'				=> 	'5.00',
														'ultima_atualizacao'		=> 	date('Y-m-d'),
														'sincronismo_id_pedido'		=> 	$sincronismo['sincronismo_id'],
														'sincronismo_id'			=> 	$sincronismo_id['sincronismo_id'],
														'sincronismo_data'			=> 	date('Y-m-d'),
														'sincronismo_situacao'		=> 	3,
														'numero_item_pai' 			=> 	$numeroItemPai 	);

			 				$this->pedidosM->insereStxPedidoItem($insereItemPai);
		 				}
			 		}		 			
	 			} 	 			
    		}    	
    	}
    }

    public function mapaClientesIndRep(){
    	$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Mapa dos indicadores e Representantes";
		$this->_load_view('area-administrador/empresas/mapa-clientes-representantes',$parametros	);
			
    }

 	public function todosIndicadoresRepresentantes(){

    	$contatos 	= 	$this->empresasM->listarTodosClientesIndRep();   	
    	$orcamentos	= 	$this->empresasM->listarOrcamentosPorCliente();    	
	    $retorno 	= 	array();
	    $retorno['sucesso'] = true;
	    for ($i=0; $i < count($contatos); $i++){
	        $contatos[$i]['razao_social'] 			= $contatos[$i]['empresa'];	        
	        $contatos[$i]['grupo'] 					= $contatos[$i]['tipo'];
	        $contatos[$i]['cep'] 					= $contatos[$i]['cep'];
	        $contatos[$i]['endereco'] 				= $contatos[$i]['endereco'];
	        $contatos[$i]['bairro'] 				= $contatos[$i]['bairro'];
	        $contatos[$i]['estado'] 				= $contatos[$i]['estado'];
	        $contatos[$i]['cidade'] 				= $contatos[$i]['cidade'];	        
	        $contatos[$i]['telefone'] 				= $contatos[$i]['telefone'];	        
	        $contatos[$i]['email'] 					= $contatos[$i]['email'];
	        $contatos[$i]['status'] 				= utf8_encode($contatos[$i]['status']);	        
	        $contatos[$i]['lng'] 					= $contatos[$i]['longitude'];
	        $contatos[$i]['lat'] 					= $contatos[$i]['latitude'];
	        

			$contatos[$i]['orcamentos'] = '';
	        foreach( $orcamentos  as $orcamento){
				if( $contatos[$i]['id'] == $orcamento['empresa_id'] ){
					$contatos[$i]['orcamentos'].= '<a href="'.base_url('areaAdministrador/visualizarOrcamento/'.$orcamento['id']).'" target="_blank">'.$orcamento['id'].' - Status.:'.$orcamento['status'].' <br/> ';
					
				}
	        }

            if ($contatos[$i]['longitude'] == ''){
            	if( $contatos[$i]['cep'] != '' &&  $contatos[$i]['estado'] != '' &&  $contatos[$i]['pais'] != ''){
                	$geoloc = $this->ConvertCepToLatLng($contatos[$i]['cep'],$contatos[$i]['estado'],'BRASIL');
                	
                	if(!is_object($geoloc) ){
		                if ($geoloc['status'] == 'OK'){
		                    $update = array(  'id' 			=>	$contatos[$i]['id'],
		                    				  'latitude'	=> 	$geoloc['lat'],
		                    				  'longitude'	=> 	$geoloc['lng']	);


		                    $this->empresasM->atualizaEmpresas($update);
		                }else{
		                    
		                    $update = array( 'id' 			=>	$contatos[$i]['id'],
		                    				  'latitude'	=> 	-1,
		                    				  'longitude'	=> 	-1	);


		                    $this->empresasM->atualizaEmpresas($update);
		                    // salva mesmo assim para não ficar em loop buscando....
		               	}
	               	}else{
	               		$update = array( 	'id' 			=>	$contatos[$i]['id'],
		                    				'latitude'		=> 	-1,
		                    				'longitude'		=> 	-1	);


	                    $this->empresasM->atualizaEmpresas($update);
	                    // salva mesmo assim para não ficar em loop buscando....		
	               	}                
	            }else {
	        		$update = array( 	'id' 		=>	$contatos[$i]['id'],
	                				 	'latitude'	=> 	-1,
	                				  	'longitude'	=> 	-1	);


	                $this->empresasM->atualizaEmpresas($update);
	                // salva mesmo assim para não ficar em loop buscando....
	            }            
            
            }else {
                $contatos[$i]['lat'] =  $contatos[$i]['latitude'];
                $contatos[$i]['lng'] = 	$contatos[$i]['longitude'];
            }
	        
	    }

	    echo json_encode($contatos);
    
    }

    public function confirmaVisualizacaoAlerta(){

    	$dadosUpdate = array(	'id' 				=> 	$this->input->post('alerta_id'),
    							'fl_visualizado'	=> 	1,
    							'dthr_visualizado'	=> 	date('Y-m-d H:i:s')	);

    	if(	$this->orcamentosAlertasM->atualizar($dadosUpdate)	){
    		
    		echo json_encode(array('retorno' => 'sucesso'));
    	}else{

    		echo json_encode(array('retorno' => 'erro'));
		}
    }

    public function zonaAtuacao()
	{
		
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->zonaM->select();
		$parametros['title']	=	"Zona de Atuação - Wertco";
		$this->_load_view('area-administrador/zona-atuacao/zona-atuacao',$parametros);
	}

	public function cadastrarZona()
	{
		
		if($this->input->post('salvar') == 1){
			
			$erro = 0;
			foreach( $this->input->post('estado') as $estado ){
				$insert = array(	'usuario_id' 	=> 	$this->input->post('usuario_id'),
									'estado'		=>	$estado 	);	
				if( ! $this->zonaM->adicionar($insert) ){
					$erro++;
				}

			}

			if( $erro == 0 ){
				$this->log('Área Administrador | cadastro zonas de atuacao','ZonaAtuacaoModel','INSERÇÃO', $this->session->userdata('usuario_id'),$insert,$insert,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$this->zonaAtuacao();

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Zonas de Atuação - Equipe Comercial";
			$parametros['usuarios']	= 	$this->usuariosM->buscaUsuariosComercialWertco();
			$parametros['estados']	= 	$this->zonaM->buscaEstadosDisponiveis();
			$this->_load_view('area-administrador/zona-atuacao/cadastrar-zona-atuacao',$parametros);
		}
	
	}
	
	public function editarZona($usuario_id = null){

		if($this->input->post('salvar') == 1){
			
			$erro = 0;
			
			
			if(	$this->zonaM->delete($this->input->post('usuario_id'))	){
				
				foreach( $this->input->post('estado') as	$estado){

					$insert = array(	'usuario_id' 	=> 	$this->input->post('usuario_id'),
										'estado'		=>	$estado 	);	

					if( ! $this->zonaM->adicionar($insert) ){
						$erro++;
					}

				}
			
			}

			if($erro == 0){
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$this->zonaAtuacao();	
			

		}else{
			
			$parametros 						= 	$this->session->userdata();
			$parametros['usuarios']				= 	$this->usuariosM->buscaUsuariosComercialWertcoTodos();
			$parametros['estados_selecionados']	=	$this->zonaM->buscaEstadosUsuarios($usuario_id);
			$parametros['estados_disponiveis']	= 	$this->zonaM->buscaEstadosDisponiveis();
			$parametros['title']				=	"Editar entregas";
			$parametros['usuario_id']			= 	$usuario_id;			
			$this->_load_view('area-administrador/zona-atuacao/editar-zona-atuacao',$parametros );
		}
	}

	public function excluirZonaAtuacao(){

		if(	$this->zonaM->delete($this->input->post('usuario_id'))	){
			echo json_encode(array('retorno' 	=> 	'sucesso'));
		}else{
			echo json_encode(array('retorno' 	=> 	'erro'));
		}
	}

	public function relatorioRepresentantes(){
    	
		$parametros 				= 	$this->session->userdata();
		$parametros['title']		=	"Relatório Orçamentos";				
		$parametros['consultores'] 	= 	$this->zonaM->buscaConsultorWertco();		
		$this->_load_view('area-administrador/zona-atuacao/relatorio-representantes',$parametros );
	}

    public function gerarRelatorioRepresentantes(){
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-representantes-pdf.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');	  

	   	$filtros = array(	'usuario_id'	=> 	$this->input->post('consultor'),
	   						'dt_fim'		=> 	'',
	   						'dt_ini'		=> 	''); 	 
	   	$data['subtitulo'] = '';
	   	if( $this->input->post('dt_ini') != '' &&  $this->input->post('dt_fim') != ''){
	   		$ini = explode(	'/',	$this->input->post('dt_ini')	);
	   		$fim = explode(	'/',	$this->input->post('dt_fim')	);
	   		$filtros['dt_ini'] = $ini[2].'-'.$ini[1].'-'.$ini[0]; 
	   		$filtros['dt_fim'] = $fim[2].'-'.$fim[1].'-'.$fim[0];
	   		$data['subtitulo'] = 'Período '.$this->input->post('dt_ini').' a '.$this->input->post('dt_fim');
	   	}

	    $data['dados'] 	= 	$this->zonaM->getRelatorioRepresentantes($filtros);
		
	    //Load html view		
		$this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/zona-atuacao/relatorio-representantes-pdf', $data,true)));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-representantes-pdf.pdf'));
	    
		}
    }

	public function copiarAtividadeAtendimento(){
		
		$insert = array(	'atividade_id' 	=> 	$this->input->post('atividade_id'),
							'modelo_id' 	=> 	$this->input->post('modelo_id'),
							'numero_serie' 	=> 	$this->input->post('numero_serie'),
							'observacao'	=>	$this->input->post('observacao')	);

		$atividade_id = $this->atividadeM->copiaAtividade($insert);

	 	if( $atividade_id ){
	 		if( $this->input->post('atendimento') == 's' ){
	 			$this->atividadeM->copiaAtendimentos($this->input->post('atividade_id'), $atividade_id);
	 		}
	 		$this->atividadeM->copiaCausaSolucoes($this->input->post('atividade_id'), $atividade_id);
	 		$this->atividadeM->copiaAnexos($this->input->post('atividade_id'), $atividade_id);
	 		echo json_encode(array(	'retorno' => 'sucesso' ));
				
		}else{

			echo 'erro inserção';
		}

	}

	public function atualizarRegrasPP(){

		if($this->input->post('salvar')){
			$data = explode('/', $this->input->post('dt_abertura_prod'));

			$update = array(	'id'				=>	$this->input->post('id'),
								'nr_bombas_semana'	=>	$this->input->post('nr_bombas_semana'),
								'nr_bicos_semana'	=> 	$this->input->post('nr_bicos_semana'),
								'dt_abertura_prod'	=> 	$data[2].'-'.$data[1].'-'.$data[0]	);
						 
			if($this->programacaoProdM->atualiza($update)){
				$this->log('Área Administrador | Regras Programação Produção','programacao_producao','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				
				$this->session->set_flashdata('sucesso', 'sucesso');								
				redirect(base_url('AreaAdministrador/programacaoProducaoRegras'));			
			}else{

				$this->session->set_flashdata('erro', 'erro');				
				redirect(base_url('AreaAdministrador/programacaoProducaoRegras'));				
			}

		}
	}

	public function programacaoProducaoRegras(){

		$parametros 			= 	$this->session->userdata();			
		$parametros['title']	=	"REGRAS - Programação de Produção";			
		$parametros['dados']	= 	$this->programacaoProdM->select();
		$this->_load_view('area-administrador/programacao-producao/regras',$parametros );
	}

	private function enviaEmailAlerta($cliente){

		$conteudo = '<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="http://www.wertco.com.br/bootstrap/assets/demo/default/media/img/logo/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">⚠️ Atenção! Startup aberto para cliente INADIMPLENTE ⚠️</h2>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Cliente: '.$cliente.'
								</p>
							</div>
						</body>
					</html>';
		
		$this->enviaEmail('financeiro@wertco.com.br','⚠️ Atenção! Startup aberto para cliente INADIMPLENTE ⚠️', $conteudo );
	
	}

	public function programacaoProducao(){

		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Programação de Produção - SEMANAS";
		$parametros['dados']	= 	$this->programacaoProdM->selectDatasPedidos();
		$parametros['regras']	= 	$this->programacaoProdM->select();
		$this->_load_view('area-administrador/programacao-producao/programacao-producao',$parametros );
	}

	public function atualizarProgramacaoSemanal(){

		$update = array(	'id' 					=> 	$this->input->post('programacao_semanal_id'),
							'dt_semana_prog' 		=> 	$this->input->post('dt_semana_prog'),	
							'dt_previsao_entrega'	=> 	date('Y-m-d',strtotime($this->input->post('dt_semana_prog').' +15 days')) );

		if( $this->programacaoProdM->atualizaProgramacao($update) ){ 

			if( $this->programacaoProdM->reorganizaSemana() ){
				echo json_encode(array(	'retorno'	=>	'sucesso' ));
			}else{
				echo json_encode(array(	'retorno' 	=> 	'erro' ));
			}

		}else{
			echo json_encode(array(	'retorno'	=> 	'erro' )	);
		}

	}

	public function buscaDadosPedido(){

		$dados = $this->programacaoProdM->buscaInfoPedido($this->input->post('pedido_id'));
		echo json_encode($dados);

	}

	public function alteraDataInexistePedido(){
		
		$dt_alteracao 		= 	explode('*', $this->input->post('dt_semana_prog'));
		$dt_alteracao 		= 	$dt_alteracao[2].'-'.$dt_alteracao[1].'-'.$dt_alteracao[0];
		$dt_existente 		= 	$this->programacaoProdM->verificaData($dt_alteracao);

		if($dt_existente['total'] > 0 ){
			echo json_encode(array(	'retorno' 	=> 	'data existente' ));

		}else{

			$update = array(	'id' 				=> 	$this->input->post('programacao_semanal_id'),
								'dt_semana_prog' 	=> 	$dt_alteracao,
								'dt_previsao_entrega'	=> 	date('Y-m-d',strtotime($dt_alteracao.' +15 days'))	);

			if( $this->programacaoProdM->atualizaProgramacao($update) ){
				//reorganiza a semana com o total de bicos e bombas
				$this->programacaoProdM->reorganizaSemana();
				echo json_encode(array('retorno' => 'sucesso'));

			}else{
				echo json_encode(array('retorno' => 'erro'));
				
			}

		}

	}

	public function verificaBicosBombasDt(){
		$dt_alteracao 	= 	explode('*', $this->input->post('dt_semana_prog'));
		$semana 		= 	$dt_alteracao[2].'-'.$dt_alteracao[1].'-'.$dt_alteracao[0];
		
		if( $semana != $this->input->post('dt_semana_pedido') ){
			$retorno 		= 	$this->programacaoProdM->retornaBicosBombasSemana($semana);
			$regras			= 	$this->programacaoProdM->select();
			$total_bicos 	= 	$this->input->post('bicos') + $retorno['bicos'];
			$total_bombas 	= 	$this->input->post('bombas') + $retorno['bombas'];
			  
			if( $total_bicos > $regras['nr_bicos_semana'] || $total_bombas > $regras['nr_bombas_semana'] ){

				echo json_encode(array('retorno'	=> 	false));
			}else{
				
				echo json_encode(array('retorno'	=> 	true));
			}
		}else{
			echo json_encode(array('retorno'	=> 	true));	
		}

	}

	public function quebraPedidoProgramacao()
	{
					
		$dt_alteracao1 	= 	explode('*', $this->input->post('dt_semana_nova1'));
		$semana1 		= 	$dt_alteracao1[2].'-'.$dt_alteracao1[1].'-'.$dt_alteracao1[0];
		
		$inserePedidos = array( 'pedido_id' 			=> 	$this->input->post('pedido_id'),
								'dt_semana_prog' 		=> 	$semana1,	
								'nr_bicos'				=> 	$this->input->post('bicos1'),
								'nr_bombas'				=> 	$this->input->post('bombas1'),
								'dt_previsao_entrega' 	=> 	date('Y-m-d',strtotime($semana1.' +15 days')),
								'modelos_quebra'		=> 	$this->input->post('modelo1')	);

		if( $this->programacaoProdM->inserirPedido($inserePedidos) ){
			$dt_alteracao2 	= 	explode('*', $this->input->post('dt_semana_nova2'));
			$semana2 		= 	$dt_alteracao2[2].'-'.$dt_alteracao2[1].'-'.$dt_alteracao2[0];
			$inserePedidos2 = array( 	'pedido_id' 			=> 	$this->input->post('pedido_id'),
										'dt_semana_prog' 		=> 	$semana2,	
										'nr_bicos'				=> 	$this->input->post('bicos2'),
										'nr_bombas'				=> 	$this->input->post('bombas2'),
										'dt_previsao_entrega' 	=> 	date('Y-m-d',strtotime($semana2.' +15 days')),
										'modelos_quebra'		=> 	$this->input->post('modelo2')		);			
			if( $this->programacaoProdM->inserirPedido($inserePedidos2) ){

				if( $this->programacaoProdM->excluirPedido($this->input->post('programacao_semanal_id')) ){
					//reorganiza a semana com o total de bicos e bombas
					$this->programacaoProdM->reorganizaSemana();
					echo json_encode(array('retorno' =>	true ));
				}else{
					echo json_encode(array('retorno' => false ));
				}
			}else{
				echo json_encode(array('retorno' =>	false ));
			}	
		}else{
			echo json_encode(array('retorno' =>	false ));
		}
		

	}

	public function buscaModelosPedido()
	{
		echo json_encode($this->programacaoProdM->buscaModelosPedido($this->input->post('pedido_id')));
	}

	public function solicitaAprovacao()
	{
		$update = array('id'				=> 	$this->input->post('pedido_id'),
						'status_pedido_id'	=> 	9);
		
		if($this->pedidosM->atualizaPedidos($update)){
			$this->insereAndamentoPedido( $this->input->post('pedido_id'), 9 );
			$conteudo = '<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">⚠️ Atenção! Solicitação de Aprovação de Pedido ⚠️</h2>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Pedido: #'.$this->input->post('pedido_id').'<br/>
									Cliente: '.$this->input->post('cliente').'
								</p>
								<a href="https://www.wertco.com.br/Home/aprovaPedido/'.$this->input->post('pedido_id').'" style="font-family: verdana;font-size: 16px;font-weight: 500;line-height: 2.5;color: #000;border: 1px solid #000;padding: 10px;background: #ffcc00;text-decoration: none;border-radius: 5px;">Aprovar Pedido e Solicitar Liberação do Financeiro para Pagto Sinal</a>
							</div>
						</body>
					</html>';
			// ALTERAR PARA GESTOR@WERTCO.COM.BR
			if($this->enviaEmail('gestor@wertco.com.br','⚠️ Atenção! Solicitação de Aprovação de Pedido ⚠️', $conteudo )){
				echo json_encode(array('retorno' =>	'sucesso'));
			}else{
				echo json_encode(array('retorno' =>	'erro'));
			}
		}
	}

	public function solicitaLiberacao()
	{
		$update = array('id'				=> 	$this->input->post('pedido_id'),
						'status_pedido_id'	=> 	17 );
		
		if($this->pedidosM->atualizaPedidos($update)){
			$this->insereAndamentoPedido( $this->input->post('pedido_id'), 17 );
			$conteudo = '<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">⚠️ Atenção! Solicitação de Confirmação de Pedido ⚠️</h2>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Pedido: #'.$this->input->post('pedido_id').'<br/>
									Cliente: '.$this->input->post('cliente').'
								</p>
								<a href="https://www.wertco.com.br/Home/aprovaConfirmacaoPedido/'.$this->input->post('pedido_id').'" style="font-family: verdana;font-size: 16px;font-weight: 500;line-height: 2.5;color: #000;border: 1px solid #000;padding: 10px;background: #ffcc00;text-decoration: none;border-radius: 5px;">Aprovar Pedido</a>
							</div>
						</body>
					</html>';
			
			if($this->enviaEmail('financeiro@wertco.com.br','⚠️ Atenção! Solicitação de Confirmação do cliente ⚠️', $conteudo )){
				echo json_encode(array('retorno' =>	'sucesso'));
			}else{
				echo json_encode(array('retorno' =>	'erro'));
			}
		}
	
	}


	 public function relatorioHistoricoNegociacoes(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Histórico de Negociações";
		
		$this->_load_view('area-administrador/orcamentos/relatorio-historico-negociacoes',$parametros );

    }

    public function relatorioHistoricoNegociacoesAjax(){
    	
    	$filtros = '1=1';
    	if( $this->input->post('dt_ini') != '' and $this->input->post('dt_fim') != '')
    	{
    		$dt_ini = explode('/', $this->input->post('dt_ini'));
    		$dt_fim = explode('/', $this->input->post('dt_fim'));

    		$filtros.= " and o.emissao between '".$dt_ini[2]."-".$dt_ini[1]."-".$dt_ini[0]."' and '".$dt_fim[2]."-".$dt_fim[1]."-".$dt_fim[0]."' ";
    	}

    	if( $this->input->post('status') == 'abertas' )
    	{
    		$filtros.=" and (p.status_pedido_id = 1 or o.id not in (select orcamento_id from pedidos) ) ";
    	}

    	if( $this->input->post('status') == 'fechadas' )
    	{
    		$filtros.=" and (p.status_pedido_id > 1 and p.status_pedido_id <> 7) and o.status_orcamento_id <> 4 ";
    	}

    	if( $this->input->post('cliente_id') != '' )
    	{
    		$filtros.=" and e.id = ". $this->input->post('cliente_id');
    	}

		$data['dados']	= 	$this->orcamentosM->historicoNegocicoes($filtros);
		$retorno 		= 	$this->load->view('/area-administrador/orcamentos/relatorio-historico-negociacoes-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno ));

    }

    public function buscaAndamentosHistorico()
    {
    	
    	if($this->input->post('tipo') == 'orcamento')
    	{
    		$dados = $this->orcamentosM->buscaAndamentos($this->input->post('id'));
    	}else{
    		$dados = $this->pedidosM->buscaAndamentos($this->input->post('id'));
    	}

    	$html='<div class="row">
				<div class="col-md-12">					
					<ul class="timeline">';
    	foreach($dados as $dado){
    		$html.='	<li>
							<a href="#" style="color: #000;font-weight: 500;">'.$dado['status'].'</a>
							<a href="#" class="float-right" style="color: #000;font-weight: 500;">'.$dado['dt_andamento'].' - '.$dado['usuario'].' </a>
							<p style="margin-top: 12px;">'.$dado['andamento'].'</p>
						</li>';
    	}

		$html.='	</ul>
				</div>
			   </div>';

		echo json_encode(array('retorno' => $html	));	   
    }

    public function relatorioClientesInadimplentes(){
    	
		$parametros 				= 	$this->session->userdata();
		$parametros['title']		=	"Relatório Clientes Inadimplentes";				
		$this->_load_view('area-administrador/empresas/relatorio-clientes-inadimplentes',$parametros );	
	}

    public function gerarRelatorioClientesInadimplentes(){
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-clientes-inadimplentes.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');		

	    $filtros = array(	'estado'		=> 	$this->input->post('estado'),
	    					'cliente_id' 	=> 	$this->input->post('cliente_id')	); 


	    switch ($this->input->post('regiao')) {
	     	case 'NORTE':
	     		$filtros['regiao'] = "('TO', 'AC', 'PA', 'RO', 'RR', 'AP', 'AM')";
	     		break;
	     	case 'NORDESTE':
	     		$filtros['regiao'] = "('BA', 'SE', 'AL', 'PB', 'PE', 'RN', 'CE', 'PI', 'MA')";
	     		break;
	     	case 'CENTRO-OESTE':
	     		$filtros['regiao'] = "('MT', 'MS', 'GO', 'DF')";
	     		break;
	     	case 'SUDESTE':
	     		$filtros['regiao'] = "('ES', 'RJ', 'MG', 'SP')";
	     		break;
	     	case 'SUL':
	     		$filtros['regiao'] = "('PR', 'SC', 'RS')";
	     		break;
	     	default :
	     		$filtros['regiao'] 	= 	'';
	     		break;	     	
	    }
	    
	    $data['dados']	=	$this->empresasM->getRelatorioClientesInadimplentes($filtros);
		//$this->load->view('/area-administrador/orcamentos/relatorio-orcamentos-pdf', $data,true);
	    //Load html view		
		$this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/empresas/relatorio-clientes-inadimplentes-pdf', $data,true)));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-clientes-inadimplentes.pdf'));
	    
		}
    }

    private function enviaEmailStatusGestor($orcamento_id, $status_id)
    {
    	$cliente = $this->orcamentosM->buscaClienteOrcamento($orcamento_id);
    	
    	$status = "";

    	switch($status_id){
			case 1:
		        $status = "Orçamento Aberto!";
		        break;
		    case 2:
		        $status = "Orçamento Fechado, venda realizada!";
		        break;
		    case 3:
		        $status = "Orçamento Perdido.";
		        break;
		    case 4:
		        $status = "Orçamento Cancelado.";
		        break;
		    case 5:
		        $status = "Orçamento entregue ao cliente.";
		        break;        
		    case 6:
		        $status = "Orçamento em negociação.";
		        break;
		    case 7:
		    	$status = "Solicitação de Orçamentos - Indicador";
		    	break;
		     case 9:
		        $status = "Bombas em teste no cliente.";
		        break; 
		    case 10:
		        $status = "Orçamento Perdido para Wayne.";
		        break;
		    case 11:
		        $status = "Orçamento Perdido para Gilbarco.";
		        break;
		    case 12:
		        $status = "Aguardando Aprovação Gestor.";
		        break;
		    case 13:
		        $status = "Expirado.";
		        break;
		    case 14:
		    	$status = "Orçamento Sem Retorno.";
		        break;
		}

    	$conteudo = '<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Modificação de Status - Orçamento: '.$orcamento_id.'</h2>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Orçamento: <a href="'.base_url('areaAdministrador/visualizarOrcamento/'.$orcamento_id).'"> #'.$orcamento_id.'</a><br/>									
								</p>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Cliente: '.$cliente['cliente'].'</a><br/>									
								</p>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Cidade/Estado: '.$cliente['cidade'].'/'.$cliente['estado'].'<br/>		
								</p>	
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Novo Status: <b>'.$status.'</b><br/>		
								</p>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Alterado Por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>							
							</div>
						</body>
					</html>';
		
		if($this->enviaEmail('gestor@wertco.com.br','⚠️ Atenção! Status Alterado - Orçamento:'.$orcamento_id.' ⚠️',$conteudo)){
			return true;
		}else{
			return false;
		}
	}

	public function buscaNrSerieOpPedido(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Busca Nr. Séries";	
		
		$this->_load_view('area-administrador/producao/busca-nr-serie-op-pedido',$parametros );
    }

    public function buscaNrSerieOpPedidoAjax(){

    	$parametros 	= 	$this->input->post();
    	$filtros = "1=1";
    	if($parametros['nr_serie'] != ""){
    		$filtros.=" and b.id=". $parametros['nr_serie'];

    	}
    	if($parametros['nr_op'] != ""){
    		$filtros.=" and op.id=". $parametros['nr_op'];

    	}
    	if($parametros['nr_pedido'] != ""){
    		$filtros.=" and pe.id =". $parametros['nr_pedido'];

    	}
	    
		$data['dados']	= 	$this->pedidosM->relatorioNrSerieOpPedidos($filtros);
		$retorno 		= 	$this->load->view('/area-administrador/producao/busca-nr-serie-op-pedido-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno ));


    }

    public function buscaPedidosAndamento()
    {
    	$dados 	= 	$this->pedidosM->buscaAndamentoPedido($this->input->post('pedido_id'));
    	
    	$html 	=	'<div class="row">
						<div class="col-md-12">					
							<ul class="timeline">';
		    	foreach($dados as $dado){
		    		$html.='	<li>
									<a href="#" style="color: #000;font-weight: 500; text-decoration: none;">'.$dado['andamento'].'</a>
									<a href="#" class="float-right" style="color: #000;font-weight: 500;text-decoration: none;">'.$dado['dthr_andamento'].' - '.$dado['usuario'].' </a>
									<p style="margin-top: 12px;"></p>
								</li>';
		    	}

				$html.='	</ul>
						</div>
					   </div>';

		echo json_encode(array('retorno' => $html	));	
    }

    public function buscaOrdemProducao()
    {
    	$dados 	= 	$this->opM->buscaOpNrSerie($this->input->post('op_id'),$this->input->post('nr_serie'));

    	$html 	=	'<div class="row">
						<div class="col-md-12">
							<table class="table">
		    					<thead>
		    						<th style="width: 40%;"> Descrição 		</th>		    							
		    						<th style="width: 20%;"> Combustíveis	</th>
		    						<th style="width: 40%;"> Info.	</th>
		    					</thead>
		    					<tbody>
		    						<tr>
		    							<td>'.$dados['descricao'].'</td>		    								
		    							<td>'.$dados['combustivel'].'</td>
		    							<td>'.$dados['informacoes'].'</td>
		    						</tr>
		    					</tbody>				
		    				</table>
						</div>
				   </div>';

		echo json_encode(array('retorno' => $html	));	
	}

	public function relatorioProgramacaoSemanal(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Relatório da Programação Semanal";
		$this->_load_view('area-administrador/producao/relatorio-bombas-producao',$parametros );
    }

    public function gerarRelatorioProgramacaoSemanal(){
    	    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-bombas-producao-pdf.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
		
	    $dt_ini = '';
	    $dt_fim = '';
	    $data['semana'] = '';
	    if($this->input->post('dt_ini') != '' && $this->input->post('dt_fim') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_ini'));
	    	$dt_ini 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    	$explodeF 	= 	explode('/',$this->input->post('dt_fim'));	    	
	    	$dt_fim 	= 	$explodeF[2].'-'.$explodeF[1].'-'.$explodeF[0];
	    	//$dt_fim 	=	date('Y-m-d', strtotime('+4 days',strtotime($dt_ini)));
	    	//$fim 		= 	date('d/m/Y', strtotime('+4 days',strtotime($dt_ini)));
	    	$data['semana'] = 'Semana: '.date('W', strtotime($dt_ini) ).' - '. $this->input->post('dt_ini').' a Semana: '.date('W', strtotime($dt_fim) ).' - '.$this->input->post('dt_fim') ;
	    }	    

	    $filtros = array(	'dt_ini'			=> 	$dt_ini,
	    					'dt_fim'			=> 	$dt_fim 	); 
	    
	    $data['dados']	=	$this->pedidosM->relatorioProgramacaoSemanal($filtros);
		//$this->load->view('/area-qualidade/relatorios/relatorio-rastreabilidade-pdf', $data);				
		$this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/producao/relatorio-bombas-producao-pdf', $data,true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-bombas-producao-pdf.pdf'));
	    
		}
    }

    public function buscaNrSeriePorPedido()
    {
    	$nr_series = $this->producaoAndamentoM->buscaNrSeriePorPedido($this->input->post('pedido_id'));
    	$combo = "";
    	foreach($nr_series as $nr_serie){
    		$combo.="<option value='".$nr_serie['id']."'> ".$nr_serie['id']." - ".$nr_serie['modelo']." </option> ";
    	}

    	echo json_encode(array('combo' => $combo));
    }

    public function buscaStatusProducao()
    {
    	$status_p = $this->producaoAndamentoM->buscaStatusProducao();
    	$combo = "";
    	foreach($status_p as $status){
    		$combo.="<option value='".$status['id']."'> ".$status['descricao']." </option> ";
    	}

    	echo json_encode(array('combo' => $combo));
    }

    public function insereAndamentoProducao()
    {
    	$insere = array( 	'pedido_id' 			=> 	$this->input->post('pedido_id'),
    						'nr_serie_id' 			=> 	$this->input->post('nr_serie_id'),
    						'status_producao_id' 	=> 	$this->input->post('status_producao_id'),
    						'descricao' 			=> 	$this->input->post('observacao'),
    						'usuario_id' 			=> 	$this->session->userdata('usuario_id') 	);
    	if( $this->producaoAndamentoM->add($insere) ){
    		$conteudo = '<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Status de Pedido em Produção Alterado #'.$this->input->post('pedido_id').'</h2>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Pedido: '.$this->input->post('pedido_id').' <br/>									
								</p>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Nº Série: '.$this->input->post('nr_serie_id').'</a><br/>									
								</p>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Observação: '.$this->input->post('observacao').'<br/>		
								</p>									
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Alterado Por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>							
							</div>
						</body>
					</html>';
		
			if($this->enviaEmail('industrial@wertco.com.br','⚠️ Atenção! Status de Pedido em Produção Alterado #'.$this->input->post('pedido_id').' ⚠️',$conteudo)){
    			echo json_encode(array('retorno' => 'sucesso'));
    		}else{
    			echo json_encode(array('retorno' => 'email não enviado'));	
    		}
    	}else{
    		echo json_encode(array('retorno' => 'erro'));
    	}

    }

    public function buscaStatusPedidosProducao()
    {
    	$dados = $this->producaoAndamentoM->buscaStatusPedidoProducao($this->input->post('pedido_id'));
    	$html="";
    	foreach($dados as $dado){
    		$html .= "<tr>
    					<td><b>".$dado['nr_serie_id']."</b></td>
    					<td>".$dado['status']."</td>
    					<td>".$dado['descricao']."</td>
    					<td>".$dado['dthr_andamento']."</td>
    					</tr>";
    	}

    	echo json_encode(array('html' => $html));

    }

    public function gerarRastreabilidade($nr_serie){
    	    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('formulario-rastreabilidade-pdf.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape','en', true, 'UTF-8', array(0, 0, 0, 0));
			    	    
	    $data['dados']	=	$this->opM->buscaPedidoOpPorNrSerie($nr_serie);

	    //$this->load->view('/area-administrador/producao/formulario-rastreabilidade-pdf', $data);
		$this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/producao/formulario-rastreabilidade-pdf', $data,true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/formulario-rastreabilidade-pdf.pdf'));
	    
		}
    }

    public function uploadEmbalagem()
    {
    	if(isset($_FILES['arquivos']['name'][0])) {

			$uploads_realizados = $this->uploadMultiplo($_FILES['arquivos'],'./fotos_pedido/');
			$erro=0;
			foreach ($uploads_realizados as $upload) {

				$dadosInsert = 	array(	'pedido_id' 	=> $this->input->post('pedido_id'),
										'foto'		=> $upload['nome'] 	);

				if(! $this->pedidosM->inserirFotos($dadosInsert) ){
					$erro++;
				}

			}

			if($erro==0){
				$this->session->set_flashdata('sucesso', 'ok');
				$dados = array(	'id'				=>	$this->input->post('pedido_id'),
					   			'status_pedido_id' 	=>	14);

				if($this->pedidosM->atualizaStatusPedido($dados))
				{
					$this->insereAndamentoPedido($this->input->post('pedido_id'), 14);					
					
					$email='<html>
							<head></head>
							<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
								<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
									<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido '.$this->input->post('pedido_id').' acabado e pronto para ser embalado e finalizado</h1>
									<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										Pedido: '.$this->input->post('pedido_id').'  <br/>
										Usuário: '.$this->session->userdata('nome').'
										<br/>
									</p>
								</div>
							</body>
						</html>';

		    		$this->enviaEmail('producao@wertco.com.br','⚠️ Pedido '.$this->input->post('pedido_id').' acabado e pronto para ser embalado ⚠️', $email);
			    	
			    }
				redirect(base_url('AreaAdministrador/pedidos'));
			}
		}
    }

    public function listarFotosPedido()
    {
    	$fotos = $this->pedidosM->listarFotosPedido($this->input->post('pedido_id'));
    	$html = "";
    	foreach($fotos as $foto){
    		$html.= '<div class="col-xs-3" style="padding-right: 5px;">
    					<a href="'.base_url('fotos_pedido/'.$foto['foto']).'" target="_blank"><img src="'.base_url('fotos_pedido/'.$foto['foto']).'" width="150"></a>
    					</div>';

    	}

    	echo json_encode(array('html' => $html));

    }


    public function listarPinturaPedido()
    {
    	$arquivo 	= 	$this->pedidosM->getPedidosById($this->input->post('pedido_id'));
    	$html_arquivo = ''; 
    	$existe = 0;
    	if($arquivo->arquivo != ''){
    		$html_arquivo ='<div class="col-lg-4" style="border-right: 1px solid #ffcc00;text-align: center; ">
    							<a href="'.base_url('pedidos/'.$arquivo->arquivo).'" target="_blank" style="color: #000;"><i class="fa fa-picture-o" style="font-size: 70px;margin: 0 31px;color: #ffcc00;"></i><br/> Visualizar Arquivo</a>
    						</div>';
    		$existe =	1;
    	}
    	$html 		= 	'<div class="row"  >
    						'.$html_arquivo.'
    						<div class="col-lg-8">
    							<h5>Descrição da Pintura</h5>
    							<p>'.$arquivo->pintura_descricao.'</p>
    						</div>
    					</div>';
    	
    	echo json_encode(array(	'html' 		=> 	$html,
    							'pedido' 	=> 	$arquivo,
    							'existe' 	=> 	$existe )	);

    }

    public function verificaCombustiveis()
    {
    	echo json_encode($this->pedidosM->verificaCombustiveis($this->input->post('pedido_id')));
    }

    public function liberaPedidoProducao()
    {
    	
    	if( $this->input->post('pedido_id') ){

    		$pedido = $this->pedidosM->getPedidosById($this->input->post('pedido_id'));

    		if( $pedido->testeira == '' || $pedido->painel_frontal == '' || $pedido->painel_lateral == '' ){
    			echo json_encode(array('retorno' => 'erro pintura'));
    		}else{

	    		$dados = array(	'id'				=>	$this->input->post('pedido_id'),
						   		'status_pedido_id' 	=>	16);

				if($this->pedidosM->atualizaStatusPedido($dados))
				{				
					$this->insereAndamentoPedido($this->input->post('pedido_id'), 2);
					$this->insereAndamentoPedido($this->input->post('pedido_id'), 16);
					
					$email='<html>
							<head></head>
							<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
								<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">

									<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido #'.$this->input->post('pedido_id').' confirmado pelo cliente e liberado para serem geradas as ordens de produção</h1>
									<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										Pedido: '.$this->input->post('pedido_id').'  <br/>
										Usuário: '.$this->session->userdata('nome').'
										<br/>
									</p>
								</div>
							</body>
						</html>';
					// ALTERAR PARA	
		    		if($this->enviaEmail('producao@wertco.com.br','⚠️ Pedido #'.$this->input->post('pedido_id').' confirmado pelo cliente ⚠️', $email)){	
		    			echo json_encode(array('retorno' => 'sucesso'));
		    		}else{
		    			echo json_encode(array('retorno' => 'erro de envio de e-mail'));
		    		}
			    
			    }    		
			}

    	}else{
    		echo json_encode(array('retorno' => 'erro'));
    	}

    }

    public function retornaNrSeriePedido()
	{
		$retorno = $this->nrSerieM->buscaNrSeriePorPedido($this->input->post('pedido_id'));
		echo json_encode($retorno);
	}

	public function anexarPintura(){
		
		if($_FILES['pintura']['name'] != 'NULL' && $_FILES['pintura']['name'] != '' ){

			$tipo 			= 	explode('.', $_FILES['pintura']['name']);
			$arquivo 		=	md5($_FILES['pintura']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
			
			$configuracao 	= array(	'upload_path'   	=> 	'./pedidos/',		        
							        	'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv|cdr|zip|rar|jpeg|JPEG',
							        	'file_name'     	=> 	$arquivo 	);      
		    		    
		    $this->load->library('upload', $configuracao);
		    
		    if ($this->upload->do_upload('pintura')){

		    	$data = explode('/', $this->input->post('dt_emissao_nf'));

		    	$update = array(	'id' 				=> 	$this->input->post('pedido_id'),
		    						'arquivo' 			=>	$arquivo,
		    						'pintura_descricao' => 	$this->input->post('descricao_pintura'),
		    						'testeira'			=> 	$this->input->post('testeira'),
		    						'painel_frontal'	=> 	$this->input->post('painel_frontal'),
		    						'painel_lateral'	=> 	$this->input->post('painel_lateral')	);
		    
		    	if( $this->pedidosM->atualizaPedidos($update) ){
		    		
		    		$insert = array(	'pedido_id'			=> 	$this->input->post('pedido_id'),
		    							'pintura' 			=> 	$arquivo,
		    							'pintura_descricao' => 	$this->input->post('descricao_pintura'),
		    							'usuario'			=>	$this->session->userdata('nome') 	 );

		    		$this->pedidosM->insereLogPinturas($insert);

		    		$email='<html>
								<head></head>
								<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
									<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
										<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Padrão de pintura do pedido '.$this->input->post('pedido_id').' foi alterado</h1>
										<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										Pedido: '.$this->input->post('pedido_id').'<br/>
										Arquivo: <a href="https://www.wertco.com.br/pedidos/'.$arquivo.'">'.$arquivo.'</a><br/>
										Descrição: '.$this->input->post('descricao_pintura').'<br/>
										Usuário Responsável: '.$this->session->userdata('nome').'<br/>
										</p>
									</div>
								</body>
							</html>';

		    		if( $this->enviaEmail('industrial@wertco.com.br','⛽ Padrão de pintura alterado - Pedido: '.$this->input->post('pedido_id'), $email,'./pedidos/'.$arquivo) ){
		    			$this->enviaEmail('producao@wertco.com.br','⛽ Padrão de pintura alterado - Pedido: '.$this->input->post('pedido_id'), $email,'./pedidos/'.$arquivo);
		    			$this->session->set_flashdata('sucesso', 'ok'); 
		    		}else{
		    			$this->session->set_flashdata('erro', 'erro');
		    		}
		    	}else{
		    		$this->session->set_flashdata('erro', 'erro');
		    	}
		    }else{
		    	$this->session->set_flashdata('erro', 'erro');
		    }
		}

	    $this->pedidos();

	}

	public function calcularTotalPedidos()
	{
		$status = 	implode(',', $this->input->post('status'));
		$total 	= 	$this->pedidosM->buscarValorTotalStatus($status);
		echo json_encode(array('total' => $total['total']));
	}

	public function relatorioVendasModeloFiltro(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Relatório de Vendas por Modelo";		
		$parametros['modelos']	=	$this->pedidosM->buscaModelosPedidos();

		$this->_load_view('area-administrador/pedidos/relatorio-vendas-modelo-filtro',$parametros );

    }

    public function gerarRelatorioVendasModeloFiltro(){
    	    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-vendas-modelo-filtro-pdf.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');

	    $dt_ini = '';
	    $dt_fim = '';
	    $data['periodo'] = '';
	    $where='1=1';
	    $data['modelo'] = '';
	    if($this->input->post('dt_fim') != '' && $this->input->post('dt_ini') != '' ){
	    	$explode1 	= 	explode('/',$this->input->post('dt_ini'));  	
	    	$dt_ini 	= 	$explode1[2].'-'.$explode1[1].'-'.$explode1[0];
	    	$explode 	= 	explode('/',$this->input->post('dt_fim'));  	
	    	$dt_fim 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    	$where.= 	" and op.dthr_geracao BETWEEN '".$dt_ini."' and '".$dt_fim."'";
	    	$data['periodo'] = $this->input->post('dt_ini').' a '.$this->input->post('dt_fim');
	    }
	   
	    if($this->input->post('produto_id') != '')
	    {
	    	$data['modelo'] = $this->input->post('modelo_desc');
	    	$where.= " and p.id =".$this->input->post('produto_id');
	    }
	    	    	    
	    $data['dados']	=	$this->pedidosM->relatorioMensalBombas($where);
		$this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/pedidos/relatorio-vendas-modelo-filtro-pdf', $data,true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-vendas-modelo-filtro-pdf.pdf'));
	    
		}

    }

    public function previsaoVendasDetalhado(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Relatório de Previsão de Vendas Detalhado";
		$parametros['modelos']	=	$this->produtosM->select();
		$this->_load_view('area-administrador/pedidos/previsao-vendas-detalhado',$parametros );

    }

    public function previsaoVendasDetalhadoAjax(){
    	
    	$filtros = array(	'produto_id' 	=> 	$this->input->post('produto_id')); 
		$data['dados']	=	$this->pedidosM->getRelatorioPrevisaoVendas($filtros);
		$retorno 		= 	$this->load->view('/area-administrador/pedidos/previsao-vendas-detalhado-ajax', $data, true);
		
		echo json_encode(array('retorno' 	=> 	$retorno ));
	}

	public function buscaOportunidades()
	{
		$data['dados'] 	= 	$this->pedidosM->buscaOportunidadesPorModelo($this->input->post('produto_id'));		
		$retorno 		= 	$this->load->view('/area-administrador/pedidos/busca-oportunidades-ajax', $data, true);		
		echo json_encode(array('retorno' 	=> 	$retorno ));
	
	}

	public function garantia()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->nrSerieM->buscaNrSerie();				
		$parametros['title']	=	"Gestão de Garantia";
		$this->_load_view('area-administrador/garantia/garantia',$parametros);
	}

	public function editarGarantia($id = null)
	{

		if($this->input->post()) {

			$dt_ini = explode('/',$this->input->post('dt_inicio_garantia'));
			$dt_fim = explode('/',$this->input->post('dt_fim_garantia'));
			$update = array('id'				=>	$this->input->post('id'),
							'dt_inicio_garantia'=>	$dt_ini[2].'-'.$dt_ini[1].'-'.$dt_ini[0],
							'dt_fim_garantia'	=>	$dt_fim[2].'-'.$dt_fim[1].'-'.$dt_fim[0] 	);
						 
			if($this->nrSerieM->update($update)){
				$this->log('Área Administrador | atualizar garantia ','bombas_nr_serie','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');				
				$this->garantia();

			}else{

				$this->session->set_flashdata('erro', 'erro');
				$this->garantia();
			}

		}else{

			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$this->nrSerieM->buscaNrSerieId($id);
			$parametros['title']				=	"Editar Garantia";			
			$this->_load_view('area-administrador/garantia/editar-garantia',$parametros );
		}
	}

	public function geraGarantia($pedido_id)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $arquivo = 'garantia-'.$pedido_id.'.pdf';
	    $this->html2pdf->filename($arquivo);
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $dados 	= 	$this->nrSerieM->buscaDadosGarantia($pedido_id);
	    	    
	    $data['empresa'] = array(	'empresa_id'		=>  $dados[0]['empresa_id'],
	    							'razao_social' 		=> 	$dados[0]['razao_social'],    								
									'cnpj' 				=> 	$dados[0]['cnpj'],
									'pedido_id'			=> 	$dados[0]['pedido_id'],
									'nr_nf'				=> 	$dados[0]['nr_nf'],
									'dt_emissao_nf' 	=> 	$dados[0]['dt_emissao_nf'] 	);

	    foreach ($dados as $dado) {
	    	$data['produtos'][] = array('codigo' 			=> 	$dado['codigo'],
	    								'descricao'			=>	$dado['descricao'],
										'modelo'			=>	$dado['modelo'],	    								
	    								'nr_serie'			=>	$dado['nr_serie'] );
	    }
	    //Load html view
	    $this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/garantia/garantia-pdf', $data, true)));
	    /*$conteudo = $this->load->view('/area-administrador/garantia/garantia-pdf', $data, true);
	    echo $conteudo;*/
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded	    	
			redirect(base_url('pdf/'.$arquivo));
	    }
	    
    }

    public function ordensPagto(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Ordens de Pagamentos";
		$parametros['dados']  	= 	$this->chamadoM->buscaTodasOrdensPagto();
		$this->_load_view('area-administrador/ordens-pagto/ordens-pagto',$parametros );
    }

    public function anexarNfeOp(){
		
		if($_FILES['nota_fiscal']['name'] != 'NULL' && $_FILES['nota_fiscal']['name'] != '' && $_FILES['comprovante']['name'] != 'NULL' && $_FILES['comprovante']['name']  != ''){

			$tipo 			= 	explode('.', $_FILES['nota_fiscal']['name']);
			$arquivo 		=	md5($_FILES['nota_fiscal']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
			
			$configuracao = array(
		        'upload_path'   	=> 	'./nfe/',		        
		        'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv|txt'		        
		    );
			$configuracao['file_name'] = $arquivo;
		    $this->load->library('upload', $configuracao);
		    
		    if ($this->upload->do_upload('nota_fiscal')){
		    	$tipo 		= 	explode('.', $_FILES['comprovante']['name']);
				$arquivo_c	=	md5($_FILES['comprovante']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];				

				
				$configuracao['file_name'] = $arquivo_c;

				$this->upload->initialize($configuracao);
				
				if($this->upload->do_upload('comprovante')){
					
			    	$update = array( 	'id' 				=> 	$this->input->post('op_id'),
			    						'nr_nf' 			=>	$this->input->post('nr_nf'),
			    						'nota_fiscal' 		=> 	$arquivo,
			    						'comprovante' 		=> 	$arquivo_c,
			    						'dthr_pagamento'	=> 	date('Y-m-d H:i:s'),
			    						'status_id'			=> 	3 );
			    
			    	if( $this->ordemPagtoM->atualizarOrdemPagamento($update) ){
			    		
			    		$email='<html>
							<head></head>
							<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
								<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
									<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Ordem de pagamento paga</h1>
									<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Nota Fiscal: '.$this->input->post('nr_nf').'<br/>
									cliente: '.$this->input->post('cliente').'<br/> 	
									Chamado: '.$this->input->post('chamado_id').'<br/>
									Ordem de Pagamento: '.$this->input->post('op_id').'<br/>
									Usuário: '.$this->session->userdata('nome').'
									</p>
								</div>
							</body>
						</html>';

			    		if( $this->enviaEmail('industrial@wertco.com.br','⛽ Ordem de pagamento paga', $email,'./nfe/'.$arquivo) ){
			    			$this->session->set_flashdata('sucesso', 'ok'); 
			    		}else{
			    			$this->session->set_flashdata('erro', 'erro');
			    		}
			    	}else{
			    		$this->session->set_flashdata('erro', 'erro');
			    	}
			    }else{
			    	echo 'Erro no comprovante'. $this->upload->display_errors();die;$this->session->set_flashdata('erro', 'erro');
			    }
			}else{
				echo 'erro na nota fiscal';
			}
		}

	    $this->ordensPagto();

	}

	public function buscaItensOp()
	{
		$dados = $this->ordemPagtoM->buscaItensOp($this->input->post('op_id'));
		if($dados != ''){
			$html = "<table class='table m-table'>";
			foreach($dados as $dado){
				$html.= "<tr>
							<td>".$dado['item']."</td>
							<td>".$dado['qtd']."</td>
							<td>".$dado['valor']."</td>
						</tr>";
			}
			$html.="</table>";

			echo json_encode(array('retorno' => $html));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function relatorioOrdemPagto(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Relatório de Ordens de Pagto";		
		$this->_load_view('area-administrador/assistencia-tecnica/relatorio-opagto',$parametros );

    }

    public function gerarRelatorioOrdemPagto()
    {
    	//Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-ordem-pagto.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');	    
	    
	    $where = array( 'dt_ini' 		=> 	$this->input->post('dt_ini'),
	    				'dt_fim' 		=> 	$this->input->post('dt_fim'), 
	    				'tipo_id' 		=> 	$this->input->post('tipo_id'),
	    				'cliente_id' 	=> 	$this->input->post('posto_id'),
	    				'favorecido_id'	=> 	$this->input->post('tecnico_id'),
	    				'estado'		=> 	$this->input->post('estado')	);
	    $data['periodo'] = "";
	    if( $this->input->post('dt_ini') != '' && $this->input->post('dt_fim') != '' ){
	    	$data['periodo'] = "Período: ".$this->input->post('dt_ini')." a ".$this->input->post('dt_fim');
	    }

	    $data['dados']	=	$this->ordemPagtoM->relatorioOrdemPagto($where);
				
		$this->html2pdf->html(utf8_decode($this->load->view('/area-administrador/assistencia-tecnica/relatorio-ordem-pagto-pdf', $data,true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-ordem-pagto.pdf'));
	    
		}
    } 
	
}
