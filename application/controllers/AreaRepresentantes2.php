<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');
class AreaRepresentantes2 extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() || $this->session->userdata('tipo_acesso') != 'representantes2'){
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('ProdutosModel', 'produtosM');
		$this->load->model('OrcamentosModel', 'orcamentosM');
		$this->load->model('IcmsModel', 'icmsM');
		$this->load->model('FretesModel', 'fretesM');
		$this->load->model('EntregasModel', 'entregasM');
		$this->load->model('FormaPagtoModel', 'formaPagtoM');
		$this->load->model('AreaAtuacaoModel', 'areaAtuacaoM');
		$this->load->model('PedidosModel' , 'pedidosM');
		$this->load->model('ZonaAtuacaoModel' , 'zonaM');		
		$this->load->helper('form');

	}
 
	public function index()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();
		$parametros['title']	=	"Área do Administrador do sistema";
		$this->_load_view('area-representantes2/index',$parametros);
	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone') 	);

			if( $_FILES['nome_arquivo']['name'] != ""){

				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha')));
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-representantes2/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-representantes2/editar-cliente',	$parametros );	
		}
	
	} 	

	
	public function orcamentos()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->orcamentosM->buscaOrcamentosRepresentantesStatus($this->session->userdata('usuario_id'),1);				
		$parametros['title']	=	"Orçamentos";
		$this->_load_view('area-representantes2/orcamentos/orcamentos-rep2',$parametros);
	}

	public function orcamentosOutros()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->orcamentosM->buscaOrcamentosRepresentantesStatus($this->session->userdata('usuario_id'),2);				
		$parametros['title']	=	"Orçamentos";
		$this->_load_view('area-representantes2/orcamentos/orcamentos-rep2',$parametros);
	}

	public function buscaProximosStatus()
	{	
		$retorno = $this->orcamentosM->buscaProximosStatusOrcamento($_POST['orcamento_id']);			
		echo json_encode($retorno);
		
	}

	public function buscaAndamentos()
	{
		$retorno = $this->orcamentosM->buscaAndamentos($_POST['orcamento_id']);			
		echo json_encode($retorno);
		
	}
	
	public function alteraStatusOrcamento()
	{
		$dados = array('id'						=>	$_POST['orcamento_id'],
					   'status_orcamento_id'	=>	$_POST['status_orcamento']);

		if($this->orcamentosM->atualizaStatusOrcamento($dados))
		{

			switch($_POST['status_orcamento']){
				
				case 1:
			        $andamento = "Orçamento Aberto!";
			        break;
			    case 2:
			        $andamento = "Orçamento Fechado, venda realizada!";
			        break;
			    case 3:
			        $andamento = "Orçamento Perdido para concorrência.";
			        break;
			    case 4:
			        $andamento = "Orçamento Cancelado.";
			        break;
			    case 5:
			        $andamento = "Orçamento entregue ao cliente.";
			        break;        
			    case 6:
			        $andamento = "Orçamento em negociação.";
			        break;
			    case 10:
				    $andamento = "Orçamento Perdido para Wayne.";
				        break;
			    case 11:
			        $andamento = "Orçamento Perdido para Gilbarco.";
			        break;

			}

			$inserirAndamento = array('orcamento_id'			=>	$_POST['orcamento_id'],
										'status_orcamento_id'	=> 	$_POST['status_orcamento'],
										'andamento'				=>	$andamento,
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id'));

			if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
			{
				echo json_encode(array('retorno' => 'sucesso'));	
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}

	public function insereAndamentoOrcamento(){
		
		$inserirAndamento = array('orcamento_id'		=>	$_POST['orcamento_id'],
								'status_orcamento_id'	=> 	$_POST['status_orcamento_id'],
								'andamento'				=>	$_POST['andamento'],
								'dthr_andamento'		=>	date('Y-m-d H:i:s'),
								'usuario_id'			=> 	$this->session->userdata('usuario_id'));

		if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
		{
			echo json_encode(array('retorno' => 'sucesso'));	
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}			

	}

	public function retornaRepresentantes()
	{
		
		$rows = $this->usuariosM->retornaRepresentante($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label' 		=>	$row['label'],
								'id'		=>	$row['id'],
								'value'		=>	$row['label']	);				
				
			}

			echo json_encode($dados);
		}

	}

	public function visualizarOrcamento($id) 
	{
		$parametros 				= 	$this->session->userdata();
		$parametros['dados']		=	$this->orcamentosM->getOrcamentoProdutos(base64_decode($id));
		$parametros['desconto']		=	$this->orcamentosM->getOrcamentoDesconto(base64_decode($id));
		$parametros['solicitante']	=	$this->orcamentosM->getSolicitante(base64_decode($id));
		$parametros['indicador']	= 	$this->orcamentosM->buscaIndicador(base64_decode($id));
		$parametros['bombas'] 		= 	$this->produtosM->select();
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['title']		=	"Orçamento #".$id;		
		$this->_load_view('area-representantes2/orcamentos/visualiza-orcamento-rep2',$parametros);

	}


	public function emitirOrcamento()
	{
			
		
		$status_orcamento_id = $_POST['status_orcamento_id'];		

		//altera o status para emitido	
		$dados = array('id'						=>	$_POST['orcamento_id'],
			   		   'status_orcamento_id'	=>	$status_orcamento_id);

		if($this->orcamentosM->atualizaStatusOrcamento($dados)){

			//orçamento entregue/emitido
			$andamentoOrcamento = array(	'andamento' 			=> 	'Orçamento emitido ao cliente para o email '.$_POST['email_destino'].', por '.$this->session->userdata('nome').'',
		 									'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
		 									'orcamento_id'			=> 	$_POST['orcamento_id'],
		 									'status_orcamento_id'	=> 	$status_orcamento_id,
		 									'usuario_id'			=> 	$this->session->userdata('usuario_id')	);	

	 		if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento)){

				$retorno = $this->enviaEmailEmissaoCliente($_POST['email_destino'],$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');

				if($retorno){
					echo json_encode(array('retorno' => 'sucesso'));
				}else{
					echo json_encode(array('retorno' => 'erro'));	
				}
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}	
		
	}

	private function enviaEmailEmissaoCliente( $email_destino, $orcamento_id, $valor_tributo, $valor_total, $contato_posto )
	{
		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);
		
		$anexo = $this->geraOrcamento($orcamento_id,'email',$contato_posto);

		$email = '<html>
		<head></head>
		<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
		<div class="content" style="width: 600px; height: 100%;">
		<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/backgroundCabecalho.png); height: 528px; "></div>
		<div style="width: 600px; background-color: #ffcc00; height: 100%">
			<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
				<tbody>
					<tr>
						<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
						<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
						<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
						<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
					</tr>
					<tr>
						<td style="padding-bottom: 18px;font-weight: 700;">Validade:</td>
						<td style="padding-bottom: 18px;">30 Dias</td>
						<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
						<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
					</tr>
					<tr>
						<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
						<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
						<td style=" font-weight: 700; text-align: right;">Telefone:</td>
						<td>'.$empresa['telefone'].'</td>
					</tr>
					<tr>
						<td style=" font-weight: 700;">E-mail:</td>
						<td colspan="3">'.$empresa['email'].'</td>
					</tr>
				</tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
			<table style="width: 570px;margin-left: 14px;" cellspacing="0">
				<thead style="height: 55px;">
					<tr><th style="    height: 30px;">Modelo</th>
					<th>Descrição</th>
					<th>Quantidade</th>
					<th>Valor</th>
				</tr></thead>
				<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
				$total = 0; foreach($produtos as $produto){
					
					$email.='	<tr>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
								<td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>
								<td style="   text-align: right; padding: 10px;">R$ '.number_format($produto['valor'], 2, ',', '.').'</td>
							</tr>';					
					$total = $total + ($produto['valor'] * $produto['qtd']);
				}							
		$email.='	<tr>
						<td colspan="3" style="border-top: 1px solid #f0f0f0; border-right: 1px solid #f0f0f0;    padding: 10px;text-align: right;">
						<b>total</b></td>
						<td style="border-top: 1px solid #f0f0f0; text-align: right; padding: 10px;"><b>R$ '.number_format($total, 2, ',', '.').'</b></td>
					</tr>';								
		$email.='		
				</tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
		</div>
	</div>
</body>
</html>';	
		
		$this->load->library('email');
		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to($email_destino)
		    ->subject('Orçamento Wertco')
		    ->message($email)
		    ->attach('./pdf/'.$anexo)
		    ->send();

		return $result;		
	
	}

	public  function geraOrcamentoPdf()
	{
		$retorno = $this->geraOrcamento($this->input->post('orcamento_id'),'post',$this->input->post('contato_posto'));		
		redirect('./pdf/'.$retorno);
	}

	/************************************************************ 
	************** GERA PDF DO ORÇAMENTO SOLICITADO *************
	*************************************************************/
	public function geraOrcamento($orcamento_id, $origem_solicitacao,$contato)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-'.$orcamento_id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $dados				=	$this->orcamentosM->getOrcamentoProdutos($orcamento_id);
	    $total_descontos 	= 	$this->orcamentosM->getOrcamentoDesconto($orcamento_id);
	    $responsavel 		= 	$this->orcamentosM->getResponsavel($orcamento_id);	    
	    $orcamento 			= 	$this->orcamentosM->getFreteEntregaPagto($orcamento_id);
	    $indicador			= 	$this->orcamentosM->getIndicador($orcamento_id);
	    $data['frete']		= 	(is_object($orcamento)) ? $orcamento->frete : ' ';
	    $data['entrega']	= 	(is_object($orcamento)) ? $orcamento->entrega : ' ';
	    $data['formaPagto'] = 	(is_object($orcamento)) ? $orcamento->forma_pagto : ' ';
	    $data['emissao']	= 	$this->orcamentosM->retornaDiferencaEmissao($orcamento_id);
	    $data['primeira_emissao']	= 	$this->orcamentosM->primeiraEmissao($orcamento_id);
	    
	    $data['empresa'] = array(	'razao_social' 			=> 	$dados[0]->razao_social,
    								'fantasia' 				=> 	$dados[0]->fantasia,
									'cnpj' 					=> 	$dados[0]->cnpj,
									'telefone'				=> 	$dados[0]->telefone,
									'email'					=> 	$dados[0]->email,
									'endereco'				=> 	$dados[0]->endereco,
									'cidade'				=> 	$dados[0]->cidade,		
									'pais'					=> 	$dados[0]->pais,
									'estado'				=> 	$dados[0]->estado,
									'orcamento_id'			=> 	$dados[0]->orcamento_id,
									'emissao'				=> 	$dados[0]->emissao,
									'valor_tributo'			=>	$dados[0]->valor_tributo,
									'representante_legal' 	=> 	$dados[0]->representante_legal,
									'inscricao_estadual' 	=> 	$dados[0]->insc_estadual,
									'bairro'				=> 	$dados[0]->bairro,
									'cep'					=> 	$dados[0]->cep,
									'observacao'			=> 	$dados[0]->observacao,									
									'tp_cadastro'			=> 	$dados[0]->tp_cadastro,
									'celular'				=> 	$dados[0]->celular,
									'fl_especial' 			=> 	$dados[0]->fl_especial,
									'dt_previsao'			=> 	$dados[0]->dt_previsao	);

	    foreach ($dados as $dado) {
	    	$data['produtos'][] = array('codigo' 			=> 	$dado->codigo,
	    								'descricao'			=>	$dado->descricao,
										'modelo'			=>	$dado->modelo,
	    								'qtd'				=>	$dado->qtd,
	    								'valor_unitario' 	=>	$dado->valor_unitario,
	    								'valor_orcamento'	=> 	$dado->valor_orcamento,
	    								'valor_produto' 	=> 	$dado->valor);
	    }		

	    $data['info']  	= 	array(	'nome' 			=> 	$this->session->userdata('nome'),
	    						 	'email'			=> 	$this->session->userdata('email'),
	    						 	'telefone'		=>	$this->session->userdata('telefone'),	    						 	
	    						 	'contato'		=> 	( isset($_POST['contato'])) ? $_POST['contato'] : $contato );

	    $data['representante'] 	= 	array(	'responsavel' 	=>  $responsavel->responsavel,
								  			'empresa' 		=>	$responsavel->razao_social);

	    foreach( $total_descontos as $total_desconto ){
	    	$data['desconto'] = array('valor_desconto' => $total_desconto['valor_desconto'] );
	    }

	    if(!isset($data['desconto'])){
	    	$data['desconto'] = array('valor_desconto' => '' );
	    }

	    $data['indicador'] = array('indicador' 	=> 	(count($indicador) > 0) ? $indicador[0]->indicador : '');	    
 
	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-representantes2/orcamentos/orcamento-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'orcamento-'.$orcamento_id.'.pdf'));
	    	}else{
	    		return $retorno['retorno'] = 'orcamento-'.$orcamento_id.'.pdf';
	    	}
	    }	    
	    
    }

    public function cadastraOrcamentos()
	{
		$parametros 				= 	$this->session->userdata();
		$parametros['dados']		=	$this->orcamentosM->buscaOrcamentosIndicadores($parametros['usuario_id']);		
		$parametros['bombas'] 		= 	$this->produtosM->select();		
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['title']		=	"Orçamentos";
		$this->_load_view('area-representantes2/orcamentos/cadastra-orcamento-rep2',$parametros);	
	}

	public function orcamentosRep()
	{
		if( $this->input->post() )
		{
			
			if( $this->input->post('salvar') == '1')
			{			
				$dadosEmpresa = array(
			        'razao_social' 			=>	$this->input->post('razao_social'),
					'fantasia' 				=>	$this->input->post('fantasia'),
					'cnpj' 					=>	$this->input->post('cnpj'),
					'telefone' 				=>	$this->input->post('telefone'),
					'endereco' 				=>	$this->input->post('endereco'),
					'email' 				=>	$this->input->post('email'),				
					'cidade' 				=>	$this->input->post('cidade'),
					'estado'				=>	$this->input->post('estado'),
					'pais'					=>	$this->input->post('pais'),	
					'bairro'				=>	$this->input->post('bairro'),
					'cep'					=>	$this->input->post('cep'),
					'insc_estadual' 		=>	$this->input->post('insc_estadual'),
					'tipo_cadastro_id'		=>	1,
					'cartao_cnpj'			=>	$this->input->post('cartao_cnpj'),
					'codigo_ibge_cidade'	=> 	$this->input->post('codigo_ibge_cidade')
				);			

				if($this->empresasM->add($dadosEmpresa))
				{		
					$empresa_id = $this->db->insert_id();			

				}
	 		}else{
	 			$empresa_id = $this->input->post('empresa_id');
	 		}
	 		//$entrega_id = $this->input->post('entrega');
	 		$entrega_id = 17;
	 		$dadosOrcamento = array('empresa_id' 			=> 	$empresa_id,
	 								'status_orcamento_id'	=> 	1,
	 								'origem'				=> 	'Representante nvl.2',
	 								'solicitante_id'		=> 	$this->session->userdata('usuario_id'),
	 								'usuario_id'			=> 	$this->session->userdata('usuario_id'),
	 								'indicador_id'			=> 	$this->session->userdata('usuario_id'),
	 								'frete_id'				=> 	$this->input->post('frete'),
	 								'entrega_id'			=> 	$entrega_id,
	 								'forma_pagto_id'		=> 	$this->input->post('forma_pagto'),
									'contato_posto'			=> 	$this->input->post('contato_posto'),
									'dt_previsao'			=> 	date('Y-m-d',strtotime('+68 days'))	);

	 		if( $this->orcamentosM->insereOrcamento($dadosOrcamento) )
	 		{
	 			$orcamento_id  = $this->db->insert_id();
	 			$erro = 0;

	 			$dadosContato = array(	'empresa_id' 		=> 	$empresa_id,
	 									'nome'				=> 	$this->input->post('contato_posto'),
	 									'email'				=> 	$this->input->post('email_contato_posto'),
	 									'celular'			=> 	$this->input->post('tel_contato_posto'),
	 									'tipo_cadastro_id'	=> 1);

	 			if( $this->usuariosM->add($dadosContato) )
	 			{

	 				$contato_id = $this->db->insert_id();
	 				$updateContato = array(	'id' 			=> 	$orcamento_id,
	 										'contato_id'	=> 	$contato_id	);
	 				$this->orcamentosM->atualizaOrcamento($updateContato);

		 			$andamentoOrcamento = array('andamento' 			=> 'Orçamento realizado pelo Representante nvl2. '.$this->session->userdata('nome').' !',
			 									'dthr_andamento'		=> date('Y-m-d H:i:s'),
			 									'orcamento_id'			=> $orcamento_id,
			 									'status_orcamento_id'	=> 1,
			 									'usuario_id'			=> $this->session->userdata('usuario_id')
			 									);	

		 			$comissao  = array(	'orcamento_id' 		=> 	$orcamento_id,
		 								'valor_desconto' 	=> 	$this->input->post('valor_desconto_orc'),
		 								'motivo_desconto'	=> 	'Comissão Indicador' );

		 			if(	$this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento) && $this->orcamentosM->insereOrcamentosDesconto($comissao) )
		 			{
		 				$erro = $erro;
		 				$orcamentoResp = array(	'orcamento_id' 	=> 	$orcamento_id,
			 									'usuario_id' 	=> 	$this->input->post('responsavel_id') );
	 					if($this->orcamentosM->insereResponsavelOrcamento($orcamentoResp))
	 					{
	 						$erro = $erro;
	 					
				 			for ($i=0; $i < count($this->input->post('produto')) ; $i++) 	 			  
				 			{ 
				 				$orcamentoProdutos = array(	'orcamento_id' 	=>	$orcamento_id,
				 											'produto_id'  	=> 	$this->input->post('produto')[$i],
				 											'qtd'			=>	$this->input->post('quantidade')[$i],
				 											'valor'			=> 	$this->input->post('valor')[$i]	);	

				 				if($this->orcamentosM->insereOrcamentoProdutos($orcamentoProdutos))
				 				{
				 					$erro = $erro;
				 					
								}else{
									$erro++;
								}		 					 			
				 			}
				 		}else{
							$erro++;
						}	 					

					}else{
						$erro++;
					}
				}else{
					$erro++;
				}
			}else{
				$erro++;
			}

	 		if( $erro == 0){	 				
	 				
	 			$email_enviar = $this->zonaM->buscaEmailResponsavel($this->session->userdata('usuario_id'));

	 			if($this->enviarEmailOrcamento($orcamento_id, 'todosvendas@wertco.com.br', 'Orçamento realizado no Sistema pela Revenda nvl.2: '. $this->session->userdata('nome').'. Zona:'. $email_enviar['nome']))
 				{
 					
 					$this->session->set_flashdata('sucesso', 'ok.');
 				}else{
 					
 					$this->session->set_flashdata('erro', 'erro.');	
 				}
 			}else{
 				$this->session->set_flashdata('erro', 'erro.');
 			}

			$this->orcamentos();
		}

	}

	public function insereDescontoOrcamento()
	{
		$dados_desconto = array('orcamento_id' 		=> 	$_POST['orcamento_id'],
								'valor_desconto'	=>	str_replace(',', '.', $_POST['valor_desconto']));

		if( $this->orcamentosM->insereOrcamentosDesconto( $dados_desconto ) )
		{
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}		

	public function alterarValorProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['id'],
							 	'valor'	=>	(strpos($_POST['valor'],',') === false ) ? $_POST['valor'] : str_replace(',','.', str_replace('.','',$_POST['valor'])) );

		if($this->orcamentosM->atualizaValorProduto($dadosUpdate)){
			if( isset($_POST['comissao']) ){

				$comissao = array('orcamento_id' 	=> $_POST['orcamento_id'],
								  'valor_desconto'	=> $_POST['comissao']);

				$this->orcamentosM->atualizaComissao($comissao);

			}
			echo json_encode(array('retorno' => 'sucesso' ));						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}

	}

	public function alterarProdutoOrcamento(){

		$dadosUpdate = array(	'id' 				=> 	$_POST['orcamento_produto_id'],
							 	'produto_id'		=>	$_POST['produto_id'],
							 	'valor' 			=>	$_POST['valor_produto']	);

		if($this->orcamentosM->alterarProdutoOrcamentorep($dadosUpdate)){
			
			echo json_encode(array('retorno' => 'sucesso' ));						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarQtdProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['orcamento_produto_id'],
							 	'qtd'	=>	$_POST['qtd']	);

		if($this->orcamentosM->alterarQtdProduto($dadosUpdate)){
			
			echo json_encode(array('retorno' => 'sucesso' ));						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	private function enviarEmailOrcamento($orcamento_id){

		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);		
		$email = '<html>
		<head></head>
		<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
		<div class="content" style="width: 600px; height: 100%;">
		<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/backgroundCabecalho.png); height: 528px; "></div>
		<div style="width: 600px; background-color: #ffcc00; height: 100%">
			<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
				<tbody>
					<tr>
						<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">Orçamento:</td>
						<td style="padding-bottom: 18px;font-weight: 700;">'.$orcamento_id.'</td>
						<td style="padding-bottom: 18px;font-weight: 700;"></td>
						<td style="padding-bottom: 18px;font-weight: 700;"></td>
					</tr>
					<tr>
						<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
						<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
						<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
						<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
					</tr>
					<tr>
						<td style="padding-bottom: 18px;font-weight: 700;">Validade:</td>
						<td style="padding-bottom: 18px;">30 Dias</td>
						<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
						<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
					</tr>
					<tr>
						<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
						<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
						<td style=" font-weight: 700; text-align: right;">Telefone:</td>
						<td>'.$empresa['telefone'].'</td>
					</tr>
					<tr>
						<td style=" font-weight: 700;">E-mail:</td>
						<td colspan="3">'.$empresa['email'].'</td>
					</tr>
				</tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
			<table style="width: 570px;margin-left: 14px;" cellspacing="0">
				<thead style="height: 55px;">
					<tr><th style="    height: 30px;">Modelo</th>
					<th>Descrição</th>
					<th>Quantidade</th>					
				</tr></thead>
				<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
				$total = 0; foreach($produtos as $produto){
					
					$email.='	<tr>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
								<td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>						
							</tr>';					
					$total = $total + ($produto['valor'] * $produto['qtd']);
				}			
		
			$email.=' </tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
		</div>
	</div>
</body>
</html>';	

		$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('webmaster@wertco.com.br')    // Optional, an account where a human being reads.
		    ->to('todosvendas@wertco.com.br')
		    ->subject('Orçamento realizado no Sistema pelo representante nvl2' . $this->session->userdata('nome'))
		    ->message($email)
		    ->send();

		return $result;		
	}

	private function enviarEmailPedido($orcamento_id){

		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);		
		$email = '<html>
					<head></head>
					<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
					<div class="content" style="width: 600px; height: 100%;">
					<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/backgroundCabecalhoPedido.png); height: 528px; "></div>
					<div style="width: 600px; background-color: #ffcc00; height: 100%">
						<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
							<tbody>
								<tr>
									<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
									<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
									<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
									<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
								</tr>
								<tr>
									<td style="padding-bottom: 18px;font-weight: 700;">Validade:</td>
									<td style="padding-bottom: 18px;">30 Dias</td>
									<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
									<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
								</tr>
								<tr>
									<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
									<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
									<td style=" font-weight: 700; text-align: right;">Telefone:</td>
									<td>'.$empresa['telefone'].'</td>
								</tr>
								<tr>
									<td style=" font-weight: 700;">E-mail:</td>
									<td colspan="3">'.$empresa['email'].'</td>
								</tr>
							</tbody>
						</table>
						<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
						<table style="width: 570px;margin-left: 14px;" cellspacing="0">
							<thead style="height: 55px;">
								<tr>
									<th style="height: 30px;">Modelo</th>
									<th>Descrição</th>
									<th>Quantidade</th>					
								</tr>
							</thead>
							<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
							$total = 0; foreach($produtos as $produto){
								
								$email.='<tr>
											<td style="border-right: 1px solid #f0f0f0; padding: 10px;">'.$produto['modelo'].'</td>
											<td style="border-right: 1px solid #f0f0f0; padding: 10px;">'.$produto['descricao'].'</td>
											<td style="text-align: center;border-right: 1px solid #f0f0f0;	padding: 10px;">'.$produto['qtd'].'</td>						
										</tr>';					
								$total = $total + ($produto['valor'] * $produto['qtd']);
							}			
					
						$email.=' </tbody>
						</table>
						<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
					</div>
				</div>
			</body>
			</html>';	

		$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')
		    ->to('todosvendas@wertco.com.br') 
		    ->subject('Pedido realizado no Sistema Pelo Representante Nvl.2: '.$this->session->userdata('nome'))
		    ->message($email)
		    ->send();

		return $result;		
	}

   	public function verificaEstadoIcms()
	{
		$dados = $this->icmsM->getIcmsPorEstado($_POST['estado_id']);
		if( $dados[0]['total'] > 0 ){
			echo json_encode(array('retorno' => 'erro'));
		}else{
			echo json_encode(array('retorno' => 'sucesso'));
		}

	}

	public function insereProdutosNovosOrcamento(){
		$erro = 0;
		
		foreach ($_POST['dados'] as $dados) {
			if( $dados['name'] == 'produto_id'){
				$insert['produto_id'] = $dados['value'];
				$i=1;
			}
			if( $dados['name'] == 'qtd'){
				$insert['qtd'] = $dados['value'];
				$i=2;
			}
			if( $dados['name'] == 'valor_unitarios'){
				$insert['valor'] = $dados['value'];
				$i=3;
				$insert['orcamento_id'] = $_POST['orcamento_id'];
				if($this->orcamentosM->insereOrcamentoProdutos($insert)){
					$erro = $erro;
				}else{
					$erro++;
				}
			}			
		}

		if($erro == 0){
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Produto Adicionado ao orçamento por '.$this->session->userdata('nome'),
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
			
			$this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Icms ******************************
	*****************************************************************************/
	public function excluirProdutoOrcamento()
	{
		if($this->orcamentosM->excluirProduto($_POST['orcamento_produto_id'])){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function alterarFormaPagto()
	{
		$dadosUpdate = array(	'id' 				=> 	$_POST['orcamento_id'],
							 	'forma_pagto_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarFormaPagto($dadosUpdate)){
			
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de Pagamento alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarEntrega(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['orcamento_id'],
							 	'entrega_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarEntrega($dadosUpdate)){
			
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de entrega alterada.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarFrete(){
		
		$dadosUpdate = array(	'id' 		=> 	$_POST['orcamento_id'],
							 	'frete_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarFrete($dadosUpdate)){
			
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Frete alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function verificaIcms()
	{
		$dados = $this->icmsM->getIcmsPorUF($_POST['estado']);
		echo json_encode(array('fator' => $dados[0]->fator,
								'valor_tributo' => $dados[0]->valor_tributo ) 	);

	}

	public function retornaIndicadores()
	{
		
		$rows = $this->usuariosM->retornaIndicadores($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label' 	=>	$row['label'],
								 'id'		=>	$row['id'],
								 'value'	=>	$row['label'] );
				
			}

			echo json_encode($dados);
		}
	}

	public function ValidaCnpj(){
    	$cnpj 			= 	$this->input->post('cnpj');		
		$consulta_rf 	= 	$this->ConsultaCNPJRF($cnpj);

        if (isset($consulta_rf) && $consulta_rf->{'code'} == '1')
        {
            if ($consulta_rf->{'data'}->{'situacao_cadastral'} == 'ATIVA')
            {
            	$arr['retorno'] 	= 	$consulta_rf->{'data'};
            	$arr['save'] 		= 	$consulta_rf->{'Save_Receita'};
                $arr['result'] 		= 	true;
                $arr['situacao'] 	= 	$consulta_rf->{'data'}->{'situacao_cadastral'};
            	//echo json_encode($consulta_rf);            	
                
            	$arr['result'] 	= 	true;
				$arr['erro'] 	= 	0;

            }else {
            	$arr['erro'] 	= 	1;
            	$arr['msg'] 	= 	'* Situação cadastral '.$consulta_rf->{'data'}->{'situacao_cadastral'}.' na Receita Federal.'; 
            }
		}else {

            if (isset($consulta_rf)){
            	$arr['erro'] 	= 	1;
            	$arr['msg'] 	= 	'* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 2 - '.$consulta_rf->{'code'}.')';
            }else{
            	$arr['erro'] 	= 	1;
            	$arr['msg'] 	= 	'* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 3)';
            }

        }

		echo json_encode(array(	'receita' 	=> $arr));
	}

	public function areaAtuacao()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->areaAtuacaoM->select($parametros['empresa_id']);
		$parametros['title']	=	"Área de Atuação";
		$this->_load_view('area-representantes2/area-atuacao/area-atuacao',$parametros);	
	}

	public function cadastraArea()
	{
		if(	$this->input->post() ){
			
			$empresa_id = $this->session->userdata('empresa_id');
			$erro = 0;

			//upload do arquivo
			$tipo 		= 	explode('.', $_FILES['arquivo']['name']);
			$arquivo 	=	md5($_FILES['arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
			
			$configuracao = array(
		        'upload_path'   	=> 	'./downloads/',		        
		        'allowed_types' 	=> 	'xls',
		        'file_name'     	=> 	$arquivo
		    );      

		    $this->load->library('upload', $configuracao);
		    
		    if ($this->upload->do_upload('arquivo')){
		    	$xls = $this->lerXls($arquivo);
		    	$msg_erro = ' As seguintes cidades não foram inseridas: ';
		    	foreach($xls as $dado){
		    		if( count($dado) == 3 ){
			    		if( utf8_encode($dado[1]) != 'Município' ){			    		
				    		
				    		if($dado[3] != ''){
								$insert =	array(	'empresa_id'		=>	$empresa_id,
													'cidade'			=>	utf8_encode($dado[1]),
													'estado'			=> 	$dado[2],
													'cidade_cod_ibge'	=> 	$dado[3]	);
								
								if(!$this->areaAtuacaoM->add($insert)){
									$erro = 1;
									$msg_erro.= utf8_encode($dado[1]).". ";
								}
							}else{
								$erro = 1;
								$msg_erro.= utf8_encode($dado[1]).". "; 
							}
						}					
					}else{
						$erro = 2;
						
					}
		    	}
		    	
		    }		

			if($erro == 0){
	    		$this->session->set_flashdata('sucesso_cadastro', TRUE);

	    	}else{
	    		if( $erro == 1){
	    			$msg_erro.= 'Insira essas cidades manualmente.';		    	
	    			
	    		}else{
	    			$msg_erro = "Arquivo fora do padrão, corrija o arquivo ou insira as cidades manualmente.";
	    			
	    		}
	    		
	    		$this->session->set_flashdata(	'erro',	TRUE);
	    		$this->session->set_flashdata(	'msg_erro',	$msg_erro	);
	    	}

			$this->areaAtuacao();

		}else{

			$parametros 				= 	$this->session->userdata();
			$parametros['title']		=	"Cadastro de área de atuação";
			$this->_load_view('area-representantes2/area-atuacao/cadastra-area',$parametros);
		}
	}

	public function salvarAreasAjax(){
		$erro = 0;
		$empresa_id = $this->session->userdata('empresa_id');
		foreach($this->input->post('cidades') as $key => $value){
			
			$insert = array(	'empresa_id'		=>	$empresa_id,
								'cidade'			=>	$value,
								'estado'			=> 	$this->input->post('estado'),
								'cidade_cod_ibge'	=> 	$this->input->post('codigos')[$key]	);
			
			if(!$this->areaAtuacaoM->add($insert)){
				$erro = 1;
			}
		}

		if($erro == 0){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}

	public function excluirAreaAtuacao()
	{
		if( $this->input->post('id') ){
			if($this->areaAtuacaoM->excluir($this->input->post('id')) ){

				echo json_encode(array('retorno' => 'sucesso'));
				
			}else{
				echo json_encode(array('retorno' => 'sucesso'));
			}

		}
	}

	public function lerXls($arquivo){

		$this->load->library('Spreadsheet_Excel_Reader');
		$excel = new Spreadsheet_Excel_Reader();		
		$excel->read('downloads/'.$arquivo);
        return $excel->sheets[0]['cells'];

	}

	private function retiraSpecialChars($string){
		return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(Ç)/","/(ç)/"),explode(" ","a A e E i I o O u U n N C c"),str_replace(' ','-',utf8_encode($string)));
	}


	public function gestaoFuncionarios()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	= 	$this->usuariosM->getTodosUsuariosEmpresa($parametros['empresa_id']);		
		$parametros['title']	=	"Área do Representante - Gestão de Funcionários";		
		
		$this->_load_view('area-representantes2/gestao-funcionarios/gestao-funcionarios-rep',$parametros);
	}

	public function cadastraFuncionario()
	{
		if( $this->input->post()  ){
			$dadosUsuarios = array(	'nome'					=>	$this->input->post('nome'),
									'cpf' 					=>	$this->input->post('cpf'),
									'telefone' 				=>	$this->input->post('telefone'),
									'celular' 				=>	$this->input->post('celular'),
									'email' 				=>	$this->input->post('email'),					
									'empresa_id'			=>	$this->input->post('empresa_id'),
									'tipo_cadastro_id'		=>	$this->input->post('tipo_cadastro_id'),
									'senha'					=>	md5(md5($this->input->post('senha'))),									
									'ativo'					=> 	$this->input->post('ativo')	);

			if($this->usuariosM->add($dadosUsuarios)){

				$this->session->set_flashdata('sucesso_cadastro', 'ok.');	
				$this->gestaoFuncionarios();		
			}else{

				$this->session->set_flashdata('erro', 'erro.');			
				$this->gestaoFuncionarios();
			}

		}else{
			$parametros 					= 	$this->session->userdata();
			$parametros['subtipo_cadastro']	= 	$this->empresasM->getSubtipoByTipo(4);
			$parametros['title']			=	"Área do Representante - Cadastro de Funcionários";
			
			$this->_load_view('area-representantes2/gestao-funcionarios/cadastra-funcionario',$parametros);
		}
	} 

	/****************************************************************************
	**************** Método Ajax - Excluir Funcionário **************************
	*****************************************************************************/
	public function excluirFuncionario()
	{
		if($this->usuariosM->excluirUsuario($_POST['id'])){

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function visualizarFuncionario($id)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	= 	$this->usuariosM->findMarca($id);
		$parametros['title']	=	"Visualizar Informações Funcionário ".$parametros['dados']['nome'];		
		
		$this->_load_view('area-representantes2/gestao-funcionarios/visualizar-funcionario-rep',$parametros);
	}

	public function editarFuncionario($id)
	{
		
		if(	$this->input->post()	){
			
			$update = $this->input->post();			
			unset($update['salvar']);
			unset($update['senha2']);
			unset($update['senha']);
			
			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha')));
			}
			 
			if($this->usuariosM->atualizaUsuario($update)){
				
				$this->session->set_flashdata('sucesso_editar', 'ok');				
				$this->gestaoFuncionarios();	
			}else{
				$this->session->set_flashdata('erro', 'erro');				
				$this->gestaoFuncionarios();	
			}

		}else{
			
			$parametros 					= 	$this->session->userdata();
			$parametros['dados']			= 	$this->usuariosM->findMarca($id);
			$parametros['subtipo_cadastro']	= 	$this->empresasM->getSubtipoByTipo(4);
			$parametros['title']			=	"Área do Representante - Editar Informações Funcionário ".$parametros['dados']['nome'];
			
			$this->_load_view('area-representantes2/gestao-funcionarios/editar-funcionario',$parametros);
		}
	}

	public function pedidos()
	{
	
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->pedidosM->selectPedidos();
		$parametros['title']	=	"Pedidos";
		$this->_load_view('area-representantes2/pedidos/pedidos-rep2',$parametros);
	
	}

	public function cadastraPedidos()
	{
		if($this->input->post('salvar') == 1){
			if($this->input->post('frete_id') == 2 ){
				$frete = 'C';
			}elseif($this->input->post('frete_id') == 3){
				$frete = 'F';
			}else{
				$frete = 'O';
			}

			$dados  = array( 	'orcamento_id'		=> 	$this->input->post('orcamento_id'),
								'pintura'			=>	$this->input->post('pintura'),
								'motor'				=>	$this->input->post('motor'),
								'combustiveis'		=> 	$this->input->post('combustiveis'),
								'fl_receber_info'	=> 	$this->input->post('fl_receber_info'),
								'status_pedido_id'	=> 	1,
								'usuario_geracao'	=>	$this->session->userdata('usuario_id'),
								'tecnico_id'		=> 	$this->input->post('tecnico_id'),
								'endereco_entrega'	=> 	$this->input->post('endereco_entrega'),
								'nome_cliente'		=> 	$this->input->post('nome_cliente'),
								'rg_cliente'		=> 	$this->input->post('rg_cliente'),
								'cpf_cliente'		=> 	$this->input->post('cpf_cliente'),
								'pintura_descricao'	=> 	$this->input->post('pintura_descricao'),
								'empresa_id'		=> 	$this->input->post('empresa_id'),
								'valor_total'		=> 	number_format($this->input->post('valor_total'), 6, '.', ''),
								'valor_total_stx'	=> 	number_format($this->input->post('valor_total_stx'), 6, '.', ''),
								'valor_ipi'			=> 	'5.00',
								'frete'				=> 	$frete,
								'observacao'		=> 	$this->input->post('observacao')	);

			$tipo 		= 	explode('.', $_FILES['arquivo_bb']['name']);
			$arquivo 	=	md5($_FILES['arquivo_bb']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
			
			$configuracao 	= array(	'upload_path'   	=> 	'./pedidos/',
							        	'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv|cdr',
							        	'file_name'     	=> 	$arquivo 	);
		    
		    $this->load->library('upload', $configuracao);
		    
		    if ( 	$this->upload->do_upload('arquivo_bb') 	){

		    	$dados['arquivo'] 	= 	$arquivo;
		    }

	   		$aux_produto 	= "";
	   		$aux_produto_id = "";
			$qtd = 1;
							
			for($i=0;$i<count($this->input->post('produto_id'));$i++){
									
				if($this->input->post('produto_id')[$i] == $aux_produto_id && trim(mb_strtoupper($this->input->post('produtos')[$i])) == trim(mb_strtoupper($aux_produto)) ){

					$qtd++;
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array(	'qtd' 		=> 	$qtd,
																																	'valor'		=> 	$this->input->post('valor')[$i],
																																	'valor_stx'	=> 	$this->input->post('valor_stx')[$i]	);

				}else{
					$qtd =	1;
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array(	'qtd' 		=> 	$qtd,
																																	'valor'		=> 	$this->input->post('valor')[$i],
																																	'valor_stx'	=> 	$this->input->post('valor_stx')[$i]	);
				}

				$aux_produto 	= mb_strtoupper($this->input->post('produtos')[$i]);
				$aux_produto_id = $this->input->post('produto_id')[$i];
			}

			if( $this->pedidosM->add($dados) ){
				$pedido_id = $this->db->insert_id();
								
				$formaPagto = array();
				$insereProd = array();
				for($i=0;$i<count($this->input->post('porcentagem'));$i++){
					$formaPagto = array(	'pedido_id' 	=> 	$pedido_id,
											'porcentagem'	=> 	$this->input->post('porcentagem')[$i],
											'descricao'		=> 	$this->input->post('descricao')[$i]);

					$this->pedidosM->insereFormaPagamento($formaPagto);
					
				}

				foreach($produtos as $key=>$value)
				{
					$prod = explode('|',$key);
					
					if( $prod[1] == 'OPCIONAL' ){
						$prod[1] = '';
					}

					$insereProd = array('pedido_id' 	=>	$pedido_id,
										'produto_id'	=>	$prod[0],
										'produtos'		=> 	$prod[1],
										'qtd'			=> 	$value['qtd'],
										'valor'			=> 	$value['valor']);
					
					$this->pedidosM->inserePedidoItens($insereProd);
				}

				$this->log('Área Representante2 | cadastro de pedidos','pedidos','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				$pdf = $this->geraPedidosPdf($pedido_id,'tela');
				$this->session->set_flashdata('pedidoPdf', $pdf);
				$this->enviarEmailPedido($this->input->post('orcamento_id'));
			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->pedidosM->selectPedidos();				
			$parametros['title']	=	"Pedidos";
			$this->_load_view('area-representantes2/pedidos/pedidos-rep2',$parametros);

		}else{

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->pedidosM->selectPedidos();
			$parametros['title']	=	"Pedidos";
			$this->_load_view('area-representantes2/pedidos/cadastra-pedidos-rep2',$parametros);
		}
	}

	public function retornaOrcamentos()
	{
		
		$rows = $this->orcamentosM->retornaOrcamentosFechadosRepresentantes($_GET['term'], $this->session->userdata('empresa_id'));
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'			=>	$row['label'],
									'id'			=>	$row['id'],
									'value'			=>	$row['label'],
									'frete_id'		=> 	$row['frete_id'],
									'observacao'	=> 	$row['observacao']	);
				
			}

			echo json_encode($dados);
		}
	}

	public function retornaFormaPagto()
	{
		
		$rows = $this->orcamentosM->retornaFormaPagto($this->input->post('orcamento_id'));			
		echo json_encode($rows);

	}
	
	public function retornaItens()
	{
		
		$rows = $this->itensM->retornaItens($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label'	=>	$row['label'],
								'id'		=>	$row['id'],
								'value'		=>	$row['label']	);
				
			}

			echo json_encode($dados);
		}
	}
	
	public function retornaProdutosAvulsos()
	{
		
		$rows = $this->itensM->retornaProdutosAvulsos($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label'		=>	$row['label'],
								'id'			=>	$row['id'],
								'value'			=>	$row['label'],
								'item_id'		=> 	$row['item_id']	);
				
			}

			echo json_encode($dados);
		}
	}
	
	public function retornaProdutosOrcamentos()
	{	
		$retorno = $this->orcamentosM->retornaProdutosOrcamentos(	$_POST['orcamento_id']	);
		echo json_encode($retorno);
	}

	public function editarPedidos($id = null){

		if($this->input->post('salvar') == 1){
			$update = array();
			
			
			if($this->input->post('tecnico') == 0){
				$tecnico_id = 0;
			}else{
				$tecnico_id = $this->input->post('tecnico_id');
			}

			$update = array('id'				=>	$this->input->post('id'),
							'orcamento_id'		=> 	$this->input->post('orcamento_id'),
							'pintura'			=>	$this->input->post('pintura'),							
							'fl_receber_info'	=> 	$this->input->post('fl_receber_info'),														
							'tecnico_id'		=> 	$tecnico_id,
							'endereco_entrega'	=> 	$this->input->post('endereco_entrega'),
							'nome_cliente'		=> 	$this->input->post('nome_cliente'),
							'rg_cliente'		=> 	$this->input->post('rg_cliente'),
							'cpf_cliente'		=> 	$this->input->post('cpf_cliente'),							
							'pintura_descricao'	=> 	$this->input->post('pintura_descricao') );

			if($this->input->post('pintura') != 'bandeira_branca'){
				$update['arquivo'] 	= 	'';	

			}else{
				$tipo 			= 	explode('.', $_FILES['arquivo_bb']['name']);
				$arquivo 		=	md5($_FILES['arquivo_bb']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$configuracao = array(
			        'upload_path'   	=> 	'./pedidos/',		        
			        'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv|cdr',
			        'file_name'     	=> 	$arquivo
			    );      
			    
			    $this->load->library('upload', $configuracao);
			    
			    if ($this->upload->do_upload('arquivo_bb')){

			    	$update['arquivo'] 	= 	$arquivo;
			    }
			}

			$aux_produto 	= "";
	   		$aux_produto_id = "";
			$qtd = 1;
							
			for($i=0;$i<count($this->input->post('produto_id'));$i++){
									
				if($this->input->post('produto_id')[$i] == $aux_produto_id && trim(mb_strtoupper($this->input->post('produtos')[$i])) == trim(mb_strtoupper($aux_produto)) ){

					$qtd++;
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array('qtd' => $qtd, 'valor'	=> 	$this->input->post('valor')[$i]);	
				}else{
					$qtd =1; 	
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array(	'qtd'   => $qtd, 'valor'	=> 	$this->input->post('valor')[$i]);
				}

				$aux_produto 	= mb_strtoupper($this->input->post('produtos')[$i]);
				$aux_produto_id = $this->input->post('produto_id')[$i];
			}		

			if($this->pedidosM->atualizaPedidos($update)){

				$this->log('Área Administrador | atualizar pedidos','pedidos','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				
				for($i=0;$i<count($this->input->post('descricao'));$i++){
					if($this->input->post('descricao')[$i] != ''){
						$formaPagto = array(	'pedido_id' 	=> 	$this->input->post('id'),
												'porcentagem'	=> 	$this->input->post('porcentagem')[$i],
												'descricao'		=> 	$this->input->post('descricao')[$i]);

						$this->pedidosM->insereFormaPagamento($formaPagto);
					}
				}

				$this->pedidosM->excluirPedidoItens($this->input->post('id'));

				foreach($produtos as $key=>$value)
				{
					$prod = explode('|',$key);
					
					if( $prod[1] == 'OPCIONAL' ){
						$prod[1] = '';
					}

					$insereProd = array('pedido_id' 	=>	$this->input->post('id'),
										'produto_id'	=>	$prod[0],
										'produtos'		=> 	$prod[1],
										'qtd'			=> 	$value['qtd'],
										'valor'			=> 	$value['valor']);
					
					$this->pedidosM->inserePedidoItens($insereProd);
				}

				$this->session->set_flashdata('sucesso', 'ok');
				$pdf = $this->geraPedidosPdf($this->input->post('id'),'tela');
				$this->session->set_flashdata('pedidoPdf', $pdf);
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->pedidosM->selectPedidos();				
				$parametros['title']	=	"Pedidos";
				$this->_load_view('area-representantes2/pedidos/pedidos-rep2',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->pedidosM->selectPedidos();				
				$parametros['title']	=	"Pedidos";
				$this->_load_view('area-representantes2/pedidos/pedidos-rep2',$parametros);
			}

		}else{

			$row 						= 	$this->pedidosM->getPedidosById($id);
			$parametros 				= 	$this->session->userdata();
			$parametros['dados']		=	$row;
			$parametros['formaPagto'] 	= 	$this->pedidosM->pedidosFormaPagto($id);
			$parametros['produtos']		= 	$this->pedidosM->pedidosProdutos($id);						
			$parametros['title']		=	"Editar Pedidos";
			$this->_load_view('area-representantes2/pedidos/editar-pedidos-rep2',	$parametros );
		
		}

	}

	/************************************************************ 
	************** GERA PDF DO ORÇAMENTO SOLICITADO *************
	*************************************************************/
	public function geraPedidosPdf($pedido_id, $origem_solicitacao)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pedidos/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('pedidos-'.$pedido_id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
		
	    $data['empresa'] 		= 	$this->pedidosM->pedidosEmpresasPdf($pedido_id);
	    $data['produtos'] 		= 	$this->pedidosM->pedidosProdutos($pedido_id);
	    $data['forma_pagto']	= 	$this->pedidosM->pedidosFormaPagto($pedido_id);
	    $data['icms'] 			= 	$this->pedidosM->buscaIcmsOrcamentoPedido($pedido_id);
	    $data['usuario_wertco'] = 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-representantes2/pedidos/pedidos-rep2-pdf', $data, true));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'pedidos-'.$pedido_id.'.pdf'));
	    	}else{
	    		return $retorno['retorno'] = 'pedidos-'.$pedido_id.'.pdf';
	    	}
	    }	    
	    
    }
/****************************************************************************
	********* Método Ajax - Excluir Forma de pagamento Pedidos ******************
	*****************************************************************************/
	public function excluirFormaPagtoPedidos()
	{

		if($this->pedidosM->excluirPedidoFormPagto($_POST['id'])){
			$this->log('Área Administrador | excluir produtos pedidos','pedido_itens','EXCLUSÃO', $this->session->userdata('usuario_id'), array('id' => $_POST['id']),array('id' => $_POST['id']),$_SERVER['REMOTE_ADDR']);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function alteraStatusPedido()
	{

		$dados = array('id'					=>	$_POST['pedido_id'],
					   'status_pedido_id' 	=>	$_POST['status_pedido']);

		if($this->pedidosM->atualizaStatusPedido($dados))
		{
			if($_POST['status_pedido'] == 2){ 
			
				$email='<html>
						<head></head>
						<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
							<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
								<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido '.$_POST['pedido_id'].' Confirmado pelo cliente</h1>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Pedido: '.$_POST['pedido_id'].' Confirmado pelo cliente <br/>
									Usuário: '.$this->session->userdata('nome').'
									<br/>
								</p>
							</div>
						</body>
					</html>';

	    		$this->enviaEmail('servicos@wertco.com.br','Pedido '.$_POST['pedido_id'].' confirmado enviado para produção', $email);
	    	}
			
			$this->log('Área Administrador | atualizar status pedido','pedidos','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso'));
			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function verificaOpsPedido(){

		$rows = $this->pedidosM->buscaOpsPorPedido($_POST['pedido_id']);		
		echo json_encode($rows);

	}

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('servicos@wertco.com.br')
			    ->reply_to('servicos@wertco.com.br')    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)		    
			    ->send();
		}else{
			$result = $this->email
			    ->from('servicos@wertco.com.br')
			    ->reply_to('servicos@wertco.com.br')    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)		    
			    ->send();
		}

		return $result;		
	}

	public function verificaEmissao(){

		$resultado = $this->orcamentosM->retornaDiferencaEmissao($this->input->post('orcamento_id'));
		echo json_encode($resultado);
			
	}

	public function insereMotivoReemissao(){
		$insereMotivo = array(	'orcamento_id'		=> 	$this->input->post('orcamento_id'),
								'dthr_emissao'		=> 	date('Y-m-d H:i:s'),
								'usuario_emissao'	=> 	$this->session->userdata('usuario_id'),
								'motivo_emissao'	=> 	$this->input->post('motivo_emissao') );

		if( $this->orcamentosM->insereMotivoReemissao($insereMotivo) ){

			$inserirAndamento = array(	'orcamento_id'			=>	$this->input->post('orcamento_id'),
										'status_orcamento_id'	=> 	$this->input->post('status_orcamento_id'),
										'andamento'				=>	'Orçamento reemitido. Motivo da reemissão: '.$this->input->post('motivo_emissao'),
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id')	);

			if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
			{
			
				echo json_encode(array('retorno' => 'sucesso'));	
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}	
		}  
	}


	public function buscaConsultor()
	{
		echo json_encode($this->zonaM->buscaConsultorEstado($this->input->post('estado')));
	}
	
}