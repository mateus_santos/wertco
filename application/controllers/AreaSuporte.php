<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0); 
ini_set('memory_limit', '1256M');

class AreaSuporte extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'suporte' && $this->session->userdata('tipo_acesso') != 'administrador geral') ) 
		{
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('ChamadoModel', 'chamadoM');		
		$this->load->model('BombasNrSerieModel', 'bombasM');
		$this->load->model('PedidosModel', 'pedidosM');
		$this->load->model('solicitacaoPecasModel', 'solicitacaoPecasM');
		$this->load->model('solicitacaoPecasItensModel', 'solicitacaoPecasItensM');
		$this->load->helper('form');
		$this->load->helper('url');
	}

	public function index()
	{
		$parametros			 			=	$this->session->userdata();
		$parametros['dados']			=	$this->usuariosM->getUsuarios();		
		$parametros['title']			=	"Área do Suporte";		
		$parametros['total_status']		= 	$this->chamadoM->selectTotalStatus();
		$parametros['total_defeitos']	= 	$this->chamadoM->selectTotalDefeitos();
		$parametros['total_mes']		= 	$this->chamadoM->totalChamadoMes();
		$parametros['total_estado']		= 	$this->chamadoM->totalChamadoEstado();
		$parametros['ultimos_chamados']	= 	$this->chamadoM->ultimosChamadosRealizados();
		$parametros['ultimas_solicitacoes']	= 	$this->solicitacaoPecasM->ultimasSolicitacoes();
		$parametros['chamados_inativos']	=	$this->chamadoM->buscaChamadosInativos($this->session->userdata('usuario_id'));
		$parametros['dados']			=	$this->chamadoM->select(1);	
		
		$this->_load_view('area-assistencia/assistencia-tecnica/dashboard-assistencia',$parametros );	

	}

	
}