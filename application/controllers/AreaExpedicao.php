<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0);
ini_set('memory_limit', '1256M');

class AreaExpedicao extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'expedicao' && $this->session->userdata('tipo_acesso') != 'administrador geral') ) 
		{
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('ChamadoModel', 'chamadosM');
		$this->load->model('ChamadoModel', 'chamadosM');
		$this->load->model('BombasNrSerieModel', 'bombasM');
		$this->load->model('PedidosModel', 'pedidosM');
		$this->load->model('transportadorasModel', 'transM');
		$this->load->helper('form');
		$this->load->helper('url');
	}

	public function index()
	{
		$parametros			 	=	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();		
		$parametros['title']	=	"Área da Qualidade";		
		$this->_load_view('area-expedicao/index',$parametros);

	}

	public function transportadoras() 
	{		
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->transM->getTransportadoras();	 	
		$parametros['title']	=	"Transportadoras";
		$this->_load_view('area-expedicao/transportadoras/transportadoras',$parametros);
	}

	public function cadastraTransportadora()
	{
		
		if($this->input->post('salvar') == 1){

			$insert = array(	'tipo_id'	=>	$this->input->post('tipo_id'),								
								'descricao'			=> 	$this->input->post('descricao')	);
						 
			if($this->transM->insere($insert)){
				$this->log('Área Administrador | insere transportadora','Transportadoras','INSERÇÃO', $this->session->userdata('usuario_id'),$insert,$insert,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				$this->transportadoras();
			}else{
				$this->session->set_flashdata('erro', 'erro');
				$this->transportadoras();
			}

		}

		$parametros 			= 	$this->session->userdata();		
		$parametros['tipo']		=	$this->transM->getTipoTransportadoras();
		$parametros['title']	=	"Cadastro de Transportadoras";
		$this->_load_view('area-expedicao/transportadoras/cadastra-transportadora',$parametros);
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Transportadora ******************************
	*****************************************************************************/
	public function excluirTransportadora()
	{
		
		if($this->transM->excluir($_POST['id'])){
			$this->log('Área Administrador | excluir transportadora','transportadoras','EXCLUSÃO', $this->session->userdata('usuario_id'),array('id' => $_POST['id'] ),array('id' => $_POST['id'] ),$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}
	
	public function editarTransportadora($id){

		if($this->input->post('salvar') == 1){

			$update = array(	'id'			=>	$this->input->post('id'),
								'tipo_id'		=>	$this->input->post('tipo_id'),								
								'descricao'		=>	$this->input->post('descricao')	);
						 
			if($this->transM->atualiza($update)){
				$this->log('Área Administrador | atualiza transportadora','transportadoras','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');				
				$this->transportadoras();

			}else{

				$this->session->set_flashdata('erro', 'erro');
				$this->transportadoras();
			}

		}else{

			$row 							= 	$this->transM->getTransportadorasById($id);
			$parametros 					= 	$this->session->userdata();
			$parametros['dados']			=	$row;
			$parametros['title']			=	"Editar Despesa";
			$parametros['tipo']	=	$this->transM->getTipoTransportadoras();
			$this->_load_view('area-expedicao/transportadoras/editar-transportadora',$parametros );
		}
		
	}

	
}