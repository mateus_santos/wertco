<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class AreaFeira extends MY_Controller { 
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'administrador comercial' && $this->session->userdata('tipo_acesso') != 'administrador tecnico' && $this->session->userdata('tipo_acesso') != 'administrador geral' && $this->session->userdata('tipo_acesso') != 'assistencia tecnica' && $this->session->userdata('tipo_acesso') != 'feira')) {
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('ProdutosModel', 'produtosM');
		$this->load->model('OrcamentosModel', 'orcamentosM');
		$this->load->model('IcmsModel', 'icmsM');
		$this->load->model('DownloadsModel', 'downloadsM');
		$this->load->model('CartaoTecnicosModel', 'cartaoM');
		$this->load->model('FretesModel', 'fretesM');
		$this->load->model('EntregasModel', 'entregasM');
		$this->load->model('FormaPagtoModel', 'formaPagtoM');
		$this->load->model('TreinamentoTecnicoModel' , 'treinamentoM');
		$this->load->model('PedidosModel' , 'pedidosM');
		$this->load->model('ItensModel' , 'itensM');
		$this->load->model('OrdemProducaoModel' , 'opM');
		$this->load->model('ChamadoModel' , 'chamadoM');
		$this->load->model('AtividadeModel' , 'atividadeM');
		$this->load->model('SubAtividadeModel' , 'subatividadeM');
		$this->load->model('ChamadoDefeitoModel' , 'defeitoM');
		$this->load->model('ChamadoCausaModel' , 'causaM');
		$this->load->model('ChamadoSolucaoModel' , 'solucaoM');
		$this->load->model('AtividadeCausaSolucaoModel' , 'causa_solucaoM');
		$this->load->model('ConfiguracaoModel' , 'configuracaoM');
		$this->load->model('ConfiguracaoBombaModel' , 'configuracao_bombaM');
		$this->load->model('ConfiguracaoConcentradorModel' , 'configuracao_concentradorM');
		$this->load->model('ConfiguracaoIdentificadorModel' , 'configuracao_identificadorM');
		$this->load->model('DespesasModel' , 'despesasM');
		$this->load->model('OrdemPagamentoModel' , 'ordemPagtoM');
		$this->load->model('SolicitacaoPecasModel' , 'solicitacaoPecasM');
		$this->load->model('PecasModel' , 'pecasM');
		$this->load->model('MotivoExclusaoModel' , 'motivoM');
		$this->load->model('SolicitacaoPecasItensModel' , 'solicitacaoPecasItensM');
		$this->load->model('AutorizServicoModel' , 'autorizServicoM');
		$this->load->model('AreaAtuacaoModel' , 'areaAtuacaoM');
		$this->load->helper('form');

	}
 
	public function index()
	{
		$parametros						 =	$this->session->userdata();			
		$parametros['title']			 =	"Área do Administrador do sistema";			
		$this->_load_view_feira('area-feira/index',$parametros);
	}

	public function buscaOrcamentosPorStatus(){

		$retorno = $this->orcamentosM->buscaOrcamentosPorStatus($_POST['status']);		
		echo json_encode($retorno); 

	}

	public function buscaOrcamentosPorPeriodo(){
		$retorno = $this->orcamentosM->selectTotalStatusPeriodo($_POST['periodo']);
		echo json_encode($retorno);
	}

	public function buscaOrcamentosFechadosMes(){

		$retorno = $this->orcamentosM->buscaOrcamentosFechadosMes($_POST['mes']);		
		echo json_encode($retorno);
	}

	public function buscaOrcamentosPorStatusPeriodo(){

		$retorno = $this->orcamentosM->buscaOrcamentosPorStatusPeriodo($_POST['status'],$_POST['periodo']);		
		echo json_encode($retorno);
	}	

	/****************************************************************************
	**************** Método Responsável por editar usuários *********************
	*****************************************************************************/

	public function editarUsuario($id = null){

		if($this->input->post('salvar') == 1){
			
			$update = $this->input->post();			
			unset($update['salvar']);
			unset($update['senha2']);
			unset($update['senha']);
			
			if($this->input->post('dt_treinamento') != null && $this->input->post('dt_treinamento') != ''){
				$data 			= 	explode('/',$this->input->post('dt_treinamento'));
				$dt_treinamento	= 	'20'.$data[2].'-'.$data[1].'-'.$data[0];
				$update['dt_treinamento']	=	$dt_treinamento;
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha')));
			}
			 
			if($this->usuariosM->atualizaUsuario($update)){
				$this->log('Área Administrador | atualiza usuário','usuarios','EDIÇÂO', $this->session->userdata('usuario_id'), $update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 			= 	$this->session->userdata();
				$parametros['title']	=	"Gestão de Usuários";
				$parametros['dados']	=	$this->usuariosM->getUsuarios();						
				$this->_load_view_feira('area-feira/gestao-usuario/gestao-usuario',	$parametros );	
			}

		}else{
			$row 								= 	$this->usuariosM->getUsuario($id);		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Usuários";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view_feira('area-feira/gestao-usuario/editar-usuario',	$parametros );	
		}
	}	

	public function orcamentos($pesquisa = null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->orcamentosM->buscaOrcamentosFeira();
		$parametros['title']	=	"Orçamentos";
		$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view_feira('area-feira/orcamentos/orcamentos-feira',$parametros);
	}

	public function buscaOrcamentosSS(){
		$dados 	 = $this->input->post();
		$retorno =	$this->orcamentosM->buscaOrcamentos($dados);
		echo json_encode($retorno);
	}

	public function buscaProximosStatus()
	{
		$retorno = $this->orcamentosM->buscaProximosStatusOrcamento($_POST['orcamento_id']);			
		echo json_encode($retorno);
		
	}

	public function buscaAndamentos()
	{
		$retorno = $this->orcamentosM->buscaAndamentos($_POST['orcamento_id']);
			
		echo json_encode($retorno);
		
	}
	
	public function alteraStatusOrcamento()
	{

		if( $_POST['indicacao'] == 1  ){
			if($_POST['aprovar_indicacao'] == 0){
				$_POST['status_orcamento'] = 8;
			}
		}

		$dados = array('id'						=>	$_POST['orcamento_id'],
					   'status_orcamento_id'	=>	$_POST['status_orcamento']);

		if($this->orcamentosM->atualizaStatusOrcamento($dados))
		{
			$this->log('Área Administrador | atualizar status orçamento','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
			if( $_POST['indicacao'] == 1  ){
				if($_POST['aprovar_indicacao'] == 1){
	        		$andamento = "Indicação Aprovada pela WERTCO!";
	        	}else{
	        		$andamento = "Indicação não Aprovada pela WERTCO!";
	        	}
			}else{
				
				switch($_POST['status_orcamento']){
					case 1:
				        $andamento = "Orçamento Aberto!";
				        break;
				    case 2:
				        $andamento = "Orçamento Fechado, venda realizada!";
				        break;
				    case 3:
				        $andamento = "Orçamento Perdido.";
				        break;
				    case 4:
				        $andamento = "Orçamento Cancelado.";
				        break;
				    case 5:
				        $andamento = "Orçamento entregue ao cliente.";
				        break;        
				    case 6:
				        $andamento = "Orçamento em negociação.";
				        break;
				     case 9:
				        $andamento = "Bombas em teste no cliente.";
				        break; 
				    case 10:
				        $andamento = "Orçamento Perdido para Wayne.";
				        break;
				    case 11:
				        $andamento = "Orçamento Perdido para Gilbarco.";
				        break;
				}
			}

			$inserirAndamento = array('orcamento_id'			=>	$_POST['orcamento_id'],
										'status_orcamento_id'	=> 	$_POST['status_orcamento'],
										'andamento'				=>	$andamento,
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id'));

			if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
			{
				echo json_encode(array('retorno' => 'sucesso'));	
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}
	
	public function alteraIndicadorOrcamento(){

		$update = array('id' 			=> 	$_POST['orcamento_id'], 
						'indicador_id'	=>	$_POST['indicador'] );

		if($this->orcamentosM->atualizaIndicador($update)){

			echo json_encode(array('retorno' => 'sucesso'));	
		}else{

			echo json_encode(array('retorno' => 'erro'));	
		}

	}

	public function insereAndamentoOrcamento(){

		$inserirAndamento = array(	'orcamento_id'			=>	$_POST['orcamento_id'],
									'status_orcamento_id'	=> 	$_POST['status_orcamento_id'],
									'andamento'				=>	$_POST['andamento'],
									'dthr_andamento'		=>	date('Y-m-d H:i:s'),
									'usuario_id'			=> 	$this->session->userdata('usuario_id')	);

		if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
		{
			echo json_encode(array('retorno' => 'sucesso'));	
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}

	}

	public function retornaRepresentantes()
	{
	
		$rows = $this->usuariosM->retornaRepresentante($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'		 =>		$row['label'],
									'id'	 	 =>		$row['id'],
									'value'		 =>		$row['label'],
									'empresa_id' => 	$row['id']	);
				
			}

			echo json_encode($dados);
		}
	}

	public function retornaResponsavelOrcamentos()
	{
	
		$rows = $this->usuariosM->retornaIndicadores($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'		 =>		$row['label'],
									'id'	 	 =>		$row['id'],
									'value'		 =>		$row['label']);
				
			}

			echo json_encode($dados);
		}
	}

	public function retornaIndicadores()
	{
		
		$rows = $this->usuariosM->retornaIndicadores($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label' 	=>	$row['label'],
								 'id'		=>	$row['id'],
								 'value'	=>	$row['label'] );
				
			}

			echo json_encode($dados);
		}
	}

	public function alteraResponsavelOrcamento()
	{
		// SE O ID DA TABELA VIER EM BRANCO, INSERE NOVO REPREENTANTE, SE NÃO ATUALIZA O REPRESENTANTE RESPONSÁVEL
		if( $_POST['orcamento_responsavel_id'] == '' )
		{
			$insereResponsavel =  array('orcamento_id' 	=> 	$_POST['orcamento_id'],
										'usuario_id'	=> 	$_POST['responsavel_id']	);

			if($this->orcamentosM->insereResponsavelOrcamento($insereResponsavel)){
				$this->log('Área Administrador | responsavel_orcamento','orcamentos','INSERÇÃO', $this->session->userdata('usuario_id'), $insereResponsavel,$insereResponsavel,$_SERVER['REMOTE_ADDR']);
				$insereAndamentoOrcamento 	= 	array('orcamento_id' 		=> 	$_POST['orcamento_id'],
													  'andamento'			=> 	'Orçamento enviado para o representante responsável',
													  'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													  'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													  'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);

				if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
				{

					echo json_encode(array('retorno' => 'sucesso'));	
				}else{

					echo json_encode(array('retorno' => 'erro'));	
				}			
			}else{

				echo json_encode(array('retorno' => 'erro'));	
			}

		}else{
			$atualizaResponsavel 	= 	array(	'id'			=>	$_POST['orcamento_responsavel_id'],
					   	   						'usuario_id' 	=>	$_POST['responsavel_id']);

			if( $this->orcamentosM->atualizaResponsavelOrcamento($atualizaResponsavel) )
			{
				$this->log('Área Administrador | responsavel_orcamento','orcamentos','EDIÇÂO', $this->session->userdata('usuario_id'), $atualizaResponsavel,$atualizaResponsavel,$_SERVER['REMOTE_ADDR']);
				$insereAndamentoOrcamento 	= 	array('orcamento_id' 		=> 	$_POST['orcamento_id'],
													  'andamento'			=> 	'Alterado o indicador responsável pelo orçamento.',
													  'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													  'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													  'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
				
				if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
				{

					echo json_encode(array('retorno' => 'sucesso'));	
				}else{

					echo json_encode(array('retorno' => 'erro'));	
				}
			}else{

				echo json_encode(array('retorno' => 'erro'));	
			}
		}
	}

	public function visualizarOrcamento($id,$pesquisa = null)
	{
		$parametros 				= 	$this->session->userdata();
		$parametros['dados']		=	$this->orcamentosM->getOrcamentoProdutos($id);
		$parametros['desconto']		=	$this->orcamentosM->getOrcamentoDesconto($id);
		$parametros['solicitante']	=	$this->orcamentosM->getSolicitante($id);
		$parametros['indicador']	= 	$this->orcamentosM->buscaIndicador($id);
		$parametros['bombas'] 		= 	$this->produtosM->select();
		//$parametros['prospeccaoStatus'] 		= 	$this->prospeccaoStatusM->selectprospeccaoStatus();
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['title']		=	"Orçamento #".$id;
		$parametros['zona_atuacao'] = 	$this->orcamentosM->buscaOrcamentoZona($id);		
		$parametros['title']		=	"Orçamento #".$id;
		$parametros['pesquisa']		= 	htmlentities($pesquisa);
		$this->_load_view_feira('area-feira/orcamentos/visualiza-orcamento-feira',$parametros);
	}

	public function emitirOrcamento()
	{
			
		$status_orcamento_id = $_POST['status_orcamento_id'];
	
		//altera o status para emitido	
		$dados = array('id'						=>	$_POST['orcamento_id'],
			   		   'status_orcamento_id'	=>	$status_orcamento_id);

		if($this->orcamentosM->atualizaStatusOrcamento($dados)){

			//orçamento entregue/emitido
			$andamentoOrcamento = array('andamento' 			=> 'Orçamento emitido ao cliente para o email '.$_POST['email_destino'].', por '.$this->session->userdata('nome').'',
		 								'dthr_andamento'		=> date('Y-m-d H:i:s'),
		 								'orcamento_id'			=> $_POST['orcamento_id'],
		 								'status_orcamento_id'	=> $status_orcamento_id,
		 								'usuario_id'			=> $this->session->userdata('usuario_id') );	
	 		if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento)){

				$retorno = $this->enviaEmailEmissaoCliente($_POST['email_destino'],$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');
				if($retorno){
					$this->enviaEmailEmissaoCliente('vendas@wertco.com.br',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');

					echo json_encode(array('retorno' => 'sucesso'));
				}else{
					echo json_encode(array('retorno' => 'erro'));	
				}
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}	
		
	}

	private function enviaEmailEmissaoCliente( $email_destino, $orcamento_id, $valor_tributo, $valor_total, $contato_posto  )
	{
		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);
		
		$total_desconto 	= 	"";		
		
		$anexo = $this->geraOrcamento($orcamento_id,'email',$contato_posto);

		$email = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title>tst</title><style type="text/css">
        /* Resets */
        .ReadMsgBody { width: 100%; background-color: #ebebeb;}
        .ExternalClass {width: 100%; background-color: #ebebeb;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        a[x-apple-data-detectors]{
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
        }        
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        body {margin:0; padding:0;}
        .yshortcuts a {border-bottom: none !important;}
        .rnb-del-min-width{ min-width: 0 !important; }

        /* Add new outlook css start */
        .templateContainer{
            max-width:590px !important;
            width:auto !important;
        }
        /* Add new outlook css end */

        /* Image width by default for 3 columns */
        img[class="rnb-col-3-img"] {
        max-width:170px;
        }

        /* Image width by default for 2 columns */
        img[class="rnb-col-2-img"] {
        max-width:264px;
        }

        /* Image width by default for 2 columns aside small size */
        img[class="rnb-col-2-img-side-xs"] {
        max-width:180px;
        }

        /* Image width by default for 2 columns aside big size */
        img[class="rnb-col-2-img-side-xl"] {
        max-width:350px;
        }

        /* Image width by default for 1 column */
        img[class="rnb-col-1-img"] {
        max-width:550px;
        }

        /* Image width by default for header */
        img[class="rnb-header-img"] {
        max-width:590px;
        }

        /* Ckeditor line-height spacing */
        .rnb-force-col p, ul, ol{margin:0px!important;}
        .rnb-del-min-width p, ul, ol{margin:0px!important;}

        /* tmpl-2 preview */
        .rnb-tmpl-width{ width:100%!important;}

        /* tmpl-11 preview */
        .rnb-social-width{padding-right:15px!important;}

        /* tmpl-11 preview */
        .rnb-social-align{float:right!important;}

        @media only screen and (min-width:590px){
        /* mac fix width */
        .templateContainer{width:590px !important;}
        }

        @media screen and (max-width: 360px){
        /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
        .rnb-yahoo-width{ width:360px !important;}
        }

        @media screen and (max-width: 380px){
        /* fix width and font size "tmpl-4 tmpl-6" in mobile preview */
        .element-img-text{ font-size:24px !important;}
        .element-img-text2{ width:230px !important;}
        .content-img-text-tmpl-6{ font-size:24px !important;}
        .content-img-text2-tmpl-6{ width:220px !important;}
        }

        @media screen and (max-width: 480px) {
        td[class="rnb-container-padding"] {
        padding-left: 10px !important;
        padding-right: 10px !important;
        }

        /* force container nav to (horizontal) blocks */
        td.rnb-force-nav {
        display: inherit;
        }
        }

        @media only screen and (max-width: 600px) {

        /* center the address &amp; social icons */
        .rnb-text-center {text-align:center !important;}

        /* force container columns to (horizontal) blocks */
        td.rnb-force-col {
        display: block;
        padding-right: 0 !important;
        padding-left: 0 !important;
        width:100%;
        }

        table.rnb-container {
         width: 100% !important;
        }

        table.rnb-btn-col-content {
        width: 100% !important;
        }
        table.rnb-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-last-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table[class~="rnb-col-2"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-col-2-noborder-onright {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        }

        table.rnb-col-2-noborder-onleft {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-top: 10px;
        padding-top: 10px;
        }

        table.rnb-last-col-2 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table.rnb-col-1 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        img.rnb-col-3-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xs {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xl {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-1-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-header-img {
        /**max-width:none !important;**/
        width:100% !important;
        margin:0 auto;
        }

        img.rnb-logo-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        td.rnb-mbl-float-none {
        float:inherit !important;
        }

        .img-block-center{text-align:center !important;}

        .logo-img-center
        {
            float:inherit !important;
        }

        /* tmpl-11 preview */
        .rnb-social-align{margin:0 auto !important; float:inherit !important;}

        /* tmpl-11 preview */
        .rnb-social-center{display:inline-block;}

        /* tmpl-11 preview */
        .social-text-spacing{margin-bottom:0px !important; padding-bottom:0px !important;}

        /* tmpl-11 preview */
        .social-text-spacing2{padding-top:15px !important;}

    }</style><!--[if gte mso 11]><style type="text/css">table{border-spacing: 0; }table td {border-collapse: separate;}</style><![endif]--><!--[if !mso]><!--><style type="text/css">table{border-spacing: 0;} table td {border-collapse: collapse;}</style> <!--<![endif]--><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body>

<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#f9fafc" style="background-color: rgb(249, 250, 252);">

    <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr>
    <tr>
        <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="590" style="width:590px;">
                        <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
        <tbody><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_5" id="Layout_5">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:600px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/backgroundCabecalho.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_6">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div>&nbsp;</div>

<div>
<table style="color: rgb(0, 0, 0); font-family: sans-serif; font-style: normal; padding-top: 20px; font-size: 16px; margin-left: 15px; width: 100%;">
   <tbody>
                    <tr>
                        <td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
                        <td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
                        <td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
                        <td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 18px;font-weight: 700;">Validade:</td>
                        <td style="padding-bottom: 18px;">30 Dias</td>
                        <td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
                        <td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
                    </tr>
                    <tr>
                        <td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
                        <td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
                        <td style=" font-weight: 700; text-align: right;">Telefone:</td>
                        <td>'.$empresa['telefone'].'</td>
                    </tr>
                    <tr>
                        <td style=" font-weight: 700;">E-mail:</td>
                        <td colspan="3">'.$empresa['email'].'</td>
                    </tr>
                </tbody>
</table>
</div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_7" id="Layout_7">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffcc00" align="center" cellspacing="0" style="background-color: rgb(255, 204, 0);">
                            <tbody><tr>
                                <td valign="top" align="left">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:139;;max-width:139px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="139" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/produtos.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_8">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><table cellspacing="0" style="width: 500px;margin-left: 14px">
    <thead style="height: 55px">
        <tr>
            <th style="height: 30px; text-align: center;">Modelo</th>
            <th style="text-align: center;">Descrição</th>
            <th>Qtd</th>
            <th style="text-align: right;">Valor</th>
        </tr>
    </thead>
    <tbody style="background-color: #fff;margin-left: 10px;font-size: 12px">';
       $total = 0; foreach($produtos as $produto){
            
            $email.='   <tr>
                        <td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
                        <td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
                        <td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>
                        <td style="   text-align: right; padding: 10px;">R$ '.number_format($produto['valor'], 2, ',', '.').'</td>
                    </tr>';                 
            $total = $total + ($produto['valor'] * $produto['qtd']);
        }   
        $email.='   <tr>
                        <td colspan="3" style="border-top: 1px solid #f0f0f0; border-right: 1px solid #f0f0f0;    padding: 10px;text-align: right;">
                        <b>Total (Sub-total +'.$valor_tributo.'% de ICMS + 5% de IPI)</b></td>
                        <td style="border-top: 1px solid #f0f0f0; text-align: right; padding: 10px;"><b>R$ '.number_format($total, 2, ',', '.').'</b></td>
                    </tr>';
        /*          <tr>
                        <td colspan="3" style=" border-top: 1px solid #f0f0f0;border-right: 1px solid #f0f0f0;    padding: 10px;text-align: right;"><b>Total (Sub-Total + '.$valor_tributo.'% de ICMS + 5% de IPI)</b></td>
                        <td style="border-top: 1px solid #f0f0f0; padding: 10px; text-align: right;"><b>R$ '.number_format($valor_total, 2, ',', '.').'</b></td>
                    </tr>';*/
        $email.='      
    </tbody>
</table>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_10" id="Layout_10">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffcc00" align="center" cellspacing="0" style="background-color: rgb(255, 204, 0);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:5px; width:542;;max-width:542px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="542" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 5px; " src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_4" id="Layout_4">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="590" class="rnb-container rnb-yahoo-width" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#f9fafc" style="padding-right: 20px; padding-left: 20px; background-color: rgb(249, 250, 252);">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-size:14px; color:#888888; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
                                    <div>© 2018 wertco</div>
                                </td></tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        </td>
        </tr>
        </tbody></table>

</body></html>';	
		
		$this->load->library('email');
		$result = $this->email
		    ->from('vendas@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to($email_destino)
		    ->subject('Orçamento Wertco')
		    ->message($email)
		    ->attach('./pdf/'.$anexo)
		    ->send();

		return $result;		
	
	}

	/************************************************************ 
	************** GERA PDF DO ORÇAMENTO SOLICITADO *************
	*************************************************************/
	public function geraOrcamento($orcamento_id, $origem_solicitacao,$contato)
    {
    	
	   //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-'.$orcamento_id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $dados				=	$this->orcamentosM->getOrcamentoProdutos($orcamento_id);
	    $total_descontos 	= 	$this->orcamentosM->getOrcamentoDesconto($orcamento_id);
	    $responsavel 		= 	$this->orcamentosM->getResponsavel($orcamento_id);
	    $orcamento 			= 	$this->orcamentosM->getFreteEntregaPagto($orcamento_id);
	    $indicador			= 	$this->orcamentosM->getIndicador($orcamento_id);
	    $data['frete']		= 	(is_object($orcamento)) ? $orcamento->frete : ' ';
	    $data['entrega']	= 	(is_object($orcamento)) ? $orcamento->entrega : ' ';
	    $data['formaPagto'] = 	(is_object($orcamento)) ? $orcamento->forma_pagto : ' ';
	    $data['emissao']	= 	$this->orcamentosM->retornaDiferencaEmissao($orcamento_id);
	    $data['primeira_emissao']	= 	$this->orcamentosM->primeiraEmissao($orcamento_id);
	    
	    $data['empresa'] = array(	'razao_social' 			=> 	$dados[0]->razao_social,
    								'fantasia' 				=> 	$dados[0]->fantasia,
									'cnpj' 					=> 	$dados[0]->cnpj,
									'telefone'				=> 	$dados[0]->telefone,
									'email'					=> 	$dados[0]->email,
									'endereco'				=> 	$dados[0]->endereco,
									'cidade'				=> 	$dados[0]->cidade,		
									'pais'					=> 	$dados[0]->pais,
									'estado'				=> 	$dados[0]->estado,
									'orcamento_id'			=> 	$dados[0]->orcamento_id,
									'emissao'				=> 	$dados[0]->emissao,
									'valor_tributo'			=>	$dados[0]->valor_tributo,
									'representante_legal' 	=> 	$dados[0]->representante_legal,
									'inscricao_estadual' 	=> 	$dados[0]->insc_estadual,
									'bairro'				=> 	$dados[0]->bairro,
									'cep'					=> 	$dados[0]->cep,
									'observacao'			=> 	$dados[0]->observacao,									
									'tp_cadastro'			=> 	$dados[0]->tp_cadastro,
									'celular'				=> 	$dados[0]->celular,
									'fl_especial' 			=> 	$dados[0]->fl_especial,
									'dt_previsao'			=> 	$dados[0]->dt_previsao	);

	    foreach ($dados as $dado) {
	    	$data['produtos'][] = array('codigo' 			=> 	$dado->codigo,
	    								'descricao'			=>	$dado->descricao,
										'modelo'			=>	$dado->modelo,
	    								'qtd'				=>	$dado->qtd,
	    								'valor_unitario' 	=>	$dado->valor_unitario,
	    								'valor_orcamento'	=> 	$dado->valor_orcamento,
	    								'valor_produto' 	=> 	$dado->valor);
	    }		

	    $data['info']  	= 	array(	'nome' 			=> 	$this->session->userdata('nome'),
	    						 	'email'			=> 	$this->session->userdata('email'),
	    						 	'telefone'		=>	$this->session->userdata('telefone'),	    						 	
	    						 	'contato'		=> 	( isset($_POST['contato'])) ? $_POST['contato'] : $contato );

	    $data['representante'] 	= 	array(	'responsavel' 	=>  $responsavel->responsavel,
								  			'empresa' 		=>	$responsavel->razao_social);

	    foreach( $total_descontos as $total_desconto ){
	    	$data['desconto'] = array('valor_desconto' => $total_desconto['valor_desconto'] );
	    }

	    if(!isset($data['desconto'])){
	    	$data['desconto'] = array('valor_desconto' => '' );
	    }

	    $data['indicador'] = array('indicador' 	=> 	(count($indicador) > 0) ? $indicador[0]->indicador : '');	    

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-administrador-comercial/orcamentos/orcamento-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'orcamento-'.$orcamento_id.'.pdf'));
	    	}else{
	    		return $retorno['retorno'] = 'orcamento-'.$orcamento_id.'.pdf';
	    	}
	    }	    
	    
    }

    public function cadastraOrcamentos($pesquisa = null)
	{
		$parametros 				= 	$this->session->userdata();
		//$parametros['dados']		=	$this->orcamentosM->buscaOrcamentosIndicadores($parametros['usuario_id']);
		$parametros['bombas'] 		= 	$this->produtosM->select();		
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['title']		=	"Orçamentos";
		$parametros['pesquisa']		= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view_feira('area-feira/orcamentos/cadastra-orcamento-feira',$parametros);	
	}

	public function pesquisarOrcamentos()
	{
		$parametros 				= 	$this->session->userdata();		
		$parametros['title']		=	"Pesquisar Orçamentos";		
		$this->_load_view_feira('area-feira/orcamentos/pesquisar-orcamentos',$parametros);	
	}

	public function pesquisarOrcamentosAjax(){

    	$parametros 	= 	$this->input->post();
		$data['dados']	= 	$this->orcamentosM->buscaOrcamentosPorCnpj($parametros['cnpj']);
		$retorno 		= 	$this->load->view('/area-feira/orcamentos/pesquisar-orcamentos-ajax', $data, true);
		
		echo json_encode(array('retorno'	=> 	$retorno ));    
    }

	/* FUNÇÃO RESPONSÁVEL POR CADASTRAR ORÇAMENTOS PELOS ADMINISTRADORES COMERCIAIS DA WERTCO 	*/
	public function orcamentosAdm()
	{
		if( $this->input->post() )
		{
			
			if( $this->input->post('salvar') == '1')
			{
				$dadosEmpresa = array(
			        'razao_social' 				=>	$this->input->post('razao_social'),
					'fantasia' 					=>	$this->input->post('fantasia'),
					'cnpj' 						=>	$this->input->post('cnpj'),
					'telefone' 					=>	$this->input->post('telefone'),
					'endereco' 					=>	$this->input->post('endereco'),
					'email' 					=>	$this->input->post('email'),				
					'cidade' 					=>	$this->input->post('cidade'),
					'estado'					=>	$this->input->post('estado'),
					'pais'						=>	$this->input->post('pais'),				
					'tipo_cadastro_id'			=>	1,
					'bairro'					=>	$this->input->post('bairro'),
					'cep'						=>	$this->input->post('cep'),
					'insc_estadual'				=> 	$this->input->post('inscricao_estadual'),
					'tp_cadastro'				=> 	$this->input->post('tp_cadastro'),
					'cartao_cnpj'				=> 	$this->input->post('cartao_cnpj'),					
					'codigo_ibge_cidade'		=> 	$this->input->post('codigo_ibge_cidade')
			    );

				if($this->empresasM->add($dadosEmpresa))
				{		
					$empresa_id = $this->db->insert_id();

				}
	 		}else{
	 			$empresa_id = $this->input->post('empresa_id');
	 		}

	 		$dt_previsao 		= date('d/m/Y',strtotime('+68 days'));
	 		$dt_previsao_prod 	= date('d/m/Y',strtotime('+68 days'));

	 		$dadosOrcamento = array('empresa_id' 			=> 	$empresa_id,
	 								'status_orcamento_id'	=> 	( $this->input->post('valor_desconto_orc') == 'outro' ) ? 12 : 1,
	 								'origem'				=> 	'Administrador Feira',
	 								'solicitante_id'		=> 	$this->session->userdata('usuario_id'),
	 								'usuario_id'			=> 	$this->session->userdata('usuario_id'),
	 								'indicador_id'			=> 	$this->input->post('indicador_id'),
	 								'frete_id'				=> 	$this->input->post('frete'),
	 								'entrega_id'			=> 	$this->input->post('entrega'),
	 								'forma_pagto_id'		=> 	$this->input->post('forma_pagto'),
									'contato_posto'			=> 	$this->input->post('contato_posto'),
									'fl_especial'			=> 	( $this->input->post('valor_desconto_orc') == 'outro' ) ? '1' : '0',
	 								'dt_previsao'			=>  $dt_previsao,
	 								'dt_previsao_prod'		=> 	$dt_previsao_prod	);

	 		if( $this->orcamentosM->insereOrcamento($dadosOrcamento) )
	 		{
	 			$orcamento_id  = $this->db->insert_id();
	 			$erro = 0;

	 			$dadosContato = array(	'empresa_id' 		=> 	$empresa_id,
	 									'nome'				=> 	$this->input->post('contato_posto'),
	 									'email'				=> 	$this->input->post('email_contato_posto'),
	 									'celular'			=> 	$this->input->post('tel_contato_posto'),
	 									'tipo_cadastro_id'	=> 1);

	 			if( $this->usuariosM->add($dadosContato) )
	 			{ 

	 				$contato_id = $this->db->insert_id();
	 				$updateContato = array(	'id' 			=> 	$orcamento_id,
	 										'contato_id'	=> 	$contato_id	);

	 				$this->orcamentosM->atualizaOrcamento($updateContato);

		 			$andamentoOrcamento = array('andamento' 			=> 'Orçamento realizado por '.$this->session->userdata('nome').'. Equipe Wertco!',
			 									'dthr_andamento'		=> date('Y-m-d H:i:s'),
			 									'orcamento_id'			=> $orcamento_id,
			 									'status_orcamento_id'	=> 1,
			 									'usuario_id'			=> $this->session->userdata('usuario_id')
			 									);

		 			$motivo 		= 	'Tabela de comissão';
		 			$comissao_val 	= 	$this->input->post('valor_desconto_orc');
		 			
		 			if( $this->input->post('valor_desconto_orc') == 'outro' ){
	 					
	 					$comissao_val = 0.01;
	 					$motivo = 'Outro';
	 					
		 			}elseif( $this->input->post('valor_desconto_orc') == 'TB2' ){
		 				
		 				$comissao_val = 0.02;
		 				$motivo = 'Tabela 2% TB2';

		 			}

		 			$comissao  = array(		'orcamento_id' 		=> 	$orcamento_id,
		 									'valor_desconto' 	=> 	$comissao_val,
		 									'motivo_desconto'	=> 	$motivo );

		 			if(	$this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento) && $this->orcamentosM->insereOrcamentosDesconto($comissao) )
		 			{
		 				$erro = $erro;
		 				$orcamentoResp = array(	'orcamento_id' 	=> 	$orcamento_id,
			 									'usuario_id' 	=> 	$this->session->userdata('usuario_id') );
	 					if($this->orcamentosM->insereResponsavelOrcamento($orcamentoResp))
	 					{
	 						$erro = $erro;
	 					
				 			for ($i=0; $i < count($this->input->post('produto')) ; $i++) 	 			  
				 			{ 

				 				$valor 	=	$this->input->post('valor')[$i];
		 						if( $this->input->post('valor_desconto_orc') == 'outro' ){
		 							$valor = str_replace(",", ".", str_replace(".","", $this->input->post('valor')[$i] ));
		 						}

				 				$orcamentoProdutos = array(	'orcamento_id' 	=>	$orcamento_id,
				 											'produto_id'  	=> 	$this->input->post('produto')[$i],
				 											'qtd'			=>	$this->input->post('quantidade')[$i],
				 											'valor'			=>  $valor 	);	

				 				if($this->orcamentosM->insereOrcamentoProdutos($orcamentoProdutos))
				 				{
				 					$erro = $erro;
				 					
								}else{
									$erro++;
								}		 					 			
				 			}
				 		}else{
							$erro++;
						}	 					

					}else{
						$erro++;
					}
				}else{
					$erro++;
				}
			}else{
				$erro++;
			}

	 		if( $erro == 0 ){
	 				
	 			if($this->enviarEmailOrcamento($orcamento_id))
 				{
 					echo $this->input->post('valor_desconto_orc');
 					if($this->input->post('valor_desconto_orc') == 'outro'){
 						$this->enviarEmailConfirmacaoPreco($orcamento_id);
					}

 					$this->session->set_flashdata('sucesso', 'ok.');
 				}else{
 				 	$this->session->set_flashdata('erro', 'erro.');	
 				}
 			}else{
 				$this->session->set_flashdata('erro', 'erro.');
 			}

			$this->orcamentos();
		}

	}

	public function insereDescontoOrcamento()
	{
		$dados_desconto = array('orcamento_id' 		=> 	$_POST['orcamento_id'],
								'valor_desconto'	=>	str_replace(',', '.', $_POST['valor_desconto']),
								'motivo_desconto'	=> 	$_POST['motivo_desconto'] );

		if( $this->orcamentosM->insereOrcamentosDesconto( $dados_desconto ) )
		{
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}		

	public function alterarValorProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['id'],
							 	'valor'	=>	(strpos($_POST['valor'],',') === false ) ? $_POST['valor'] : str_replace(',','.', str_replace('.','',$_POST['valor'])) );

		if($this->orcamentosM->atualizaValorProduto($dadosUpdate)){
			
			if( isset($_POST['comissao']) ){

				$motivo 		= 	'Tabela de comissão';
	 			$comissao_val 	= 	$_POST['comissao'];
	 				
	 			if( $_POST['comissao'] == 'outro' ){
					$this->enviarEmailConfirmacaoPreco( $_POST['orcamento_id']);	
					$comissao_val = 0.01;
					$motivo = 'Outro';
						
	 			}elseif( $_POST['comissao'] == 'TB2' ){
	 					
	 				$comissao_val = 0.02;
	 				$motivo = 'Tabela 2% TB2';

				}

				$comissao = array('orcamento_id' 	=> $_POST['orcamento_id'],
								  'valor_desconto'	=> $comissao_val);

				$this->orcamentosM->atualizaComissao($comissao);

			}

			echo json_encode(array('retorno' => 'sucesso' ));						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}

	}

	public function alterarProdutoOrcamento(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['orcamento_produto_id'],
							 	'produto_id'	=>	$_POST['produto_id']	);

		if($this->orcamentosM->alterarProdutoOrcamento($dadosUpdate)){
			$this->log('Área Administrador | atualiza produto','orcamento_produtos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Produto #'.$_POST['produto_nr'].' alterado para ' .$_POST['produto'],
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function insereContatoOrcamento(){
		$update = array(	'id' 			=> 	$_POST['orcamento_id'],
							'contato_id' 	=> 	$_POST['contato_id'] 	);	
		
		if($this->orcamentosM->atualizaOrcamento($update)){
						$this->log('Área Administrador | atualiza contato orcamento','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso' ));						
		}else{
			echo json_encode(array('retorno' => 'erro' ));
		}					

	}

	public function alterarQtdProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['orcamento_produto_id'],
							 	'qtd'	=>	$_POST['qtd']	);

		if($this->orcamentosM->alterarQtdProduto($dadosUpdate)){

			$this->log('Área Administrador | atualiza qtd produto','orcamento_produtos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Quantidade de um produto foi alterada',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 					 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarObservacao(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['id'],
							 	'observacao'	=>	$_POST['observacao']	);

		if($this->orcamentosM->alterarObservacao($dadosUpdate)){
			$this->log('Área Administrador | atualiza observacao','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);			
			echo json_encode(array('retorno' => 'sucesso' ));						 	
			
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}


	public function alterarFormaPagto(){
		$dadosUpdate = array(	'id' 				=> 	$_POST['orcamento_id'],
							 	'forma_pagto_id'	=>	$_POST['valor']	);

		if(	$this->orcamentosM->alterarFormaPagto($dadosUpdate)	)	{
			$this->log('Área Administrador | atualiza forma pagto','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de Pagamento alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarEntrega(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['orcamento_id'],
							 	'entrega_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarEntrega($dadosUpdate)){
			$this->log('Área Administrador | atualiza entrega','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de entrega alterada.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarFrete(){
		$dadosUpdate = array(	'id' 		=> 	$_POST['orcamento_id'],
							 	'frete_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarFrete($dadosUpdate)){
			$this->log('Área Administrador | atualiza frete','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Frete alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	private function enviarEmailOrcamento($orcamento_id){

		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);		 
		$email = '<html>
				<head></head>
				<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
				<div class="content" style="width: 600px; height: 100%;">
				<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/backgroundCabecalho.png); height: 528px; "></div>
				<div style="width: 600px; background-color: #ffcc00; height: 100%">
					<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
						<tbody>
							<tr>
								<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
								<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
								<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
								<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
							</tr>
							<tr>
								<td style="padding-bottom: 18px;font-weight: 700;">Validade:</td>
								<td style="padding-bottom: 18px;">30 Dias</td>
								<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
								<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
							</tr>
							<tr>
								<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
								<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
								<td style=" font-weight: 700; text-align: right;">Telefone:</td>
								<td>'.$empresa['telefone'].'</td>
							</tr>
							<tr>
								<td style=" font-weight: 700;">E-mail:</td>
								<td colspan="3">'.$empresa['email'].'</td>
							</tr>
						</tbody>
					</table>
					<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
					<table style="width: 570px;margin-left: 14px;" cellspacing="0">
						<thead style="height: 55px;">
							<tr><th style="    height: 30px;">Modelo</th>
							<th>Descrição</th>
							<th>Quantidade</th>			
						</tr></thead>
						<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
				$total = 0; foreach($produtos as $produto){
					
					$email.='	<tr>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
								<td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>							
							</tr>';					
					$total = $total + ($produto['valor'] * $produto['qtd']);
				}
				
				$email.='		
						</tbody>
					</table>
					<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
				</div>
			</div>
		</body>
		</html>';	


		$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to('webmaster@wertco.com.br')
		    ->subject('Orçamento realizado no Sistema pelo administrador:'.$this->session->userdata('nome'))
		    ->message($email)
		    ->send();

		return $result;		
	}

	public function insereProdutosNovosOrcamento(){
		$erro = 0;
		foreach ($_POST['dados'] as $dados) {
			if( $dados['name'] == 'produto_id'){
				$insert['produto_id'] = $dados['value'];
				$i=1;
			}
			if( $dados['name'] == 'qtd'){
				$insert['qtd'] = $dados['value'];
				$i=2;
			}
			if( $dados['name'] == 'valor_unitario'){
				$insert['valor'] = str_replace(",", ".", str_replace(".","", $dados['value']));
				$i=3;
				$insert['orcamento_id'] = $_POST['orcamento_id'];
				if($this->orcamentosM->insereOrcamentoProdutos($insert)){
					$this->log('Área Administrador | insere produto novo','orcamento_produtos','INSERÇÃO', $this->session->userdata('usuario_id'),$insert,$insert,$_SERVER['REMOTE_ADDR']);
					$erro = $erro;
				}else{
					$erro++;
				}
			}			
		}

		if($erro == 0){
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Produto Adicionado ao orçamento por '.$this->session->userdata('nome'),
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			$this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Icms ******************************
	*****************************************************************************/
	public function excluirProdutoOrcamento()
	{
		if($this->orcamentosM->excluirProduto($_POST['orcamento_produto_id'])){
			$this->log('Área Administrador | excluir produto','orcamento_produtos','INSERÇÃO', $this->session->userdata('usuario_id'),array('id' => $_POST['orcamento_produto_id']),array('id' => $_POST['orcamento_produto_id']),$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

    public function gestaoEmpresas($pesquisa = null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->empresasM->getEmpresas();
		$parametros['title']	=	"Gestão de Empresas";
		$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view_feira('area-feira/empresas/gestao-empresas',$parametros);
	}

	public function visualizarEmpresa($id,$pesquisa=null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->empresasM->getEmpresasUsuarios($id);			
		$parametros['subtipos']	=	$this->empresasM->getSubtipoByTipo($parametros['dados'][0]->tipo_cadastro_id);		
		$parametros['title']	=	"Empresa #".$id;
		$parametros['pesquisa'] = 	$pesquisa;
		$parametros['chamados'] = 	$this->chamadoM->selectPorEmpresa($id);	
		$parametros['pedidos'] 	= 	$this->pedidosM->selectPedidosPorEmpresa($id);
		$this->_load_view_feira('area-feira/empresas/visualiza-empresa',$parametros);
	}

	public function cadastrarEmpresa()
	{
		
		if($this->input->post()){

			$dadosEmpresa = array(
		        'razao_social' 		=>	$this->input->post('razao_social'),
				'fantasia' 			=>	$this->input->post('fantasia'),
				'cnpj' 				=>	$this->input->post('cnpj'),
				'telefone' 			=>	$this->input->post('telefone'),
				'endereco' 			=>	$this->input->post('endereco'),
				'email' 			=>	$this->input->post('email'),				
				'cidade' 			=>	$this->input->post('cidade'),
				'estado' 			=>	$this->input->post('estado'),
				'tipo_cadastro_id'	=> 	$this->input->post('tipo_cadastro_id'),
				'bairro'			=> 	$this->input->post('bairro'),
				'cep'				=> 	$this->input->post('cep'),
				'cartao_cnpj'		=>	$this->input->post('cartao_cnpj')
		    );
			
			$conteudo= 	"CNPJ: ". 	$this->input->post('cnpj')." <br/> ";
			$conteudo.= "Razão Social: ". 	$this->input->post('razao_social')." <br/> ";
			$conteudo.=	'Fantasia: '.	$this->input->post('fantasia').' <br/> ';
			$conteudo.=	'CNPJ: '.	$this->input->post('cnpj').' <br/> ';
			$conteudo.=	'Telefone: '.	$this->input->post('telefone').' <br/> ';
			$conteudo.=	'Endereço: '.	$this->input->post('endereco').' <br/> ';
			$conteudo.=	'Cidade:  '	.	$this->input->post('cidade').' <br/> ';
			$conteudo.=	'Estado:  '	.	$this->input->post('estado').' <br/> ';
			$conteudo.=	'País: 	  '.	$this->input->post('pais').' <br/> ';	
			$conteudo.=	'Usuário que cadastrou:	  '.	$this->session->userdata('nome');

			if($this->empresasM->add($dadosEmpresa))
			{
				$this->log('Área Administrador | cadastro empresa','empresas','INSERÇÃO', $this->session->userdata('usuario_id'),$dadosEmpresa,$dadosEmpresa,$_SERVER['REMOTE_ADDR']);
				$this->enviaEmail('webmaster@companytec.com.br','Empresa Cadastrada',$conteudo,$this->input->post('cartao_cnpj'));
				$this->session->set_flashdata('sucesso', 'ok');	
				$this->gestaoEmpresas();			
			}else{
				$this->session->set_flashdata('erro', 'erro');
				$this->gestaoEmpresas();
			}					 
			
		}else{

			$parametros 			= 	$this->session->userdata();		
			$parametros['estados']	=	$this->icmsM->getEstados();
			$parametros['title']	=	"Cadastro de Empresas";
			$this->_load_view_feira('area-feira/empresas/cadastra-empresa',$parametros);
		}
	}

	public function editarEmpresa($id = null, $pesquisa = null){

		if($this->input->post('salvar') == 1){

			$update = $this->input->post();
			
			unset($update['salvar']);
			unset($update['tipo_acesso_id']);
			$update['tipo_cadastro_id'] = $this->input->post('tipo_acesso_id');

			if($this->input->post('venc_inmetro') != null){
				$data 			= 	explode('/',$this->input->post('venc_inmetro'));
				$dt_vencimento	= 	'20'.$data[2].'-'.$data[1].'-'.$data[0];
				$update['venc_inmetro']	=	$dt_vencimento;
			}

			if($this->empresasM->atualizaEmpresas($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				$this->log('Área Administrador | editar empresa','empresas','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				
				$parametros 			= 	$this->session->userdata();
				$parametros['title']	=	"Gestão de Empresas";
				$parametros['dados']	=	$this->empresasM->getEmpresas();	
				$parametros['pesquisa']	= 	$pesquisa;			 
				$this->_load_view_feira('area-feira/empresas/gestao-empresas',	$parametros );
			}

		}else{
			$row 								= 	$this->empresasM->getEmpresa($id);		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Empresas";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();			
			$parametros['pesquisa']				= 	$pesquisa;			 
			$this->_load_view_feira('area-feira/empresas/editar-empresa',	$parametros );	
		}
	}

		
	public function retornaTecnicos()
	{
		
		$rows = $this->usuariosM->retornaTecnicosPedidos($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'		=>	$row['label'],
									'id'		=>	$row['id'],
									'value'		=> 	$row['label'],
									'email'		=> 	$row['email']	);
				
			}
			
			echo json_encode($dados);
		}
	}

	public function retornaTecnicosGeral()
	{
		
		$rows = $this->usuariosM->retornaTecnicosGeral($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'		=>	$row['label'],
									'id'		=>	$row['id'],
									'value'		=> 	$row['label']	);
				
			}
			
			echo json_encode($dados);
		}
	}	

	public function pedidos()
	{
	
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->pedidosM->selectPedidosFeira();
		$parametros['title']	=	"Pedidos";
		$this->_load_view_feira('area-feira/pedidos/pedidos-feira',$parametros);
	
	}

	public function cadastraPedidos()
	{
		if($this->input->post() ){
			if($this->input->post('frete_id') == 2 ){
				$frete = 'C';
			}elseif($this->input->post('frete_id') == 3){
				$frete = 'F';
			}else{
				$frete = 'O';
			}
			
			$dados  = array( 	'orcamento_id'		=> 	$this->input->post('orcamento_id'),
								'pintura'			=>	$this->input->post('pintura'),
								'motor'				=>	$this->input->post('motor'),
								'combustiveis'		=> 	$this->input->post('combustiveis'),
								'fl_receber_info'	=> 	$this->input->post('fl_receber_info'),
								'status_pedido_id'	=> 	1,
								'usuario_geracao'	=>	$this->session->userdata('usuario_id'),
								'tecnico_id'		=> 	$this->input->post('tecnico_id'),
								'endereco_entrega'	=> 	$this->input->post('endereco_entrega'),
								'nome_cliente'		=> 	$this->input->post('nome_cliente'),
								'rg_cliente'		=> 	$this->input->post('rg_cliente'),
								'cpf_cliente'		=> 	$this->input->post('cpf_cliente'),
								'pintura_descricao'	=> 	$this->input->post('pintura_descricao'),
								'empresa_id'		=> 	$this->input->post('empresa_id'),
								'valor_total'		=> 	number_format($this->input->post('valor_total'), 6, '.', ''),
								'valor_total_stx'	=> 	number_format($this->input->post('valor_total_stx'), 6, '.', ''),
								'valor_ipi'			=> 	'5.00',
								'frete'				=> 	$frete,
								'observacao'		=> 	$this->input->post('observacao'),
								'testeira'			=> 	$this->input->post('testeira'), 
								'painel_frontal'	=> 	$this->input->post('painel_frontal'),
								'painel_lateral'	=> 	$this->input->post('painel_lateral'),
								'fl_identidade_visual' => $this->input->post('fl_identidade_visual') );

			$dados['arquivo'] = "";
			if($_FILES['arquivo_bb']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['arquivo_bb']['name']);
				$arquivo 		=	md5($_FILES['arquivo_bb']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$configuracao 	= array(	'upload_path'   	=> 	'./pedidos/',		        
								        	'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv|cdr|zip|rar|jpeg|JPEG',
								        	'file_name'     	=> 	$arquivo 	);      
			    
			    $this->load->library('upload', $configuracao);
			    
			    if ( 	$this->upload->do_upload('arquivo_bb') 	){

			    	$dados['arquivo'] 	= 	$arquivo;
			    }
			}

	   		$aux_produto 	= "";
	   		$aux_produto_id = "";
			$qtd = 1;
							
			for($i=0;$i<count($this->input->post('produto_id'));$i++){
									
				if($this->input->post('produto_id')[$i] == $aux_produto_id && trim(mb_strtoupper($this->input->post('produtos')[$i])) == trim(mb_strtoupper($aux_produto)) ){

					$qtd++;
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array(	'qtd' 		=> 	$qtd,																			'valor'		=> 	$this->input->post('valor')[$i],												'valor_stx'	=> 	$this->input->post('valor_stx')[$i]	);
				}else{
					$qtd =	1;
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array( 'qtd' 		=> 	$qtd,																			'valor'		=> 	$this->input->post('valor')[$i],
								'valor_stx'	=> 	$this->input->post('valor_stx')[$i]	);
				}

				$aux_produto 	= mb_strtoupper($this->input->post('produtos')[$i]);
				$aux_produto_id = $this->input->post('produto_id')[$i];
			}
			
			if( $this->pedidosM->add($dados) ){
				$pedido_id = $this->db->insert_id();
				
				$this->insereAndamentoPedido( $pedido_id, 1 );				
				$insereProd = array();
				$formaPagto = array();
				for($i=0;$i<count($this->input->post('porcentagem'));$i++){
                	$formaPagto = array(    'pedido_id'     =>     	$pedido_id,
                                            'porcentagem' 	=>     	$this->input->post('porcentagem')[$i],
                                            'descricao' 	=>		$this->input->post('descricao')[$i]	);				

					$this->pedidosM->insereFormaPagamento($formaPagto);					
				
				}

				foreach($produtos as $key=>$value)
				{
					$prod = explode('|',$key);
					
					if( $prod[1] == 'OPCIONAL' ){
						$prod[1] = '';
					}

					$insereProd = array('pedido_id' 	=>	$pedido_id,
										'produto_id'	=>	$prod[0],
										'produtos'		=> 	$prod[1],
										'qtd'			=> 	$value['qtd'],
										'valor'			=> 	$value['valor']);
					
					$this->pedidosM->inserePedidoItens($insereProd);
				}

				// caso tenha ocorrido alguma alteração
				if( $this->input->post('edicao_forma_pgto') == 1 || ($this->input->post('edicao_forma_pgto') == 0 && (trim($this->input->post('forma_pagto_orc')) != trim($this->input->post('descricao')[0]) ) ) ){

					$formaPagtoT = '';
					for($i=0;$i<count($this->input->post('porcentagem'));$i++){
	                	$formaPagtoT .= $this->input->post('porcentagem')[$i].'% '.$this->input->post('descricao')[$i].'<br>';		
					}	
					
					$email = '<html>
								<head></head>
								<body style="width: 850px; position:absolute;">
									<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
										<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
										<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">⚠️ Atenção! Forma de Pagamento Alterada ⚠️</h2>
										<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
											Pedido: #'.$pedido_id.'<br/><hr />
											Forma de pagamento orçamento: '.$this->input->post('forma_pagto_orc').'<br/>
											Nova Forma de pagamento: <b>'.$formaPagtoT.'</b><br/><hr /><br/>
											Usuário: '.$this->session->userdata('nome').'
										</p>
										<p><b>NÃO ESQUEÇA DE APROVAR O PEDIDO NO SEU DASHBOARD</b></p>
									</div>
								</body>
							</html>';


					$this->enviaEmail('gestor@wertco.com.br','⚠️ ATENÇÃO! FORMA DE PAGTO ALTERADO. PEDIDO '.$pedido_id.'  ⚠️', $email );
					$this->enviaEmail('administrativo@companytec.com.br','⚠️ ATENÇÃO! FORMA DE PAGTO ALTERADO. PEDIDO '.$pedido_id.'  ⚠️', $email );
					$this->enviaEmail('adm@companytec.com.br','⚠️ ATENÇÃO! FORMA DE PAGTO ALTERADO. PEDIDO '.$pedido_id.'  ⚠️', $email );

					$dados = array(	'id' 				=>	$pedido_id,
					   				'status_pedido_id' 	=>	9 	);

					$this->pedidosM->atualizaStatusPedido($dados);
					$this->insereAndamentoPedido( $pedido_id,9 );

				}

				$this->log('Área Administrador | cadastra pedidos','pedidos','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');
				$pdf = $this->geraPedidosPdf($pedido_id,'tela');
				$this->session->set_flashdata('pedidoPdf', $pdf);
			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->pedidosM->selectPedidos();				
			$parametros['title']	=	"Pedidos";
			$this->_load_view_feira('area-feira/pedidos/pedidos-feira',$parametros);

		}else{ 

			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->pedidosM->selectPedidos();
			$parametros['title']	=	"Pedidos";
			$this->_load_view_feira('area-feira/pedidos/cadastra-pedidos-feira',$parametros);
		}
	}

	public function retornaOrcamentos()
	{
		
		$rows = $this->orcamentosM->retornaOrcamentosFechados($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array(	'label'		=>	$row['label'],
									'id'		=>	$row['id'],
									'value'		=>	$row['label'],
									'frete_id'	=> 	$row['frete_id']	);
				
			}

			echo json_encode($dados);
		}
	}
	
	public function retornaItens()
	{
		
		$rows = $this->itensM->retornaItens($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label'	=>	$row['label'],
								'id'		=>	$row['id'],
								'value'		=>	$row['label']	);
				
			}

			echo json_encode($dados);
		}
	}
	
	public function retornaProdutosAvulsos()
	{
		
		$rows = $this->itensM->retornaProdutosAvulsos($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label'	=>	$row['label'],
								'id'		=>	$row['id'],
								'value'		=>	$row['label'],
								'item_id'	=> 	$row['item_id']);
				
			}

			echo json_encode($dados);
		}
	}
	
	public function retornaProdutosOrcamentos()
	{	
		$retorno = $this->orcamentosM->retornaProdutosOrcamentos($_POST['orcamento_id']);
		echo json_encode($retorno);
	}

	public function editarPedidos($id = null){

		if($this->input->post('salvar') == 1){
			$update = array();
			
			
			if($this->input->post('tecnico') == 0){
				$tecnico_id = 0;
			}else{
				$tecnico_id = $this->input->post('tecnico_id');
			}

			$update = array('id'				=>	$this->input->post('id'),
							'orcamento_id'		=> 	$this->input->post('orcamento_id'),
							'pintura'			=>	$this->input->post('pintura'),							
							'fl_receber_info'	=> 	$this->input->post('fl_receber_info'),														
							'tecnico_id'		=> 	$tecnico_id,
							'endereco_entrega'	=> 	$this->input->post('endereco_entrega'),
							'nome_cliente'		=> 	$this->input->post('nome_cliente'),
							'rg_cliente'		=> 	$this->input->post('rg_cliente'),
							'cpf_cliente'		=> 	$this->input->post('cpf_cliente'),							
							'pintura_descricao'	=> 	$this->input->post('pintura_descricao') );

			if($this->input->post('pintura') != 'bandeira_branca'){
				$update['arquivo'] 	= 	'';	

			}else{
				$tipo 			= 	explode('.', $_FILES['arquivo_bb']['name']);
				$arquivo 		=	md5($_FILES['arquivo_bb']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$configuracao = array(
			        'upload_path'   	=> 	'./pedidos/',		        
			        'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv|cdr',
			        'file_name'     	=> 	$arquivo
			    );      
			    
			    $this->load->library('upload', $configuracao);
			    
			    if ($this->upload->do_upload('arquivo_bb')){

			    	$update['arquivo'] 	= 	$arquivo;
			    }
			}

			$aux_produto 	= "";
	   		$aux_produto_id = "";
			$qtd = 1;
							
			for($i=0;$i<count($this->input->post('produto_id'));$i++){
									
				if($this->input->post('produto_id')[$i] == $aux_produto_id && trim(mb_strtoupper($this->input->post('produtos')[$i])) == trim(mb_strtoupper($aux_produto)) ){

					$qtd++;
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array('qtd' => $qtd,
																																'valor'	=> 	$this->input->post('valor')[$i]);	
				}else{
					$qtd =1; 	
					$produtos[$this->input->post('produto_id')[$i].'|'.mb_strtoupper($this->input->post('produtos')[$i])] = array('qtd' => $qtd,
																																'valor'	=> 	$this->input->post('valor')[$i]);
				}

				$aux_produto 	= mb_strtoupper($this->input->post('produtos')[$i]);
				$aux_produto_id = $this->input->post('produto_id')[$i];
			}		

			if($this->pedidosM->atualizaPedidos($update)){

				$this->log('Área Administrador | atualizar pedidos','pedidos','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				
				for($i=0;$i<count($this->input->post('descricao'));$i++){
					if($this->input->post('descricao')[$i] != ''){
						$formaPagto = array(	'pedido_id' 	=> 	$this->input->post('id'),
												'porcentagem'	=> 	$this->input->post('porcentagem')[$i],
												'descricao'		=> 	$this->input->post('descricao')[$i]);

						$this->pedidosM->insereFormaPagamento($formaPagto);
					}
				}

				$this->pedidosM->excluirPedidoItens($this->input->post('id'));

				foreach($produtos as $key=>$value)
				{
					$prod = explode('|',$key);
					
					if( $prod[1] == 'OPCIONAL' ){
						$prod[1] = '';
					}

					$insereProd = array('pedido_id' 	=>	$this->input->post('id'),
										'produto_id'	=>	$prod[0],
										'produtos'		=> 	$prod[1],
										'qtd'			=> 	$value['qtd'],
										'valor'			=> 	$value['valor']);
					
					$this->pedidosM->inserePedidoItens($insereProd);
				}

				$this->session->set_flashdata('sucesso', 'ok');
				$pdf = $this->geraPedidosPdf($this->input->post('id'),'tela');
				$this->session->set_flashdata('pedidoPdf', $pdf);
				
				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->pedidosM->selectPedidos();				
				$parametros['title']	=	"Pedidos";
				$this->_load_view_feira('area-feira/pedidos/pedidos',$parametros);

			}else{

				$this->session->set_flashdata('erro', 'erro');

				$parametros 			= 	$this->session->userdata();
				$parametros['dados']	=	$this->pedidosM->selectPedidos();				
				$parametros['title']	=	"Pedidos";
				$this->_load_view_feira('area-feira/pedidos/pedidos',$parametros);
			}

		}else{

			$row 						= 	$this->pedidosM->getPedidosById($id);
			$parametros 				= 	$this->session->userdata();
			$parametros['dados']		=	$row;
			$parametros['formaPagto'] 	= 	$this->pedidosM->pedidosFormaPagto($id);
			$parametros['produtos']		= 	$this->pedidosM->pedidosProdutos($id);						
			$parametros['title']		=	"Editar Pedidos";			
			$this->_load_view_feira('area-feira/pedidos/editar-pedidos',	$parametros );
		}
	}

	/************************************************************ 
	************** GERA PDF DO ORÇAMENTO SOLICITADO *************
	*************************************************************/
	public function geraPedidosPdf($pedido_id, $origem_solicitacao)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pedidos/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('pedidos-'.$pedido_id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
		
	    $data['empresa'] 		= 	$this->pedidosM->pedidosEmpresasPdf($pedido_id);
	    $data['produtos'] 		= 	$this->pedidosM->pedidosProdutos($pedido_id);
	    $data['forma_pagto']	= 	$this->pedidosM->pedidosFormaPagto($pedido_id);
	    $data['icms'] 			= 	$this->pedidosM->buscaIcmsOrcamentoPedido($pedido_id);
	    $data['usuario_wertco'] = 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
	    $data['entrega']		= 	$this->pedidosM->buscaFormaEntrega($pedido_id);
	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-feira/pedidos/pedidos-pdf', $data, true));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'pedidos-'.$pedido_id.'.pdf'));
	    	}else{
	    		return $retorno['retorno'] = 'pedidos-'.$pedido_id.'.pdf';
	    	}
	    }	    
	    
    }

	/************************************************************ 
	************** GERA PDF DAS OP'S GERADAS ********************
	*************************************************************/
	public function geraOpsPdf($pedido_id, $origem_solicitacao)
    {
    	//Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pedidos/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('ops-pedidos-'.$pedido_id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
		
	    $data['empresa'] 		= 	$this->pedidosM->pedidosEmpresasPdf($pedido_id);
	    $data['produtos'] 		= 	$this->pedidosM->pedidosProdutos($pedido_id);
	    $data['forma_pagto']	= 	$this->pedidosM->pedidosFormaPagto($pedido_id);
	    $data['icms'] 			= 	$this->pedidosM->buscaIcmsOrcamentoPedido($pedido_id);
		$data['ops'] 			= 	$this->pedidosM->buscaOpsPorPedido($pedido_id);
	    $data['usuario_wertco'] = 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-feira/pedidos/ops-pedidos-pdf', $data, true));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'pedidos-'.$pedido_id.'.pdf'));
	    	}else{
	    		return $retorno['retorno'] = 'pedidos-'.$pedido_id.'.pdf';
	    	}
	    }
	
    }
	
    /****************************************************************************
	********* Método Ajax - Excluir Forma de pagamento Pedidos ******************
	*****************************************************************************/
	public function excluirFormaPagtoPedidos()
	{

		if($this->pedidosM->excluirPedidoFormPagto($_POST['id'])){
			$this->log('Área Administrador | excluir produtos pedidos','pedido_itens','EXCLUSÃO', $this->session->userdata('usuario_id'), array('id' => $_POST['id']),array('id' => $_POST['id']),$_SERVER['REMOTE_ADDR']);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function alteraStatusPedido()
	{

		$dados = array('id'					=>	$_POST['pedido_id'],
					   'status_pedido_id' 	=>	$_POST['status_pedido']);

		if($this->pedidosM->atualizaStatusPedido($dados))
		{
			if($_POST['status_pedido'] == 2){ 
			
				$email='<html>
						<head></head>
						<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
							<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
								<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido '.$_POST['pedido_id'].' Confirmado pelo cliente</h1>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Pedido: '.$_POST['pedido_id'].' Confirmado pelo cliente <br/>
									Usuário: '.$this->session->userdata('nome').'
									<br/>
								</p>
							</div>
						</body>
					</html>';

	    		$this->enviaEmail('servicos@wertco.com.br','Pedido '.$_POST['pedido_id'].' confirmado enviado para produção', $email);
	    	}
			
			$this->log('Área Administrador | atualizar status pedido','pedidos','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
			echo json_encode(array('retorno' => 'sucesso'));
			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}
	
	public function alteraStatusOp()
	{

		$dados = array('id'				=>	$_POST['op_id'],
					   'status_op_id' 	=>	$_POST['status_op']);

		if($this->opM->atualizarOrdemProducao($dados))
		{
			$this->log('Área Administrador | atualizar status op','op','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);			
			echo json_encode(array('retorno' => 'sucesso'));	
			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function confirmaPedidoOps($id)
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados = $this->input->post();

			$updatePedido = array('id' 				 => $id,
								  'status_pedido_id' => 2);

			$this->pedidosM->atualizaPedidos($updatePedido);
			
			$i = 1;
			$erro = 0;			
			
			//var_dump($this->input->post('produto_id'));die;
			//MONTA SQL PARA VERIFICAR SE EXISTE ITEM			
			foreach ($this->input->post('produto_id') as $produto_id) {
				$whereProduto=$produto_id;
				$monta_where = " i.produto_id = ". $produto_id;
				$j=0;
				$insereItem = array();
				$insereSubItem = array();
				$descricao 	= "";
				$verifica = false;
				$whereIni="";
				$whereFim="";
				$whereNotIn="";				
				$obs_subitem="";
				//echo count($dados['subitem'][$produto_id][$i]) .">= 1 &&". $dados['subitem'][$produto_id][$i][$j]."!= ''";die;
				
				$unico = 0;
				if(count($dados['subitem'][$produto_id][$i]) >= 1 && $dados['subitem'][$produto_id][$i][$j] != ""){
					
					foreach($this->input->post('subitem')[$produto_id][$i] as $subitem){
						
						$subitem_id = explode('+',$subitem);
						
						if($subitem_id[0] != ''){

							$sql = "SELECT count(*) as total FROM itens i, subitens s
							         WHERE i.id = s.item_id and i.produto_id = ". $produto_id."
									 and (s.produto_id=". $subitem_id[0]."
									 and s.qtd=".$this->input->post('subitem_qtd')[$produto_id][$i][$j].")";							
														
							$retorno = $this->itensM->verificaItem($sql);
							
							if($retorno['total'] == 0){
								$verifica = true;									
								
							}else{
								
								/*$sql = "SELECT i.id FROM itens i, subitens s
							         WHERE i.id = s.item_id and i.produto_id = ". $produto_id."
									 and (s.produto_id=". $subitem_id[0]."
									 and s.qtd=".$this->input->post('subitem_qtd')[$produto_id][$i][$j].")";
									 
								$id_item = $this->itensM->verificaItem($sql);*/

								$whereIni.="(select item_id from subitens WHERE item_id in ";
								$whereFim.=" and produto_id = ". $subitem_id[0]." and qtd=".$this->input->post('subitem_qtd')[$produto_id][$i][$j].")";
								$whereNotIn.=" and (produto_id != ". $subitem_id[0]." and qtd != ".$this->input->post('subitem_qtd')[$produto_id][$i][$j].")";
								$whereProduto = $produto_id;
								
							}
							
							$sql_item 	= "select id from itens where produto_id =".$subitem_id[0];
							
							$item_filho = $this->itensM->getIdItem($sql_item);  
													
							$insereSubItem[] = array( 'item_filho'	=> 	$item_filho, 
													  'produto_id' 	=> 	$subitem_id[0],
													  'qtd'			=> 	$this->input->post('subitem_qtd')[$produto_id][$i][$j],
													  'descricao'	=> 	$subitem_id[1]);
							
							$descricao .= ', '.$subitem_id[1]; 
							$obs_subitem.= ' '.$this->input->post('subitem_qtd')[$produto_id][$i][$j].' - ' .$subitem_id[1].'. ';
						}
												
						$j++;

					}
					
					/*echo '<pre>';
					var_dump($insereSubItem);
					die;*/
				}else{
					$sql = "SELECT count(*) as total FROM itens i
							left join subitens s on i.id = s.item_id
							 WHERE i.produto_id = ". $produto_id."
							 and s.item_id is null ";
									 
					$retorno = $this->itensM->verificaItem($sql);
					if($retorno['total'] == 0){
						$verifica= true;									
						
					}else{
						$sql = "SELECT i.id FROM itens i
							 WHERE i.produto_id = ". $produto_id ." and i.id not in (select item_id from subitens)";
						
						$verifica= false;
						$unico = 1;
						$id_item = $this->itensM->verificaItem($sql);
					}
				}

				//echo ($verifica) ? 'verifica: true' : 'verifica: false';
				//echo '<br>unico:'.$unico.'<br>';				
				if($verifica){
					
					/***************** INSERE NAS TABELAS ITENS E SUBITENS ***********************/
					$insereItem = array('produto_id' 	=> $produto_id,
										'descricao'		=> $this->input->post('item_descricao')[$produto_id][$i][0].','.$descricao );
					
					//INSERE NA TABELA ITENS
					if(!$this->itensM->insereItens($insereItem)){
						$erro = 1;
					}

					$item_id = $this->db->insert_id();
					
					//INSERE NA TABELA SUBITENS
					foreach( $insereSubItem as $insereSubs ){
						
						if($insereSubs['item_filho'] == '' || $insereSubs['item_filho'] == null){
							$insereSubs['item_filho'] = $produto_id;
						}

						$inserir = array( 	'item_filho' => $insereSubs['item_filho'],	
											'produto_id' => $insereSubs['produto_id'],
											'qtd'	 	 =>	$insereSubs['qtd'],
											'item_id'	 => $item_id 	);

						if(!$this->itensM->insereSubItens($inserir)){
							$erro = 1;
						}

					}

				}else{

					if($unico==0){
						//$item_id = $id_item['id'];
						$sql = "SELECT distinct item_id FROM subitens, itens i WHERE item_id in " .$whereIni." (SELECT item_id FROM `subitens` WHERE 1=1 ".$whereFim.") and item_id not in (SELECT distinct item_id from subitens where 1=1 ".$whereNotIn.")  and i.id = subitens.item_id and i.produto_id = ".$whereProduto;
						
						$id_item = $this->itensM->verificaItem($sql);
						$item_id = $id_item['item_id'];
						
					}else{
						$item_id = $id_item['id'];
						//echo 'item_id: '.$item_id.'<br>';
					}

				}

				
				/*********************************** INSERE NA tabela ORDEM DE PRODUÇÃO ********************************/
				//Caso não tenha item ainda, salva o mesmo
				
				if($item_id == ''){
					/***************** INSERE NAS TABELAS ITENS E SUBITENS ***********************/
					$insereItem = array('produto_id' 	=> $produto_id,
										'descricao'		=> $this->input->post('item_descricao')[$produto_id][$i][0].','.$descricao );
					
					//INSERE NA TABELA ITENS
					if(!$this->itensM->insereItens($insereItem)){
						$erro = 1;
					}

					$item_id = $this->db->insert_id();
					
					//INSERE NA TABELA SUBITENS
					foreach( $insereSubItem as $insereSubs ){
						
						if($insereSubs['item_filho'] == '' || $insereSubs['item_filho'] == null){
							$insereSubs['item_filho'] = $produto_id;
						}

						$inserir = array( 	'item_filho' => $insereSubs['item_filho'],	
											'produto_id' => $insereSubs['produto_id'],
											'qtd'	 	 =>	$insereSubs['qtd'],
											'item_id'	 => $item_id 	);

						if(!$this->itensM->insereSubItens($inserir)){
							$erro = 1;
						}

					}
				}

				$sql = "SELECT count(*) as total, op.id 
				 		 FROM  ordem_producao op, itens i 
				 		 WHERE op.item_id = i.id 
				 		 	and i.id = ". $item_id ." and op.pedido_id = ".$id." group by id";						
				
				$ops = $this->opM->verificaOp($sql);				
				$observacao = "";
				$modelo = explode('-',$this->input->post('modelo')[$produto_id][$i][0]);					
					
				$vazao = '';

				if( count($modelo) == 2 ){
					$vazao = "Vazão Baixa - 40 L/Min";

				}elseif(count($modelo) == 3){
					switch ($modelo[count($modelo)-1]) {
						case 'MV':
							$vazao = "Vazão Média - 75 L/Min";
							break;
						case 'AV':
							$vazao = "Vazão Alta - 125 L/Min";
							break;
						case 'BL':
							$vazao = "Vazão Baixa - 40 L/Min";
							break;
						case 'BF':
							$vazao = "Vazão Baixa - 40 L/Min";
							break;
						
					}
				
				}elseif(count($modelo) == 4){

					if($modelo[count($modelo)-1]=='MV'){
						$vazao = "Média Vazão - 75 L/Min";

					}elseif($modelo[count($modelo)-1]=='AV'){
						$vazao = "Vazão Alta - 130 L/Min";

					}else{
						$vazao = "Vazão Mista - ".$modelo[count($modelo)-1];

					}
				}
				
				if( $ops['total'] == 0)
				{
					$codigo = explode('-', $this->input->post('item_descricao')[$produto_id][$i][0]);
					$observacao = 'Opcionais: '.$obs_subitem.'Produtos: '.$dados['produtos'][$produto_id][$i][0].', ';
					$observacao.= 'Pintura: '.$dados['pintura'][$produto_id][$i][0].', ';
					$observacao.= 'Potência: '.$dados['potencia'][$produto_id][$i][0].', ';
					$observacao.= 'Voltagem: '.$dados['voltagem'][$produto_id][$i][0].', ';
					$observacao.= 'Frequência: '.$dados['frequencia'][$produto_id][$i][0].', ';
					$observacao.= 'Marca do Motor: WEG, ';
					$observacao.= 'Motor: '.$dados['motor'][$produto_id][$i][0].', ';
					$observacao.= 'Vazão: '.$vazao.', ';
					$observacao.= 'Mangueira: '.$dados['mangueira'][$produto_id][$i][0].', ';
					$observacao.= 'Bicos: '.$dados['bicos'][$produto_id][$i][0].', ';
					$observacao.= 'Obs: '.$dados['obs'][$produto_id][$i][0].', |#|' ;
					$data_previsao 	= 	explode('/',$this->input->post('dthr_previsao'));
					$insereOp 	= array(	'item_id' 			=> 	$item_id,
										  	'status_op_id'		=> 	1,
										  	'pedido_id' 		=> 	$id,
										  	'observacao'		=> 	mb_strtoupper($observacao),
										  	'qtd' 				=>  1,
										  	'usuario_geracao' 	=> 	mb_strtoupper($this->session->userdata('usuario_id')),
										  	'modelo' 			=> 	mb_strtoupper($this->input->post('modelo')[$produto_id][$i][0]),
										  	'modelo_tecnico'	=> 	mb_strtoupper($this->input->post('modelo_tecnico')[$produto_id][$i][0]),
											'nome_cliente'		=> 	mb_strtoupper($this->input->post('cliente')),
											'codigo'			=> 	$codigo[0],											
											'combustivel'		=>	mb_strtoupper($dados['produtos'][$produto_id][$i][0]),
											'cliente_id'		=> 	$this->input->post('cliente_id'),	
											'cidade'			=> 	mb_strtoupper($this->input->post('cidade')),
											'uf'				=> 	mb_strtoupper($this->input->post('uf')),
											'pais'				=> 	mb_strtoupper($this->input->post('pais')),
											'descricao'			=> 	mb_strtoupper($codigo[count($codigo)-1].$descricao),
											'dthr_previsao'		=> 	$data_previsao[2].'-'.$data_previsao[1].'-'.$data_previsao[0],
											'informacoes'		=> 	trim(mb_strtoupper($this->input->post('informacoes'))) ) ;

					if(!$this->opM->insereOp($insereOp)){
						$erro = 1;
					}
					
				}else{

					$codigo = explode('-', $this->input->post('item_descricao')[$produto_id][$i][0]);
					$observacao = 'Opcionais: '.$obs_subitem.'Produtos: '.$dados['produtos'][$produto_id][$i][0].', ';
					$observacao.= 'Pintura: '.$dados['pintura'][$produto_id][$i][0].', ';
					$observacao.= 'Potência: '.$dados['potencia'][$produto_id][$i][0].', ';
					$observacao.= 'Voltagem: '.$dados['voltagem'][$produto_id][$i][0].', ';
					$observacao.= 'Frequência: '.$dados['frequencia'][$produto_id][$i][0].', ';
					$observacao.= 'Marca do Motor: WEG, ';
					$observacao.= 'Motor: '.$dados['motor'][$produto_id][$i][0].', ';
					$observacao.= 'Vazão: '.$vazao.', ';
					$observacao.= 'Mangueira: '.$dados['mangueira'][$produto_id][$i][0].', ';
					$observacao.= 'Bicos: '.$dados['bicos'][$produto_id][$i][0].', ';
					$observacao.= 'Obs: '.$dados['obs'][$produto_id][$i][0].', |#|';

					$qtdOp 		= $this->opM->verificaQtd($ops['id']);					
					$obs 		= $qtdOp['observacao'].$observacao;
					$combustivel= $qtdOp['combustivel'].' - '. $dados['produtos'][$produto_id][$i][0];
					$updateQtd 	= array('id' 			=> 	$ops['id'],
										'qtd'			=> 	$qtdOp['qtd']+1,
										'observacao' 	=>	mb_strtoupper($obs),
										'combustivel'	=> 	mb_strtoupper($combustivel) );

					if(!$this->opM->atualizaQtdObs($updateQtd)){
						$erro = 1;
					}

				}

				$i++;
				
			}
			
			$ops = $this->opM->getOpsByPedido($id);
			$apiWorkfeed = array(	'acao' => 'agendamento_op',
								 	'versao'=>	'0.0'	);
			foreach( $ops as $op){
				$apiWorkfeed['ops'][] = array(	'op' 		=>	$op['id'],
												'item' 		=> 	$op['item_id'],
												'qtd'		=> 	$op['qtd'],
												'modelo'	=> 	$op['modelo']	);
			}

			//Envia para o workfeed as informações das op's geradas
			/*$url = 'http://177.135.100.198/WorkFeed/Api/ApiWertco.php';			
			$options = array(
			    'http' => array(
			        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			        'method'  => 'POST',
			        'content' => http_build_query($apiWorkfeed)
			    )
			);
			$context  = stream_context_create($options);
			$result = file_get_contents($url, false, $context);
			
			if ($result === FALSE) { 
				$this->session->set_flashdata('erro', 'erro');
			}else{
				//echo $result;die;
				// Envia email para o Miro e para o Marcelo
				//$this->enviaEmailProducao('suprimentos@wertco.com.br','suprimentos');
				//$this->enviaEmailProducao('qualidade@wertco.com.br','producao');
				$this->session->set_flashdata('sucesso', 'ok');
			}	
			*/			
			$this->session->set_flashdata('sucesso', 'ok');
			
			$this->enviaEmailProducao('suprimentos@wertco.com.br','suprimentos',$id);
			$this->enviaEmailProducao('qualidade@wertco.com.br','producao',$id);
			$this->enviaEmailProducao('engenharia@wertco.com.br','marcio',$id);
			$this->enviaEmailProducao('webmaster@companytec.com.br','mateus',$id); 
			$this->enviaEmailProducao('producao@wertco.com.br','leomar',$id); 
			
			$email='<html>
					<head></head>
					<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
						<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Pedido '.$id.' confirmado e ops geradas </h1>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">							
							Pedido: '.$id.'<br/>
							Cliente: '.$this->input->post('cliente').'<br/>
							Usuario: '.$this->session->userdata('nome').'
							</p>
						</div>
					</body>
				</html>';

	    	$this->enviaEmail('servicos@wertco.com.br','⛽ Pedido:'.$id.' confirmado pelo cliente e ops geradas', $email); 
						
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->pedidosM->selectPedidos();				
			$parametros['title']	=	"Pedidos";

			$this->_load_view_feira('area-feira/pedidos/pedidos',$parametros);
			 
		}else{

			$row 						= 	$this->pedidosM->getPedidosById($id);
			$parametros 				= 	$this->session->userdata();
			$parametros['dados']		=	$row;			
			$parametros['produtos']		= 	$this->pedidosM->pedidosProdutos($id);
			$parametros['title']		=	"Confirmação de Pedidos/Geração de Ordem de Produção";
			$parametros['id']			= 	$id;
			$parametros['cliente']		= 	$this->pedidosM->retornaClientePorPedido($id);			
			$this->_load_view_feira('area-feira/pedidos/confirm-gera-pedidos', $parametros );
		}
	}

	public function confirmaEnviaEmailPedido(){

		$pedido_id 	= $_POST['pedido_id'];
		$email 		= $_POST['email'];

		$update = array('id'				=> 	$pedido_id,
						'status_pedido_id' 	=> 	4);

		if($this->pedidosM->atualizaStatusPedido($update)){

			if($this->enviaEmailConfirmacaoPedido($pedido_id,$email)) {
				echo json_encode(array('retorno' => 'sucesso'));
			}else{
				echo json_encode(array('retorno' => 'Não foi enviado e-mail'));
			}

		}else{
			echo json_encode(array('retorno' => 'Erro: Não atualizou o pedido'));
		}

	}	

	private function enviaEmailConfirmacaoPedido($pedido_id, $email_usuario){
		
		$dados = $this->opM->verificaOpFechadaPorPedido($pedido_id);

		if( $dados['total'] > 0)
		{
			return false;

		}else{

			$ops = $this->opM->getOpsByPedido($pedido_id);

			$email = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title>teste</title><style type="text/css">
        /* Resets */
        .ReadMsgBody { width: 100%; background-color: #ebebeb;}
        .ExternalClass {width: 100%; background-color: #ebebeb;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        a[x-apple-data-detectors]{
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
        }        
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        body {margin:0; padding:0;}
        .yshortcuts a {border-bottom: none !important;}
        .rnb-del-min-width{ min-width: 0 !important; }

        /* Add new outlook css start */
        .templateContainer{
            max-width:590px !important;
            width:auto !important;
        }
        /* Add new outlook css end */

        /* Image width by default for 3 columns */
        img[class="rnb-col-3-img"] {
        max-width:170px;
        }

        /* Image width by default for 2 columns */
        img[class="rnb-col-2-img"] {
        max-width:264px;
        }

        /* Image width by default for 2 columns aside small size */
        img[class="rnb-col-2-img-side-xs"] {
        max-width:180px;
        }

        /* Image width by default for 2 columns aside big size */
        img[class="rnb-col-2-img-side-xl"] {
        max-width:350px;
        }

        /* Image width by default for 1 column */
        img[class="rnb-col-1-img"] {
        max-width:550px;
        }

        /* Image width by default for header */
        img[class="rnb-header-img"] {
        max-width:590px;
        }

        /* Ckeditor line-height spacing */
        .rnb-force-col p, ul, ol{margin:0px!important;}
        .rnb-del-min-width p, ul, ol{margin:0px!important;}

        /* tmpl-2 preview */
        .rnb-tmpl-width{ width:100%!important;}

        /* tmpl-11 preview */
        .rnb-social-width{padding-right:15px!important;}

        /* tmpl-11 preview */
        .rnb-social-align{float:right!important;}

        @media only screen and (min-width:590px){
        /* mac fix width */
        .templateContainer{width:590px !important;}
        }

        @media screen and (max-width: 360px){
        /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
        .rnb-yahoo-width{ width:360px !important;}
        }

        @media screen and (max-width: 380px){
        /* fix width and font size "tmpl-4 tmpl-6" in mobile preview */
        .element-img-text{ font-size:24px !important;}
        .element-img-text2{ width:230px !important;}
        .content-img-text-tmpl-6{ font-size:24px !important;}
        .content-img-text2-tmpl-6{ width:220px !important;}
        }

        @media screen and (max-width: 480px) {
        td[class="rnb-container-padding"] {
        padding-left: 10px !important;
        padding-right: 10px !important;
        }

        /* force container nav to (horizontal) blocks */
        td.rnb-force-nav {
        display: inherit;
        }
        }

        @media only screen and (max-width: 600px) {

        /* center the address &amp; social icons */
        .rnb-text-center {text-align:center !important;}

        /* force container columns to (horizontal) blocks */
        td.rnb-force-col {
        display: block;
        padding-right: 0 !important;
        padding-left: 0 !important;
        width:100%;
        }

        table.rnb-container {
         width: 100% !important;
        }

        table.rnb-btn-col-content {
        width: 100% !important;
        }
        table.rnb-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-last-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table[class~="rnb-col-2"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-col-2-noborder-onright {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        }

        table.rnb-col-2-noborder-onleft {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-top: 10px;
        padding-top: 10px;
        }

        table.rnb-last-col-2 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table.rnb-col-1 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        img.rnb-col-3-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xs {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xl {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-1-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-header-img {
        /**max-width:none !important;**/
        width:100% !important;
        margin:0 auto;
        }

        img.rnb-logo-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        td.rnb-mbl-float-none {
        float:inherit !important;
        }

        .img-block-center{text-align:center !important;}

        .logo-img-center
        {
            float:inherit !important;
        }

        /* tmpl-11 preview */
        .rnb-social-align{margin:0 auto !important; float:inherit !important;}

        /* tmpl-11 preview */
        .rnb-social-center{display:inline-block;}

        /* tmpl-11 preview */
        .social-text-spacing{margin-bottom:0px !important; padding-bottom:0px !important;}

        /* tmpl-11 preview */
        .social-text-spacing2{padding-top:15px !important;}

    }</style><!--[if gte mso 11]><style type="text/css">table{border-spacing: 0; }table td {border-collapse: separate;}</style><![endif]--><!--[if !mso]><!--><style type="text/css">table{border-spacing: 0;} table td {border-collapse: collapse;}</style> <!--<![endif]--><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body>

<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#f9fafc" style="background-color:#f9fafc;">

    <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr>
    <tr>
        <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="590" style="width:590px;">
                        <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
        <tbody><tr>

        <td align="center" valign="top" bgcolor="#f9fafc" style="background-color:#f9fafc;">

            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px; background-color:#f9fafc;" name="Layout_0" id="Layout_0">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" bgcolor="#f9fafc" style="min-width:590px; background-color:#f9fafc;">
                        <table width="100%" cellpadding="0" border="0" height="30" cellspacing="0" bgcolor="#f9fafc" style="background-color:#f9fafc;">
                            <tbody><tr>
                                <td valign="top" height="30">
                                    <img width="20" height="30" style="display:block; max-height:30px; max-width:20px;" alt="" src="http://img.mailinblue.com/new_images/rnb/rnb_space.gif">
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr><tr>

        <td align="center" valign="top" bgcolor="#f9fafc" style="background-color:#f9fafc;">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#f9fafc" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px; background-color:#f9fafc;" name="Layout_5" id="Layout_5">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color:#ffffff;">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius: 0px; width:590;;max-width:698px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/bombaHeader.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top" bgcolor="#f9fafc" style="background-color:#f9fafc;">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#f9fafc" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px; background-color:#f9fafc;" name="Layout_6" id="Layout_6">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color:#ffffff;">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:700px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/upApp/'.$ops[count($ops)-1]['foto'].'">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
    	</div></td>
    </tr><tr>

        <td align="center" valign="top" bgcolor="#f9fafc" style="background-color:#f9fafc;">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#f9fafc" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px; background-color:#f9fafc;" name="Layout_7" id="Layout_7">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color:#ffffff;">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:700px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/bombaFooter.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        </td>
        </tr>
        </tbody></table>

</body></html>';

				$this->load->library('email');

				$result = $this->email
							    ->from('webmaster@companytec.com.br')
							    ->reply_to('webmaster@companytec.com.br')
							    ->to($email_usuario)
						    	->subject('Wertco - Seu pedido está pronto!')
						    	->message($email)
						    	->send();
			return $result;
		}
	
	}

	private function enviaEmailProducao($email_destino,$tipo,$id){
				
		$ops = $this->opM->getOpsByPedido($id);

		$email='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title>tst</title><style type="text/css">.ReadMsgBody{width:100%;background-color:#ebebeb}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important;font-size:inherit!important;font-family:inherit!important;font-weight:inherit!important;line-height:inherit!important}body{-webkit-text-size-adjust:none;-ms-text-size-adjust:none;margin:0;padding:0}.yshortcuts a{border-bottom:none!important}.rnb-del-min-width{min-width:0!important}.templateContainer{max-width:590px!important;width:auto!important}img[class=rnb-col-3-img]{max-width:170px}img[class=rnb-col-2-img]{max-width:264px}img[class=rnb-col-2-img-side-xs]{max-width:180px}img[class=rnb-col-2-img-side-xl]{max-width:350px}img[class=rnb-col-1-img]{max-width:550px}img[class=rnb-header-img]{max-width:590px}.rnb-del-min-width p,.rnb-force-col p,ol,ul{margin:0!important}.rnb-tmpl-width{width:100%!important}.rnb-social-width{padding-right:15px!important}.rnb-social-align{float:right!important}@media only screen and (min-width:590px){.templateContainer{width:590px!important}}@media screen and (max-width:360px){.rnb-yahoo-width{width:360px!important}}@media screen and (max-width:380px){.content-img-text-tmpl-6,.element-img-text{font-size:24px!important}.element-img-text2{width:230px!important}.content-img-text2-tmpl-6{width:220px!important}}@media screen and (max-width:480px){td[class=rnb-container-padding]{padding-left:10px!important;padding-right:10px!important}td.rnb-force-nav{display:inherit}}@media only screen and (max-width:600px){table.rnb-col-1,table.rnb-col-2-noborder-onleft,table.rnb-col-3,table.rnb-last-col-2,table.rnb-last-col-3{float:none!important}.img-block-center,.rnb-text-center{text-align:center!important}td.rnb-force-col{display:block;padding-right:0!important;padding-left:0!important;width:100%}img.rnb-col-1-img,img.rnb-col-2-img,img.rnb-col-2-img-side-xl,img.rnb-col-2-img-side-xs,img.rnb-col-3-img,img.rnb-header-img,img.rnb-logo-img,table.rnb-btn-col-content,table.rnb-col-1,table.rnb-col-2-noborder-onleft,table.rnb-col-3,table.rnb-container,table.rnb-last-col-2,table.rnb-last-col-3{width:100%!important}table.rnb-col-3{margin-bottom:10px;padding-bottom:10px}table.rnb-col-2-noborder-onright,table[class~=rnb-col-2]{float:none!important;width:100%!important;margin-bottom:10px;padding-bottom:10px}table.rnb-col-2-noborder-onleft{margin-top:10px;padding-top:10px}.logo-img-center,.rnb-social-align,td.rnb-mbl-float-none{float:inherit!important}img.rnb-header-img{margin:0 auto}.rnb-social-align{margin:0 auto!important}.rnb-social-center{display:inline-block}.social-text-spacing{margin-bottom:0!important;padding-bottom:0!important}.social-text-spacing2{padding-top:15px!important}}</style><!--[ifgtemso11]><style type="text/css">table,</style><![endif]--><!--[if!mso]><!--><style type="text/css">table{border-spacing:0}table td{border-collapse:collapse}</style> <!--<![endif]--><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body>
<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#f9fafc" style="background-color: rgb(249, 250, 252);">
    <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr>
    <tr>
        <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="590" style="width:590px;">
                        <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
        <tbody><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_5" id="Layout_5">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:600px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/backgroundCabecalhoOps.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">
            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_6">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 0px; padding-right: 0px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px">
	<tbody>
					<tr>
						<td style="padding-bottom: 18px;font-weight: 700;">Pedido:</td>
						<td style="padding-bottom: 18px;">'.$ops[0]['pedido_id'].'</td>
						
					</tr>
					<tr>
						<td style="padding-bottom: 18px;font-weight: 700;">Cliente:</td>
						<td style="padding-bottom: 18px;">'.$ops[0]['nome_cliente'].'</td>						
					</tr>					
				</tbody>
</table>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_7" id="Layout_7">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffcc00" align="center" cellspacing="0" style="background-color: rgb(255, 204, 0);">
                            <tbody><tr>
                                <td valign="top" align="left">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:296;;max-width:296px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="296" class="rnb-header-img" alt="" style="display: block;float: left;border-radius: 0px;margin: 0 15px; " src="http://www.wertco.com.br/email-cliente/Ordens_de_producao.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_9">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 0px; padding-right: 0px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><table cellspacing="0" style="width: 570px;margin-left: 14px;padding-bottom: 25px">
	<thead style="height: 55px">
		<tr>
			<th style="height: 30px">Nr.Op</th>
			<th style="text-align: center;">Modelo - Descrição</th>
			<th>Qtd</th>
		</tr>
	</thead>
	<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;" bgcolor="#ffffff">';
		$total = 0; foreach($ops as $op){
			
			$email.='	<tr>
						<td style="border-right: 1px solid #f0f0f0;    padding: 10px;">'.$op['id'].'</td>
						<td style="border-right: 1px solid #f0f0f0;    padding: 10px;">'.$op['modelo'].' - '.$op['descricao'].'</td>
						<td style="text-align: center;border-right: 1px solid #f0f0f0;   padding: 10px;">'.$op['qtd'].'</td>						
					</tr>';					
			
		}			
		$email.='	</tbody>
</table>
</td>
	</tr>
	</tbody>
	</table>
 	</td></tr>
 		</tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_4" id="Layout_4">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="590" class="rnb-container rnb-yahoo-width" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#f9fafc" style="padding-right: 20px; padding-left: 20px; background-color: rgb(249, 250, 252);">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-size:14px; color:#888888; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
                                    <div>© 2018 wertco</div>
                                </td></tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        </td>
        </tr>
        </tbody></table>

</body></html>';
		$this->load->library('email');

		if( $ops[0]['upload_bb'] != 'NULL'){
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject('⛽ Ordens de produção | pedido: '.$id.' ⛽')
			    ->message($email)
			    ->attach('./pedidos/'.$ops[0]['upload_bb'])
			    ->send();
		}else{
		
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject('⛽ Ordens de produção | pedido: '.$id.' ⛽')
			    ->message($email)			    
			    ->send();
		}

		return $result;
			
	}
	
	public function ops()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->opM->selectOps();				
		$parametros['title']	=	"Ops";
		$this->_load_view_feira('area-feira/ops/ops',$parametros);
	}
	
	public function cadastraOps()
	{

		if($this->input->post('salvar') == 1){
			
			$descricao = explode('|', 	$this->input->post('itens'));
			$insereOp = array(	'item_id' 			=> 	$this->input->post('item_id'),
								'pedido_id' 		=> 	0,
								'status_op_id' 		=> 	1,
								'observacao_manual' => 	$this->input->post('observacoes_manual'),
								'modelo' 			=>  'Nenhum',
								'qtd'				=> 	$this->input->post('qtd'),
								'nome_cliente'		=> 	'WERTCO IND. COM. E SERVIÇOS DE MANUTENÇÃO EM BOMBAS LTDA. | 27.314.980/0001-53',
								'cliente_id'		=> 	 2,
								'cidade'			=> 	'Arujá',
								'uf'				=> 	'SP',
								'pais'				=> 	'Brasil',
								'codigo'			=> 	'S/Código',
								'descricao'			=> 	$descricao[count($descricao)-1],
								'combustivel'		=> 	'Nenhum',
								'observacao' 		=> 	'Ordem de Produção Avulsa, produza o ítem: '.$descricao[count($descricao)-1]);
			
			if( $this->opM->insereOp($insereOp) ){
				
				$this->session->set_flashdata('sucesso', 'ok');
				
			}else{
				$this->session->set_flashdata('erro', 'erro');
			}
			
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$this->opM->selectOps();				
			$parametros['title']	=	"Ops";
			$this->_load_view_feira('area-feira/ops/ops',$parametros);
		}else{
			$parametros 			= 	$this->session->userdata();			
			$parametros['title']	=	"Cadastro de Ops";
			$this->_load_view_feira('area-feira/ops/cadastra-ops',$parametros);
		}
		
	}
	
	public function editaOps($id)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['op']		=	$this->opM->find($id);				
		$parametros['title']	=	"Edição de Ops";
		$this->_load_view_feira('area-feira/ops/edita-ops',$parametros);
	}
	
	public function editarOps()
	{
		$op = $this->input->post();
		if($this->opM->atualizarOrdemProducao($op)){
			$this->session->set_flashdata('success', 'ok.');	
		}else{
			$this->session->set_flashdata('erro', 'erro.');
		}
		redirect('areaAdministrador/ops');
	}
	
	public function excluirOps()
	{
		$id = $this->input->post('id');
		//$op = array('id' => $this->input->post('id') ); 
		//$op['status_op_id'] = 4;
		
		if($this->opM->excluirOrdemProducao($this->input->post('id'))){
			echo json_encode(array('status' => 'success',
									'titulo' => 'OP excluida',
									'mensagem' => 'OP excluida com sucesso'));
		}else{
			echo json_encode(array('status' => 'error',
									'titulo' => 'OP não excluida',
									'mensagem' => 'Nã foi possível excluir a OP'));	
		}
	}
	
	public function insereItensSubitens(){
		
		$produto = array('descricao' 		=> 	$_POST['item_desc'],
						'tipo_produto_id'	=>	5,
						'codigo' 			=>  '',
						'modelo'            =>  '',						
						'valor_unitario'    =>  '0.00',
						'modelo_tecnico'	=> '',
						'vazao'				=> '',
						'nr_rotativa'		=> 0);
		
		$this->produtosM->insereProduto($produto);		
		$produto_id = $this->db->insert_id();
		
		$item = array(	'produto_id' 	=> $produto_id,
						'descricao'		=> $_POST['item_desc']);
		
		$this->itensM->insereItens($item);		
		$item_pai = $this->db->insert_id();
		$erro = 0;
				
		$sub_descricao 		= 	json_decode(stripslashes($_POST['sub_descricao']));
		$subitens_item_id 	= 	json_decode(stripslashes($_POST['subitens_item_id']));
		$sub_qtd 			= 	json_decode(stripslashes($_POST['sub_qtd']));

		for($i=0;$i<count($sub_descricao);$i++){
			if($sub_descricao[$i] != ''){
				$insert = array( 'produto_id'	=> 	$sub_descricao[$i],
								 'item_id'		=>	$item_pai,
								 'item_filho'	=>  $subitens_item_id[$i],
								 'qtd'			=> 	$sub_qtd[$i]);
				
				if(!$this->itensM->insereSubItens($insert)){
					$erro++;	
				}
			}
				
		}
		
		if( $erro == 0 ){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
			
		}
	}

	public function verificaOpsPedido(){

		$rows = $this->pedidosM->buscaOpsPorPedido($_POST['pedido_id']);		
		echo json_encode($rows);

	}

	public function anexarNfe(){
		
		$tipo 			= 	explode('.', $_FILES['nfe']['name']);
		$arquivo 		=	md5($_FILES['nfe']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
		
		$configuracao = array(
	        'upload_path'   	=> 	'./nfe/',		        
	        'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv',
	        'file_name'     	=> 	$arquivo
	    );      
	    $this->load->library('upload', $configuracao);
	    
	    if ($this->upload->do_upload('nfe')){

	    	$update = array( 	'id' 				=> 	$this->input->post('pedido_id'),
	    						'nr_nf' 			=>	$this->input->post('nr_nf'),
	    						'arquivo_nfe' 		=> 	$arquivo,
	    						'prazo_entrega'		=> 	$this->input->post('prazo_entrega'),
	    						'status_pedido_id'	=> 	5);
	    
	    	if( $this->pedidosM->atualizaPedidos($update) ){

	    		$email='<html>
					<head></head>
					<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
						<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Nota fiscal Vinculada ao Pedido</h1>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
							Nota Fiscal: '.$this->input->post('nr_nf').'<br/>
							cliente: 	'.$this->input->post('cliente').'<br/> 	
							Pedido: '.$this->input->post('pedido_id').'<br/>
							Prazo de entrega: '.$this->input->post('prazo_entrega').'<br/>
							</p>
						</div>
					</body>
				</html>';

	    		if( $this->enviaEmail('servicos@wertco.com.br','⛽ Nota Fiscal Vinculada ao Pedido:'.$this->input->post('pedido_id'), $email,'./nfe/'.$arquivo) ){
	    			$this->session->set_flashdata('sucesso', 'ok'); 
	    		}else{
	    			$this->session->set_flashdata('erro', 'erro');
	    		}
	    	}else{
	    		$this->session->set_flashdata('erro', 'erro');
	    	}
	    }

	    $this->pedidos();

	}

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('servicos@wertco.com.br')
			    ->reply_to('servicos@wertco.com.br')    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)		    
			    ->send();
		}else{
			$result = $this->email
			    ->from('servicos@wertco.com.br')
			    ->reply_to('servicos@wertco.com.br')    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)		    
			    ->send();
		}

		return $result;		
	}
		
	
	private function uploadMultiplo($arquivos,$local){
		
		for ($i=0; $i < count($arquivos['name']); $i++) {
			
			if(is_array($arquivos['name'])){
				
				if( count($arquivos['name']) == 1 && array_key_exists('0',$arquivos['name']) ){
					$_FILES['file']['name']     =  	$arquivos['name'][0];
	            	$_FILES['file']['type']     =  	$arquivos['type'][0];
	            	$_FILES['file']['tmp_name'] =   $arquivos['tmp_name'][0];
	            	$_FILES['file']['error']    =   $arquivos['error'][0];
	            	$_FILES['file']['size']     =   $arquivos['size'][0];
            	
				}else{
					
					$_FILES['file']['name']     = (count($arquivos['name']) > 1) ? $arquivos['name'][$i] : $arquivos['name'];
		            $_FILES['file']['type']     = (count($arquivos['name']) > 1) ? $arquivos['type'][$i] : $arquivos['type'];
		            $_FILES['file']['tmp_name'] = (count($arquivos['name']) > 1) ? $arquivos['tmp_name'][$i] : $arquivos['tmp_name'];
		            $_FILES['file']['error']    = (count($arquivos['name']) > 1) ? $arquivos['error'][$i] : $arquivos['error'];
		            $_FILES['file']['size']     = (count($arquivos['name']) > 1) ? $arquivos['size'][$i] : $arquivos['size'];		

		        }
			}else{
				$_FILES['file']['name']     =  	$arquivos['name'];
            	$_FILES['file']['type']     =  	$arquivos['type'];
            	$_FILES['file']['tmp_name'] =   $arquivos['tmp_name'];
            	$_FILES['file']['error']    =   $arquivos['error'];
            	$_FILES['file']['size']     =   $arquivos['size'];
			}
			
            
			$arquivo =	str_replace(' ','',trim(preg_replace( "[^a-zA-Z0-9_]", "", strtr($_FILES['file']['name'], "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"))));

			$configuracao = array(
		        'upload_path'   	=> 	$local,
		        'allowed_types' 	=> 	'gif|jpg|jpeg|png|pdf|doc|docx|xls|csv|vnd.ms-outlook|msg|zip|rar|txt|log|html|htm|mp4|mp3|xlsx|docx',
		        'file_name'     	=> 	$arquivo,
		        'encrypt_name'		=> 	false
		    );			

		    $this->load->library('upload');

		    $this->upload->initialize($configuracao);
		    
		    if (!$this->upload->do_upload('file')){
		    	return false;
			}else{
		    	$upload[] = array( 	'nome' => $this->upload->data('file_name') 	);
			}

		}
		

	    return $upload;
	}

	
	/************************************************************ 
	************** GERA PDF DOS ORÇAMENTOS FECHADOS *************
	*************************************************************/
	public function geraPdfOrcamentosFechados($mes)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-fechado-'.$mes.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
		
	    $data['orcamentos']	= $this->orcamentosM->buscaOrcamentosFechadosMes($mes);
	    $data['mes']	= 	$mes;
	    
	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-feira/orcamentos/orcamentos-fechados-pdf', $data, true));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded	    	
	    	redirect(base_url('pdf/orcamento-fechado-'.$mes.'.pdf'));
	    	
	    }
	    
    }


    /************************************************************ 
	*********** GERA PDF DOS ORÇAMENTOS POR STATUS **************
	*************************************************************/
	public function geraPdfOrcamentosStatus($status)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-status.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
		
	    $data['orcamentos']	= 	$this->orcamentosM->buscaOrcamentosPorStatus($status);	    
	    $data['status']		= 	$status;
	    
	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-feira/orcamentos/orcamentos-status-pdf', $data, true));

	    if($this->html2pdf->create('save')) {

	    	//PDF was successfully saved or downloaded	
	    	// Configuramos os headers que serão enviados para o browser
			header('Content-Description: File Transfer');
			header('Content-Disposition: attachment; filename="orcamento-status.pdf"');
			header('Content-Type: application/octet-stream');
			header('Content-Transfer-Encoding: binary');
			//header('Content-Length: ' . filesize(base_url('pdf/orcamento-status.pdf')));
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Expires: 0');
			// Envia o arquivo para o cliente			
			ob_clean();
            flush();
			readfile(base_url('pdf/orcamento-status.pdf'));    		    	
	    	
	    }
	    
    }

   
    public function ValidaCnpj(){
    	$cnpj 			= 	$this->input->post('cnpj');		
		$consulta_rf 	= 	$this->ConsultaCNPJRF($cnpj);

        if (isset($consulta_rf) && $consulta_rf->{'code'} == '1')
        {
            if ($consulta_rf->{'data'}->{'situacao_cadastral'} == 'ATIVA')
            {
            	$arr['retorno'] 	= 	$consulta_rf->{'data'};
            	$arr['save'] 		= 	$consulta_rf->{'Save_Receita'};
                $arr['result'] 		= 	true;
                $arr['situacao'] 	= 	$consulta_rf->{'data'}->{'situacao_cadastral'};
            	//echo json_encode($consulta_rf);            	
                
            	$arr['result'] = true;
				$arr['erro'] = 0;

            }else {
            	$arr['erro'] 	= 1;
            	$arr['msg'] 	= '* Situação cadastral '.$consulta_rf->{'data'}->{'situacao_cadastral'}.' na Receita Federal.'; 
            }
		}else {

            if (isset($consulta_rf)){
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 2 - '.$consulta_rf->{'code'}.')';
            }else{
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 3)';
            }

        }

		echo json_encode(array(	'receita' 	=> $arr ));
	}	

}