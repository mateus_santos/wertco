<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class AreaAdministradorComercial extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'administrador comercial' && $this->session->userdata('tipo_acesso') != 'administrador geral' && $this->session->userdata('tipo_acesso') != 'suporte' && $this->session->userdata('tipo_acesso') != 'feira') ){
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('ProdutosModel', 'produtosM');
		$this->load->model('OrcamentosModel', 'orcamentosM');
		$this->load->model('IcmsModel', 'icmsM');
		$this->load->model('prospeccaoStatusModel', 'prospeccaoStatusM');
		$this->load->model('EntregasModel', 'entregasM');
		$this->load->model('FormaPagtoModel', 'formaPagtoM');
		$this->load->model('AreaAtuacaoModel', 'areaAtuacaoM');
		$this->load->model('MotivoRetornoExpModel' , 'motivoExpM');
		$this->load->model('ProspeccoesModel' , 'prospeccaoM');
		$this->load->model('FretesModel', 'fretesM');
		$this->load->helper('form');

	}
 
	public function index()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();
		$parametros['title']	=	"Área do Administrador Comercial do sistema";
		$this->_load_view('area-administrador-comercial/index',$parametros);
	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha')));
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-administrador-comercial/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-administrador-comercial/editar-cliente',	$parametros );	
		}
	} 	
	
	public function orcamentos($pesquisa = null)
	{
		$parametros 					= 	$this->session->userdata();					
		$parametros['title']			=	"Orçamentos";
		$parametros['pesquisa']			= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$parametros['representantes']	=	$this->usuariosM->retornaRepresentantesAtivos($this->session->userdata('usuario_id'));
		$parametros['status']			=	$this->orcamentosM->selectStatus();
		$parametros['zona_atuacao'] 	= 	$this->session->userdata('usuario_id');
		$this->_load_view('area-administrador-comercial/orcamentos/orcamentos-com',$parametros);
	}

	public function orcamentosStatus($status_id, $pesquisa = null)
	{
		$parametros 					= 	$this->session->userdata();					
		$parametros['title']			=	"Orçamentos";
		$parametros['pesquisa']			= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$parametros['representantes']	=	$this->usuariosM->retornaRepresentantesAtivos($this->session->userdata('usuario_id'));		
		$parametros['zona_atuacao'] 	= 	$this->session->userdata('usuario_id');
		$parametros['status'] 			= 	$status_id;		
		$this->_load_view('area-administrador-comercial/orcamentos/orcamentos-com-status',$parametros);
	}

	public function buscaOrcamentoZona()
	{
		$retorno = $this->orcamentosM->buscaOrcamentoZona($this->input->post('orcamento_id'));
		echo json_encode($retorno);
	}

	public function retornaUsuarioLogado()
	{
		$retorno['usuario_id'] = $this->session->userdata('usuario_id');
		echo json_encode($retorno);
	}

	public function buscaOrcamentosSS(){
		$dados 	 				= 	$this->input->post();
		$dados['usuario_id'] 	= 	$this->session->userdata('usuario_id');
		$retorno 				=	$this->orcamentosM->buscaOrcamentos($dados);
		echo json_encode($retorno);
	}

	public function buscaOrcamentosStatusSS($status_id, $responsavel_id){
		$dados 	 				= 	$this->input->post();
		$dados['usuario_id'] 	=   $responsavel_id;
		$dados['status_id'] 	=   $status_id;
		$retorno 				=	$this->orcamentosM->buscaOrcamentosStatus($dados);
		echo json_encode($retorno);
	}

	public function orcamentosExpirados($pesquisa = null)
	{
		$parametros 			= 	$this->session->userdata();		
		$parametros['title']	=	"Orçamentos";
		$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$parametros['representantes']	=	$this->usuariosM->retornaRepresentantesAtivos($this->session->userdata('usuario_id'));
		$parametros['status']			=	$this->orcamentosM->selectStatus();
		$this->_load_view('area-administrador-comercial/orcamentos/orcamentos-expirados-com',$parametros);
	}
	
	public function buscaOrcamentosSSExpirados(){
		$dados 	 			 	= 	$this->input->post();
		$dados['usuario_id'] 	= 	$this->session->userdata('usuario_id');
		$retorno 				= 	$this->orcamentosM->buscaOrcamentosExpirados($dados);
		echo json_encode($retorno);
	}


	public function buscaProximosStatus()
	{
		$retorno = $this->orcamentosM->buscaProximosStatusOrcamento($_POST['orcamento_id']);
		echo json_encode($retorno);
		
	}

	public function buscaAndamentos()
	{
		$retorno = $this->orcamentosM->buscaAndamentos($_POST['orcamento_id']);			
		echo json_encode($retorno);

	}
	
	public function alteraStatusOrcamento()
	{

		if( isset($_POST['indicacao'])   ){			
			if($_POST['aprovar_indicacao'] == 0){
				$_POST['status_orcamento'] = 8;
			}
		}

		$dados = array('id'						=>	$_POST['orcamento_id'],
					   'status_orcamento_id'	=>	$_POST['status_orcamento']);

		if( isset($_POST['dt_previsao']) ){
			$dt_previsao = explode('/', $_POST['dt_previsao']);
			$dados['dt_previsao'] = $dt_previsao[2]."-".$dt_previsao[1]."-".$dt_previsao[0];
		}
		
		if($this->orcamentosM->atualizaStatusOrcamento($dados))
		{
			$this->log('Área Administrador | atualizar status orçamento','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
							
				switch($_POST['status_orcamento']){
					case 1:
				        $andamento = "Orçamento Aberto!";
				        break;
				    case 2:
				        $andamento = "Orçamento Fechado, venda realizada!";
				        break;
				    case 3:
				        $andamento = "Orçamento Perdido.";
				        break;
				    case 4:
				        $andamento = "Orçamento Cancelado.";
				        break;
				    case 5:
				        $andamento = "Orçamento entregue ao cliente.";
				        break;        
				    case 6:
				        $andamento = "Orçamento em negociação.";
				        break;
				     case 9:
				        $andamento = "Bombas em teste no cliente.";
				        break; 
				    case 10:
				        $andamento = "Orçamento Perdido para Wayne.";
				        break;
				    case 11:
				        $andamento = "Orçamento Perdido para Gilbarco.";
				        break;
				    case 12:
				        $andamento = "Aguardando Aprovação Gestor.";
				        break;
				    case 14:
				    	$andamento = "Orçamento Sem Retorno.";
				        break;
				    case 16:
				    	$andamento = "Orçamento em Reta Final de Fechamento.";
				        break;
				}
			
			
			$inserirAndamento = array(	'orcamento_id'			=>	$_POST['orcamento_id'],
										'status_orcamento_id'	=> 	$_POST['status_orcamento'],
										'andamento'				=>	$andamento,
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id'));
			
			if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
			{
				$this->enviaEmailStatusGestor($_POST['orcamento_id'], $_POST['status_orcamento']);
				echo json_encode(array('retorno' => 'sucesso'));	
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function insereAndamentoOrcamento(){
		
		$inserirAndamento = array(	'orcamento_id'		=>	$_POST['orcamento_id'],
									'status_orcamento_id'	=> 	$_POST['status_orcamento_id'],
									'andamento'				=>	$_POST['andamento'],
									'dthr_andamento'		=>	date('Y-m-d H:i:s'),
									'usuario_id'			=> 	$this->session->userdata('usuario_id'));

		if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
		{
			echo json_encode(array('retorno' => 'sucesso'));	
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}

	}

	public function retornaRepresentantes()
	{
		
		$rows = $this->usuariosM->retornaRepresentante($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label' 		=>	$row['label'],
								'id'		=>	$row['id'],
								'value'		=>	$row['label']	);
				
				
			}

			echo json_encode($dados);
		}
	}

	public function visualizarOrcamento($id) 
	{
		$parametros 				= 	$this->session->userdata();
		$parametros['dados']		=	$this->orcamentosM->getOrcamentoProdutos($id);
		$parametros['desconto']		=	$this->orcamentosM->getOrcamentoDesconto($id);
		$parametros['solicitante']	=	$this->orcamentosM->getSolicitante($id);
		$parametros['indicador']	= 	$this->orcamentosM->buscaIndicador($id);
		$parametros['bombas'] 		= 	$this->produtosM->select();
		//$parametros['prospeccaoStatus'] 		= 	$this->prospeccaoStatusM->selectprospeccaoStatus();
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['title']		=	"Orçamento #".$id;
		$parametros['zona_atuacao'] = 	$this->orcamentosM->buscaOrcamentoZona($id);
		$parametros['responsavel'] 	= 	$this->orcamentosM->getResponsavel($id); 
		$this->_load_view('area-administrador-comercial/orcamentos/visualiza-orcamento-com',$parametros);
	}

	public function emitirOrcamento()
	{
			
		$status_orcamento_id = $_POST['status_orcamento_id'];
	
		//altera o status para emitido	
		$dados = array('id'						=>	$_POST['orcamento_id'],
			   		   'status_orcamento_id'	=>	$status_orcamento_id);

		if($this->orcamentosM->atualizaStatusOrcamento($dados)){

			//orçamento entregue/emitido
			$andamentoOrcamento = array('andamento' 			=> 'Orçamento emitido ao cliente para o email '.$_POST['email_destino'].', por '.$this->session->userdata('nome').'',
		 								'dthr_andamento'		=> date('Y-m-d H:i:s'),
		 								'orcamento_id'			=> $_POST['orcamento_id'],
		 								'status_orcamento_id'	=> $status_orcamento_id,
		 								'usuario_id'			=> $this->session->userdata('usuario_id') );	
	 		if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento)){

				$retorno = $this->enviaEmailEmissaoCliente($_POST['email_destino'],$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');
				if($retorno){
					$this->enviaEmailEmissaoCliente('vendas@wertco.com.br',$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');

					echo json_encode(array('retorno' => 'sucesso'));
				}else{
					echo json_encode(array('retorno' => 'erro'));	
				}
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}	
		
	}

	private function enviaEmailEmissaoCliente( $email_destino, $orcamento_id, $valor_tributo, $valor_total, $contato_posto )
	{
		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);
		
		$anexo = $this->geraOrcamento($orcamento_id,'email',$contato_posto);

		$email = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title>tst</title><style type="text/css">
        /* Resets */
        .ReadMsgBody { width: 100%; background-color: #ebebeb;}
        .ExternalClass {width: 100%; background-color: #ebebeb;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        a[x-apple-data-detectors]{
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
        }        
        body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        body {margin:0; padding:0;}
        .yshortcuts a {border-bottom: none !important;}
        .rnb-del-min-width{ min-width: 0 !important; }

        /* Add new outlook css start */
        .templateContainer{
            max-width:590px !important;
            width:auto !important;
        }
        /* Add new outlook css end */

        /* Image width by default for 3 columns */
        img[class="rnb-col-3-img"] {
        max-width:170px;
        }

        /* Image width by default for 2 columns */
        img[class="rnb-col-2-img"] {
        max-width:264px;
        }

        /* Image width by default for 2 columns aside small size */
        img[class="rnb-col-2-img-side-xs"] {
        max-width:180px;
        }

        /* Image width by default for 2 columns aside big size */
        img[class="rnb-col-2-img-side-xl"] {
        max-width:350px;
        }

        /* Image width by default for 1 column */
        img[class="rnb-col-1-img"] {
        max-width:550px;
        }

        /* Image width by default for header */
        img[class="rnb-header-img"] {
        max-width:590px;
        }

        /* Ckeditor line-height spacing */
        .rnb-force-col p, ul, ol{margin:0px!important;}
        .rnb-del-min-width p, ul, ol{margin:0px!important;}

        /* tmpl-2 preview */
        .rnb-tmpl-width{ width:100%!important;}

        /* tmpl-11 preview */
        .rnb-social-width{padding-right:15px!important;}

        /* tmpl-11 preview */
        .rnb-social-align{float:right!important;}

        @media only screen and (min-width:590px){
        /* mac fix width */
        .templateContainer{width:590px !important;}
        }

        @media screen and (max-width: 360px){
        /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
        .rnb-yahoo-width{ width:360px !important;}
        }

        @media screen and (max-width: 380px){
        /* fix width and font size "tmpl-4 tmpl-6" in mobile preview */
        .element-img-text{ font-size:24px !important;}
        .element-img-text2{ width:230px !important;}
        .content-img-text-tmpl-6{ font-size:24px !important;}
        .content-img-text2-tmpl-6{ width:220px !important;}
        }

        @media screen and (max-width: 480px) {
        td[class="rnb-container-padding"] {
        padding-left: 10px !important;
        padding-right: 10px !important;
        }

        /* force container nav to (horizontal) blocks */
        td.rnb-force-nav {
        display: inherit;
        }
        }

        @media only screen and (max-width: 600px) {

        /* center the address &amp; social icons */
        .rnb-text-center {text-align:center !important;}

        /* force container columns to (horizontal) blocks */
        td.rnb-force-col {
        display: block;
        padding-right: 0 !important;
        padding-left: 0 !important;
        width:100%;
        }

        table.rnb-container {
         width: 100% !important;
        }

        table.rnb-btn-col-content {
        width: 100% !important;
        }
        table.rnb-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-last-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table[class~="rnb-col-2"] {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
        }

        table.rnb-col-2-noborder-onright {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        }

        table.rnb-col-2-noborder-onleft {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-top: 10px;
        padding-top: 10px;
        }

        table.rnb-last-col-2 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        table.rnb-col-1 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
        }

        img.rnb-col-3-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xs {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-2-img-side-xl {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-col-1-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        img.rnb-header-img {
        /**max-width:none !important;**/
        width:100% !important;
        margin:0 auto;
        }

        img.rnb-logo-img {
        /**max-width:none !important;**/
        width:100% !important;
        }

        td.rnb-mbl-float-none {
        float:inherit !important;
        }

        .img-block-center{text-align:center !important;}

        .logo-img-center
        {
            float:inherit !important;
        }

        /* tmpl-11 preview */
        .rnb-social-align{margin:0 auto !important; float:inherit !important;}

        /* tmpl-11 preview */
        .rnb-social-center{display:inline-block;}

        /* tmpl-11 preview */
        .social-text-spacing{margin-bottom:0px !important; padding-bottom:0px !important;}

        /* tmpl-11 preview */
        .social-text-spacing2{padding-top:15px !important;}

    }</style><!--[if gte mso 11]><style type="text/css">table{border-spacing: 0; }table td {border-collapse: separate;}</style><![endif]--><!--[if !mso]><!--><style type="text/css">table{border-spacing: 0;} table td {border-collapse: collapse;}</style> <!--<![endif]--><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body>

<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#f9fafc" style="background-color: rgb(249, 250, 252);">

    <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td></td><td></td></tr>
    <tr>
        <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="590" style="width:590px;">
                        <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
        <tbody><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_5" id="Layout_5">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:590;;max-width:600px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/backgroundCabecalho.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_6">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><div>&nbsp;</div>

<div>
<table style="color: rgb(0, 0, 0); font-family: sans-serif; font-style: normal; padding-top: 20px; font-size: 16px; margin-left: 15px; width: 100%;">
   <tbody>
                    <tr>
                        <td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
                        <td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
                        <td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
                        <td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
                    </tr>
                    <tr>                        
                        <td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
                        <td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
                        <td style="padding-bottom: 18px;font-weight: 700;">:</td>
                        <td style="padding-bottom: 18px;"></td>
                    </tr>
                    <tr>
                        <td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
                        <td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
                        <td style=" font-weight: 700; text-align: right;">Telefone:</td>
                        <td>'.$empresa['telefone'].'</td>
                    </tr>
                    <tr>
                        <td style=" font-weight: 700;">E-mail:</td>
                        <td colspan="3">'.$empresa['email'].'</td>
                    </tr>
                </tbody>
</table>
</div>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_7" id="Layout_7">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffcc00" align="center" cellspacing="0" style="background-color: rgb(255, 204, 0);">
                            <tbody><tr>
                                <td valign="top" align="left">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:0px; width:139;;max-width:139px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="139" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="http://www.wertco.com.br/email-cliente/produtos.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_8">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffcc00" style="background-color: rgb(255, 204, 0); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                        <tbody><tr>
                                            <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="rnb-container-padding" align="left">

                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                                    <tbody><tr>
                                                        <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                            <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                                <tbody><tr>
                                                                    <td style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><table cellspacing="0" style="width: 500px;margin-left: 14px">
    <thead style="height: 55px">
        <tr>
            <th style="height: 30px; text-align: center;">Modelo</th>
            <th style="text-align: center;">Descrição</th>
            <th>Qtd</th>
            <th style="text-align: right;">Valor</th>
        </tr>
    </thead>
    <tbody style="background-color: #fff;margin-left: 10px;font-size: 12px">';
       $total = 0; foreach($produtos as $produto){
            
            $email.='   <tr>
                        <td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
                        <td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
                        <td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>
                        <td style="   text-align: right; padding: 10px;">R$ '.number_format($produto['valor'], 2, ',', '.').'</td>
                    </tr>';                 
            $total = $total + ($produto['valor'] * $produto['qtd']);
        }   
        $email.='   <tr>
                        <td colspan="3" style="border-top: 1px solid #f0f0f0; border-right: 1px solid #f0f0f0;    padding: 10px;text-align: right;">
                        <b>Total (Sub-total +'.$valor_tributo.'% de ICMS + 5% de IPI)</b></td>
                        <td style="border-top: 1px solid #f0f0f0; text-align: right; padding: 10px;"><b>R$ '.number_format($total, 2, ',', '.').'</b></td>
                    </tr>';
        /*          <tr>
                        <td colspan="3" style=" border-top: 1px solid #f0f0f0;border-right: 1px solid #f0f0f0;    padding: 10px;text-align: right;"><b>Total (Sub-Total + '.$valor_tributo.'% de ICMS + 5% de IPI)</b></td>
                        <td style="border-top: 1px solid #f0f0f0; padding: 10px; text-align: right;"><b>R$ '.number_format($valor_total, 2, ',', '.').'</b></td>
                    </tr>';*/
        $email.='      
    </tbody>
</table>
</td>
                                                                </tr>
                                                                </tbody></table>

                                                            </td></tr>
                                                </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px;border-bottom:0px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->

            </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div>
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
            <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_10" id="Layout_10">
                <tbody><tr>
                    <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                        <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffcc00" align="center" cellspacing="0" style="background-color: rgb(255, 204, 0);">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <table cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td>
                                                <div style="display:block; border-radius:5px; width:542;;max-width:542px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                    <div><img border="0" hspace="0" vspace="0" width="542" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 5px; " src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png">
                                                        </div><div style="clear:both;"></div>
                                                    </div></td>
                                        </tr>
                                    </tbody></table>

                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr></tbody></table>
            <!--[if mso]>
                </td>
                <![endif]-->
                
                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <table class="rnb-del-min-width rnb-tmpl-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:590px;" name="Layout_4" id="Layout_4">
                <tbody><tr>
                    <td class="rnb-del-min-width" align="center" valign="top" style="min-width:590px;">
                        <table width="590" class="rnb-container rnb-yahoo-width" cellpadding="0" border="0" align="center" cellspacing="0" bgcolor="#f9fafc" style="padding-right: 20px; padding-left: 20px; background-color: rgb(249, 250, 252);">
                            <tbody><tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-size:14px; color:#888888; font-weight:normal; text-align:center; font-family:Arial,Helvetica,sans-serif;">
                                    <div>© 2018 wertco</div>
                                </td></tr>
                            <tr>
                                <td height="20" style="font-size:1px; line-height:0px;">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
            </td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                        </td>
        </tr>
        </tbody></table>

</body></html>';	
		
		$this->load->library('email');
		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to($this->session->userdata('email'))    // Optional, an account where a human being reads.
		    ->to($email_destino)
		    ->subject('Orçamento Wertco')
		    ->message($email)
		    ->attach('./pdf/'.$anexo)
		    ->send();

		return $result;		
	
	}

	public  function geraOrcamentoPdf()
	{
		$retorno = $this->geraOrcamento($this->input->post('orcamento_id'),'post',$this->input->post('contato_posto'));		
		redirect('./pdf/'.$retorno);
	}

	/************************************************************ 
	************** GERA PDF DO ORÇAMENTO SOLICITADO *************
	*************************************************************/
	public function geraOrcamento($orcamento_id, $origem_solicitacao,$contato)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-'.$orcamento_id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $dados				=	$this->orcamentosM->getOrcamentoProdutos($orcamento_id);
	    $total_descontos 	= 	$this->orcamentosM->getOrcamentoDesconto($orcamento_id);
	    $responsavel 		= 	$this->orcamentosM->getResponsavel($orcamento_id);
	    $orcamento 			= 	$this->orcamentosM->getFreteEntregaPagto($orcamento_id);
	    $indicador			= 	$this->orcamentosM->getIndicador($orcamento_id);
	    $data['frete']		= 	(is_object($orcamento)) ? $orcamento->frete : ' ';
	    $data['entrega']	= 	(is_object($orcamento)) ? $orcamento->entrega : ' ';
	    $data['formaPagto'] = 	(is_object($orcamento)) ? $orcamento->forma_pagto : ' ';
	    $data['emissao']	= 	$this->orcamentosM->retornaDiferencaEmissao($orcamento_id);
	    $data['primeira_emissao']	= 	$this->orcamentosM->primeiraEmissao($orcamento_id);
	    
	    $data['empresa'] = array(	'razao_social' 			=> 	$dados[0]->razao_social,
    								'fantasia' 				=> 	$dados[0]->fantasia,
									'cnpj' 					=> 	$dados[0]->cnpj,
									'telefone'				=> 	$dados[0]->telefone,
									'email'					=> 	$dados[0]->email,
									'endereco'				=> 	$dados[0]->endereco,
									'cidade'				=> 	$dados[0]->cidade,		
									'pais'					=> 	$dados[0]->pais,
									'estado'				=> 	$dados[0]->estado,
									'orcamento_id'			=> 	$dados[0]->orcamento_id,
									'emissao'				=> 	$dados[0]->emissao,
									'valor_tributo'			=>	$dados[0]->valor_tributo,
									'representante_legal' 	=> 	$dados[0]->representante_legal,
									'inscricao_estadual' 	=> 	$dados[0]->insc_estadual,
									'bairro'				=> 	$dados[0]->bairro,
									'cep'					=> 	$dados[0]->cep,
									'observacao'			=> 	$dados[0]->observacao,									
									'tp_cadastro'			=> 	$dados[0]->tp_cadastro,
									'celular'				=> 	$dados[0]->celular,
									'fl_especial' 			=> 	$dados[0]->fl_especial,
									'dt_previsao'			=> 	$dados[0]->dt_previsao,
									'garantia'				=> 	$dados[0]->garantia );

	    foreach ($dados as $dado) {
	    	$data['produtos'][] = array('codigo' 			=> 	$dado->codigo,
	    								'descricao'			=>	$dado->descricao,
										'modelo'			=>	$dado->modelo,
	    								'qtd'				=>	$dado->qtd,
	    								'valor_unitario' 	=>	$dado->valor_unitario,
	    								'valor_orcamento'	=> 	$dado->valor_orcamento,
	    								'valor_produto' 	=> 	$dado->valor);
	    }		

	    $data['info']  	= 	array(	'nome' 		=> 	$this->session->userdata('nome'),
	    						 	'email'		=> 	$this->session->userdata('email'),
	    						 	'telefone'	=>	$this->session->userdata('telefone'),
									'contato'	=> 	( isset($_POST['contato'])) ? $_POST['contato'] : $contato );

	    $data['representante'] 	= 	array(	'responsavel' 	=>  $responsavel->responsavel,
								  			'empresa' 		=>	$responsavel->razao_social 	);

	    foreach( $total_descontos as $total_desconto ){
	    	$data['desconto'] = array('valor_desconto' => $total_desconto['valor_desconto'] );
	    }

	    if(!isset($data['desconto'])){
	    	$data['desconto'] = array('valor_desconto' => '' );
	    }

	    $data['indicador'] = array('indicador' 	=> 	(count($indicador) > 0) ? $indicador[0]->indicador : '');	    

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-administrador-comercial/orcamentos/orcamento-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'orcamento-'.$orcamento_id.'.pdf'));
	    	}else{
	    		return $retorno['retorno'] = 'orcamento-'.$orcamento_id.'.pdf';
	    	}
	    }	    
	    
    }

    public function cadastraOrcamentos($pesquisa = null)
	{
		$parametros 				= 	$this->session->userdata();
		//$parametros['dados']		=	$this->orcamentosM->buscaOrcamentosIndicadores($parametros['usuario_id']);		
		$parametros['bombas'] 		= 	$this->produtosM->select();		
		//$parametros['prospeccaoStatus']	= 	$this->prospeccaoStatusM->selectprospeccaoStatus();
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['title']		=	"Orçamentos";
		$parametros['pesquisa']		= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-administrador-comercial/orcamentos/cadastra-orcamento-com',$parametros);	
	}

	public function orcamentosRep()
	{
		if( $this->input->post() )
		{
			
			if( $this->input->post('salvar') == '1')
			{	
				$dadosEmpresa = array(
			        'razao_social' 			=>	$this->input->post('razao_social'),
					'fantasia' 				=>	$this->input->post('fantasia'),
					'cnpj' 					=>	$this->input->post('cnpj'),
					'telefone' 				=>	$this->input->post('telefone'),
					'endereco' 				=>	$this->input->post('endereco'),
					'email' 				=>	$this->input->post('email'),				
					'cidade' 				=>	$this->input->post('cidade'),
					'estado'				=>	$this->input->post('estado'),
					'pais'					=>	$this->input->post('pais'),	
					'bairro'				=>	$this->input->post('bairro'),
					'cep'					=>	$this->input->post('cep'),
					'insc_estadual' 		=>	$this->input->post('insc_estadual'),
					'tipo_cadastro_id'		=>	1,
					'cartao_cnpj'			=>	$this->input->post('cartao_cnpj'),
					'codigo_ibge_cidade'	=> 	$this->input->post('codigo_ibge_cidade')

				);

				if($this->empresasM->add($dadosEmpresa))
				{
					$empresa_id = $this->db->insert_id();			

				}
	 		}else{
	 			$empresa_id = $this->input->post('empresa_id');
	 		}

	 		$dt_previsao = explode('/',$this->input->post('dt_previsao'));
	 		$dt_previsao_prod = explode('/',$this->input->post('dt_previsao_prod'));

	 		//$entrega_id = $this->input->post('entrega');
	 		$entrega_id = 17;
	 		$dadosOrcamento = array('empresa_id' 			=> 	$empresa_id,
	 								'status_orcamento_id'	=> 	1,
	 								'origem'				=> 	'Administrador Comercial',
	 								'solicitante_id'		=> 	$this->session->userdata('usuario_id'),
	 								'usuario_id'			=> 	$this->session->userdata('usuario_id'),
	 								'indicador_id'			=> 	$this->input->post('indicador_id'),
	 								'frete_id'				=> 	$this->input->post('frete'),
	 								'entrega_id'			=> 	$entrega_id,
	 								'forma_pagto_id'		=> 	$this->input->post('forma_pagto'),
									'contato_posto'			=> 	$this->input->post('contato_posto'),
									'fl_especial'			=> 	( $this->input->post('valor_desconto_orc') == 'outro' ) ? '1' : '0',
	 								'dt_previsao'			=> 	$dt_previsao[2].'-'.$dt_previsao[1].'-'.$dt_previsao[0],
	 								'dt_previsao_prod'		=> 	$dt_previsao_prod[2].'-'.$dt_previsao_prod[1].'-'.$dt_previsao_prod[0],
	 								'garantia'				=> 	$this->input->post('garantia') );

	 		if( $this->orcamentosM->insereOrcamento($dadosOrcamento) )
	 		{
	 			$orcamento_id  = $this->db->insert_id();
	 			$erro = 0;

	 			$dadosContato = array(	'empresa_id' 		=> 	$empresa_id,
	 									'nome'				=> 	$this->input->post('contato_posto'),
	 									'email'				=> 	$this->input->post('email_contato_posto'),
	 									'celular'			=> 	$this->input->post('tel_contato_posto'),
	 									'tipo_cadastro_id'	=> 1);

	 			if( $this->usuariosM->add($dadosContato) )
	 			{

	 				$contato_id = $this->db->insert_id();
	 				$updateContato = array(	'id' 			=> 	$orcamento_id,
	 										'contato_id'	=> 	$contato_id	);

	 				$this->orcamentosM->atualizaOrcamento($updateContato);

		 			$andamentoOrcamento = array('andamento' 			=> 'Orçamento realizado por '.$this->session->userdata('nome').'. Equipe Wertco!',
			 									'dthr_andamento'		=> date('Y-m-d H:i:s'),
			 									'orcamento_id'			=> $orcamento_id,
			 									'status_orcamento_id'	=> 1,
			 									'usuario_id'			=> $this->session->userdata('usuario_id')
			 									);

		 			$motivo 		= 	'Tabela de comissão';
		 			$comissao_val 	= 	$this->input->post('valor_desconto_orc');
		 			
		 			if( $this->input->post('valor_desconto_orc') == 'outro' ){
	 					
	 					$comissao_val = 0.01;
	 					$motivo = 'Outro';
	 					
		 			}elseif( $this->input->post('valor_desconto_orc') == 'TB2' ){
		 				
		 				$comissao_val = 0.02;
		 				$motivo = 'Tabela 2% TB2';

		 			}elseif( $this->input->post('valor_desconto_orc') == 'TN' ){
		 				
		 				$comissao_val = 6.00;
		 				$motivo = 'Tabela TN Especial de 5%';

		 			}

		 			$comissao  = array(		'orcamento_id' 		=> 	$orcamento_id,
		 									'valor_desconto' 	=> 	$comissao_val,
		 									'motivo_desconto'	=> 	$motivo );

		 			if(	$this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento) && $this->orcamentosM->insereOrcamentosDesconto($comissao) )
		 			{
		 				$erro = $erro;
		 				$orcamentoResp = array(	'orcamento_id' 	=> 	$orcamento_id,
			 									'usuario_id' 	=> 	$this->session->userdata('usuario_id') );
	 					if($this->orcamentosM->insereResponsavelOrcamento($orcamentoResp))
	 					{
	 						$erro = $erro;
	 					
				 			for ($i=0; $i < count($this->input->post('produto')) ; $i++) 	 			  
				 			{ 

				 				$valor 	=	$this->input->post('valor')[$i];
		 						if( $this->input->post('valor_desconto_orc') == 'outro' ){
		 							$valor = str_replace(",", ".", str_replace(".","", $this->input->post('valor')[$i] ));
		 						}

				 				$orcamentoProdutos = array(	'orcamento_id' 	=>	$orcamento_id,
				 											'produto_id'  	=> 	$this->input->post('produto')[$i],
				 											'qtd'			=>	$this->input->post('quantidade')[$i],
				 											'valor'			=>  $valor 	);	

				 				if($this->orcamentosM->insereOrcamentoProdutos($orcamentoProdutos))
				 				{
				 					$erro = $erro;
				 					
								}else{
									$erro++;
								}		 					 			
				 			}
				 		}else{
							$erro++;
						}	 					

					}else{
						$erro++;
					}
				}else{
					$erro++;
				}
			}else{
				$erro++;
			}

	 		if( $erro == 0 ){
	 				
	 			if($this->enviarEmailOrcamento($orcamento_id))
 				{ 					
 					if($this->input->post('valor_desconto_orc') == 'outro'){
 						$this->enviarEmailConfirmacaoPreco($orcamento_id);
					}

 					$this->session->set_flashdata('sucesso', 'ok.');
 				}else{
 				 	$this->session->set_flashdata('erro', 'erro.');	
 				}
 			}else{
 				$this->session->set_flashdata('erro', 'erro.');
 			}

			$this->orcamentosStatus(1);
		}

	}

	public function excluirOrcamento()
	{
		if( $this->input->post('enviar_exclusao') == '1' ){
			$insertMotivo = array(	'usuario_id' 	=> $this->session->userdata('usuario_id'),
									'tabela'		=> 	'orcamento',
									'motivo'		=> $this->input->post('motivo_exclusao'),
									'id_exclusao'	=> $this->input->post('orcamento_id') 	);

			if($this->motivoM->add($insertMotivo)){

				if( $this->orcamentosM->excluirOrcamento($this->input->post('orcamento_id')) ){
					
					$this->session->set_flashdata('exclusao', 'sucesso'); 	
				}else{

					$this->session->set_flashdata('erro', 'erro'); 	
				}
				
			}else{
				$this->session->set_flashdata('erro', 'erro'); 
			}

		}

		$this->orcamentosStatus(1);
	}

	public function alterarDtPrevisao(){
		$data = explode('/',$_POST['dt_previsao']);
		$dadosUpdate = array(	'id' 			=> 	$_POST['id'],
							 	'dt_previsao'	=>	$data[2].'-'.$data[1].'-'.$data[0]	);

		if($this->orcamentosM->atualizaOrcamento($dadosUpdate)){
			$this->log('Área Administrador | atualiza dt_previsao','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);			
			echo json_encode(array('retorno' => 'sucesso' ));						 	
			
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}
		
	}

	public function alterarDtPrevisaoProd(){
		$data = explode('/',$_POST['dt_previsao_prod']);
		$dadosUpdate = array(	'id' 			=> 	$_POST['id'],
							 	'dt_previsao_prod'	=>	$data[2].'-'.$data[1].'-'.$data[0]	);

		if($this->orcamentosM->atualizaOrcamento($dadosUpdate)){
			$this->log('Área Administrador | atualiza dt_previsao_prod','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);			
			echo json_encode(array('retorno' => 'sucesso' ));						 	
			
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}
		
	}
	
	public function insereDescontoOrcamento()
	{	
		if($_POST['valor_desconto'] == 'outro'){
			$_POST['valor_desconto'] = '0,01';
		}

		$dados_desconto = array(	'orcamento_id' 		=> 	$_POST['orcamento_id'],
									'valor_desconto'	=>	str_replace(',', '.', $_POST['valor_desconto']) 	);

		if( $this->orcamentosM->insereOrcamentosDesconto( $dados_desconto ) )
		{
			$atualiza = array(	'id'			=> 	$_POST['orcamento_id'],
								'fl_especial'	=> 	1	);

			if( $this->orcamentosM->atualizaOrcamento($atualiza) ){
				echo json_encode(array('retorno' => 'sucesso'));				
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}

	public function alterarValorProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['id'],
							 	'valor'	=>	(strpos($_POST['valor'],',') === false ) ? $_POST['valor'] : str_replace(',','.', str_replace('.','',$_POST['valor'])) );

		if($this->orcamentosM->atualizaValorProduto($dadosUpdate)){
			
			if( isset($_POST['comissao']) ){

				$motivo 		= 	'Tabela de comissão';
	 			$comissao_val 	= 	$_POST['comissao'];
	 				
	 			if( $_POST['comissao'] == 'outro' ){
					//$this->enviarEmailConfirmacaoPreco( $_POST['orcamento_id']);	
					$comissao_val = 0.01;
					$motivo = 'Outro';
						
	 			}elseif( $_POST['comissao'] == 'TB2' ){
	 					
	 				$comissao_val = 0.02;
	 				$motivo = 'Tabela 2% TB2';

				}elseif( $_POST['comissao'] == 'TN' ){
	 					
	 				$comissao_val = 6.00;
	 				$motivo = 'Tabela TN Especial de 5%';
	 			}

				$comissao = array('orcamento_id' 	=> $_POST['orcamento_id'],
								  'valor_desconto'	=> $comissao_val);

				$this->orcamentosM->atualizaComissao($comissao);

			}

			echo json_encode(array('retorno' => 'sucesso' ));						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}

	}

	public function alterarProdutoOrcamento(){

		$dadosUpdate = array(	'id' 				=> 	$_POST['orcamento_produto_id'],
							 	'produto_id'		=>	$_POST['produto_id'],
							 	'valor' 			=>	$_POST['valor_produto']	);

		if($this->orcamentosM->alterarProdutoOrcamentoInd3($dadosUpdate)){
			
			echo json_encode(array('retorno' => 'sucesso' ));						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarQtdProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['orcamento_produto_id'],
							 	'qtd'	=>	$_POST['qtd']	);

		if($this->orcamentosM->alterarQtdProduto($dadosUpdate)){
			
			echo json_encode(array('retorno' => 'sucesso' ));						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	private function enviarEmailOrcamento($orcamento_id){

		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);		
		$email = '<html>
		<head></head>
		<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
		<div class="content" style="width: 600px; height: 100%;">
		<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/backgroundCabecalho.png); height: 528px; "></div>
		<div style="width: 600px; background-color: #ffcc00; height: 100%">
			<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
				<tbody>
					<tr>
						<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
						<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
						<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
						<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
					</tr>
					<tr>						
						<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
						<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
						<td style="padding-bottom: 18px;font-weight: 700;"></td>
						<td style="padding-bottom: 18px;"></td>
					</tr>
					<tr>
						<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
						<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
						<td style=" font-weight: 700; text-align: right;">Telefone:</td>
						<td>'.$empresa['telefone'].'</td>
					</tr>
					<tr>
						<td style=" font-weight: 700;">E-mail:</td>
						<td colspan="3">'.$empresa['email'].'</td>
					</tr>
				</tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
			<table style="width: 570px;margin-left: 14px;" cellspacing="0">
				<thead style="height: 55px;">
					<tr><th style="    height: 30px;">Modelo</th>
					<th>Descrição</th>
					<th>Quantidade</th>					
				</tr></thead>
				<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
				$total = 0; foreach($produtos as $produto){
					
					$email.='	<tr>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
								<td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>						
							</tr>';					
					$total = $total + ($produto['valor'] * $produto['qtd']);
				}			
		
			$email.=' </tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
		</div>
	</div>
</body>
</html>';	

		$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('webmaster@wertco.com.br')    // Optional, an account where a human being reads.
		    ->to('gestor@wertco.com.br')
		    ->subject('Orçamento realizado no Sistema pela Equipe Wertco')
		    ->message($email)
		    ->send();

		return $result;		
	}

	private function enviarEmailConfirmacaoPreco($orcamento_id){

		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);		
		$email = '<html>
		<head></head>
		<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
		<div class="content" style="width: 600px; height: 100%;">
		<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/cabecalhoAprovacaoOrcamento.png); height: 528px; "></div>
		<div style="width: 600px; background-color: #ffcc00; height: 100%">
			<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
				<tbody>
					<tr>
						<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
						<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
						<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
						<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
					</tr>
					<tr>						
						<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
						<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
						<td style="padding-bottom: 18px;font-weight: 700;"></td>
						<td style="padding-bottom: 18px;"></td>
					</tr>
					<tr>
						<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
						<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
						<td style=" font-weight: 700; text-align: right;">Telefone:</td>
						<td>'.$empresa['telefone'].'</td>
					</tr>
					<tr>
						<td style=" font-weight: 700;">E-mail:</td>
						<td colspan="3">'.$empresa['email'].'</td>
					</tr>
				</tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
			<table style="width: 570px;margin-left: 14px;" cellspacing="0">
				<thead style="height: 55px;">
					<tr><th style="    height: 30px;">Modelo</th>
					<th>Descrição</th>
					<th>Quantidade</th>					
					<th>Valor</th>					
				</tr></thead>
				<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
				$total = 0; foreach($produtos as $produto)	{
					
					$email.='	<tr>
								<td style="border-right: 1px solid #f0f0f0; padding: 10px;">'. $produto['modelo'] .'</td>
								<td style="border-right: 1px solid #f0f0f0; padding: 10px;">'. $produto['descricao'] .'</td>
								<td style="text-align: center;border-right: 1px solid #f0f0f0; padding: 10px;">'.$produto['qtd'].'</td>						
								<td style="text-align: center;border-right: 1px solid #f0f0f0; padding: 10px;">'.number_format($produto['valor'],2,',','.').'</td>
							</tr>';					
					$total = $total + ($produto['valor'] * $produto['qtd']);
				}			
		
			$email.=' </tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
			<table style="width: 570px;margin-left: 14px;text-align: center;margin-bottom: 50px;" cellspacing="0">
				<tr>
					<td>
						<a href="https://www.wertco.com.br/Home/confirmaOrcamentoGestor/'.$orcamento_id.'" style="font-size: 20px;font-weight: bold;color: #000;border: 1px solid #000;text-align: center;">Aprovar Orçamento</a>
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>';	
		$result = $this->enviaEmail('gestor@wertco.com.br','Orçamento necessita aprovação no Sistema',$email,$anexo=null);
		if( $result ){
			$this->enviaEmail('administrativo@companytec.com.br','Orçamento necessita aprovação no Sistema',$email,$anexo=null);
			$this->enviaEmail('adm@companytec.com.br','Orçamento necessita aprovação no Sistema',$email,$anexo=null);
		}

		/*$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to('gestor@wertco.com.br')
		    ->subject('Orçamento necessita aprovação no Sistema')
		    ->message($email)
		    ->send();
		*/
		
		return $result;		
	}

   	public function verificaEstadoIcms()
	{
		$dados = $this->icmsM->getIcmsPorEstado($_POST['estado_id']);
		if( $dados[0]['total'] > 0 ){
			echo json_encode(array('retorno' => 'erro'));
		}else{
			echo json_encode(array('retorno' => 'sucesso'));
		}

	}

	public function insereProdutosNovosOrcamento(){
		$erro = 0;
		
		foreach ($_POST['dados'] as $dados) {
			if( $dados['name'] == 'produto_id'){
				$insert['produto_id'] = $dados['value'];
				$i=1;
			}
			if( $dados['name'] == 'qtd'){
				$insert['qtd'] = $dados['value'];
				$i=2;
			}
			if( $dados['name'] == 'valor_unitarios'){
				$insert['valor'] = $dados['value'];
				$i=3;
				$insert['orcamento_id'] = $_POST['orcamento_id'];
				if($this->orcamentosM->insereOrcamentoProdutos($insert)){
					$erro = $erro;
				}else{
					$erro++;
				}
			}			
		}

		if($erro == 0){
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Produto Adicionado ao orçamento por '.$this->session->userdata('nome'),
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
			
			$this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Icms ******************************
	*****************************************************************************/
	public function excluirProdutoOrcamento()
	{
		if($this->orcamentosM->excluirProduto($_POST['orcamento_produto_id'])){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function alterarFormaPagto()
	{
		$dadosUpdate = array(	'id' 				=> 	$_POST['orcamento_id'],
							 	'forma_pagto_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarFormaPagto($dadosUpdate)){
			
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de Pagamento alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarEntrega(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['orcamento_id'],
							 	'entrega_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarEntrega($dadosUpdate)){
			
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de entrega alterada.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarFrete(){
		
		$dadosUpdate = array(	'id' 		=> 	$_POST['orcamento_id'],
							 	'frete_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarFrete($dadosUpdate)){
			
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Frete alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarGarantia(){
		$dadosUpdate = array(	'id' 		=> 	$_POST['orcamento_id'],
							 	'garantia'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarObservacao($dadosUpdate)){
			$this->log('Área Administrador | atualiza garantia','orcamentos','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Garantia alterada: '.$_POST['valor'].' meses',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function verificaIcms()
	{
		if( $_POST['estado'] != 'EX' && $_POST['estado'] != '' ){
	
			$dados = $this->icmsM->getIcmsPorUF($_POST['estado']);


			echo json_encode(	array(	'fator' 		=> $dados[0]->fator,
										'valor_tributo' => $dados[0]->valor_tributo ) 	);
		}else{

			echo json_encode(	array(	'fator' 		=> '1.2589928057554',
										'valor_tributo' => '7.00' ) 	);
		}

	}

	public function retornaIndicadores()
	{
		
		$rows = $this->usuariosM->retornaIndicadores($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label' 	=>	$row['label'],
								 'id'		=>	$row['id'],
								 'value'	=>	$row['label'] );
				
			}

			echo json_encode($dados);
		}
	}

	public function ValidaCnpj(){

    	$cnpj 			= 	$this->input->post('cnpj');		
		$consulta_rf 	= 	$this->ConsultaCNPJRF($cnpj);

        if (isset($consulta_rf) && $consulta_rf->{'code'} == '1')
        {
            if ($consulta_rf->{'data'}->{'situacao_cadastral'} == 'ATIVA')
            {
            	$arr['retorno'] 	= 	$consulta_rf->{'data'};
            	$arr['save'] 		= 	$consulta_rf->{'Save_Receita'};
                $arr['result'] 		= 	true;
                $arr['situacao'] 	= 	$consulta_rf->{'data'}->{'situacao_cadastral'};
            	//echo json_encode($consulta_rf);            	
                
            	$arr['result'] 	= 	true;
				$arr['erro'] 	= 	0;

            }else {
            	$arr['erro'] 	= 	1;
            	$arr['msg'] 	= 	'* Situação cadastral '.$consulta_rf->{'data'}->{'situacao_cadastral'}.' na Receita Federal.'; 
            }
		}else {

            if (isset($consulta_rf)){
            	$arr['erro'] 	= 	1;
            	$arr['msg'] 	= 	'* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 2 - '.$consulta_rf->{'code'}.')';
            }else{
            	$arr['erro'] 	= 	1;
            	$arr['msg'] 	= 	'* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 3)';
            }

        }

		echo json_encode(array(	'receita' 	=> $arr));
	}

	public function verificaEmissao(){

		$resultado = $this->orcamentosM->retornaDiferencaEmissao($this->input->post('orcamento_id'));
		echo json_encode($resultado);
			
	}

	public function insereMotivoReemissao(){
		$insereMotivo = array(	'orcamento_id'		=> 	$this->input->post('orcamento_id'),
								'dthr_emissao'		=> 	date('Y-m-d H:i:s'),
								'usuario_emissao'	=> 	$this->session->userdata('usuario_id'),
								'motivo_emissao'	=> 	$this->input->post('motivo_emissao') );

		if( $this->orcamentosM->insereMotivoReemissao($insereMotivo) ){

			$inserirAndamento = array(	'orcamento_id'			=>	$this->input->post('orcamento_id'),
										'status_orcamento_id'	=> 	$this->input->post('status_orcamento_id'),
										'andamento'				=>	'Orçamento reemitido. Motivo da reemissão: '.$this->input->post('motivo_emissao'),
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id')	);

			if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
			{
			
				echo json_encode(array('retorno' => 'sucesso'));	
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}	
		}  
	}

	public function insereMotivoRetornoExp(){

    	$dadosInsert = array(	'orcamento_id' 	=> 	$this->input->post('orcamento_id'),
    							'motivo'		=> 	$this->input->post('motivo'),
    							'usuario_id'	=> 	$this->session->userdata('usuario_id')	);

    	if($this->motivoExpM->add($dadosInsert)){

  			$inserirAndamento = array(	'orcamento_id'			=>	$this->input->post('orcamento_id'),
										'status_orcamento_id' 	=> 	13,
										'andamento'				=>	'Atualização de status do orçamento expirado. Motivo: '.$this->input->post('motivo'),
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id')	);

			if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento)){
				$cliente = $this->orcamentosM->buscaOrcamentoById($this->input->post('orcamento_id'));
				$params = array(	'cliente' 		=> 	$cliente['cliente'],
									'orcamento_id'	=> 	$this->input->post('orcamento_id'),
									'motivo'		=> 	$this->input->post('motivo'),
									'usuario'		=> 	$this->session->userdata('nome'),
									'dthr_motivo'	=> 	date('d/m/Y H:i:s') 	);

    			$email 	 = $this->load->view('area-administrador-comercial/orcamentos/email-motivo-retorno-expiracao', $params, true);
    			$this->load->library('email');

				$result = $this->email
				    ->from('webmaster@wertco.com.br')
				    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
				    ->to('gestor@wertco.com.br')
				    ->subject('Orçamento expirado atualizado no Sistema pelo usuario:'.$this->session->userdata('nome')	)
				    ->message($email)
				    ->send();
				if($result){ 
    				$retorno = array('retorno' => 'sucesso');    	
				}else{
					$retorno = array('retorno' => 'erro');    	
				}
    		}
    	}else{
    		$retorno = array('retorno' => 'erro');
    	
    	}

    	echo json_encode($retorno);

    }

    public function prospeccoes()
	{
		$parametros 			=	$this->session->userdata();
		$parametros['dados']	=	$this->prospeccaoM->buscaProspecPorUsuario($this->session->userdata('usuario_id'));
		$parametros['title']	=	"Prospecções";
		$this->_load_view('area-administrador-comercial/prospeccoes/prospeccoes',$parametros);
	}

	public function cadastrarProspeccao()
	{
		$parametros 			=	$this->session->userdata();
		$parametros['status']	=	$this->prospeccaoM->buscaStatusProspeccao();
		$parametros['title']	=	"Cadastrar Prospecções";
		$this->_load_view('area-administrador-comercial/prospeccoes/cadastrar-prospeccao',$parametros);
	}

	public function insereProspeccao()
	{
		// Se for atendimento, salva com os dados da Wertco
		if( $this->input->post('tp_atendimento') == 'a' ){

			$insere = array( 'empresa_id' => 2,
							 'usuario_id' => $this->session->userdata('usuario_id'),
							 'status_id'  => 2 );
		}else{

			if( $this->input->post('empresa_id') != '' ){
				$empresa_id = $this->input->post('empresa_id');

			}else{

				$dadosEmpresa = array(
			        'razao_social' 			=>	$this->input->post('razao_social'),
					'fantasia' 				=>	$this->input->post('fantasia'),
					'cnpj' 					=>	$this->input->post('cnpj'),
					'telefone' 				=>	$this->input->post('telefone'),
					'endereco' 				=>	$this->input->post('endereco'),
					'email' 				=>	$this->input->post('email'),				
					'cidade' 				=>	$this->input->post('cidade'),
					'estado'				=>	$this->input->post('estado'),
					'pais'					=>	$this->input->post('pais'),	
					'bairro'				=>	$this->input->post('bairro'),
					'cep'					=>	$this->input->post('cep'),
					'insc_estadual' 		=>	$this->input->post('insc_estadual'),
					'tipo_cadastro_id'		=>	1,
					'cartao_cnpj'			=>	$this->input->post('cartao_cnpj'),
					'codigo_ibge_cidade'	=> 	$this->input->post('codigo_ibge_cidade')
				);

				if($this->empresasM->add($dadosEmpresa))
				{
					$empresa_id = $this->db->insert_id();			
				}
			}
			
			$insere = array( 'empresa_id' => $empresa_id,
							 'usuario_id' => $this->session->userdata('usuario_id'),
							 'status_id'  => 1 );
			
		}

		if( $this->prospeccaoM->insert($insere) ){
			$prospeccao_id = $this->db->insert_id();	
			redirect('AreaAdministradorComercial/visualizarProspeccao/'.$prospeccao_id);
		}	

	}

	public function visualizarProspeccao($prospeccao_id)
	{
		$parametros 				=	$this->session->userdata();
		$parametros['status']		=	$this->prospeccaoM->buscaStatusProspeccao();
		$parametros['dados']		=	$this->prospeccaoM->buscaProspeccao($prospeccao_id);
		$parametros['atividades']	=	$this->prospeccaoM->buscaAtividades($prospeccao_id);		
		$parametros['title']		=	"Visualizar Prospecções";
		$this->_load_view('area-administrador-comercial/prospeccoes/visualizar-prospeccao',$parametros);
	}

	public function inserirAtividade()
	{
		$insert = $this->input->post();

		if( $this->prospeccaoM->insertAtividade($insert) ){
			
			$dados	=	$this->prospeccaoM->buscaProspeccao($this->input->post('prospeccao_id'));
			
			$titulo = "Prospecção atualizada. ".$dados['cliente'];
			$email='<html>
					<head></head>
					<body style="width: 100%; height: 100%; position:absolute;">
						<div style="width: 80%;padding-top: 10px;position: relative;text-align: left;margin-left: 40px;">
							<img src="https://wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" width="200" />
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Prospecção '.$dados['cliente'].'</h1>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
								Nome do Contato:  '.$this->input->post('nome').'<br/>
								Telefone: '.$this->input->post('telefone').'<br/>
								Celular:  '.$this->input->post('celular').'<br/>
								Cargo:  '.$this->input->post('cargo').'<br/>
							</p>
							<hr/>
							<p>Descrição da atividade: <b>'.$this->input->post('descricao').'</b>
						</div>
					</body>
				</html>';
			if($this->enviaEmail('gestor@wertco.com.br',$titulo,$email )){
				$this->session->set_flashdata('retorno', 'sucesso');
				redirect('AreaAdministradorComercial/visualizarProspeccao/'.$this->input->post('prospeccao_id'));	
			}else{
				$this->session->set_flashdata('retorno', 'erro');
				redirect('AreaAdministradorComercial/visualizarProspeccao/'.$this->input->post('prospeccao_id'));
			} 
		}
	}

	public function buscaAtividade()
	{
		echo json_encode($this->prospeccaoM->buscaAtividade($this->input->post('atividade_id')));
	}

	public function editarAtividade()
	{
		$update = $this->input->post();
		if( $this->prospeccaoM->atualizarAtividade($update) ){
			$this->session->set_flashdata('retorno', 'sucesso');
			redirect('AreaAdministradorComercial/visualizarProspeccao/'.$this->input->post('prospeccao_id'));
		}else{
			$this->session->set_flashdata('retorno', 'sucesso');
			redirect('AreaAdministradorComercial/visualizarProspeccao/'.$this->input->post('prospeccao_id'));
		}
	}

	public function prospeccoesStatus()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->prospeccaoM->buscaProspeccaoStatus();
		$parametros['title']	=	"Status das Prospecções";
		$this->_load_view('area-administrador-comercial/prospeccoes/prospeccao-status',$parametros);
	}

	public function cadastraProspeccaoStatus()
	{
		
		if($this->input->post('salvar') == 1){
			
			$dados  = array('descricao' 		=> 	$this->input->post('descricao'));

			if( $this->prospeccaoM->insertStatus($dados) ){
				$this->log('Área Administrador | insere status prospecção','prospeccaoStatus','INSERÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');

			}else{
				$this->session->set_flashdata('erro', 'erro');					
			}    

			redirect('AreaAdministradorComercial/prospeccoesStatus');

		}else{

			$parametros 			= 	$this->session->userdata();					
			$parametros['title']	=	"Cadastro de Status de Prospecções";
			$this->_load_view('area-administrador-comercial/prospeccoes/cadastrar-prospeccao-status',$parametros);
		}
	
	}
	
	/****************************************************************************
	************* Método Responsável por editar os prospeccaoStatus da WERTCO *************
	*****************************************************************************/

	public function editarProspeccaoStatus($id = null){

		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'descricao'			=>	$this->input->post('descricao'));
						 
			if($this->prospeccaoM->atualizarStatus($update)){
				$this->log('Área Administrador | atualizar status prospecção','prospeccaoStatus','EDIÇÃO', $this->session->userdata('usuario_id'),$update,$update,$_SERVER['REMOTE_ADDR']);
				$this->session->set_flashdata('sucesso', 'ok');						

			}else{

				$this->session->set_flashdata('erro', 'erro');				
			}
			
			redirect('AreaAdministradorComercial/prospeccoesStatus');

		}else{

			$row 					= 	$this->prospeccaoM->buscaStatusPorId($id);
			$parametros 			= 	$this->session->userdata();
			$parametros['dados']	=	$row;
			$parametros['title']	=	"Editar Status Prospecções";			
			$this->_load_view('area-administrador-comercial/prospeccoes/editar-prospeccao-status',$parametros );
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir prospeccaoStatus ******************************
	*****************************************************************************/
	public function excluirStatusProspeccao()
	{
																															
		if($this->prospeccaoM->excluirStatus($_POST['id'])){
			echo json_encode(array('retorno' => 'sucesso'));

		}else{
			echo json_encode(array('retorno' => 'erro'));

		}

	}

	public function encaminharAtendimento()
	{
		$atendimento = array(	'id' 					=> 	$this->input->post('id'),
								'setor_destinacao' 		=> 	$this->input->post('setor_destinacao'),
								'dthr_destinacao'		=> 	date('Y-m-d H:i:s'),
								'usuario_destinacao'	=> 	$this->session->userdata('usuario_id'),
								'msg_destinacao'		=>	$this->input->post('msg_destinacao'),
								'status_id'				=> 	2	);

		if( $this->prospeccaoM->update($atendimento) ){
			$atividade = $this->prospeccaoM->buscaAtividades($this->input->post('id'));
			$titulo = "Encaminhamento de atendimento - Setor Comercial";
			$email='<html>
					<head></head>
					<body style="width: 100%; height: 100%; position:absolute;">
						<div style="width: 80%;padding-top: 10px;position: relative;text-align: left;margin-left: 40px;">
							<img src="https://wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" width="200" />
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Solicitante: '.$this->input->post('cliente').'</h1>
							<hr/>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
								'.$this->input->post('msg_destinacao').'
							</p>
							<hr/>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
								Nome do Contato:  '.$atividade[0]['nome'].'<br/>
								Telefone: '.$atividade[0]['telefone'].'<br/>
								Celular:  '.$atividade[0]['celular'].'<br/>
								Cargo:  '.$atividade[0]['cargo'].'<br/>
							</p>
							<hr/>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
								Mensagem enviada por: '.$this->session->userdata('nome').'
							</p>
						</div>
					</body>
				</html>';
			if($this->enviaEmail('gestor@wertco.com.br',$titulo,$email )){
				echo json_encode(array('retorno' => 'sucesso'));
			}else{
				echo json_encode(array('retorno' => 'erro'));
			}
		}else{
			echo json_encode(array('retorno' 	=> 	'erro'));
		}
	}

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->send();
		}else{
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)
			    ->send();
		}

		return $result;		
	}

	private function enviaEmailStatusGestor($orcamento_id, $status_id)
    {
    	$cliente = $this->orcamentosM->buscaClienteOrcamento($orcamento_id);
    	
    	switch($status_id){
			case 1:
		        $status = "Orçamento Aberto!";
		        break;
		    case 2:
		        $status = "Orçamento Fechado, venda realizada!";
		        break;
		    case 3:
		        $status = "Orçamento Perdido.";
		        break;
		    case 4:
		        $status = "Orçamento Cancelado.";
		        break;
		    case 5:
		        $status = "Orçamento entregue ao cliente.";
		        break;        
		    case 6:
		        $status = "Orçamento em negociação.";
		        break;
		    case 7:
		    	$status = "Solicitação de Orçamentos - Indicador";
		    	break;

		     case 9:
		        $status = "Bombas em teste no cliente.";
		        break; 
		    case 10:
		        $status = "Orçamento Perdido para Wayne.";
		        break;
		    case 11:
		        $status = "Orçamento Perdido para Gilbarco.";
		        break;
		    case 12:
		        $status = "Aguardando Aprovação Gestor.";
		        break;
		    case 13:
		        $status = "Expirado.";
		        break;
		    case 14:
		    	$status = "Orçamento Sem Retorno.";
		        break;
		}

    	$conteudo = '<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Modificação de Status - Orçamento: '.$orcamento_id.'</h2>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Orçamento: <a href="'.base_url('areaAdministrador/visualizarOrcamento/'.$orcamento_id).'"> #'.$orcamento_id.'</a><br/>									
								</p>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Cliente: '.$cliente['cliente'].'</a><br/>									
								</p>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Cidade/Estado: '.$cliente['cidade'].'/'.$cliente['estado'].'<br/>		
								</p>	
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Novo Status: <b>'.$status.'</b><br/>		
								</p>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Alterado Por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>							
							</div>
						</body>
					</html>';
		
		if($this->enviaEmail('gestor@wertco.com.br','⚠️ Atenção! Status Alterado - Orçamento:'.$orcamento_id.' ⚠️',$conteudo)){
			return true;
		}else{
			return false;
		}
	}

}