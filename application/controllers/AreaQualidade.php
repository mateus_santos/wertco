<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0); 
ini_set('memory_limit', '1256M');

class AreaQualidade extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'qualidade' && $this->session->userdata('tipo_acesso') != 'administrador geral' && $this->session->userdata('tipo_acesso') != 'suporte' && $this->session->userdata('tipo_acesso') != 'assistencia tecnica')  ) 
		{
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('ChamadoModel', 'chamadosM');
		$this->load->model('BombasNrSerieModel', 'bombasM');
		$this->load->model('PedidosModel', 'pedidosM');
		$this->load->helper('form');
		$this->load->helper('url');
	}

	public function index()
	{
		$parametros			 	=	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();		
		$parametros['title']	=	"Área da Qualidade";		
		$this->_load_view('area-qualidade/index',$parametros);

	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados'] 				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-qualidade/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-qualidade/editar-cliente',	$parametros );	
		}
	
	}

	public function relatorioChamados(){
    	
		$parametros 				= 	$this->session->userdata();
		$parametros['title']		=	"Relatório de Chamados";
		$parametros['status']		=	$this->chamadosM->selectStatus();
		$parametros['tipo']			= 	$this->chamadosM->selectTipos();
		$this->_load_view('area-qualidade/relatorios/relatorio-chamado-qualidade',$parametros );	
	}

    public function gerarRelatorioChamados(){
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-qualidade-chamados-pdf.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');		

	    $dt_ini = '';
	    if($this->input->post('dt_ini') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_ini'));	    	
	    	$dt_ini 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    }
	    $dt_fim = '';
	    if($this->input->post('dt_fim') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_fim'));	    	
	    	$dt_fim 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    }

	    $filtros = array(	'dt_ini'		=> 	$dt_ini,
	    					'dt_fim'		=> 	$dt_fim 	); 

	    $filtros['status_id'] = ($this->input->post('status_id') != '') ? ' status_id ='.$this->input->post('status_id') : ' 1=1';

	    $filtros['tipo_id'] = ($this->input->post('tipo_id') != '') ? ' tipo_id ='.$this->input->post('tipo_id') : ' 1=1';

	    $data['periodo'] = $this->input->post('dt_ini').' a '.$this->input->post('dt_fim');

	     
	    if($this->input->post('tipo_id') != ''){
	    	$tipo = $this->chamadosM->buscaTiposById($this->input->post('tipo_id'));
	    	$data['periodo'].= ' - Tipo: '. $tipo['descricao'];
	    }

	    if( $this->input->post('status_id') != '' ){
	    	$status = $this->chamadosM->buscaStatusById($this->input->post('status_id'));
	    	$data['periodo'].= ' - Status: '. $status['descricao'];
	    }
	    
	    $data['dados']	=	$this->chamadosM->relatorioQualidadeChamados($filtros);
		//$this->load->view('/area-qualidade/relatorios/relatorio-chamados-qualidade-pdf', $data,true)
	    //Load html view		
		$this->html2pdf->html($this->load->view('/area-qualidade/relatorios/relatorio-chamados-qualidade-pdf', $data,true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-qualidade-chamados-pdf.pdf'));
	    
		}
    }

    public function rastreabilidade()
    {
    	$parametros			 	=	$this->session->userdata();
		//$parametros['dados']	=	$this->bombasM->buscaNrSerie();		
		$parametros['title']	=	"Área da Qualidade";		
		$this->_load_view('area-qualidade/rastreabilidade',$parametros);
    } 

    public function buscaNrSerieSS(){

		$dados 		=	$this->input->post();
		$retorno 	=	$this->bombasM->buscaNrSerieSS($dados);
		echo json_encode($retorno);
	
	}

	public function atualizaNrSerieAfericao()
	{	
		
		$updateDtAfericao = array( 	'id' 			=> 	$this->input->post('id'),
									'dt_afericao' 	=> 	date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('dt_afericao') ) ) ) );

		if($this->bombasM->update($updateDtAfericao)){
			
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}	

	public function atualizaNrSerieQtdLacres()
	{
		
		$updateQtdLacres = array( 	'id' 			=> 	$this->input->post('id'),
									'qtd_lacres' 	=> 	$this->input->post('qtd_lacres') );

		if($this->bombasM->update($updateQtdLacres)){
			
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function atualizaNrSerieObs()
	{
		
		$updateObs = array( 'id' 			=> 	$this->input->post('id'),
							'observacoes' 	=> 	$this->input->post('observacao') );

		if($this->bombasM->update($updateObs)){
			
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function buscaNrSerieAfericao()
	{
		$afericao = $this->bombasM->buscaNrSerieId($this->input->post('id'));
		$dt_afericao = date('d/m/Y', strtotime( $afericao['dt_afericao'] ));  
		echo json_encode(array(	'retorno'  => 'sucesso',
								'dt_afericao' => $dt_afericao) );
	
	}

	public function buscaNrSerieQtdLacres()
	{
		$retorno = $this->bombasM->buscaNrSerieId($this->input->post('id'));
		
		echo json_encode(array(	'retorno'  => 'sucesso',
								'qtd_lacres' => $retorno['qtd_lacres']) );
	
	}

	public function buscaNrSerieObs()
	{
		$retorno = $this->bombasM->buscaNrSerieId($this->input->post('id'));
		
		echo json_encode(array(	'retorno'  => 'sucesso',
								'observacao' => $retorno['observacoes']) );
	
	}

	public function relatorioRastreabilidade(){
    	
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Relatório Rastreabilidade";

		$this->_load_view('area-qualidade/relatorios/relatorio-rastreabilidade',$parametros );

    }

    public function gerarRelatorioRastreabilidade(){
    	    	
	    //Load the library
	    /*$this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-rastreabilidade-pdf.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');
		*/
	    $dt_ini = '';
	    $data['periodo'] = '';
	    if($this->input->post('dt_ini') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_ini'));	    	
	    	$dt_ini 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    	$data['periodo'] = $this->input->post('dt_ini');
	    }

	    $dt_fim = '';
	    if($this->input->post('dt_fim') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_fim'));	    	
	    	$dt_fim 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    	$data['periodo'].= ' a '.$this->input->post('dt_fim');
	    }

	    $filtros = array(	'nr_serie'	 		=> 	$this->input->post('nr_serie'),
	    					'dt_ini'			=> 	$dt_ini,
	    					'dt_fim'			=> 	$dt_fim 	); 
	    
	    $data['dados']	=	$this->bombasM->buscaRastreabilidade($filtros);
		$this->load->view('/area-qualidade/relatorios/relatorio-rastreabilidade-pdf', $data);				
		/*$this->html2pdf->html(utf8_decode($this->load->view('/area-qualidade/relatorios/relatorio-rastreabilidade-pdf', $data,true)));

	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-rastreabilidade-pdf.pdf'));
	    
		}*/
    }

    public function avisoProducaoReprovado()
    {
    	$dados = array('id'					=>	$this->input->post('pedido_id'),
					   'status_pedido_id' 	=>	3);

		if($this->pedidosM->atualizaStatusPedido($dados)){

	    	$insere = array( 	'pedido_id' => 	$this->input->post('pedido_id'),
	    						'motivo'	=> 	$this->input->post('motivo'),
	    						'usuario_id'=> 	$this->session->userdata('usuario_id') );
	    	if( $this->pedidosM->insereMotivoReprovacao($insere) ){
	    		$conteudo = '<html>
							<head></head>
							<body style="width: 850px; position:absolute;">
								<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
									<img src="https://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
									<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Produto(s) reprovado(s) na inspeção da qualidade - Pedido #'.$this->input->post('pedido_id').'</h2>
									<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										Motivo: '.$this->input->post('motivo').' <br/>									
									</p>								
									<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										Enviado por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
									</p>							
								</div>
							</body>
						</html>';
			
				if($this->enviaEmail('producao@wertco.com.br','⚠️ Produto(s) Reprovado(s) na Inspeção da Qualidade #'.$this->input->post('pedido_id').' ⚠️',$conteudo)){
					//$this->enviaEmail('webmaster@wertco.com.br','⚠️ Produto(s) Reprovado(s) na Inspeção da Qualidade #'.$this->input->post('pedido_id').' ⚠️',$conteudo);
					$this->insereAndamentoPedido($_POST['pedido_id'], $_POST['status_pedido']);
	    			echo json_encode(array('retorno' => 'sucesso'));
	    		}else{
	    			echo json_encode(array('retorno' => 'email não enviado'));	
	    		}
	    	}else{
	    		echo json_encode(array('retorno' => 'erro'));
	    	}
    	}else{
	    	echo json_encode(array('retorno' => 'erro'));
	    }

    }

    private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->send();
		}else{
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)
			    ->send();
		}

		return $result;		
	}

	// Método para inserção dos andamentos dos pedidos
	private function insereAndamentoPedido($pedido_id, $status_pedido_id)
	{
		$andamento = $this->pedidosM->buscaStatusByid($status_pedido_id);

		$insert = array(	'pedido_id' 		=> 	$pedido_id,
							'status_pedido_id' 	=>	$status_pedido_id,
							'andamento'			=> 	$andamento['descricao'],
							'usuario_id' 		=> 	$this->session->userdata('usuario_id') );

		if( $this->pedidosM->insereAndamento($insert) ){
			return true;
		}else{
			return false;
		}
	}

	public function relatorioMediaChamados(){
    	
		$parametros 				= 	$this->session->userdata();
		$parametros['title']		=	"Relatório de Média de Chamados Atendidos";		
		$parametros['tipo']			= 	$this->chamadosM->selectTipos();
		$this->_load_view('area-qualidade/relatorios/relatorio-media-chamado',$parametros );	
	}

    public function gerarRelatorioMediaChamados(){
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('relatorio-media-chamados-pdf.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'landscape');		

	    $dt_ini = '';
	    if($this->input->post('dt_ini') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_ini'));	    	
	    	$dt_ini 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    }
	    $dt_fim = '';
	    if($this->input->post('dt_fim') != ''){
	    	$explode 	= 	explode('/',$this->input->post('dt_fim'));	    	
	    	$dt_fim 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
	    }

	    $filtros = array(	'dt_ini'		=> 	$dt_ini,
	    					'dt_fim'		=> 	$dt_fim 	); 	    

	    $filtros['tipo_id'] = ($this->input->post('tipo_id') != '') ? ' tipo_id ='.$this->input->post('tipo_id') : ' 1=1';

	    $data['periodo'] = $this->input->post('dt_ini').' a '.$this->input->post('dt_fim');
	     
	    if($this->input->post('tipo_id') != ''){
	    	$tipo = $this->chamadosM->buscaTiposById($this->input->post('tipo_id'));
	    	$data['periodo'].= ' - Tipo: '. $tipo['descricao'];
	    }	    
	    
	    $data['dados']	=	$this->chamadosM->relatorioMediaChamado($filtros);
		//$this->load->view('/area-qualidade/relatorios/relatorio-chamados-qualidade-pdf', $data,true)
	    //Load html view		
		$this->html2pdf->html($this->load->view('/area-qualidade/relatorios/relatorio-media-chamado-pdf', $data,true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	redirect(base_url('pdf/relatorio-media-chamados-pdf.pdf'));
	    
		}
    }

}