<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contato extends CI_Controller {
	public function __construct() {
		parent::__construct();

		// carrega a model a ser utilizada neste controller
		$this->load->model('ContatoModel','contatoM');

	}

	public function cadastra()
	{
		$dados = $this->contatoM->getTotalContato($_POST['email'],$_POST['message'] );

		if($dados[0]->total > 0){
			echo json_encode(array('retorno' => 'Mensagem já enviada'));
		}else{
			
			$insere = array('email' 	=> 	$_POST['email'],
							'firstname' =>	$_POST['firstname'],
							'lastname' 	=>	$_POST['lastname'],
							'message' 	=>	$_POST['message'],
							'phone'		=> 	$_POST['phone']);

			if($this->contatoM->add($insere))
			{

				$conteudo = "<p><b>Nome:</b> ". $_POST['firstname'].' '.$_POST['lastname']."<p/>";
				$conteudo.= "<p><b>Email: </b>". $_POST['email']."<p/>";
				$conteudo.= "<p><b>Telefone: </b>". $_POST['phone']."<p/>";
				$conteudo.= "<p><b>Mensagem: </b>". $_POST['message']."<p/>";  	

				if($this->enviaEmail('marketing@companytec.com.br','Contato do site',$conteudo ))
				{
					echo json_encode(array('retorno' => 'sucesso'));		
				}else{
					echo json_encode(array('retorno' => 'Menasgem não enviada'));		
				}
				
			}else{
				echo json_encode(array('retorno' => 'Erro de inserção'));		
			}

		}

	}

	private function enviaEmail($destinatario, $titulo = NULL, $conteudo = NULL  )
	{
		$this->load->library('email');
		// Get full html:
		$body = '<html>
					<head>
						<title>'.$titulo.'</title>
					</head>
				<body background="#ffcc00"> 						

					<img src="http://www.wertco.com.br/wertcofundoescuro.png" height="225" style="width: 50px !important;height: 25px !important;" />

					<h2>'.$titulo.'</h2>

					<div>

						'.$conteudo.'

					</div>

					</body>

					</html>';
		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('marketing@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to($destinatario)
		    ->subject($titulo)
		    ->message($body)
		    ->send();

		return $result;		

		
	}
	
}