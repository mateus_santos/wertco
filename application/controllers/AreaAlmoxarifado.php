<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AreaAlmoxarifado extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta		
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'almoxarifado' && ($this->session->userdata('tipo_acesso') != 'administrador geral' && $this->session->userdata('tipo_acesso') != 'suporte')) ){
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('solicitacaoPecasModel', 'solicitacaoPecasM');
		$this->load->model('solicitacaoPecasItensModel', 'solicitacaoPecasItensM');
		$this->load->model('empresasModel', 'empresasM');
		$this->load->model('chamadoModel', 'chamadoM');
		$this->load->model('chamadoModel', 'chamadoM');
		$this->load->model('CartaoTecnicosModel', 'cartaoM');

		$this->load->helper('form');
	}

	public function index()
	{
		$parametros 						= 	$this->session->userdata();
		$parametros['title']				=	"Área da Assistencia Técnica";
		$parametros['total_status']			= 	$this->chamadoM->selectTotalStatus();
		$parametros['total_defeitos']		= 	$this->chamadoM->selectTotalDefeitos();
		$parametros['total_mes']			= 	$this->chamadoM->totalChamadoMes();
		$parametros['total_estado']			= 	$this->chamadoM->totalChamadoEstado();
		$parametros['ultimos_chamados']		= 	$this->chamadoM->ultimosChamadosRealizados();
		$parametros['ultimas_solicitacoes']	= 	$this->solicitacaoPecasM->ultimasSolicitacoes();
		$parametros['chamados_inativos']	=	$this->chamadoM->buscaChamadosInativos($this->session->userdata('usuario_id'));
		$parametros['dados']				=	$this->chamadoM->select(1);
		$this->_load_view('area-almoxarifado/index',$parametros );
	}
	
	public function editarCliente()
	{
		if($this->input->post('salvar') == 1)
		{

			$update = array(	'id'				=>	$this->input->post('id'),
								'nome'				=>	$this->input->post('nome'),
								'cpf'				=>	$this->input->post('cpf'),
								'email'				=>	$this->input->post('email'),
								'telefone'			=>	$this->input->post('telefone')	);

			if( $_FILES['nome_arquivo']['name'] != "")
			{
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}
			
			if($this->input->post('senha') != "")
			{
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update))
			{
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-clientes/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-clientes/editar-cliente',	$parametros );	
		}
	}

	public function solicitacaoPecas()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Solicitações de Peças - Almoxarifado";
		$parametros['dados'] 	=	$this->solicitacaoPecasM->buscaSolicitacoes();
		
		$this->_load_view('area-almoxarifado/assistencia-tecnica/solicitacao-pecas-almox',$parametros );
	}	

	public function visualizarSolicitacaoPecas($id)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->solicitacaoPecasM->buscaSolicitacaoPorId($id);		
		$parametros['itens']	=	$this->solicitacaoPecasM->buscaItensSolicitacao($id);		
		$parametros['title']	=	"Solicitação de Peças #".$id;
		$this->_load_view('area-almoxarifado/assistencia-tecnica/visualiza-solicitacao-pecas-almox',$parametros);
	}

	public function atualizaInfoEnvio()
	{
		$update = array('id' 			=> 	$this->input->post('solicitacao_peca_id'),
						'info_caixas'	=> 	$this->input->post('info_caixas')	);

		if( $this->solicitacaoPecasM->atualizar($update) )
		{
			$this->session->set_flashdata('retorno', 'sucesso');
		}else{
			$this->session->set_flashdata('retorno', 'erro');
		}
		
		$this->visualizarSolicitacaoPecas($update['id']);
	}

	public function alterarNrSerie()
	{
		$update = array(	'id' 		=> 	$this->input->post('id'),
							'nr_serie'	=> 	$this->input->post('nr_serie') );
		if( $this->solicitacaoPecasM->alterarNrSerie($update) )
		{
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function confirmarPecasEnviar()
	{
		$update = array(	'id' 		=> 	$this->input->post('solicitacao_id'),
							'status_id'	=> 10	);

		if( $this->solicitacaoPecasM->atualizar($update) )
		{
			$inserirAndamento = array(	'solicitacao_pecas_id'		=>	$this->input->post('solicitacao_id'),
										'status_solicitacao_id'		=> 	10,
										'andamento'					=>	'Peças disponíveis para envio!',
										'dthr_andamento'			=>	date('Y-m-d H:i:s'),
										'usuario_id'				=> 	$this->session->userdata('usuario_id') );

			if($this->solicitacaoPecasM->insereAndamentos($inserirAndamento)){
				$titulo = "⚠️ Peças disponíveis para envio. Chamado: #".$this->input->post('chamado_id');
				$email='<html>
						<head></head>
						<body style="width: 650px; height: 600px; position:absolute;">
							<div style="width: 80%;padding-top: 10px;position: relative;text-align: left;margin-left: 40px;">
								<img src="https://wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" width="200" />
								<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Peças disponíveis para envio</h1>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Solicitação: 	'.$this->input->post('solicitacao_id').'<br/>
									Cliente:'.$this->input->post('cliente').'<br/>
									Chamado:'.$this->input->post('chamado_id').'<br/>
								</p>
							</div>
						</body>
					</html>';
				if( $this->enviaEmail('assistenciatecnica@wertco.com.br',$titulo, $email) ){
					$this->enviaEmail('todossuporte@wertco.com.br',$titulo, $email);
					echo json_encode(array('retorno' => 'sucesso'));
				}else{
					echo json_encode(array('retorno' => 'erro'));	
				}
			}
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	
	}

	public function alterarStatusSolicitacaoAlmox()
	{
		$dados = array(	'id' 		=> 	$this->input->post('solicitacao_id'),
						'status_id' => 	3);

		if( $this->solicitacaoPecasM->atualizar($dados) ){

			$andamento = array(	'solicitacao_pecas_id' 	=> 	$this->input->post('solicitacao_id'),
								'status_solicitacao_id'	=> 	3,
								'andamento' 			=> 	'Almoxarifado recebeu a solicitação e já está dando andamento no processo.',
								'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
								'usuario_id'			=> 	$this->session->userdata('usuario_id') );
			if($this->solicitacaoPecasM->insereAndamentos($andamento)){

				echo json_encode(array('retorno' => 'sucesso'));
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function excluirItem()
	{		
		$qtd = $this->input->post('qtd') - 1;
		if( $qtd > 0 ){
			$where = array( 'id' 	=>  $this->input->post('item_id'),
							'qtd' 	=> 	$qtd);
			$this->solicitacaoPecasItensM->excluirSubItem($this->input->post('item_nr_id'), $where);
		}else{
			$this->solicitacaoPecasItensM->excluirItem($this->input->post('item_id'));
		}
		$andamento = array(	'solicitacao_pecas_id' 	=> 	$this->input->post('solicitacao_id'),
							'status_solicitacao_id'	=> 	$this->input->post('status_id'),
							'andamento' 			=> 	'Exclusão de item da peça <b>'.$this->input->post('descricao').'.</b> Motivo: '.$this->input->post('motivo_exclusao'),
							'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
							'usuario_id'			=> 	$this->session->userdata('usuario_id') );
		if($this->solicitacaoPecasM->insereAndamentos($andamento)){

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}

	}

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)		    
			    ->send();
		}else{
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)		    
			    ->send();
		}

		return $result;		
	}

}