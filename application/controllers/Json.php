<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Json extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado
		if( !$this->_is_logged() ){
			redirect(base_url('usuarios/login'));
		}
	}

	public function index()
	{		
		$parametros = $this->session->userdata();
		$this->_load_view('area-tecnico/json/json',$parametros);
	}

	public function senha()
	{
		$this->_load_view('area-tecnico/json/senha');
	}

}
