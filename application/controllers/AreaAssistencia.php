<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AreaAssistencia extends MY_Controller { 
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta		
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'assistencia tecnica' && $this->session->userdata('tipo_acesso') != 'administrador geral' && $this->session->userdata('tipo_acesso') != 'administrador tecnico' && $this->session->userdata('tipo_acesso') != 'almoxarifado' && $this->session->userdata('tipo_acesso') != 'financeiro' && $this->session->userdata('tipo_acesso') != 'suporte' && $this->session->userdata('tipo_acesso') != 'expedicao') ){
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('solicitacaoPecasModel', 'solicitacaoPecasM');
		$this->load->model('solicitacaoPecasItensModel', 'solicitacaoPecasItensM');
		$this->load->model('empresasModel', 'empresasM');
		$this->load->model('chamadoModel', 'chamadoM');
		$this->load->model('chamadoModel', 'chamadoM');
		$this->load->model('CartaoTecnicosModel', 'cartaoM');

		$this->load->helper('form');
	}
 
	public function index()
	{
		$parametros 					= 	$this->session->userdata();
		$parametros['title']			=	"Área da Assistencia Técnica";
		$parametros['total_status']		= 	$this->chamadoM->selectTotalStatus();
		$parametros['total_defeitos']	= 	$this->chamadoM->selectTotalDefeitos();
		$parametros['total_mes']		= 	$this->chamadoM->totalChamadoMes();
		$parametros['total_estado']		= 	$this->chamadoM->totalChamadoEstado();
		$parametros['ultimos_chamados']	= 	$this->chamadoM->ultimosChamadosRealizados();
		$parametros['ultimas_solicitacoes']	= 	$this->solicitacaoPecasM->ultimasSolicitacoes();
		$parametros['chamados_inativos']	=	$this->chamadoM->buscaChamadosInativos($this->session->userdata('usuario_id'));
		$parametros['dados']			=	$this->chamadoM->select(1);	
		
		$this->_load_view('area-assistencia/assistencia-tecnica/dashboard-assistencia',$parametros );	
	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){

			$update = array(	'id'				=>	$this->input->post('id'),
								'nome'				=>	$this->input->post('nome'),
								'cpf'				=>	$this->input->post('cpf'),
								'email'				=>	$this->input->post('email'),
								'telefone'			=>	$this->input->post('telefone')	);

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}
			
			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-clientes/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-clientes/editar-cliente',	$parametros );	
		}
	}	
	
	public function solicitacaoPecas(){

		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Solicitações de Peças - Assistência Técnica";
		$parametros['situacao']	= 	$this->solicitacaoPecasM->retornaStatusSolicitacao();
		$parametros['dados'] 	=	$this->solicitacaoPecasM->buscaSolicitacoes();	
		$parametros['titulo']	= 	"Solicitação de peças pendentes";
		$this->_load_view('area-assistencia/assistencia-tecnica/solicitacao-pecas',$parametros );	
	}

	public function solicitacaoPecasCompletas(){

		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Solicitações de Peças Completas- Assistência Técnica";
		$parametros['situacao']	= 	$this->solicitacaoPecasM->retornaStatusSolicitacao();
		$parametros['dados'] 	=	$this->solicitacaoPecasM->buscaSolicitacoesFechadas();	
		$parametros['titulo']	= 	"Solicitação de peças concluídas";
		$this->_load_view('area-assistencia/assistencia-tecnica/solicitacao-pecas',$parametros );	
	}

	public function visualizarSolicitacaoPecas($id)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->solicitacaoPecasM->buscaSolicitacaoPorId($id);		
		$parametros['itens']	=	$this->solicitacaoPecasM->buscaItensSolicitacao($id);
		$parametros['title']	=	"Solicitação de Peças #".$id;
		$this->_load_view('area-assistencia/assistencia-tecnica/visualiza-solicitacao-pecas',$parametros);
	}

	public function alteraStatusSolicitacaoPecas($solicitacao_id=null, $status_solicitacao=null, $chamado_id=null, $nome_cliente=null)
	{

		$dados = array('id'			=>	$_POST['solicitacao_id'],
					   'status_id' 	=>	$_POST['status_solicitacao']);

		if($this->solicitacaoPecasM->atualizar($dados))
		{
			switch( $_POST['status_solicitacao'] ){
				case 1 : 
					$status = 'SOLICITAÇÃO REALIZADA';
					break;
				case 2 :
					$status = 'VERIFICAÇÃO DAS PEÇAS NO ESTOQUE';
					break;
				case 3 :
					$status = 'PEÇAS EM SEPARAÇÃO PARA ENVIO';
					break;
				case 4 :
					$status = 'PEÇAS ENVIADAS';
					break;
				case 5 :
					$status = 'SOLICITAÇÃO CANCELADA';
					break;
				case 6 :
					$status = 'PEÇA(S) ENTREGUE(S)';
					break;
				case 7 :
					$status = 'SOLICITAÇÃO DE PEÇAS CONCLUÍDA';
					$update = array(	'id'				=>	$_POST['solicitacao_id'],
					   					'tipo'				=> 	$_POST['tipo'],
					   					'nr_nf_retorno' 	=> 	$_POST['nf_retorno'] );

					$this->solicitacaoPecasM->atualizar($update);
					break;
				case 8 :
					$status = 'PEÇAS RETORNADAS A WERTCO';
					break;
				case 9 :
					$status = 'SERVIÇO REALIZADO PELO TÉCNICO';
					break;
				case 10 : 
					$status = 'PEÇAS DISPONÍVEIS PARA ENVIO';
					break;
				case 11 : 
					$status = 'AGUARDANDO NF E PEÇAS';
					break;
				case 12 : 
					$status = 'AGUARDANDO NF';
					break;
				case 13 : 
					$status = 'AGUARDANDO PEÇAS';
					break;
				case 14 : 
					$status = 'DISPONÍVEL PARA RETIRADA';
					$email='<html>
						<head></head>
						<body style="width: 850px; position:absolute;">
							<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
								<img src="https://wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" /><br/>
								<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Peça(s) Disponível(is) para Retirada </h2>						
								<hr>														
								<h2><b>ATENÇÃO!</b><BR>Entrar em contato com o responsável para realizar a retirada das peças.</h2>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Cliente: '.$_POST['nome_cliente'].'							
								</p>
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Chamado: '.$_POST['chamado_id'].'							
								</p>								
								<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
									Enviado por: '.$this->session->userdata('nome').' - '.date('d/m/Y H:i:s').'
								</p>							
							</div>
						</body>
					</html>';	
					$this->enviaEmail('expedicao@wertco.com.br','Peças Liberadas para Retirada #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'],$email );
					$envio_email = 	$this->enviaEmail('todossuporte@wertco.com.br','Peças Liberadas para Retirada',$email );
					break;
						
			}

			$envio_email = 0;
			if($_POST['status_solicitacao']	==	9)
			{
				$email='<html>
					<head></head>
					<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
						<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Solicitação de Peças #'.$_POST['solicitacao_id'].'<br/> Chamado# '.$_POST['chamado_id'].' <br/><hr>Status alterado para:<br/> "'.$status.'".</h1>

							<hr>														
							<h2>ATENÇÃO!<BR>Entrar em contato com o técnico para verificar se deve haver retorno das peças para a WERTCO.</h2>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
								Cliente: '.$_POST['nome_cliente'].'							
							</p>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
								Alterado pelo Usuário: '.$this->session->userdata('nome').'

							</p>
							
						</div>
					</body>
				</html>';	
				
				$envio_email = 	$this->enviaEmail('industrial@wertco.com.br',	'Solicitação de Peças #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'].'- Status alterado',$email );
				$envio_email = 	$this->enviaEmail('todossuporte@wertco.com.br','Solicitação de Peças #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'].'- Status alterado',$email );
				$envio_email = 	$this->enviaEmail('expedicao@wertco.com.br','Solicitação de Peças #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'].'- Status alterado',$email );
				
			}else{

				$email='<html>
							<head></head>
							<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
								<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
									<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Solicitação de Peças #'.$_POST['solicitacao_id'].'<br/> Chamado# '.$_POST['chamado_id'].' <br/><hr>Status alterado para:<br/> "'.$status.'".</h1>

									<hr>														
									<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										Cliente: '.$_POST['nome_cliente'].'
									
									</p>
									<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
										Alterado pelo Usuário: '.$this->session->userdata('nome').'

									</p>
									
								</div>
							</body>
						</html>';
				$envio_email = 	$this->enviaEmail('industrial@wertco.com.br','Solicitação de Peças #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'].'- Status alterado',$email );
				$envio_email = 	$this->enviaEmail('todossuporte@wertco.com.br','Solicitação de Peças #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'].'- Status alterado',$email );
				$envio_email = 	$this->enviaEmail('expedicao@wertco.com.br','Solicitação de Peças #'.$_POST['solicitacao_id'].' Chamado #'.$_POST['chamado_id'].'- Status alterado',$email );
			}

    		if($envio_email != 0){
    			$inserirAndamento = array(	'solicitacao_pecas_id'	=>	$_POST['solicitacao_id'],
											'status_solicitacao_id'	=> 	$_POST['status_solicitacao'],
											'andamento'				=>	'Status da solicitação alterado para <b>'.$status.' '.$_POST['nf_retorno'].'</b>',
											'dthr_andamento'		=>	date('Y-m-d H:i:s'),
											'usuario_id'			=> 	$this->session->userdata('usuario_id'));

				if($this->solicitacaoPecasM->insereAndamentos($inserirAndamento)){
					if( $_POST['status_solicitacao'] == 11 || $_POST['status_solicitacao'] == 12 || $_POST['status_solicitacao'] == 13){
						$cobranca = array(	'solicitacao_pecas_id'	=>	$_POST['solicitacao_id'],
													'status_solicitacao_id'	=> 	$_POST['status_solicitacao'],
													'andamento'				=>	$_POST['andamento_cobranca'],
													'dthr_andamento'		=>	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'));
						$this->solicitacaoPecasM->insereAndamentos($cobranca);
					}

					$this->log('Área Administrador | Atualizar Status Solicitação de Peças','solicitação de peças','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);
					echo json_encode(array('retorno' => 'sucesso'));
				}
    		}else{
    			
    			echo json_encode(array('retorno' => 'erro'));
    		}			
			
		}else{

			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function alterarCustoSolicitacao(){

		$dadosUpdate = array(	'id'	=> 	$_POST['solicitacao_pecas_id'],
							 	'custo'	=>	str_replace(',','.', str_replace('.','',$_POST['custo']))	);
		
		if($this->solicitacaoPecasItensM->atualizarCusto($dadosUpdate)){
			$this->log('Área Administrador | atualiza custo solicitacao pecas','solicitacao_pecas_itens','EDIÇÃO', $this->session->userdata('usuario_id'),$dadosUpdate,$dadosUpdate,$_SERVER['REMOTE_ADDR']);
			
			$insereAndamentoSolicitacao 	= 	array(	'solicitacao_pecas_id' 	=> 	$_POST['solicitacao_id'],
														'andamento'				=> 	'Custo da peça <b>'. $_POST['descricao'].'</b>, foi alterado/adicionada. Novo valor: <b>R$ '.$_POST['custo'].'</b>',
														'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
														'usuario_id'			=> 	$this->session->userdata('usuario_id'),
														'status_solicitacao_id'	=> 	$_POST['status_solicitacao_id']);
		
			if($this->solicitacaoPecasM->insereAndamentos($insereAndamentoSolicitacao))
			{
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}
		}else{
			echo json_encode(array('retorno' => 'erro' ));		
		}

	}

	public function insereAndamentoSolicitacao(){
		
		$inserirAndamento = array(	'solicitacao_pecas_id'	=>	$_POST['solicitacao_id'],
									'status_solicitacao_id'	=> 	$_POST['status_solicitacao_id'],
									'andamento'				=>	$_POST['andamento'],
									'dthr_andamento'		=>	date('Y-m-d H:i:s'),
									'usuario_id'			=> 	$this->session->userdata('usuario_id'));

		if($this->solicitacaoPecasM->insereAndamentos($inserirAndamento))
		{
			echo json_encode(array('retorno' => 'sucesso'));	
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}

	}

	public function anexarNfeAssistencia(){
		
		$tipo 			= 	explode('.', $_FILES['nfe']['name']);
		$arquivo 		=	md5($_FILES['nfe']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
		
		$configuracao = array(
	        'upload_path'   	=> 	'./nfe-assistencia/',		        
	        'allowed_types' 	=> 	'gif|jpg|png|pdf|doc|docx|xls|csv|mp3',
	        'file_name'     	=> 	$arquivo
	    );      

	    $this->load->library('upload', $configuracao);
	    
	    if ($this->upload->do_upload('nfe')){
	    	$data_hora = explode('/', $this->input->post('dthr_envio'));

	    	$update = array( 	'id' 			=> 	$this->input->post('solicitacao_id'),
	    						'nr_nf' 		=>	$this->input->post('nr_nf'),
	    						'arquivo_nfe' 	=> 	$arquivo,
	    						'dthr_envio'	=>  $data_hora[2].'-'.$data_hora[1].'-'.$data_hora[0].' 00:00:00',
	    						'status_id'		=>  4 	);
	    	
	    	if( $this->solicitacaoPecasM->atualizar($update) ){

	    		$email='<html>
					<head></head>
					<body style="width: 650px; height: 600px; position:absolute;">
						<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Nota fiscal de emissão/substituição vinculada a solicitação</h1>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
							Nota Fiscal: '.$this->input->post('nr_nf').'<br/>
							cliente: 	'.$this->input->post('cliente').'<br/> 	
							Solicitação de Peças: '.$this->input->post('solicitacao_id').'<br/>
							Data de envio: '.$this->input->post('dthr_envio').'<br/>
							</p>
						</div>
					</body>
				</html>';

	    		if( $this->enviaEmail('industrial@wertco.com.br','⛽ Nota Fiscal de emissão Vinculada a Solicitação de Peças:'.$this->input->post('solicitacao_id'), $email,'./nfe-assistencia/'.$arquivo) ){
	    			$this->session->set_flashdata('sucesso', 'ok'); 

	    			$inserirAndamento = array(	'solicitacao_pecas_id'		=>	$this->input->post('solicitacao_id'),
												'status_solicitacao_id'		=> 	4,
												'andamento'					=>	'<b>Nota fiscal #'.$this->input->post('nr_nf').'</b> vinculada a solicitacao ',
												'dthr_andamento'			=>	date('Y-m-d H:i:s'),
												'usuario_id'				=> 	$this->session->userdata('usuario_id'));

					$erro = $this->solicitacaoPecasM->insereAndamentos($inserirAndamento);					 
					
	    		}else{
	    			$this->session->set_flashdata('erro', 'erro');
	    		}
	    	}else{
	    		$this->session->set_flashdata('erro', 'erro');
	    	}
	    }

	    $this->solicitacaoPecas();

	}

	public function anexarRastreio(){
			    	
	    	$update = array( 	'id' 			=> 	$this->input->post('solicitacao_id'),
	    						'rastreio' 		=>	$this->input->post('rastreio'),
	    						'status_id'		=> 	4 );

	    	$titulo = '⛽ Inserção do número de rastreio Solicitação de Peças:'.$this->input->post('solicitacao_id');
	    	$status = 'PEÇAS ENVIADAS';
	    	if( $_FILES['comprovante_rastreio']['name'] != '' ){
	    		$upload = $this->uploadUnico($_FILES['comprovante_rastreio'],'./comprovantes_rastreio');	    		
	    		$update['comprovante_rastreio'] =  $upload[0]['nome'];
	    		$update['status_id']			= 	4;
	    		$status = 'PEÇAS ENVIADAS';
	    	}

	    	if( $this->input->post('rastreio_entrada') != '' ){
	    		
	    		$update['rastreio_entrada'] =  $this->input->post('rastreio_entrada');
	    		$update['status_id']			= 	8;
	    		$status = 'PEÇAS RETORNADAS A WERTCO';
	    	}

	    	if( $_FILES['comprovante_rastreio_entrada']['name'] != '' ){

	    		$upload = $this->uploadUnico($_FILES['comprovante_rastreio_entrada'],'./comprovantes_rastreio');
	    		$update['comprovante_rastreio_entrada'] =  $upload[0]['nome'];
	    		$titulo = '⛽ Inserção do número de rastreio de volta Solicitação de Peças:'.$this->input->post('solicitacao_id');
	    	}
	    	
	    	if( $this->solicitacaoPecasM->atualizar($update) ){

	    		$email='<html>
					<head></head>
					<body style="width: 650px; height: 600px; position:absolute;">
						<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Inserção dos números de rastreio</h1>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
							Código Rastreio Saída: 	'.$this->input->post('rastreio').'<br/>
							Código Rastreio Entrada:'.$this->input->post('rastreio_entrada').'<br/>
							cliente: 				'.$this->input->post('cliente').'<br/> 	
							Solicitação de Peças: 	'.$this->input->post('solicitacao_id').'<br/>							
							</p>
						</div>
					</body>
				</html>';

	    		if( $this->enviaEmail('todossuporte@wertco.com.br',$titulo, $email) ){
	    									
					$this->enviaEmail('expedicao@wertco.com.br',$titulo, $email);

	    			$inserirAndamento = array(	'solicitacao_pecas_id'	=>	$this->input->post('solicitacao_id'),
										'status_solicitacao_id'	=> 	$update['status_id'],
										'andamento'				=>	'Status da solicitação alterado para <b>'.$status.'</b>',
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id'));

					if($this->solicitacaoPecasM->insereAndamentos($inserirAndamento)){				

						$this->session->set_flashdata('sucesso', 'ok');
					}
	    		}else{
	    			$this->session->set_flashdata('erro', 'erro');
	    		}			
						
					
	    	}else{
	    		$this->session->set_flashdata('erro', 'erro');
	    	}
	    

	    $this->solicitacaoPecas();

	}

	public function buscaAndamentos()
	{
		$retorno = $this->solicitacaoPecasM->buscaAndamentos($_POST['solicitacao_id']);			
		echo json_encode($retorno);

	}

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)		    
			    ->send();
		}else{
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))    // Optional, an account where a human being reads.
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)		    
			    ->send();
		}

		return $result;		
	}

	private function uploadMultiplo($arquivos,$local){
		
		for ($i=0; $i < count($arquivos['name']); $i++) {
			
			//$tipo  = 	explode('.', $arquivos['name'][$i]);
			$_FILES['file']['name']     = (is_array($arquivos['name'])) ? $arquivos['name'][$i] : $arquivos['name'];
            $_FILES['file']['type']     = (is_array($arquivos['name'])) ? $arquivos['type'][$i] : $arquivos['type'];
            $_FILES['file']['tmp_name'] = (is_array($arquivos['name'])) ? $arquivos['tmp_name'][$i] : $arquivos['tmp_name'];
            $_FILES['file']['error']    = (is_array($arquivos['name'])) ? $arquivos['error'][$i] : $arquivos['error'];
            $_FILES['file']['size']     = (is_array($arquivos['name'])) ? $arquivos['size'][$i] : $arquivos['size'];		
            
			$arquivo =	str_replace(' ','',trim(preg_replace( "[^a-zA-Z0-9_]", "", strtr($_FILES['file']['name'], "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"))));

			$configuracao = array(
		        'upload_path'   	=> 	$local,
		        'allowed_types' 	=> 	'gif|jpg|jpeg|png|pdf|doc|docx|xls|csv|vnd.ms-outlook|msg|zip|rar|txt|log|html|htm|mp4|mp3|xlsx|docx',
		        'file_name'     	=> 	$arquivo,
		        'encrypt_name'		=> 	false
		    );			

		    $this->load->library('upload');
		    $this->upload->initialize($configuracao);
		    
		    if (	!$this->upload->do_upload('file')	){
		    	return false;
			}else{
		    	$upload[] = array( 	'nome' => $this->upload->data('file_name') 	);
			}

		}		
		
	    return $upload;
	}

	function uploadUnico($arquivos,$local){

		$_FILES['file']['name']     = 	$arquivos['name'];
        $_FILES['file']['type']     = 	$arquivos['type'];
        $_FILES['file']['tmp_name'] = 	$arquivos['tmp_name'];
        $_FILES['file']['error']    = 	$arquivos['error'];
        $_FILES['file']['size']     = 	$arquivos['size'];		
        
		$arquivo =	str_replace(' ','',trim(preg_replace( "[^a-zA-Z0-9_]", "", strtr($_FILES['file']['name'], "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"))));

		$configuracao = array(
	        'upload_path'   	=> 	$local,
	        'allowed_types' 	=> 	'gif|jpg|jpeg|png|pdf|doc|docx|xls|csv|vnd.ms-outlook|msg|zip|rar|txt|log|html|htm|mp4|mp3|xlsx|docx',
	        'file_name'     	=> 	$arquivo,
	        'encrypt_name'		=> 	false
	    );			

	    $this->load->library('upload');
	    $this->upload->initialize($configuracao);
	    
	    if (	!$this->upload->do_upload('file')	){
	    	return false;
		}else{
	    	$upload[] = array( 	'nome' => $this->upload->data('file_name') 	);
		}

		return $upload;	
	}

	public function geraChamadoPdf($id, $origem_solicitacao){
		 //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('chamado-'.$id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $data['chamados'] 	= 	$this->chamadoM->buscaChamadosAtividadesById($id);
	    $data['ops'] 		= 	$this->chamadoM->buscaOrdensDePagamento($id);
	    $data['tecnicos'] 	= 	$this->chamadoM->buscaTecnicosChamados($id);

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-assistencia/assistencia-tecnica/chamado-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'chamado-'.$id.'.pdf'));
	    	}else{
	    		//return $retorno['retorno'] = 'chamado-'.$id.'.pdf';
	    		redirect(base_url('pdf/chamado-'.$id.'.pdf'));
	    	}
	    }	   	
	}

	public function geraTodosChamadosPdf(){
		 //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('todos-chamados.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $data['chamados'] 	= 	$this->chamadoM->buscaTodosChamadosPdf();	  

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-assistencia/assistencia-tecnica/chamados-geral-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded	    		    	
	    	redirect(base_url('pdf/todos-chamados.pdf'));
	    	
	    }	   		

	}

	public function atualizaValidadeCartao()
	{
		$parametros 			= 	$this->session->userdata();			
		$parametros['title']	=	"Atualiza Cartão Técnico";

		$this->_load_view('area-assistencia/assistencia-tecnica/atualiza-cartao-admin',$parametros);

	}

	public function atualizaCartaoTecnico(){
		
		$data = explode('/',$_POST['ultima_atualizacao']);		

		$dados = array(	'id'					=>	$_POST['id'],
						'tag'					=>	$_POST['tag'],
						'ultima_atualizacao'	=>	'20'.$data['2'].'-'.$data['1'].'-'.$data['0'],
						'ultima_senha'			=>	$_POST['senha'] );

		if($this->cartaoM->atualizaCartao($dados)){

			$dadosLog = array( 'usuario_id' 		=> $this->session->userdata('usuario_id'),
							   'cartao_tecnico_id' 	=> 	$_POST['id']);
			
			$this->cartaoM->insereLogCartao($dadosLog);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{

			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function retornaDadosCartao(){

		$data_atual 					=	new DateTime(date('Y-m-d'));				
		$ultima_atualizacao 			= 	new DateTime($_POST['ultima_atualizacao']);		 	 	
		$dt_vencimento 					= 	new DateTime($_POST['dt_vencimento']);
		$diferenca						=	$data_atual->diff($ultima_atualizacao);
		$nova_data 						= 	'+'.$_POST['periodo_renovacao'].' month';
		$retorno['vencimento'] 			=  	date('d/m/y',strtotime($nova_data, strtotime($_POST['ultima_atualizacao'])));		

		if(	$dt_vencimento > date('Y-m-d',strtotime($nova_data, strtotime($_POST['ultima_atualizacao']))))
		{
			$retorno['data_renovacao_vencimento'] = 'MAIOR';
		}else{
			$retorno['data_renovacao_vencimento'] = 'MENOR';
		}

		$retorno['maior_vencimento'] 	= 	(strtotime(date('Y-m-d',strtotime($_POST['ultima_atualizacao']))) < strtotime(date('Y-m-d'))) ? 0 : 1;
		$retorno['diferenca_dias']		= 	$diferenca->d;
		$retorno['diferenca_mes']		= 	$diferenca->m;
		$retorno['diferenca_ano']		= 	$diferenca->y;
		$retorno['ultima_atualizacao'] 	= 	date('d/m/Y', strtotime($_POST['ultima_atualizacao']));

		echo json_encode($retorno);
	}

	public function retornaSolicitacaoPecas()
	{
		$dados = array(	'id'				=>	$this->input->post('solicitacao_id'),
					   	'status_id' 		=>	1,
						'motivo_retorno'	=> 	$this->input->post('motivo_retorno'),
						'dthr_retorno' 		=> 	date('Y-m-d H:i:s'),
						'usuario_retorno'	=>	$this->session->userdata('usuario_id') );

		if($this->solicitacaoPecasM->atualizar($dados))
		{
			$inserirAndamento = array(	'solicitacao_pecas_id'	=>	$_POST['solicitacao_id'],
										'status_solicitacao_id'	=> 	1,
										'andamento'				=>	'Solicitação retornada para o almoxarifado. Motivo: <b>'.$this->input->post('motivo_retorno').'</b>',
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id') );

			if($this->solicitacaoPecasM->insereAndamentos($inserirAndamento)){
		
				$email='<html>
					<head></head>
					<body style="background: url(http://www.wertco.com.br/bootstrap/img/emailbackground.png) no-repeat; width: 650px; height: 600px; position:absolute;">
						<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
							<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Retorno da Solicitação de Peças #'.$_POST['solicitacao_id'].'.</h1>

							<hr>														
							<h2>Motivo do retorno: '.$this->input->post('motivo_retorno').'</h2>
							<h2>Enviado por: '.$this->session->userdata('nome').'</h2>
						</div>
					</body>
				</html>';	
		
				if($this->enviaEmail('almoxarifado@wertco.com.br','Retorno de Solicitação de Peças #'.$_POST['solicitacao_id'],$email)){
					$this->enviaEmail('almoxarifado2@wertco.com.br','Retorno de Solicitação de Peças #'.$_POST['solicitacao_id'],$email);
					echo json_encode(array('retorno' => 'sucesso'));
				}else{
					echo json_encode(array('retorno' => 'erro'));
				}	
			}else{
				echo json_encode(array('retorno' => 'erro'));
			}
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function anexarFotosConferencia(){
		
		$total = count($_FILES['foto']['name']);
		for($i=0;$i<$total;$i++){

			$_FILES['fotos']['name'] 	= 	$_FILES['foto']['name'][$i];
			$_FILES['fotos']['type'] 	= 	$_FILES['foto']['type'][$i];
			$_FILES['fotos']['tmp_name'] = 	$_FILES['foto']['tmp_name'][$i];
			$_FILES['fotos']['error'] 	= 	$_FILES['foto']['error'][$i];		
			$_FILES['fotos']['size'] 	= 	$_FILES['foto']['size'][$i];		

			$tipo 			= 	explode('.', $_FILES['fotos']['name']);
			$arquivo 		=	md5($_FILES['fotos']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
			
			$configuracao = array(
		        'upload_path'   	=> 	'./nfe-assistencia/',		        
		        'allowed_types' 	=> 	'gif|jpeg|jpg|png|pdf|doc|docx|xls|csv|mp3',
		        'file_name'     	=> 	$arquivo
		    );      
			
		    $this->load->library('upload', $configuracao);
		    
		    if ($this->upload->do_upload('fotos')){
		    	
		    	$insert = array( 	'solicitacao_id' 	=> 	$this->input->post('solicitacao_id'),			
		    						'img' 				=> 	$this->upload->data('file_name') );

		    	$erro = $this->solicitacaoPecasM->insereFotoConferencia($insert);

		    }
		}
    	$email='<html>
				<head></head>
				<body style="width: 650px; height: 600px; position:absolute;">
					<div style="width: 80%;padding-top: 145px;position: relative;text-align: center;margin-left: 62px;">
						<h1 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Foto inserida para conferência</h1>
						<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">							
						Solicitação de Peças: '.$this->input->post('solicitacao_id').'<br/>
						Data de envio: '.date('d/m/Y H:i:s').'<br/>
						</p>
					</div>
				</body>
			</html>';

    		if( $this->enviaEmail('webmaster@wertco.com.br','Foto para conferência - Solicitação de Peças:'.$this->input->post('solicitacao_id'), $email,'./nfe-assistencia/'.$arquivo) ){
    			$this->session->set_flashdata('sucesso', 'ok');    			
							 					
    		}else{
    			$this->session->set_flashdata('erro', 'erro');
    		}
		    	
		

		$this->solicitacaoPecas();
		
	}

	public function buscarFotosConferencia()
	{
		echo json_encode($this->solicitacaoPecasM->buscarFotosConferencia($this->input->post('solicitacao_id')));
	}

}