<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AreaIndicadores3 extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() || $this->session->userdata('tipo_acesso') != 'indicadores3'){
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('ProdutosModel', 'produtosM');
		$this->load->model('OrcamentosModel', 'orcamentosM');
		$this->load->model('IcmsModel', 'icmsM');
		$this->load->model('FretesModel', 'fretesM');
		$this->load->model('EntregasModel', 'entregasM');
		$this->load->model('FormaPagtoModel', 'formaPagtoM');
		$this->load->helper('form');

	}
 
	public function index()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();		
		$parametros['title']	=	"Área do Administrador do sistema";
		$this->_load_view('area-indicadores3/index',$parametros);
	}


	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha')));
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-indicadores3/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-indicadores3/editar-cliente',	$parametros );	
		}
	} 	

	
	public function orcamentos()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->orcamentosM->buscaOrcamentosRepresentantes($this->session->userdata('usuario_id'));				
		$parametros['title']	=	"Orçamentos";
		$this->_load_view('area-indicadores3/orcamentos/orcamentos-ind3',$parametros);
	}

	public function buscaProximosStatus()
	{
		$retorno = $this->orcamentosM->buscaProximosStatusOrcamento($_POST['orcamento_id']);			
		echo json_encode($retorno);
		
	}

	public function buscaAndamentos()
	{
		$retorno = $this->orcamentosM->buscaAndamentos($_POST['orcamento_id']);
			
		echo json_encode($retorno);
		
	}
	
	public function alteraStatusOrcamento()
	{
		$dados = array('id'						=>	$_POST['orcamento_id'],
					   'status_orcamento_id'	=>	$_POST['status_orcamento']);

		if($this->orcamentosM->atualizaStatusOrcamento($dados))
		{

			switch($_POST['status_orcamento']){
				
				case 1:
			        $andamento = "Orçamento Aberto!";
			        break;
			    case 2:
			        $andamento = "Orçamento Fechado, venda realizada!";
			        break;
			    case 3:
			        $andamento = "Orçamento Perdido para concorrência.";
			        break;
			    case 4:
			        $andamento = "Orçamento Cancelado.";
			        break;
			    case 5:
			        $andamento = "Orçamento entregue ao cliente.";
			        break;        
			    case 6:
			        $andamento = "Orçamento em negociação.";
			        break;
			    case 10:
				    $andamento = "Orçamento Perdido para Wayne.";
				        break;
			    case 11:
			        $andamento = "Orçamento Perdido para Gilbarco.";
			        break;

			}

			$inserirAndamento = array('orcamento_id'			=>	$_POST['orcamento_id'],
										'status_orcamento_id'	=> 	$_POST['status_orcamento'],
										'andamento'				=>	$andamento,
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id'));

			if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
			{
				echo json_encode(array('retorno' => 'sucesso'));	
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function insereAndamentoOrcamento(){
		$inserirAndamento = array('orcamento_id'		=>	$_POST['orcamento_id'],
								'status_orcamento_id'	=> 	$_POST['status_orcamento_id'],
								'andamento'				=>	$_POST['andamento'],
								'dthr_andamento'		=>	date('Y-m-d H:i:s'),
								'usuario_id'			=> 	$this->session->userdata('usuario_id'));

		if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
		{
			echo json_encode(array('retorno' => 'sucesso'));	
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}			

	}

	public function retornaRepresentantes()
	{
		
		$rows = $this->usuariosM->retornaRepresentante($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label' 		=>	$row['label'],
								'id'		=>	$row['id'],
								'value'		=>	$row['label']	);

				
			}

			echo json_encode($dados);
		}
	}

	public function visualizarOrcamento($id) 
	{
		$parametros 				= 	$this->session->userdata();
		$parametros['dados']		=	$this->orcamentosM->getOrcamentoProdutos(base64_decode($id));
		$parametros['desconto']		=	$this->orcamentosM->getOrcamentoDesconto(base64_decode($id));
		$parametros['solicitante']	=	$this->orcamentosM->getSolicitante(base64_decode($id));
		$parametros['indicador']	= 	$this->orcamentosM->buscaIndicador(base64_decode($id));
		$parametros['bombas'] 		= 	$this->produtosM->select();
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['title']		=	"Orçamento #".$id;		
		$this->_load_view('area-indicadores3/orcamentos/visualiza-orcamento-ind3',$parametros);
	}


	public function emitirOrcamento()
	{
			
			$status_orcamento_id = $_POST['status_orcamento_id'];
			

			//altera o status para emitido	
			$dados = array('id'						=>	$_POST['orcamento_id'],
				   		   'status_orcamento_id'	=>	$status_orcamento_id);

		if($this->orcamentosM->atualizaStatusOrcamento($dados)){

			//orçamento entregue/emitido
			$andamentoOrcamento = array('andamento' 			=> 'Orçamento emitido ao cliente para o email '.$_POST['email_destino'].', por '.$this->session->userdata('nome').'',
		 								'dthr_andamento'		=> date('Y-m-d H:i:s'),
		 								'orcamento_id'			=> $_POST['orcamento_id'],
		 								'status_orcamento_id'	=> $status_orcamento_id,
		 								'usuario_id'			=> $this->session->userdata('usuario_id') );	
	 		if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento)){

				$retorno = $this->enviaEmailEmissaoCliente($_POST['email_destino'],$_POST['orcamento_id'],  $_POST['valor_tributo'], $_POST['valor_total'],' ');
				if($retorno){
					echo json_encode(array('retorno' => 'sucesso'));
				}else{
					echo json_encode(array('retorno' => 'erro'));	
				}
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}	
		
	}

	private function enviaEmailEmissaoCliente( $email_destino, $orcamento_id, $valor_tributo, $valor_total, $contato_posto )
	{
		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);
		
		$anexo = $this->geraOrcamento($orcamento_id,'email',$contato_posto);

		$email = '<html>
		<head></head>
		<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
		<div class="content" style="width: 600px; height: 100%;">
		<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/backgroundCabecalho.png); height: 528px; "></div>
		<div style="width: 600px; background-color: #ffcc00; height: 100%">
			<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
				<tbody>
					<tr>
						<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
						<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
						<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
						<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
					</tr>
					<tr>
						<td style="padding-bottom: 18px;font-weight: 700;">Validade:</td>
						<td style="padding-bottom: 18px;">30 Dias</td>
						<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
						<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
					</tr>
					<tr>
						<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
						<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
						<td style=" font-weight: 700; text-align: right;">Telefone:</td>
						<td>'.$empresa['telefone'].'</td>
					</tr>
					<tr>
						<td style=" font-weight: 700;">E-mail:</td>
						<td colspan="3">'.$empresa['email'].'</td>
					</tr>
				</tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
			<table style="width: 570px;margin-left: 14px;" cellspacing="0">
				<thead style="height: 55px;">
					<tr><th style="    height: 30px;">Modelo</th>
					<th>Descrição</th>
					<th>Quantidade</th>
					<th>Valor</th>
				</tr></thead>
				<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
				$total = 0; foreach($produtos as $produto){
					
					$email.='	<tr>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
								<td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>
								<td style="   text-align: right; padding: 10px;">R$ '.number_format($produto['valor'], 2, ',', '.').'</td>
							</tr>';					
					$total = $total + ($produto['valor'] * $produto['qtd']);
				}							
		$email.='	<tr>
						<td colspan="3" style="border-top: 1px solid #f0f0f0; border-right: 1px solid #f0f0f0;    padding: 10px;text-align: right;">
						<b>total</b></td>
						<td style="border-top: 1px solid #f0f0f0; text-align: right; padding: 10px;"><b>R$ '.number_format($total, 2, ',', '.').'</b></td>
					</tr>';								
		$email.='		
				</tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
		</div>
	</div>
</body>
</html>';	
		
		$this->load->library('email');
		$result = $this->email
		    ->from('webmaster@companytec.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to($email_destino)
		    ->subject('Orçamento Wertco')
		    ->message($email)
		    ->attach('./pdf/'.$anexo)
		    ->send();

		return $result;		
	
	}

	/************************************************************ 
	************** GERA PDF DO ORÇAMENTO SOLICITADO *************
	*************************************************************/
	public function geraOrcamento($orcamento_id, $origem_solicitacao,$contato)
    {
    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-'.$orcamento_id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $dados				=	$this->orcamentosM->getOrcamentoProdutos($orcamento_id);
	    $total_descontos 	= 	$this->orcamentosM->getOrcamentoDesconto($orcamento_id);
	    $responsavel 		= 	$this->orcamentosM->getResponsavel($orcamento_id);
	    $orcamento 			= 	$this->orcamentosM->getFreteEntregaPagto($orcamento_id);
	    $indicador			= 	$this->orcamentosM->getIndicador($orcamento_id);
	    $data['frete']		= 	(is_object($orcamento)) ? $orcamento->frete : ' ';
	    $data['entrega']	= 	(is_object($orcamento)) ? $orcamento->entrega : ' ';
	    $data['formaPagto']	= 	(is_object($orcamento)) ? $orcamento->forma_pagto : ' ';

	    $data['empresa'] = array(	'razao_social' 			=> 	$dados[0]->razao_social,
    								'fantasia' 				=> 	$dados[0]->fantasia,
									'cnpj' 					=> 	$dados[0]->cnpj,
									'telefone'				=> 	$dados[0]->telefone,
									'email'					=> 	$dados[0]->email,
									'endereco'				=> 	$dados[0]->endereco,
									'cidade'				=> 	$dados[0]->cidade,		
									'pais'					=> 	$dados[0]->pais,
									'estado'				=> 	$dados[0]->estado,
									'orcamento_id'			=> 	$dados[0]->orcamento_id,
									'emissao'				=> 	$dados[0]->emissao,
									'valor_tributo'			=>	$dados[0]->valor_tributo,
									'representante_legal' 	=> 	$dados[0]->representante_legal,
									'inscricao_estadual' 	=> 	$dados[0]->insc_estadual,
									'bairro'				=> 	$dados[0]->bairro,
									'cep'					=> 	$dados[0]->cep,
									'observacao'			=> 	$dados[0]->observacao );

	    foreach ($dados as $dado) {
	    	$data['produtos'][] = array('codigo' 			=> 	$dado->codigo,
	    								'descricao'			=>	$dado->descricao,
										'modelo'			=>	$dado->modelo,
	    								'qtd'				=>	$dado->qtd,
	    								'valor_unitario' 	=>	$dado->valor_unitario,
	    								'valor_orcamento'	=> 	$dado->valor_orcamento,
	    								'valor_produto' 	=> 	$dado->valor);
	    }		

	    $data['info']  	= 	array(	'nome' 			=> 	$this->session->userdata('nome'),
	    						 	'email'			=> 	$this->session->userdata('email'),
	    						 	'telefone'		=>	$this->session->userdata('telefone'),	    						 	
	    						 	'contato'		=> 	( isset($_POST['contato'])) ? $_POST['contato'] : $contato );

	    $data['representante'] 	= 	array(	'responsavel' 	=>  $responsavel->responsavel,
								  			'empresa' 		=>	$responsavel->razao_social);

	    foreach( $total_descontos as $total_desconto ){
	    	$data['desconto'] = array('valor_desconto' => $total_desconto['valor_desconto'] );
	    }

	    if(!isset($data['desconto'])){
	    	$data['desconto'] = array('valor_desconto' => '' );
	    }

	    $data['indicador'] = array('indicador' 	=> 	(count($indicador) > 0) ? $indicador[0]->indicador : '');	    

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-indicadores3/orcamentos/orcamento-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'orcamento-'.$orcamento_id.'.pdf'));
	    	}else{
	    		return $retorno['retorno'] = 'orcamento-'.$orcamento_id.'.pdf';
	    	}
	    }	    
	    
    }

    public function cadastraOrcamentos()
	{
		$parametros 				= 	$this->session->userdata();
		$parametros['dados']		=	$this->orcamentosM->buscaOrcamentosIndicadores($parametros['usuario_id']);		
		$parametros['bombas'] 		= 	$this->produtosM->select();		
		$parametros['fretes'] 		= 	$this->fretesM->selectFretes();
		$parametros['entregas']		= 	$this->entregasM->selectEntregas();
		$parametros['formaPagto']	= 	$this->formaPagtoM->selectFormaPagto();
		$parametros['title']		=	"Orçamentos";
		$this->_load_view('area-indicadores3/orcamentos/cadastra-orcamento-ind3',$parametros);	
	}

	public function orcamentosInd3()
	{
		if( $this->input->post() )
		{
			
			if( $this->input->post('salvar') == '1')
			{			
				$dadosEmpresa = array(
			        'razao_social' 			=>	$this->input->post('razao_social'),
					'fantasia' 				=>	$this->input->post('fantasia'),
					'cnpj' 					=>	$this->input->post('cnpj'),
					'telefone' 				=>	$this->input->post('telefone'),
					'endereco' 				=>	$this->input->post('endereco'),
					'email' 				=>	$this->input->post('email'),				
					'cidade' 				=>	$this->input->post('cidade'),
					'estado'				=>	$this->input->post('estado'),
					'pais'					=>	$this->input->post('pais'),	
					'bairro'				=>	$this->input->post('bairro'),
					'cep'					=>	$this->input->post('cep'),
					'insc_estadual' 		=>	$this->input->post('insc_estadual'),
					'tipo_cadastro_id'		=>	1,
					'cartao_cnpj'			=>	$this->input->post('cartao_cnpj')			    
				);			

				if($this->empresasM->add($dadosEmpresa))
				{		
					$empresa_id = $this->db->insert_id();			

				}
	 		}else{
	 			$empresa_id = $this->input->post('empresa_id');
	 		}

	 		$dadosOrcamento = array('empresa_id' 			=> 	$empresa_id,
	 								'status_orcamento_id'	=> 	1,
	 								'origem'				=> 	'Representante',
	 								'solicitante_id'		=> 	$this->session->userdata('usuario_id'),
	 								'usuario_id'			=> 	$this->session->userdata('usuario_id'),
	 								'indicador_id'			=> 	$this->session->userdata('usuario_id'),
	 								'frete_id'				=> 	$this->input->post('frete'),
	 								'entrega_id'			=> 	$this->input->post('entrega'),
	 								'forma_pagto_id'		=> 	$this->input->post('forma_pagto'),
									'contato_posto'			=> 	$this->input->post('contato_posto'));

	 		if( $this->orcamentosM->insereOrcamento($dadosOrcamento) )
	 		{
	 			$orcamento_id  = $this->db->insert_id();
	 			$erro = 0;

	 			$dadosContato = array(	'empresa_id' 		=> 	$empresa_id,
	 									'nome'				=> 	$this->input->post('contato_posto'),
	 									'email'				=> 	$this->input->post('email_contato_posto'),
	 									'celular'			=> 	$this->input->post('tel_contato_posto'),
	 									'tipo_cadastro_id'	=> 1);

	 			if( $this->usuariosM->add($dadosContato) )
	 			{

	 				$contato_id = $this->db->insert_id();
	 				$updateContato = array(	'id' 			=> 	$orcamento_id,
	 										'contato_id'	=> 	$contato_id	);
	 				$this->orcamentosM->atualizaOrcamento($updateContato);

		 			$andamentoOrcamento = array('andamento' 			=> 'Orçamento realizado pelo Representante '.$this->session->userdata('nome').' !',
			 									'dthr_andamento'		=> date('Y-m-d H:i:s'),
			 									'orcamento_id'			=> $orcamento_id,
			 									'status_orcamento_id'	=> 1,
			 									'usuario_id'			=> $this->session->userdata('usuario_id')
			 									);	

		 			$comissao  = array(	'orcamento_id' 		=> 	$orcamento_id,
		 								'valor_desconto' 	=> 	$this->input->post('valor_desconto_orc'),
		 								'motivo_desconto'	=> 	'Comissão Indicador' );

		 			if(	$this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento) && $this->orcamentosM->insereOrcamentosDesconto($comissao) )
		 			{
		 				$erro = $erro;
		 				$orcamentoResp = array(	'orcamento_id' 	=> 	$orcamento_id,
			 									'usuario_id' 	=> 	$this->session->userdata('usuario_id') );
	 					if($this->orcamentosM->insereResponsavelOrcamento($orcamentoResp))
	 					{
	 						$erro = $erro;
	 					
				 			for ($i=0; $i < count($this->input->post('produto')) ; $i++) 	 			  
				 			{ 
				 				$orcamentoProdutos = array(	'orcamento_id' 	=>	$orcamento_id,
				 											'produto_id'  	=> 	$this->input->post('produto')[$i],
				 											'qtd'			=>	$this->input->post('quantidade')[$i],
				 											'valor'			=> 	$this->input->post('valor')[$i]	);	

				 				if($this->orcamentosM->insereOrcamentoProdutos($orcamentoProdutos))
				 				{
				 					$erro = $erro;
				 					
								}else{
									$erro++;
								}		 					 			
				 			}
				 		}else{
							$erro++;
						}	 					

					}else{
						$erro++;
					}
				}else{
					$erro++;
				}
			}else{
				$erro++;
			}

	 		if( $erro == 0 ){
	 				
	 			if($this->enviarEmailOrcamento($orcamento_id))
 				{
 					$this->session->set_flashdata('sucesso', 'ok.');
 				}else{
 					
 					$this->session->set_flashdata('erro', 'erro.');	
 				}
 			}else{
 				$this->session->set_flashdata('erro', 'erro.');
 			}

			$this->orcamentos();
		}

	}

	public function insereDescontoOrcamento()
	{
		$dados_desconto = array('orcamento_id' 		=> 	$_POST['orcamento_id'],
								'valor_desconto'	=>	str_replace(',', '.', $_POST['valor_desconto']));

		if( $this->orcamentosM->insereOrcamentosDesconto( $dados_desconto ) )
		{
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}

	}		

	public function alterarValorProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['id'],
							 	'valor'	=>	(strpos($_POST['valor'],',') === false ) ? $_POST['valor'] : str_replace(',','.', str_replace('.','',$_POST['valor'])) );

		if($this->orcamentosM->atualizaValorProduto($dadosUpdate)){
			if( isset($_POST['comissao']) ){

				$comissao = array('orcamento_id' 	=> $_POST['orcamento_id'],
								  'valor_desconto'	=> $_POST['comissao']);

				$this->orcamentosM->atualizaComissao($comissao);

			}
			echo json_encode(array('retorno' => 'sucesso' ));						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}

	}

	public function alterarProdutoOrcamento(){

		$dadosUpdate = array(	'id' 				=> 	$_POST['orcamento_produto_id'],
							 	'produto_id'		=>	$_POST['produto_id'],
							 	'valor' 			=>	$_POST['valor_produto']	);

		if($this->orcamentosM->alterarProdutoOrcamentoInd3($dadosUpdate)){
			
			echo json_encode(array('retorno' => 'sucesso' ));						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarQtdProduto(){

		$dadosUpdate = array(	'id' 	=> 	$_POST['orcamento_produto_id'],
							 	'qtd'	=>	$_POST['qtd']	);

		if($this->orcamentosM->alterarQtdProduto($dadosUpdate)){
			
			echo json_encode(array('retorno' => 'sucesso' ));						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	private function enviarEmailOrcamento($orcamento_id){

		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);		
		$email = '<html>
		<head></head>
		<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
		<div class="content" style="width: 600px; height: 100%;">
		<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/backgroundCabecalho.png); height: 528px; "></div>
		<div style="width: 600px; background-color: #ffcc00; height: 100%">
			<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
				<tbody>
					<tr>
						<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
						<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
						<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
						<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
					</tr>
					<tr>
						<td style="padding-bottom: 18px;font-weight: 700;">Validade:</td>
						<td style="padding-bottom: 18px;">30 Dias</td>
						<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
						<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
					</tr>
					<tr>
						<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
						<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
						<td style=" font-weight: 700; text-align: right;">Telefone:</td>
						<td>'.$empresa['telefone'].'</td>
					</tr>
					<tr>
						<td style=" font-weight: 700;">E-mail:</td>
						<td colspan="3">'.$empresa['email'].'</td>
					</tr>
				</tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
			<table style="width: 570px;margin-left: 14px;" cellspacing="0">
				<thead style="height: 55px;">
					<tr><th style="    height: 30px;">Modelo</th>
					<th>Descrição</th>
					<th>Quantidade</th>					
				</tr></thead>
				<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
				$total = 0; foreach($produtos as $produto){
					
					$email.='	<tr>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
								<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
								<td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>						
							</tr>';					
					$total = $total + ($produto['valor'] * $produto['qtd']);
				}			
		
			$email.=' </tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
		</div>
	</div>
</body>
</html>';	

		$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to('vendas@wertco.com.br')
		    ->subject('Orçamento realizado no Sistema por Indicadore nível 3')
		    ->message($email)
		    ->send();

		return $result;		
	}

   	public function verificaEstadoIcms()
	{
		$dados = $this->icmsM->getIcmsPorEstado($_POST['estado_id']);
		if( $dados[0]['total'] > 0 ){
			echo json_encode(array('retorno' => 'erro'));
		}else{
			echo json_encode(array('retorno' => 'sucesso'));
		}

	}

	public function insereProdutosNovosOrcamento(){
		$erro = 0;
		
		foreach ($_POST['dados'] as $dados) {
			if( $dados['name'] == 'produto_id'){
				$insert['produto_id'] = $dados['value'];
				$i=1;
			}
			if( $dados['name'] == 'qtd'){
				$insert['qtd'] = $dados['value'];
				$i=2;
			}
			if( $dados['name'] == 'valor_unitarios'){
				$insert['valor'] = $dados['value'];
				$i=3;
				$insert['orcamento_id'] = $_POST['orcamento_id'];
				if($this->orcamentosM->insereOrcamentoProdutos($insert)){
					$erro = $erro;
				}else{
					$erro++;
				}
			}			
		}

		if($erro == 0){
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Produto Adicionado ao orçamento por '.$this->session->userdata('nome'),
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
			
			$this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento);
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	/****************************************************************************
	**************** Método Ajax - Excluir Icms ******************************
	*****************************************************************************/
	public function excluirProdutoOrcamento()
	{
		if($this->orcamentosM->excluirProduto($_POST['orcamento_produto_id'])){
			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function alterarFormaPagto()
	{
		$dadosUpdate = array(	'id' 				=> 	$_POST['orcamento_id'],
							 	'forma_pagto_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarFormaPagto($dadosUpdate)){
			
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de Pagamento alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarEntrega(){
		$dadosUpdate = array(	'id' 			=> 	$_POST['orcamento_id'],
							 	'entrega_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarEntrega($dadosUpdate)){
			
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Forma de entrega alterada.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function alterarFrete(){
		
		$dadosUpdate = array(	'id' 		=> 	$_POST['orcamento_id'],
							 	'frete_id'	=>	$_POST['valor']	);

		if($this->orcamentosM->alterarFrete($dadosUpdate)){
			
			$insereAndamentoOrcamento 	= 	array(	'orcamento_id' 			=> 	$_POST['orcamento_id'],
													'andamento'				=> 	'Frete alterado.',
													'dthr_andamento'		=> 	date('Y-m-d H:i:s'),
													'usuario_id'			=> 	$this->session->userdata('usuario_id'),
													'status_orcamento_id'	=> 	$_POST['status_orcamento_id']);
		
			if($this->orcamentosM->insereOrcamentoAndamentos($insereAndamentoOrcamento))
			{	
				echo json_encode(array('retorno' => 'sucesso' ));						 	
			}else{
				echo json_encode(array('retorno' => 'erro' ));						
			}						 	
		}else{

			echo json_encode(array('retorno' => 'erro' ));		
		}		
	}

	public function verificaIcms()
	{
		$dados = $this->icmsM->getIcmsPorUF($_POST['estado']);
		echo json_encode(array('fator' => $dados[0]->fator));

	}

	public function retornaIndicadores()
	{
		
		$rows = $this->usuariosM->retornaIndicadores($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label' 	=>	$row['label'],
								 'id'		=>	$row['id'],
								 'value'	=>	$row['label'] );
				
			}

			echo json_encode($dados);
		}
	}

	public function ValidaCnpj(){
    	$cnpj 			= 	$this->input->post('cnpj');		
		$consulta_rf 	= 	$this->ConsultaCNPJRF($cnpj);

        if (isset($consulta_rf) && $consulta_rf->{'code'} == '1')
        {
            if ($consulta_rf->{'data'}->{'situacao_cadastral'} == 'ATIVA')
            {
            	$arr['retorno'] 	= 	$consulta_rf->{'data'};
            	$arr['save'] 		= 	$consulta_rf->{'Save_Receita'};
                $arr['result'] 		= 	true;
                $arr['situacao'] 	= 	$consulta_rf->{'data'}->{'situacao_cadastral'};
            	//echo json_encode($consulta_rf);            	
                
            	$arr['result'] = true;
				$arr['erro'] = 0;

            }else {
            	$arr['erro'] 	= 1;
            	$arr['msg'] 	= '* Situação cadastral '.$consulta_rf->{'data'}->{'situacao_cadastral'}.' na Receita Federal.'; 
            }
		}else {

            if (isset($consulta_rf)){
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 2 - '.$consulta_rf->{'code'}.')';
            }else{
            	$arr['erro'] = 1;
            	$arr['msg'] = '* Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 3)';
            }

        }

		echo json_encode(array(	'receita' 	=> $arr));
	}
	
}