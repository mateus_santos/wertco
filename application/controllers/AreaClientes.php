<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AreaClientes extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta		
		if( !$this->_is_logged() || $this->session->userdata('tipo_acesso') != 'clientes'){
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('ProdutosModel', 'produtosM');
		$this->load->model('OrcamentosModel', 'orcamentosM');
		$this->load->model('ChamadoModel' , 'chamadoM');
		$this->load->model('AtividadeModel' , 'atividadeM');
		$this->load->model('SubAtividadeModel' , 'subatividadeM');
		$this->load->model('ChamadoDefeitoModel' , 'defeitoM');
		$this->load->model('ChamadoCausaModel' , 'causaM');
		$this->load->model('ChamadoSolucaoModel' , 'solucaoM');
		$this->load->model('AtividadeCausaSolucaoModel' , 'causa_solucaoM');
		$this->load->model('ConfiguracaoModel' , 'configuracaoM');
		$this->load->model('ConfiguracaoBombaModel' , 'configuracao_bombaM');
		$this->load->model('ConfiguracaoConcentradorModel' , 'configuracao_concentradorM');
		$this->load->model('ConfiguracaoIdentificadorModel' , 'configuracao_identificadorM');
		$this->load->helper('form');
	}
 
	public function index()
	{		
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();		
		$parametros['title']	=	"Área do Cliente";
		$this->_load_view('area-clientes/index',$parametros);
	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}
			
			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-clientes/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-clientes/editar-cliente',	$parametros );	
		}
	}

	public function orcamentosCli()
	{
		if( $this->input->post() )
		{
			
			$verifica = $this->orcamentosM->verificaOrcamentoCliente($this->session->userdata('empresa_id'));			

			if( $verifica->total > 0 ){
				
				$this->session->set_flashdata('Atenção', 'Já encontra-se um orçamento aberto para o seu cnpj, entre em contato com o representante da sua região, ou com o setor de vendas da WERTCO.');

				redirect('AreaClientes/orcamentosCli/',$parametros);

			}else{
			
		 		$empresa_id = $this->input->post('empresa_id');	 		

		 		$dadosOrcamento = array('empresa_id' 			=> 	$empresa_id,
		 								'status_orcamento_id'	=> 	1,
		 								'origem'				=> 	'Cliente',
		 								'usuario_id'			=> 	$this->session->userdata('usuario_id'),
		 								'solicitante_id'		=> 	$this->session->userdata('usuario_id'));

		 		if( $this->orcamentosM->insereOrcamento($dadosOrcamento) )
		 		{
		 			$orcamento_id  = $this->db->insert_id();
		 			$erro = 0;
		 			$andamentoOrcamento = array('andamento' 			=> 'Orçamento realizado pelo cliente '.$this->session->userdata('nome').' da empresa código: '.$empresa_id.'!',
			 									'dthr_andamento'		=> date('Y-m-d H:i:s'),
			 									'orcamento_id'			=> $orcamento_id,
			 									'status_orcamento_id'	=> 1
			 									);	
		 			if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento))
		 			{	 				
		 				$erro = $erro;
		 				$orcamentoResp = array(	'orcamento_id' 	=> 	$orcamento_id,
			 											'usuario_id' 	=> 	$this->session->userdata('usuario_id') );
	 					if($this->orcamentosM->insereResponsavelOrcamento($orcamentoResp))
	 					{
	 						$erro = $erro;
	 					
				 			for ($i=0; $i < count($this->input->post('produto')) ; $i++) 	 			  
				 			{ 
				 				$orcamentoProdutos = array(	'orcamento_id' 	=>	$orcamento_id,
				 											'produto_id'  	=> 	$this->input->post('produto')[$i],
				 											'qtd'			=>	$this->input->post('quantidade')[$i]);	

				 				if($this->orcamentosM->insereOrcamentoProdutos($orcamentoProdutos))
				 				{
				 					$erro = $erro;
				 					
								}else{
									$erro++;
								}		 					 			
				 			}
				 		}else{
							$erro++;
						}	 					

					}else{
						$erro++;
					}
				}else{
					$erro++;
				}

		 		if( $erro == 0){	 				
		 				
		 			if($this->enviarEmailOrcamento($orcamento_id))
	 				{
	 					$this->session->set_flashdata('sucesso', 'ok.');
	 				}else{
	 					die;
	 					$this->session->set_flashdata('erro', 'erro.');	
	 				}
	 			}else{
	 				$this->session->set_flashdata('erro', 'erro.');
	 			}			
			}
		}
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->orcamentosM->buscaOrcamentosRepresentantes($parametros['usuario_id']);
		$parametros['title']	=	"Orçamentos";		
		$this->_load_view('area-clientes/orcamentos/orcamento-cli',$parametros);

	}

	public function cadastraOrcamentos()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->orcamentosM->buscaOrcamentosRepresentantes($parametros['usuario_id']);
		$parametros['usuario'] = 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
		$parametros['bombas'] 	= 	$this->produtosM->select();
		$parametros['title']	=	"Orçamentos";
		$this->_load_view('area-clientes/orcamentos/cadastra-orcamento-cli',$parametros);
	}

	public function buscaAndamentos()
	{

		$retorno = $this->orcamentosM->buscaAndamentos($_POST['orcamento_id']);			
		echo json_encode($retorno);		
	}
	
	public function alteraStatusOrcamento()
	{
		$dados = array('id'						=>	$_POST['orcamento_id'],
					   'status_orcamento_id'	=>	$_POST['status_orcamento']);

		if($this->orcamentosM->atualizaStatusOrcamento($dados))
		{

			switch($_POST['status_orcamento']){
				case 1:
			        $andamento = "Orçamento Aberto!";
			        break;
			    case 2:
			        $andamento = "Orçamento Fechado, venda realizada!";
			        break;
			    case 3:
			        $andamento = "Orçamento Perdido para concorrência.";
			        break;
			    case 4:
			        $andamento = "Orçamento Cancelado.";
			        break;
			    case 5:
			        $andamento = "Orçamento entregue ao cliente.";
			        break;        
			    case 6:
			        $andamento = "Orçamento em negociação.";
			        break;        
			}

			$inserirAndamento = array('orcamento_id'			=>	$_POST['orcamento_id'],
										'status_orcamento_id'	=> 	$_POST['status_orcamento'],
										'andamento'				=>	$andamento,
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id'));

			if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
			{
				echo json_encode(array('retorno' => 'sucesso'));	
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function insereAndamentoOrcamento()
	{ 
		$inserirAndamento = array('orcamento_id'		=>	$_POST['orcamento_id'],
								'status_orcamento_id'	=> 	$_POST['status_orcamento_id'],
								'andamento'				=>	$_POST['andamento'],
								'dthr_andamento'		=>	date('Y-m-d H:i:s'),
								'usuario_id'			=> 	$this->session->userdata('usuario_id'));

		if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
		{
			echo json_encode(array('retorno' => 'sucesso'));	
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}			

	}
	
	public function visualizarOrcamento($id)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->orcamentosM->getOrcamentoProdutos($id);		
		$parametros['bombas'] 	= 	$this->produtosM->select();
		$parametros['title']	=	"Orçamento #".$id;		
		$this->_load_view('area-clientes/orcamentos/visualiza-orcamento-cli',$parametros);
	}


	/************************************************************ 
	************** GERA PDF DO ORÇAMENTO SOLICITADO *************
	*************************************************************/
	public function geraOrcamento()
    {    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-'.$_POST['orcamento_id'].'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $dados	=	$this->orcamentosM->getOrcamentoProdutos($_POST['orcamento_id']);

	    $data['empresa'] = array('razao_social' 	=> 	$dados[0]->razao_social,
	    							'fantasia' 		=> 	$dados[0]->fantasia,
									'cnpj' 			=> 	$dados[0]->cnpj,
									'telefone'		=> 	$dados[0]->telefone,
									'email'			=> 	$dados[0]->email,
									'endereco'		=> 	$dados[0]->endereco,
									'cidade'		=> 	$dados[0]->cidade,		
									'pais'			=> 	$dados[0]->pais,
									'estado'		=> 	$dados[0]->estado,
									'orcamento_id'	=> 	$dados[0]->orcamento_id,
									'emissao'		=> 	$dados[0]->emissao	);

	    foreach ($dados as $dado) {
	    	$data['produtos'][] = array('codigo' 			=> 	$dado->codigo,
	    								'descricao'			=>	$dado->descricao,
	    								'modelo'			=>	$dado->modelo,
	    								'qtd'				=>	$dado->qtd,
	    								'valor_unitario' 	=>	$dado->valor_unitario);
	    }		

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-clientes/orcamentos/orcamento-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	echo json_encode(array('retorno' => 'orcamento-'.$_POST['orcamento_id'].'.pdf'));
	    }	    
	    
    }

    private function enviarEmailOrcamento($orcamento_id){
		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);		
		$email 		= 	"<html>
								<body style='width: 100%;height: 100%;font-family: sans-serif;color: #fff;'>
									<div style='margin: 0 auto;width: 710px;padding: 20px;border: 15px solid #ffcc00;box-shadow: 1px 1px 110px 3px #999;background: #000;color: #fff;'>	
										<div style='text-align: center;'>
											<h1>Solicitação de Orçamento</h1>	
											<p>Razão Social: ".$empresa['razao_social']." | Cnpj: ".$empresa['cnpj']." | Validade: 30 dias <br/> Emissão: ".$date->format('d/m/Y H:i:s')." | Telefone: ".$empresa['telefone']." | Email: ".$empresa['email']."</p>
										</div>	
										<div style='text-align: center;'>
											<h2 style='text-align: center;'>Produtos</h2>	
											<table style='border: 1px solid #f0f0f0;padding: 20px;margin:0 auto;background: #333;'>
												<thead>
													<tr>
														<td>Modelo</td>
														<td>Descrição</td>
														<td>Qtd.</td>			
													</tr>	
												</thead>
												<tbody>";
		foreach($produtos as $produto){												
		$email.="										
													<tr>
														<td style='padding-top: 5px;width: 15%;'>".$produto['codigo']."</td>
														<td style='padding-top: 5px;width: 80%;'>".$produto['descricao']."</td>
														<td style='padding-top: 5px;width: 5%;text-align: center;'>".$produto['qtd']."</td>					
													</tr>";

		}											
		$email.="								</tbody>
												
											</table>
											<h2 style='text-align: center;'>Atenção</h2>
											<h3>A diferença de aliquota do icms, devida ao estado destino, é de responsabilidade do cliente.</h3>

										</div>
										<img src='http://www.wertco.com.br/wertcofundoescuro.png' style='width: 25%;padding-top: 40px;'>
									</div>
								</body>
							</html>";

		$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
		    ->to('webmaster@companytec.com.br')
		    ->subject('Novo Orçamento solicitado pelo Cliente: '. $this->session->userdata('nome'))
		    ->message($email)
		    ->send();

		return $result;		
	}

	public function chamados()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->chamadoM->selectPorUsuario($this->session->userdata('usuario_id'));	
		$parametros['title']	=	"Chamados";
		$this->_load_view('area-clientes/assistencia-tecnica/chamados-cliente',$parametros);
	}

	public function cadastraChamado()
	{
		$parametros 				= 	$this->session->userdata();
		$parametros['tipos']		=	$this->chamadoM->selectTipos();
		$parametros['prioridades']	=	$this->chamadoM->selectPrioridades();
		$parametros['cliente']		=	$this->empresasM->getEmpresa($this->session->userdata('empresa_id'));

		$parametros['title']		=	"Cadastro de Chamado de Assistência Técnica";
		$this->_load_view('area-clientes/assistencia-tecnica/cadastra-chamado-cliente',$parametros);
	}

	public function inserirChamado()
	{
		$chamado = $this->input->post();
		$chamado['status_id'] = 10;
		unset($chamado['razao_social']);
		unset($chamado['nome_solicitante']);
		if(isset($chamado['cliente_id'])){			
			if($chamado['cliente_id'] != '' && $chamado['contato_id'] != ''){
				if($id = $this->chamadoM->insert($chamado)){

					$this->load->library('email');
					$email = '<html>
					<head></head>
					<body style="" width: 650px; position:absolute;">

						<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
						<img src="http://www.wertco.com.br/bootstrap/assets/demo/default/media/img/logo/wertcofundoescuro.png" /><br/>
							<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">🔧 Abertura de Chamado: '.$id.' 🔧</h2>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">							
							<b>Cliente:</b> 	'.	$this->input->post('razao_social').'<br/>
							<b>Solicitante:</b> '.$this->input->post('nome_solicitante').'<br/>
							<b>Descrição / Observação:</b> '.	nl2br($chamado['descricao']).'<br/>														
							</p>
						</div>
					</body>
				</html>';
					$result = $this->email
						    ->from('webmaster@wertco.com.br')
						    ->reply_to('webmaster@companytec.com.br')    // Optional, an account where a human being reads.
						    ->to('suporte@wertco.com.br')
						    ->subject('🔧 Chamado '.$id.' Criado pelo Cliente: '. $this->session->userdata('nome').' 🔧')
						    ->message($email)
						    ->send();

					

					$this->session->set_flashdata('retorno', 'sucesso');
					$this->chamados();
				}else{
					$this->session->set_flashdata('retorno', 'erro');
					$this->chamados();
				}
			}
		}
		
	}

	public function retornaClienteWS()
	{
		$rows = $this->usuariosM->retornaClientes($_GET['term']);
		if(count($rows) > 0){
			echo json_encode($rows);
		}
	}	

	public function geraChamadoPdf($id, $origem_solicitacao){
		 //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('chamado-'.$id.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $data['chamados'] 	= 	$this->chamadoM->buscaChamadosAtividadesById($id);
	    $data['ops'] 		= 	$this->chamadoM->buscaOrdensDePagamento($id);
	    $data['tecnicos'] 	= 	$this->chamadoM->buscaTecnicosChamados($id);

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-clientes/assistencia-tecnica/chamado-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	if($origem_solicitacao == 'ajax'){
	    		echo json_encode(array('retorno' => 'chamado-'.$id.'.pdf'));
	    	}else{
	    		//return $retorno['retorno'] = 'chamado-'.$id.'.pdf';
	    		redirect(base_url('pdf/chamado-'.$id.'.pdf'));
	    	}
	    }	   	
	}




}