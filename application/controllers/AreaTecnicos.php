<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AreaTecnicos extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta		
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'tecnicos' && $this->session->userdata('tipo_acesso') != 'administrador tecnico' && $this->session->userdata('tipo_acesso') != 'administrador geral' && $this->session->userdata('tipo_acesso') != 'assistencia tecnica' && $this->session->userdata('tipo_acesso') != 'qualidade')  ){
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('DownloadsModel', 'downloadsM');
		$this->load->model('CartaoTecnicosModel', 'cartaoM');
		$this->load->model('ProdutosModel', 'produtosM');
		$this->load->model('OrcamentosModel', 'orcamentosM');
		$this->load->model('ZonaAtuacaoModel' , 'zonaM');		
		$this->load->helper('form');
	}
 
	public function index()
	{		
		
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	= 	$this->empresasM->empresaUsuario($parametros['usuario_id']);
		$parametros['title']	=	"Área do Técnico";
		$this->_load_view('area-tecnicos/index',$parametros);
	}

	public function contrato()
	{		
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	= 	$this->empresasM->empresaUsuario($parametros['usuario_id']);
		$parametros['title']	=	"Área do Técnico - Contrato de Prestação de Serviço";		

		$this->_load_view('area-tecnicos/contrato',$parametros);
	}

	public function geradorJson()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['title']	=	"Gerador de Json";
		$this->_load_view('area-tecnicos/json/json', $parametros);
	}

	public function geradorJsonRtm()
	{
		$parametros 			= 	$this->session->userdata();		
		$parametros['title']	=	"Gerador de Json";
		$this->_load_view('area-tecnicos/json/json2',$parametros);
	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha'))); 
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados']				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-tecnicos/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-tecnicos/editar-cliente',	$parametros );	
		}
	}

	public function downloads()
	{		
		$parametros 			= 	$this->session->userdata();		
		$parametros['title']	=	"Downloads";
		$parametros['dados']	=	$this->downloadsM->getDownloadsPorArea('tecnicos');
		$this->_load_view('area-tecnicos/downloads/downloads-tecnicos',$parametros);
	}

	public function atualizaValidadeCartao()
	{	
		$parametros 			= 	$this->session->userdata();			
		$parametros['dados']	=	$this->cartaoM->getDadosCartao($this->session->userdata('usuario_id'));						
		$parametros['title']	=	"Atualiza Cartão Técnico";

		$data_atual 					=	new DateTime(date('Y-m-d'));				
		$ultima_atualizacao 			= 	new DateTime($parametros['dados']->ultima_atualizacao);		 	 	
		$parametros['diferenca_dias']	=	$data_atual->diff($ultima_atualizacao);
		$nova_data = '+'.$parametros['dados']->periodo_renovacao.' month';
		//echo date('Y-m-d') .'>='. date('Y-m-d',strtotime($parametros['dados']->ultima_atualizacao));die;
		if(date('Y-m-d') >= date('Y-m-d',strtotime($parametros['dados']->ultima_atualizacao))) {
			$parametros['dados']->data_renovacao_vencimento = 'MAIOR';
		}else{
			$parametros['dados']->data_renovacao_vencimento = 'MENOR';
		}

		$this->_load_view('area-tecnicos/cartao-tecnicos/atualiza-cartao',$parametros);

	}

	public function atualizaCartaoTecnico(){
		
		$data = explode('/',$_POST['ultima_atualizacao']);		

		$dados = array(	'id'					=>	$_POST['id'],
						'tag'					=>	$_POST['tag'],
						'ultima_atualizacao'	=>	'20'.$data['2'].'-'.$data['1'].'-'.$data['0'],
						'ultima_senha'			=>	$_POST['senha'] );

		if($this->cartaoM->atualizaCartao($dados)){

			$dadosLog = array( 'usuario_id' 		=> $this->session->userdata('usuario_id'),
							   'cartao_tecnico_id' 	=> 	$_POST['id']);
			
			$this->cartaoM->insereLogCartao($dadosLog);

			echo json_encode(array('retorno' => 'sucesso'));
		}else{

			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function solicitaCartaoTecnico()
	{
		if($this->input->post('salvar') == 1){
			
			$dados = array(	'solicitante_id' 	=>	$this->session->userdata('usuario_id'),
							'situacao'			=> 	'Solicitação realizada',
							'empresa_id' 		=>	$this->input->post('empresa_id'));

			if($this->cartaoM->insereSolicitacaoCartao($dados)){
				$empresa 	= $this->empresasM->empresaUsuario($this->session->userdata('usuario_id'));				
				$posto 		= $this->empresasM->getEmpresa($this->input->post('empresa_id'));				
				$conteudo1 = " <img src='http://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png' style='width: 285px;' >
					<h3>💳 Solicitação de Cartão Técnico 💳</h3>
					<hr/>
					<p>ID Solicitante:  ".$this->session->userdata('usuario_id')."</p>
					<p>Nome:			".$this->session->userdata('nome')."</p>
					<p>Empresa:         ".$empresa->id." - ".$empresa->cnpj." - ".$empresa->razao_social."</p>
					<p>Endereço Empresa:".$empresa->endereco."</p>
					<p>Bairro: 			".$empresa->bairro."</p>
					<p>Cidade/Estado:	".$empresa->cidade."/".$empresa->estado." </p>
					<p>CEP: 			".$empresa->cep. "</p>														
					<p>POSTO:      		".$posto[0]['cnpj']. " - " .$posto[0]['razao_social']."</p>";

				$this->load->library('email');
				$result = $this->email
								->from('webmaster@wertco.com.br')
								->reply_to('industrial@wertco.com.br')    // Optional, an account where a human being reads.
								->to('suporte@wertco.com.br')
								->subject('💳 WERTCO - Solicitação de Cartão Técnico 💳')
								->message($conteudo1)								
								->send();
				if( $result ){
					$result1 = $this->email
								->from('webmaster@wertco.com.br')
								->reply_to('industrial@wertco.com.br')    // Optional, an account where a human being reads.
								->to('industrial@wertco.com.br')
								->subject('💳 WERTCO - Solicitação de Cartão Técnico 💳')
								->message($conteudo1)								
								->send();
					$this->session->set_flashdata('sucesso', 'ok');
				}else{
					$this->session->set_flashdata('erro', 'email não enviado');
				}

				$parametros 				= 	$this->session->userdata();		
				$parametros['solicitacao'] 	= 	$this->cartaoM->verificaSolicitacaoCartao($this->session->userdata('usuario_id'));
				$parametros['title']		=	"Solicita Cartão Técnico";
				$this->_load_view('area-tecnicos/cartao-tecnicos/solicita-cartao',$parametros);
			}
		}else{
			
			$parametros 				= 	$this->session->userdata();		
			$parametros['solicitacao'] 	= 	$this->cartaoM->verificaSolicitacaoCartao($this->session->userdata('usuario_id'));
			$parametros['title']		=	"Solicita Cartão Técnico";
			$this->_load_view('area-tecnicos/cartao-tecnicos/solicita-cartao',$parametros);
		}
	}

	public function retornaEmpresas()
	{
		
		$rows = $this->empresasM->getEmpresaAutocomplete($_GET['term']);
			
		if(count($rows) > 0){

			foreach ($rows as $row) {

				$dados[] = array('label' 	=>	$row['label'],
								'id'		=>	$row['id'],
								'value'		=>	$row['label']	);
				
			}

			echo json_encode($dados);
		}

	}

	public function orcamentosTec()
	{
		if( $this->input->post() )
		{
			
			if( $this->input->post('salvar') == '1')
			{			
				$dadosEmpresa = array(
			        'razao_social' 				=>	$this->input->post('razao_social'),
					'fantasia' 					=>	$this->input->post('fantasia'),
					'cnpj' 						=>	$this->input->post('cnpj'),
					'telefone' 					=>	$this->input->post('telefone'),
					'endereco' 					=>	$this->input->post('endereco'),
					'email' 					=>	$this->input->post('email'),
					'cidade' 					=>	$this->input->post('cidade'),
					'estado'					=>	$this->input->post('estado'),
					'pais'						=>	$this->input->post('pais'),
					'bairro'					=>	$this->input->post('bairro'),
					'cep'						=>	$this->input->post('cep'),
					'insc_estadual'				=>	$this->input->post('insc_estadual'),
					'tipo_cadastro_id'			=>	1,
					'cartao_cnpj'				=> 	$this->input->post('cartao_cnpj'),
					'codigo_ibge_cidade'		=> 	$this->input->post('codigo_ibge_cidade')
			    );			

				if($this->empresasM->add($dadosEmpresa))
				{		
					$empresa_id = $this->db->insert_id();			

				}
	 		}else{
	 			$empresa_id = $this->input->post('empresa_id');
	 			
	 		}

	 		$dadosOrcamento = array('empresa_id' 			=> 	$empresa_id,
	 								'status_orcamento_id'	=> 	1,
	 								'origem'				=> 	'Técnico',
	 								'usuario_id'			=> 	$this->session->userdata('usuario_id'),
	 								'solicitante_id'		=> 	$this->session->userdata('usuario_id'),
									'indicador_id'			=> 	$this->session->userdata('usuario_id'),
									'contato_posto'			=> 	$this->input->post('contato_posto'),
									'entrega_id'			=> 	17,
									'frete_id'				=> 	3,
									'forma_pagto_id'		=> 	6,
									'dt_previsao'			=> 	date('Y-m-d',strtotime('+68 days'))		);

	 		if( $this->orcamentosM->insereOrcamento($dadosOrcamento) )
	 		{
	 			$orcamento_id  = $this->db->insert_id();
	 			$erro = 0;

	 			$dadosContato = array(	'empresa_id' 		=> 	$empresa_id,
	 									'nome'				=> 	$this->input->post('contato_posto'),
	 									'email'				=> 	$this->input->post('email_contato_posto'),
	 									'celular'			=> 	$this->input->post('tel_contato_posto'),
	 									'tipo_cadastro_id'	=> 	1);

	 			if( $this->usuariosM->add($dadosContato) )
	 			{

	 				$contato_id = $this->db->insert_id();
	 				$updateContato = array(	'id' 			=> 	$orcamento_id,
	 										'contato_id'	=> 	$contato_id	);
	 				
	 				$this->orcamentosM->atualizaOrcamento($updateContato);

		 			$andamentoOrcamento = array('andamento' 			=> 'Orçamento realizado pelo técnico '.$this->session->userdata('nome').' da empresa código: '.$empresa_id.'!',
			 									'dthr_andamento'		=> date('Y-m-d H:i:s'),
			 									'orcamento_id'			=> $orcamento_id,
			 									'status_orcamento_id'	=> 7
			 									);	
		 			if($this->orcamentosM->insereOrcamentoAndamentos($andamentoOrcamento))
		 			{	 				
		 				$erro = $erro;
		 				$orcamentoResp = array(	'orcamento_id' 	=> 	$orcamento_id,
			 									'usuario_id' 	=> 	$this->input->post('responsavel_id') );
	 					if($this->orcamentosM->insereResponsavelOrcamento($orcamentoResp))
	 					{
	 						$erro = $erro;
	 					
				 			for ($i=0; $i < count($this->input->post('produto')) ; $i++) 	 			  
				 			{ 
				 				$orcamentoProdutos = array(	'orcamento_id' 	=>	$orcamento_id,
				 											'produto_id'  	=> 	$this->input->post('produto')[$i],
				 											'qtd'			=>	$this->input->post('quantidade')[$i]);	

				 				if($this->orcamentosM->insereOrcamentoProdutos($orcamentoProdutos))
				 				{
				 					$erro = $erro;
				 					
								}else{
									$erro++;
								}		 					 			
				 			}
				 		}else{
							$erro++;
						}	 					

					}else{
						$erro++;
						
					}
				}else{
					$erro++;				
				}	
			}else{
				$erro++;
			}

	 		if( $erro == 0){	 				
	 				
	 			$email_enviar = $this->zonaM->buscaEmailResponsavel($this->input->post('responsavel_id'));

	 			if($this->enviarEmailOrcamento($orcamento_id, 'todosvendas@wertco.com.br', 'Orçamento realizado no Sistema pelo Indicador: '. $this->session->userdata('nome').'. Zona:'. $email_enviar['nome']))
 				{
 					//$email_enviar = $this->zonaM->buscarEmailResp($this->input->post('estado')); 					
 					$this->enviarEmailOrcamento($orcamento_id, $email_enviar['email'], 'Orçamento realizado no Sistema pelo Indicador: '. $this->session->userdata('nome'));
 					$this->session->set_flashdata('sucesso', 'ok.');
 				}else{
 					
 					$this->session->set_flashdata('erro', 'erro.');	
 				}
 			}else{
 				$this->session->set_flashdata('erro', 'erro.');
 			}	 		
		
		}

		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->orcamentosM->buscaOrcamentosIndicadores($parametros['usuario_id']);		
		$parametros['title']	=	"Orçamentos";
		$this->_load_view('area-tecnicos/orcamentos/orcamentos-tec',$parametros);

	}

	public function cadastraOrcamento()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->orcamentosM->buscaOrcamentosIndicadores($parametros['usuario_id']);		
		$parametros['bombas'] 	= 	$this->produtosM->select();		
		$parametros['title']	=	"Orçamentos";
		$this->_load_view('area-tecnicos/orcamentos/cadastra-orcamento-tec',$parametros);	
		//$this->orcamentosTec();
	}

	public function buscaAndamentos()
	{

		$retorno = $this->orcamentosM->buscaAndamentos($_POST['orcamento_id']);			
		echo json_encode($retorno);		
	}
	
	public function alteraStatusOrcamento()
	{
		$dados = array('id'						=>	$_POST['orcamento_id'],
					   'status_orcamento_id'	=>	$_POST['status_orcamento']);

		if($this->orcamentosM->atualizaStatusOrcamento($dados))
		{

			switch($_POST['status_orcamento']){
				case 1:
			        $andamento = "Orçamento Aberto!";
			        break;
			    case 2:
			        $andamento = "Orçamento Fechado, venda realizada!";
			        break;
			    case 3:
			        $andamento = "Orçamento Perdido para concorrência.";
			        break;
			    case 4:
			        $andamento = "Orçamento Cancelado.";
			        break;
			    case 5:
			        $andamento = "Orçamento entregue ao cliente.";
			        break;        
			    case 6:
			        $andamento = "Orçamento em negociação.";
			        break;        
			}

			$inserirAndamento = array('orcamento_id'			=>	$_POST['orcamento_id'],
										'status_orcamento_id'	=> 	$_POST['status_orcamento'],
										'andamento'				=>	$andamento,
										'dthr_andamento'		=>	date('Y-m-d H:i:s'),
										'usuario_id'			=> 	$this->session->userdata('usuario_id'));

			if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
			{
				echo json_encode(array('retorno' => 'sucesso'));	
			}else{
				echo json_encode(array('retorno' => 'erro'));	
			}			
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function insereAndamentoOrcamento()
	{ 
		$inserirAndamento = array('orcamento_id'		=>	$_POST['orcamento_id'],
								'status_orcamento_id'	=> 	$_POST['status_orcamento_id'],
								'andamento'				=>	$_POST['andamento'],
								'dthr_andamento'		=>	date('Y-m-d H:i:s'),
								'usuario_id'			=> 	$this->session->userdata('usuario_id'));

		if($this->orcamentosM->insereOrcamentoAndamentos($inserirAndamento))
		{
			echo json_encode(array('retorno' => 'sucesso'));	
		}else{
			echo json_encode(array('retorno' => 'erro'));	
		}			

	}
	
	public function visualizarOrcamentoInd($id)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->orcamentosM->getOrcamentoProdutos($id);		
		$parametros['title']	=	"Orçamento #".$id;
		$this->_load_view('area-indicadores/orcamentos/visualiza-orcamento-ind',$parametros);
	}


	/************************************************************ 
	************** GERA PDF DO ORÇAMENTO SOLICITADO *************
	*************************************************************/
	public function geraOrcamento()
    {    	
	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('orcamento-'.$_POST['orcamento_id'].'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $dados	=	$this->orcamentosM->getOrcamentoProdutos($_POST['orcamento_id']);

	    $data['empresa'] = array('razao_social' 	=> 	$dados[0]->razao_social,
	    							'fantasia' 		=> 	$dados[0]->fantasia,
									'cnpj' 			=> 	$dados[0]->cnpj,
									'telefone'		=> 	$dados[0]->telefone,
									'email'			=> 	$dados[0]->email,
									'endereco'		=> 	$dados[0]->endereco,
									'cidade'		=> 	$dados[0]->cidade,		
									'pais'			=> 	$dados[0]->pais,
									'estado'		=> 	$dados[0]->estado,
									'orcamento_id'	=> 	$dados[0]->orcamento_id,
									'emissao'		=> 	$dados[0]->emissao);

	    foreach ($dados as $dado) {
	    	$data['produtos'][] = array('codigo' 			=> 	$dado->codigo,
	    								'descricao'			=>	$dado->descricao,
	    								'modelo'			=>	$dado->modelo,
	    								'qtd'				=>	$dado->qtd,
	    								'valor_unitario' 	=>	$dado->valor_unitario);
	    }		

	    //Load html view
	    $this->html2pdf->html($this->load->view('/area-tecnicos/orcamentos/orcamento-pdf', $data, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	echo json_encode(array('retorno' => 'orcamento-'.$_POST['orcamento_id'].'.pdf'));
	    }	    
	    
    }

    private function enviarEmailOrcamento($orcamento_id){
		$produtos 	= 	$this->orcamentosM->listarProdutos($orcamento_id);				
		$empresa 	= 	$this->orcamentosM->listarEmpresaOrcamento($orcamento_id);				
		$date 		= 	new DateTime($empresa['emissao']);		
		$email = '<html>
		<head></head>
		<body style="width: 600px;height: 100%;font-family: sans-serif;color: #000; background-color: #fff;">
		<div class="content" style="width: 600px; height: 100%;">
		<div style="width: 600px; background-image: url(http://www.wertco.com.br/email-cliente/backgroundCabecalho.png); height: 528px; "></div>
		<div style="width: 600px; background-color: #ffcc00; height: 100%">
			<table style="padding-top: 20px;font-size: 16px;margin-left: 15px;width: 570px;">
				<tbody>
					<tr>
						<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">Orçamento:</td>
						<td style="padding-bottom: 18px;font-weight: 700;">'.$orcamento_id.'</td>
						<td style="padding-bottom: 18px;font-weight: 700;"></td>
						<td style="padding-bottom: 18px;font-weight: 700;"></td>
					</tr>
					<tr>
						<td style="padding-bottom: 18px;width: 73px;font-weight: 700;">R. Social:</td>
						<td style="padding-bottom: 18px;width: 231px;">'.$empresa['razao_social'].'</td>
						<td style="padding-bottom: 18px;font-weight: 700;text-align: right;">CNPJ:</td>
						<td style="padding-bottom:  18px;width: 157px;">'.$empresa['cnpj'].'</td>
					</tr>
					<tr>
						<td style="padding-bottom: 18px;font-weight: 700;">Validade:</td>
						<td style="padding-bottom: 18px;">30 Dias</td>
						<td style="padding-bottom: 18px;font-weight: 700;width: 64px;text-align: right;">Emissão:</td>
						<td style="padding-bottom: 18px;">'.date('d/m/Y H:i:s',strtotime($empresa['emissao'])).'</td>
					</tr>
					<tr>
						<td style="font-weight: 700;padding-bottom: 18px;">Fantasia:</td>
						<td style="   padding-bottom: 18px;">'.$empresa['fantasia'].'</td>
						<td style=" font-weight: 700; text-align: right;">Telefone:</td>
						<td>'.$empresa['telefone'].'</td>
					</tr>
					<tr>
						<td style=" font-weight: 700;">E-mail:</td>
						<td colspan="3">'.$empresa['email'].'</td>
					</tr>
				</tbody>
			</table>
			<img src="http://www.wertco.com.br/email-cliente/produtos.png" style="margin-top: 50px;margin-left: 46px;margin-bottom: 30px;">
			<table style="width: 570px;margin-left: 14px;" cellspacing="0">
				<thead style="height: 55px;">
					<tr><th style="    height: 30px;">Modelo</th>
					<th>Descrição</th>
					<th>Quantidade</th>
					<th>Valor</th>
				</tr></thead>
				<tbody style="background-color: #fff;margin-left: 10px;font-size: 12px;">';
		$total = 0; foreach($produtos as $produto){
			
			$email.='	<tr>
						<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['modelo'].'</td>
						<td style="    border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['descricao'].'</td>
						<td style="    text-align: center;border-right: 1px solid #f0f0f0;    padding: 10px;">'.$produto['qtd'].'</td>
						
					</tr>';					
			$total = $total + ($produto['valor'] * $produto['qtd']);
		}			
		$email.='		
				</tbody>
				</table>
				<img src="http://www.wertco.com.br/email-cliente/diferencaAliquota.png" style="margin-left: 17px;">
				</div>
			</div>
		</body>
		</html>';

		$this->load->library('email');

		$result = $this->email
		    ->from('webmaster@wertco.com.br')
		    ->reply_to('industrial@wertco.com.br')    // Optional, an account where a human being reads.
		    ->to('vendas@wertco.com.br')
		    ->subject('Orçamento solicitado pelo técnico - indicador: '. $this->session->userdata('nome'))
		    ->message($email)
		    ->send();

		return $result;		
	}

	public function aceiteContrato(){

		$usuario = $this->empresasM->empresaUsuario($this->input->post('usuario_id'));

		if( $this->input->post('valor') == 1 ){

			$dadosUpdate = array(	'fl_termos'				=> $this->input->post('valor'),
									'id'					=> $this->input->post('usuario_id'),
									'dthr_aceite_termos'	=> date('Y-m-d H:i:s') );
			if( $this->usuariosM->atualizaUsuario($dadosUpdate) ){

				$conteudo1 = " <img src='http://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png' style='width: 285px;' >
					<h3>Contrato Aceito</h3>
					<hr/>
					<p>ID Técnico:  	".$this->input->post('usuario_id')."</p>
					<p>Nome:			".$usuario->nome."</p>
					<p>Empresa:         ".$usuario->id." - ".$usuario->cnpj." - ".$usuario->razao_social."</p>
					<p>Endereço Empresa:".$usuario->endereco.", ".$usuario->bairro.", ".$usuario->cidade."-".$usuario->estado." " .$usuario->cep. "</p>";					

				$this->load->library('email');
				$result = $this->email
								->from('webmaster@wertco.com.br')
								->reply_to('industrial@wertco.com.br')    // Optional, an account where a human being reads.
								->to('industrial@wertco.com.br')
								->subject('✎ WERTCO - Contrato Técnico Aceito ✎')
								->message($conteudo1)								
								->send();
				if( $result ){
					echo json_encode(array('retorno' => '1'));			
				}else{
					echo json_encode(array('retorno' => 'erro'));
				}
			}else{
				echo json_encode(array('retorno' => 'erro'));
			}

		}else{

			$dadosUpdate = array(	'fl_termos'	=> $this->input->post('valor'),
									'id'		=> $this->input->post('usuario_id'),
									'ativo'	=> 0,
									'dthr_aceite_termos'	=> date('Y-d-m H:i:s') 	);

			if( $this->usuariosM->atualizaUsuario($dadosUpdate) ){
								
				$conteudo1 = " <img src='http://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png' style='width: 285px;' >
						<h3>Contrato Negado</h3>
						<hr/>
						<p>ID Técnico:  	".$this->input->post('usuario_id')."</p>
						<p>Nome:			".$usuario->nome."</p>
						<p>Empresa:         ".$usuario->id." - ".$usuario->cnpj." - ".$usuario->razao_social."</p>
						<p>Endereço Empresa:".$usuario->endereco.", ".$usuario->bairro.", ".$usuario->cidade."-".$usuario->estado." " .$usuario->cep. "</p>
						<hr/>
						<p><b> LOGIN DO USUÁRIO DESATIVADO AUTOMÁTICAMENTE<b></p>";

				$this->load->library('email');
				$result = $this->email
								->from('webmaster@wertco.com.br')
								->reply_to('industrial@wertco.com.br')    // Optional, an account where a human being reads.
								->to('industrial@wertco.com.br')
								->subject('✎ WERTCO - Contrato Técnico Negado ✎')
								->message($conteudo1)								
								->send();
				if( $result ){
					echo json_encode(array('retorno' => '0'));			
				}else{
					echo json_encode(array('retorno' => 'erro'));
				}
			}else{
				echo json_encode(array('retorno' => 'erro'));
			}

		}
	}

	public function gestaoFuncionarios()
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	= 	$this->usuariosM->getTodosUsuariosEmpresa($parametros['empresa_id']);		
		$parametros['title']	=	"Área do Técnico - Gestão de Funcionários";		
		
		$this->_load_view('area-tecnicos/gestao-funcionarios/gestao-funcionarios',$parametros);
	}

	public function cadastraFuncionario()
	{
		if( $this->input->post()  ){
			$dadosUsuarios = array(	'nome'					=>	$this->input->post('nome'),
									'cpf' 					=>	$this->input->post('cpf'),
									'telefone' 				=>	$this->input->post('telefone'),
									'celular' 				=>	$this->input->post('celular'),
									'email' 				=>	$this->input->post('email'),					
									'empresa_id'			=>	$this->input->post('empresa_id'),
									'tipo_cadastro_id'		=>	$this->input->post('tipo_cadastro_id'),
									'senha'					=>	md5(md5($this->input->post('senha'))),
									'subtipo_cadastro_id'	=> 	$this->input->post('subtipo_cadastro_id'),
									'ativo'					=> 	$this->input->post('ativo')	);

			if($this->usuariosM->add($dadosUsuarios)){

				$this->session->set_flashdata('sucesso_cadastro', 'ok.');	
				$this->gestaoFuncionarios();		
			}else{

				$this->session->set_flashdata('erro', 'erro.');			
				$this->gestaoFuncionarios();
			}

		}else{
			$parametros 					= 	$this->session->userdata();
			$parametros['subtipo_cadastro']	= 	$this->empresasM->getSubtipoByTipo(4);
			$parametros['title']			=	"Área do Técnico - Cadastro de Funcionários Técnicos";
			
			$this->_load_view('area-tecnicos/gestao-funcionarios/cadastra-funcionario',$parametros);
		}
	} 

	/****************************************************************************
	**************** Método Ajax - Excluir Funcionário **************************
	*****************************************************************************/
	public function excluirFuncionario()
	{
		if($this->usuariosM->excluirUsuario($_POST['id'])){

			echo json_encode(array('retorno' => 'sucesso'));
		}else{
			echo json_encode(array('retorno' => 'erro'));
		}
	}

	public function visualizarFuncionario($id)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	= 	$this->usuariosM->findMarca($id);
		$parametros['title']	=	"Visualizar Informações Funcionário ".$parametros['dados']['nome'];		
		
		$this->_load_view('area-tecnicos/gestao-funcionarios/visualizar-funcionario',$parametros);
	}

	public function editarFuncionario($id)
	{
		
		if(	$this->input->post()	){
			
			$update = $this->input->post();			
			unset($update['salvar']);
			unset($update['senha2']);
			unset($update['senha']);			
			
			if($this->input->post('senha') != ""){
				$update['senha']	=	md5(md5($this->input->post('senha')));
			}
			 
			if($this->usuariosM->atualizaUsuario($update)){
				
				$this->session->set_flashdata('sucesso_editar', 'ok');				
				$this->gestaoFuncionarios();	
			}else{
				$this->session->set_flashdata('erro', 'erro');				
				$this->gestaoFuncionarios();	
			}

		}else{
			$parametros 					= 	$this->session->userdata();
			$parametros['dados']			= 	$this->usuariosM->findMarca($id);
			$parametros['subtipo_cadastro']	= 	$this->empresasM->getSubtipoByTipo(4);
			$parametros['title']			=	"Área do Técnico - Editar Informações Funcionário ".$parametros['dados']['nome'];
			
			$this->_load_view('area-tecnicos/gestao-funcionarios/editar-funcionario',$parametros);
		}
	}

	public function solicitarCartaoFuncionario(){

		$dados = array(	'solicitante_id' 	=>	$this->input->post('usuario_id'),
						'situacao'			=> 	'Solicitação realizada',
						'empresa_id' 		=>	$this->input->post('empresa_id'));

		if($this->cartaoM->insereSolicitacaoCartao($dados)){

			$empresa 	= $this->empresasM->empresaUsuario($this->input->post('usuario_id'));				
			$posto 		= $this->empresasM->getEmpresa($this->input->post('empresa_id'));				
			$conteudo1 = " <img src='http://www.wertco.com.br/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png' style='width: 285px;' >
							<h3>💳 Solicitação de Cartão Técnico 💳</h3>
							<hr/>
							<p>ID Técnico: 			".$this->input->post('usuario_id')."</p>
							<p>Nome:				".$this->input->post('nome')."</p>							
							<p>Empresa:         	".$empresa->id." - ".$empresa->cnpj." - ".$empresa->razao_social."</p>
							<p>Endereço: 			".$empresa->endereco."</p>
							<p>Bairro: 				".$empresa->bairro."</p>
							<p>Cidade/Estado:		".$empresa->cidade."/".$empresa->estado." </p>
							<p>CEP: 				".$empresa->cep. "</p>							
							<p>POSTO:      			".$posto[0]['cnpj']. " - " .$posto[0]['razao_social']."</p>";

			$this->load->library('email');
			$this->email
					->from('webmaster@wertco.com.br')
					->reply_to('industrial@wertco.com.br')    // Optional, an account where a human being reads.
					->to('industrial@wertco.com.br')
					->subject('💳 WERTCO - Solicitação de Cartão Técnico 💳')
					->message($conteudo1)								
					->send();

			echo json_encode(array('retorno' =>	'sucesso'));
			
		}else{
			echo json_encode(array('retorno' =>	'erro'));
		}

	}

	public function ValidaCnpj(){
    	$cnpj 			= 	$this->input->post('cnpj');		
		$consulta_rf 	= 	$this->ConsultaCNPJRF($cnpj);

        if (isset($consulta_rf) && $consulta_rf->{'code'} == '1')
        {
            if ($consulta_rf->{'data'}->{'situacao_cadastral'} == 'ATIVA')
            {
            	$arr['retorno'] 	= 	$consulta_rf->{'data'};
            	$arr['save'] 		= 	$consulta_rf->{'Save_Receita'};
                $arr['result'] 		= 	true;
                $arr['situacao'] 	= 	$consulta_rf->{'data'}->{'situacao_cadastral'};
            	//echo json_encode($consulta_rf);            	
                
            	$arr['result'] = true;
				$arr['erro'] = 0;

            }else {
            	$arr['erro'] 	= 1;
            	$arr['msg'] 	= 'Situação cadastral '.$consulta_rf->{'data'}->{'situacao_cadastral'}.' na Receita Federal.'; 
            }
		}else {

            if (isset($consulta_rf)){
            	$arr['erro'] = 1;
            	$arr['msg'] = 'Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 2 - '.$consulta_rf->{'code'}.')';
            }else{
            	$arr['erro'] = 1;
            	$arr['msg'] = 'Falha na consulta a Receita Federal. Contate a Companytec 3284-8100 e informe o erro: (API 3)';
            }

        }

		echo json_encode(array(	'receita' 	=> $arr));
	}

	public function buscaConsultor()
	{
		echo json_encode($this->zonaM->buscaConsultorEstado($this->input->post('estado')));
	}

}