<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

class AreaFinanceiro extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		//verifica se está conectado e na área correta
		if( !$this->_is_logged() || ($this->session->userdata('tipo_acesso') != 'financeiro' && $this->session->userdata('tipo_acesso') != 'administrador geral' && $this->session->userdata('tipo_acesso') != 'suporte') ) 
		{
			redirect(base_url('usuarios/login'));
		}

		$this->load->model('EmpresasModel', 'empresasM');
		$this->load->model('UsuariosModel', 'usuariosM');
		$this->load->model('ChamadoModel', 'chamadoM');

	}
	
	public function index()
	{
		$parametros			 	=	$this->session->userdata();
		$parametros['dados']	=	$this->usuariosM->getUsuarios();		
		$parametros['title']	=	"Área do Administrador Financeiro";		
		$this->_load_view('area-financeiro/index',$parametros);

	}

	public function chamados($abertos = 1)
	{
		$parametros 					= 	$this->session->userdata();
		$parametros['title']			=	"Área do Administrador Financeiro - Chamados";
		//$parametros['total_status']		= 	$this->chamadoM->selectTotalStatus();
		//$parametros['total_defeitos']	= 	$this->chamadoM->selectTotalDefeitos();
		//$parametros['total_mes']		= 	$this->chamadoM->totalChamadoMes();
		//$parametros['total_estado']		= 	$this->chamadoM->totalChamadoEstado();
		//$parametros['ultimos_chamados']	= 	$this->chamadoM->ultimosChamadosRealizados();		
		//$parametros['chamados_inativos']	=	$this->chamadoM->buscaChamadosInativos($this->session->userdata('usuario_id'));
		$parametros['dados']			=	$this->chamadoM->selectFinanceiro($abertos);	
		
		$this->_load_view('area-financeiro/chamados/chamados-financeiro',$parametros );	
	}

	public function editarCliente()
	{
		if($this->input->post('salvar') == 1){

			$update = array('id'				=>	$this->input->post('id'),
							'nome'				=>	$this->input->post('nome'),
							'cpf'				=>	$this->input->post('cpf'),
							'email'				=>	$this->input->post('email'),
							'telefone'			=>	$this->input->post('telefone'));

			if( $_FILES['nome_arquivo']['name'] != ""){
				$tipo 			= 	explode('.', $_FILES['nome_arquivo']['name']);
				$arquivo 		=	md5($_FILES['nome_arquivo']['name'].date('d/m/Y H:i:s')).'.'.$tipo[count($tipo)-1];
				
				$sourcefile		= 	$_FILES['nome_arquivo']['tmp_name'];
				$endfile 		= 	'img_perfil/'.$arquivo;
				$type 			=	$_FILES['nome_arquivo']['type'];
							    
			    $this->makeThumbnail($sourcefile, $max_width=70, $max_height=70, $endfile, $type);
			   	$update['foto']	= 	$endfile;
			    
			}

			if($this->input->post('senha') != ""){
				$update['senha']	=	$this->input->post('senha');
			}
			
			if($this->usuariosM->atualizaCliente($update)){
				$this->session->set_flashdata('sucesso', 'ok');
				
				$parametros 						= 	$this->session->userdata();
				$parametros['dados'] 				= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));
				$parametros['title']				=	"Editar Perfil";		
				$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
				$parametros['dados_empresa']		=	$this->empresasM->select();			
				$this->_load_view('area-financeiro/editar-cliente',	$parametros );	
			}

		}else{

			$row 								= 	$this->usuariosM->getUsuario($this->session->userdata('usuario_id'));		
			$parametros 						= 	$this->session->userdata();
			$parametros['dados']				=	$row;
			$parametros['title']				=	"Editar Perfil";		
			$parametros['dados_tipo_acesso']	=	$this->usuariosM->getTiposCadastro();
			$parametros['dados_empresa']		=	$this->empresasM->select();			
			$this->_load_view('area-financeiro/editar-cliente',	$parametros );	
		}
	
	}

	public function clientesInadimplentes($pesquisa = null)
	{
		$parametros 			= 	$this->session->userdata();
		$parametros['dados']	=	$this->empresasM->getClientesTecnicos();
		$parametros['title']	=	"Situação Financeira Clientes";
		$parametros['pesquisa']	= 	(strpos($pesquisa,'%20') >= 0) ? str_replace('%20', ' ', $pesquisa) : $pesquisa;
		$this->_load_view('area-financeiro/situacao/clientes-inadimplentes',$parametros);
	
	}

	public function alteraSituacaoCliente(){

		$dados = array(	'id'				=>	$_POST['id'],
						'fl_inadimplencia'	=>	$_POST['ativo']	);

		if($this->empresasM->atualizaEmpresas($dados)){
			$this->log('Área Financeiro | altera inadimplencia cliente','empresas','EDIÇÃO', $this->session->userdata('usuario_id'),$dados,$dados,$_SERVER['REMOTE_ADDR']);	
			$status  = ($_POST['ativo'] == 1) ? 'Inadimplente' : 'Adimplente';
			$conteudo = '<html>
					<head></head>
					<body style="" width: 650px; position:absolute;">

						<div style="width: 80%;position: relative;text-align: left;margin-left: 62px;">
						<img src="http://www.wertco.com.br/bootstrap/assets/demo/default/media/img/logo/wertcofundoescuro.png" /><br/>
							<h2 style="font-family: verdana;font-size: 21px;font-weight: 600;color: #000;">Atualização de Situação Financeira do Cliente: '.$_POST['conteudo'].' </h2>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
								Cliente: '.$_POST['conteudo'].'			
							</p>
							<p style="font-family: verdana;font-size: 16px;font-weight: 400;line-height: 1.5;color: #000;">
								Situação: <b>'.$status.'</b>
							</p>
						</div>
					</body>
				</html>';

			$this->enviaEmail('gestor@wertco.com.br',		'💲 Cliente atualizado como '.$status.' no sistema 💲', $conteudo );
			$this->enviaEmail('industrial@wertco.com.br',	'💲 Cliente atualizado como '.$status.' no sistema 💲', $conteudo );
			$this->enviaEmail('suporte@wertco.com.br',		'💲 Cliente atualizado como '.$status.' no sistema 💲', $conteudo );
			$this->enviaEmail('webmaster@wertco.com.br',	'💲 Cliente atualizado como '.$status.' no sistema 💲', $conteudo );
			
			echo json_encode(array('retorno' => 'sucesso'));

		}else{
			echo json_encode(array('retorno' => 'erro'));

		}

	}	

	private function enviaEmail($email_destino,$titulo,$conteudo,$anexo=null){

		$this->load->library('email');
		
		if($anexo == null){
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->send();
		}else{
			$result = $this->email
			    ->from('webmaster@wertco.com.br')
			    ->reply_to($this->session->userdata('email'))
			    ->to($email_destino)
			    ->subject($titulo)
			    ->message($conteudo)
			    ->attach($anexo)
			    ->send();
		}

		return $result;		
	}

}