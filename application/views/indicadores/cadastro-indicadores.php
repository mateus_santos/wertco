<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <title>WERTCO - Cadastro de Indicadores</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon.png')?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />


    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

</head>

<body class="double-diagonal blog-page dark">
    <!-- Preloader Starts -->
   
    <!-- Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
    	<div id="loading" style=" text-align: center;width: 100%;height: 100%; display: none; background: #cccccc45;z-index: 9;/* top: 0; */position: absolute;">
    		<div style="position: relative;top: 63%;  background: #ccc; padding: 20px; text-align: center;">
	    		<h2 style="color: #000;" id="msg_loading"> Aguarde, estamos validando seu CNPJ... </h2>
	    		<img src="<?php echo base_url('bootstrap/img/loading.gif'); ?>" style="height: 70px;">
    		</div>
    	</div>
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                    <div class="logo">
                        <a class="navbar-brand link-menu nav-external" >
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external"  href="<?=base_url('/home/index')?>"><i class="fa fa-arrow-left"></i> Voltar ao Site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		<!-- Banner Starts -->
        <section class="banner">
			<div class="content text-center">
				<div class="text-center top-text">
                    <h1>Cadastro de Indicadores</h1>
					<hr>
                    <h4></h4>
                </div>
			</div>
		</section>
		<!--    Banner Ends        -->
        <!--    Services Begin     -->
        <section id="clientes" class="services">
            <!-- Container Starts -->
            <div class="container" style="background: #333;border-radius: 10px;">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Dados</span> da empresa</h1>
                    <h4></h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
					<span class="outer-line"></span>
					<img src="<?=base_url('bootstrap/img/bombaIcoG.png')?>" />
					<span class="outer-line"></span>
				</div>
                <!-- Divider Ends -->
                <!-- Services Starts -->
				<div class="cadastros">
					<form accept-charset="utf-8" id="formulario" action="<?php echo base_url('indicadores/add/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;">
					
					<div class="row dados">  						
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control cnpj" id="cnpj" maxlength="15" name="cnpj" placeholder="cnpj" required />	
							<input type="hidden"  id="salvar" maxlength="15" name="salvar" value="1"	required />
							<input type="hidden"  id="empresa_id" maxlength="15" name="empresa_id" value=""  />
							<input type="hidden"  id="cartao_cnpj" name="cartao_cnpj"   />
						</div>                    
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control" id="razao_social" maxlength="250" name="razao_social" placeholder="Razão Social" value="" required />	
						</div>                    
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control" id="fantasia" maxlength="250" name="fantasia" placeholder="fantasia" placeholder="Nome Fantasia" value="" />	
						</div>                    
					</div>
					<div class="row dados">
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control telefone" required id="telefone" maxlength="250" name="telefone" placeholder="telefone" value=""  />	
						</div>   
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="email" class="form-control" id="email" maxlength="250" name="email" placeholder="E-mail" value="" required />	
						</div>                    
						<div class="col-md-3 col-lg-3 col-sm-12 ">
							<input type="text" class="form-control" id="endereco" maxlength="250" name="endereco" placeholder="Endereço" value="" required />	
						</div> 				
						<div class="col-md-3 col-lg-3 col-sm-12 ">
							<input type="text" class="form-control" id="bairro" maxlength="250" name="bairro" placeholder="Bairro" value="" required />	
						</div> 							
					</div>					
					<div class="row dados">  						
						<div class="col-md-3 col-lg-3 col-sm-12 ">
							<input type="text" class="form-control" id="cep" maxlength="250" name="cep" placeholder="CEP" value="" required />	
						</div>
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" id="cidade" name="cidade" placeholder="Cidade" value="" required />	
						</div>                    
						<div class="col-md-3 col-lg-3 col-sm-12">
							<select  class="form-control"  maxlength="250" name="estado" id="estado" placeholder="Estado" required>
                                <option value="">Estado</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espírito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>
                            </select> 	
						</div>		
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" name="pais" id="pais" placeholder="País" value="" required />	
						</div>				
					</div>

					<div class="text-center top-text" style="margin-top: 50px;">
						<h1><span>Dados</span> Pessoais</h1>
						<h4>Dados para acesso ao site</h4>
					</div>

					<div class="divider text-center">
						<span class="outer-line"></span>
						<span class="fa fa-user" aria-hidden="true"></span>
						<span class="outer-line"></span>
					</div>

					<div class="row dados">  						
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" name="pessoal_nome" placeholder="Nome" value="" required />	
						</div>                    
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control" id="cpf" maxlength="250" name="pessoal_cpf" placeholder="CPF" value="" required />	
						</div>
						<div class="col-md-2 col-lg-2 col-sm-12">
							<input type="text" class="form-control telefone" id="pessoal_telefone" name="pessoal_telefone" placeholder="Telefone"  />	
						</div>
						<div class="col-md-2 col-lg-2 col-sm-12">
							<input type="text" class="form-control celular" id="pessoal_celular" maxlength="250" name="pessoal_celular" placeholder="Celular" value="" />
						</div>
						<div class="col-md-2 col-lg-2 col-sm-12">
							<input type="email" class="form-control" id="email_pessoal" maxlength="250" name="pessoal_email" placeholder="E-mail para Acesso" value="" required />	
						</div>                     
					</div>		
					<div class="row dados"> 
						<div class="col-md-6 col-lg-6 col-sm-12">
							<input type="password" class="form-control" id="senha" maxlength="250" name="senha" placeholder="Senha" value="" required />	
						</div>                    
						<div class="col-md-6 col-lg-6 col-sm-12">
							<input type="password" class="form-control" id="senha2" maxlength="250" name="senha2" placeholder="Confirme sua senha" value="" required />	
						</div>		
					</div>						
					<div class="row dados"> 
						<div class="col-md-4 col-lg-4 col-sm-12 offset-md-4" style="text-align: center; margin: 0 auto;">
							<button type="submit" class="btn btn-outline-secondary btn-lg"  maxlength="250" name="enviar" id="enviar" placeholder="Enviar" value="" required style="margin-bottom: 10px;"> Enviar </button>
							<button style="margin-bottom: 10px;" type="reset"  class="btn btn-outline-secondary btn-lg"  maxlength="250" name="limpar" placeholder="Enviar" value="" required> Limpar </button>
						</div>								
					</div>
					</form>
				</div>
            </div>
                <!-- Services Ends -->
        </section>
        <!-- Service Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center footer-area-restrita" style="border-top: 2px solid #ffcc00 !important;">
            <!-- Container Starts -->
            <div class="container">
               <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/0001-53 </p>
                <p>
                    &copy; Copyright <?php echo date('Y'); ?> Wertco 
                </p>
              
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
    </div>
    <!-- Wrapper Ends -->
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>       
	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>		
	<script type="text/javascript" src="<?=base_url('bootstrap/js/sweetalert.js')?>"></script>

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);}  else { ?>
	<script type="text/javascript">
		$('#cnpj').focus();
	</script>
<?php } ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
		   		title: "OK!",
		   		text: 'Cadastro realizado com sucesso!',
		   		type: 'success'
	    	}).then(function() {
	    	   	window.location = '<?=base_url('home/index');?>';
    		}); 	    	
	</script>	
<?php unset($_SESSION['sucesso']); } ?>

	<script type="text/javascript">

		$(document).ready(function(){

			$('#cnpj').mask('00.000.000/0000-00');
			$('#cpf').mask('000.000.000-00');
			$('#cep').mask('00000-000');
			$('#bicos').mask('00000');				
			
			$('.data_constituicao').mask('00/00/0000');
			
			$('#formulario').submit(function(){
					 
				$('#enviar').attr('disabled', 'disabled');
					 
			});

			$('#cnpj').bind('focusout', function(){	
				var cnpj = $(this).val();
				if(isCNPJValid(cnpj)){
					$('#loading').show();
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_cnpj');?>",
						async: true,
						data: { cnpj 	:	cnpj }
						}).success(function( data ) {
							var dados = $.parseJSON(data);
							if(dados.length > 0){
																	
								$('#razao_social').val(dados[0].razao_social);		
								$('#razao_social').attr('disabled', true);		
								$('#fantasia').val(dados[0].fantasia);		
								$('#fantasia').attr('disabled', true);
								$('#telefone').val(dados[0].telefone);
								$('#telefone').attr('disabled', true);
								$('#endereco').val(dados[0].endereco);		
								$('#endereco').attr('disabled', true);	
								$('#bairro').val(dados[0].bairro);		
								$('#bairro').attr('disabled', true);	
								$('#cep').val(dados[0].cep);		
								$('#cep').attr('disabled', true);	
								$('#email').val(dados[0].cep);		
								$('#email').attr('disabled', true);	
								$('#cidade').val(dados[0].cep);		
								$('#cidade').attr('disabled', true);	
								$('#estado').val(dados[0].estado);		
								$('#estado').attr('disabled', true);
								$('#pais').val(dados[0].pais);		
								$('#pais').attr('disabled', true);
								$('input[name="posto_rede"]').attr('disabled', true);
								$('#nr_bombas').val(dados[0].nr_bombas);
								$('#nr_bombas').attr('disabled',true); 
								$('#bicos').val(dados[0].bicos);
								$('#bicos').attr('disabled',true);
								$('input[type="checkbox"]').attr('disabled',true);
								$('select').attr('disabled',true);
								$('input[name="software_utilizado"]').attr('disabled',true);
								$('input[name="software_utilizado"]').val(dados[0].software);
								$('input[name="mecanico_atende"]').attr('disabled',true);
								$('input[name="mecanico_atende"]').val(dados[0].mecanico_atende);
								$('#salvar').val('0');
								$('#empresa_id').val(dados[0].id);
								$('#loading').hide();
								
							}else{
								var cnpj_sem_mascara = cnpj.replace(/[^\d]+/g,'');
								
	                            $.ajax({
	                                method: "POST",
	                                url: "<?php echo base_url('Clientes/ValidaCnpj'); ?>",
	                                async: true,
	                                data: {	cnpj 	: 	cnpj_sem_mascara }
	                            }).success(function( data ) {
	                                var dados = $.parseJSON(data);   
	                                
	                                if (dados.receita.erro == 0 && dados.receita.situacao == "ATIVA" ){
	                                    $('#razao_social').attr('disabled', false);     
	                                    $('#razao_social').val(dados.receita.retorno.razao_social);
	                                    if(dados.receita.retorno.razao_social != ''){    
	                                        $('#razao_social').attr('style','border-color: #5fda17;');
	                                    }
	                                    $('#fantasia').attr('disabled', false);
	                                    $('#fantasia').val(dados.receita.retorno.nome_fantasia );
	                                    if(dados.receita.retorno.nome_fantasia != ''){
	                                        $('#fantasia').attr('style','border-color: #5fda17;');
	                                    }
	                                    $('#telefone').attr('disabled', false);
	                                    $('#telefone').val(dados.receita.retorno.telefone);
	                                    if(dados.receita.retorno.telefone != ''){
	                                        $('#telefone').attr('style','border-color: #5fda17;');
	                                    }
	                                    $('#endereco').attr('disabled', false); 
	                                    $('#endereco').val(dados.receita.retorno.logradouro+', '+dados.receita.retorno.numero+' '+dados.receita.retorno.complemento);                                
	                                    $('#endereco').attr('style','border-color: #5fda17;');
	                                    
	                                    $('#email').attr('disabled', false); 
	                                    $('#email').val('');                                                                                                
	                                    $('#cidade').attr('disabled', false); 
	                                    $('#cidade').val(dados.receita.retorno.municipio_ibge);  
	                                    if(dados.receita.retorno.municipio_ibge != ''){
	                                        $('#cidade').attr('style','border-color: #5fda17;');
	                                    }
	                                    $('#estado').attr('disabled', false); 
	                                    $('#estado').val(dados.receita.retorno.uf);
	                                    if(dados.receita.retorno.uf != ''){
	                                        $('#estado').attr('style','border-color: #5fda17;');
	                                    }
	                                    $('#bairro').attr('disabled', false); 
	                                    $('#bairro').val(dados.receita.retorno.bairro);
	                                    if(dados.receita.retorno.bairro != ''){
	                                        $('#bairro').attr('style','border-color: #5fda17;');
	                                    }    
	                                    $('#cep').attr('disabled', false); 
	                                    $('#cep').val(dados.receita.retorno.cep);                          
	                                    if(dados.receita.retorno.bairro != ''){
	                                        $('#cep').attr('style','border-color: #5fda17;');
	                                    }
	                                    $('#pais').attr('disabled', false);
	                                    $('#pais').attr('style','border-color: #5fda17;');                                                
	                                    $('#inscricao_estadual').attr('disabled', false);
	                                    $('#inscricao_estadual').val('');                        
	                                    
	                                    $('#cartao_cnpj').val(dados.receita.save);
	                                    
	                                    $('#pais').val('Brasil');         
	                                    $('#salvar').val('1');

	                                }else{
	                                   swal({
	                                        title: "Atenção!",
	                                        text: dados.receita.msg,
	                                        type: 'warning'
	                                    }).then(function() {
	                                        $('#cnpj').val('');
	                                    });

	                                }

	                                $('#loading').hide();
	                                
							});	
						}

					});	
				} else {
					swal({
						title: 'CNPJ inválido!',
						text: 'Favor verificar o CNPJ informado',
						type:'error'
					}).then(function(isConfirm) {
						$('#cnpj').focus();
					})
				}	
			});
			
			function isCNPJValid(cnpj) {  
				var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
				if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
					return false;
				for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
				if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
					return false; 
				for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
				if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
					return false; 
				return true; 
			};

			$('#cpf').bind('focusout', function(){
				var cpf = $(this).val();
				if(cpf != ""){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_cpf');?>",
						async: true,
						data: { cpf 	: 	cpf }
					}).success(function( data ) {
						var dados = $.parseJSON(data);
							if(dados.status == 'erro'){
								swal(
							  		'Ops!',
							  		dados.mensagem,
							  		'error'
								)
								$('#cpf').val('');	
							}
						
					});
				}
			});

			$('#email_pessoal').bind('focusout', function(){
				var email = $(this).val();
				if(email != ""){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_email');?>",
						async: true,
						data: { email 	: 	email }
					}).success(function( data ) {
						var dados = $.parseJSON(data);
							if(dados.status == 'erro'){
								swal(
							  		'Ops!',
							  		dados.mensagem,
							  		'error'
								)
								$('#email_pessoal').val('');	
							}
						
					});
				}
			});

			$('#senha2').bind('focusout', function(){		
				var senha 	= 	$('#senha').val();
				var senha2 	= 	$('#senha2').val();
				if( senha != senha2 ){
					swal(
				  		'Ops!',
				  		'Senha não confere, tente novamente',
				  		'error'
					)
					$('#senha').val('');
					$('#senha2').val('');
				}	
			});

		});
	</script>
    <!-- Main JS Initialization File -->
    <!--<script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>-->
    
    
</body>

</html>