<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-list-2"></i>
					</span>
					<h3 class="m-portlet__head-text">
						
						Orçamento #<?php echo $dados[0]->id_orcamento; ?>						
					</h3>
				</div>			
			</div>
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<?php if($dados[0]->status_orcamento_id != 7){ ?>
					<li class="m-portlet__nav-item">
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="geraPdf" orcamento_id="<?php echo $dados[0]->id_orcamento; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>' data-toggle="modal" data-target="#m_modal_10"  title="Gerar PDF do Orçamento">
							<i class="la la-print"></i>
						</a>
						 <input type="hidden" id="contato_posto" name="contato_posto" value="<?php echo ($dados[0]->contato_do_posto == "") ? $dados[0]->contato_posto : $dados[0]->contato_do_posto; ?>">	
					</li>
				<?php } ?>
				</ul>
			</div>
		</div>
		<div class="m-portlet__body">		
			<div class="row">
				<div class="col-lg-4 col-xs-12"> 
					<input type="hidden" id="fl_especial" value="<?php echo $dados[0]->fl_especial; ?>">
					<h5><span class="m--font-warning m--font-boldest">Empresa/Cliente: </span>							
						<?php echo $dados[0]->cnpj.' | '.$dados[0]->razao_social.' | '.$dados[0]->fantasia;?>						
					</h5>
				</div>							
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">Telefone: </span>
						<?php echo $dados[0]->telefone;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">E-mail: </span>
						<?php echo $dados[0]->email;?>
					</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Enderço:</span>
						 <?php echo $dados[0]->endereco;?>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Bairro:</span>
						 <?php echo $dados[0]->bairro;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Cep:</span>
						 <?php echo $dados[0]->cep;?>
					</h5>
				</div>	
			</div>
			<div class="row">				
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Cidade:</span>
						 <?php echo $dados[0]->cidade;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Estado:</span>
						 <?php echo $dados[0]->estado;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Estado:</span>
						 <?php echo $dados[0]->pais;?>
					</h5>
				</div>								
			</div>
			
		</div>
	</div>
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>Produtos</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<table class="table m-table m-table--head-separator-warning">
			  	<thead>
			    	<tr>
			      		<th>#</th>			      		
			      		<th>Modelo</th>
			      		<th>Descrição</th>
			      		<th>Qtd</th>
			      		<?php if($dados[0]->status_orcamento_id != 7){ ?>			      					      		
			      		<th>Valor</th>	
			      		<?php } ?>	
			    	</tr>
			  	</thead>
			  	<tbody>
			  		<?php $i=1;$total=0; foreach($dados as $dado){?>
			    	<tr>
				      	<th scope="row"><?php echo $i;?></th>				      	
				      	<td><?php echo $dado->modelo; ?></td>
				      	<td><?php echo $dado->descricao; ?></td>
				      	<td><?php echo $dado->qtd; ?></td>				      	
				      	<?php if($dados[0]->status_orcamento_id != 7){ 
				      		$total = $total + ($dado->valor * $dado->qtd);
				      		?>			      					      		
			      			<td>R$ <?php echo number_format($dado->valor,2,",","."); ?></td>	
			      		<?php } ?>
			    	</tr>
			    	<?php $i++; } ?>			    	
			    	<?php if($dados[0]->status_orcamento_id != 7){ ?>
			    	<tr style="text-align: right;">
			    		<td colspan="5"><b>Total: R$ <?php echo number_format($total,2,",",".");?></b></td>
			    	</tr>
			    	<?php } ?> 
			  	</tbody>
			</table>
		</div>
	</div>
</div>	
<!-- Modal Andamento -->
<div class="modal fade" id="modal_reemissao" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">		
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Motivo da reemissão do orçamento</h5>
			</div>
			<div class="modal-body" style="width: 750px;">				
				<div class="form-group m-form__group">
					<div class="form-group m-form__group" >
						<label for="exampleSelect1"></label>
						<textarea type="text" class="form-control m-input m-input--air" id="motivo_emissao" placeholder="Motivo"></textarea>
					</div>
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" id ="adicionar_motivo" class="btn btn-primary" status_orcamento="<?php echo $dados[0]->status_orcamento_id; ?>" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>">Adicionar Andamento</button>
			</div>
		</div>
	</div>		
</div>