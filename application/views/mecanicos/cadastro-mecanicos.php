<!DOCTYPE html>
<html lang="pt-br">


<!-- Mirrored from celtano.top/salimo/demos/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
<head>
    <meta charset="utf-8" />
    <title>WERTCO - Cadastro de Mecânico</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon.png')?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />


    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

</head>

<body class="double-diagonal blog-page dark">
    <!-- Preloader Starts -->
    <!--<div class="preloader" id="preloader">
        <div class="logopreloader">
            <img src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-black">
        </div>
        <div class="loader" id="loader"></div>
    </div>
    <!-- Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                    <div class="logo">
                        <a class="navbar-brand link-menu nav-external" >
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external"  href="<?=base_url('/home/index')?>"><i class="fa fa-arrow-left"></i> Voltar ao Site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		<!-- Banner Starts -->
        <section class="banner">
			<div class="content text-center">
				<div class="text-center top-text">
                    <h1>Cadastro de Mecânico</h1>
					<hr>
                    <h4></h4>
                </div>
			</div>
		</section>
		<!--    Banner Ends        -->
        <!--    Services Begin     -->
        <section id="clientes" class="services">
            <!-- Container Starts -->
            <div class="container" style="background: #333;border-radius: 10px;">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Dados</span> da empresa</h1>
                    <h4></h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
					<span class="outer-line"></span>
					<img src="<?=base_url('bootstrap/img/bombaIcoG.png')?>" />
					<span class="outer-line"></span>
				</div>
                <!-- Divider Ends -->
                <!-- Services Starts -->
				<div class="cadastros">
					<form accept-charset="utf-8" action="<?php echo base_url('mecanicos/add/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;">
					
					<div class="row dados">  						
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control cnpj" id="cnpj" maxlength="15" name="cnpj" placeholder="cnpj" required />	
							<input type="hidden"  id="salvar" maxlength="15" name="salvar" value="1"	required />
							<input type="hidden"  id="empresa_id" maxlength="15" name="empresa_id" value=""  />
						</div>                    
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control" id="razao_social" maxlength="250" name="razao_social" placeholder="Razão Social" value="" required />	
						</div>                    
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control" id="fantasia" maxlength="250" name="fantasia" placeholder="fantasia" placeholder="Nome Fantasia" value="" />	
						</div>                    
					</div>
					<div class="row dados">  						

						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control telefone" id="telefone" maxlength="250" name="telefone" placeholder="telefone" value=""  />	
						</div>   
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="email" class="form-control" id="email" maxlength="250" name="email" placeholder="E-mail" value="" required />	
						</div>                    
						<div class="col-md-4 col-lg-4 col-sm-12 ">
							<input type="text" class="form-control" id="endereco" maxlength="250" name="endereco" placeholder="Endereço" value="" required />	
						</div> 						
					</div>					
					<div class="row dados">  						
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" name="cidade" placeholder="Cidade" value="" required />	
						</div>                    
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" name="estado" placeholder="Estado" value="" required />	
						</div>		
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" name="pais" placeholder="País" value="" required />	
						</div>				
					</div>
					<div class="row dados">  						
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" name="regiao_atua" placeholder="Região que Atua" value="" required />	
						</div>
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" name="credenciamento_inmetro" placeholder="Credenciamento Inmetro" value="" required />	
						</div>
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" name="credenciamento_crea" placeholder="Credenciamento Crea" value="" />	
						</div>										
					</div>				
					
					<div class="text-center top-text" style="margin-top: 50px;">
						<h1><span>Dados</span> Pessoais</h1>
						<h4>Dados para acesso ao site</h4>
					</div>
					<div class="divider text-center">
						<span class="outer-line"></span>
						<span class="fa fa-user" aria-hidden="true"></span>
						<span class="outer-line"></span>
					</div>
					<div class="row dados">  						
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" name="pessoal_nome" placeholder="Nome" value="" required />	
						</div>                    
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control" id="cpf" maxlength="250" name="pessoal_cpf" placeholder="CPF" value="" required />	
						</div>
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control telefone" id="pessoal_telefone" maxlength="250" name="pessoal_telefone" placeholder="Telefone" value="" />	
						</div>
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="email" class="form-control" id="email_pessoal" maxlength="250" name="pessoal_email" placeholder="E-mail Pessoal" value="" required />	
						</div>                     
					</div>
					
					<div class="row dados"> 
						<div class="col-md-6 col-lg-6 col-sm-12">
							<input type="password" class="form-control" id="senha" maxlength="250" name="senha" placeholder="Senha" value="" required />	
						</div>                    
						<div class="col-md-6 col-lg-6 col-sm-12">
							<input type="password" class="form-control" id="senha2" maxlength="250" name="senha2" placeholder="Confirme sua senha" value="" required />	
						</div>		
					</div>						
					<div class="row dados">  						
						                   
						<div class="col-md-4 col-lg-4 col-sm-12 offset-md-4" style="text-align: center; margin: 0 auto;">
							<button type="submit" class="btn btn-outline-secondary btn-lg"  maxlength="250" name="enviar" id="enviar" placeholder="Enviar" value="" required> Enviar </button>
							<button type="reset"  class="btn btn-outline-secondary btn-lg"  maxlength="250" name="limpar" placeholder="Enviar" value="" required> Limpar </button>

						</div>		
						
					</div>
					</form>
				</div>
            </div>
                <!-- Services Ends -->
        </section>
        <!-- Service Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center footer-area-restrita" style="border-top: 2px solid #ffcc00 !important;">
            <!-- Container Starts -->
            <div class="container">
               <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/00001-53 </p>
                <p>
                    &copy; Copyright 2017 Wertco 
                </p>
              
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
    </div>
    <!-- Wrapper Ends -->
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>       
	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>		
	<script type="text/javascript" src="<?=base_url('bootstrap/js/sweetalert.js')?>"></script>

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		},function(){
			window.open(base_url+'home/');
		});
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ok!',
	  		'Cadastro realizado com sucesso!',
	  		'success'
		)
	</script>	
<?php unset($_SESSION['sucesso']); } ?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#cnpj').mask('00.000.000/0000-00');
			$('#cpf').mask('000.000.000-00');
			$('#cep').mask('00000-000');
			$('#bicos').mask('00000');	
			$('.telefone').mask('(00) 0000 - 00000');

			$('#cnpj').bind('focusout', function(){	
				var cnpj = $(this).val(); 
				if(cnpj != ""){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_cnpj');?>",
						async: true,
						data: { cnpj 	:	cnpj }
						}).success(function( data ) {
							var dados = $.parseJSON(data);		 

							if(dados.length > 0){
								
								$('#razao_social').val(dados[0].razao_social);		
								$('#razao_social').attr('disabled', true);		
								$('#fantasia').val(dados[0].fantasia);		
								$('#fantasia').attr('disabled', true);
								$('#telefone').val(dados[0].telefone);
								$('#telefone').attr('disabled', true);
								$('#endereco').val(dados[0].endereco);		
								$('#endereco').attr('disabled', true);	
								$('input[name="posto_rede"]').attr('disabled', true);
								$('#nr_bombas').val(dados[0].nr_bombas);
								$('#nr_bombas').attr('disabled',true); 
								$('#bicos').val(dados[0].bicos);
								$('#bicos').attr('disabled',true);
								$('input[type="checkbox"]').attr('disabled',true);
								$('select').attr('disabled',true);
								$('input[name="software_utilizado"]').attr('disabled',true);
								$('input[name="software_utilizado"]').val(dados[0].software);
								$('input[name="mecanico_atende"]').attr('disabled',true);
								$('input[name="mecanico_atende"]').val(dados[0].mecanico_atende);
								$('#salvar').val('0');
								$('#empresa_id').val(dados[0].id);
								
							}else{
								$('#razao_social').attr('disabled', false);		
								$('#razao_social').val('');
								$('#fantasia').attr('disabled', false);
								$('#fantasia').val('');
								$('#telefone').attr('disabled', false);
								$('#telefone').val('');
								$('#endereco').attr('disabled', false);	
								$('#endereco').val('');	
								$('input[name="posto_rede"]').attr('disabled', false);
								$('input[name="posto_rede"]').val('');
								$('#nr_bombas').attr('disabled',false);			
								$('#nr_bombas').val('');			
								$('#bicos').attr('disabled',false);
								$('#bicos').val('');
								$('input[type="checkbox"]').attr('disabled',false);
								$('input[type="checkbox"]').val('');
								$('select').attr('disabled',false);

								$('input[name="software_utilizado"]').attr('disabled',false);
								$('input[name="software_utilizado"]').val('');
								$('input[name="mecanico_atende"]').val('');
								$('input[name="mecanico_atende"]').attr('disabled',false);
								$('#salvar').val('1');

							}							
					});	
				}
			});

			$('#cpf').bind('focusout', function(){
				var cpf = $(this).val();
				if(cpf != ""){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_cpf');?>",
						async: true,
						data: { cpf 	: 	cpf }
					}).success(function( data ) {
						var dados = $.parseJSON(data);
							if(dados.status == 'erro'){
								swal(
							  		'Ops!',
							  		dados.mensagem,
							  		'error'
								)
								$('#cpf').val('');	
							}
						
					});
				}
			});

			$('#senha2').bind('focusout', function(){		
				var senha 	= 	$('#senha').val();
				var senha2 	= 	$('#senha2').val();
				if( senha != senha2 ){
					swal(
				  		'Ops!',
				  		'Senha não confere, tente novamente',
				  		'error'
					)
					$('#senha').val('');
					$('#senha2').val('');
				}	
			});

		});
	</script>
    <!-- Main JS Initialization File -->
    <!--<script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>-->
    
    
</body>

</html>