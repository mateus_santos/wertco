	<!DOCTYPE html>
	<html lang="en">


	<!-- Mirrored from celtano.top/salimo/demos/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
	<head>
	    <meta charset="utf-8" />
	    <title><?php echo $dados[0]['descricao']; ?></title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

	    <!-- Google Fonts 
	    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
		-->
	    <!-- Favicon -->
	    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon.png')?>">

	    <!-- Template CSS Files -->
	    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
	    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
	    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
	    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
	    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />


	    <!-- Template JS Files -->
	    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

	</head>

	<body class="double-diagonal blog-page dark">
	    <!-- Preloader Starts -->
	    <!--<div class="preloader" id="preloader">
	        <div class="logopreloader">
	            <img src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-black">
	        </div>
	        <div class="loader" id="loader"></div>
	    </div>
	    <!-- Preloader Ends -->
	    
	    <!-- Page Wrapper Starts -->
	    <div class="wrapper">
	    	<div id="loading" style=" text-align: center;width: 100%;height: 100%; display: none; background: #cccccc45;z-index: 9;/* top: 0; */position: absolute;">
	    		<div style="position: relative;top: 63%;  background: #ccc; padding: 20px; text-align: center;">
		    		<h2 style="color: #000;" id="msg_loading"> Aguarde, estamos enviando seu arquivo... </h2>
		    		<img src="http://localhost/wertco-site/bootstrap/img/loading.gif" style="height: 70px;">
	    		</div>
	    	</div>
	        <!-- Header Starts -->
	        <header id="header" class="header">
	            <div class="header-inner">
	                <!-- Navbar Starts -->
	                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
						<!-- Logo Starts -->
	                    <div class="logo">
	                        <a class="navbar-brand link-menu nav-external" >
	                            <!-- Logo White Starts -->
	                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
	                            <!-- Logo White Ends -->
	                            
	                        </a>
	                    </div>
						<!-- Logo Ends -->
						<!-- Hamburger Icon Starts -->
	                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span id="icon-toggler">
							  <span></span>
							  <span></span>
							  <span></span>
							  <span></span>
							</span>
						</button>
						<!-- Hamburger Icon Ends -->
						<!-- Navigation Menu Starts -->
	                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
	                        <ul class="nav-menu-inner ml-auto">
	                            <li><a class="link-menu nav-external"  href="<?=base_url('/home/index')?>"><i class="fa fa-arrow-left"></i> Voltar ao Site</a></li>
	                        </ul>
	                    </div>
						<!-- Navigation Menu Ends -->
	                </nav>
	                <!-- Navbar Ends -->
	            </div>
	        </header>
			<!-- Banner Starts -->
	        <section class="banner">
				<div class="content text-center">
					<div class="text-center top-text">
	                    <h1><?php echo $dados[0]['descricao']; ?></h1>
						<hr/>
						<!--<img src="<?php echo base_url('bootstrap/img/abieps.png'); ?>" width="200" />-->
	                    <h4></h4>
	                </div>
				</div>
			</section>
			<!--    Banner Ends        -->
	        <!--    Services Begin     -->
	        <section id="clientes" class="services">
	            <!-- Container Starts -->
	            <div class="container" style="background: #333;border-radius: 10px;">
	            	 <!-- Main Heading Starts  
						******************** VERIFICA TOTAL DE INSCRIÇões *****************
						*******************************************************************	
					-->
	            	 <?php  if($dados[0]['total_vagas'] <= $total_aprovados){ ?>
	            	 	<div class="text-center top-text">
		                    <h1><span>Atenção</span></h1>
		                    <h4 style="margin-top: 20px;margin-bottom: 50px;line-height: 1.5;">
		                    	Vagas Encerradas, aguarde nova oportunidade!
							</h4>
	                	</div>
	            	 <?php } else { ?>		                
	               
	                <!-- Main Heading Starts  -->
	                <div class="text-center top-text">
	                    <h1><span>Dados</span> Pessoais</h1>
	                    <h4></h4>
	                </div>
	                <!-- Main Heading Starts -->
	                <!-- Divider Starts -->
	                <div class="divider text-center">
						<span class="outer-line"></span>
						<img src="<?=base_url('bootstrap/img/bombaIcoG.png')?>" />
						<span class="outer-line"></span>
					</div>
	                <!-- Divider Ends -->
	                <!-- Services Starts -->
					<div class="cadastros">
						<form accept-charset="utf-8" autocomplete="off" action="<?php echo base_url('treinamento/add/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;"  id="formulario"> 
						
						<div class="row dados"> 
							<div class="col-md-3 col-lg-3 col-sm-12">
								<input type="text" autocomplete="off" class="form-control" id="cpf_fake" maxlength="20" name="cpf" placeholder="CPF" value="" style="display: none;" />	
								<input type="text" autocomplete="off" class="form-control" id="cpf" maxlength="20" name="cpf" placeholder="CPF" value="" required />	
							</div> 						
							<div class="col-md-3 col-lg-3 col-sm-12">
								<input type="text" class="form-control" id="nome" maxlength="100" name="nome" placeholder="Nome" value="" required />	
								<input type="hidden" class="form-control"  maxlength="1000" name="descricao_treinamento" placeholder="Nome" value="<?php echo $dados[0]['descricao']; ?>" required />	
								<input type="hidden" class="form-control"  maxlength="100" name="treinamento_id" id="treinamento_id" placeholder="Nome" value="<?php echo $dados[0]['id']; ?>" required />	
								<input type="hidden" class="form-control"  maxlength="1000" name="usuario_id" id="usuario_id" value="" />
								
							</div>
							<div class="col-md-3 col-lg-3 col-sm-12">
								<input type="text" class="form-control" id="sobrenome"  maxlength="100" name="sobrenome" placeholder="Sobrenome" value="" required />	
							</div>							
							<div class="col-md-3 col-lg-3 col-sm-12">
								<input type="text" class="form-control" id="rg" maxlength="20" name="rg" required placeholder="RG" value="" />	
							</div>
						</div>	


						<div class="row dados">
							<div class="col-md-4 col-lg-4 col-sm-12">
								<input type="email" class="form-control" placeholder="E-mail" name="email" id="email_fake" class="hidden" autocomplete="off" style="display: none;" />
  								<input type="email" class="form-control" placeholder="E-mail" name="email" id="email" autocomplete="off" />
							</div>
							<div class="col-md-4 col-lg-4 col-sm-12">
								<input type="text" class="form-control telefone" id="telefone" required maxlength="20" name="telefone" placeholder="Telefone" value="" />	
							</div>  
							<div class="col-md-4 col-lg-4 col-sm-12">
								<input type="text" class="form-control telefone" id="celular" required maxlength="20" name="celular" placeholder="Celular/Whatsapp" value="" />	
							</div>                   
						</div>	
						<div class="row dados"> 
							<div class="col-md-6 col-lg-6 col-sm-12">
								<input type="password" class="form-control" id="senha_fake" autocomplete="off" maxlength="250" name="senha" placeholder="Digite uma senha" value=""  style="display: none;" />	
								<input type="password" class="form-control" autocomplete="off" id="senha" maxlength="250" name="senha" placeholder="Digite uma senha" value="" required />	
							</div>
							<div class="col-md-6 col-lg-6 col-sm-12">
								<input type="password" class="form-control" id="senha2" required maxlength="20" name="senha2" placeholder="Repita a sua senha"  />	
							</div>  
						</div>
						<div class="row dados">
							&nbsp;
						</div>
						 <!-- Main Heading Starts -->
		                <div class="text-center top-text">
		                    <h1><span>Dados da</span> Empresa</h1>
		                    <h4></h4>
		                </div>
		                <!-- Main Heading Starts -->
		                <!-- Divider Starts -->
		                <div class="divider text-center" style="margin-bottom: 50px;">
							<span class="outer-line"></span>
							<img src="<?=base_url('bootstrap/img/bombaIcoG.png')?>" />
							<span class="outer-line"></span>
						</div>
		                <!-- Divider Ends -->
						<div class="row dados"> 						
							

							<div class="col-md-4 col-lg-4 col-sm-12">
								<input type="text" class="form-control cnpj" id="cnpj" maxlength="15" name="cnpj" placeholder="cnpj" required />		
								<input type="hidden" class="form-control" id="empresa_id" name="empresa_id" maxlength="15" value="" />		
							</div>                    
							<div class="col-md-4 col-lg-4 col-sm-12">
								<input type="text" class="form-control" id="razao_social" maxlength="250" name="razao_social" placeholder="Razão Social" value="" required />	
							</div>                    
							<div class="col-md-4 col-lg-4 col-sm-12">
								<input type="text" class="form-control" id="fantasia" required maxlength="250" name="fantasia" placeholder="fantasia" placeholder="Nome Fantasia" value="" />	
							</div> 

							
						</div>
						<div class="row dados">
							<div class="col-md-6 col-sm-12 col-lg-6">

								<input type="text" class="form-control" id="inscricao_estadual" maxlength="50" name="inscricao_estadual" placeholder="inscrição Estadual" placeholder="Inscrição Estadual" value="" required="required" />	

							
							</div>	
							<div class="col-md-6 col-sm-12 col-lg-6">

								<input type="text" class="form-control" id="inscricao_municipal" maxlength="50" name="inscricao_municipal" placeholder="inscricao_municipal" placeholder="Inscrição Municipal" value="" />	
								
							
							</div>	
						</div>
						<div class="row dados">                
							<div class="col-md-3 col-lg-3 col-sm-12 ">
								<input type="text" class="form-control" id="endereco"  name="endereco" placeholder="Endereço Comercial " value="" required />	
							</div> 					
							<div class="col-md-3 col-lg-3 col-sm-12">
								<input type="text" class="form-control" id="numero" maxlength="20" name="numero" placeholder="Nº" value="" required />	
							</div>	
							<div class="col-md-3 col-lg-3 col-sm-12 ">
								<input type="text" class="form-control" id="complemento" maxlength="250" name="complemento" placeholder="Complemento" value=""  />	
							</div> 	
							<div class="col-md-3 col-lg-3 col-sm-12 ">
								<input type="text" class="form-control" id="bairro" maxlength="250" name="bairro" placeholder="Bairro" value="" required />	
							</div> 	
											                   
						</div>					
						<div class="row dados">  
							<div class="col-md-4 col-lg-4 col-sm-12 ">
								<input type="text" class="form-control" id="cep" maxlength="250" name="cep" placeholder="CEP" value="" required />	
							</div> 						
							<div class="col-md-4 col-lg-4 col-sm-12">
								<input type="text" class="form-control" id="cidade"  maxlength="250" name="cidade" placeholder="Cidade" value="" required />	
							</div>                    
							<div class="col-md-4 col-lg-4 col-sm-12">
								<select  class="form-control"  maxlength="250" name="estado" placeholder="Estado" required>
	                                <option value="">Estado</option>
	                                <option value="AC"  >Acre</option>
	                                <option value="AL">Alagoas</option>
	                                <option value="AP">Amapá</option>
	                                <option value="AM">Amazonas</option>
	                                <option value="BA">Bahia</option>
	                                <option value="CE">Ceará</option>
	                                <option value="DF">Distrito Federal</option>
	                                <option value="ES">Espírito Santo</option>
	                                <option value="GO">Goiás</option>
	                                <option value="MA">Maranhão</option>
	                                <option value="MT">Mato Grosso</option>
	                                <option value="MS">Mato Grosso do Sul</option>
	                                <option value="MG">Minas Gerais</option>
	                                <option value="PA">Pará</option>
	                                <option value="PB">Paraíba</option>
	                                <option value="PR">Paraná</option>
	                                <option value="PE">Pernambuco</option>
	                                <option value="PI">Piauí</option>
	                                <option value="RJ">Rio de Janeiro</option>
	                                <option value="RN">Rio Grande do Norte</option>
	                                <option value="RS">Rio Grande do Sul</option>
	                                <option value="RO">Rondônia</option>
	                                <option value="RR">Roraima</option>
	                                <option value="SC">Santa Catarina</option>
	                                <option value="SP">São Paulo</option>
	                                <option value="SE">Sergipe</option>
	                                <option value="TO">Tocantins</option>
	                            </select> 	
							</div>			
						</div>
						<div class="row dados">
							<div class="col-md-6 col-lg-6 col-sm-6">
								<input type="text"  class="form-control" placeholder="CREA"  name="crea" id="cra"  />	
							</div>
							<div class="col-md-6 col-lg-6 col-sm-6">
								
								<input type="text" required="required" class="form-control" placeholder="Inmetro" name="inmetro" id="inmetro"  />	
							</div>
						</div>
						<div class="row dados">
							<div class="col-md-12 col-lg-12 col-sm-12">
								<input type="text"  class="form-control" placeholder="Representante Legal" name="representante_legal" id="representante_legal" />
							</div>
						</div>
						<div class="row dados">
							<div class="col-md-12 col-lg-12 col-sm-12">
								<label> Interesse em: </label>
								<div class="form-check form-check-inline">
								  <label class="form-check-label">
									<input class="form-check-input" type="radio" name="interesse" id="inlineRadio1" value="Indicação"  /> Indicação
								  </label>
								</div>
								<div class="form-check form-check-inline">
								  <label class="form-check-label">
									<input class="form-check-input" type="radio" name="interesse" id="inlineRadio2" value="Técnico Instalador" checked /> Técnico Instalador
								  </label>
								</div>
							</div>
						</div>	
						<div class="row dados"> 						 
							                  
							<div class="col-md-4 col-lg-4 col-sm-12 offset-md-4" style="text-align: center; margin: 0 auto;">								
								<button type="submit" class="btn btn-outline-secondary btn-lg"  maxlength="250" name="enviar" id="enviar" placeholder="Enviar" value="" required> Enviar </button>
								<button type="reset"  class="btn btn-outline-secondary btn-lg"  maxlength="250" name="limpar" placeholder="Enviar" value="" > Limpar </button>

							</div>		
							
						</div>
						</form>
					</div>
					<?php } ?>
	            </div>
	                <!-- Services Ends -->
	                
	        </section>
	        <!-- Service Section Ends -->
	        <!-- Footer Section Starts -->
	        <footer class="footer text-center footer-area-restrita" style="border-top: 2px solid #ffcc00 !important;">
	            <!-- Container Starts -->
	            <div class="container">
	               <!-- Copyright Text Starts -->
	                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/0001-53 </p>
	                <p>
	                    &copy; Copyright <?php date('Y'); ?> Wertco 
	                </p>
	              
	            </div>
	            <!-- Container Ends -->
	        </footer>
	        <!-- Footer Section Starts -->
	    </div>

	    <!-- Wrapper Ends -->
	    <!-- Template JS Files -->
	    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
	    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
	    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
	    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
	    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>       
		<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>		
		<script type="text/javascript" src="<?=base_url('bootstrap/js/sweetalert.js')?>"></script>
		<script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/AjaxFileUpload.js')?>"></script>
	<?php if ($this->session->flashdata('erro')){ ?>
		<script type="text/javascript"> 	
			swal("Atenção!", <?php echo $this->session->flashdata('erro'); ?>, "warning")

		</script>
	<?php } ?>
	<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
			   		title: "OK!",
			   		text: 'Cadastro realizado com sucesso!',
			   		type: 'success'
		    	}).then(function() {
		    	   	window.location.href = "http://www.wertco.com.br";
	    		}); 
		</script>	
	<?php unset($_SESSION['sucesso']); } ?>

		<script type="text/javascript">
			$(document).ready(function(){
				$('#email').attr('autocomplete','off');	 
				setTimeout(function(){ $('#email').val('');	 
				$('#senha').val('');	  }, 2000);

				
				$('#formulario').submit(function(){
					 
					$('#enviar').attr('disabled', 'disabled');
					 
				});
				

				$('#cnpj').mask('00.000.000/0000-00');
				$('#cpf').mask('000.000.000-00');
				$('#cep').mask('00000-000');
				$('#bombas').mask('00000');
				$('#bicos').mask('00000');	
				$('.telefone').mask('(00) 0000 - 00000');

				$('#cnpj').bind('focusout', function(){	
				var cnpj 			= $(this).val(); 
				var treinamento_id 	= $(this).val();
				if(isCNPJValid(cnpj)){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_cnpj');?>",
						async: true,
						data: { cnpj 	:	cnpj }
						}).success(function( data ) {
							var dados = $.parseJSON(data);		 

							if(dados.length > 0){
								
								$('#empresa_id').val(dados[0].id);		
								
								$('#razao_social').val(dados[0].razao_social);		
								$('#razao_social').attr('readonly', true);									
								$('#fantasia').val(dados[0].fantasia);		
								$('#fantasia').attr('readonly', true);								
								$('#endereco').val(dados[0].endereco);		
								$('#endereco').attr('readonly', true);	
								$('#inscricao_estadual').val(dados[0].insc_estadual);		
								$('#inscricao_estadual').attr('readonly', true);

								$('#representante_legal').val(dados[0].representante_legal);	
								$('#representante_legal').attr('readonly', true);	
								$('#bairro').val(dados[0].bairro);		
								$('#bairro').attr('readonly', true);	
								$('#cep').val(dados[0].cep);		
								$('#cep').attr('readonly', true);	
								$('#cidade').val(dados[0].cidade);		
								$('#cidade').attr('readonly', true);	
								$('#estado').val(dados[0].estado);		
								$('#estado').attr('readonly', true);												
								$('#inmetro').val(dados[0].credenciamento_inmetro);
								$('#inmetro').attr('readonly', false);
								$('#crea').val(dados[0].credenciamento_crea);
								$('#crea').attr('readonly', true);
								
							}else{ 
								$('#razao_social').attr('readonly', false);		
								$('#razao_social').val('');
								$('#fantasia').attr('readonly', false);
								$('#fantasia').val('');
								$('#endereco').attr('readonly', false);	
								$('#endereco').val('');		
								$('#endereco').val('');		
								$('#endereco').attr('readonly', false);
								$('#representante_legal').val('');	
								$('#representante_legal').attr('readonly', false);	
								$('#bairro').val('');		
								$('#bairro').attr('readonly', false);	
								$('#cep').val('');		
								$('#cep').attr('readonly', false);	
								$('#cidade').val('');		
								$('#cidade').attr('readonly', false);	
								$('#estado').val('');		
								$('#estado').attr('readonly', false);
								$('#inmetro').val('');		
								$('#inmetro').attr('readonly', false);
								$('#crea').val('');		
								$('#crea').attr('readonly', false);

							}							
					});	
				} else {
					swal({
						title: 'CNPJ inválido!',
						text: 'Favor verificar o CNPJ informado',
						type:'error'
					}).then(function(isConfirm) {
						$('#cnpj').focus();
					})
				}
			});
			
			function isCNPJValid(cnpj) {
			
				var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
				if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
					return false;
				for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
				if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
					return false; 
				for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
				if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
					return false; 
				return true; 
			};



				$('#cpf').bind('focusout', function(){
					var cpf = $(this).val();
					var treinamento_id = $('#treinamento_id').val();
					
					if(cpf != ""){

						$.ajax({
							method: "POST",
							url: "<?php echo base_url('Treinamento/verifica_cpf');?>",
							async: true,
							data: { cpf 	: 	cpf,
									id 		:	treinamento_id}
						}).success(function( data ) {
							var dados = $.parseJSON(data);						

							if(dados.status == 'erro'){
								swal(
							  		'Ops!',
							  		dados.mensagem,
							  		'error'
								)
								$('#cpf').val('');	
							}else{
								$('input').attr('readonly',true);
								$('#cpf').attr('readonly',false);
								$('#celular').attr('readonly',false);
								$('#usuario_id').val(dados.id);
								$('#nome').val(dados.nome);	
								$('#email').val(dados.email);		
								$('#razao_social').val(dados.razao_social);		
								$('#cnpj').val(dados.cnpj);		
								$('#fantasia').val(dados.fantasia);		
								$('#telefone').val(dados.telefone);
								$('#endereco').val(dados.endereco);
								$('#inscricao_estadual').val(dados.insc_estadual);										
								$('#representante_legal').val(dados.representante_legal);								
								$('#bairro').val(dados.bairro);		
								$('#cep').val(dados.cep);		
								$('#cidade').val(dados.cidade);	
								$('#estado').val(dados.estado);	
								$('#inmetro').val(dados.credenciamento_inmetro);		
								$('#crea').val(dados.credenciamento_crea);				
								$('#salvar').val('0');
								$('#empresa_id').val(dados.empresa_id);	
								$('#arquivo').attr('readonly',false);
							}
							
						});
					
					}else{
						$('input').each(function(){
							
							if( $(this).attr('name') != 'descricao_treinamento' && $(this).attr('name') != 'treinamento_id' ){ 
								$(this).val('');
							}
						});
						
						$('input').attr('readonly', false);
					}
				});

				$('#email').bind('focusout', function(){
					var email = $(this).val();
					if(email != "" && $('#senha').val() == ""){
						$.ajax({
							method: "POST",
							url: "<?php echo base_url('clientes/verifica_email');?>",
							async: true,
							data: { email 	: 	email }
						}).success(function( data ) {
							var dados = $.parseJSON(data);
								if(dados.status == 'erro'){
									swal(
								  		'Ops!',
								  		dados.mensagem,
								  		'error'
									)
									$('#email').val('');	
								}
							
						});
					}
				});
				
				$('#senha2').bind('focusout', function(){
					var senha 	= 	$('#senha').val();
					var senha2 	= 	$('#senha2').val();
					if( senha != senha2 ){
						swal(
					  		'Ops!',
					  		'Senha não confere, tente novamente',
					  		'error'
						)
						$('#senha').val('');
						$('#senha2').val('');
					}	
				});

				$('#arquivo').on('change', function(){
					
					$('#loading').show();
					
					$.ajaxFileUpload({
						url 			: 	'<?php echo base_url('Treinamento/uploadArquivo'); ?>', 
						secureuri		: 	false,
						fileElementId	: 	'arquivo',
						dataType		:  	'json',						
						success	: function (data)
						{
							var dados = $.parseJSON(data);
							if(  typeof dados.msg.file_name !== 'undefined' ){							
								$('#arquivo_upload').val(dados.msg.file_name);
								$('#msg_loading').text('Aguarde o envio do arquivo...');
								$('#loading').hide();
								swal(
							  		'Ok!',
							  		'Agora clique em enviar no botão abaixo no formulário para completar a inscrição.',
							  		'success'
								);
								$('#arquivo').attr('disabled',true);
							}else{ 

								$('#loading').hide();
								swal(
							  		'Atenção!',
							  		'Não possível enviar o arquivo, verifique o tamanho e o tipo do arquivo e tente novamente.',
							  		'warning'
								);

								$('#arquivo').val('');

							}
						}
					});
					return false;
					
				});
			});
		</script>
	    <!-- Main JS Initialization File -->
	    <!--<script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>-->
	    
	</body>

	</html>
