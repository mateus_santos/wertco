<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8" />
    <title>WERTCO - Cadastro de Técnicos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon.png')?>">
    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />
    <!-- Template JS Files -->
    <style type="text/css"> 
	   	@media (max-width: 425px) {

	   		.form-control{
	   			margin-bottom: 10px !important;
	   		}
	   		
	   		.about .container, .services .container, .team .container, .blog .container, .contact .container {
    			/*padding: 15px 15px !important;*/
			}

    	}
    </style>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

</head>

<body class="double-diagonal blog-page dark">    
    <!-- Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
    	<div id="loading" style=" text-align: center;width: 100%;height: 100%; display: none; background: #cccccc45;z-index: 9;/* top: 0; */position: absolute;">
    		<div style="position: relative;top: 63%;  background: #ccc; padding: 20px; text-align: center;">
	    		<h2 style="color: #000;" id="msg_loading"> Aguarde, estamos enviando seu arquivo... </h2>
	    		<img src="<?php echo base_url('bootstrap/img/loading.gif'); ?>" style="height: 70px;">
    		</div>
    	</div>
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                    <div class="logo">
                        <a class="navbar-brand link-menu nav-external" >
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->                            
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external"  href="<?=base_url('/home/index')?>"><i class="fa fa-arrow-left"></i> Voltar ao Site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		<!-- Banner Starts -->        
		<!--    Banner Ends        -->
        <!--    Services Begin     -->
        <section id="clientes" class="services">
            <!-- Container Starts -->
            <div class="container" style="background: #333;border-radius: 10px;">                
                <!-- Divider Ends -->
                <!-- Services Starts -->
				<div>
					<form accept-charset="utf-8" id="formulario" action="<?php echo base_url('home/cadastroExpopostos/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;">
					
						<div class="row dados">  						
							<div class="col-md-4 col-lg-4 col-sm-12">
								<input type="text" 	class="form-control" id="nome" maxlength="100" name="nome" placeholder="Nome" required />	

							</div>                    
							<div class="col-md-4 col-lg-4 col-sm-12">
								<input type="text" class="form-control" id="telefone" maxlength="100" name="telefone" placeholder="Telefone" value="" required />	
							</div>                    
							<div class="col-md-4 col-lg-4 col-sm-12">
								<input type="email" class="form-control" id="email" maxlength="100" name="email" placeholder="E-mail" value="" required />		
							</div>                    						
							<div class="col-md-6 col-lg-6 col-sm-12">
								<input type="text" class="form-control" id="cidade" maxlength="100" name="cidade" placeholder="Cidade" value=""  />	
							</div>   
							<div class="col-md-6 col-lg-6 col-sm-12">
								<select  class="form-control"  maxlength="100" name="estado" placeholder="Estado" required id="estado">
									<option value="">Estado</option>
									<option value="AC">Acre</option>
									<option value="AL">Alagoas</option>
									<option value="AP">Amapá</option>
									<option value="AM">Amazonas</option>
									<option value="BA">Bahia</option>
									<option value="CE">Ceará</option>
									<option value="DF">Distrito Federal</option>
									<option value="ES">Espírito Santo</option>
									<option value="GO">Goiás</option>
									<option value="MA">Maranhão</option>
									<option value="MT">Mato Grosso</option>
									<option value="MS">Mato Grosso do Sul</option>
									<option value="MG">Minas Gerais</option>
									<option value="PA">Pará</option>
									<option value="PB">Paraíba</option>
									<option value="PR">Paraná</option>
									<option value="PE">Pernambuco</option>
									<option value="PI">Piauí</option>
									<option value="RJ">Rio de Janeiro</option>
									<option value="RN">Rio Grande do Norte</option>
									<option value="RS">Rio Grande do Sul</option>
									<option value="RO">Rondônia</option>
									<option value="RR">Roraima</option>
									<option value="SC">Santa Catarina</option>
									<option value="SP">São Paulo</option>
									<option value="SE">Sergipe</option>
									<option value="TO">Tocantins</option>
								</select> 
							</div>                    
						</div>						
						<div class="row dados">  						
							<div class="col-md-4 col-lg-4 col-sm-12 offset-md-4" style="text-align: center; margin: 0 auto;">
								<button type="submit" class="btn btn-outline-secondary btn-lg"  maxlength="250" name="enviar" id="enviar" placeholder="Enviar" value="" required> Enviar </button>
								<button type="reset"  class="btn btn-outline-secondary btn-lg"  maxlength="250" name="limpar" placeholder="Enviar" value="" required> Limpar </button>
							</div>		
						</div>
					</form>
				</div>				
            </div>
                <!-- Services Ends -->
        </section>
        <!-- Service Section Ends -->
        <!-- Footer Section Starts -->       
        <!-- Footer Section Starts -->
    </div>
    <!-- Wrapper Ends -->
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>    
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>    
	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>		
	<script type="text/javascript" src="<?=base_url('bootstrap/js/sweetalert.js')?>"></script>	

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
		   		title: "OK!",
		   		text: 'Acesse seu e-mail e confira o material que preparamos para você!',
		   		type: 'success'
	    	}).then(	function() {
	    		window.location.href = 'https://www.wertco.com.br/';
    		}); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>


	<script type="text/javascript">
		$(document).ready(function(){
			
			
			$('#telefone').mask('(00) 00000 - 0000');

			$('#formulario').submit(function(){
					 
				$('#enviar').attr('disabled', 'disabled');
					 
			});
		});
			
			
	</script>
    <!-- Main JS Initialization File -->
    <!--<script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>-->
    
    
</body>

</html>
