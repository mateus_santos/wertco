<div class="m-content">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<a href="<?php echo base_url('areaRepresentantes/areaAtuacao'); ?>"><i class="la la-arrow-left"></i></a>
					</span>
					<h3 class="m-portlet__head-text">	
						<a href="<?php echo base_url('areaRepresentantes/areaAtuacao'); ?>">Voltar</a>	Cadastro de Áreas de Atuação					
					</h3>
									
				</div>				
			</div>
			<span style="float: left; padding: 20px 11px; " >
				<a href="<?=base_url('downloads/lista-cidades-brasil.xls')?>" class="btn btn-outline-warning btn-sm m-btn m-btn--icon">
					<i class="la la-file-excel-o"></i>Lista das Cidades
				</a>
				<a href="<?=base_url('downloads/modelo-planilha-municipios.xls')?>" class="btn btn-outline-success btn-sm m-btn m-btn--icon">
					<i class="la la-file-excel-o"></i>Modelo de Planilha
				</a>
			</span>	
			<span style="float: right;padding: 10px 11px;" >
				<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air">
					<i class="la la-info"></i>
				</a>	
			</span> 
		</div> 
		<!--begin::Form-->
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaRepresentantes/cadastraArea');?>" method="post" enctype="multipart/form-data" id="form_area">
			<div class="m-portlet__body">	
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
						<label>Estado:</label>
						<div class="m-input-icon m-input-icon--left">
							<select  class="form-control"  maxlength="250" name="estado" placeholder="Estado" id="estado" >
                                <option value="">Selecione um Estado</option>
                                <option value="AC" codigo="12">Acre</option>
                                <option value="AL" codigo="27">Alagoas</option>
                                <option value="AP" codigo="16">Amapá</option>
                                <option value="AM" codigo="13">Amazonas</option>
                                <option value="BA" codigo="29">Bahia</option>
                                <option value="CE" codigo="23">Ceará</option>
                                <option value="DF" codigo="53">Distrito Federal</option>
                                <option value="ES" codigo="32">Espírito Santo</option>
                                <option value="GO" codigo="52">Goiás</option>
                                <option value="MA" codigo="21">Maranhão</option>
                                <option value="MT" codigo="51">Mato Grosso</option>
                                <option value="MS" codigo="50">Mato Grosso do Sul</option>
                                <option value="MG" codigo="31">Minas Gerais</option>
                                <option value="PA" codigo="15">Pará</option>
                                <option value="PB" codigo="25">Paraíba</option>
                                <option value="PR" codigo="41">Paraná</option>
                                <option value="PE" codigo="26">Pernambuco</option>
                                <option value="PI" codigo="22">Piauí</option>
                                <option value="RJ" codigo="33">Rio de Janeiro</option>
                                <option value="RN" codigo="24">Rio Grande do Norte</option>
                                <option value="RS" codigo="43">Rio Grande do Sul</option>
                                <option value="RO" codigo="11">Rondônia</option>
                                <option value="RR" codigo="14">Roraima</option>
                                <option value="SC" codigo="42">Santa Catarina</option>
                                <option value="SP" codigo="35">São Paulo</option>
                                <option value="SE" codigo="28">Sergipe</option>
                                <option value="TO" codigo="17">Tocantins</option>
                            </select>
							<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-file-text-o"></i></span></span>
						</div>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-5">
						<label>Cidades:</label>
						<div class="m-input-icon m-input-icon--left">
							<select class="custom-select" id="cidades" multiple style="height: 450px;">
							</select>	
						</div>
					</div>
					<div class="col-lg-2" style="text-align: center;margin: 15% auto;">
						<a class="btn btn-outline-primary btn-lg m-btn m-btn--icon" id="btnLeft">
							<span>
								<i class="la la-arrow-left" ></i>
							</span>
						</a>
						<a class="btn btn-outline-primary btn-lg m-btn m-btn--icon" id="btnRight">
							<span>
								<i class="la la-arrow-right" ></i>
							</span>
						</a>
					</div>
					<div class="col-lg-5" id="div_selecionados" indice="0">
						<label>Selecionados:</label>
						<div class="m-input-icon m-input-icon--left">
							<select class="custom-select" id="selecionados" name="selecionados[]" multiple style="height: 450px;">
							</select>	
						</div>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
						<label>Importar arquivo:</label>
						<div class="m-input-icon m-input-icon--left">
							<input type="file" class="form-control" id="file" name="arquivo" accept=".xls" />
						</div>
					</div>
				</div>
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<a id="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</a>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
		</div>
</div>

<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-status-title" id="exampleModalLongTitle">
					Tutorial cadastro de áreas de atuação					
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 1024px;height: 530px;">
				<iframe width="100%" height="100%" src="https://www.youtube.com/embed/9m3Bq7fxYyk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>			
		</div>
	</div>
</div>	