<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head" >
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Pedidos &nbsp;&nbsp;

					</h3>
					<div class="m-portlet__head-caption cadastrar_orcamento">													
						<a href="<?php echo base_url('AreaRepresentantes2/cadastraPedidos')?>" class="" style="color: #ffcc00; font-weight: bold;" >
							<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo pedido"></i>
						</a>
					</div>
				</div>
				
			</div>
			
			<div class="m-list__content">
                <div class="m-list-badge" style="margin-top: 25px;">
                	<span class="m-list-badge__item">&nbsp;Pedido Gerado&nbsp;&nbsp;<span class="m-badge m-badge--secondary"></span></span>				    	
					<span class="m-list-badge__item">&nbsp;Pedido Confirmado Pelo Cliente&nbsp;&nbsp;<span class="m-badge m-badge--info"></span></span>
					<span class="m-list-badge__item">&nbsp;Pedido em produção&nbsp;&nbsp;<span class="m-badge m-badge--primary"></span></span>
					<span class="m-list-badge__item">&nbsp;Pedido concluído (produzido)&nbsp;&nbsp;<span class="m-badge m-badge" style="background: #ff9800;"></span></span>
					<span class="m-list-badge__item">&nbsp;Pedido em transporte&nbsp;&nbsp;<span class="m-badge m-badge" style="background: #18cfe6;"></span></span>
					<span class="m-list-badge__item">&nbsp;Pedido entregue&nbsp;&nbsp;<span class="m-badge m-badge--success" ></span></span>
				    <span class="m-list-badge__item">&nbsp;Cancelado&nbsp;&nbsp;<span class="m-badge m-badge--danger"></span></span>
				</div>
			</div>		
			<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: -25px;" data-toggle="m-tooltip" data-placement="top" title="Tour Virtual" id="tour_virtual">
				<i class="la la-info"></i>
			</a>
		
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;"># Pedido</th>
						<th style="text-align: center;"># Orçamento</th>
						<th style="text-align: center;">Cliente</th>											
						<th style="text-align: center;">Situação</th>						
						<th style="text-align: center;">Padrão de Pintura</th>						
						<th style="text-align: center;">Dthr Geração</th>
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				<tbody>					
					<?php foreach($dados as $dado){
						if( $dado->status_pedido_id == 1 ){
							$status = "btn m-btn--pill m-btn--air btn-secondary";
							$background = '';
						}elseif( $dado->status_pedido_id == 2 ){
							$status = "btn m-btn--pill m-btn--air btn-info";
							$background = '';
						}elseif( $dado->status_pedido_id == 3 ){
							$status = "btn m-btn--pill m-btn--air btn-primary";
							$background = '';
						}elseif( $dado->status_pedido_id == 4 ){
							$status = "btn m-btn--pill m-btn--air";
							$background = "style='background: #ff9800;'";
						}elseif( $dado->status_pedido_id == 5 ){
							$status = "btn m-btn--pill m-btn--air";
							$background = "style='background: #18cfe6;'";
						}elseif( $dado->status_pedido_id == 6){
							$status = "btn m-btn--pill m-btn--air btn-success";
							$background = '';
						}elseif($dado->status_pedido_id == 7){
							$status = "btn m-btn--pill m-btn--air btn-danger";
							$background = '';
						}

						?>
						<tr>
							<td style="width: 5%;text-align: center;"><?php echo $dado->id; ?></td>
							<td style="width: 5%;text-align: center;"> <?php echo $dado->orcamento_id; ?></td>
							<td style="width: 40%;text-align: center;"><?php echo $dado->cliente; ?></td>		
							<td style="width: 10%;text-align: center; text-transform: capitalize;">
								<button class="status <?php echo $status; ?>" status="<?php echo $dado->status_pedido_id;?>" <?php echo $background;?> title="Status do orçamento" >
									<?php echo ucfirst($dado->status); ?>								
								</button>
							</td>	
							<td style="width: 10%;text-align: center;">
								<?php echo $dado->pintura; ?>							
							</td>													
							<td style="width: 10%;text-align: center;">
								<?php echo date('d/m/Y H:i:s',strtotime($dado->dthr_geracao)); ?>							
							</td>							
							<td data-field="Actions" class="m-datatable__cell " style="width: 20%;text-align: center !important;">
								<span style="overflow: visible; width: 110px;" class="">								
									<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick="gerarPdf(<?php echo $dado->id; ?>);" pedido_id="<?php echo $dado->id; ?>" >
										<i class="la la-print visualizar"></i>
									</a>
									<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" pedido_id="<?php echo $dado->id; ?>" onclick="verificaOps(<?php echo $dado->status_pedido_id;?>,<?php echo $dado->id; ?>,'<?php echo base_url('AreaRepresentantes2/editarPedidos/'.$dado->id); ?>' );">
										<i class="la la-edit editar"></i>
									</a> 									
								</span>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group status_orcamento_" >
						<label for="exampleSelect1">Situação do Pedido</label>
						<select class="form-control m-input m-input--air" id="status_pedido">
							<option value="">Selecione o Status do Pedido</option>						
							<option value="1">PEDIDO GERADO</option>
							<option value="2">PEDIDO CONFIRMADO PELO CLIENTE</option>
							<option value="3">PEDIDO EM PRODUÇÃO</option>
							<option value="4">PEDIDO CONCLUÍDO (PRODUZIDO)</option>
							<option value="5">PEDIDO EM TRANSPORTE</option>
							<option value="6">PEDIDO ENTREGUE</option>
							<option value="7">CANCELADO</option>
						</select>
					</div>					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" class="btn btn-primary" pedido_id="" indicacao="0">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>	

	<!-- Modal Finalizar pedido -->
	<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-finalizar-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" >
							<label for="exampleSelect1">E-mail do cliente</label>
							<input type="email" style="text-transform: lowercase;" class="form-control m-input--air" value="" id="email" placeholder="E-mail" />
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="finalizar_pedido" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal Anexar nfe ao pedido -->
	<div class="modal fade" id="anexarNfe" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('AreaRepresentantes2/anexarNfe');?>" method="post"  enctype="multipart/form-data">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-anexar-nfe-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="row">
						<div class="form-group m-form__group col-lg-6 col-xs-12">						
							<label for="exampleSelect1">Nota fiscal</label>
							<input type="text" class="form-control m-input--air" name="nr_nf" id="nr_nf" placeholder="Número" required="required" />
							<input type="file" class="form-control m-input--air" name="nfe" id="nfe" placeholder="Anexar Nfe" required="required" />
						</div>	
						<div class="form-group m-form__group col-lg-6 col-xs-12">						
							<label for="exampleSelect1">Prazo de entrega do pedido</label>
							<input type="text" class="form-control m-input--air" name="prazo_entrega" id="prazo_entrega" placeholder="Prazo de entrega" required="required" />
							<input type="hidden" id ="pedido_id"  name="pedido_id" placeholder="Prazo de entrega" />
							<input type="hidden" id ="cliente_id" name="cliente"  />
						</div>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
			</div>			
		</div>
		</form>
	</div>
	<!-- Modal Gerar Startup -->
	<div class="modal fade" id="gerarPdfStartup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('AreaRepresentantes2/gerarFormularioStartup');?>" method="post"  enctype="multipart/form-data">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-anexar-nfe-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="row" id="modelos_nr_series">
						
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" class="cliente_id" value="" name="cliente_id">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
			</div>			
		</div>
		</form>
	</div>
	<!-- 	************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal(
				'Ops!',
				'Aconteceu algum problema, reveja seus dados e tente novamente!',
				'error'
				);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){

				if($this->session->flashdata('pedidoPdf')){ ?>
					<script type="text/javascript">
						window.open('http://www.wertco.com.br/pedidos/<?php echo $this->session->flashdata('pedidoPdf'); ?>');
					</script>	
		<?php	} ?>		 
		<script type="text/javascript">
			
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaRepresentantes2/pedidos';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>