<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaRepresentante2/pedidos'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaRepresentante2/pedidos'); ?>">Voltar</a>						
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaRepresentantes2/cadastraPedidos');?>" method="post" enctype="multipart/form-data">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Orçamento</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="orcamento" 			id="orcamento" class="form-control m-input" placeholder="" >
								<input type="hidden" required name="orcamento_id" 		id="orcamento_id" class="form-control m-input" placeholder="" >
								<input type="hidden" required name="empresa_id" 		id="empresa_id" class="form-control m-input" placeholder="" >
								<input type="hidden" required name="frete_id" 			id="frete_id" class="form-control m-input" placeholder="" >
								<input type="hidden" required name="valor_total" 		id="valor_total" class="form-control m-input" placeholder="" >
								<input type="hidden" required name="valor_total_stx" 	id="valor_total_stx" class="form-control m-input" placeholder="" >
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
							</div>
						</div>						
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>Padrão Pintura:</label>
							<div class="m-input-icon m-input-icon--right">
								<select class="form-control m-input m-input--square" name="pintura" class="pintura" id="exampleSelect1">
									<option value="">
										Selecione uma bandeira
									</option>
									<option value="BR">
										BR
									</option>
									<option value="IPIRANGA">
										Ipiranga
									</option>
									<option value="SHELL">
										Shell
									</option>
									<option value="ALE">
										Ale
									</option>
									<option value="bandeira_branca">
										Bandeira Branca
									</option>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-paint"></i></span></span>
							</div>
							<input type="file" name="arquivo_bb" class="form-control m-input" placeholder="insira o arquivo" id="arquivo_bb" style="display: none;margin-top: 10px;" />		
							<textarea name="pintura_descricao" value="" id="pintura_descricao" class="form-control m-input" placeholder="Descrição da pintura" style="display: none;margin-top: 10px;"></textarea>							
						</div>
						<div class="col-lg-3">
							<label>Motor:</label>
							<div class="m-input-icon m-input-icon--right">
								<select class="form-control m-input m-input--square" name="motor" class="motor" id="exampleSelect2">
									<option value="220V">
										220V
									</option>
									<option value="380V">
										380V
									</option>									
								</select>
							</div>
						</div>
						<div class="col-lg-3">														
							<label>Técnico para startup cadastrado:</label>	
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" class="tecnico" name="tecnico" id="" value="1"  />
									Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" class="tecnico" name="tecnico" id="" value="0" checked="checked">
									Não
									<span></span>
								</label>								
							</div>
							<input type="text" name="tecnico_startup" class="form-control m-input" placeholder="Pesquise o técnico" id="tecnico_startup" style="display: none;margin-top: 20px;" />		
							<input type="hidden" name="tecnico_id" id="tecnico_id"  />						
						</div>											
						<div class="col-lg-3">														
							<label>Cliente deseja receber informações da produção do pedido:</label>	
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" name="fl_receber_info" id="" value="1"  />
									Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" name="fl_receber_info" id="" value="0" checked="checked">
									Não
									<span></span>
								</label>								
							</div>						
						</div>
					</div>		
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>Endereço de entrega:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="endereco_entrega" id="endereco_entrega" class="form-control m-input" placeholder="" required>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>Nome do Cliente:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="nome_cliente" value="" id="nome_cliente" class="form-control m-input" placeholder="" required>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>RG:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="rg_cliente" id="rg_cliente" class="form-control m-input" placeholder="">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>CPF</label> 
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="cpf_cliente" id="cpf_cliente" class="form-control m-input" placeholder="">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
					</div>	
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Observação</label>
							<textarea class="form-control" name="observacao" id="observacao"></textarea>
						</div>
					</div>				
	           	 <div class="form-group m-form__group row">
	                <div class="m-portlet col-lg-12">
	                    <div class="m-portlet__head">
	                        <div class="m-portlet__head-caption">
	                            <div class="m-portlet__head-title">
	                                <span class="m-portlet__head-icon">
	                                    <i class="la la-thumb-tack m--font-warning"></i>
	                                </span>
	                                <h3 class="m-portlet__head-text m--font-warning">
	                                    Forma de Pagamento (informações, parcelamentos, etc...)
	                                </h3>
	                            </div>
	                        </div> 
	                    </div>
						<div class="m-portlet__body ">
							<table class="table m-table m-table--head-separator-warning">
							  	<thead>
							    	<tr>			      		
							      		<th style="width: 100%;">Descrição</th>							      		
							      						      		
							    	</tr>
							  	</thead>
							  	<tbody>
							  		 <tr id="modelo" indice='0' total_indice='0'>
							  		 	<td>
							  		 		
											<input type="hidden" name="porcentagem[]" min="0" max="100" maxlength="3" class="form-control m-input porcentagem" placeholder="%" value="0.00" />
											<input type="text" required name="descricao[]" id="descricao" class="form-control m-input descricao" placeholder="Descrição" required value="" readonly="readonly" />
											
							  		 	</td>
							  		 </tr>
							  	</tbody>
							  </table>
						</div>	
	                </div>                                               
                </div> 
		        <div class="form-group m-form__group row produtos" style="display: none;">
	                <div class="m-portlet col-lg-12">
	                    <div class="m-portlet__head">
	                        <div class="m-portlet__head-caption">
	                            <div class="m-portlet__head-title">
	                                <span class="m-portlet__head-icon">
	                                    <i class="la la-thumb-tack m--font-success"></i>
	                                </span>
	                                <h3 class="m-portlet__head-text m--font-success">
	                                    Produtos
	                                </h3>
	                            </div>
	                        </div> 
	                    </div>
						<div class="m-portlet__body " id="produtos_cadastrar">
						</div>	
	                </div>                                               
                </div> 
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>      	
			</div>
			</form>
			<!--end::Form-->
		</div>
</div>	