<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">Perfil</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaFinanceiro/editarCliente');?>" method="post"  enctype="multipart/form-data">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>Nome:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required	name="nome"	class="form-control m-input" placeholder="" required value="<?php echo $dados['nome'];?>">
								<input type="hidden" name="id" 	class="form-control m-input" placeholder="" required value="<?php echo $dados['id'];?>">		
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
								</span>
							</div>
						</div>
						<div class="col-lg-6">
							<label class="">E-mail:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="email" required name="email" class="form-control m-input" placeholder="" required value="<?php echo $dados['email'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-envelope-o"></i></span></span>
							</div>	
						</div>
					</div>	 
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>CPF:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="cpf" id="cpf" class="form-control m-input" placeholder="Insira seu cpf" required value="<?php echo $dados['cpf'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>
							
						</div>
						<div class="col-lg-6">
							<label class="">Telefone:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="telefone" required id="telefone" class="form-control m-input" placeholder="Insira seu telefone" value="<?php echo $dados['telefone'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
							</div>
							
						</div>
					</div>	 														                
	            </div>	            	
				<div class="form-group m-form__group row">
					<div class="col-lg-6">
						<label>Redefinir Senha:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="password" name="senha" class="form-control m-input" placeholder=""  value="" id="senha"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-key"></i></span>
							</span>
						</div>
					</div>
					<div class="col-lg-6">
						<label class="">Confirmar Senha:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="password" name="senha2" id="senha2" class="form-control m-input" placeholder="" value="">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-key"></i></span></span>
						</div>	
					</div>
				</div>
				<div class="form-group m-form__group row">	            					
					<div class="col-lg-6">
						<label class="">Data/Hora Cadastro:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="dthr_cadastro" disabled="disabled" class="form-control m-input" placeholder="Data/Hora Cadastro" value="<?php echo date('d/m/Y H:i:s', strtotime($dados['dthr_cadastro']));?>">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar-o"></i></span></span>
						</div>						
					</div>					
					<div class="col-lg-6">
						<label class="">Escolha uma foto para seu perfil:</label>
						<div class="custom-file">
						  	<input type="file" name="nome_arquivo" class="custom-file-input" id="customFile">
						  	<label class="custom-file-label" for="customFile">Escolha uma foto</label>
						</div>	 
					</div>
				</div>
				
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "OK!",
               	text: 'CASO TENHA ALTERADO A FOTO DO PERFIL, ELA SERÁ CARREGADA NO PRÓXIMO ACESSO',
               	type: "success"
        }); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>