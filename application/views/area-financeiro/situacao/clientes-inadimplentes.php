<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-dollar" style="font-weight: bold;margin: 0px;padding: 0px;color: #ffcc00;" ></i>
						<i class="la la-dollar" style="font-weight: bold;margin: 0px;padding: 0px;color: #ffcc00;" ></i>
						<i class="la la-dollar" style="font-weight: bold;margin: 0px;padding: 0px;color: #ffcc00;" ></i>
					</span>
					<h3 class="m-portlet__head-text">
						Situação Financeira - Clientes Inadimplentes
					</h3>
				</div>
			</div>			
		</div>
		<div class="m-portlet__body">
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="Field #1" >ID</th>
						<th title="Field #2" >CNPJ</th>
						<th title="Field #3" >Razão Social</th>
						<th title="Field #4" >E-mail</th>						
						<th title="Field #5" >Inadimplente</th>
					</tr>
				</thead>
				<tbody>
					<tr>
					<?php foreach($dados as $dado){ ?>
						<td style="text-align: center;"><?php echo $dado['id']; ?></td>
						<td><?php echo $dado['cnpj']; ?></td>
						<td><?php echo $dado['razao_social']; ?></td>
						<td><?php echo $dado['email']; ?></td>												
						<td>
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" name="ativo<?php echo $dado['id'];?>" onclick="atualizarStatusCliente('1',<?php echo $dado['id']?>,'<?php echo $dado['email'];?>', '<?php echo $dado['cnpj'].'-'.$dado['razao_social']?>' );" class="ativo" <?php if($dado['fl_inadimplencia']=='1') echo 'checked="checked"' ?> value="1" empresa_id="<?php echo $dado['id']; ?>"> Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" name="ativo<?php echo $dado['id'];?>" class="ativo" <?php if($dado['fl_inadimplencia']=='0') echo 'checked="checked"' ?> value="0" empresa_id="<?php echo $dado['id']; ?>" onclick="atualizarStatusCliente('0',<?php echo $dado['id'];?>,'<?php echo $dado['email'];?>','<?php echo $dado['cnpj'].'-'.$dado['razao_social']?>');" > Não
									<span></span>
								</label>								
							</div>
						</td>
					</tr>
						<?php } ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>		        
</div>				
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Atualização realizada com sucesso!',
           	type: "success"
        }).then(function() {
			window.location = base_url+'AreaAdministrador/gestaoUsuarios';
		});  
	</script>	
<?php unset($_SESSION['sucesso']); } ?>