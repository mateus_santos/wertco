<div class="m-content">
	
	<div class="row">		
		<div class="col-xl-12">
			
			<!-- begin Portlet -->
			<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5" style="background-color: #464e3f14;">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon">
								<i class="flaticon-diagram" style="color: #464e3f;" ></i>
							</span>
							<h3 class="m-portlet__head-text" style="color: #000;">
								Chamados
							</h3>
							
						</div>

					</div>
					<div class="m-portlet__head-tools">
						<span class="m-portlet__head-icon" style="float: right;">
							<a href="<?php echo base_url('AreaAssistencia/geraTodosChamadosPdf');?>" target="blank" class="btn btn-outline-metal m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air">
								<i class="la la-print" style="color: #464e3f;font-size: 25px;" ></i>
							</a>
						</span>
					</div>
				</div>				
				<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >			
					<!--begin: Datatable -->
					<table class="" id="html_table" width="100%">
						<thead>
							<tr>
								<th style="text-align: center; width: 5%;"># Chamado</th>
								<th style="text-align: center; width: 45%;">Cliente</th>						
								<th style="text-align: center; width: 10%;">Situação</th>						
								<th style="text-align: center; width: 10%;">Tipo</th>	
								<th style="text-align: center; width: 20%;">Data Criação</th>
								<th style="text-align: center; width: 10%;">Ações</th>
							</tr>
						</thead>
						<tbody>					
							<?php foreach($dados as $dado){
								

								?>
								<tr>
									<td style="width: 5%;text-align: center;"><?php echo $dado['id']; ?></td>
									<td style="width: 40%;text-align: center;"><?php echo $dado['cliente']; ?></td>											
									<td style="width: 10%;text-align: center; text-transform: capitalize;">
										<?php if( $dado['status_id'] == 1 ){
											$status = "btn m-btn--pill m-btn--air btn-success";
										}elseif( $dado['status_id'] == 2 ){
											$status = "btn m-btn--pill m-btn--air btn-info";
										}elseif( $dado['status_id'] == 3 ){
											$status = "btn m-btn--pill m-btn--air btn-warning";
										}elseif( $dado['status_id'] == 4 ){
											$status = "btn m-btn--pill m-btn--air btn-danger";
										}
										?>
										<button class="status <?php echo $status; ?>" status="<?php echo $dado['status'];?>" title="Status do chamado" onclick="status(<?php echo $dado['id']; ?>);">
											<?php echo ucfirst($dado['status']); ?>								
										</button>
									</td>
									<td style="width: 40%;text-align: center;"><?php echo $dado['tipo']; ?></td>	
									<td style="width: 10%;text-align: center;">
										<?php echo date('d/m/Y H:i:s',strtotime($dado['inicio'])); ?>							
									</td>							
									<td data-field="Actions" class="m-datatable__cell " style="width: 20%;text-align: center !important;">
										<a href="<?php echo base_url('AreaAssistencia/geraChamadoPdf/'.$dado['id'].'/post')?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
											<i class="la la-print"></i>
										</a>
										<a onclick="listaAnexos(<?php echo $dado['id']; ?>);" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill">
											<i class="flaticon-tool-1"></i>
										</a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						<!--end: Datatable -->
				</div>						
			</div>
			<!--end::Portlet-->	

		</div>
	</div>
	<div class="row">
		
		<div class="col-xl-4" >
			
		</div>
		<div class="col-xl-4" >
			
		</div>
	</div>
		<div class="row">
		<div class="col-xl-12" style="margin-top:0;">
			
		</div>
	</div>		
</div>	
<div class="modal fade" id="m_lista_anexo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_lista_anexo_title" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
				<div class="row anexos">
					
				</div>				
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>	
<script type="text/javascript">
	
	var config = {
		type: 'pie',
		data: {
			datasets: [{
				data:  <?php echo $total_status['valor']; ?>,
				backgroundColor: <?php echo $total_status['cor'];?>,
				label: 'Status'
			}],
			labels: <?php echo $total_status['descricao'];?>,
		},
		options: {
			responsive: true,			 
			 legend: {
           		 labels: {
               		 // This more specific font property overrides the global property
               		 fontColor: '#000'
          	 	 }
       		 }
		}
	};

	var config2 = {
		type: 'bar',
		data: {
			datasets: [{
				data:  				<?php echo $total_defeitos['valor']; ?>,
				backgroundColor: 	<?php echo $total_defeitos['cor'];?>,				
				borderColor: '#000'
			}],
			labels: <?php echo $total_defeitos['descricao'];?>,
		},
		options: {
			responsive: true,
			legend: {
            	display: false
        	}
		}
	};
	
	var config1 = {
            type: 'line',
            data: {
                labels: <?php echo $total_mes['mes'];?>,
                datasets: [{                    
                    backgroundColor: 'rgba(255,204,0,0.24)',
                    borderColor: mUtil.getColor('warning'),
                    labels: <?php echo $total_mes['mes'];?>,
                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointHoverBackgroundColor: mUtil.getColor('danger'),
                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
                    data: <?php echo $total_mes['valor']; ?>
                }]
            },
            options: {
                title: {
                    display: false,
                },
                tooltips: {
                   intersect: true,                    
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 0   
                                    
                },
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: true,               
                scales: {
                    xAxes: [{
                        display: false,
                        gridLines: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mês'
                        }
                    }],
                    yAxes: [{
                        display: false,
                        gridLines: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0.000001
                    },
                    point: {
                        radius: 6,
                        borderWidth: 12
                    }
                },
                layout: {
                    padding: {
                        left: 6,
                        right: 2,
                        top: 0,
                        bottom: 0
                    }
                }
            }
        };

   	var config3 = {
		type: 'bar',
		data: {
			datasets: [{
				data:  				<?php echo $total_estado['valor']; ?>,
				backgroundColor: 	<?php echo $total_estado['cor'];?>,				
				borderColor: '#000'
			}],
			labels: <?php echo $total_estado['descricao'];?>,
		},
		options: {
			responsive: true,
			legend: {
            	display: false
        	}
		}
	};	

	$(document).ready(function(){
		
		var total_chamados 	= new Chart($('#total_chamados'), config);		
		var total_defeitos 	= new Chart($('#total_defeitos'), config2);
		var total_mes 		= new Chart($('#total_mes'), config1);
		var totat_estado 	= new Chart($('#total_estado'), config3);

	});
		
</script>