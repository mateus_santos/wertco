<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Treinamentos Técnicos - WERTCO
					</h3>
					<div class="m-portlet__head-caption cadastrar_orcamento">													
						<a href="<?php echo base_url('AreaAdministrador/cadastraTreinamento')?>" class="" style="color: #ffcc00; font-weight: bold;" >
							<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo treinamento"></i>
						</a>
					</div>
				</div>
				
			</div>
			<div class="m-portlet__head-caption cadastrar_orcamento" style="text-align: right;" ></div>			

		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/listaTreinamento');?>" method="post" enctype="multipart/form-data">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Treinamento</label>
							<div class="m-input-icon m-input-icon--right">
								<select class="form-group form-control" name="treinamento_id">
									<option value="">Selecione o treinamento</option>
								<?php  	foreach($dados as $dado)  { ?>
									<option value="<?php echo $dado['id']; ?>"><?php echo $dado['id'].' - '. $dado['descricao']; ?></option>
									
								<?php } ?>
								</select>
							</div>
						</div>
						
					</div>
					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
				</div>
			</form>			
		</div>
	</div>
</div>	
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
				
		<script type="text/javascript">
			
			swal({
				title: "OK!",
				text: 'Treinamento criado/editado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaAdministrador/treinamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>
