<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-building-o"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Inscrição #<?php  echo $dados[0]->id; ?>
					</h3>
				</div>			
			</div>
			<!--<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item">
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="geraPdf" empresa_id="<?php echo $dados[0]->empresa_id; ?>" data-toggle="m-tooltip" data-placement="top" title="Gerar PDF do Orçamento">
							<i class="la la-print"></i>
						</a>	
					</li>
					<li class="m-portlet__nav-item">
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="geraPdf" empresa_id="<?php echo $dados[0]->empresa_id; ?>" data-toggle="m-tooltip" data-placement="top" title="Emitir orçamento ao cliente">
							<i class="la la-send"></i>
						</a>	
					</li>
				</ul>
			</div>-->
		</div>
		<div class="m-portlet__body">
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>Nome: 
						<small class="text-muted"> <?php echo $dados[0]->nome.' '.$dados[0]->sobrenome;?></small>
					</h5>
				</div>							
				<div class="col-lg-4 col-xs-12">
					<h5>Cpf: 
						<small class="text-muted"> <?php echo $dados[0]->cpf;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>Rg: 
						<small class="text-muted"> <?php echo $dados[0]->rg;?></small>
					</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Empresa: <small class="text-muted"> <?php echo $dados[0]->cnpj . ' | '.$dados[0]->razao_social; ?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Inscrição Estadual: <small class="text-muted"> <?php echo $dados[0]->insc_estadual;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						Inscrição Municipal: <small class="text-muted"> <?php echo $dados[0]->insc_municipal;?></small>
					</h5>
				</div>							
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Endereço: <small class="text-muted"> <?php echo $dados[0]->endereco_comercial . ' - '.$dados[0]->numero; ?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Complemento: <small class="text-muted"> <?php echo $dados[0]->complemento;?></small>
					</h5>
				</div>							
				<div class="col-lg-4 col-xs-12">
					<h5>
						Bairro: <small class="text-muted"> <?php echo $dados[0]->bairro;?></small>
					</h5>
				</div>
				
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Cep: <small class="text-muted"> <?php echo $dados[0]->cep; ?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Cidade: <small class="text-muted"> <?php echo $dados[0]->cidade;?></small>
					</h5>
				</div>							
				<div class="col-lg-4 col-xs-12">
					<h5>
						Estado: <small class="text-muted"> <?php echo $dados[0]->estado;?></small>
					</h5>
				</div>
				
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Telefone: <small class="text-muted"> <?php echo $dados[0]->telefone; ?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Celular/Whatssapp: <small class="text-muted"> <?php echo $dados[0]->celular_whats;?></small>
					</h5>
				</div>							
				<div class="col-lg-4 col-xs-12">
					<h5>
						E-mail: <small class="text-muted"> <?php echo $dados[0]->email;?></small>
					</h5>
				</div>
				
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Interesse: <small class="text-muted"> <?php echo $dados[0]->interesse; ?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Aprovado: <small class="text-muted"> <?php echo ($dados[0]->fl_aprovado == 1) ? 'Sim' : 'Não';?></small>
					</h5>
				</div>							
				<div class="col-lg-4 col-xs-12">
					<h5>
						Arquivo Depósito: <small class="text-muted"> <a href="http://www.wertco.com.br/depositos/<?php echo $dados[0]->arquivo;?>">Baixar</a></small>
					</h5>
				</div>				
			</div>
			<div class="row">
				<div class="col-lg-12 col-xs-12">
					<h5>
						Data/hora inscrição: <small class="text-muted"> <?php echo date('d/m/Y H:i:s' ,strtotime($dados[0]->dthr_inscricao)); ?></small>
					</h5>
				</div>
			</div>			
		</div>
	</div>
</div>	