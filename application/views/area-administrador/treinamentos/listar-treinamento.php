<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						<?php echo (count($dados) > 0) ? '#'.$treinamento_id.' '.$dados[0]->descricao : 'treinamento id:'.$treinamento_id; ?>	
					</h3>
					<a href="<?php echo base_url('AreaAdministrador/editarTreinamento/'.$treinamento_id); ?>" class="btn btn-success m-btn m-btn--icon btn-lg m-btn--icon-only" style="margin-top: 10px;margin-left: 10px;">
					<i class="la la-pencil"></i>
					</a>
					
				</div>
				
			</div>
			<div class="m-portlet__head-caption cadastrar_orcamento" style="text-align: right;" >													
				
				<h3 >Total Aprovados: <b class="m--font-warning total_aprovados"><?php echo $total_aprovados;?></b>&nbsp;&nbsp;
					<a href="https://www.wertco.com.br/geraExcelTreinamentos/gera_excel_treinamento.php?id=<?php echo $treinamento_id;?>" class="btn btn-success m-btn m-btn--icon btn-lg m-btn--icon-only" target="blank">
						<i class="la la-file-excel-o"></i>
					</a>
				</h3>

			</div>			

		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
			&nbsp;<span style="text-align: center;margin-left: 30% !important;"><b style="text-transform: lowercase;">https://www.wertco.com.br/treinamento/inscricao/</b><span style="text-transform: none !important;" ><?php echo base64_encode($treinamento_id); ?></span>
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;">Número</th>
						<th style="text-align: center;">CNPJ | Razão social</th>						
						<th style="text-align: center;">Nome</th>
						<th style="text-align: center;">CPF</th>																	
						<th style="text-align: center;">Arquivo Depósito</th>	
						<th style="text-align: center;">Situação</th>
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				<tbody>
					
					<?php foreach($dados as $dado){ ?>
						<tr>
							<td style="width: 5%;text-align: center;"><?php echo $dado->id; ?></td>
							<td style="width: 30%;text-align: center;"> <?php echo $dado->cnpj." | ".$dado->razao_social; ?></td>
							<td style="width: 30%;text-align: center;"><?php echo $dado->nome.' '.$dado->sobrenome; ?></td>		
							<td style="width: 10%;text-align: center;" ><?php echo $dado->cpf; ?></td>
							<td style="width: 10%;text-align: center;" ><a href="http://www.wertco.com.br/depositos/<?php echo $dado->arquivo; ?>" target="_blank">Baixar</a></td>
							<td style="width: 10%;text-align: center;">
								<div class="m-radio-inline">
									<label class="m-radio">
										<input type="radio" name="ativo<?php echo $dado->id;?>" onclick="atualizarStatus('1',<?php echo $dado->id?>,'<?php echo $dado->email;?>');" class="ativo ativo_sim" <?php if($dado->fl_aprovado=='1') echo 'checked="checked"' ?> value="1" user_id="<?php echo $dado->id; ?>"> Sim
										<span></span>
									</label>									
									<label class="m-radio">
										<input type="radio" name="ativo<?php echo $dado->id;?>" class="ativo" <?php if($dado->fl_aprovado=='0') echo 'checked="checked"' ?> value="0" user_id="<?php echo $dado->id; ?>" onclick="atualizarStatus('0',<?php echo $dado->id?>,'<?php echo $dado->email;?>');" > Não
										<span></span>
									</label>								
								</div>
							</td>
							<td data-field="Actions" class="m-datatable__cell " style="width: 5%;">
								<span style="overflow: visible; width: 110px;" class="">								
									<a href="<?php echo base_url('AreaAdministrador/visualizarTreinamento/'.$dado->id); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Visualizar" orcamento_id="<?php echo $dado->id; ?>" >
										<i class="la la-eye visualizar"></i>
									</a> 
									<!--<button  data-toggle="m-tooltip"  id="excluir" onclick="excluirOrcamento(<?php echo $dado->id; ?>);" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" orcamento_id="<?php echo $dado->id; ?>">
										<i class="la la-trash"></i>
									</button>-->
								</span>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	

	<!-- end:: Body -->
	
	