<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Gestão de Empresas 	<a href="<?php echo base_url('AreaAdministrador/cadastrarEmpresa'); ?>" style="color: #ffcc00; font-weight: bold;" >
												<i class="la la-plus-circle" style="font-size: 38px;"></i>
											</a>
					</h3>
				</div>
			</div>					
		</div>
		<div class="m-portlet__body">			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;" width="5%"> #</th>
						<th title="Field #1" width="10%">CNPJ</th>
						<th title="Field #2"  width="35%">Razão Social</th>
						<th title="Field #3"  width="15%">Fantasia</th>
						<th title="Field #4"  width="5%">E-mail</th>
						<th title="Field #5"  width="5%">Telefone</th>
						<th title="Field #6"  width="5%">Tipo de Acesso</th>
						<th title="Field #7"  width="5%">Dthr Cadastro</th>						
						<th title="Field #8"  width="15%">Ações</th>
					</tr>
				</thead>
				<tbody>					
					<?php foreach($dados as $dado){ 
						if( $dado->tipo_cadastro == 'clientes' ){
							$bgcolor = 'btn-success';
						}elseif( $dado->tipo_cadastro == 'representantes' ){
							$bgcolor = 'btn-warning';
						}elseif( $dado->tipo_cadastro == 'tecnicos' || $dado->tipo_cadastro == 'mecanicos' ){
							$bgcolor = 'btn-info';	
						}elseif(  $dado->tipo_cadastro == 'administrador' ){
							$bgcolor = 'btn-secondary';	
						}else{
							$bgcolor = 'btn-danger';	
						}
						?>
					<tr>
						<td style="text-align: center;"><?php echo $dado->id; ?></td>
						<td><?php echo $dado->cnpj; ?></td>
						<td><?php echo $dado->razao_social; ?></td>
						<td><?php echo $dado->fantasia; ?></td>
						<td><?php echo $dado->email; ?></td>
						<td><?php echo $dado->telefone; ?></td>
						<td><button type="button" class="btn m-btn--pill m-btn--air <?php echo $bgcolor; ?>"><?php echo $dado->tipo_cadastro; ?></button></td>						
						<td><?php echo date('d/m/Y H:i:s', strtotime( $dado->dthr_cadastro)); ?></td>						
						<td data-field="Actions" class="m-datatable__cell">
							<span style="overflow: visible; width: 110px;">								
								<!--<a class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Visualizar Empresa e Usuários Vinculados" onclick="visualizar_empresa('<?php echo $dado->id; ?>')">
									<i class="la la-eye"></i>
								</a>-->
								<a href="<?php echo base_url('AreaAdministrador/visualizarEmpresa/'.$dado->id.'/'.$dado->tipo_cadastro_id); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar " target="_blank">
									<i class="la la-eye"></i>
								</a><!--
								<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir">
									<i class="la la-trash"></i>
								</a>-->
							</span>
						</td>
					</tr>
						<?php } ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>		        
</div>				
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
           	title: "OK!",
           	text: 'Atualização realizada com sucesso!',
           	type: "success"
        }).then(function() {
		   	window.location = base_url+'AreaAdministrador/gestaoEmpresas';
		});
	</script>	
<?php unset($_SESSION['sucesso']); } ?>