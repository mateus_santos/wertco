<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('AreaAdministrador/visualizarEmpresa/'.$dados[0]['id'].'/'.$pesquisa);?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text" style="padding-right: 10px;">	
							<a href="<?php echo base_url('AreaAdministrador/visualizarEmpresa/'.$dados[0]['id'].'/'.$pesquisa);?>">Voltar</a>						
						</h3>
						<span class="m-portlet__head-icon m--hide">
						<i class="la la la-building-o"></i>
						</span>
						<h3 class="m-portlet__head-text">	
							Empresa #<?php echo $dados[0]['id'];?>						
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarEmpresa');?>" method="post">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>Razão Social:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required	name="razao_social"	class="form-control m-input" placeholder="" required value="<?php echo $dados[0]['razao_social'];?>">
								<input type="hidden" name="id" 	class="form-control m-input" placeholder="" required value="<?php echo $dados[0]['id'];?>">								
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
								</span>
							</div>
						</div>
						<div class="col-lg-3">
							<label class="">Fantasia:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="fantasia" class="form-control m-input" placeholder="" required value="<?php echo $dados[0]['fantasia'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>	
						</div>
						<div class="col-lg-3">
							<label>CNPJ:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="cnpj" id="cnpj" class="form-control m-input" placeholder="Insira um cnpj" required value="<?php echo $dados[0]['cnpj'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>
							
						</div>
						<div class="col-lg-3">
							<label>Inscrição Estadual:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="insc_estadual" id="insc_estadual" class="form-control m-input" placeholder="Insira a inscrição estadual" <?php if($dados[0]['tipo_cadastro_id'] == 1) echo "required"; ?> value="<?php echo $dados[0]['insc_estadual'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>
							
						</div>
					</div>	 
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>E-mail:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="email" required name="email" id="email" class="form-control m-input" placeholder="Insira um email" required value="<?php echo $dados[0]['email'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-3">
							<label>Telefone:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="telefone" id="telefone" class="form-control m-input" placeholder="Insira um telefone" required value="<?php echo $dados[0]['telefone'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-3">
							<label>Endereço:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="endereco" id="endereco" class="form-control m-input" placeholder="Insira um endereço" required value="<?php echo $dados[0]['endereco'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-3">
							<label>Bairro:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="bairro" id="bairro" class="form-control m-input" placeholder="Insira um Bairro" required value="<?php echo $dados[0]['bairro'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
							</div>							
						</div>
					</div>
					<div class="form-group m-form__group row">	
						<div class="col-lg-3">
							<label class="">CEP:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="cep" required id="cep" class="form-control m-input" placeholder="Insira um cep" value="<?php echo $dados[0]['cep'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-3">
							<label class="">Cidade:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="cidade" required id="cidade" class="form-control m-input" placeholder="Insira um cidade" value="<?php echo $dados[0]['cidade'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
							</div>
							
						</div>
						<div class="col-lg-3">
							<label class="">Estado:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="estado" required id="estado" class="form-control m-input" placeholder="Insira um estado" value="<?php echo $dados[0]['estado'];?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
							</div>							
						</div>					
						<div class="col-lg-3">
							<label>País:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="pais" id="pais" class="form-control m-input" placeholder="Insira um pais" required value="<?php echo $dados[0]['pais'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map"></i></span></span>
							</div>							
						</div>
					</div>
					<?php if($dados[0]['tipo_cadastro_id'] == 4){ ?>
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>Credenciamento Inmetro:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="credenciamento_inmetro" id="credenciamento_inmetro" class="form-control m-input" placeholder="Insira o número de credenciamento do Inmetro" value="<?php echo $dados[0]['credenciamento_inmetro'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>
							
						</div>
						
						<div class="col-lg-6">
							<label>Data Vencimento do Credenciamento Inmetro:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="venc_inmetro" id="m_datepicker" class="form-control m-input" placeholder="Insira a data de credenciamento do Inmetro" required value="<?php if ($dados[0]['venc_inmetro'] != null) echo date('d/m/Y', strtotime($dados[0]['venc_inmetro']));?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar"></i></span></span>
							</div>
							
						</div>
					</div>
					<?php } ?>
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label for="Acesso">Tipo de Empresa</label>
							<select class="form-control m-input" name="tipo_acesso_id" required>
							<?php  foreach ($dados_tipo_acesso as $tipo_acesso) { ?>
								<option value="<?php echo $tipo_acesso['id']; ?>" <?php if($tipo_acesso['id'] == $dados[0]['tipo_cadastro_id']) { echo 'selected="selected"'; }?> >
									<?php echo $tipo_acesso['descricao']; ?>									
								</option>
							<?php } ?>
							
							</select>
						</div>
						
					</div>	
					<?php if( $dados[0]['tipo_cadastro_id'] == 1 ){ /* ?>					

					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Nr. Bombas:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="nr_bombas" id="nr_bombas" class="form-control m-input" placeholder="Insira o Nr. Bombas" required value="<?php echo $dados[0]['nr_bombas'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Marca Bombas:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="marca_bombas" id="marca_bombas" class="form-control m-input" placeholder="Insira o Marca Bombas" required value="<?php echo $dados[0]['marca_bombas'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Nr. Bicos:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="bicos" id="bicos" class="form-control m-input" placeholder="Insira o Nr. Bicos Ativos" required value="<?php echo $dados[0]['bicos'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>						
					</div>	
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Bandeira:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="bandeira" id="bandeira" class="form-control m-input" placeholder="Insira a bandeira" required value="<?php echo $dados[0]['bandeira'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Software que utiliza:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="software" id="software" class="form-control m-input" placeholder="Insira o software que utiliza" required value="<?php echo $dados[0]['software'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Postos ou Rede:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="postos_rede" id="bicos" class="form-control m-input" placeholder="Insira o postos_rede" required value="<?php echo $dados[0]['postos_rede'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>						
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Nr. postos:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="nr_postos" id="nr_postos" class="form-control m-input" placeholder="Insira o nr_postos" required value="<?php echo $dados[0]['nr_postos'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Mecânico que atende:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="mecanico_atende" id="mecanico_atende" class="form-control m-input" placeholder="Insira o mecânico que atende" required value="<?php echo $dados[0]['mecanico_atende'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Observações/Solicitações:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="observacoes" id="observacoes" class="form-control m-input" placeholder="Insira o observacoes" required value="<?php echo $dados[0]['observacoes'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>						
					</div>

					<?php }elseif( $dados[0]['tipo_cadastro_id'] == 2 ){ ?>
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Data Constituição:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="data_constituicao" id="data_constituicao" class="form-control m-input" placeholder="Insira o data_constituicao" required value="<?php echo $dados[0]['data_constituicao'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Região que atua:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="regiao_atua" id="regiao_atua" class="form-control m-input" placeholder="Insira o mecânico que atende" required value="<?php echo $dados[0]['regiao_atua'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Representante Legal:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="representante_legal" id="representante_legal" class="form-control m-input" placeholder="Observações" required value="<?php echo $dados[0]['representante_legal'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>	
						<div class="col-lg-4">
							<label>Produtos que representa:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="representante_legal" id="representante_legal" class="form-control m-input" placeholder="Observações" required value="<?php echo $dados[0]['representante_legal'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>	
						<div class="col-lg-4">
							<label>Nr. de Funcionaários:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="nr_funcionarios" id="nr_funcionarios" class="form-control m-input" placeholder="Observações" required value="<?php echo $dados[0]['nr_funcionarios'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>						
					</div>
					<?php }elseif( $dados[0]['tipo_cadastro_id'] == 3 || $dados[0]['tipo_cadastro_id'] == 4 ) { ?>
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Credenciamento Inmetro:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="credenciamento_inmetro" id="credenciamento_inmetro" class="form-control m-input" placeholder="Insira o credenciamento_inmetro" required value="<?php echo $dados[0]['credenciamento_inmetro'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Credenciamento Crea:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="credenciamento_crea" id="credenciamento_crea" class="form-control m-input" placeholder="Insira o mecânico que atende" required value="<?php echo $dados[0]['credenciamento_crea'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Nr. Técnicos:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="representante_legal" id="representante_legal" class="form-control m-input" placeholder="Observações" required value="<?php echo $dados[0]['representante_legal'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>	
						<div class="col-lg-4">
							<label>Região que atua:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="regiao_atua" id="regiao_atua" class="form-control m-input" placeholder="Insira o mecânico que atende" required value="<?php echo $dados[0]['regiao_atua'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>							
						</div>				
					</div>
					<?php */} ?>
	            </div>
				<div class="form-group m-form__group row">
		            <div class="col-lg-6">
						<label>Tipo de Bandeira:</label>
						<div class="m-input-icon m-input-icon--right">
							<select  class="form-control"  maxlength="250" name="tipo_cadastro_id" required>
	                       <option value="">Selecione uma bandeira</option>
							<option value="BR">BR</option>
							<option value="BR NOVA">Br Nova</option>
							<option value="IPIRANGA">Ipiranga</option>
							<option value="IPIRANGA NOVA">Ipiranga Nova</option>
							<option value="SHELL">Shell</option>
							<option value="ALE">Ale</option>
							<option value="CHARRUA">Charrua</option>
							<option value="TOTAL">Total</option>
							<option value="WERTCO">Wertco </option>
							<option value="BANDEIRA_BRANCA">Bandeira Branca</option>
							<option value="TABOCAO">Tabocão</option>
							<option value="VIBRA">Vibra</option>
							<option value="FIT">Fit</option>
							<option value="SIM">Sim </option>
							<option value="RODOIL">RodOil </option>
							<option value="RAYGAS">RayGas</option>
							<option value="PERU">EXTERIOR - PERU </option>	
	                    </select>
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-list"></i></span></span>
						</div>							
					</div>
				</div>
	            <div class="form-group m-form__group row">
	            				
					<div class="col-lg-6">
						<label class="">Data/Hora Cadastro:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="dthr_cadastro" disabled="disabled" class="form-control m-input" placeholder="Data/Hora Cadastro" value="<?php echo date('d/m/Y H:i:s', strtotime($dados[0]['dthr_cadastro']));?>">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar-o"></i></span></span>
						</div>						
					</div>
					<?php if( $dados[0]['cartao_cnpj'] != '' ){ 
							 $arquivo = explode('/',$dados[0]['cartao_cnpj']); 
							if(count($arquivo) == 5){
					?>
					<div class="col-lg-6">
						<label class="">Cartão CNPJ:</label>
						<div class="">

							<a href="<?php echo base_url('Consultas/Receita_Federal/'.$arquivo[5]); ?>" target="_blank">
								<img  src="<?php echo base_url('bootstrap/img/receita.gif'); ?>" />
							</a>
							
						</div>						
					</div>
				<?php }
					} 
				?>					  
				</div>	

	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	