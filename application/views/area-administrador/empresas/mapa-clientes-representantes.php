 <style>
/* Always set the map height explicitly to define the size of the div
 * element that contains the map. */
#map {
    height: 100%;
}

.controls {
    margin-top: 10px;
    border: 1px solid transparent;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    height: 32px;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
  }

  #pac-input {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 300px;
  }

  #pac-input:focus {
    border-color: #4d90fe;
  }

  .pac-container {
    font-family: Roboto;
  }

  #type-selector {
    color: #fff;
    background-color: #4d90fe;
    padding: 5px 11px 0px 11px;
  }

  #type-selector label {
    font-family: Roboto;
    font-size: 13px;
    font-weight: 300;
  }

  #target {
    width: 345px;
  }

  #menu img{
    margin-bottom: 5px;
    margin-top: 5px;
    /*width: 25px;*/
  }

  #menu a{
    cursor: pointer;
  }

  .gm-style-iw-c{
    background: #ffffffde !important;
  }
   
  .gm-style-iw-d{
    overflow: inherit !important;
  }
   
</style>

  
  <div id="parceiros">
        <!--#todo-->
        <input id="pac-input" class="controls" type="text" placeholder="pesquisar por localidade">
        <div style="z-index: 999;position: absolute;left: 265px;top: 115px;background: #ffffffde; padding: 50px;height: 500px;overflow: auto;" class="controls" id="menu"> 
          <h4>Filtros</h4>
          <input id="empresas" class="form-control controls" type="text" placeholder="pesquisar razão social/CNPJ" /> <br/>        
          <a onclick="filterMarkers('todos');"><img src="<?php echo base_url('bootstrap/img/todos_2.png'); ?>"> &nbsp;&nbsp;Todos</a><br/>
          <a onclick="filterMarkers('indi_rep');"><img src="<?php echo base_url('bootstrap/img/representante_indicador.png'); ?>"> &nbsp;&nbsp;Indicadores/representantes</a><br/>
          <a onclick="filterMarkers('indicadores');"><img src="<?php echo base_url('bootstrap/img/indicadores.png'); ?>"> &nbsp;&nbsp;Indicadores</a><br/>
          <a onclick="filterMarkers('tecnicos');"><img src="<?php echo base_url('bootstrap/img/chave-inglesa.png'); ?>"> &nbsp;&nbsp;Técnicos</a><br/>
          <a onclick="filterMarkers('representantes');"><img src="<?php echo base_url('bootstrap/img/representante_1.png'); ?>"> &nbsp;&nbsp;Representantes</a><br/>
          <a onclick="filterMarkers('representantes2');"><img src="<?php echo base_url('bootstrap/img/representante_2.png'); ?>"> &nbsp;&nbsp;Representantes2</a><br/>
          <a onclick="filterMarkers('clientes');"><img src="<?php echo base_url('bootstrap/img/orcamento.png'); ?>"> &nbsp;&nbsp;Orçamentos</a><br/>                    
          <hr/>
          <a onclick="filterMarkers('aberto');"><img src="<?php echo base_url('bootstrap/img/orcamento_1.png'); ?>"> &nbsp;&nbsp; Aberto</a><br/>
          <a onclick="filterMarkers('fechado');"><img src="<?php echo base_url('bootstrap/img/orcamento_2.png'); ?>">&nbsp;&nbsp;Fechado</a><br/>
          <a onclick="filterMarkers('orçamento entregue');"><img src="<?php echo base_url('bootstrap/img/orcamento_3.png'); ?>">&nbsp;&nbsp; Orçamento Entregue</a><br/>
          <a onclick="filterMarkers('em negociação');"><img src="<?php echo base_url('bootstrap/img/orcamento_4.png'); ?>">&nbsp;&nbsp; Em Negociação</a><br/>
          <a onclick="filterMarkers('Aguardando Aprovação Gestor');"><img src="<?php echo base_url('bootstrap/img/orcamento_5.png'); ?>"> &nbsp;&nbsp;Aguardando Aprovação Gestor</a><br/>          
          <a onclick="filterMarkers('Expirados');"><img src="<?php echo base_url('bootstrap/img/orcamento_5.png'); ?>"> &nbsp;&nbsp;Expirados </a>
        </div>        
  </div>
<div id="map"></div>                
                  
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCacJw1Pk4vCdrDqvX1_AJSstEAv1oHbF4&libraries=places"></script>   
<script type="text/javascript">
        var First_Sh = [], retornaivus = 0, chart_vendas, map, first_time = true, First_UF, t_cadastrados, t_n_cadastrados;
        var gmarkers1 = [];
        var empresas = [];
        var map;
        function remover_title(){
            
            $(".gm-ui-hover-effect").each(function( index ) {
                console.log(index);
                $( this ).attr('title', '');
            });

        }
                     

        
function initMap() {

          map = new google.maps.Map(document.getElementById('map'), {
                      center: {lat: -16.68, lng: -49.25},
                      zoom: 4
                  });

          
          google.maps.event.trigger(map, 'resize');

          //map.setCenter(new google.maps.LatLng(-16.68, -52.2850379));

          // Create the search box and link it to the UI element.
          var input = document.getElementById('pac-input');
          var searchBox = new google.maps.places.SearchBox(input);
          map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

          // Bias the SearchBox results towards current map's viewport.
          map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());

          });
          /*
          var mouse_over_txt = "<div>mouse_over</div>";
          var click_txt = "<div>click_txt</div>";

          addMarker(-31.6915219,-52.3014871,map, mouse_over_txt,click_txt);

          var mouse_over_txt = "<div>mouse_over2</div>";
          var click_txt = "<div>click_txt2</div>";

          addMarker(-31.7046043,-52.3319408,map, mouse_over_txt,click_txt);*/          
          
          // Listen for the event fired when the user selects a prediction and retrieve
          // more details for that place.
          searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
              return;
            }

            // Clear out the old markers.
            /*markers.forEach(function(marker) {
              marker.setMap(null);
            });
            markers = [];
            */
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
              if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
              }
              var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
              };

              // Create a marker for each place.
             /* markers.push(new google.maps.Marker({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
              }));*/

              if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
              } else {
                bounds.extend(place.geometry.location);
              }
            });
            map.fitBounds(bounds);
          });                  

          $.ajax({
            type:   'POST',
            url:  base_url+"AreaAdministrador/todosIndicadoresRepresentantes",         
            async: true,

            success: function(retorno) {

              retorno = JSON.parse(retorno);
              
              console.log(retorno);
              for (var i=0; i< retorno.length; i++){
                
                var click_txt = "<div style='padding: 10px; '>";
                click_txt += "<b>Tipo de empresa: </b>";
                click_txt += retorno[i].tipo;
                click_txt += "<br><br><b>Código: </b>";
                click_txt += retorno[i].id;
                click_txt += "<br><br/><b>Razão: </b>";
                click_txt += retorno[i].empresa;                                
                click_txt += "<br><br><b>Cidade: </b>";
                click_txt += retorno[i].cidade.toLowerCase();
                click_txt += "<br><br><b>Estado: </b>";
                click_txt += retorno[i].estado;
                click_txt += "<br><br><b>Rua: </b>";
                click_txt += retorno[i].endereco;                                
                click_txt += "<br><br><b>CEP: </b>";
                click_txt += retorno[i].cep;
                click_txt += "<br><br><b>Telefone: </b>";
                click_txt += retorno[i].telefone;
                click_txt += "<br><br><b>Celular: </b>";
                click_txt += retorno[i].celular;
                click_txt += "<br><br><b>Email: </b>";
                click_txt += retorno[i].email+"";
                if( retorno[i].tipo == 'cliente' ){
                  click_txt += "<br><br><b>Orçamentos: </b>";                                  
                  click_txt += retorno[i].orcamentos+"";
                  retorno[i].tipo = retorno[i].status;
                }
                click_txt += "</div>";
                
                addMarker(retorno[i].lat,retorno[i].lng,map,"",click_txt,retorno[i].tipo,retorno[i].empresa);
                empresas.push(retorno[i].empresa);

              }

            }

          });

        //filtro por pesquisa  
        $( "#empresas" ).autocomplete({
          source: empresas,
          select: function( event, ui ) {
            console.log( "Selected: " + ui.item.value  );
            buscaEmpresa(ui.item.value);
          }

        });
    }


function addMarker(lat,lng, map, mouse_over_txt,click_txt,grupo,razao_social) {

    var icone = "";
    //console.log(grupo);
    
    grupo = decodeURIComponent(escape(grupo));
    if (grupo == 'indicadores')               icone = "https://www.wertco.com.br/bootstrap/img/indicadores.png"; 
    else if (grupo == 'tecnicos')             icone = "https://www.wertco.com.br/bootstrap/img/chave-inglesa.png";
    else if (grupo == 'indicadores2')         icone = "https://www.wertco.com.br/bootstrap/img/indicadores.png";   
    else if (grupo == 'representantes')       icone = "https://www.wertco.com.br/bootstrap/img/representante_1.png"; 
    else if (grupo == 'representantes2')      icone = "https://www.wertco.com.br/bootstrap/img/representante_2.png"; 
    else if (grupo == 'aberto')               icone = "https://www.wertco.com.br/bootstrap/img/orcamento.png";  
    else if (grupo == 'fechado')              icone = "https://www.wertco.com.br/bootstrap/img/orcamento_1.png"; 
    else if (grupo == 'orçamento entregue')   icone = "https://www.wertco.com.br/bootstrap/img/orcamento_2.png"; 
    else if (grupo == 'em negociação')        icone = "https://www.wertco.com.br/bootstrap/img/orcamento_3.png"; 
    else if (grupo == 'Aguardando Aprovação Gestor')        icone = "https://www.wertco.com.br/bootstrap/img/orcamento_4.png"; 
    else if (grupo == 'Expirados')            icone = "https://www.wertco.com.br/bootstrap/img/orcamento_5.png"; 
    else icone = "https://www.wertco.com.br/bootstrap/img/chave-inglesa.png"; // Aguardando técnico
    
        
    var icon = {
        url: icone, // url
        //scaledSize: new google.maps.Size(40,40), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };

    var marker = new google.maps.Marker({
        map: map,
        draggable: false,
        icon: icon,
        animation: google.maps.Animation.DROP,
        category: grupo,
        position: {lat: Number(lat), lng: Number(lng)},        
        razao_social:   razao_social
        
    });

    gmarkers1.push(marker);

    var mouse_over = mouse_over_txt;
    var mouse_click = click_txt;

    var infowindow = new google.maps.InfoWindow({
        content: mouse_over
    });

    var infowindow_click = new google.maps.InfoWindow({
        content: mouse_click
    });
    
    google.maps.event.addListener(infowindow_click, 'closeclick', function() {
        marker.setAnimation(null);
    });

    /*marker.addListener('mouseover', function() {
        infowindow.open(map, marker);
    });*/

    marker.addListener('click', function() {
        //map.setZoom(8);
        //map.setCenter(marker.getPosition());
        
        toggleBounce(marker);
        infowindow.close();
        infowindow_click.open(map, marker);
        setTimeout(function (){
            remover_title();
        },100);
        
    });
    
    marker.setMap(map);
}

function toggleBounce(marker) {
    if (marker.getAnimation() !== null) {
      marker.setAnimation(null);
    } else {
      marker.setAnimation(google.maps.Animation.BOUNCE);
    }
} 

function  filterMarkers(category) {

    for (i = 0; i < gmarkers1.length; i++) {
        marker = gmarkers1[i];
        marker.setVisible(false);
        
        if( category == 'todos' ){
          marker.setVisible(true);

        }else if (marker.category == category ) {
          marker.setVisible(true);

        }else if( category == 'indi_rep' &&  (marker.category == 'indicadores' ||  marker.category == 'indicadores2' || marker.category == 'representantes' || marker.category == 'representantes2' || marker.category == 'tecnicos' )){
          marker.setVisible(true);

        }else if( category == 'clientes' &&  (marker.category == 'aberto' ||  marker.category == 'fechado' || marker.category == 'orçamento entregue' || marker.category == 'em negociação' || marker.category == 'Aguardando Aprovação Gestor' || marker.category == 'Expirados') ){

          marker.setVisible(true);

        }

    }    
    
}

function buscaEmpresa(empresa){
    for (i = 0; i < gmarkers1.length; i++) {
      marker = gmarkers1[i];
      if( marker.razao_social == empresa ){                
          marker.setVisible(true);
          toggleBounce(marker);
          map.setZoom(8);
          map.panTo(marker.position);

      }else{
          marker.setVisible(false);
      }  
    }
}
    



function remover_title(){

    $(".gm-ui-hover-effect").each(function( index ) {
        
        $( this ).attr('title', '');
    });
}


</script>