<div class="m-content">	
	<form accept-charset="utf-8" action="<?php echo base_url('AreaAdministrador/cadastrarEmpresa/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-building"></i>
					</span>
					<h3 class="m-portlet__head-text"> 
						Cadastro de Empresa
					</h3>
				</div>			
			</div> 
			<div class="m-portlet__head-tools">
				
			</div>
		</div>
				
		<div class="m-portlet__body">					
			<div class="form-group m-form__group row">									
				<div class="col-lg-12">
					<label><b>Tipo de Cadastro:</b></label>
					<div class="m-radio-inline">
						<label class="m-radio">
							<input type="radio" name="tp_cadastro" value="0" class="tp_cadastro" />
							Nacional
							<span></span>
						</label>
						<label class="m-radio">
							<input type="radio" name="tp_cadastro" value="1" class="tp_cadastro" />
							Estrangeira
							<span></span>
						</label>
					</div>											
				</div>	
			</div>
			<div id="cadastro">
			<div class="form-group m-form__group row">
				<div class="col-lg-3">
					<label id='label_documento'>Documento (CNPJ):</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="cnpj" id="cnpj" class="form-control m-input" placeholder="Insira um cnpj" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
						<input type="hidden"  id="salvar" maxlength="15" name="salvar" value="1"    required />
						<input type="hidden"  id="cartao_cnpj" name="cartao_cnpj"  />
                        <input type="hidden"  id="empresa_id" maxlength="15" name="empresa_id" value=""  />                        
					</div>					
				</div>
				<div class="col-lg-3">
					<label id="label_razao">Razão Social:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required	name="razao_social" id="razao_social" class="form-control m-input" placeholder="" required value="">
						<input type="hidden" name="id" 	class="form-control m-input" placeholder="Insira uma razão social" required value="">								
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
						</span>
					</div>
				</div>
				<div class="col-lg-3" id="campo_fantasia">
					<label class="">Fantasia:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="fantasia" id="fantasia" class="form-control m-input" placeholder="Insira uma fantasia" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>	
				</div>
				<div class="col-lg-3" id="campo_ie">
					<label class="">Inscrição Estadual:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control m-input" placeholder="Inscrição Estadual" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>	
				</div>					
				
			</div>	 
			<div class="form-group m-form__group row">	
				<div class="col-lg-3">
					<label>E-mail:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="email"  name="email" id="email" class="form-control m-input" placeholder="Insira um email" value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>							
				</div>			
				<div class="col-lg-3">
					<label>Telefone:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="telefone" id="telefone" class="form-control m-input" placeholder="Insira um telefone" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Cep:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="cep" id="cep" class="form-control m-input" placeholder="Insira um cep" required value="" >
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Endereço:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="endereco" id="endereco" class="form-control m-input" placeholder="Insira um endereço" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
					</div>							
				</div>
				
			</div>
			<div class="form-group m-form__group row">
				<div class="col-lg-3">
					<label>Bairro:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="bairro" id="bairro" class="form-control m-input" placeholder="Insira o bairro" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
					</div>							
				</div>	
				<div class="col-lg-3">
					<label class="">Cidade:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="cidade" required id="cidade" class="form-control m-input" placeholder="Insira um cidade" value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
					</div>
					
				</div>
				<div class="col-lg-3">
					<label class="">Estado:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="hidden" name="estado" id="estadoE" class="form-control" placeholder="Insira um estado" />
						<select  class="form-control"  maxlength="250" name="estado" placeholder="Estado" id="estado" required>
                            <option value="">Selecione um Estado</option>
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AP">Amapá</option>
                            <option value="AM">Amazonas</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PR">Paraná</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SP">São Paulo</option>
                            <option value="SE">Sergipe</option>
                            <option value="TO">Tocantins</option>
                        </select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
					</div>							
				</div>					
				<div class="col-lg-3">
					<label>País:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="pais" id="pais" class="form-control m-input" placeholder="Insira um pais" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map"></i></span></span>
					</div>							
				</div>
			</div>
		 	<div class="form-group m-form__group row">
		 		<div class="col-lg-6">
					<label>Tipo de Bandeira:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="bandeira" required>
	                       	<option value="">Selecione uma bandeira</option>
	                    <?php foreach($bandeiras as $bandeira){ ?>
	                    	<option value="<?=$bandeira['id']?>"><?=$bandeira['descricao']?></option>
                    	</select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-list"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-6">
					<label>Tipo de Cadastro:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="tipo_cadastro_id" required>
                        <option value="">Selecione um tipo de cadastro</option>
                        <option value="1">Cliente</option>                            
                        <option value="4">Técnicos</option>
                        <option value="6">Indicador</option>
                        <option value="10">Indicador2</option>
                        <option value="2">Revenda 1</option>
                        <option value="12">Revenda 2</option>
                    </select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-list"></i></span></span>
					</div>							
				</div>	
			</div>
		</div>
			<hr/>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="form-group row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>
					</div>
				</div>
			</div>					
		</div>
		
	</div>
</form>
</div>
		

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Orçamento solicitado com sucessoo, em breve entraremos em contato!',
                type: 'success'
            }).then(function() {
                window.location = base_url+'AreaAdministrador/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>