<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">						
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/buscaNrSerieOpPedido'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/buscaNrSerieOpPedido'); ?>">Voltar</a> <i class="la la-certificate"></i> Editar Nr. de Série - PEDIDO #<?=$pedido_id?>
						</h3>						
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			
			<div class="m-portlet__body">	
				<div class="form-group m-form__group row">
					<div class="col-lg-6">
						<label>Modelo a ser alterado:</label>
						<div class="m-input-icon m-input-icon--right">
							<select class="form-control" name="pedido_inicial" id="pedido_origem">
								<option>Selecione um modelo</option>
								<?php foreach($dados as $dado){ ?>
									<option value="<?=$dado['id']?>" nr_serie="<?=$dado['id']?>" pedido_id="<?php echo $dado['item_id']?>" status_pedido_id = "<?=$dado['status_pedido_id']?>" ><?=$dado['label_modelo']?></option>
								<?php } ?>
							</select>
							<input type="hidden" id="pedido_id" value="<?=$pedido_id?>">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-code"></i></span>
							</span>
						</div>
					</div>
					<div class="col-lg-1">
						<label class=""> </label>
						<div class="m-input-icon m-input-icon--right" style="text-align: center; ">
							<i class="fa fa-refresh" style="font-size: 3.5rem;color: #ffcc00;"></i>
						</div>
					</div>
					<div class="col-lg-5">
						<label class="">Nr. de séries disponíveis para alteração:</label>
						<div class="m-input-icon m-input-icon--right">
							<select class="form-control" name="pedido_final" id="nr_serie_final">
								<option>Selecione um nr de série</option>
								<?php foreach($estoques as $estoque){ ?>
									<option value="<?=$estoque['id']?>" nr_serie="<?=$estoque['id']?>" ><?='Nr. Série: '.$estoque['id']. ' - '. $estoque['modelo'].' - '.$estoque['combustivel']?></option>
								<?php } ?>
							</select>
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-code"></i></span></span>
						</div>	
					</div>						
				</div>	
				<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="button" name="salvar" id="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>									
			</div>	 				
            
			
			<!--end::Form-->
		</div>
</div>	