<div class="m-content">
		<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<i class="flaticon-diagram" style="color: #464e3f;" ></i>
						</span>
						<h3 class="m-portlet__head-text" style="color: #000;">
							Editar Carteira - Equipe Comercial
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools"></div>
			</div>
			<div class="m-portlet__body" >	
				<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarCarteiras');?>" method="post" enctype="multipart/form-data" id="form_area">			
					<div class="form-group m-form__group row">								
						<div class="col-lg-6">
							<label>Responsável pela área:</label>
							<div class="m-input-icon m-input-icon--right">							
								<select  class="form-control"  maxlength="250" name="usuario_id" placeholder="Selecione um novo responsável" id="usuarios" required>
								<?php foreach($usuarios as $usuario){?>
									<option value="<?php echo $usuario['id']; ?>" <?php echo ($usuario['id'] == $usuario_id) ? "selected='selected'" : ''; ?> >
										<?php echo $usuario['nome']; ?>
									</option>
								<?php } ?>
								</select>
							</div>
						</div>
					</div>				
					<div class="form-group m-form__group row">	
						<div class="col-lg-5">
							<label>Indicadores:</label>
							<div class="m-input-icon m-input-icon--left">
								<select class="custom-select" id="indicadores" multiple style="height: 450px;">
									<?php foreach($indicadores_disponiveis as $indicador_disponivel){ ?>
									<option value="<?php echo $indicador_disponivel['id']; ?>" indicador_id="<?php echo $indicador_disponivel['id'];?>">
										<?php echo mb_strtoupper($indicador_disponivel['nome']).' - '.mb_strtoupper($indicador_disponivel['empresa']); ?>									
									</option>
									<?php } ?>	
								</select>	
							</div>
						</div>
						<div class="col-lg-2" style="text-align: center;margin: 15% auto;">
							<a class="btn btn-outline-primary btn-lg m-btn m-btn--icon" id="btnLeft">
								<span>
									<i class="la la-arrow-left" ></i>
								</span>
							</a>
							<a class="btn btn-outline-primary btn-lg m-btn m-btn--icon" id="btnRight">
								<span>
									<i class="la la-arrow-right" ></i>
								</span>
							</a>
						</div>
						<div class="col-lg-5" id="div_selecionados" indice="0">
							<label>Selecionados:</label>
							<div class="m-input-icon m-input-icon--left">
								<select class="custom-select" id="selecionados" name="selecionados[]" multiple style="height: 450px;">
								<?php foreach($indicadores_selecionados as $indicador_selecionado){ ?>
									<option value="<?php echo $indicador_selecionado['indicador_id']; ?>" selected="selected" indicador_id="<?php echo $indicador_selecionado['indicador_id'];?>">
										<?php echo mb_strtoupper($indicador_selecionado['nome']).' - '.mb_strtoupper($indicador_selecionado['empresa']); ?>
									</option>
								<?php } ?>
								</select>
								<?php foreach($indicadores_selecionados as $indicador_selecionado){	?>
									<input type="hidden" name="indicadores[]" value="<?php echo $indicador_selecionado['indicador_id'];?>" id="<?php echo $indicador_selecionado['indicador_id'];?>" />
								<?php } ?>
							</div>
						</div>
					</div>
					
					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
									<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
								</div>							
							</div>
						</div>
					</div>
				</form>
			</div>						
		</div>
		<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" style="display: none;">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
							Áreas de atuação
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->			
		</div>
	</div>

	
	<!--end::Portlet-->	
</div>	
<div class="modal fade" id="m_lista_anexo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_lista_anexo_title" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
				<div class="row anexos">
					
				</div>				
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

