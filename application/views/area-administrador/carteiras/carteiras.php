<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-diagram" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Carteiras - Equipe Comercial <a href="<?php echo base_url('AreaAdministrador/cadastrarCarteira')?>" class="" style="color: #ffcc00; font-weight: bold;">
							<i class="la la-plus-circle" style="font-size: 38px;"></i>
						</a>
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools"></div>
		</div>		
		<div class="m-portlet__body">
			<!--begin: Search Form -->
			<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
				<div class="row align-items-center">
					<div class="col-xl-8 order-2 order-xl-1">
						<div class="form-group m-form__group row align-items-center">
							<div class="col-md-4">
								<div class="m-input-icon m-input-icon--left">
									<input type="text" class="form-control m-input m-input--solid" placeholder="Pesquisar..." id="generalSearch">
									<span class="m-input-icon__icon m-input-icon__icon--left">
										<span><i class="la la-search"></i></span>
									</span>
								</div>
							</div>
							
						</div>
					</div>
								
				</div>
			</div>
			<!--end: Search Form -->

			<!--begin: Datatable -->
			<table id="html_table" width="100%">
				<thead>
					<tr>
						<th title="id" style="width: 5% !important;">ID</th>
						<th title="usuario" style="width: 10% !important;">Consultor</th>										
						<th title="estados" style="width: 80% !important;">Indicadores</th>										
						<th title="" style="width: 5% !important;">Ações</th>
					</tr>
				</thead>
				<tbody>
					<tr>
					<?php foreach($dados as $dado){ ?>
						<td style="width: 5% !important;"><?php echo $dado['id']; ?></td>
						<td style="width: 10% !important;"><?php echo $dado['nome']; ?></td>
						<td style="width: 80% !important;"><?php echo $dado['indicadores']; ?></td>
						<td data-field="Actions" class="m-datatable__cell" style="width: 5% !important;">
							<span style="overflow: visible; width: 110px;">								
								<a href="<?php echo base_url('AreaAdministrador/editarCarteiras/'.$dado['equipe_id']); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar" carteira_equipe="<?php echo $dado['equipe_id']; ?>" >
									<i class="la la-edit"></i>
								</a>
								<button id="excluir" onclick="excluirCarteiras(<?php echo $dado['equipe_id']; ?>);" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" carteira_equipe="<?php echo $dado['equipe_id']; ?>">
									<i class="la la-trash"></i>
								</button>
							</span>
						</td>
					</tr>
						<?php } ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>		        
</div>				
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
            title: "OK!",
            text: 'Cadastro realizado com sucesso!',
            type: "success"
        }).then(function() {
		 	//window.location = base_url+'AreaAdministrador/Entregas';
		}); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>