<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">						
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/garantia'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/garantia'); ?>">Voltar</a> <i class="la la-certificate"></i> Editar Garantia						
						</h3>						
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarGarantia');?>" method="post">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>N° de Série:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" 	name="id"	class="form-control m-input" placeholder=""  value="<?php echo $dados['id']; ?>" readonly="readonly">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-code"></i></span>
								</span>
							</div>
						</div>
						<div class="col-lg-3">
							<label class="">Pedido #:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" class="form-control m-input" placeholder="" required value="<?php echo $dados['pedido_id']; ?>" readonly="readonly">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>	
						</div>
						<div class="col-lg-3">
							<label class="">Modelo:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" class="form-control m-input" placeholder="" required value="<?php echo $dados['modelo']; ?>" readonly="readonly">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>	
						</div>
						<div class="col-lg-3">
							<label class="m-input-icon m-input-icon--right">Combustíveis:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" class="form-control m-input" placeholder="" required value="<?php echo $dados['combustivel']; ?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>	
						</div>
					</div>	
					<div class="form-group m-form__group row">												
						<div class="col-lg-6">
							<label class="m-input-icon m-input-icon--right">Data Início garantia:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="dt_inicio_garantia" id="dt_inicio_garantia" class="form-control m-input data" placeholder="" required="required" value="<?php echo ($dados['dt_inicio_garantia'] != "") ? date('d/m/Y',strtotime($dados['dt_inicio_garantia'])) : ""; ?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-calendar"></i></span></span>
							</div>
						</div>
						<div class="col-lg-6">
							<label class="m-input-icon m-input-icon--right">Data Fim garantia:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="dt_fim_garantia" id="dt_fim_garantia" class="form-control m-input data" placeholder="" required="required" value="<?php echo ($dados['dt_fim_garantia'] != '') ? date('d/m/Y', strtotime($dados['dt_fim_garantia'])) : ''; ?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-calendar"></i></span></span>
							</div>	
						</div>	
					</div> 						
				</div>	 				
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	