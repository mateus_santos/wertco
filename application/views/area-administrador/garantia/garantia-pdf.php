<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 9px;
 	color: #333;
 	width: 100%;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px;
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{
	padding: 2px !important;
	border-bottom: 1px solid #ffcc00;
	border-right: 1px solid #ffcc00;
	font-size: 12px;	
}
</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;" >
		<div class="cabecalho">			
			<img src="./bootstrap/img/cabecalhoCertificadoGarantia.jpg" style="margin: 5px 25%;" />			
		</div>
		<div class="corpo" style="padding-top: 150px;">	
			<table cellspacing="0">				
				<tbody>
					<tr>
						<td colspan="2" style="text-align: center;background: #ffcc00;"><b>DADOS DO CLIENTE</b></td>
					</tr>
					<tr>
						<td style="background: #ffcc00;">CÓDIGO</td>
						<td style="background: #FFE065;"><?php echo $empresa['empresa_id']; ?></td>
					</tr>
					<tr>
						<td style="background: #ffcc00;">RAZÃO SOCIAL:</td>
						<td style="background: #FFE065;"><?php echo $empresa['razao_social']; ?></td>
					</tr>
					<tr>
						<td style="background: #ffcc00">CNPJ: </td>
						<td style="background: #FFE065"><?php echo $empresa['cnpj']; ?> </td>
					</tr>
					<tr>
						<td style="background: #ffcc00">PEDIDO: </td>
						<td style="background: #FFE065"><?php echo $empresa['pedido_id']; ?></td>
					</tr>
					<tr>
						<td style="background: #ffcc00">NOTA FISCAL:</td>
						<td style="background: #FFE065"><?php echo $empresa['nr_nf']; ?> </td>					
					</tr>
					<tr>
						<td style="background: #ffcc00">DATA DE EMISSÃO DA NOTA FISCAL:</td>
						<td style="background: #FFE065"><?php echo date('d/m/Y', strtotime($empresa['dt_emissao_nf'])); ?> </td>		
					</tr>									
				</tbody>
			</table>	
			<hr />
			<table cellspacing="0" style="margin-top: 5px;">	
				<tbody>
					<tr>
						<td style="background: #ffcc00"><b>CÓDIGO</b></td>
						<td style="background: #ffcc00"><b>MODELO | DESCRIÇÃO</b></td>
						<td style="background: #ffcc00"><b>NR. SÉRIE</b></td>
						<td style="background: #ffcc00"><b>VALIDADE GARANTIA</b></td>
					</tr>					
					<?php foreach($produtos as $produto){?>
					<tr>	
						<td style="background: #ffcc00"><?php echo $produto['codigo']; ?></td>
						<td style="background: #FFE065"><?php echo $produto['modelo'].'|'.$produto['descricao']; ?></td>
						<td style="background: #FFE681"><?php echo $produto['nr_serie']; ?></td>
						<td style="background: #FFE065"><?php echo date('d/m/Y', strtotime($produto['dt_fim_garantia'])); ?></td>
					</tr>	
					<?php }	?>					
				</tbody>
			</table>			
		</div>
		<div class="cabecalho" style="PAGE-BREAK-BEFORE: always">
			<img src="./bootstrap/img/cabecalhoCertificadoGarantia.jpg" style="margin: 5px 25%;" >	
		</div>		
		<div class="info" style="padding-top: 100px;">
			<h3>Certificado de Garantia</h3>
	<p>Este documento compreende as condições gerais de garantia aplicáveis aos produtos fabricados pela Wertco sendo regulados pelas disposições abaixo especificadas.</p>
    <p><b>1. Termos utilizados neste documento: </b><br>
        &nbsp;&nbsp;1.1. Fabricante: WERTCO INDUSTRIA, COMERCIO E SERVICOS EM BOMBAS DE ABASTECIMENTO DE COMBUSTIVEIS, IMPORTACAO E EXPORTACAO LTDA.<br>
        &nbsp;&nbsp;1.2. CNPJ.: 27.314.980/0001-53; <br>
        &nbsp;&nbsp;1.3. Produto: Bomba/Dispenser de abastecimento de combustíveis líquidos (Gasolina, Etanol e Diesel) fabricadas e comercializadas pela Wertco, conforme Nota Fiscal emitida pelo Fabricante;<br> 
        &nbsp;&nbsp;1.4. Cliente: Pessoa física ou jurídica, que efetua a compra do produto, conforme descrito na Nota Fiscal de compra do produto;<br>
        &nbsp;&nbsp;1.5. Startup: Partida inicial do produto, primeira vez que o produto é ligado e acionado para abastecimento.<br>
        &nbsp;&nbsp;1.6. Condições Normais de uso: são aquelas para as quais os equipamentos ou produtos foram projetados e desenvolvidos para funcionar, atendendo às &nbsp;&nbsp;especificações constantes do manual.<br>
        &nbsp;&nbsp;1.7. Defeito de fabricação: falha no equipamento ou produto que impeça o seu normal funcionamento e que não decorra de utilização inadequada, má instalação, desgaste natural, ou outras variações normais e esperadas em máquinas e bombas hidráulicas.<br>
        &nbsp;&nbsp;1.8. Garantia Legal: garantia geral e obrigatória, prevista em lei, pelo prazo de 90 dias a contar da entrega dos produtos e equipamentos duráveis, abrangendo todos os produtos constantes da Nota Fiscal.<br>
        &nbsp;&nbsp;1.9. Garantia Contratual: prazo de garantia adicionado à garantia legal, que somente será válida mediante o preenchimento das condições estabelecidas no presente documento, abrangendo apenas os equipamentos e componentes ora especificados, e pelos prazos e condições de uso assinalados.</p>
   	 <p><b>2. Condições Gerais da Garantia:</b><br/>
        &nbsp;&nbsp;2.1. Além da garantia legal, vigente pelo prazo de 90 (noventa) dias a contar da entrega do produto, a Wertco Industria, Comércio e Serviços em Bombas de Abastecimento de Combustíveis, Importação e Exportação Limitada, assegura ao Cliente aqui identificado a Garantia Contratual contra defeitos de fabricação, desde que assim constatados por técnicos autorizados pela Wertco, ou por seus próprios funcionários, e preenchidas as condições constantes do presente Termo.<br/>
        &nbsp;&nbsp;2.2. Para ativação da garantia, seja legal ou contratual, é obrigação do proprietário o acionamento da assistência técnica do fabricante, o qual terá 30 dias para solucionar o defeito (após a reclamação) através de reparo no equipamento, caso esteja coberto pela garantia. <br/>
        &nbsp;&nbsp;2.3. Para acionamento da garantia, é essencial o envio do maquinário para a assistência técnica, contando o prazo acima estabelecido a partir do recebimento do equipamento em nossa sede.<br/>
        &nbsp;&nbsp;2.4. Caso a constatação do defeito seja realizada in loco, no estabelecimento do cliente, o prazo iniciará a partir da data do diagnóstico.<br/>
        &nbsp;&nbsp;2.5. Em não havendo reparo no prazo de 30 dias, o fabricante promoverá a troca dos componentes, e caso isso não seja suficiente, do equipamento por outro igual, ou similar.<br/>
        &nbsp;&nbsp;2.6. Havendo solicitação de visita técnica, caso seja constatado defeito de fabricação, a mesma não será cobrada, assim como não serão cobrados os reparos necessários ou troca de produtos e componentes que sejam realizados.<br/>
        &nbsp;&nbsp;2.7. Caso o defeito não esteja coberto pela garantia, decorrendo de outra causa que não esteja coberta, haverá cobrança da visita técnica, além de ser apresentado orçamento de reparo ao cliente, o qual poderá, ou não, ser aprovado.</p>

    <p><b>3. Das Condições da Garantia Contratual:</b><br/>
        &nbsp;&nbsp;3.1. Para aquisição da garantia contratual, o Startup deverá ser solicitado através do site www.wertco.com.br e será realizado pelo Fabricante ou por uma empresa indicada pela Wertco. O cliente tem o prazo máximo de até 180 (cento e oitenta) dias após a emissão da nota fiscal para realização do Startup;<br/>
        &nbsp;&nbsp;3.2. Caso o Startup seja realizado por terceiros, ou fora do prazo acima estabelecido, o cliente perderá, de imediato, a garantia contratual;<br/>
        &nbsp;&nbsp;3.3. No ato do Startup será realizada uma avaliação do equipamento, principalmente quanto às suas condições de transporte, integridade aparente, condições de instalação e funcionamento das atividades básicas, e com a aprovação, o cliente adquire a garantia contratual;<br/>
        &nbsp;&nbsp;3.4. Caso o técnico responsável não aprove a instalação durante o Startup, será apresentado relatório especificando as causas e as adequações/reparos necessários para o correto procedimento, se houverem, às custas do proprietário, dentro do prazo original de 180 (cento e oitenta) dias. Caso exista algum óbice decorrente de defeito coberto pela garantia, o mesmo será sanado pela própria fabricante;<br/>
        &nbsp;&nbsp;3.5. No curso do prazo de garantia contratual, a mão de obra e a substituição de peça(s) com defeito(s) de fabricação, em uso normal do equipamento, serão gratuitas e suportadas pelo fabricante.<br/>
        &nbsp;&nbsp;3.6. Nos mesmos moldes da garantia legal, cabe ao proprietário o acionamento da assistência técnica do fabricante para avaliação da cobertura, e realização de reparos em 30 (trinta dias), sem custo ao cliente, aplicando-se na íntegra o disposto nos itens 2.2, 2.3, 2.4, 2.5, 2.6 e 2.7.<br/>
    </p>
    <p><b>4. Da Ausência de Cobertura de Garantia:</b><br/>
        &nbsp;&nbsp;4.1. Não são considerados defeitos de fabricação e, portanto, não estão cobertos pela garantia os seguintes procedimentos:<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;4.1.1. Instalação do produto ou orientação de manuseio.<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;4.1.2. Resolução problemas na linha de combustível, tais como vazamentos, distância e altura manométrica do tanque fora de padrão, ou qualquer outra definição especificada no Manual do Proprietário.<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;4.1.3. Despesas com serviços de instalações, peças e acessórios sujeitos a desgaste natural, descartáveis e removíveis, tais como: filtros, Breakaway (válvula de segurança), correias, mangueiras, bicos, adesivos e conexões giratórias. <br/>
            &nbsp;&nbsp;&nbsp;&nbsp;4.1.4. Serviços de aferição, limpeza de filtros, limpeza de densímetro e ajuste da correia do motor.<br/>
        &nbsp;&nbsp;4.2. Verificação e teste de calibração rotineiros nos equipamentos.<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;4.2.1.  A responsabilidade pela aferição da calibragem dos equipamentos é do proprietário: Conforme Orienta a Norma Brasileira ABNT NBR 15594-1 - Posto revendedor de combustível automotivo (PRC) Parte 1: Operação e procedimentos de inspeção e manutenção:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;<i>"A aferição deve atender na íntegra as prescrições das Portarias do INMETRO, referentes aos equipamentos medidores de abastecimento, sendo recomendado ser realizado diariamente, antes do início das operações.
Para o estabelecimento que opere por 24 h ininterruptas, o operador deve definir um horário para a realização da aferição.
No caso de anormalidade constatada na aferição dos medidores, o responsável pelo posto revendedor de combustível veicular deve imediatamente paralisar a utilização do equipamento. Entende-se como paralisação de equipamentos o atendimento às seguintes atividades:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) lacrar o bico de abastecimento no receptáculo do bico de abastecimento na unidade abastecedora;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) sinalizar que o bico de abastecimento está fora de uso;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c) comunicar à empresa autorizada para a manutenção técnica."</i><br/>
            &nbsp;&nbsp;&nbsp;&nbsp;4.2.2. A simples perda de calibragem não importa em defeito de fabricação: a perda de calibração em bombas hidráulicas, de qualquer espécie, é normal e esperada, decorrente de inúmeras causas. Por tratar-se de produto corpóreo, ainda que líquido, o simples atrito dos materiais poderá ocasionar alteração na calibragem, além de outras causas. É importante que o proprietário do equipamento esteja constantemente verificando a calibragem, em atenção às normativas aplicáveis.<br/>
        &nbsp;&nbsp;4.3. A garantia aqui descrita não cobre lucros cessantes ou perdas e danos do Cliente devido ao mau funcionamento do produto. Em qualquer circunstância, a responsabilidade máxima da Wertco para com o Cliente estará limitada ao valor de compra do produto apresentado na Nota Fiscal de Venda.</p>
    <p><b>5. Prazo de Garantia: </b><br/>
        &nbsp;&nbsp;5.1. A Garantia Legal terá vigência pelo prazo de 90 (noventa) dias, por força da lei, para todos os itens constantes na nota fiscal. <br/>
        &nbsp;&nbsp;5.2. A Garantia Contratual terá a vigência, apenas para os itens específicos e listados como "Itens Específicos com Garanta Contratual", pelos prazos descritos na Tabela 1 - Prazo Contratual), que serão somados à Garantia Legal. No total, a soma da garantia legal e contratual, atingirá 360 (trezentos e sessenta) dias ou 720 (setecentos e vinte) dias, partir da data da nota fiscal de compra, ou o atingimento da volumetria de 2 milhões de litros, o que ocorrer primeiro. <br/>
        &nbsp;&nbsp;5.3. Baterias e demais itens de produção não especificados e não listados como "Itens Específicos com Garanta Contratual" neste termo de garantia, terão apenas a cobertura da Garantia Legal.<br/>
       </p>
       <div class="cabecalho" style="PAGE-BREAK-BEFORE: always">
			<img src="./bootstrap/img/cabecalhoCertificadoGarantia.jpg" style="margin: 5px 25%;" >	
		</div>
       		<table cellspacing="0" style="padding-top: 150px;">				
				<tbody>
					<tr>
						<td colspan="3" style="text-align: center;background: #ffcc00;"><b>ITENS ESPECÍFICOS COM GARANTA ESTENDIDA</b></td>
					</tr>
					<tr>
						<td style="background: #ffcc00;"><b>ITEM</b></td>
						<td style="background: #FFE065;"><b>GARANTIA LEGAL</b></td>
						<td style="background: #FFE681"><b>GARANTIA CONTRATUAL</b></td>	
					</tr>
					<tr>
						<td style="background: #ffcc00;">UNIDADE BOMBEADORA</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">630 dias ou 2 milhões de litros, o que ocorrer primeiro</td>
					</tr>
					<tr>
						<td style="background: #ffcc00;">VÁLVULA</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">630 dias ou 2 milhões de litros, o que ocorrer primeiro</td>
					</tr>
					<tr>
						<td style="background: #ffcc00;">MOTOR</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">630 dias ou 2 milhões de litros, o que ocorrer primeiro</td>
					</tr>
					<tr>
						<td style="background: #ffcc00;">ACIONAMENTO DE BICO</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">-</td>
					</tr>
					<tr>
						<td style="background: #ffcc00;">BREAKAWAY*</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">-</td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">MANGUEIRA</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">-</td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">JUNTA GIRATÓRIA</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">-</td>					
					</tr>									
					<tr>
						<td style="background: #ffcc00;">BICO</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">-</td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">DENSÍMETRO</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">-</td>					
					</tr>	
					<tr>
						<td style="background: #ffcc00;">FILTRO</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">-</td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">CORREIA</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">-</td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">COMPONENTES ELETRÔNICOS, EXCETO AS BATERIAS (PLACA INTERFACE E CPU)</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">630 dias</td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">Baterias</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681"></td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">COMPONENTES DA MÍDIA</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">270 DIAS</td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">VIDROS DOS MOSTRADORES</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">630 DIAS</td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">VIDROS DOS MOSTRADORES</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">630 DIAS</td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">CHAPARIA</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">630 DIAS</td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">ESTRUTURA</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">630 DIAS</td>					
					</tr>
					<tr>
						<td style="background: #ffcc00;">COMPONENTES EM SMC</td>
						<td style="background: #FFE065;">90</td>
						<td style="background: #FFE681">630 DIAS</td>					
					</tr>					
				</tbody>
			</table>
			<span>Tabela 1: Prazo de garantia</span><br/><br/>
			<p><b>Observação:</b> Breakaway é uma válvula de segurança e sua atuação é justamente o rompimento. Caso o rompimento seja indevido, o componente deverá ser enviado para a fábrica para avaliação e posterior substituição caso seja constatado defeito de fabricação.</p>
		<div class="cabecalho" style="PAGE-BREAK-BEFORE: always">
			<img src="./bootstrap/img/cabecalhoCertificadoGarantia.jpg" style="margin: 5px 25%;" >	
		</div>
    <p style="padding-top: 150px;"><b>6. Causas Excludentes da Garantia:</b> <br/>
        &nbsp;&nbsp;6.1. Não será prestada qualquer garantia caso os danos sofridos pelo produto tenham ocorrido em função de armazenamento, transporte e/ou manuseio incorretos ou inadequados, após a entrega do mesmo pela Fabricante ao Cliente, na forma especificada das "Condições Gerais de Fornecimento";<br/>
        &nbsp;&nbsp;6.2. Não será prestada qualquer garantia se o equipamento sofrer qualquer dano provocado por ações não relacionadas com a fabricação, tais como acidentes de qualquer sorte, ação do fogo ou incêndios não relacionados a defeito de fabricação do equipamento, ação de agentes corrosivos, explosivos, infestação de insetos, ou por agentes da natureza (raios, inundações, desabamentos, etc.);<br/>
        &nbsp;&nbsp;6.3. Não será prestada qualquer garantia no caso de as condições de instalações tenham ocorrido fora dos parâmetros determinados no Manual do Proprietário, em especial a não adequação do sistema hidráulico, altura do tanque, entre outros;<br/>
        &nbsp;&nbsp;6.4. Não será prestada qualquer garantia no caso de o equipamento ter sido ligado à rede elétrica imprópria, ou sujeita a flutuações excessivas;<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;6.5. Não será prestada qualquer garantia no caso de o equipamento apresentar sinais de ter sido violado ou consertado por pessoas não autorizadas pela Wertco.<br/>
        &nbsp;&nbsp;6.6. Não será prestada qualquer garantia no caso de uso em desacordo com o manual de instruções e os danos sofridos pelo produto e seus acessórios ocorrerem em função de sua utilização inadequada, incorreta ou não autorizada, em desacordo com as especificações fixadas pela Fabricante em sua proposta comercial;<br/>
        &nbsp;&nbsp;6.7. Não será prestada qualquer garantia no caso de as avarias sofridas pelo produto e seus acessórios ocorrerem em consequência de sua utilização para finalidades diversas das especificadas pela Fabricante em sua proposta comercial;<br/>
       &nbsp;&nbsp;6.8. Não será prestada qualquer garantia no caso de o mau funcionamento do produto tenha sido ocasionado pela presença de resíduos sólidos (sujeira) bombeados dos tanques ou tubulações;<br/>
        &nbsp;&nbsp;6.9. Não será prestada qualquer garantia no caso de o produto tiver sofrido, sem autorização prévia e por escrito da Fabricante, qualquer tipo de modificação estética ou funcional;<br/>
       &nbsp;&nbsp;6.10. Não será prestada qualquer garantia no caso de danos sofridos pelo produto decorrentes de atos dolosos ou culposos, praticados por terceiros ou pelo Cliente, como, por exemplo, abalroamento, quebra de vidraria (densímetros, visores, etc.) e atos de vandalismo.<br/>
        &nbsp;&nbsp;6.11. Não será prestada garantia contratual caso o produto não seja instalado e não tenha seu Startup concluído dentro do prazo máximo de 180 (cento e oitenta) dias, contados da data de emissão da respectiva nota fiscal, hipótese em que perderá a referida garantia. <br/>
        &nbsp;&nbsp;6.12. Não será prestada garantia contratual caso o Startup não seja realizado ou acompanhado pela Fabricante ou pelo Serviço Especializado Wertco; <br/>
        &nbsp;&nbsp;6.13. Não será prestada garantia contratual no caso de o equipamento não ser aprovado na avaliação realizada no Startup, conforme referido no item 3.3., e o cliente não tenha realizado os reparos ou correções conforme o item 3.4.<br/>
    </p>
    <p><b>7. Transferência da Garantia Contratual:</b><br/>
        &nbsp;&nbsp;7.1. Caso o proprietário do equipamento o transfira para terceiros, seja através de venda do estabelecimento ou seu arrendamento, a Garantia Contratual somente será mantida na hipótese em que o novo proprietário ou controlador do equipamento assine Termo de Transferência de Garantia com a Fabricante, onde deverá submeter-se à cadastro prévio, assumir responsabilidade solidária pelos custos de reparos não cobertos pela garantia, e declarar-se ciente de todos os termos aplicáveis.<br/>
        &nbsp;&nbsp;7.2. O prazo máximo da garantia mantém-se o mesmo a contar da data de emissão da nota fiscal, independentemente da data da Transferência de titularidade, posse ou o uso do equipamento por terceiros diferente do adquirente principal do produto junto ao fabricante.</p>
    <p>    
    <b>8. Extinção da Garantia:</b> <br/>
        &nbsp;&nbsp;8.1. A Garantia Legal será extinta pela mera passagem do prazo (90 dias), com o advento de seu termo;<br/>
        &nbsp;&nbsp;8.2. A Garantia Contratual, uma vez preenchidos os requisitos para sua obtenção, será extinta quando o equipamento atingir a medição de 2 milhões de litros, ou pelo transcurso dos prazos fixados na Tabela 1 - Prazo de garantia contratual, independentemente de notificação prévia.<br/>
        &nbsp;&nbsp;8.3. Ambas as Garantias também restarão extintas na hipótese de perecimento, desaparecimento ou perdimento dos bens, ou na ocorrência nas hipóteses constantes dos itens 6.3., 6.5., 6.9, ou qualquer ato doloso por parte do proprietário.<br/>
        &nbsp;&nbsp;8.4. A Garantia Contratual será extinta caso haja transferência dos equipamentos em desacordo com o disposto no item 7 do presente documento.
</p>
<p>
   <b> 9. Demais Condições:</b><br/>
        &nbsp;&nbsp;9.1. Quaisquer custos incorridos pela Fabricante oriundos das situações descritas no item 4 serão reembolsados pelo Cliente, de acordo com as taxas de serviços aplicadas pela Fabricante na ocasião do atendimento. <br/>
        &nbsp;&nbsp;9.2. Para casos excepcionais, como, por exemplo, quando o produto se destinar a um local não abrangido por qualquer serviço especializado Wertco, as partes poderão negociar a exclusão total ou parcial das condições de garantia descritas acima. Os termos da exclusão serão previamente discutidos e negociados entre as partes, devendo, necessariamente, constar da Confirmação de Pedido e da respectiva Nota Fiscal. <br/>
       &nbsp;&nbsp;9.3. Tendo a Fabricante cumprido com todos os deveres descritos neste instrumento, esta não poderá ser responsabilizada por quaisquer danos indiretos ou lucros cessantes sofridos pelo Cliente, em virtude de falhas no produto sob garantia.<br/>
       </p>
		</div>
	</div>	
</body>
</html>