<div class="col-xl-3">			
	<div class="m-portlet m-portlet--bordered-semi" style="max-height: 400px;overflow: auto;background: #1761ac;">
		<div class="m-portlet__head" style="border-bottom: 1px solid #f0f0f0;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h4 class="m-portlet__head-text" style="color: #ffcc00">
						Pedidos Pendentes (Até 10 dias)
					</h4>
				
				</div>
			</div>													
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
						<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listar pedidos." onclick="listar_pedidos_abertos(0,10);">
							<i class="fa fa-list" style="color: #ffcc00"></i>
						</a>
					</li>
				</ul>
			</div>												
		</div>
		<div class="m-portlet__body" style="padding: 0 0 0 2rem">
		<?php 
			if( $pedidos_abertos_10['total'] > 0 ){ ?>
				<div class="m-widget4 ">
					<div class="m-widget4__item">							
						<div class="m-widget4__info" style="padding-left: 0px;">
							<i class="fa fa-shopping-cart" style="font-size: 5em;color: #ffcc00;"></i>
						</div>
						<span class="m-widget4__ext" style="min-width: 180px;text-align: center;">
							<span class="m-widget4__number m--font-brand" style="font-size: 5em; color: #ffcc00 !important;">
								<?=$pedidos_abertos_10['total']?> </span>
						</span>						
					</div>
				</div>		
		
		<?php }else{ ?>
			<h4 class="m--font-warning" style="margin: 30px 25px 57px 0px;">Não há pedidos nesse período!</h4>
		<?php } ?>	
		</div>
		
	</div>
	<!--end:: Widgets/Authors Profit-->
</div>
<div class="col-xl-3">			
	<div class="m-portlet m-portlet--bordered-semi" style="max-height: 400px;overflow: auto;background: #1761ac;">
		<div class="m-portlet__head" style="border-bottom: 1px solid #f0f0f0;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h4 class="m-portlet__head-text" style="color: #ffcc00">
						Pedidos Pendentes (11 - 30 dias)
					</h4>
				</div>
			</div>										
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
						<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listar pedidos." onclick="listar_pedidos_abertos(11,30);">
							<i class="fa fa-list" style="color: #ffcc00"></i>
						</a>
					</li>
				</ul>
			</div>												
		</div>
		<div class="m-portlet__body" style="padding: 0 0 0 2rem">
		<?php 
			if( $pedidos_abertos_11['total'] > 0 ){ ?>
				<div class="m-widget4 ">
					<div class="m-widget4__item">							
						<div class="m-widget4__info" style="padding-left: 0px;">
							<i class="fa fa-shopping-cart" style="font-size: 5em;color: #ffcc00;"></i>
						</div>
						<span class="m-widget4__ext" style="min-width: 180px;text-align: center;">
							<span class="m-widget4__number m--font-brand" style="font-size: 5em; color: #ffcc00 !important;">
								<?=$pedidos_abertos_11['total']?> </span>
						</span>						
					</div>
				</div>		
		
		<?php }else{ ?>
			<h4 class="m--font-warning" style="margin: 30px 25px 57px 0px;">Não há pedidos nesse período!</h4>
		<?php } ?>	
		</div>
		
	</div>
	<!--end:: Widgets/Authors Profit-->
</div>
<div class="col-xl-3">			
	<div class="m-portlet m-portlet--bordered-semi" style="max-height: 400px;overflow: auto;background: #1761ac;">
		<div class="m-portlet__head" style="border-bottom: 1px solid #f0f0f0;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h4 class="m-portlet__head-text" style="color: #ffcc00">
						Pedidos Pendentes (31 - 60 dias)
					</h4>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
						<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listar pedidos." onclick="listar_pedidos_abertos(31,60);">
							<i class="fa fa-list" style="color: #ffcc00"></i>
						</a>
					</li>
				</ul>
			</div>												
		</div>
		<div class="m-portlet__body" style="padding: 0 0 0 2rem">
		<?php 
			if( $pedidos_abertos_31['total'] > 0 ){ ?>
				<div class="m-widget4 ">
					<div class="m-widget4__item">							
						<div class="m-widget4__info" style="padding-left: 0px;">
							<i class="fa fa-shopping-cart" style="font-size: 5em;color: #ffcc00;"></i>
						</div>
						<span class="m-widget4__ext" style="min-width: 180px;text-align: center;">
							<span class="m-widget4__number m--font-brand" style="font-size: 5em; color: #ffcc00 !important;">
								<?=$pedidos_abertos_31['total']?> </span>
						</span>						
					</div>
				</div>		
		
		<?php }else{ ?>
			<h4 class="m--font-warning" style="margin: 30px 25px 57px 0px;">Não há pedidos nesse período!</h4>
		<?php } ?>	
		</div>		
	</div>
	<!--end:: Widgets/Authors Profit-->
</div>
<div class="col-xl-3">			
	<div class="m-portlet m-portlet--bordered-semi" style="max-height: 400px;overflow: auto;background: #1761ac;">
		<div class="m-portlet__head" style="border-bottom: 1px solid #f0f0f0;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h4 class="m-portlet__head-text" style="color: #ffcc00">
						Pedidos Pendentes (61 dias ou mais)
					</h4>
				</div>
			</div>	
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
						<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listar pedidos." onclick="listar_pedidos_abertos(61,10000);">
							<i class="fa fa-list" style="color: #ffcc00"></i>
						</a>
					</li>
				</ul>
			</div>												
		</div>
		<div class="m-portlet__body" style="padding: 0 0 0 2rem">
		<?php 
			if( $pedidos_abertos_61['total'] > 0 ){ ?>
				<div class="m-widget4 ">
					<div class="m-widget4__item">							
						<div class="m-widget4__info" style="padding-left: 0px;">
							<i class="fa fa-shopping-cart" style="font-size: 5em;color: #ffcc00;"></i>
						</div>
						<span class="m-widget4__ext" style="min-width: 180px;text-align: center;">
							<span class="m-widget4__number m--font-brand" style="font-size: 5em; color: #ffcc00 !important;">
								<?=$pedidos_abertos_61['total']?> </span>
						</span>						
					</div>
				</div>		
		
		<?php }else{ ?>
			<h4 class="m--font-warning" style="margin: 30px 25px 57px 0px;">Não há pedidos nesse período!</h4>
		<?php } ?>	
		</div>		
	</div>
	<!--end:: Widgets/Authors Profit-->
</div>