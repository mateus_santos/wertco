<div class="col-xl-3">			
	<div class="m-portlet m-portlet--bordered-semi" style="max-height: 400px;overflow: auto;">
		<div class="m-portlet__head" style="border-bottom: 1px solid #f0f0f0;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Abertos (Até 10 dias)
					</h3>
				
				</div>
			</div>	
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
						<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listar orçamentos." onclick="listar_orcamentos_abertos(0,10);">
							<i class="fa fa-list" style="color: #1761ac"></i>
						</a>
					</li>
				</ul>
			</div>												
		</div>
		<div class="m-portlet__body" style="padding: 0 0 0 2rem">
		<?php 
			if( $orcamentos_abertos_10['total'] > 0 ){ ?>
				<div class="m-widget4 ">
					<div class="m-widget4__item">							
						<div class="m-widget4__info" style="padding-left: 0px;">
							<i class="fa fa-shopping-cart" style="font-size: 5em;color: #1761ac;"></i>
						</div>
						<span class="m-widget4__ext" style="min-width: 180px;text-align: center;">
							<span class="m-widget4__number m--font-brand" style="font-size: 5em; color: #1761ac !important;">
								<?=$orcamentos_abertos_10['total']?> </span>
						</span>						
					</div>
				</div>		
		
		<?php }else{ ?>
			<h4 class="m--font-primary" style="margin: 30px 0px 57px 0px;">Não há orçamentos nesse período!</h4>
		<?php } ?>	
		</div>
		
	</div>
	<!--end:: Widgets/Authors Profit-->
</div>
<div class="col-xl-3">			
	<div class="m-portlet m-portlet--bordered-semi" style="max-height: 400px;overflow: auto;">
		<div class="m-portlet__head" style="border-bottom: 1px solid #f0f0f0;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Abertos (11 - 30 dias)
					</h3>
				</div>
			</div>	
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
						<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listar orçamentos." onclick="listar_orcamentos_abertos(11,30);">
							<i class="fa fa-list" style="color: #1761ac"></i>
						</a>
					</li>
				</ul>
			</div>									
		</div>
		<div class="m-portlet__body" style="padding: 0 0 0 2rem">
		<?php
			if( $orcamentos_abertos_11['total'] > 0 ){ ?>
				<div class="m-widget4 ">
					<div class="m-widget4__item">							
						<div class="m-widget4__info" style="padding-left: 0px;">
							<i class="fa fa-shopping-cart" style="font-size: 5em;color: #1761ac;"></i>
						</div>
						<span class="m-widget4__ext" style="min-width: 180px;text-align: center;">
							<span class="m-widget4__number m--font-brand" style="font-size: 5em; color: #1761ac !important;">
								<?=$orcamentos_abertos_11['total']?> </span>
						</span>						
					</div>
				</div>		
		
		<?php }else{ ?>
			<h4 style="margin: 30px 0px 57px 0px;color: #1761ac;">Não há orçamentos nesse período!</h4>
		<?php } ?>	
		</div>
	</div>
	<!--end:: Widgets/Authors Profit-->
</div>
<div class="col-xl-3">			
	<div class="m-portlet m-portlet--bordered-semi" style="max-height: 400px;overflow: auto;">
		<div class="m-portlet__head" style="border-bottom: 1px solid #f0f0f0;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Abertos (31 - 60 dias)
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
						<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listar orçamentos." onclick="listar_orcamentos_abertos(31,60);">
							<i class="fa fa-list" style="color: #1761ac"></i>
						</a>
					</li>
				</ul>
			</div>										
		</div>
		<div class="m-portlet__body" style="padding: 0 0 0 2rem">
		<?php
			if( $orcamentos_abertos_31['total'] > 0 ){ ?>
				<div class="m-widget4 ">
					<div class="m-widget4__item">							
						<div class="m-widget4__info" style="padding-left: 0px;">
							<i class="fa fa-shopping-cart" style="font-size: 5em;color: #1761ac;"></i>
						</div>
						<span class="m-widget4__ext" style="min-width: 180px;text-align: center;">
							<span class="m-widget4__number m--font-brand" style="font-size: 5em; color: #1761ac !important;">
								<?=$orcamentos_abertos_31['total']?> </span>
						</span>						
					</div>
				</div>		
		
		<?php }else{ ?>
			<h4 class="" style="margin: 30px 0px 57px 0px; color: #1761ac">Não há orçamentos nesse período!</h4>
		<?php } ?>	
		</div>
	</div>
	<!--end:: Widgets/Authors Profit-->
</div>
<div class="col-xl-3">			
	<div class="m-portlet m-portlet--bordered-semi" style="max-height: 400px;overflow: auto;">
		<div class="m-portlet__head" style="border-bottom: 1px solid #f0f0f0;">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Abertos (61 dias ou mais)
					</h3>
				</div>
			</div>	
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
						<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listar orçamentos." onclick="listar_orcamentos_abertos(61,10000);">
							<i class="fa fa-list" style="color: #1761ac"></i>
						</a>
					</li>
				</ul>
			</div>									
		</div>
		<div class="m-portlet__body" style="padding: 0 0 0 2rem">
		<?php
			if( $orcamentos_abertos_61['total'] > 0 ){ ?>
				<div class="m-widget4 ">
					<div class="m-widget4__item">							
						<div class="m-widget4__info" style="padding-left: 0px;">
							<i class="fa fa-shopping-cart" style="font-size: 5em;color: #1761ac;"></i>
						</div>
						<span class="m-widget4__ext" style="min-width: 180px;text-align: center;">
							<span class="m-widget4__number m--font-brand" style="font-size: 5em; color: #1761ac !important;">
								<?=$orcamentos_abertos_61['total']?> </span>
						</span>						
					</div>
				</div>		
		
		<?php }else{ ?>
			<h4 class="" style="margin: 30px 0px 57px 0px; color: #1761ac">Não há orçamentos nesse período!</h4>
		<?php } ?>	
		</div>
	</div>
	<!--end:: Widgets/Authors Profit-->
</div>