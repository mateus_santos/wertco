<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">   Ops &nbsp;&nbsp;    </h3>
                    <div class="m-portlet__head-caption cadastrar_orcamento">
                        <a href="<?php echo base_url('AreaAdministrador/cadastraOps')?>" class="" style="color: #ffcc00; font-weight: bold;"> <i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar Nova Ordem de Produção"></i> </a>
                    </div>
                </div>
            </div>
            <div class="m-list__content">
                <div class="m-list-badge" style="margin-top: 25px;"> <span class="m-list-badge__item">&nbsp;Liberada;&nbsp;<span class="m-badge m-badge--secondary"></span></span> <span class="m-list-badge__item">&nbsp;Em Execução&nbsp;&nbsp;<span class="m-badge m-badge--info"></span></span> <span class="m-list-badge__item">&nbsp;Concluída&nbsp;&nbsp;<span class="m-badge m-badge--success"></span></span> <span class="m-list-badge__item">&nbsp;Cancelada&nbsp;&nbsp;<span class="m-badge m-badge--warning" style="background: #ff9800;"></span></span> <span class="m-list-badge__item">&nbsp;Planejada&nbsp;&nbsp;<span class="m-badge m-badge" style="background: #18cfe6;"></span></span>
                </div>
            </div>
            <a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: -25px;" data-toggle="m-tooltip" data-placement="top" title="Tour Virtual" id="tour_virtual"> <i class="la la-info"></i> </a>
        </div>
        <div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
            <!--begin: Datatable -->
            <table class="" id="html_table" width="100%">
                <thead>
                    <tr>
                        <th style="text-align: center;"># OP</th>
                        <th style="text-align: center;"># Pedido</th>
                        <th style="text-align: center;">Cliente</th>
                        <th style="text-align: center;">Descrição Op</th>
                        <th style="text-align: center;">Situação</th>
                        <th style="text-align: center;">Modelo</th>
                        <th style="text-align: center;">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($dados as $dado){
                    	if( $dado['status_op_id'] == 1 ){
                        	$status = "btn m-btn--pill m-btn--air btn-secondary";
                            $background = '';
                        }elseif( $dado['status_op_id'] == 2 ){
                            $status = "btn m-btn--pill m-btn--air btn-info";
                            $background = '';
                        }elseif( $dado['status_op_id']== 3 ){
                            $status = "btn m-btn--pill m-btn--air btn-success";
                            $background = '';
                        }elseif( $dado['status_op_id'] == 4 ){
                            $status     = "btn m-btn--pill m-btn--air";
                            $background = "style='background: #ff9800;'";
                        }elseif( $dado['status_op_id'] == 5 ){
                            $status     = "btn m-btn--pill m-btn--air";
                            $background = "style='background: #18cfe6;'";						
                        }
                    ?>
                        <tr>
                            <td style="text-align: center;">
                                <?php echo $dado['id']; ?>
                            </td>
                            <td style="text-align: center;">
                                <?php echo $dado['pedido_id']; ?>
                            </td>
                            <td style="text-align: center;">
                                <?php echo $dado['nome_cliente']; ?>
                            </td>
                            <td style="text-align: center;">
                                <?php echo $dado['descricao']; ?>
                            </td>
                            <td style="text-align: center; text-transform: capitalize;">
                                <button class="status <?php echo $status; ?>" status="<?php echo $dado['status_op_id'];?>" <?php echo $background;?> title="Status da op" onclick="status(<?php echo $dado['id']; ?>)"> 
                                    <?php echo ucfirst($dado['status']); ?>
                                </button>
                            </td>
                            <td style="text-align: center;">
                                <?php echo $dado['modelo']; ?>
                            </td>
                            <td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;"> 
                                <span style="overflow: visible;" class="">
                                    <a href="<?php echo ($dado['status_op_id'] == 1) ? base_url('AreaAdministrador/editaOps/'.$dado['id']) : '#'; ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" pedido_id="<?php echo $dado['id']; ?>" >
                                        <i class="la la-edit editar"></i>
                                    </a><!-- <a href="<?php echo base_url('AreaAdministrador/excluirOps/'.$dado['id']); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" pedido_id="<?php echo $dado['id']; ?>" >										<i class="fa fa-trash excluir"></i>
                                    </a>  -->
                                    <a href="#" onclick="confirma_exclusao(<?= $dado['id'] ?>)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" pedido_id="<?php echo $dado['id']; ?>" >										
                                        <i class="fa fa-trash excluir"></i>
                                    </a>
                                </span> 
                            </td>
                        </tr>
                        <?php } ?>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
</div>
<!--    
        ************************************************************************************** 			  
        **************************************************************************************
        ************************************ Início Modais ***********************************			**************************************************************************************			**************************************************************************************	
-->
<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> 
                    <span aria-hidden="true">&times;</span> 
                </button>
            </div>
            <div class="modal-body" style="width: 750px;">
                <div class="form-group m-form__group status_orcamento_">
                    <label for="exampleSelect1">Situação da OP</label>
                    <select class="form-control m-input m-input--air" id="status_op">
                        <option value="">Selecione o Status da Op</option>
                        <option value="1">OP LIBERADA</option>
                        <option value="2">EM EXECUÇÃO</option>
                        <option value="3">CONCLUÍDA</option>
                        <option value="4">CANCELADA</option>
                        <option value="5">PLANEJADA</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" id="alterar_status" class="btn btn-primary" op_id="" indicacao="0">Alterar Situação</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Finalizar pedido -->
<div class="modal fade" id="excluir_op" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title modal-finalizar-title">Excluir Op</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body" style="width: 750px;">
                <div class="form-group m-form__group">
                    <div class="form-group m-form__group">                        
                        <textarea class="form-control m-input--air" value="" id="motivo" placeholder="Motivo" ></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" id="excluir_op_enviar" class="btn btn-primary" op="" orcamento_id="" >Enviar</button>
            </div>
        </div>
    </div>
</div>
<!--    **************************************************************************************
 		**************************************************************************************
        ************************************ FIM Modais **************************************			**************************************************************************************			**************************************************************************************	
-->
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">
        swal('Ops!', 'Aconteceu algum problema, reveja seus dados e tente novamente!', 'error');
    </script>
    <?php unset($_SESSION['erro']); } ?>
        <?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
            <script type="text/javascript">
                swal({
                    title: "OK!",
                    text: 'Cadastro realizado com sucesso!',
                    type: "success"
                }).then(function() {
                    window.location = base_url + 'AreaAdministrador/ops';
                });
            </script>
        <?php unset($_SESSION['sucesso']); } ?>
            <?php if ($this->session->flashdata('success') == TRUE){ ?>
                <script type="text/javascript">
                    swal({
                        title: "OK!",
                        text: 'Alteração realizada com sucesso!',
                        type: "success"
                    });
                </script>
        <?php } ?>