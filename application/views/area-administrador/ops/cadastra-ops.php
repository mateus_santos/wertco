<div class="m-content">

	<div class="m-portlet">

			<div class="m-portlet__head">

				<div class="m-portlet__head-caption">

					<div class="m-portlet__head-title">                       

                        

						<span class="m-portlet__head-icon">

							<a href="<?php echo base_url('areaAdministrador/ops'); ?>"><i class="la la-arrow-left"></i></a>

						</span>

						<h3 class="m-portlet__head-text">	

							<a href="<?php echo base_url('areaAdministrador/ops'); ?>">Voltar</a>&nbsp;Cadastro de Op's

							

						</h3>

					</div>

				</div> 

			</div> 

			<!--begin::Form-->

			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/cadastraOps');?>" method="post" enctype="multipart/form-data">

				<div class="m-portlet__body">	

					<div class="form-group m-form__group row">

						<div class="col-lg-12">

							<label style="width: 100%;">Item <span style="float: right;">

									<i class="la la-plus-circle add" style="color: #ffcc00;font-size: 21px;cursor: pointer;" data-toggle="m-tooltip" data-placement="top" title="Adicionar Novo Item"></i>

							</span>

							</label>

							<div class="m-input-icon m-input-icon--right">

								<input type="text" required name="itens" id="itens" class="form-control m-input" placeholder="" required>

								<input type="hidden" required name="item_id" id="item_id" class="form-control m-input" placeholder="" required>

								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>

							</div>

						</div>

						

					</div>									

					<div class="form-group m-form__group row">

						<div class="col-lg-4">

							<label>Quantidade:</label>

							<div class="m-input-icon m-input-icon--right">

								<input type="text" required name="qtd" maxlength="3" id="qtd" class="form-control m-input" placeholder="" required>

								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-paint"></i></span></span>

							</div>

							

						</div>

						<div class="col-lg-4">

							<label>Observações:</label>

							<div class="m-input-icon m-input-icon--right">

								<textarea class="form-control" name="observacoes_manual"></textarea>

							</div>

						</div>

						<div class="col-lg-4">														

							<label>Modelo:</label>	

							<div class="m-input-icon m-input-icon--right">

								<input type="text" name="modelo" id="modelo" class="form-control m-input" placeholder="">

								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-paint"></i></span></span>

							</div>

							

						</div>											

					</div> 

					<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">

						<div class="m-form__actions m-form__actions--solid">

							<div class="row">

								<div class="col-lg-6">

									<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>

									<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>

								</div>							

							</div>

						</div>

					</div>

				</div>

			</div>

			</form>

			<!--end::Form-->

		</div>

<!-- Modal Andamento -->

	<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

		<div class="modal-dialog modal-dialog-centered" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title modal-andamento-title" >Cadastrar Novo Item</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body" style="width: 750px;">

					

					<div class="m-list-timeline__items">

						<label for="exampleSelect1">Descrição do Item</label>

						<input type="text" class="form-control m-input m-input--air" name="descricao_pai" id="descricao_pai" required="required">

					</div>

					

					<hr/><h3>Adicionar Sub-itens que compõe este Ítem<i class="la la-plus-circle add-subitem" indice='0' style="color: #ffcc00;font-size: 21px;cursor: pointer;float: right;"></i></h3>

					<div class="form-group m-form__group row">

						<div class="form-group m-form__group col-lg-6" >

							<label for="exampleSelect1">Ítem</label>							

							<input type="text"  class="form-control m-input m-input--air sub" placeholder="Descrição do Subitem" indice='0' name="descricao_filho[]" filho="produto_id_filho-0" id="descricao_filho-0" style="margin-bottom: 10px;float: left;" />

							<input type="hidden" class="subi" indice='0' name="produto_id_filho[]" id="produto_id_filho-0" style="margin-bottom: 10px;float: left;" />

						</div>

						<div class="form-group m-form__group col-lg-6" >

							<label for="exampleSelect1">Quantidade</label>							

							<input type="text"  class="form-control m-input m-input--air qtd" placeholder="Quanidade do Subitem" indice='0' name="qtd_filho[]" id="qtd_filho-0" style="margin-bottom: 10px;float: right;" /> 

						</div>

					</div>	

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>

					<button type="button" id ="adicionar_item" class="btn btn-primary" status_orcamento="" orcamento_id="">Adicionar Item</button>

				</div>

			</div>

		</div>

	</div>