<div class="m-content">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">						
					<span class="m-portlet__head-icon">
						<i class="la la-pencil" style="color: #ffcc00;"></i>
					</span>
					<h3 class="m-portlet__head-text">	
						&nbsp;&nbsp;&nbsp;&nbsp;REGRAS - Programação de Produção
					</h3>
				</div>
			</div>
		</div>
		<!--begin::Form-->
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/atualizarRegrasPP');?>" method="post">
			<div class="m-portlet__body">	
				<div class="form-group m-form__group row">
					<div class="col-lg-6">
						<label>Nr. Total de Bombas Semana:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required	name="nr_bombas_semana"	class="form-control m-input" placeholder=""  value="<?php echo $dados['nr_bombas_semana'];?>">
							<input type="hidden" name="id" 	class="form-control m-input" placeholder="" value="<?php echo $dados['id'];?>">
							<span class="m-input-icon__icon m-input-icon__icon--right">
								<span><i class="la la-user"></i></span>
							</span>
						</div>
					</div>
					<div class="col-lg-6">
						<label class="">Nr.Total de Bicos Semana:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text"  name="nr_bicos_semana" class="form-control m-input" placeholder="" required value="<?php echo $dados['nr_bicos_semana'];?>">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-envelope-o"></i></span></span>
						</div>
					</div>					
				</div>	 
				<div class="form-group m-form__group row">		
					<div class="col-lg-4">
						<label>Semana Abertura Produção:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" name="dt_abertura_prod" id="dt_abertura_prod" class="form-control m-input" placeholder="" value="<?php echo $dados['dt_abertura_prod'];?>"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
						</div>	
					</div>				
					<div class="col-lg-4">
						<label class="">Semana Atual:</label>
						<div class="m-input-icon m-input-icon--right">
							<h4><?php echo date('d/m/Y', strtotime($dados['data_semana_atual']));?></h4>
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar"></i></span></span>
						</div>							
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>					
            </div>	            
		</form>
		<!--end::Form-->
	</div>
</div>	
<?php if ($this->session->flashdata('erro') == TRUE){	?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
			title: "OK!",
			text: 'Regras - Programação de produção atualizado com sucesso!',
			type: "success"
        }); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>