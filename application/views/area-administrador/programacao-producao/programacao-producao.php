<style type="text/css">
.linha_ano{	
	text-align: left;
    padding-left: 4%;
    border-top: 1px solid #ffcc00;
    height: 50px;
    font-size: 18px;
    font-weight: 500;
    cursor: pointer;
    background: #fff;
    border-bottom: 1px solid #ffcc00;
}

.fa{
	font-size: 20px;
}

.fa-arrow-down{
	color: #ffcc00;
	font-size: 20px;
}

.m-badge{
	font-weight: 600;
    background: #ffcc00;
    color: #000;
    font-size: 1.0em;
}

.m-list__content{
	position: fixed;
	background: #000;
	z-index: 999;
	padding-bottom: 16px;
	padding-top: 16px;
	width: 90%;
}

@media  (max-width: 1024px){
	.m-list__content{
		width: 75% !important;
	}
}

@media  (max-width: 768px){
	.m-list__content{
		width: 100% !important;
	}	
}

</style>
<div class="m-list__content" style="">
    <div class="m-list-badge" style="font-size: 12px; text-align:center; margin: auto;line-height: 30px;font-weight: 500;">
    	<span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ccc;">3</span>&nbsp;CONFIRMADO PELO CLIENTE&nbsp;</span>
    	<span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ccc;">4</span>&nbsp;LIBERADO PARA PRODUÇÃO&nbsp;</span>
    	<span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ccc;">5</span>&nbsp;OPS GERADAS&nbsp;</span>
		<span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ffcc00;">6</span>&nbsp;EM PRODUÇÃO&nbsp;</span>
		<span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ffcc00;">7</span>&nbsp;LIBERADO PARA ADESIVAÇÃO&nbsp;</span>
		<span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ffcc00;">8</span>&nbsp;LIBERADO PARA INSPEÇÃO DE QUALIDADE&nbsp;</span>
		<span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ffcc00;">9</span>&nbsp;EM REVISÃO - C1&nbsp;</span>
		<span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ffcc00;">9</span>&nbsp;LIBERADO PARA LACRAÇÃO&nbsp;</span>
		<span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ffcc00;">10</span>&nbsp;LIBERADO PARA INSPEÇÃO FINAL&nbsp;</span><br/>
	    <span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ffcc00;">11</span>&nbsp;PRODUÇÃO CONCLUÍDA E LIBERADO PARA EMBALAGEM&nbsp;</span>
	    <span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ffcc00;">11</span>&nbsp;EM REVISÃO C2&nbsp;</span>
	    <span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #00ca00;">12</span>&nbsp;PEDIDO DISPONÍVEL PARA CARREGAMENTO/FINALIZADO&nbsp;</span>
	    <span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #00ca00;">13</span>&nbsp;AGUARDANDO CARREGAMENTO&nbsp;</span>
	    <span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #00ca00;">14</span>&nbsp;PEDIDO EM TRANSPORTE&nbsp;</span>
	    <span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #00ca00;">15</span>&nbsp;PEDIDO ENTREGUE&nbsp;</span>
	    <span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #00ca00;">16</span>&nbsp;EM FUNCIONAMENTO&nbsp;</span>
	    <input type="hidden" id="usuario_id" value="<?=$usuario_id?>" />
	    <input type="hidden" id="semana_atual" value="<?=$regras['data_abertura']?>">
	</div>
</div>
<div style="margin: 0 auto;text-align: center;">
	<table style="width: 100%;margin-top: 50px; ">
		<thead>
			<th style="padding-bottom: 20px;width: 5%;">Semana</th>
			<th style="padding-bottom: 20px;width: 10%;">&nbsp;</th>
			<th style="padding-bottom: 20px;width: 85%;">Pedidos</th>
		</thead>
		<tbody style="margin-top: 10px;">
			
			
		<?php $td_id=0;  $aux = 0;  foreach($dados as $key=> $dados){
			$chave 	= 	explode('*', $key);
			$data 	= 	explode('-', $chave[0]);
			$td_id++;
			$ano = $data[0];
			if($aux != $ano){ ?>
				<tr>
					<td colspan="3" class="linha_ano" ano="<?=$data[0]?>"><?=$data[0]?>  <i class="fa fa-arrow-down" ano="<?=$data[0]?>"></i> </td>
				</tr>
			<?php $aux = $ano; } ?>			
			<tr class="linha_semana" ano="<?=$data[0]?>">			
				<td style="border-top: 1px solid #ffcc00;border-bottom: 1px solid #ffcc00;padding: 20px;height: 150px;">
					<b style="font-size: 35px;color: #ffcc00;"><?=date('W',strtotime($chave[0]))?></b><br/>
					<b style="font-size: 20px;"><?=$data[2].'/'.$data[1].'/'.$data[0]?></b>					
				</td>
				<td style="border-top: 1px solid #ffcc00;border-bottom: 1px solid #ffcc00;padding: 20px;height: 150px;">
					<img src="<?php echo base_url('bootstrap/img/bomba.png');?>" style="margin-bottom: 15px;" title="Total de Bombas"> <b><?=$chave[2];?></b><br/>
					<img src="<?php echo base_url('bootstrap/img/bico.png');?>" style="margin-bottom: 15px;" title="Total de Bicos"> <b><?=$chave[1];?></b><br/>
				</td>				
				<td style="border-top: 1px solid #ffcc00;border-bottom: 1px solid #ffcc00;line-height: 3em;" class="area_pedidos" semana="<?=$chave[0];?>" bicos="<?=$chave[1];?>" bombas="<?=$chave[2];?>" total_bombas="<?=$regras['nr_bombas_semana']; ?>" total_bicos="<?=$regras['nr_bicos_semana']?>" id="<?=$td_id;?>">
					<?php foreach($dados as $dado){	
						switch ($dado['status_pedido_id']) {
							case 11:
								$value = '1';
								$bg = '#ccc';
								$color = "#000";
								break;
							case 3:
								$value = '2';
								$bg = '#ffcc00';
								$color = "#000";
								break;
							case 4:
								$value = '3';
								$bg = '#ffcc00';
								$color = "#000";
								break;
							case 2:
								$value = '4';
								$bg = '#ccc';
								$color = "#000";
								break;
							case 16:
								$value = '5';
								$bg = '#ccc';
								$color = "#000";
								break;
							case 12:
								$value = '4';
								$bg = '#ffcc00';
								$color = "#000";
								break;
							case 19:
								$value = '4';
								$bg = '#ffcc00';
								$color = "#000";
								break;
							case 13:
								$value = '5';
								$bg = '#ffcc00';
								$color = "#000";
								break;
							case 14:
								$value = '6';
								$bg = '#ffcc00';
								$color = "#000";
								break;
							case 20:
								$value = '6';
								$bg = '#ffcc00';
								$color = "#000";
								break;
							case 15:
								$value = '7';
								$bg = '#00ca00';
								$color = "#fff";
								break;
							case 5:
								$value = '8';
								$bg = '#00ca00';
								$color = "#fff";
								break;
							case 6:
								$value = '9';
								$bg = '#00ca00';
								$color = "#fff";
								break;
							case 8:
								$value = '10';
								$bg = '#00ca00';
								$color = "#fff";
								break;
							case 18:
								$value = '8';
								$bg = '#ffcc00';
								$color = "#fff";
								break;	
							case 18:
								$value = '8';
								$bg = '#ffcc00';
								$color = "#fff";
								break;						
							case 7:
								$value = '0';
								$bg = '#c44127';
								break;	
						}

						?>
						<span class="pedidos" style="background: #ffffff;padding: 5px;border-radius: 10px 10px 10px 10px;margin-right: 2px;cursor: pointer;width: 215px;height: 95px; font-size: 12px; border: 1px solid #ffcc00;" bicos="<?php echo $dado['bicos'];?>" bombas="<?php echo $dado['bombas']; ?>" pedido_id="<?=$dado['pedido_id']?>" programacao_semanal_id="<?=$dado['id']?>" title="<?=$dado['razao_social']?>" semana="<?=$chave[0];?>" >
							<span class="m-badge m-badge" title="" style="<?php echo 'background:'.$bg; ?>"><?php echo ($dado['ordem'] == 99) ? 'X' : $dado['ordem'];?></span>
							<i class="la primeiro" style="font-size: 16px !important;font-weight: 600;"></i> <span class="texto"><?php echo $dado['pedido_id'].'-'.substr($dado['razao_social'],0,12).'...';?></span> 
							<i class="la la-info-circle" style="font-size: 16px !important;font-weight: 600;" title="Informações"></i>&nbsp;
							<?php if( $usuario_id == '2' || $usuario_id == '12' || $usuario_id == '134'){ ?>
							<i class="la la-cut" style="font-size: 16px !important;font-weight: 600;" title="Dividir"></i>
							<?php } ?>
						</span>
					<?php }	?>
				</td>
			</tr>
		<?php  } ?>
		</tbody>	
	</table>
</div>
<div class="modal fade" id="info-pedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="titulo"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="m-portlet__body">
						<div class="row" style="border-bottom: 1px solid #ffcc00;line-height: 3em;padding-bottom: 12px;margin-top: 10px;">							
							<div class="col-lg-6 col-xs-6">
								<h5>Cliente: 
									<small class="text-muted" id="cliente"></small>
								</h5>
							</div>
							<div class="col-lg-6 col-xs-6">
								<h5>Cidade/Estado: 
									<small class="text-muted" id="cidade"></small>
								</h5>
							</div>														
						</div>						
						<div class="row" style="border-bottom: 1px solid #ffcc00;line-height: 3em;padding-bottom: 12px;margin-top: 18px;">
							<div class="col-lg-12 col-xs-12">
								<h5>Modelos Contidos no Pedido:</h5>
								<span  id="modelos"></span>
							</div>		
						</div>
						<div class="row" style="border-bottom: 1px solid #ffcc00;line-height: 3em;padding-bottom: 12px;margin-top: 18px;">
							<div class="col-lg-6 col-xs-12">
								<h5>Nr. Bicos: 
									<small class="text-muted" id="bicos"></small>
								</h5>
							</div>
							<div class="col-lg-6 col-xs-12">
								<h5>Nr. Bombas: 
									<small class="text-muted" id="bombas"></small>
								</h5>
							</div>								
														
						</div>						
																								
						<div class="row" style="margin-top: 18px;">
							<div class="col-lg-6 col-xs-6">
								<h5>
									<input type="text" class="form-control" name="dt_programacao" id="dt_programacao" style="width: 291px;" placeholder="Alterar Data Semana Inexistente" />
								</h5>
							</div>
							<div class="col-lg-6 col-xs-6">
								<h5>Status do Pedido na Produção</h5>	
								<button class="btn btn-success" id="visualizar_status" pedido_id="">Visualizar</button>
							</div>							
						</div>	
						<div class="row" style="margin-top: 18px;">
							<div class="col-lg-12 col-xs-12">
								<textarea class="form-control" id="observacao_pedido" placeholder="Insira uma observação para esse pedido"></textarea>
							</div>
						</div>		
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_dt" class="btn btn-primary" programacao_semanal_id="" indicacao="0">Alterar Situação</button>
					<button type="button" id ="excluir_pedido" class="btn btn-warning" pedido_id="" indicacao="0" style="display: none;">Excluir pedido Cancelado</button>
				</div>
			</div>
		</div>
	</div>
<div class="modal fade" id="dividir-pedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-status-title" id="titulo_divisao"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="m-portlet__body">										
					<div class="row" style="border-bottom: 1px solid #ffcc00;line-height: 3em;padding-bottom: 12px;">
						<div class="col-lg-6 col-xs-12">
							<h5>Nr. Bicos: 
								<small class="text-muted" id="bicos_divisao"></small>
							</h5>
						</div>
						<div class="col-lg-6 col-xs-12">
							<h5>Nr. Bombas: 
								<small class="text-muted" id="bombas_divisao"></small>
							</h5>
						</div>																	
					</div>																									
					<div class="row" style="margin-top: 18px;">
						<div class="col-lg-6 col-xs-6">
							<div class="" style="display: inline;">
								<h5 style="font-size: 16px;">Divisão 1: <i class="la la-plus" id="add_div_1" style="    float: right;font-size: 22px;color: #ffcc00;" indice="1"></i>
								</h5>
								<span id="copiar_1">
									<select name="" class="form-control modelo_1" style="width: 50%;float: left;margin-right: 10px;" indice="1"></select>
									<input type="number" class="form-control qtd_1" name="qtd" style="width: 33%;float: left;" placeholder="Qtd" indice="1" />
									<i class="la la-minus" style="display: none;padding-top: 10px;width: 10px;float: right;margin-right: 35px;" indice="1"></i>
								</span>
								<span id="colar_1"></span>
							</div>
							
							<div class="" style="display: inline;">
								<h5>
									<input type="text" class="form-control" name="dt_semana_nova" id="dt_semana_nova1" style="width: 291px; margin-top: 25px;" placeholder="Dt. Semana 1º Divisão" />
								</h5>
							</div>
						</div>
						<div class="col-lg-6 col-xs-6">
							<div class="" style="display: inline;">
								<h5 style="font-size: 16px;">Divisão 2: <i class="la la-plus" id="add_div_2" style="    float: right;font-size: 22px;color: #ffcc00;" indice="1"></i>
								</h5>
								<span id="copiar_2">
									<select name="" class="form-control modelo_2" style="width: 50%;float: left;margin-right: 10px;" indice="1"></select>
									<input type="number" class="form-control qtd_2" name="qtd2" id="qtd_2" style="width: 33%;float: left;" placeholder="Qtd" indice="1" />
									<i class="la la-minus" style="display: none;padding-top: 10px;width: 10px;float: right;margin-right: 35px;" indice="1"></i>
								</span>
								<span id="colar_2"></span>
							</div>
							
							<div class="" style="display: inline;">
								<h5>
									<input type="text" class="form-control" name="dt_semana_nova" id="dt_semana_nova2" style="width: 291px; margin-top: 25px;" placeholder="Dt. Semana 2º Divisão" />
								</h5>
							</div>
						</div>
					</div>		
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" id ="dividir_pedido" class="btn btn-primary" programacao_semanal_id="" pedido_id="" bicos="" bombas="" dt_semana_pedido="">Dividir Pedido</button>
			</div>
		</div>
	</div>
</div>
<!-- Status Andamento do pedido na produção -->
<div class="modal fade" id="status-pedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-status-title" >Status Nr. de Série</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="m-portlet__body">										
					<div class="row" style="border-bottom: 1px solid #ffcc00;line-height: 3em;padding-bottom: 12px;">
						<div class="form-group m-form__group row">
							<div class="col-lg-4">
								<label>Bomba (Nº Série):</label>
								<div class="m-input-icon m-input-icon--right">
									<select class="form-control" id="nr_serie_id">										
									</select>
									<input type="hidden" name="pedido_id" id="pedido_id" class="form-control m-input" placeholder="" required >
									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
									</span>
								</div>
							</div>
							<div class="col-lg-4">
								<label>Status Produção</label>
								<div class="m-input-icon m-input-icon--right">
									<select class="form-control" id="status_producao_id">										
									</select>									
									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
									</span>
								</div>
							</div>
							<div class="col-lg-4">
								<label>Observação</label>
								<div class="m-input-icon m-input-icon--right">
									<textarea class="form-control" id="observacao"></textarea>								
									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-info-circle"></i></span>
									</span>
								</div>
							</div>	
						</div>	
						<div class="form-group m-form__group row">
							<div class="col-lg-4" style="float: right;">
								<button id="enviar_status" class="btn btn-success">Alterar Status</button>
							</div>
						</div>														
					</div>																									
					<div class="row" style="margin-top: 18px;">
						<table class="table">
							<thead>
								<th>Nº Série</th>
								<th>Status</th>
								<th>Observação</th>
								<th>Dthr. Modificação</th>
							</thead>
							<tbody id="conteudo_status">
								
							</tbody>
						</table>		
					</div>		
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				
			</div>
		</div>
	</div>
</div>