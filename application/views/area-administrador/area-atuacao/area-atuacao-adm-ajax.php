
<table class="" id="html_table" width="100%">
<thead>
	<tr>
		
		<th style="width: 5%;">#</th>
		<th style="width: 45%;">Cidade</th>
		<th style="width: 15%;">Estado</th>
		<th style="width: 30%;">Código</th>
		<th style="width: 5%;">Ações</th>
	</tr>
</thead>
<tbody>					
	<?php foreach($dados as $dado){	?>
		<tr>			
			<td style="text-align: center;"><?php echo $dado['id']; ?></td>
			<td style=""><?php echo $dado['cidade']; ?></td>											
			<td style="text-transform: lowercase;"><?php echo mb_strtolower($dado['estado']); ?></td>
			<td style=""><?php echo mb_strtoupper($dado['cidade_cod_ibge']); ?></td>
			<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
				<div class="m-radio-inline">
					<label class="m-radio">
						<input type="radio" name="ativo<?php echo $dado['id'];?>" onclick="atualizarStatus('1',<?php echo $dado['id'];?>);" class="ativo" <?php if($dado['fl_ativo']=='1') echo 'checked="checked"' ?> value="1" > Sim
						<span></span>
					</label>									
					<label class="m-radio">
						<input type="radio" name="ativo<?php echo $dado['id'];?>" class="ativo" <?php if($dado['fl_ativo']=='0') echo 'checked="checked"' ?> value="0"  onclick="atualizarStatus('0',<?php echo $dado['id'];?>);" > Não
						<span></span>
					</label>								
				</div>
			</td>
		</tr>
	<?php } ?> 
	</tbody>
</table>

<script type="text/javascript">	

	$(document).ready(function(){
		$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
		table.on( 'draw.dt', function () {
			if( $('#todos').is(':checked') ){
				$('.check').attr('checked', true); 
			}
		});

		$('#todos').bind('click', function(){
			if( $(this).is(':checked') ){
				$('input[type="checkbox"]').attr('checked',true);

			}else{
				$('input[type="checkbox"]').attr('checked',false);
				
			}

		});

		var erro = 0;
		$('#aprovar_todos').bind('click', function(){
			erro = 0;		
			
			table.columns(0).every( function () {
			    var data = this.data();
			   	
			    for(i = 0; i< data.length;i++){
			    	var area_id = data[i];
			    	$.ajax({
						method: "POST",
						url: base_url+"AreaAdministrador/atualizaAreaAtuacao",
						async: false,
						success: function() {
					    	$('#loading').show();// Note the ,e that I added
						},
						data: { id 			: 	area_id,
								fl_ativo 	: 	1	}
					}).done(function( data ) {
						var dados = $.parseJSON(data);
						if(dados.retorno == 'sucesso'){
							erro = 1;
						}else{
							erro = 0;
						}						
					});
				}
				$.unblockUI();
			});

			if( erro == 1 ){

				swal({
		   			title:"Sucesso!",
		   			text: "Áreas atualizadas com sucesso!",
		   			type: 'success'
		    	}).then(function() {
		    	 	var empresa_id = $('#empresa_id').val();
				    $.ajax({
						method: "POST",
						url: base_url+"AreaAdministrador/areaAtuacaoAdmAjax",
						async: false,
						data: { empresa_id : empresa_id	}
					}).done(function( data ) {
						var dados = $.parseJSON(data);
						$('#relatorio').empty();
						$('#relatorio').append(dados.retorno);
						datatable();

					});
		    	}); 

			}
		});

		
		$('#desaprovar_todos').bind('click', function(){
			erro = 0;
			$('#loading').show();
			table.columns(0).every(	function()	{
			    var data = this.data();

			    for(i = 0; i < data.length;i++){
			    	var area_id = data[i];
			    	$.ajax({
						method: "POST",
						url: base_url+"AreaAdministrador/atualizaAreaAtuacao",
						async: false,
						data: { id 			: area_id,
								fl_ativo 	: 0	}
					}).done(function( data ) {
						var dados = $.parseJSON(data);
						if(dados.retorno == 'sucesso'){
							erro = 1;
						}else{
							erro = 0;
						}		
					});
				}				
			});

			if( erro == 1 ){
				swal({
		   			title:"Sucesso!",
		   			text: "Áreas atualizadas com sucesso!",
		   			type: 'success'
		    	}).then(function() {
		    	 	var empresa_id = $('#empresa_id').val();
				    $.ajax({
						method: "POST",
						url: base_url+"AreaAdministrador/areaAtuacaoAdmAjax",
						async: false,
						data: { empresa_id : empresa_id	}
					}).done(function( data ) {
						var dados = $.parseJSON(data);
						$('#relatorio').empty();
						$('#relatorio').append(dados.retorno);
						datatable();
					});
		    	});
			}
		});
	});
	function atualizarStatus(status,id){
		$.ajax({
			method: "POST",
			url: base_url+"AreaAdministrador/atualizaAreaAtuacao",
			async: false,
			data: { id 			: id,
					fl_ativo 	: status	}
		}).done(function( data ) {
			var dados = $.parseJSON(data);
			if(dados.retorno == 'sucesso'){
				swal({
		   			title:"Sucesso!",
		   			text: "Áreas atualizadas com sucesso!",
		   			type: 'success'
		    	}).then(function() {

		    	});
			}	
		});
	}
</script>