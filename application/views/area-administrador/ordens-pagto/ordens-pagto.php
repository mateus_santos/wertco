<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head" >
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text" style="padding-top: 30px;">
						Ordens de pagamento &nbsp;&nbsp;&nbsp;
					</h3>
					<div class="m-portlet__head-caption cadastrar_orcamento">				
						<a href="<?php echo base_url('AreaAdministrador/cadastraPedidos')?>" class="" style="color: #ffcc00; font-weight: bold;" >
							<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo pedido"></i>
						</a>
					</div>
				</div>				
			</div>				
			<div class="m-list__content" style="width: 75%; margin: 0 auto;">
                <div class="m-list-badge" style="margin-top: 25px;">
				</div>
			</div>		
			<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: -25px;" data-toggle="m-tooltip" data-placement="top" title="Tour Virtual" id="tour_virtual">
				<i class="la la-info"></i>
			</a>
		
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;"># Op</th>
						<th style="text-align: center;">Descrição</th>
						<th style="text-align: center;"># Chamado</th>
						<th style="text-align: center;">Cliente</th>											
						<th style="text-align: center;">Técnico Favorecido</th>						
						<th style="text-align: center;">Valor</th>						
						<th style="text-align: center;">Dthr Geração</th>
						<th style="text-align: center;">Status</th>
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				<tbody>					
					<?php foreach($dados as $dado){
						$status = "btn m-btn--pill m-btn--air btn-success";
						$background = '';
						if( $dado['status'] == 'EMITIDA' ){
							$status = "btn m-btn--pill m-btn--air btn-secondary";
							$background = '';
						}

						?>
						<tr>
							<td style="width: 5%;text-align: center;"><?php echo $dado['id']; ?></td>
							<td style="width: 5%;text-align: center;"> <?php echo $dado['descricao']; ?></td>
							<td style="width: 10%;text-align: center;"><?php echo $dado['chamado_id']; ?></td>										
							<td style="width: 25%;text-align: center;"><?php echo $dado['cliente']; ?></td>													
							<td style="width: 25%;text-align: center;"><?php echo $dado['favorecido']; ?></td>													
							<td style="width: 10%;text-align: center;"><?php echo $dado['valor']; ?></td>		
							<td style="width: 10%;text-align: center;"><?php echo date('d/m/Y H:i:s',strtotime($dado['dt_emissao'])); ?></td>
							<td style="width: 10%;text-align: center; text-transform: capitalize;">
								<button class="status <?php echo $status; ?>" <?php echo $background;?> title="Status da op">
									<?php echo ucfirst($dado['status']); ?>								
								</button>
							</td>
							<td data-field="Actions" class="m-datatable__cell " style="width: 20%;text-align: center !important;">
								<span style="overflow: visible; width: 110px;" class="">								
									<a class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" onclick="visualizar(<?=$dado['id']?>)" title="Visualizar itens da op">
										<i class="la la-eye "></i>
									</a>
									<a  class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" target="blank"  title="Anexar Nf e Comprovante pagamento" onclick="anexar(<?=$dado['id']?> , '<?=$dado['comprovante']?>',  '<?=$dado['nota_fiscal']?>','<?=$dado['nr_nf']?>','<?=$dado['cliente']?>','<?=$dado['favorecido']?>','<?=$dado['valor']?>','<?=$dado['descricao']?>',<?=$dado['chamado_id']?>)" >
										<i class="flaticon-tool-1"></i>
									</a>									
								</span>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			************************************************************************************** -->

	<div class="modal fade" id="anexarNfe" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/anexarNfeOp');?>" method="post"  enctype="multipart/form-data">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title modal-anexar-nfe-title" ></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" style="width: 750px;">					
						<div class="row">
							<div class="form-group m-form__group col-lg-6 col-xs-12">						
								<label for="exampleSelect1">Nota fiscal</label>
								<input type="text" class="form-control m-input--air" name="nr_nf" id="nr_nf" placeholder="Número" required="required" />
								<input type="hidden" class="form-control m-input--air" name="op_id" id="op_id" required="required" />
								<input type="hidden" class="form-control m-input--air" name="cliente" id="cliente" required="required" />
								<input type="hidden" class="form-control m-input--air" name="chamado_id" id="chamado_id" required="required" />
								<input type="hidden" class="form-control m-input--air" name="favorecido" id="favorecido" required="required" />
								<input type="hidden" class="form-control m-input--air" name="valor" id="valor" required="required" />
								<input type="file" class="form-control m-input--air" name="nota_fiscal" id="nota_fiscal" placeholder="Anexar Nfe" />
							</div>	
							<div class="form-group m-form__group col-lg-6 col-xs-12">						
								<label for="exampleSelect1">Comprovante</label>
								<input type="file" class="form-control m-input--air" name="comprovante" id="comprovante" placeholder="Comprovante" />
							</div>									
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
						<button type="submit" id="enviar_nf" class="btn btn-primary" op_id="">Enviar</button>
					</div>
				</div>			
			</div>
		</form>
	</div>

	<div class="modal fade" id="visualizarOp" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/anexarNfeOp');?>" method="post"  enctype="multipart/form-data">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title modal-title" >Itens da Op</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body"  id="conteudo_itens">

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
						<button type="submit" id="enviar_nf" class="btn btn-primary" op_id="">Enviar</button>
					</div>
				</div>			
			</div>
		</form>
	</div>
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>						 
		<script type="text/javascript">			
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaAdministrador/ordensPagto';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>