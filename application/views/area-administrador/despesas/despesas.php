<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Despesas
					</h3>
				</div>
			</div>
			<div class="col-md-4 m-portlet__head-caption">													
				<a href="<?php echo base_url('AreaAdministrador/cadastraDespesa')?>" class="" style="color: #ffcc00; font-weight: bold;">
					<i class="la la-plus-circle" style="font-size: 38px;"></i>
				</a>
			</div>					
		</div>
		<div class="m-portlet__body">
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="id" style="width: 10%;">ID</th>
						<th title="tipo" style="width: 20%">Tipo</th>
						<th title="descricao" style="width: 40%">Descrição</th>
						<th title="valor" style="width: 25%">Valor(R$)</th>						
						<th title="" style="width: 25%">Ações</th>
					</tr>
				</thead>
				<tbody>
					<tr>
					<?php foreach($dados as $dado){ ?>
						<td style="width: 5%;text-align: center;"><?php echo $dado['id']; ?></td>
						<td><?php echo $dado['tipo']; ?></td>
						<td><?php echo $dado['descricao']; ?></td>
						<td class="moeda"><?php echo str_replace('.', ',', $dado['valor']); ?></td>						
						<td data-field="Actions" class="m-datatable__cell">
							<span style="overflow: visible; width: 110px;">								
								<a href="<?php echo base_url('AreaAdministrador/editarDespesa/'.$dado['id']); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar" despesas_id="<?php echo $dado['id']; ?>" >
									<i class="la la-edit"></i>
								</a>
								<button id="excluir" onclick="excluirDespesa(<?php echo $dado['id']; ?>);" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" despesas_id="<?php echo $dado['id']; ?>">
									<i class="la la-trash"></i>
								</button>
							</span>
						</td>
					</tr>
						<?php } ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>		        
</div>				
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
            title: "OK!",
            text: 'Cadastro realizado com sucesso!',
            type: "success"
        }).then(function() {
		 	window.location = base_url+'AreaAdministrador/Despesas';
		}); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>