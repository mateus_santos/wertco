
<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/icms'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/icms'); ?>">Voltar</a>						
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarDespesa/'.$dados['id']);?>" method="post">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Tipo:</label>
							<div class="m-input-icon m-input-icon--right">								
								<select class="form-control m-input" name="tipo_despesa_id" required >
									<option value="">Selecione um tipo de despesa</option>
									<?php foreach( $tipo_despesas as $tipo_despesa ){?>
									<option value="<?php echo $tipo_despesa['id']; ?>" <?php if( $dados['tipo_despesa_id'] == $tipo_despesa['id'] ) { echo 'selected="selected"';}?> ><?php echo $tipo_despesa['descricao'];?></option>
									<?php } ?>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map"></i></span>
								</span>
							</div>
						</div>
						<div class="col-lg-4">
							<label>Descrição:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="descricao" id="descricao" class="form-control m-input" placeholder="" required style="text-align: right;" value="<?php echo $dados['descricao'];?>" />
								<input type="hidden" required name="id" id="id" class="form-control m-input" placeholder="" required style="text-align: right;" value="<?php echo $dados['id'];?>" />
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Valor:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="valor" id="valor" class="form-control m-input" placeholder="" value="<?php echo $dados['valor']; ?>" required style="text-align: right;">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-dollar"></i></span></span>
							</div>							
						</div>
					</div>				
		            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
									<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
								</div>							
							</div>
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	