<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-diagram" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Relatório de Previsão de Vendas Detalhado
					</h3>					
				</div>
			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >					
			<div class="form-group m-form__group row">
				<div class="col-lg-4">
					<label class="">Modelo:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="produto_id" placeholder="Status Chamado" id="produto_id" >
                        	<option value="">--- TODOS MODELOS DE BOMBA ---</option>
                        <?php 	foreach($modelos as $modelo ){	?>       
                        	<option value="<?php echo $modelo->id; ?>"><?php echo mb_strtoupper($modelo->modelo); ?></option>
                        <?php } ?>
                       	</select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
					</div>							
				</div>	
				<div class="col-lg-8" style="border: 1px solid #ffcc00;text-align: center;">
					<h3 style="color: #ffcc00;">Regras para gerar o relatório de previsão de vendas:</h3>
					<h5 style="font-weight: 600; ">- Todos os orçamentos com status em 'fechado' e que não geraram pedidos;</h5>
					<h5 style="font-weight: 600; ">- Todos os pedidos com status em 'aberto'.</h5>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button  name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">
							Buscar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>
					</div>
				</div>
			</div>
		</div>								
	</div>
	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" style="display: none;">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
							<i class="flaticon-statistics"></i>
						</span>
						<h3 class="m-portlet__head-text">						
						</h3>
						<h2 class="m-portlet__head-label m-portlet__head-label--warning">
							<span>
								Previsões
							</span>
						</h2>
					</div>
				</div>
				<div class="m-portlet__head-tools">
				</div>
			</div>
			<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" style="padding-bottom: 150px;">
				<!--begin: Datatable -->			
			</div>
		</div>	
</div>	
<div class="modal fade" id="m_andamentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-lg" role="document">
		<div class="modal-content" style="min-width: 850px;">
			<div class="modal-header">
				<h5 class="modal-title m_andamento_titulo" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 100%;">					
				<div class="col-lg-12 andamento"></div>			
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>