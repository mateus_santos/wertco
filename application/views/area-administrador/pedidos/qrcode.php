<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head" >
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text" style="padding-top: 30px;">
						QrCode - Nr. de série <?=$content?> &nbsp;&nbsp;&nbsp;
					</h3>
					<div class="m-portlet__head-caption cadastrar_orcamento">				
						
					</div>
				</div>				
			</div>				
			<div class="m-list__content" style="width: 75%; margin: 0 auto;">
                <div class="m-list-badge" style="margin-top: 25px;">
				</div>
			</div>						
			
		</div>

		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >					
			<img src="<?=base_url('qrcodes/'.$file)?>">	
		</div>
	</div>
</div>