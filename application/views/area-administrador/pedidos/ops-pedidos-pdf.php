<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 9px;
 	color: #333;
 	width: 100%;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px;
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{
	padding: 2px !important;
	border-bottom: 1px solid #f0f0f0;
	border-right: 1px solid #f0f0f0;
	font-size: 10px;	
	text-transform: uppercase;
}


</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
										07400-230 - Arujá - SP<br>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br>
			</p>
		</div>
		<div class="corpo">
            <table border="0">
				<tr>
					<td style="text-align: center;"><h2>Ordens de Produção Para o Pedido <b><?php echo $ops[0]['pedido_id']; ?></b></h2></td>		
				</tr>
			</table>
			<table border="0">
				<tr>
					<td><h3 style="text-align: left;margin-top: 10px;	">Cliente: <?php echo $ops[0]['nome_cliente']; ?></h3></td>	
					<td><h3 style="text-align: left;margin-top: 10px;	">Cidade: <?php echo $ops[0]['cidade'].'/'.$ops[0]['uf']; ?></h3></td>	
				</tr>
			</table>
			<?php $i=1; foreach ($ops as $op){ ?>
			<h2>Op #<?php echo $i;?></h2>	
			<table cellspacing="0" style="margin-top: 5px; margin-bottom: 15px;">
				<thead>
					<tr>
						<th style="text-align: center;">Modelo</th>
						<th style="text-align: center;">Código</th>
						<th style="text-align: center;">Qtd</th>					
					</tr>
				</thead>
				<tbody>							
					<tr>
						<td style="text-align: center;"> <?php echo $op['modelo']; ?></td>
						<td style="text-align: center;"> <?php echo $op['codigo']; ?></td>
						<td style="text-align: center;"> <?php echo $op['qtd']; ?></td>						
					</tr>
					<?php $infos = explode('|#|', $op['observacao']); 
					foreach($infos as $info){ 
						if( $info != '' ){
						?>
					<tr>
						<td colspan="3"><?php echo $info; ?></td>
					</tr>
					<?php } } ?>
					<tr>
						<td colspan="3" style="text-align: center;"> <b><?php echo $op['informacoes']; ?></b> </td>
					</tr>
								
				</tbody>							
			</table>
			<hr/>
		<?php $i++;} ?>	
	</div>
</div>
</body>
				
		