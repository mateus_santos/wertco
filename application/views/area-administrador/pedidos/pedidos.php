<input type="hidden" id="tipo_acesso" value="<?=$tipo_acesso?>">
<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head" >
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text" style="padding-top: 30px;">
						Pedidos &nbsp;&nbsp;&nbsp;
					</h3>
					<div class="m-portlet__head-caption cadastrar_orcamento">				
						<a href="<?php echo base_url('AreaAdministrador/cadastraPedidos')?>" class="" style="color: #ffcc00; font-weight: bold;" >
							<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo pedido"></i>
						</a>
					</div>
				</div>				
			</div>				
			<div class="m-list__content" style="width: 75%; margin: 0 auto;">
                <div class="m-list-badge" style="margin-top: 25px;"><!--
                	<span class="m-list-badge__item">&nbsp;Pedido Gerado&nbsp;&nbsp;<span class="m-badge m-badge--secondary"></span></span>				    	
					<span class="m-list-badge__item">&nbsp;Pedido Confirmado Pelo Cliente&nbsp;&nbsp;<span class="m-badge m-badge--info"></span></span>
					<span class="m-list-badge__item">&nbsp;Pedido em produção&nbsp;&nbsp;<span class="m-badge m-badge--primary"></span></span>
					<span class="m-list-badge__item">&nbsp;Pedido concluído (produzido)&nbsp;&nbsp;<span class="m-badge m-badge" style="background: #ff9800;"></span></span>
					<span class="m-list-badge__item">&nbsp;Pedido em transporte&nbsp;&nbsp;<span class="m-badge m-badge" style="background: #18cfe6;"></span></span>
					<span class="m-list-badge__item">&nbsp;Pedido entregue&nbsp;&nbsp;<span class="m-badge m-badge--success" ></span></span>
				    <span class="m-list-badge__item">&nbsp;Cancelado&nbsp;&nbsp;<span class="m-badge m-badge--danger"></span></span>-->
				</div>
			</div>	
				
			<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: -10px;margin-right: 10px;" data-toggle="m-tooltip" data-placement="top" title="Tour Virtual" id="tour_virtual">
				<i class="la la-info"></i>
			</a>
			<a href="https://www.wertco.com.br/FluxoDePedidosWertcoRev06.pdf" class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: -10px;margin-right: 10px;" data-toggle="m-tooltip" data-placement="top" title="Fluxo processual dos pedidos" target="_blank" >
				<i class="la la-code-fork" style="color: #000;"></i>
			</a>
		</div>

		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >					
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;"># Pedido</th>
						<th style="text-align: center;"># Orçamento</th>
						<th style="text-align: center;">Cliente</th>											
						<th style="text-align: center;">Situação</th>						
						<th style="text-align: center;">Padrão de Pintura</th>						
						<th style="text-align: center;">Dthr Geração</th>
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				<tbody>					
					<?php foreach($dados as $dado){
						if( $dado->status_pedido_id == 1 ){
							$status = "btn m-btn--pill m-btn--air btn-secondary";
							$background = '';
						}elseif( $dado->status_pedido_id == 2 ){
							$status = "btn m-btn--pill m-btn--air btn-info";
							$background = '';
						}elseif( $dado->status_pedido_id == 3 ){
							$status = "btn m-btn--pill m-btn--air btn-primary";
							$background = '';
						}elseif( $dado->status_pedido_id == 4 ){
							$status = "btn m-btn--pill m-btn--air";
							$background = "style='background: #ff9800;'";
						}elseif( $dado->status_pedido_id == 5 ){
							$status = "btn m-btn--pill m-btn--air";
							$background = "style='background: #18cfe6;'";
						}elseif( $dado->status_pedido_id == 6){
							$status = "btn m-btn--pill m-btn--air btn-success";
							$background = '';
						}elseif($dado->status_pedido_id == 7){
							$status = "btn m-btn--pill m-btn--air btn-danger";
							$background = '';
						}else{
							$status = "btn m-btn--pill m-btn--air btn-success";
							$background = '';
						}

						?>
						<tr>
							<td style="width: 5%;text-align: center;"><?php echo $dado->id; ?></td>
							<td style="width: 5%;text-align: center;"> <?php echo $dado->orcamento_id; ?></td>
							<td style="width: 40%;text-align: center;"><?php echo $dado->cliente; ?></td>		
							<td style="width: 10%;text-align: center; text-transform: capitalize;">
								<button class="status <?php echo $status; ?>" status="<?php echo $dado->status_pedido_id;?>" <?php echo $background;?> title="Status do orçamento" onclick="status(<?php echo $dado->id; ?>, '<?=$dado->cliente;?>', <?php echo $dado->status_pedido_id;?>, '<?=$tipo_acesso?>',<?=$setor_id?>, <?php echo $dado->setor_id; ?>, <?php echo $dado->ordem; ?>, <?=$usuario_id?>, '<?=$dado->fl_identidade_visual?>','<?=$dado->pintura?>');">
									<?php echo ucfirst($dado->status); ?>								
								</button>
							</td>	
							<td style="width: 10%;text-align: center;">
								<?php echo $dado->pintura; ?>							
							</td>													
							<td style="width: 10%;text-align: center;">
								<?php echo date('d/m/Y H:i:s',strtotime($dado->dthr_geracao)); ?>							
							</td>							
							<td data-field="Actions" class="m-datatable__cell " style="width: 20%;text-align: center !important;">
								<span style="overflow: visible; width: 110px;" class="">								
									<?php if($dado->status_pedido_id != 9 ){ ?>
									<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick="gerarPdf(<?php echo $dado->id; ?>);" pedido_id="<?php echo $dado->id; ?>" >
										<i class="la la-print visualizar"></i>
									</a>
									<?php } if(($tipo_acesso == 'administrador comercial'  || $tipo_acesso == 'administrador geral') && $dado->status_pedido_id == 1 || $dado->status_pedido_id == 9 || $dado->status_pedido_id == 17 || $dado->status_pedido_id == 2){
										?>
									
									<a href="<?php echo base_url('AreaAdministrador/editarPedidos/'.$dado->id); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" pedido_id="<?php echo $dado->id; ?>" >
										<i class="la la-edit editar"></i>
									</a> 
									<?php } if(($tipo_acesso == 'administrador comercial' || $tipo_acesso == 'administrador geral') && ($dado->status_pedido_id == 2 || $dado->status_pedido_id == 10)){ ?>

									<!--<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" pedido_id="<?php echo $dado->id; ?>" status="<?php echo $dado->status_pedido_id;?>" onclick="verificaOps(<?php echo $dado->status_pedido_id;?>,<?php echo $dado->id; ?>,'<?php echo base_url('AreaAdministrador/confirmaPedidoOps/'.$dado->id); ?>', '<?=$dado->cliente;?>', 'confirmar' );">
										<i class="fa fa-check gerar_ops" style="color: #ffcc00;" status="<?php echo $dado->status_pedido_id;?>" pedido_id="<?php echo $dado->id; ?>" caminho="<?php echo base_url('AreaAdministrador/confirmaPedidoOps/'.$dado->id); ?>" cliente="<?=$dado->cliente;?>"></i>
									</a>--> 

									<?php } if($tipo_acesso == 'administrador comercial' || $tipo_acesso == 'administrador geral'){ ?>
									<a href="#" onclick="finalizarPedido(<?php echo $dado->id; ?>,'<?php echo $dado->email; ?>');" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill finalizar_pedido" pedido_id="<?php echo $dado->id; ?>" >
										<i class="fa fa-envelope-open-o finalizar_pedido"></i>
									</a>
									<a href="<?php echo base_url('AreaAdministrador/excluirPedidos/'.$dado->id); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" pedido_id="<?php echo $dado->id; ?>" >
										<i class="fa fa-trash excluir"></i>
									</a> 
									<?php } if (  $dado->status_pedido_id != 1 && $dado->status_pedido_id != 7 && $dado->status_pedido_id != 9 && $dado->status_pedido_id != 10 ){ ?>
										<a href="<?php echo base_url('areaAdministrador/geraOpsPdf/'.$dado->id)?>"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" target="_blank" >
											<i class="la la-cogs"></i>
										</a>

									<?php } if ( 	$dado->status_pedido_id == 16 && ($tipo_acesso == 'administrador tecnico' || $tipo_acesso == 'administrador geral') 	){ ?>
										<!--<a href="<?php echo base_url('areaAdministrador/confirmaPedidoOps/'.$dado->id)?>"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" target="_blank" >
											<i class="fa fa-first-order"></i>
										</a>-->		 								
									
									<?php } if (  $dado->status_pedido_id != 1 && $dado->status_pedido_id != 7 ){ ?>
										<a href="#" onclick="anexarNfe(<?php echo $dado->id; ?>,'<?php echo str_replace("'","*",$dado->cliente); ?>','<?php echo $dado->nr_nf; ?>','<?php echo $dado->arquivo_nfe; ?>','<?php echo $dado->prazo_entrega; ?>','<?php echo $dado->dt_emissao_nf; ?>','<?php echo $dado->dt_previsao_entrega; ?>', '<?php echo $dado->transportadora_id; ?>' );" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill finalizar_pedido" pedido_id="<?php echo $dado->id; ?>" cliente="<?php echo $dado->cliente; ?>" >
											<i class="flaticon-tool-1"></i>
										</a>
										<a href="#" onclick="gerarPdfStartup(<?php echo $dado->id; ?>,<?php echo $dado->orcamento_id; ?>, <?php echo $dado->empresa_id; ?>);" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill finalizar_pedido" pedido_id="<?php echo $dado->id; ?>" cliente="<?php echo $dado->cliente; ?>" cliente_id="<?php echo $dado->empresa_id; ?>">
											<i class="la la-list-alt"></i>
										</a>
									<?php } ?>
									<?php if( $dado->status_pedido_id == 14 || $dado->status_pedido_id == 5 || $dado->status_pedido_id == 6 || $dado->status_pedido_id == 15 || $dado->status_pedido_id == 8) { ?>
										<a  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick="listarFotos(<?php echo $dado->id; ?>, '<?php echo $dado->cliente; ?>');" >
											<i class="la la-picture-o"></i>
										</a>
									<?php } ?>										
										<a  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick="listarPintura(<?php echo $dado->id; ?>, '<?php echo $dado->cliente; ?>', '<?php echo $dado->status_pedido_id; ?>',  '<?php echo $tipo_acesso; ?>' , '<?php echo $dado->pintura; ?>', '<?php echo $dado->fl_identidade_visual; ?>');" >
											<i class="fa fa-paint-brush"></i>
										</a>

										<?php if($dado->ordem >= 14) { ?>		
											<a href="<?=base_url('AreaAdministrador/geraGarantia/'.$dado->id)?>"  class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill"  data-placement="top" title="Gerar Certificado de Garantia">
												<i class="la la-certificate"></i>
											</a>			
										<?php } ?>
										<?php if($dado->ordem >=5 ) { ?>
											<a href="#" onclick="gerarAvisoDisp(<?php echo $dado->id; ?>,<?php echo $dado->orcamento_id; ?>, <?php echo $dado->empresa_id; ?>);" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill finalizar_pedido" pedido_id="<?php echo $dado->id; ?>" cliente="<?php echo $dado->cliente; ?>" cliente_id="<?php echo $dado->empresa_id; ?>">
												<i class="la la-warning"></i>
											</a>
										<?php } ?>
										<i class="fa fa-circle" style="cursor: pointer; color: <?=($dado->fl_inadimplencia == 1 ) ? 'red' : 'green'?>;" data-toggle="m-tooltip" data-placement="top" title="<?=($dado->fl_inadimplencia == 1 ) ? 'Cliente Inadimplente' : 'Cliente Adimplente'?>"></i>
										
								</span>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<hr style="border: 1px solid #ffcc00;width: 97%;margin-top: 20px;margin-bottom: 30px;" >
				<!--end: Datatable -->					
			</div>
		</div>
	</div>
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<?php /*
						$combo = '	<option value="">Selecione o Status do Pedido</option>						
									<option value="1">PEDIDO GERADO</option>
									<option value="2">PEDIDO CONFIRMADO PELO CLIENTE</option>							
									<option value="11">OPS GERADAS</option>
									<option value="3">PEDIDO EM PRODUÇÃO</option>
									<option value="4">LIBERADO PARA INSPEÇÃO DE QUALIDADE</option>
									<option value="12">LIBERADO PARA LACRAÇÃO</option>
									<option value="13">LIBERADO PARA INSPEÇÃO FINAL</option>									
									<option value="14">LIBERADO PARA EMBALAGEM</option>
									<option value="15">FINALIZADO</option>									
									<option value="5">PEDIDO EM TRANSPORTE</option>
									<option value="6">PEDIDO ENTREGUE</option>
									<option value="7">CANCELADO</option>
									<option value="8">EM FUNCIONAMENTO</option>';
						//Comercial			
						if( $tipo_acesso == 'administrador comercial' || $setor_id == 1){
							$combo = '	<option value="">Selecione o Status do Pedido</option>						
										<option value="1">PEDIDO GERADO</option>
										<option value="2">PEDIDO CONFIRMADO PELO CLIENTE</option>
										<option value="6">PEDIDO ENTREGUE</option>																	
										<option value="7">CANCELADO</option>';
						// Produção
						}elseif($tipo_acesso == 'administrador tecnico' || $setor_id == 2 ){
							$combo = '	<option value="3">PEDIDO EM PRODUÇÃO</option>
										<option value="4">LIBERADO PARA INSPEÇÃO DE QUALIDADE</option>	
										<option value="13">LIBERADO PARA INSPEÇÃO FINAL</option>									
										<option value="15">FINALIZADO</option>
										<option value="7">CANCELADO</option>
										<option value="8">EM FUNCIONAMENTO</option>';
						}elseif($tipo_acesso == 'qualidade' || $setor_id == 3){
							$combo = '	<option value="12">LIBERADO PARA LACRAÇÃO</option>
										<option value="4">PRODUTO NÃO APROVADO (RETORNA PARA PRODUÇÃO)</option>
										<option value="14">LIBERADO PARA EMBALAGEM</option>';
						}*/
					?>
					<div class="form-group m-form__group status_orcamento_" >
						<label for="exampleSelect1">Situação do Pedido</label>
						<select class="form-control m-input m-input--air" id="status_pedido">
							<?php //echo $combo; ?>
						</select>
					</div>					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" status_atual="" tipo_acesso="<?=$tipo_acesso?>" cliente="" class="btn btn-primary" pedido_id="" indicacao="0" fl_identidade_visual="" bandeira="">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>	

	<div class="modal fade" id="m_foto_pedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/uploadEmbalagem');?>" method="post"  enctype="multipart/form-data">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title fotos_pedido_titulo" ></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" style="width: 750px;">					
						<div id="fotos_pedido" class="row">
							
						</div>	
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>						
					</div>
				</div>
			</form>
		</div>		
	</div>

	<!-- Modal Motivo da Reprovação do produto -->
	<div class="modal fade" id="m_upload_foto_pedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/uploadEmbalagem');?>" method="post"  enctype="multipart/form-data">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title modal-finalizar-title" >Enviar Fotos Pedido</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" style="width: 750px;">					
						<div class="form-group m-form__group">
							<div class="form-group m-form__group" >
								<label for="exampleSelect1">Upload</label>
								<input type="hidden" name="pedido_id" id="pedido_id_embalagem" value="">
								<input type="file" class="form-control" name="arquivos[]" id="arquivos" accept="image/png, image/jpeg, image/jpg"  multiple /></input>
							</div>
						</div>	
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
						<button type="submit" id ="" class="btn btn-primary" pedido_id="" >Enviar</button>
					</div>
				</div>
			</form>
		</div>		
	</div>

	<!-- Modal Motivo da Reprovação do produto -->
	<div class="modal fade" id="m_retorno_status" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-finalizar-title" >Retorno de pedido</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" >
							<label for="exampleSelect1">Motivo para retorno do status do pedido</label>
							<textarea class="form-control" id="motivo_retorno"></textarea>
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="enviar_retorno" class="btn btn-primary" pedido_id="" ordem="">Enviar</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Motivo da Reprovação do produto -->
	<div class="modal fade" id="m_reprovacao_inspecao" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-finalizar-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" >
							<label for="exampleSelect1">Motivo da Reprovação da Inspeção</label>
							<textarea class="form-control" id="motivo_reprovacao_insp"></textarea>
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="enviar_reprovacao" class="btn btn-primary" pedido_id="" >Enviar</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Finalizar pedido -->
	<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-finalizar-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" >
							<label for="exampleSelect1">E-mail do cliente</label>
							<input type="email" style="text-transform: lowercase;" class="form-control m-input--air" value="" id="email" placeholder="E-mail" />
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="finalizar_pedido" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal Anexar nfe ao pedido -->
	<div class="modal fade" id="anexarNfe" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/anexarNfe');?>" method="post"  enctype="multipart/form-data">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-anexar-nfe-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="row">
						<div class="form-group m-form__group col-lg-6 col-xs-12">						
							<label for="exampleSelect1">Nota fiscal</label>
							<input type="text" class="form-control m-input--air" name="nr_nf" id="nr_nf" placeholder="Número" required="required" />
							<input type="file" class="form-control m-input--air" name="nfe" id="nfe" placeholder="Anexar Nfe" />
						</div>	
						<div class="form-group m-form__group col-lg-6 col-xs-12">						
							<label for="exampleSelect1">Dt. Emissão NF.</label>
							<input type="text" class="form-control m-input--air date" name="dt_emissao_nf" id="dt_emissao_nf" placeholder="Dt. Emissão NF."  />	
						</div>	
					</div>
					<div class="row">
						<div class="form-group m-form__group col-lg-4 col-xs-12">						
							<label for="exampleSelect1">Prazo de entrega do pedido</label>
							<input type="text" class="form-control m-input--air" name="prazo_entrega" id="prazo_entrega" placeholder="Prazo de entrega"  />
							<input type="hidden" id ="pedido_id"  name="pedido_id" placeholder="Prazo de entrega" />
							<input type="hidden" id ="cliente_id" name="cliente"  />
						</div>	
						<div class="form-group m-form__group col-lg-4 col-xs-12">						
							<label for="exampleSelect1">Dt. Previsão de Entrega</label>
							<input type="text" class="form-control m-input--air date" name="dt_previsao_entrega" id="dt_previsao_entrega" placeholder="Dt. Previsão de Entrega" required="required" />
						
						</div>
						<div class="form-group m-form__group col-lg-4 col-xs-12">						
							<label for="exampleSelect1">Transportadora</label>
							<select type="text" class="form-control m-input--air" name="transp_id" id="transp_id" placeholder="transportadora" required="required" >	
									<option> Selecione uma transportadora</option>
								<?php foreach($transportadoras as $transp){ ?>
									<option value="<?=$transp['id']?>"><?=$transp['descricao']?></option>
								<?php } ?>	
							</select>	
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="submit" id="enviar_nf" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
			</div>			
		</div>
		</form>
	</div>
	<!-- Modal Gerar Startup -->
	<div class="modal fade" id="gerarPdfStartup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/gerarFormularioStartup');?>" method="post"  enctype="multipart/form-data">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-anexar-nfe-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="row" id="modelos_nr_series">
						
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" class="cliente_id" value="" name="cliente_id">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
			</div>			
		</div>
		</form>
	</div>
	<!-- Modal Gerar Startup -->
	<div class="modal fade" id="m_listar_pintura" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">		
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-listar-pintura-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="" id="listar-pintura">
						
					</div>				
					<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/anexarPintura');?>" method="post"  enctype="multipart/form-data">	
					<div id="alterar_pintura" style="display: none;" >
						<hr style="border-bottom: 1px solid #ffcc00;" />
						<h6 style="text-align: center;">Alterar Info. Pintura</h6>
						<hr style="border-bottom: 1px solid #ffcc00;" />	
						<div class="row">
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" class="fl_identidade_visual" name="fl_identidade_visual" id="fl_idv_a" value="A" >
									Identidade visual será inserida posteriormente
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" class="fl_identidade_visual" name="fl_identidade_visual" id="fl_idv_s" value="S"  />
									Possui identidade visual
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" class="fl_identidade_visual" name="fl_identidade_visual" id="fl_idv_n" value="N"  />
									Não possui identidade visual
									<span></span>
								</label>																
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4">
								<input type="file" id="arquivo_pintura" name="pintura" class="form-control" required="required" />  
								<input type="hidden" name="pedido_id" id="pedido_id_pintura" />
							</div>
							<div class="col-lg-8">
								<textarea class="form-control" name="descricao_pintura" id="descricao_pintura" placeholder="Insira uma nova descrição para o padrão de pintura" required="required"></textarea>	
							</div>
						</div>
						<div class="row bomba_pintura" style="width: 422px;background: url(<?=base_url('bootstrap/img/silhuetaBomba1.png')?>) no-repeat;margin: 0 auto;display: none;">
							<div id="p_testeira" style="margin: 0 8px;">
																
							</div>
							<div id="p_lateral" style="float: right;margin: 220px 0px 0px 226px;">

							</div>
							<div id="p_frontal" style="margin: 0 5px 50px 4px">
								
							</div>
							<input type="hidden" name="testeira" id="v_testeira" />
							<input type="hidden" name="painel_lateral" id="v_painel_lateral" />
							<input type="hidden" name="painel_frontal" id="v_painel_frontal" />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" class="cliente_id" value="" name="cliente_id">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
				</form> 
			</div>			
		</div>
		</form>
	</div>
	<!-- Modal Gerar Startup -->
	<div class="modal fade" id="gerarAvisoDisp" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/gerarAvisoDisponibilidade');?>" method="post"  enctype="multipart/form-data" target="_blank">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-aviso-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 950px;">					
					<div class="row" id="modelos_aviso">
						
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" class="cliente_id" value="" name="cliente_id">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
			</div>			
		</div>
		</form>
	</div>


	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja os arquivos e seus dados enviados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){

				if($this->session->flashdata('pedidoPdf')){ ?>
					<script type="text/javascript">
						window.open('http://www.wertco.com.br/pedidos/<?php echo $this->session->flashdata('pedidoPdf'); ?>');
					</script>	
			<?php	} ?>		 
		<script type="text/javascript">
			
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaAdministrador/pedidos';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>
<script src="<?=base_url('bootstrap/js/jquery.ddslick.min.js')?>" type="text/javascript"></script>