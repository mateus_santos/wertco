<table class="" id="html_table_" width="100%">
<thead>	
	<tr>
		<th style="">Tipo</th>
		<th style="text-align: center;">#</th>											
		<th style="text-align: center;">Cliente</th>
		<th style="text-align: center;">Geração</th>
		<th style="text-align: center;">Valor</th>
		<th style="">Ações</th>
	</tr>
</thead>
<tbody>					
<?php $total = 0; foreach($dados as $dado){	?>
	<tr>
		<td style="padding: 20px;">
			<?php echo $dado['tipo']; ?>			
		</td>
		<td style="text-align: center;">
			<?php echo $dado['id']; ?>							
		</td>			
		<td style="text-align: center;">
			<?php echo $dado['cliente']; ?>				
		</td>
		<td style="text-align: center;">
			<?php echo $dado['dt_emissao']; ?>				
		</td>
		<td style="text-align: center;">
			<?php echo 'R$ '.number_format($dado['total'],2,",","."); ?>			
		</td>
		<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">			
			<a class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank" onclick="gerarPdf(<?php echo $dado['id']; ?>,'<?php echo $dado['tipo']; ?>')">
				<i class="la la-print"></i>
			</a>				
		</td>
	</tr>
	<?php 	$total = $total + $dado['total']; 
			
	} ?>
</tbody>
</table>
<div class="row" style="float: right;padding-top: 50px;">
	<h4>Valor Total de Oportunidades para esse modelo: <b>R$ <?=number_format($total,2,",",".")?></b></h4>	
</div>