<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">						
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/pedidos'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/pedidos'); ?>">Voltar</a>						
						</h3>
						<h3 class="m-portlet__head-text">&nbsp;&nbsp;  Pedido #<?php echo $dados->id;?></h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarPedidos');?>" method="post" enctype="multipart/form-data">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Orçamento</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="orcamento" id="orcamento" class="form-control m-input" placeholder="" required value="<?php echo '#'.$dados->orcamento_id.' | '.$dados->cliente; ?>" />
								<input type="hidden" required name="orcamento_id" id="orcamento_id" class="form-control m-input" placeholder="" required value="<?php echo $dados->orcamento_id; ?>" />
								<input type="hidden" required name="id" id="id" class="form-control m-input" placeholder="" required value="<?php echo $dados->id; ?>" />
								<input type="hidden" name="testeira" id="v_testeira" value="<?php echo $dados->testeira; ?>" />
								<input type="hidden" name="painel_lateral" id="v_painel_lateral" value="<?php echo $dados->painel_lateral; ?>" />
								<input type="hidden" name="painel_frontal" id="v_painel_frontal" value="<?php echo $dados->painel_frontal; ?>" />
								<input type="hidden" id="status_id" value="<?php echo $dados->status_pedido_id; ?>" />
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
							</div>
						</div>						
					</div>									
					<div class="form-group m-form__group row">
						<div class="col-lg-3 div_pintura">
							<label>Padrão Pintura:</label>
							<div class="m-input-icon m-input-icon--right">
								<select class="form-control m-input m-input--square" name="pintura" class="pintura" id="pintura">
									<option value="" <?php echo ($dados->pintura == '') ? 'selected="selected"' : ''; ?>>
										Selecione uma bandeira
									</option>
									<option value="PETROBRAS ANTIGA" <?php echo ($dados->pintura == 'BR' || $dados->pintura == 'PETROBRAS ANTIGA') ? 'selected="selected"' : ''; ?>>
										Petrobrás Antiga
									</option>
									<option value="PETROBRAS NOVA" <?php echo ($dados->pintura == 'BR NOVA' ||  $dados->pintura == 'PETROBRAS NOVA') ? 'selected="selected"' : ''; ?>>
										Petrobrás Nova
									</option>
									<option value="IPIRANGA ANTIGA" <?php echo ($dados->pintura =='IPIRANGA' || $dados->pintura =='IPIRANGA ANTIGA') ? 'selected="selected"' : ''; ?>>
										Ipiranga Antiga
									</option>
									<option value="IPIRANGA NOVA" <?php echo ($dados->pintura =='IPIRANGA NOVA') ? 'selected="selected"' : ''; ?>>
										Ipiranga Nova
									</option>									
									<option value="SHELL" <?php echo ($dados->pintura =='SHELL') ? 'selected="selected"' : ''; ?>>
										Shell
									</option>
									<option value="CHARRUA" <?php echo ($dados->pintura =='CHARRUA') ? 'selected="selected"' : ''; ?>>
										Charrua
									</option>
									<option value="ALE" <?php echo ($dados->pintura =='ALE') ? 'selected="selected"' : ''; ?>>
										Ale
									</option>
									<option value="TOTAL" <?php echo ($dados->pintura =='TOTAL') ? 'selected="selected"' : ''; ?>>
										Total
									</option>
									<option value="WERTCO" <?php echo ($dados->pintura =='WERTCO_BRANCA' || $dados->pintura =='WERTCO_CINZA' || $dados->pintura =='WERTCO') ? 'selected="selected"' : ''; ?>>
										Wertco
									</option>									
									<option value="BANDEIRA_BRANCA" <?php echo ($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA') ? 'selected="selected"' : ''; ?>>
										Bandeira Branca
									</option>
									<option value="TABOCAO" <?php echo ($dados->pintura == 'TABOCAO') ? 'selected="selected"' : ''; ?>>
										Tabocão
									</option>
									<option value="VIBRA B2B" <?php echo ($dados->pintura == 'VIBRA' || $dados->pintura == 'VIBRA B2B') ? 'selected="selected"' : ''; ?>>
										Vibra B2B
									</option>
									<option value="FIT" <?php echo ($dados->pintura == 'FIT') ? 'selected="selected"' : ''; ?>>
										FIT
									</option>
									<option value="SIM" <?php echo ($dados->pintura == 'SIM') ? 'selected="selected"' : ''; ?>>
										Sim
									</option>
									<option value="RODOIL" <?php echo ($dados->pintura == 'RODOIL') ? 'selected="selected"' : ''; ?>>
										RodOil
									</option>
									<option value="RAYGAS" <?php echo ($dados->pintura == 'RAYGAS') ? 'selected="selected"' : ''; ?>>
										RayGas
									</option>
									<option value="SANTA_LUCIA" <?php echo ($dados->pintura == 'SANTA_LUCIA') ? 'selected="selected"' : ''; ?>>
										Santa Lúcia
									</option>
									<option value="PERU" <?php echo ($dados->pintura == 'PERU') ? 'selected="selected"' : ''; ?>>
										EXTERIOR - PERU
									</option>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-paint"></i></span></span>
							</div>
							
						</div>
						<div class="col-lg-3">
							<label>Motor:</label>
							<div class="m-input-icon m-input-icon--right">
								<select class="form-control m-input m-input--square" name="motor" class="motor" id="exampleSelect2">									
									<option value="220V - MONOFÁSICO" <?php echo ($dados->motor == '220V - MONOFÁSICO' || $dados->motor == '220V') ? 'selected="selected"' : ''; ?>>
										220V - Monofásico
									</option>
									<option value="220V - TRIFÁSICO" <?php echo ($dados->motor == '220V - TRIFÁSICO') ? 'selected="selected"' : ''; ?>>
										220V - Trifásico
									</option>
									<option value="380V - TRIFÁSICO" <?php echo ($dados->motor == '380V - TRIFÁSICO' || $dados->motor == '380V') ? 'selected="selected"' : ''; ?>>
										380V - Trifásico
									</option>									
								</select>
							</div>
						</div>
						<div clas s="col-lg-3">														
							<label>Técnico para startup cadastrado:</label>	
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" class="tecnico" name="tecnico" id="" value="1" <?php echo ($dados->tecnico_id != 0) ? 'checked="checked"' : '';?>  />
									Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" class="tecnico" name="tecnico" id="" value="0" <?php echo ($dados->tecnico_id == 0) ? 'checked="checked"' : '';?>>
									Não
									<span></span>
								</label>								
							</div>
							<input type="text" name="tecnico_startup" class="form-control m-input" placeholder="Pesquise o técnico" id="tecnico_startup" style="<?php echo ($dados->tecnico_id == 0) ? 'display:none;' : 'display:block;';?>margin-top: 20px;" value="<?php echo $dados->tecnico; ?>" />		
							<input type="hidden" name="tecnico_id" id="tecnico_id" value="<?php echo $dados->tecnico_id?>" />						
						</div>											
						<div class="col-lg-3">														
							<label>Cliente deseja receber informações da produção do pedido:</label>	
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" name="fl_receber_info" id="" value="1" <?php echo ($dados->fl_receber_info ==1 ) ? 'checked="checked"' : ''; ?> />
									Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" name="fl_receber_info" id="" value="0"  <?php echo ($dados->fl_receber_info ==0 ) ? 'checked="checked"' : ''; ?>>
									Não
									<span></span>
								</label>								
							</div>						
						</div>
					</div>	
					<div class="form-group m-form__group row" id="arquivo_bb" style="<?=($dados->pintura == 'BANDEIRA_BRANCA' || $dados->pintura == 'bandeira_branca' || $dados->pintura == 'WERTCO') ? '' : 'display:  none'?>">
						<div class="col-lg-8"  style="border-top: 1px solid #ffcc00;border-left: 1px solid #ffcc00; border-bottom: 1px solid #ffcc00;">
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" class="fl_identidade_visual" name="fl_identidade_visual" id="" value="A" <?=($dados->fl_identidade_visual == 'A') ? 'checked=checked' : ''?>>
									Identidade visual será inserida posteriormente
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" class="fl_identidade_visual" name="fl_identidade_visual" id="" value="S" <?=($dados->fl_identidade_visual == 'S') ? 'checked=checked' : ''?> />
									Possui identidade visual
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" class="fl_identidade_visual" name="fl_identidade_visual" id="" value="N"  <?=($dados->fl_identidade_visual == 'N') ? 'checked=checked' : ''?>/>
									Não possui identidade visual
									<span></span>
								</label>																
							</div>
						</div>
						<div class="col-lg-4" style="border-top: 1px solid #ffcc00;border-right: 1px solid #ffcc00; border-bottom: 1px solid #ffcc00;">
							<div class="m-input-icon m-input-icon--right">
								<img src="<?php echo base_url('pedidos/'.$dados->arquivo);?>" class="arquivo img-responsive" width="100" style="border-radius: 50%;<?php echo ($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA') ? 'display: block;' : 'display: none;'; ?> margin-top: 10px;" />

							<input type="file" name="arquivo_bb" class="form-control m-input arquivo_bb" placeholder="insira o arquivo"  style="<?php echo ($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA' || $dados->pintura == 'WERTCO') ? 'display: block;' : 'display: none;'; ?> margin-top: 10px;" value="" />
							
							<textarea name="pintura_descricao" value="" id="pintura_descricao" class="form-control m-input" placeholder="Descrição da pintura" style="margin-top: 10px;"><?php echo $dados->pintura_descricao; ?></textarea>
							<br/>
							<h5 style="<?php echo ($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA') ? 'display: block;' : 'display: none;'; ?>" >
								Testeira: <small class="text-muted text-warning"><?php echo $dados->testeira; ?></small> </h5>
							<h5 style="<?php echo ($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA') ? 'display: block;' : 'display: none;'; ?>"> 
								Painel Lateral: <small class="text-muted text-warning"><?php echo $dados->painel_lateral; ?></small> </h5>
							<h5 style="<?php echo ($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA') ? 'display: block;' : 'display: none;'; ?>"> 
								Painel Frontal: <small class="text-muted text-warning"><?php echo $dados->painel_frontal; ?></small> </h5>	
							<a id="paineis" class="btn m-btn--pill m-btn--air btn-warning m-btn m-btn--warning m-btn--bolder m-btn--uppercase" style="<?php echo ($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA') ? 'display: block;' : 'display: none;'; ?>">Alterar Pintura</a>	
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>Endereço de entrega</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="endereco_entrega" id="endereco_entrega" class="form-control m-input" placeholder="" value="<?php echo $dados->endereco_entrega;?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>Nome do Cliente:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="nome_cliente" id="nome_cliente" value="<?php echo $dados->nome_cliente; ?>" class="form-control m-input" placeholder="" required>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>RG:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text"  name="rg_cliente" id="rg_cliente" value="<?php echo $dados->rg_cliente; ?>"  class="form-control m-input" placeholder=""	/>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>CPF</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text"  name="cpf_cliente" id="cpf_cliente" value="<?php echo $dados->cpf_cliente; ?>" class="form-control m-input" placeholder=""  />
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
												
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-5">
							<label>Garantia (Meses)</label>
							<input type="number" class="form-control" name="garantia" id="garantia" value="<?=$dados->garantia?>" required/>
						</div>
						<div class="col-lg-5">
							<label>Observação</label>
							<textarea class="form-control" name="observacao" id="observacao"><?php echo $dados->observacao; ?></textarea>
						</div>
						<div class="col-lg-2 proforma" style="<?=($dados->proforma == '') ? 'display: none;': 'display: block'; ?>"> 
							<label>Proforma</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" class="form-control" name="proforma" id="proforma" value="<?php echo $dados->proforma; ?>" />
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>							
						</div>
					</div>					
					
				    <div class="form-group m-form__group row" >
	                    <div class="m-portlet col-lg-12">
	                        <div class="m-portlet__head">
	                            <div class="m-portlet__head-caption">
	                                <div class="m-portlet__head-title">
	                                    <span class="m-portlet__head-icon">
	                                        <i class="la la-thumb-tack m--font-warning"></i>
	                                    </span>
	                                    <h3 class="m-portlet__head-text m--font-warning">
	                                        Forma de Pagamento (informações, parcelamentos, etc...)
	                                    </h3>
	                                </div>
	                            </div> 
	                        </div>
	                        <div class="m-portlet__body ">
	                        	<input type="hidden" name="edicao_forma_pgto" id="edicao_forma_pgto" value="0" />
	                            <table class="table m-table m-table--head-separator-warning">
	                                  <thead>
	                                    <tr>                          
	                                          <th style="width: 10%;">Porcentagem</th>
	                                          <th style="width: 80%;">Descrição</th>                                          
	                                          <th style="width: 10%;">
	                                              <a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add" data-toggle="m-tooltip" data-placement="top" title="Adicionar Forma de Pagamento">
	                                                <i class="la la-plus"></i>
	                                            </a>
	                                        </th>                              
	                                    </tr>
	                                  </thead>
	                                  <tbody>    
	                                      <?php $i=0; $formaPagtoT = ""; foreach($formaPagto as $pagto){ ?>
	                                              <tr id='excluir-<?php echo $i;?>'>
	                                                  <td>
	                                                      <div class="input-group">
	                                                        <?php echo $pagto['porcentagem'];  ?> %
	                                                            
	                                                        
	                                                    </div>        
	                                                </td>    
	                                                <td>
	                                                    <?php echo $pagto['descricao'];?>
	                                                </td>
	                                                <td>                                                    
	                                                    <i class="fa fa-minus excluir" pedido_id='<?php echo $dados->id; ?>' forma_pagto_id='<?php echo $pagto['id']; ?>' tr='excluir-<?php echo $i; ?>' style="color: red;cursor: pointer;"></i>                                                    
	                                                </td>
	                                      <?php $i++;}    ?>    
	                                       <tr id="modelo" indice='0' total_indice='0'>
	                                           <td>
	                                               <div class="input-group">
	                                                <input type="text" name="porcentagem[]" min="0" max="100" maxlength="3" class="form-control m-input porcentagem" placeholder="%"  >
	                                                <div class="input-group-append">
	                                                    <span class="input-group-text" id="basic-addon2">
	                                                        %
	                                                    </span>
	                                                </div>
	                                            </div>
	                                           </td>    
	                                           <td><input type="text"  name="descricao[]"  class="form-control m-input descricao" placeholder="Descrição"></td></td>
	                                           <td>
	                                            <i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
	                                        </td>
	                                       </tr>
	                                  </tbody>
	                              </table>
	                        </div>
	                        <div class="m-portlet__head">
		                        <div class="m-portlet__head-caption">
		                            <div class="m-portlet__head-title">
		                                <span class="m-portlet__head-icon">
		                                    <i class="la la-thumb-tack m--font-warning"></i>
		                                </span>
		                                <h3 class="m-portlet__head-text m--font-warning">
		                                    Informações para o Financeiro 
		                                </h3>
		                            </div>
		                        </div> 
		                    </div>
		                    <div class="m-portlet__body ">
		                    	<div class="form-group m-form__group row">
									<div class="col-lg-6">
										<label>Forma de Pagamento:</label>
										<div class="m-input-icon m-input-icon--right">
											<select class="form-control m-input m-input--square" name="faturamento_tp_pagto_id" class="" id="faturamento_tp_pagto_id">
												<option value="">Selecione um tipo/forma que vai ser realizado o pagamento</option>
											<?php foreach ($tp_pagto as $tp) { ?>
												<option value="<?=$tp['id']?>" <?=($tp['id'] == $dados->faturamento_tp_pagto_id) ? 'selected="selected"' : ''?>><?=$tp['descricao']?></option>			
											<?php } ?>
											</select>
										</div>
									</div>
									<div class="col-lg-6">
										<label>Total de Parcelas:</label>
										<div class="m-input-icon m-input-icon--right">
											<input type="number" name="nr_parcelas" min="1" value="<?=$dados->nr_parcelas?>" class="form-control" />
										</div>
									</div>
								</div>
		                    </div>    
	                    </div>                                               
                </div> 
		        <div class="form-group m-form__group row produtos" >
	                <div class="m-portlet col-lg-12">
	                    <div class="m-portlet__head">
	                        <div class="m-portlet__head-caption">
	                            <div class="m-portlet__head-title">
	                                <span class="m-portlet__head-icon">
	                                    <i class="la la-thumb-tack m--font-success"></i>
	                                </span>
	                                <h3 class="m-portlet__head-text m--font-success">
	                                    Produtos
	                                </h3>
	                            </div>
	                        </div> 
	                    </div>
						<div class="m-portlet__body " id="produtos_cadastrar">
							<?php $k=$i=0; foreach ($produtos as $produto) {

								if( $produto['tipo_produto_id'] != 4 ){
									$tipo = 'text';
									$value=$produto['produtos'];									
								}else{
									$tipo  = 'hidden';									
									$value = 'OPCIONAL';
								}
								$k = $i;
								for($j=0;$j<$produto['qtd'];$j++)
								{
									$html = '<div class="form-group m-form__group row novos_produtos">';
									$html .= '	<div class="input-group m-input-group m-input-group--solid">';
									$html .= '			<div class="input-group-prepend">';
									$html .= '				<span class="input-group-text" id="basic-addon1">';
									$html .= $produto['codigo'].' - '.$produto['modelo'].' - '.$produto['descricao'];
									$html .= '				</span>';						
									$html .= '			</div>';
									$html .= '			<input type="'.$tipo.'" class="form-control m-input" placeholder="Produtos" value="'.$value.'" style="background: #fff;" disabled="disabled">';
									if($tipo!='hidden'){
										$html .= '		<i class="fa fa-edit editar" indice="'.$k.'" style="color: #ffcc00;" nr_produtos="'.$produto['nr_produtos'].'" combustivel="'.$produto['id'].'" arla="'.$produto['fl_arla'].'"></i>';
									}
									$html .= '			<input type="hidden" indice="'.$k.'" class="form-control m-input old_'.$k.' olds" name="produtos[]" placeholder="Produtos" value="'.$value.'" style="background: #fff;" required="required">';
									$html .= '			<input type="hidden" indice="'.$k.'" class="form-control m-input produto_id" name="produto_id[]" value="'.$produto['produto_id'].'" placeholder="Produtos" style="background: #fff;">';
									$html .= '			<input type="hidden" class="form-control m-input" name="qtd['.$produto['produto_id'].']" value="'.$produto['qtd'].'" placeholder="Produtos" style="background: #fff;">';
									$html .= '			<input type="hidden" class="form-control m-input" name="tipo_produto[]" value="'.$produto['tipo_produto_id'].'" placeholder="Produtos" style="background: #fff;">';
									$html .= '			<input type="hidden" class="form-control m-input" name="valor[]" value="'.$produto['valor'].'" placeholder="valor_produto" style="background: #fff;">';
									$html .= '			<input type="hidden" class="form-control m-input" name="valor_stx[]" value="'.$produto['valor_stx'].'" placeholder="valor_produto" style="background: #fff;">';
									$html .= '		</div>';
									$html .= '	</div>';
							
									
									echo $html;
									$k++;
								}
								$i = $k+1;
						} ?>
						</div>	
	                </div>                                               
                </div>						
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</div>
			</form>
			<!--end::Form-->
		</div>
</div>	
<div class="modal fade" id="m_pedido_pintura" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">		
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title fotos_pedido_titulo" >Padrão de pintura</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
				<div class="row" style="width: 422px;height: 591px;background: url(<?=base_url('bootstrap/img/silhuetaBomba.png')?>) no-repeat;margin: 0 auto;">
					<div id="p_testeira" style="margin: 0 27px;">
						<h5></h5>
						<select id="testeira" class="form-control testeira_frontal" placeholder="Cor Testeira">
						</select>
						
					</div>
					<div id="p_lateral" style="margin-left: 295px;margin-top: 324px;">
						<h5></h5>
						<select id="painel_lateral" class="form-control" >
						</select>							

					</div>
					<div id="p_frontal" style="margin: 0 27px;">
						<h5></h5>
						<select id="painel_frontal" class="form-control testeira_frontal">
						</select>						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Confirmar</button>
			</div>
		</div>
	</div>
</div>
<script src="<?=base_url('bootstrap/js/jquery.ddslick.min.js')?>" type="text/javascript"></script>