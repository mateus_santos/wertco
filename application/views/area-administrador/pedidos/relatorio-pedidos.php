<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-diagram" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Relatório de Pedidos
					</h3>					
				</div>
			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >		
			<form class="" action="<?php echo base_url('areaAdministrador/gerarRelatorioPedidos');?>" method="post" target="_blank">
			<div class="form-group m-form__group row">
				<div class="col-lg-12">
					<label><b>Filtrar por:</b></label>
					<div class="m-radio-inline">
						<label class="m-radio">
							<input type="radio" value="s" class="tp" name="tp" />
							Situação Atual
							<span></span>
						</label>
						<label class="m-radio">
							<input type="radio" value="h" class="tp" name="tp" />
							Histórico
							<span></span>
						</label>						
					</div>											
				</div>	
			</div>
			<div class="form-group m-form__group row filtros" style="display: none;">
				<div class="col-lg-3">
					<label class="">Status:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="status_id" placeholder="Status Chamado" id="status_id" >
                        	<option value="">SELECIONE UM STATUS</option>
                        <?php 	foreach($status as $sts ){	?>       
                        	<option value="<?php echo $sts['id']; ?>" ordem="<?php echo $sts['ordem']; ?>">
                        		<?php echo mb_strtoupper($sts['descricao']); ?>                        			
                        	</option>
                        <?php } ?>
                       	</select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
						
					</div>							
				</div>
				<div class="col-lg-3 filtro_atual">
					<label>Período (Criação do Pedido):</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" 	 name="dt_ini" 	id="dt_ini"	class="form-control m-input datepicker" placeholder="data inicial criação" 	style="width: 49%;float: left;" /> 
						<input type="text" 	 name="dt_fim" 	id="dt_fim"	class="form-control m-input datepicker" placeholder="data final criação" 	style="width: 49%;float: right;"/>
						<input type="hidden" name="status_desc" id="status_desc" value="" />
						<input type="hidden" name="status_ordem" id="status_ordem" value="" />
					</div>
				</div>
				<div class="col-lg-3 filtro_hist">
					<label>Período (Histórico por status):</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="dt_ini_hist" id="dt_ini_hist" class="form-control m-input datepicker" placeholder="data inicial Histórico" style="width: 49%;float: left;" /> 
						<input type="text" name="dt_fim_hist" id="dt_fim_hist" class="form-control m-input datepicker" placeholder="data final Histórico" style="width: 49%;float: right;"/>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Representantes/Indicadores:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="indicadores" id="indicadores" class="form-control" placeholder="Pesquise por um indicador ou representante" />
						<input type="hidden" name="indicador_id" id="indicador_id"  />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
					</div>
				</div>
				<div class="col-lg-3">
					<label class="">Consultor Responsável:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="consultor" placeholder="Status Chamado" id="consultor" >
                        	<option value="">SELECIONE UM CONSULTOR</option>                        
                        	<option value="cindiel tavares">Cindiel Tavares</option>
                        	<option value="fábio lucena">Fábio Lucena</option>
                        	<option value="odair freitas">Odair Freitas</option>
                        	<option value="sebastian">Sebastian</option>
							<option value="rafaela costa">Rafaela Costa</option>
                       	</select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-users"></i></span></span>
						
					</div>							
				</div>
				<div class="col-lg-3">
					<label class="">Faturados:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="faturados" placeholder="Faturados" id="faturados" >
                        	<option value="">-- Selecione -- </option>                        
                        	<option value="S"> Sim </option>
                        	<option value="N"> Não </option>
                       	</select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-users"></i></span></span>
						
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Período Faturados:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="dt_ini_fatur" id="dt_ini_fatur" class="form-control m-input datepicker" placeholder="data inicial Histórico" style="width: 49%;float: left;" /> 
						<input type="text" name="dt_fim_fatur" id="dt_fim_fatur" class="form-control m-input datepicker" placeholder="data final Histórico" style="width: 49%;float: right;"/>
					</div>							
				</div>
			</div>			
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>							
					</div>
				</div>
			</div>			
			</form>	
		</div>						
	</div>
	<!--end::Portlet-->
	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" style="display: none;">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--danger">
						<span>
							Chamados
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->			
		</div>
	</div>

	
	<!--end::Portlet-->	
</div>	
