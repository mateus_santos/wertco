<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">						
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/pedidos'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/pedidos'); ?>">Voltar</a>						
						</h3>
						<h3 class="m-portlet__head-text">&nbsp;&nbsp;  Gerar Op's Pedido #<?php echo $dados->id;?></h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/confirmaPedidoOps/'.$id);?>" method="post" enctype="multipart/form-data"  >
				<div class="form-group m-form__group row produtos" >
	                <div class="m-portlet col-lg-12">
	                    <div class="m-portlet__head">
	                        <div class="m-portlet__head-caption">
	                            <div class="m-portlet__head-title">
	                                <span class="m-portlet__head-icon">
	                                    <i class="la la-thumb-tack m--font-warning"></i>
	                                </span>
	                                <h3 class="m-portlet__head-text m--font-warning">
	                                    Informações	                                    
	                                </h3>
	                            </div>
	                        </div> 
	                    </div>
	                    <div class="m-portlet__body " id="produtos_cadastrar">
	                    	<div class="form-group m-form__group row" >	                    		
								<div class="col-lg-10">
									<label>Informações Importantes</label>
									<div class="m-input-icon m-input-icon--right">
										<textarea type="text" value="" id="informacoes" name="informacoes" class="form-control m-input" rows="4" required="required"></textarea>
										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-warning"></i></span></span>
									</div>
								</div>
								<div class="col-lg-2">
									<label>Estoque</label>
									<div class="m-input-icon m-input-icon--right">
										<select name="fl_estoque" class="form-control">
											<option value="0">Não</option>
											<option value="1">Sim</option>
										</select>										
									</div>
								</div>
	                    	</div>	
	                    </div>
	                </div>
	            </div>        
				<div class="form-group m-form__group row produtos" >
	                <div class="m-portlet col-lg-12">
	                    <div class="m-portlet__head">
	                        <div class="m-portlet__head-caption">
	                            <div class="m-portlet__head-title">
	                                <span class="m-portlet__head-icon">
	                                    <i class="la la-thumb-tack m--font-success"></i>
	                                </span>
	                                <h3 class="m-portlet__head-text m--font-success">
	                                    Composição de itens
	                                    
	                                </h3>
	                            </div>
	                        </div> 
	                    </div>
						<div class="m-portlet__body " id="produtos_cadastrar">
							<div style="border: 1px solid #ffcc00;margin: 10px; padding: 10px;"><h4>Opcionais a serem vinculados</h4>
								<?php 
									//var_dump($dados);die;
									$total_opcional = 0;
									foreach ($produtos as $produto) {
	                                   	if( $produto['tipo_produto_id'] == 4 ){
	                                   		echo $produto['descricao'].': '.$produto['qtd'].'<br/>';
	                                   		$total_opcional = $total_opcional + $produto['qtd'];

	                                   	}
	                                }
	                            ?>
	                            <input type="hidden" id="total_opcional" value="<?php echo $total_opcional; ?>">
							</div>
							<?php 
							$combo="";
							
							foreach ($produtos as $produto) {
								if( $produto['tipo_produto_id'] == 4 ){
									$combo.="<option value='".$produto['produto_id']."+".$produto['descricao']."+".$produto['codigo']."+".$produto['valor']."'>".$produto['descricao']."</option>";
								}
							}

							$i=1;
							foreach ($produtos as $produto) {
								
								if( $produto['tipo_produto_id'] != 4 ){

									$tipo = 'text';
									$value=$produto['produtos'];									
									
									for($j=0;$j<$produto['qtd'];$j++)
									{

										if($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA'){
											$combo_pint = '<select class="form-control bandeira" name="pintura['.$produto['produto_id'].']['.$i.'][]" id="pintura-'.$i.'1" indice="'.$i.'" readonly="readonly">
															
															<option value="bandeira_branca" selected="selected">Bandeira Branca</option>
														</select>';
											
										}elseif($dados->pintura == 'BR'){
											$combo_pint = '<select class="form-control bandeira" name="pintura['.$produto['produto_id'].']['.$i.'][]" id="pintura-'.$i.'1" indice="'.$i.'" readonly="readonly">
															<option value="BR" selected="selected">BR</option>
															
														</select>';
											

										}elseif($dados->pintura == 'IPIRANGA'){	
											$combo_pint = '<select class="form-control bandeira" name="pintura['.$produto['produto_id'].']['.$i.'][]" id="pintura-'.$i.'1" indice="'.$i.'" readonly="readonly">
															
															<option value="Ipiranga" selected="selected">Ipiranga</option>		
															
														</select>'; 

											
										}elseif($dados->pintura == 'SHELL'){
											$combo_pint = '<select class="form-control bandeira" name="pintura['.$produto['produto_id'].']['.$i.'][]" id="pintura-'.$i.'1" indice="'.$i.'" readonly="readonly">
															
															<option value="Shell" selected="selected">Shell</option>
															
														</select>';	
											
										}elseif($dados->pintura == 'ALE'){
											$combo_pint = '<select class="form-control bandeira" name="pintura['.$produto['produto_id'].']['.$i.'][]" id="pintura-'.$i.'1" indice="'.$i.'" readonly="readonly">
															
															<option value="Ale" selected="selected">Ale</option>
															
														</select>';	
											
										}elseif($dados->pintura == 'TOTAL'){
											$combo_pint = '<select class="form-control bandeira" name="pintura['.$produto['produto_id'].']['.$i.'][]" id="pintura-'.$i.'1" indice="'.$i.'" readonly="readonly">
															
															<option value="TOTAL" selected="selected">TOTAL</option>
															
														</select>';	
											
										}elseif($dados->pintura == 'WERTCO'){
											$combo_pint = '<select class="form-control bandeira" name="pintura['.$produto['produto_id'].']['.$i.'][]" id="pintura-'.$i.'1" indice="'.$i.'" readonly="readonly">
															
															<option value="WERTCO" selected="selected">WERTCO</option>
															
														</select>';	
											
										}else{
											$combo_pint = '<select class="form-control bandeira" name="pintura['.$produto['produto_id'].']['.$i.'][]" id="pintura-'.$i.'1" indice="'.$i.'" readonly="readonly">
															<option value="BR" >BR</option>
															<option value="Ipiranga">Ipiranga</option>		
															<option value="Shell" >Shell</option>
															<option value="Ale" >Ale</option>
															<option value="TOTAL" >TOTAL</option>
															<option value="WERTCO" >WERTCO</option>
															<option value="bandeira_branca" selected="selected">Bandeira Branca</option>
														</select>';
										
										}	

										if( $dados->motor == '380V' ){
											$combo_motor = '<select  class="form-control" name="voltagem['.$produto['produto_id'].']['.$i.'][]" id="voltagem-'.$i.'1">
															<option value="220 V">220 V</option>
															<option value="380 V" selected="selected">380 V</option>
															<option value="110 V">110 V</option>
															<option value="440 V">440 V</option>
														</select>';
										}elseif( $dados->motor == '220V' ){
											$combo_motor = '<select  class="form-control" name="voltagem['.$produto['produto_id'].']['.$i.'][]" id="voltagem-'.$i.'1">
															<option value="220 V" selected="selected">220 V</option>
															<option value="380 V" >380 V</option>
															<option value="110 V">110 V</option>
															<option value="440 V">440 V</option>
														</select>';
										}elseif( $dados->motor == '110V' ){
											$combo_motor = '<select  class="form-control" name="voltagem['.$produto['produto_id'].']['.$i.'][]" id="voltagem-'.$i.'1">
															<option value="220 V" >220 V</option>
															<option value="380 V" >380 V</option>
															<option value="110 V" selected="selected">110 V</option>
															<option value="440 V">440 V</option>
														</select>';
										}elseif( $dados->motor == '440V' ){
											$combo_motor = '<select  class="form-control" name="voltagem['.$produto['produto_id'].']['.$i.'][]" id="voltagem-'.$i.'1">
															<option value="220 V" >220 V</option>
															<option value="380 V" >380 V</option>
															<option value="110 V" >110 V</option>
															<option value="440 V" selected="selected">440 V</option>
														</select>';
										}else{
											$combo_motor = '<select  class="form-control" name="voltagem['.$produto['produto_id'].']['.$i.'][]" id="voltagem-'.$i.'1">
															<option value="220 V" >220 V</option>
															<option value="380 V" >380 V</option>
															<option value="110 V" >110 V</option>
															<option value="440 V" >440 V</option>
														</select>';
										}


										$html = '<div class="form-group m-form__group row novos_produtos">';
										$html .= '	<div  style="width: none;display: flex;">';
										$html .= '			<div class="input-group-prepend">';
										$html .= '				<span class="input-group-text" id="basic-addon1">';
										$html .= $produto['codigo'].' - '.$produto['modelo'];
										$html .= '				</span>';						
										$html .= '			</div>';
										
										$html .= '			<input type="hidden" class="form-control m-input" name="produto_id[]" placeholder="Produtos" value="'.$produto['produto_id'].'" style="background: #fff; max-width:150px;" required>';
										$html .= '			<input type="hidden" class="form-control m-input" name="item_descricao['.$produto['produto_id'].']['.$i.'][]" placeholder="Produtos" value="'.$produto['codigo'].' - '.$produto['modelo'].' - '.$produto['descricao'].'" style="background: #fff; max-width:150px;" required>';
										$html .= '			<input type="hidden" class="form-control m-input" name="modelo['.$produto['produto_id'].']['.$i.'][]" placeholder="modelo" value="'.$produto['modelo'].'" style="background: #fff; max-width:150px;" required>';
										$html .= '			<input type="hidden" class="form-control m-input" name="codigo['.$produto['produto_id'].']['.$i.'][]" placeholder="modelo" value="'.$produto['codigo'].'" style="background: #fff; max-width:150px;" required>';
										
										$html.=	'<input class="form-control combustiveis '.$i.'" name="produtos['.$produto['produto_id'].']['.$i.'][]" value="'.$value.'" readonly="readonly">';
										$html .= '<input type="hidden" class="form-control m-input" name="produtoss['.$produto['produto_id'].']['.$i.'][]" style="background: #fff; max-width:150px;" required>';
									 	$html .= '			<input type="hidden" class="form-control m-input" name="valor['.$produto['produto_id'].']['.$i.'][]" value="'.$produto['valor'].'" style="background: #fff; max-width:150px;" required>';
										
										$html .= '<select name="subitem['.$produto['produto_id'].']['.$i.'][]" class="form-control subitens" indice='.$i.' id="subitem-'.$i.'1" style="background: #fff;" opcional_indice="1"><option value="">Selecione um opcional</option>'.$combo.'</select>';
										
										$html .= '<input type="text" id="subitem_qtd-'.$i.'1" name="subitem_qtd['.$produto['produto_id'].']['.$i.'][]" class="form-control produtos qtd_opcional" indice='.$i.' style="background: #fff;max-width: 100px;" placeholder="Qtd" opcional_indice="1">';			
										$html .= '		<i id="adicionar-'.$i.'" class="subitem fa fa-plus" indice='.$i.' style="position: absolute;right: 29px;cursor: pointer; display: none;" opcional_indice="1"></i>';
										$html .= '		</div>';
										$html.='<div class="table-responsive" style="margin-top: 10px;border-bottom: 1px solid #ffcc00;">
										<table class="table">
											<thead>
												<tr>
													<th>Pintura</th>
													<th>Potência</th>
													<th>Voltagem</th>
													<th>Frequência</th>
													<th>Motor</th>
													<th>Mangueira</th>
													<th>Bicos</th>
													<th>Observações</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														'.$combo_pint.'
													</td>
													<td>
														<select class="form-control" name="potencia['.$produto['produto_id'].']['.$i.'][]" id="potencia-'.$i.'1">
															<option value="1 CV">1 CV</option>
															<option value="2 CV">2 CV</option>
														</select> 
													</td>
													<td>
														'.$combo_motor.'
													</td>
													<td>
														<select  class="form-control" name="frequencia['.$produto['produto_id'].']['.$i.'][]" id="frequencia-'.$i.'1">
															<option value="60 Hz">60 Hz</option>
															<option value="50 Hz">50 Hz</option>															
														</select> 
													</td>
													<td>
														<select class="form-control"  name="motor['.$produto['produto_id'].']['.$i.'][]" id="motor-'.$i.'1">
															<option value="Trifásico">Trifásico</option>
															<option value="Monofásico">Monofásico</option>
														</select> 
													</td>
													<td>
														<select class="form-control"  name="mangueira['.$produto['produto_id'].']['.$i.'][]" id="mangueira-'.$i.'1">
															<option value="3/4 Pol. X 4300MM COMP. C/TERMINAIS">3/4" X 4300MM COMP. C/TERMINAIS</option>
															<option value="1 Pol. X 4300MM COMP. C/TERMINAIS">1" X 4300MM COMP. C/TERMINAIS</option>
															<option value="3/4 Pol. X 4900MM COMP. C/ 1 TERMINAL FIXO E 1 GIRATÓRIO"> 3/4" X 4900MM COMP. C/ 1 TERMINAL FIXO E 1 GIRATÓRIO</option>
															<option value="1 Pol. X 4900MM COMP. C/ 1 TERMINAL FIXO E 1 GIRATÓRIO">1" X 4900MM COMP. C/ 1 TERMINAL FIXO E 1 GIRATÓRIO</option>
														</select> 
													</td>
													<td>
														<select class="form-control"  name="bicos['.$produto['produto_id'].']['.$i.'][]" id="bicos-'.$i.'1">
															<option value="11 AP 3/4 Pol. X PONTEIRA DE 1/2 Pol. COM CAPA">11 AP 3/4" X PONTEIRA DE 1/2" COM CAPA</option>
															<option value="11 BP 3/4 Pol. X PONTEIRA DE 1/2 Pol. COM CAPA">11 BP 3/4" X PONTEIRA DE 1/2" COM CAPA</option>
															<option value="7H 1 Pol. X PONTEIRA 1 Pol. COM CAPA">7H 1" X PONTEIRA 1" COM CAPA</option>															
														</select> 
													</td>													
													<td>
														<textarea class="form-control"  name="obs['.$produto['produto_id'].']['.$i.'][]" id="obs-'.$i.'1"></textarea> 
													</td>
												
												</tr>
											</tbody>
										</table>
										</div>';
										$html .= '	</div>';
										
										echo $html;										
										$i++;
									}

								}else{
									$opcional = '<input type="hidden" name="opcional['.$produto['produto_id'].']" descricao="'.$produto['descricao'].'" produto_id="'.$produto['produto_id'].'" qtd="'.$produto['qtd'].'" class="opicionais">';
									
									$opcional .= '<input type="hidden" id="subitem_desc-'.$i.'" name="subitem_desc['.$produto['descricao'].']['.$i.'][]" class="form-control produtos" indice='.$i.' style="background: #fff;max-width: 100px;" placeholder="Qtd">';
									
									echo $opcional;
								}


								
							} 
							?>
						</div>	
	                </div>                                               
                </div>						
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<input type="hidden" name="salvar" value="1"/>
								<input type="hidden" name="cliente" value="<?php echo $cliente->cliente; ?>"/>
								<input type="hidden" name="cliente_id" value="<?php echo $cliente->cliente_id; ?>"/>
								<input type="hidden" name="cidade" value="<?php echo $cliente->cidade; ?>"/>
								<input type="hidden" name="uf" value="<?php echo $cliente->uf; ?>"/>
								<input type="hidden" name="pais" value="<?php echo $cliente->pais; ?>"/>
								<button type="button" id="enviar" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="submit" id="enviar-form" style="display: none;" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase"></button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			
			</form>
			<!--end::Form-->
		</div>
</div>	