<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{	
	border-bottom: 1px solid #f0f0f0;	
	font-size: 9px;	
	padding: 5px;
}


</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br/>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br/>
										07400-230 - Arujá - SP<br/>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br/>
			</p>

		</div>
		<div class="corpo">
			<table style="border: none;" class="t_cabecalho" style="width: 100%;">
				<tr>
					<td colspan="3" style="text-align: center;">
						<h3 style="text-align: center;margin-top: 15px;	">Relatório de Vendas por Modelo</h3>
					</td>
				</tr>
				<tr>
					<td style="text-align: left;width: 33%;">
					<?php if($modelo != ''){ ?>			
						Modelo: <?php echo $modelo; ?>
					<?php } ?>
					</td>
					<td style="text-align: center;width: 33%;">
					<?php  if($periodo != ''){  ?>			
						Período <?php echo $periodo; ?>
					<?php } ?>
					</td>
					<td style="text-align: right;width: 33%;">
						<?php echo date('d/m/Y H:i:s'); ?>
					</td>
				</tr>
			</table>			
			<h3 style="text-align: center;margin-top: 15px;	"> </h3>
			
			<?php 

				if(count($dados) > 0){

			?>
			<hr  />
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;">
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b></b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Modelo</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Qtd</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Total</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Média Bomba</b></td>
				</tr>				
				<?php $aux_mes = ''; $total = 0; $total_bombas = 0; foreach($dados as $dado){ ?>
				<tr>
					<td style="padding: 10px; <?php echo ($aux_mes != $dado['mes']) ? 'background: #f0f0f0;' : ''; ?>">
						<?php echo ($aux_mes != $dado['mes']) ? mes($dado['mes']) . ' <b>' . $dado['ano'] . '</b>' : '' . ' <b>' . $dado['ano'] . '</b>';  ?> 
					</td>
					<td style="padding: 10px; <?php echo ($aux_mes != $dado['mes']) ? 'background: #f0f0f0;' : ''; ?>">
						<?php echo $dado['modelo']; ?> 
					</td>
					<td style="padding: 10px; <?php echo ($aux_mes != $dado['mes']) ? 'background: #f0f0f0;' : ''; ?>">
						<?php echo $dado['qtd_total']; ?> 
					</td>
					<td style="padding: 10px; <?php echo ($aux_mes != $dado['mes']) ? 'background: #f0f0f0;' : ''; ?>">	
						<?php echo 'R$ '.$dado['valor_total'] ?> 
					</td>
					<td style="padding: 10px;text-align: right; <?php echo ($aux_mes != $dado['mes']) ? 'background: #f0f0f0;' : ''; ?>">
						<?php echo 'R$ '.$dado['media'];?> 
					</td>				
				</tr>
			<?php 	
					$aux_mes 		= 	$dado['mes'];
					$total 			= 	(str_replace('.', '', $dado['valor_total'])) + $total;
					$total_bombas 	= 	$dado['qtd_total'] + $total_bombas;
					

			} 
			?>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan="5">Total: R$ <?php echo number_format($total,2,',','.');?> </td>
				</tr>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan="5">Total de Bombas: <?php echo $total_bombas;?> </td>
				</tr>	
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan="5">
						Preço Médio de Bombas: <?php echo number_format(($total / $total_bombas), 2,',','.');?>
					</td>
				</tr>			
			</table>
			<?php }else { ?>	
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc; margin-top: 300px;" />								
				<h2 style="text-align: center;">Nenhum resultado encontrado!</h2>
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc;" />							
			<?php } ?>	

			
		
		</div>	
	</div>
</body>
</html>

<?php 
	
	function mes($mes){
        switch ($mes) {
            case 1:
                return 'Janeiro';
                break;
            case 2:
                return 'Fevereiro';
                break;    
            case 3:
                return 'Março';
                break;
            case 4:
                return 'Abril';
                break;    
            case 5:
                return 'Maio';
                break;    
            case 6:
                return 'Junho';
                break;       
            case 7:
                return 'Julho';
                break;         
            case 8:
                return 'Agosto';
                break;    
            case 9:
                return 'Setembro';
                break;        
            case 10:
                return 'Outubro';
                break;        
            case 11:
                return 'Novembro';
                break;        
            case 12:
                return 'Dezembro';
            case 'Janeiro':
                return '01';
            case 'Fevereiro':
                return '02';
            case 'Março':
                return '03';
            case 'Abril':
                return '04';
            case 'Maio':
                return '05';   
            case 'Junho':
                return '06';     
            case 'Julho':
                return '07';
            case 'Agosto':
                return '08';
            case 'Setembro':
                return '09';     
            case 'Outubro':
                return '10';
            case 'Novembro':
                return '11';
            case 'Dezembro':
                return '12';     
                break;                    
        }
    }

?>