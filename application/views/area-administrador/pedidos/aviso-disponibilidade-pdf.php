<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{	
	border-bottom: 1px solid #ffcc00;	
	font-size: 9px;	
	padding: 4px;
}
.t_cabecalho td{
	border-bottom: none;	
	border: none;
	text-align: center;
	width: 33%;
	padding: 0px;
	margin: 0px;
	font-size: 11px;
	font-weight: 600;
}
.rodape{
	position: absolute;
	bottom: 0px;
}

</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br/>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br/>
										07400-230 - Arujá - SP<br/>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br/>
			</p>
		</div>
		<div class="corpo">
			<table style="border: none;" class="t_cabecalho" style="width: 100%;">
				<tr>
					<td colspan="3" style="text-align: center;">
						<h3 style="text-align: center;margin-top: 15px;	">Aviso de Disponibilidade</h3>
					</td>
				</tr>								
			</table>				
			
			<hr  />
			<h3 style="margin: 20px 0 0 10px;font-size: 11px;">Informamos que o (os) equipamento (os) referente (es) ao pedido <?=$dados['pedido_id']?>, está (ão) disponíveis para retirada em nossa fábrica.</h3>
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;">
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>Emissão</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><?=date('d/m/Y H:i:s')?></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>Contato</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 4px;" colspan="3"><?=strtoupper($dados['contato'])?></td>					
				</tr>
				<tr>				
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>Razão Social</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 4px;" colspan="5"><?=$dados['razao_social']?></td>					
				</tr>				
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>CNPJ</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><?=$dados['cnpj']?></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>I.E</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><?=$dados['insc_estadual']?></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>Frete</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 4px;"><?=$dados['frete']?></td>					
				</tr>
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>Telefone</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><?=$dados['telefone']?></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>Endereço</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 4px;" colspan="3"><?=$dados['endereco']?></td>					
				</tr>
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>Celular</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><?=$dados['celular']?></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>Bairro</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 4px;" colspan="3"><?=$dados['bairro']?></td>					
				</tr>
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>Fax</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><?=$dados['fax']?></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>Cidade</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;" colspan="3"><?=$dados['cidade']?></td>					
				</tr>
				<tr>
					<td style="border-bottom: 1px solid #ffcc00;padding: 4px;"><b>E-mail</b></td>
					<td style="border-bottom: 1px solid #ffcc00;padding: 4px;"><?=$dados['email']?></td>
					<td style="border-bottom: 1px solid #ffcc00;padding: 4px;"><b>UF</b></td>
					<td style="border-bottom: 1px solid #ffcc00;padding: 4px;"><?=$dados['estado']?></td>					
					<td style="border-bottom: 1px solid #ffcc00;padding: 4px;"><b>Cep</b></td>
					<td style="border-bottom: 1px solid #ffcc00;border-right: 1px solid #ffcc00;padding: 4px;"><?=$dados['cep']?></td>					
				</tr>
			</table>			
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;">
				<tr>
					<td><b>Quantidade</b></td>
					<td><b>Modelo</b></td>
					<td><b>Descrição</b></td>
					<td><b>Valor Unit.</b></td>
					<td><b>Valor Total</b></td>
					<td><b>Dimensões (AxLxC)</b></td>
				</tr>
			<?php  for($i=0; $i<count($dados['qtd']);$i++ ){ ?>			
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b><?=$dados['qtd'][$i]?></b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b><?=$dados['modelo'][$i]?></b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b><b><?=$dados['descricao'][$i]?></b></b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b>R$ <?=number_format($dados['valor'][$i],2,',','.')?></b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 4px;"><b>R$ <?=number_format($dados['valor'][$i]*$dados['qtd'][$i],2,',','.')?></b></td>				
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px;"><b><b><?=$dados['dimensoes'][$i]?></b></b></td>	
				</tr>
			<?php } ?>					
			</table>
			
				<p style="margin: 20px 0 0;width: 400px; float: left;font-size: 11px;">Valor Total da Nota Fiscal: <b>R$ <?=number_format($valor['total'],2,',','.');?></b><br/>
					Peso Bruto: <b><?=$dados['peso_bruto']?></b><br/>
					Peso Liquido: <b><?=$dados['peso_liquido']?></b>
				</p>			
			
			<table style="margin: 10px 0 0; width: 200px; float: right;">
				<tr>
					<td>Nr. de Série</td>
					<td>Combustíveis</td>
				</tr>
			<?php foreach ($nr_serie as $nr_) { ?>
				<tr>
					<td><?=$nr_['id']?></td>
					<td><?=$nr_['combustivel']?></td>
				</tr>
			<?php } ?>
			</table><br/>
			</div>	
			<div class="corpo" style="margin-top: 20px;">	
				
					<h3 style="text-align: left;">Dados para Depósito</h3>				
					<p>Banco ITAÚ: 341 | Banco do Brasil: 001<br/>
						CNPJ: 27.314.980/0001-53<br/>
						Favorecido: Wertco Industria Comercio e Serviços de Manutenção em Bombas Ltda
					</p>				
					<h3 style="text-align: left;">Instruções para Coleta</h3>			
					<p>Caminhões com carroceria aberta, devem trazer corda e lona.<br/>
						Os equipamentos devem ser transportados na posição vertical (em pé) e nunca na horizontal (deitados).<br/>
						Equipamentos com altura acima de 2,00m necessitam de caminhões Munk para serem descarregados.<br/>
						Horários para coleta: Segunda à quinta das 8:00 às16:30, às sextas das 8:00 às 15:00.<br/><br/>
					</p>
					<p>Estamos à disposição para o esclarecimento de quaisquer dúvidas.<br>
						<b>CAMILA RODRIGUES</b><br/>
						<b>DEPARTAMENTO DE EXPEDIÇÃO</b><br/>
						+55 11 91559-3910<br/>
						+55 11 3900-2565  - <B>RAMAL: 314</B>
					</p>
			</div>	
		</div>	
	</div>
</body>
</html>