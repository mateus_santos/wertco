<table class="" id="html_table" width="100%">
<thead>	
	<tr>
		<th style="">Modelo</th>
		<th style="text-align: center;">Data</th>											
		<th style="text-align: center;">Dias</th>		
		<th style="text-align: center;">Oportunidades</th>
		<th style="text-align: center;">Nr. Bombas</th>
		<th style="text-align: center;">Nr. Bicos</th>
		<th style="text-align: center;">Valor</th>
	</tr>
</thead>
<tbody>					
<?php $total =	$total_bicos = $vl_total = 0; 	foreach($dados['orcamento'] as $dado){	?>
	<tr>
		<td style="padding-left: 10px;">
			<?php echo $dado['modelo']; ?>
			<i class="la la-plus-square orcamento" title="Andamento do Orçamento" style="color: #ffcc00" onclick="andamento(<?php echo $dado['id']; ?>,'<?=$dado['modelo']?>');"></i>	
		</td>
		<td style="text-align: center;">
			<?php echo $dado['data_primeiro']; ?>							
		</td>			
		<td style="text-align: center;">
			<?php echo $dado['diferenca']; ?>				
		</td>		
		<td style="text-align: center;">
			<?php echo $dado['total']; ?>				
		</td>		
		<td style="text-align: center;">
			<?php echo $dado['total_bombas']; ?>			
		</td>
		<td style="text-align: center;">
			<?php echo $dado['total_bicos']; ?>
		</td>					
		<td style="text-align: center;">
			<?php echo 'R$ '.number_format($dado['total_geral'],2,",","."); ?>
		</td>					
	</tr>
	<?php 	$total = $total + $dado['total_bombas']; 
			$total_bicos = $total_bicos + $dado['total_bicos'];
			$vl_total 	= 	$vl_total + $dado['total_geral'];
	} ?> 
</tbody>
</table>
<div class="row" style="float: right;padding-top: 50px;">
	<h4>Total de Bombas: <b><?=$total?></b><br>
	Total de Bicos: <b><?=$total_bicos?></b><br>
	Valor Total: R$ <b><?=number_format($vl_total,2,",",".")?>
</h4>
</div>