<style type="text/css">
.linha_ano{	
	text-align: left;
    padding-left: 4%;
    border-top: 1px solid #ffcc00;
    height: 50px;
    font-size: 18px;
    font-weight: 500;
    cursor: pointer;
    background: #fff;
    border-bottom: 1px solid #ffcc00;
}

.fa{
	font-size: 20px;
}

.fa-arrow-down{
	color: #ffcc00;
	font-size: 20px;
}

.m-badge{
	font-weight: 600;
    background: #ffcc00;
    color: #000;
    font-size: 1.0em;
}

.m-list__content{
	position: fixed;
	background: #000;
	z-index: 999;
	padding-bottom: 16px;
	padding-top: 16px;
	width: 90%;
}

@media  (max-width: 1024px){
	.m-list__content{
		width: 75% !important;
	}
}

@media  (max-width: 768px){
	.m-list__content{
		width: 100% !important;
	}	
}
.bombas{
	cursor: pointer;
}

</style>
<div class="m-list__content" style="">
    <div class="m-list-badge" style="font-size: 12px; text-align:center; margin: auto;line-height: 30px;font-weight: 500;">
    	<span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #ffcc00;"></span>&nbsp;ORÇAMENTOS EM NEGOCIAÇÃO&nbsp;</span>
		<span class="m-list-badge__item" style="color: #fff;"><span class="m-badge m-badge" style="background: #dd4308;"></span>&nbsp;PEDIDOS PENDENTES&nbsp;</span>
	</div>
</div>
<div style="margin: 0 auto;text-align: center;">
	<table style="width: 100%;margin-top: 50px; ">
		<thead>
			<th style="padding-bottom: 20px;width: 5%;">Semana</th>
			<th style="padding-bottom: 20px;width: 10%;">&nbsp;</th>
			<th style="padding-bottom: 20px;width: 85%;">Orçamentos</th>
		</thead>
		<tbody style="margin-top: 10px;">

		<?php $td_id=0;  $aux = 0; foreach($dados as $key=> $dados){
			$chave 	= 	explode('*', $key);			
			$td_id++;
			$ano = $chave[1];
			if($aux != $ano){ ?>
				<tr>
					<td colspan="3" class="linha_ano" ano="<?=$chave[1]?>"><?=$chave[1]?>  <i class="fa fa-arrow-down" ano="<?=$chave[1]?>"></i> </td>
				</tr>
			<?php $aux = $ano; } ?>			
			<tr class="linha_semana" ano="<?=$chave[1]?>">
				<td style="border-top: 1px solid #ffcc00;border-bottom: 1px solid #ffcc00;padding: 20px;height: 150px;">
					<b style="font-size: 35px;color: #ffcc00;"><?=$chave[0]?></b><br/>										
					<b style="font-size: 20px;color: #000;"><?=$chave[2]?></b>
				</td>
				<td style="border-top: 1px solid #ffcc00;border-bottom: 1px solid #ffcc00;padding: 20px;height: 150px;">
					<img src="<?php echo base_url('bootstrap/img/bomba.png');?>" style="margin-bottom: 15px;" title="Total de Bombas" onclick="mostra_bombas_bicos('<?=$chave[3];?>',<?=$chave[0]?>)" class="bombas">
					
				</td>				
				<td style="border-top: 1px solid #ffcc00;border-bottom: 1px solid #ffcc00;line-height: 3em;" class="area_pedidos" semana="<?=$chave[3];?>" bicos="" bombas="" total_bombas="" total_bicos="" id="<?=$td_id;?>">
					<?php  foreach($dados as $dado){
						switch ($dado['tipo']) {
							case 'pedido':
								$value = '1';
								$bg = '#dd4308';
								$color = "#fff";
								break;
							case 'orçamento':
								$value = '2';
								$bg = '#ffcc00';
								$color = "#000";
								break;
						}

						?>
						<span class="pedidos" style="background: #ffffff;padding: 5px;border-radius: 10px 10px 10px 10px;margin-right: 2px;cursor: pointer;width: 215px;height: 95px; font-size: 12px; border: 1px solid #ffcc00;" bicos="<?php echo $dado['bicos'];?>" bombas="<?php echo $dado['bombas']; ?>" orcamento_id="<?=$dado['id']?>" programacao_semanal_id="<?=$dado['id']?>" title="<?=$dado['razao_social']?>" semana="<?=$chave[3];?>" tipo="<?=$dado['tipo']?>">
							<span class="m-badge m-badge" title="" style="<?php echo 'background:'.$bg.'; color: '.$color; ?>;padding: 0 5px;"><?php echo $dado['id'];?></span>
							<i class="la primeiro" style="font-size: 16px !important;font-weight: 600;"></i> <span class="texto"><?php echo substr($dado['razao_social'],0,20).'...';?></span> 
							<i class="la la-info-circle" style="font-size: 16px !important;font-weight: 600;" title="Informações"></i>&nbsp;							
						</span>
					<?php }	?>
				</td>
			</tr>
		<?php  } ?>
		</tbody>	
	</table>
</div>
<div class="modal fade" id="info-pedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="titulo"></h5>					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="m-portlet__body">
						<div class="row" style="border-bottom: 1px solid #ffcc00;line-height: 3em;padding-bottom: 12px;margin-top: 10px;">							
							<div class="col-lg-6 col-xs-6">
								<h5>Cliente: 
									<small class="text-muted" id="cliente"></small>
								</h5>
							</div>
							<div class="col-lg-6 col-xs-6">
								<h5>Cidade/Estado: 
									<small class="text-muted" id="cidade"></small>
								</h5>
							</div>														
						</div>						
						<div class="row" style="border-bottom: 1px solid #ffcc00;line-height: 3em;padding-bottom: 12px;margin-top: 18px;">
							<div class="col-lg-12 col-xs-12">
								<h5>Modelos Contidos no Pedido:</h5>
								<span  id="modelos"></span>
							</div>		
						</div>
						<div class="row" style="border-bottom: 1px solid #ffcc00;line-height: 3em;padding-bottom: 12px;margin-top: 18px;">
							<div class="col-lg-6 col-xs-12">
								<h5>Nr. Bicos: 
									<small class="text-muted" id="bicos"></small>
								</h5>
							</div>
							<div class="col-lg-6 col-xs-12">
								<h5>Nr. Bombas: 
									<small class="text-muted" id="bombas"></small>
								</h5>
							</div>								
														
						</div>						
																								
						<div class="row" style="margin-top: 18px;">
							<div class="col-lg-12 col-xs-12">
								<h5>
									<input type="text" class="form-control" name="dt_programacao" id="dt_programacao" style="width: 291px;" placeholder="Alterar Data Semana Inexistente" />
								</h5>
							</div>
														
						</div>							
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_dt" class="btn btn-primary" programacao_semanal_id="" indicacao="0">Alterar Situação</button>
					<button type="button" id ="excluir_pedido" class="btn btn-warning" pedido_id="" indicacao="0" style="display: none;">Excluir pedido Cancelado</button>
				</div>
			</div>
		</div>
	</div>
<!-- Total de bombas e bicos por semana -->
<div class="modal fade" id="bombas-bicos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-status-title modal-title-bibo" ></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="m-portlet__body">										
					
						<div class="row" style="text-align: center;">
							<div class="col-lg-12 col-xs-12">
								<h5 style="background: #f0f0f0;">Comercial</h5>
							</div>								
						</div>						
						<div class="row" style="line-height: 3em;padding-bottom: 12px;margin-top: 18px;">
							<div class="col-lg-6 col-xs-12">
								<h5>Nr. Bicos: 
									<small class="text-muted" id="bicos_comercial"></small>
								</h5>
							</div>
							<div class="col-lg-6 col-xs-12">
								<h5>Nr. Bombas: 
									<small class="text-muted" id="bombas_comercial"></small>
								</h5>
							</div>																						
						</div>
						<div class="row" style="text-align: center;">
							<div class="col-lg-12 col-xs-12">
								<h5 style="background: #f0f0f0;">Produção</h5>
							</div>								
						</div>						
						<div class="row" style="line-height: 3em;padding-bottom: 12px;margin-top: 18px;">
							<div class="col-lg-6 col-xs-12">
								<h5>Nr. Bicos: 
									<small class="text-muted" id="bicos_producao"></small>
								</h5>
							</div>
							<div class="col-lg-6 col-xs-12">
								<h5>Nr. Bombas: 
									<small class="text-muted" id="bombas_producao"></small>
								</h5>
							</div>																						
						</div>									
							
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				
			</div>
		</div>
	</div>
</div>