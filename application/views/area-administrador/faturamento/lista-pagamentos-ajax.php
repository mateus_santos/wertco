<form id="form_pagto" action="<?=base_url('AreaAdministrador/salvarPagamento')?>" method="post" enctype="multipart/form-data">
<table class="" id="html_table" width="100%">
	<thead class="thead-dark">
		<tr>
			<th style="text-align: center;">#</th>
			<th style="text-align: center;">Valor</th>
			<th style="text-align: center;">Dt. Pagamento</th>
			<th style="text-align: center;">Comprovante</th>			
			<th style="text-align: center;">Ações</th>
		</tr>
	</thead>
	<tbody >		
		<?php $i=1; foreach ($dados as $dado) { ?>
		<tr>
			<td style="width: 5%;text-align: center;"><?=$i?></td>			
			<td style="text-align: center;"><?=($dado['valor']) > 0 ? 'R$ '.$dado['valor'] : '';?></td>
			<td style="text-align: center;"><?=($dado['valor']) > 0 ? date('d/m/Y',strtotime($dado['dt_pagto'])) : ''; ?></td>	
			<td style="text-align: center;"><a href="<?=base_url('comprovantes/'.$dado['comprovante'])?>" target="_blank"><?=$dado['comprovante']; ?></a> </td>
			<td data-field="Actions" class="m-datatable__cell " style="width:5%;">				
				<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick="excluir_pagamentos(<?=$dado['id']?>, <?=$dado['pedido_id']?>)" title="Excluir Pagamentos" target="_blank" >
					<i class="la la-trash"></i>
				</a>
			</td>
		</tr>
		<?php $i++;} ?>
		<tr>
			<td colspan="5">
				<hr/><h5>Adicionar pagamento</h5><hr/>
			</td>
		</tr>
		
		<tfoot>			
			<tr>				
				<td style="width: 5%;text-align: center;"><input type="hidden" name="pedido_id" id="pedido_id_ac" ></td>			
				<td style="text-align: center;"><input type="text" placeholder="valor pago" name="valor" class="form-control valor" id="valor" /> </td>
				<td style="text-align: center;"><input type="text" placeholder="dt_pagamento" name="dt_pagto" class="data form-control" id="dt_pagto" value="<?=date('d/m/Y')?>" /></td>
				<td style="text-align: center;"><input type="file" placeholder="enviar Comprovante" name="comprovante" class="form-control" id="comprovante" /></td>
				<td data-field="Actions" class="m-datatable__cell " style="width:5%; text-align: right;">
					<button type="submit" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Adicionar Pagamentos" target="_blank" >
						<i class="la la-check-circle"></i>
					</button>
				</td>			
			</tr>			
		</tfoot>			
	</tbody>
</table>
</form>	