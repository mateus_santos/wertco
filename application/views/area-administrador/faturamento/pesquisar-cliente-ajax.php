<table class="" id="html_table" width="100%">
	<thead class="thead-dark">
		<tr>
			<th style="text-align: center;">Pedido #</th>
			<th style="text-align: center;">Emissão</th>
			<th style="text-align: center;">Tipo Pagto</th>
			<th style="text-align: center;">Total Parcelas</th>
			<th style="text-align: center;">Valor Pedido</th>											
			<th style="text-align: center;">Valor Pago</th>
			<th style="text-align: center;">Ações</th>
		</tr>
	</thead>
	<tbody >		
		<?php foreach ($dados as $dado) { ?>
		<tr>
			<td style="width: 5%;text-align: center;"><?php echo $dado['id']; ?></td>			
			<td style="width: 10%;text-align: center;"><?php echo date('d/m/Y H:i:s', strtotime($dado['dthr_geracao'])); ?></td>
			<td style="width: 30%;text-align: center;"> <?php echo $dado['tipo_pagto']; ?></td>	
			<td style="width: 15%;text-align: center;"><?php echo $dado['nr_parcelas']; ?> </td>
			<td style="width: 15%;text-align: center;">R$ <?php echo $dado['valor_total']; ?> </td>
			<td style="width: 15%;text-align: center;"><?php echo ($dado['valor_pago']) > 0 ? 'R$'. $dado['valor_pago'] : ''; ?> </td>
			<td data-field="Actions" class="m-datatable__cell " style="width:5%;">				
				<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick="listar_pagamentos(<?=$dado['id']?>)" title="Adicionar Pagamentos" target="_blank" >
					<i class="la la-edit adicionar"></i>
				</a>
			</td>
		</tr>
		<?php } ?>
		</tbody>
	</table>