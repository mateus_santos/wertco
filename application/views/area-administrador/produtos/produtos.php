<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Produtos
					</h3>
				</div>
			</div>
			<div class="col-md-4 m-portlet__head-caption">													
				<a href="<?php echo base_url('AreaAdministrador/cadastraProdutos')?>" class="" style="color: #ffcc00; font-weight: bold;">
					<i class="la la-plus-circle" style="font-size: 38px;"></i>
				</a>
			</div>					
		</div>
		<div class="m-portlet__body">

			<!--begin: Datatable -->
			<table class="table table-striped" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="id" style="text-align: center;">ID</th>
						<th title="codigo">Código</th>
						<th title="modelo">Modelo</th>
						<th title="descricao">Descrição</th>						
						<th title="valor_unitario">Valor Unitário</th>						
						<th title="tipo_produto_id">Tipos</th>
						<th title="">Ações</th>
					</tr>
				</thead>
				<tbody>
					<tr>
					<?php foreach($dados as $dado){ ?>
						<td><?php echo $dado->id; ?></td>
						<td><?php echo $dado->codigo; ?></td>
						<td><?php echo $dado->modelo; ?></td>
						<td><?php echo $dado->descricao; ?></td>
						<td><span class="valor_unitario"><?php echo $dado->valor_unitario; ?></span></td>
						<td><?php echo $dado->tipo_produto; ?></td>																		
						<td data-field="Actions" class="m-datatable__cell">
							<span style="overflow: visible; width: 110px;">								
								<a href="<?php echo base_url('AreaAdministrador/editarProduto/'.$dado->id); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar" produto_id="<?php echo $dado->id; ?>" >
									<i class="la la-edit"></i>
								</a>
								<button id="excluir" onclick="excluirProduto(<?php echo $dado->id; ?>);" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" produto_id="<?php echo $dado->id; ?>">
									<i class="la la-trash"></i>
								</button>
								<a  class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill simular" title="Simular preço final de tabela" onclick="simulador(<?php echo $dado->id; ?>,'<?php echo $dado->codigo.'-'.$dado->modelo; ?>','<?php echo $dado->valor_unitario; ?>');">
									<i class="la la-usd"></i>
								</a>
							</span>
						</td>
					</tr>
						<?php } ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>		        
</div>		
<!-- Modal Chamados -->
<div class="modal fade" id="modal_simulador" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content" style="width: 100%;">
			<div class="modal-header">
				<h3 class="titulo_simulador m--font-inverse-light"></h3>				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body " style="width: 100%;">					
				<div class="row"> 
					<div class="col-lg-8">
						<p>Fórmula precificação: <b>(Valor Base / Comissão) * (Fator * ICMS )</b></p>
						<p>Valor Base: <span id="valor_base_set" style="font-weight: bold;"></span> </p>
						<hr/>
						<input type="text" class="form-control" value="" id="valor_tributo" style="width: 30%;float: left;margin-right: 30px;" placeholder="Valor base">
						<button class="btn m-btn m-btn--pill m-btn--hover-accent" id="simular_valor">Simular</button>
						<hr/>
						<table class="table">
							<thead>
								<th></th>
								<th>0%  <i class="fa fa-info-circle m--font-warning" data-toggle="m-tooltip" data-placement="top" title=" 0.7200 "></i></th>
								<th>2%  <i class="fa fa-info-circle m--font-warning" data-toggle="m-tooltip" data-placement="top" title=" 0.6945 "></i></th>
								<th>3%  <i class="fa fa-info-circle m--font-warning" data-toggle="m-tooltip" data-placement="top" title=" 0.6805 "></i></th>
								<th>4%  <i class="fa fa-info-circle m--font-warning" data-toggle="m-tooltip" data-placement="top" title=" 0.637 "></i></th>
								<th>5%  <i class="fa fa-info-circle m--font-warning" data-toggle="m-tooltip" data-placement="top" title=" 0.583 "></i></th>
							</thead>
							<tbody >
								<tr>
									<td>Icms 7% <i class="fa fa-info-circle m--font-warning" data-toggle="m-tooltip" data-placement="top" title="1.11"></td>
									<td >R$ <span id="tabela_7_0" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_7_2" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_7_3" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_7_4" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_7_5" class="tabela_precos"></span></td>									
								</tr>
								<tr>
									<td>Icms 12% <i class="fa fa-info-circle m--font-warning" data-toggle="m-tooltip" data-placement="top" title="1.09"></td>
									<td >R$ <span id="tabela_12_0" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_12_2" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_12_3" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_12_4" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_12_5" class="tabela_precos"></span></td>									
								</tr>
								<tr>
									<td>Icms 18% <i class="fa fa-info-circle m--font-warning" data-toggle="m-tooltip" data-placement="top" title="1.07"></td>
									<td >R$ <span id="tabela_18_0" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_18_2" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_18_3" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_18_4" class="tabela_precos"></span></td>
									<td >R$ <span id="tabela_18_5" class="tabela_precos"></span></td>									
								</tr>							
							</tbody>
						</table>

					</div>
					<div class="col-lg-4">
						<div class="" style="margin: 0 auto;padding: 10px;max-width: 250px;border: 1px solid #ffcc00;">
							<table class="table">
								<thead>
									<th>Icms</th>
									<th>Fator</th>
								</thead>
								<tbody>
							<?php foreach ($icms as $icm){ ?>
									<tr>
										<td><?php echo (int) $icm['valor_tributo']; ?>
										<input type="hidden" id="icms_<?php echo (int) $icm['valor_tributo']; ?>" value="<?php echo $icm['fator']; ?>"></td>
										<td><?php echo $icm['fator']; ?></td>
									</tr>
							<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">					
			</div>
		</div>
	</div>
</div>		
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
            title: "OK!",
            text: 'Cadastro realizado com sucesso!',
            type: "success"
        }).then(function() {
		 	window.location = base_url+'AreaAdministrador/produtos';
		}); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>