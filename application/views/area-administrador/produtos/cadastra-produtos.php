<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/produtos'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/produtos'); ?>">Voltar</a>						
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/cadastraProdutos');?>" method="post">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>Código:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required	name="codigo"	class="form-control m-input" placeholder="" required >										
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-code"></i></span>
								</span>
							</div>
						</div>
						<div class="col-lg-3">
							<label class="m-input-icon m-input-icon--right">Modelo:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="modelo" id="modelo" class="form-control m-input" placeholder="" required >
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>	
						</div>
						<div class="col-lg-3">
							<label class="m-input-icon m-input-icon--right">Modelo Técnico:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="modelo_tecnico" id="modelo_tecnico" class="form-control m-input" placeholder="" required >
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>	
						</div>
						<div class="col-lg-3">
							<label class="m-input-icon m-input-icon--right">Vazão:</label>
							<div class="m-input-icon m-input-icon--right">
								<select name="vazao" class="form-control">
									<option value=""> ----- Selecione a Vazão ----- </option>
									<option value="40">40 LPM</option>
									<option value="75">75 LPM</option>
									<option value="130">130 LPM</option>
									<option value="40/40">40/40 LPM</option>
									<option value="40/75">40/75 LPM</option>
									<option value="40/130">40/130 LPM</option>
									<option value="75/75">75/75 LPM</option>
									<option value="75/130">75/130 LPM</option>
									<option value="130/130">130/130 LPM</option>									
									<option value="130/130">130/130/130/130 LPM</option>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>	
						</div>	
					</div>	 
					<div class="form-group m-form__group row">						
						<div class="col-lg-3">
							<label class="m-input-icon m-input-icon--right">Nr. Rotativa:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="nr_rotativa" id="nr_rotativa" class="form-control m-input" placeholder="" required >
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
							</div>	
						</div>
						<div class="col-lg-3">
							<label class="m-input-icon m-input-icon--right">Nr. Bicos:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="nr_bicos" id="nr_bicos" class="form-control m-input" placeholder="" required="required" >
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-gas-pump"></i></span></span>
							</div>	
						</div>	
						<div class="col-lg-3">
							<label class="m-input-icon m-input-icon--right">Nr. Produtos (Combustíveis):</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="nr_produtos" id="nr_produtos" class="form-control m-input" placeholder="" required="required" >
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-gas-pump"></i></span></span>
							</div>	
						</div>
						<div class="col-lg-3">
							<label>Descrição:</label>
							<div class="m-input-icon m-input-icon--right">
								<textarea  required name="descricao" class="form-control m-input" placeholder="" required ></textarea>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-comment-o"></i></span></span>
							</div>							
						</div>
					</div>					
					<div class="form-group m-form__group row">
						
						<div class="col-lg-3">
							<label>Valor Unitário (R$):</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="valor_unitario" id="valor_unitario" class="form-control m-input" placeholder="" required >
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-dollar"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-3">
							<label for="Acesso">Tipo</label>							
							<select class="form-control m-input" name="tipo_produto_id" required>
								<option value="">Selecione o tipo do produto</option>
							<?php foreach ($dados_tipo_cadastro as $tipo_cadastro) { ?>
								<option value="<?php echo $tipo_cadastro->id; ?>">
									<?php echo $tipo_cadastro->descricao; ?>									
								</option>
							<?php } ?>							
							</select>
						</div>
						<div class="col-lg-3">
							<label><b>Novo Rtm:</b></label>
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" value="1" class="tp" name="fl_rtm" />
									Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" value="0" class="tp" name="fl_rtm" />
									Não
									<span></span>
								</label>						
							</div>											
						</div>	
					</div>	
				</div>	 				
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	