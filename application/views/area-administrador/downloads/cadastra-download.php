<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/downloads'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/downloads'); ?>">Voltar</a>						
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/cadastraDownloads');?>" method="post" enctype="multipart/form-data">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>Descrição:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="descricao" id="descricao" class="form-control m-input" placeholder="" required>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-file-text"></i></span></span>
							</div>
						</div>
						<div class="col-lg-6">
							<label>Arquivo:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="file" required name="nome_arquivo" id="nome_arquivo" class="form-control m-input" placeholder="" required style="text-align: right;">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-file"></i></span></span>
							</div>							
						</div>
					</div>				
					<div class="form-group m-form__group row">
						<div class="col-lg-12">														
							<label>Visualizado por :</label>	
							<div class="m-checkbox-list">
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" name="tipo_cadastro[]" value="1"> Clientes
									<span></span>
								</label>
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" name="tipo_cadastro[]" value="2"> Representantes
									<span></span>
								</label>
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" name="tipo_cadastro[]" value="3"> Mecânicos
									<span></span>
								</label>
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" name="tipo_cadastro[]" value="4"> Técnicos
									<span></span>
								</label>
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" name="tipo_cadastro[]" value="5"> Administradores
									<span></span>
								</label>
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" name="tipo_cadastro[]" value="6"> Indicadores
									<span></span>
								</label>
							</div>							
						</div>
					</div>	
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</div>
			</form>
			<!--end::Form-->
		</div>
</div>	