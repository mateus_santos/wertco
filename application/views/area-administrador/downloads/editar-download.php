<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/downloads'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/downloads'); ?>">Voltar</a>						
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarDownload');?>" method="post" enctype="multipart/form-data">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>Descrição:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="descricao" id="descricao" class="form-control m-input" placeholder="" required value="<?php echo $dados[0]->descricao; ?>">
								<input type="hidden" required name="download_id" id="download_id" class="form-control m-input" placeholder="" required value="<?php echo $dados[0]->id; ?>">
								<input type="hidden" required name="tipo_cadastro_id" id="tipo_cadastro_id" class="form-control m-input" placeholder="" required value="<?php echo $dados[0]->tipo_id; ?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-file-text"></i></span></span>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="m-widget4">
								<div class="m-widget4__item">
									<div class="m-widget4__img m-widget4__img--icon">							 
										<?php $tipo = explode('.',$dados[0]->nome_arquivo); ?>
										<img src="../../bootstrap/assets/app/media/img/files/<?php echo $tipo[count($tipo)-1];?>.svg" alt="">  
									</div>
									<div class="m-widget4__info">
										<span class="m-widget4__text">
										<?php echo $dados[0]->nome_arquivo; ?>
										</span> 							 		 
									</div>
									<div class="m-widget4__ext">
										<a href="../../downloads/<?php echo $dados[0]->arquivo; ?>" class="m-widget4__icon">
											<i class="la la-download"></i>
										</a>
									</div>			
								</div>
							</div>	
							<hr/>
							<label>Arquivo:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="file" name="nome_arquivo" id="nome_arquivo" class="form-control m-input" placeholder=""  style="text-align: right;" value="<?php echo $dados[0]->descricao; ?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-file"></i></span></span>
							</div>							
						</div>
					</div>				
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Visualizado por :</label>	
							<div class="m-checkbox-list">								
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" class="cadastros" name="tipo_cadastro[]" value="1" <?php if( strpos($dados[0]->tipo, 'clientes') !== false) echo 'checked=checked'; ?>  > Clientes
									<span></span>
								</label>
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" class="cadastros" name="tipo_cadastro[]" value="2" <?php if( strpos($dados[0]->tipo, 'representantes') !== false) echo 'checked=checked'; ?>> Representantes
									<span></span>
								</label>
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" class="cadastros" name="tipo_cadastro[]" value="3" <?php if( strpos($dados[0]->tipo, 'mecanicos') !== false) echo 'checked=checked'; ?>> Mecânicos
									<span></span>
								</label>
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" class="cadastros" name="tipo_cadastro[]" value="4" <?php if( strpos($dados[0]->tipo, "tecnicos") !== false) echo 'checked=checked'; ?>> Técnicos
									<span></span>
								</label>
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" class="cadastros" name="tipo_cadastro[]" value="5" <?php if( strpos($dados[0]->tipo, 'administrador')!== false) echo 'checked=checked'; ?>> Administradores
									<span></span>
								</label>
								<label class="m-checkbox m-checkbox--brand">
									<input type="checkbox" class="cadastros" name="tipo_cadastro[]" value="6" <?php if( strpos($dados[0]->tipo, 'indicadores')!== false) echo 'checked=checked'; ?>> Indicadores
									<span></span>
								</label>
							</div>							
						</div>
					</div>	
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</div>
			</form>
			<!--end::Form-->
		</div>
</div>	