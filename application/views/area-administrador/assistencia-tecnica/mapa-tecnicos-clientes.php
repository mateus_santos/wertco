 <style>
/* Always set the map height explicitly to define the size of the div
 * element that contains the map. */
#map {
    height: 100%;
}

.controls {
    margin-top: 10px;
    border: 1px solid transparent;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    height: 32px;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
  }

  #pac-input {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 300px;
  }

  #pac-input:focus {
    border-color: #4d90fe;
  }

  .pac-container {
    font-family: Roboto;
  }

  #type-selector {
    color: #fff;
    background-color: #4d90fe;
    padding: 5px 11px 0px 11px;
  }

  #type-selector label {
    font-family: Roboto;
    font-size: 13px;
    font-weight: 300;
  }

  #target {
    width: 345px;
  }

  #menu img{
    margin-bottom: 5px;
    margin-top: 5px;
    /*width: 25px;*/
  }

  #menu a{
    cursor: pointer;
  }

  .gm-style-iw-c{
    background: #ffffffde !important;
  }
   
  .gm-style-iw-d{
    overflow: inherit !important;
  }
   
</style>

  
  <div id="parceiros">
        <!--#todo-->
        <input id="pac-input" class="controls" type="text" placeholder="pesquisar por localidade">
        <div style="z-index: 999;position: absolute;left: 265px;top: 115px;background: #ffffffde; padding: 50px;height: 500px;overflow: auto;" class="controls" id="menu"> 
          <h4>Filtros</h4>
          <input id="empresas" class="form-control controls" type="text" placeholder="pesquisar razão social/CNPJ" /> <br/>        
          <a onclick="filterMarkers('todos');"><img src="<?php echo base_url('bootstrap/img/todos.png'); ?>"> &nbsp;&nbsp;Todos</a><br/>
          <a onclick="filterMarkers('tecnicos');"><img src="<?php echo base_url('bootstrap/img/chave-inglesa.png'); ?>"> &nbsp;&nbsp;Técnicos</a><br/>                  
          <a onclick="filterMarkers('chamado');"><img src="<?php echo base_url('bootstrap/img/chamado.png'); ?>"> &nbsp;&nbsp;Chamados</a><br/>          
          <hr/>
          <a onclick="filterMarkers('Aguardando Técnico');"><img src="<?php echo base_url('bootstrap/img/aguardando_tecnico.png'); ?>">&nbsp;&nbsp; Aguardando Técnico</a><br/>
          <a onclick="filterMarkers('Chamado Fechado');"><img src="<?php echo base_url('bootstrap/img/chamado_fechado.png'); ?>"> &nbsp;&nbsp;Chamado Fechado</a><br/>
          <a onclick="filterMarkers('Cancelado');"><img src="<?php echo base_url('bootstrap/img/chamado_cancelado.png'); ?>">&nbsp;&nbsp; Cancelado</a><br/>          
          <a onclick="filterMarkers('Aguardando Partida Inicial');"><img src="<?php echo base_url('bootstrap/img/aguardando_partida_inicial.png'); ?>">&nbsp;&nbsp; Aguardando Partida Inicial</a><br/>
          <a onclick="filterMarkers('Em Observação');"><img src="<?php echo base_url('bootstrap/img/em_observacao.png'); ?>"> &nbsp;&nbsp;Em Observação</a><br/>
          <a onclick="filterMarkers('Aguardando Documento');"><img src="<?php echo base_url('bootstrap/img/aguardando_documento.png'); ?>"> &nbsp;&nbsp;Aguardando Documento</a><br/>
          <a onclick="filterMarkers('Aguardando Peças');"><img src="<?php echo base_url('bootstrap/img/aguardando_documento.png'); ?>"> &nbsp;&nbsp;Aguardando Peças</a><br/>
          <a onclick="filterMarkers('Aguardando Cliente');"><img src="<?php echo base_url('bootstrap/img/aguardando_cliente.png'); ?>">&nbsp;&nbsp; Aguardando Cliente</a><br/>
          <a onclick="filterMarkers('Solicitacao Pendente');"><img src="<?php echo base_url('bootstrap/img/solicitacao_pendente.png'); ?>"> &nbsp;&nbsp;Solicitacao Pendente</a><br/>
          <a onclick="filterMarkers('Aberto pelo Cliente');"><img src="<?php echo base_url('bootstrap/img/aberto_pelo_cliente.png'); ?>"> &nbsp;&nbsp;Aberto pelo Cliente</a><br/>
          <a onclick="filterMarkers('Aguardando Pagamento');"><img src="<?php echo base_url('bootstrap/img/aguardando_pagamento.png'); ?>"> &nbsp;&nbsp;Aguardando Pagamento</a><br/>
          <a onclick="filterMarkers('Solicitação pendente');"><img src="<?php echo base_url('bootstrap/img/aguardando_pagamento.png'); ?>"> &nbsp;&nbsp;Solicitação pendente</a><br/> 
          <a onclick="filterMarkers('Pendência de O.S');"><img src="<?php echo base_url('bootstrap/img/aguardando_pagamento.png'); ?>"> &nbsp;&nbsp;Pendência de O.S</a><br/>          
          
        </div>
        
  </div>
<div id="map"></div>                
                  
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCacJw1Pk4vCdrDqvX1_AJSstEAv1oHbF4&libraries=places"></script>   
<script type="text/javascript">
        var First_Sh = [], retornaivus = 0, chart_vendas, map, first_time = true, First_UF, t_cadastrados, t_n_cadastrados;
        var gmarkers1 = [];
        var empresas = [];
        var map;
        function remover_title(){
            
            $(".gm-ui-hover-effect").each(function( index ) {
                console.log(index);
                $( this ).attr('title', '');
            });

        }
                     

        
function initMap() {

          map = new google.maps.Map(document.getElementById('map'), {
                      center: {lat: -16.68, lng: -49.25},
                      zoom: 4
                  });

          google.maps.event.trigger(map, 'resize');

          //map.setCenter(new google.maps.LatLng(-16.68, -52.2850379));

          // Create the search box and link it to the UI element.
          var input = document.getElementById('pac-input');
          var searchBox = new google.maps.places.SearchBox(input);
          map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

          // Bias the SearchBox results towards current map's viewport.
          map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());

          });
          /*
          var mouse_over_txt = "<div>mouse_over</div>";
          var click_txt = "<div>click_txt</div>";

          addMarker(-31.6915219,-52.3014871,map, mouse_over_txt,click_txt);

          var mouse_over_txt = "<div>mouse_over2</div>";
          var click_txt = "<div>click_txt2</div>";

          addMarker(-31.7046043,-52.3319408,map, mouse_over_txt,click_txt);*/          
          
          // Listen for the event fired when the user selects a prediction and retrieve
          // more details for that place.
          searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
              return;
            }

            // Clear out the old markers.
            /*markers.forEach(function(marker) {
              marker.setMap(null);
            });
            markers = [];
            */
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
              if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
              }
              var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
              };

              // Create a marker for each place.
             /* markers.push(new google.maps.Marker({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
              }));*/

              if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
              } else {
                bounds.extend(place.geometry.location);
              }
            });
            map.fitBounds(bounds);
          });                  

          $.ajax({
            type:   'POST',
            url:  base_url+"AreaAdministrador/todosTecnicosClientesMap",         
            async: true,
            success: function(retorno) {

              retorno = JSON.parse(retorno);             
              console.log(retorno.length);
              for (var i=0; i< retorno.length; i++){
                
                var click_txt = "<div style='padding: 10px; '>";
                click_txt += "<b>Tipo de empresa: </b>";
                click_txt += retorno[i].tipo;
                click_txt += "<br><br><b>Código: </b>";
                click_txt += retorno[i].id;
                click_txt += "<br><br/><b>Razão: </b>";
                click_txt += retorno[i].empresa;                                
                click_txt += "<br><br><b>Cidade: </b>";
                click_txt += retorno[i].cidade.toLowerCase();
                click_txt += "<br><br><b>Estado: </b>";
                click_txt += retorno[i].estado;
                click_txt += "<br><br><b>Rua: </b>";
                click_txt += retorno[i].endereco;                                
                click_txt += "<br><br><b>CEP: </b>";
                click_txt += retorno[i].cep;
                click_txt += "<br><br><b>Telefone: </b>";
                click_txt += retorno[i].telefone;
                click_txt += "<br><br><b>Celular: </b>";
                click_txt += retorno[i].celular;
                click_txt += "<br><br><b>Email: </b>";
                click_txt += retorno[i].email+"";
                if( retorno[i].tipo == 'tecnicos' ){
                  click_txt += "<br><br><b>Local Treinamento: </b>";
                  click_txt += retorno[i].local_treinamento+"";
                  click_txt += "<br><br><b>Tag: </b>";
                  click_txt += retorno[i].tag+"";
                }else{
                  click_txt += "<br><br><b>Status: </b>";
                  click_txt += decodeURIComponent(escape(retorno[i].status))+"";
                  click_txt += "<br><br><b>Bombas: </b>";
                  click_txt += retorno[i].modelos+"";
                }
                click_txt += "</div>";
                
                addMarker(retorno[i].lat,retorno[i].lng,map,"",click_txt,retorno[i].status,retorno[i].tipo,retorno[i].empresa);
                empresas.push(retorno[i].empresa);
              }                    

              //gmarkers1.setMap(map);

            }
          });

        //filtro por pesquisa  
        $( "#empresas" ).autocomplete({
          source: empresas,
          select: function( event, ui ) {
            console.log( "Selected: " + ui.item.value  );
            buscaEmpresa(ui.item.value);
          }

        });
    }


function addMarker(lat,lng, map, mouse_over_txt,click_txt,grupo,chamado_pedido,razao_social) {

    var icone = "";
    console.log(grupo);
    
    grupo = decodeURIComponent(escape(grupo));    
    if (grupo == 'tecnicos') icone = "https://www.wertco.com.br/bootstrap/img/chave-inglesa.png"; // posto
    //else  icone = "https://www.wertco.com.br/bootstrap/img/gas-pump.png"; // Aguardando técnico    
    else if (grupo == 'Aguardando Técnico')     icone = "https://www.wertco.com.br/bootstrap/img/aguardando_tecnico.png"; // Aguardando técnico
    else if (grupo == 'Chamado Fechado')        icone = "https://www.wertco.com.br/bootstrap/img/chamado_fechado.png"; // Aguardando técnico
    else if (grupo == 'Cancelado')              icone = "https://www.wertco.com.br/bootstrap/img/chamado_cancelado.png";
    else if (grupo == 'Aguardando Partida Inicial')   icone = "https://www.wertco.com.br/bootstrap/img/aguardando_partida_inicial.png"; 
    else if (grupo == 'Em Observação')          icone = "https://www.wertco.com.br/bootstrap/img/em_observacao.png"; 
    else if (grupo == 'Aguardando Documento')   icone = "https://www.wertco.com.br/bootstrap/img/aguardando_documento.png";
    else if (grupo == 'Aguardando Peças')       icone = "https://www.wertco.com.br/bootstrap/img/aguardando_documento.png";
    else if (grupo == 'Aguardando Cliente')     icone = "https://www.wertco.com.br/bootstrap/img/aguardando_cliente.png";
    else if (grupo == 'Solicitação pendente')   icone = "https://www.wertco.com.br/bootstrap/img/solicitacao_pendente.png";
    else if (grupo == 'Aberto pelo Cliente')    icone = "https://www.wertco.com.br/bootstrap/img/aberto_pelo_cliente.png";
    else if (grupo == 'Aguardando Pagamento')   icone = "https://www.wertco.com.br/bootstrap/img/aguardando_pagamento.png";    
    else if (grupo == 'Pendência de O.S')       icone = "https://www.wertco.com.br/bootstrap/img/aguardando_pagamento.png";
    else  icone = "https://www.wertco.com.br/bootstrap/img/gas-pump.png";    
    
    var icon = { 
        url: icone, // url
        //scaledSize: new google.maps.Size(40,40), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };

    var marker = new google.maps.Marker({
        //map: map,
        draggable: false,
        icon: icon,
        animation: google.maps.Animation.DROP,
        category: grupo,
        position: {lat: Number(lat), lng: Number(lng)},
        chamado_pedido:  chamado_pedido,
        razao_social:   razao_social
        
    });

    gmarkers1.push(marker);

    var mouse_over = mouse_over_txt;
    var mouse_click = click_txt;

    var infowindow = new google.maps.InfoWindow({
        content: mouse_over
    });

    var infowindow_click = new google.maps.InfoWindow({
        content: mouse_click
    });
    
    google.maps.event.addListener(infowindow_click, 'closeclick', function() {
        marker.setAnimation(null);
    });

    /*marker.addListener('mouseover', function() {
        infowindow.open(map, marker);
    });*/

    marker.addListener('click', function() {
        //map.setZoom(8);
        //map.setCenter(marker.getPosition());
        
        toggleBounce(marker);
        infowindow.close();
        infowindow_click.open(map, marker);
        setTimeout(function (){
            remover_title();
        },100);
        
    });
    
    marker.setMap(map);
}

function toggleBounce(marker) {
    if (marker.getAnimation() !== null) {
      marker.setAnimation(null);
    } else {
      marker.setAnimation(google.maps.Animation.BOUNCE);
    }
} 


function sleep(seconds) 
{
  var e = new Date().getTime() + (seconds * 1000);
  while (new Date().getTime() <= e) {}
}

function  filterMarkers(category) {
    
    for (i = 0; i < gmarkers1.length; i++) {
        marker = gmarkers1[i];        
        marker.setVisible(false);
        if (marker) {
          if( category == 'todos' ){
            marker.setVisible(true);

          }else if (category == marker.category ) {
            marker.setVisible(true);

          }else if (category == 'tecnicos' &&  marker.category == 'tecnicos' ) {          
            marker.setVisible(true);

          }else if( category == 'chamado' &&  marker.category != 'tecnicos'){
            
            marker.setVisible(true);

          }else if( category == 'chamado' &&  (marker.chamado_pedido != 'pedido' && marker.category != 'tecnicos') ){
            marker.setVisible(true);

          }else if( category == 'pedido'  &&   (marker.chamado_pedido != 'chamado' && marker.category != 'tecnicos' ) ){
            marker.setVisible(true); 

          }
        }else{
          console.log(marker);
        }   

    }    
    
}

function buscaEmpresa(empresa){
    for (i = 0; i < gmarkers1.length; i++) {
      marker = gmarkers1[i];
      if( marker.razao_social == empresa ){                
          marker.setVisible(true);
          toggleBounce(marker);
          map.setZoom(8);
          map.panTo(marker.position);

      }else{
          marker.setVisible(false);
      }  
    }
}
    



function remover_title(){

    $(".gm-ui-hover-effect").each(function( index ) {
        
        $( this ).attr('title', '');
    });
}


</script>