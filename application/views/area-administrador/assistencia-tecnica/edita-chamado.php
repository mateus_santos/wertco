<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/chamados'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/chamados'); ?>">Voltar</a>&nbsp;&nbsp;
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarChamado');?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="id" value="<?= $chamado['id']?>" id="id_chamado">
					<input type="hidden" id="parceiro_id">
					<div class="col-lg-12" >
						<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 62px;">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon m--hide">
											<i class="flaticon-statistics"></i>
										</span>										
										<h2 class="m-portlet__head-label m-portlet__head-label--success">
											<span>
												Informações do Chamado <b>#<?= $chamado['id']?></b>
											</span>
										</h2>
									</div>
								</div>								
							</div>
							<div class="m-portlet__body row">
								<div class="col-lg-5">
									<label>Cliente</label>
									<div class="m-input-icon m-input-icon--right">
										<input type="text" value="<?=$chamado['cliente']?>" id="cliente" class="form-control m-input" placeholder="" disabled>
										<span class="m-input-icon__icon m-input-icon__icon--right" style="right: 21px !important;"><span>
											<i onclick="window.open('<?php echo base_url('AreaAdministrador/visualizarEmpresa/'.$chamado['cliente_id']); ?>')" class="la la-file-text-o "></i>
											<i onclick="window.open('<?php echo base_url('AreaAdministrador/editarEmpresa/'.$chamado['cliente_id']); ?>')" class="la la-edit"></i>
											<i onclick="redireciona_configuracao(<?=$chamado['cliente_id']?>)" class="la la-cogs "></i></span></span>
									</div>
								</div>
								<div class="col-lg-4">
									<label>Pessoa de Contato</label>
									<div class="m-input-icon m-input-icon--right">
										<input type="text" value="<?=$chamado['contato']?>" id="contato" class="form-control m-input" placeholder="" disabled>
										<span class="m-input-icon__icon m-input-icon__icon--right"><span>
											
											<i class="la la-edit" onclick="window.open('<?php echo base_url('AreaAdministrador/editarUsuario/'.$chamado['contato_id']); ?>')"></i>

										</span></span>
									</div>
								</div>
								<div class="col-lg-3" style="text-align: center;">										
									<a  data-toggle="modal" href="#modal_ordem_pagto" class="btn btn-outline-success m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" style="text-align: center;margin-top: 20px;">
										<i class="la la-dollar"></i>
									</a>									
									<a  data-toggle="modal" href="#modal_solicitacao_pecas" class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" style="text-align: center;margin-top: 20px;">
										<i class="la la-wrench"></i>
									</a>	
									<a  data-toggle="modal" href="#modal_autorizacao" class="btn btn-outline-primary m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" style="text-align: center;margin-top: 20px;">
										<i class="flaticon-edit-1"></i>
									</a>								
								</div>
							</div>
							
							<div class="form-group m-form__group row col-lg-12" style="border-top: 1px #f0f0f0 dotted;">
								<div class="col-lg-4">
									<label>Tipo</label>
									<div class="m-input-icon m-input-icon--right">
										<select name="tipo_id" id="tipo_id" class="form-control" <?php if($chamado['status_id'] != 3 && $chamado['status_id'] != 4) echo "disabled"?>>
											<?php foreach( $tipos as $tipo ){ ?>
												<option <?php if($chamado['tipo_id'] == $tipo['id']) echo "selected"; ?> value="<?php echo $tipo['id']; ?>"><?php echo $tipo['descricao']; ?></option>												
											<?php } ?>
										</select>
										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
									</div>
								</div>	
								<div class="col-lg-4">
									<label>Prioridade</label>
									<div class="m-input-icon m-input-icon--right">
										<select name="prioridade_id" id="prioridade_id" class="form-control">
											<?php foreach($prioridades as $prioridade) { ?>
												<option <?php if($chamado['prioridade_id'] == $prioridade['id']) echo "selected"; ?> value="<?=$prioridade['id'] ?>"><?=$prioridade['descricao'] ?></option>
											<?php } ?>	
										</select>
										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
									</div>
								</div>
								<div class="col-lg-4">
									<label>Status</label>
									<div class="m-input-icon m-input-icon--right">
										<select name="status_id" id="status_id" class="form-control" required="">
											<option value="">Selecione um status</option>
											<?php foreach( $status as $status_ ){ ?>
												<option <?php if($chamado['status_id'] == $status_['id']) echo "selected"; ?> value="<?php echo $status_['id']; ?>"><?php echo $status_['descricao']; ?></option>
											<?php } ?>
										</select>
										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
									</div>
								</div>						
							</div>		
							<div class="form-group m-form__group row col-lg-12" style="border-top: 1px #f0f0f0 dotted;">
								<div class="col-lg-4">
									<label>Início Atendimento</label>
									<div class="m-input-icon m-input-icon--right">
										<input type="text" value="<?=($chamado['inicio_atendimento'] != null) ? date('d/m/Y H:i:s',strtotime($chamado['inicio_atendimento'])) : ''?>" name ="inicio_atendimento" id="inicio_atendimento" class="form-control m-input date" placeholder="" >
										<input type="hidden" name="inicio_antigo" value="<?=($chamado['inicio_atendimento'] != null) ? date('d/m/Y H:i:s',strtotime($chamado['inicio_atendimento'])) : ''?>">
										<span class="m-input-icon__icon m-input-icon__icon--right">
											<span><i class="la la-calendar"></i></span>
										</span>
									</div>
								</div>
								<div class="col-lg-4">
									<label>Fim Atendimento</label>
									<div class="m-input-icon m-input-icon--right">
										<input type="text" value="<?=($chamado['fim_atendimento'] != null) ? date('d/m/Y H:i:s',strtotime($chamado['fim_atendimento'])) : '' ?>" name="fim_atendimento" id="fim_atendimento" class="form-control m-input date" placeholder="" >
										<span class="m-input-icon__icon m-input-icon__icon--right">
											<span><i class="la la-calendar"></i></span>
										</span>
									</div>
								</div>
								<div class="col-lg-4">
									<label>Chegada do técnico do posto</label>
									<div class="m-input-icon m-input-icon--right">
										<input type="text" value="<?=($chamado['chegada_tecnico'] != null) ? date('d/m/Y H:i:s',strtotime($chamado['chegada_tecnico'])) : ''?>" name="chegada_tecnico" id="chegada_tecnico" class="form-control m-input date" placeholder="" >
										<span class="m-input-icon__icon m-input-icon__icon--right">
											<span><i class="la la-calendar"></i></span>
										</span>
									</div>
								</div>
							</div>					
							<div class="form-group m-form__group row" style="border-top: 1px #f0f0f0 dotted;">
								<div class="col-lg-12">									
									<div class="col-lg-12">
										<label>Selecione os Arquivos</label>
										<div class="m-input-icon m-input-icon--right">
											<input type="file" name="anexos_chamados[]" id="anexos_chamados" class="form-control m-input" placeholder="" multiple="" >
											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-upload"></i></span></span>
										</div>
									</div>										
								</div>	
							</div>							
							<div class="form-group m-form__group row" style="border-top: 1px #f0f0f0 dotted;" id="anexo_chamado">
							</div>
							
							<div class="form-group m-form__group row" style="border-top: 1px #f0f0f0 dotted;">
								<div class="col-lg-12">
									<label>Observação/Descrição do problema pelo cliente:</label>
									<div class="m-input-icon m-input-icon--right">
										<textarea class="form-control" name="descricao"><?php echo $chamado['descricao']; ?></textarea>
										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
									</div>
								</div>	
							</div>	
							
							<div class="form-group m-form__group row" style="border-top: 1px #f0f0f0 dotted;">
								<div class="col-lg-12">									
									<div class="col-lg-12">
										<label>Cadastrado por:</label>
										<div class="m-input-icon m-input-icon--right">
											<input type="text" name="criado" value="<?php echo $chamado['usuario']; ?>" id="" class="form-control m-input" disabled="disabled" >
											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
										</div>
									</div>										
								</div>
							</div>
							<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
								<div class="m-form__actions m-form__actions--solid">
									<div class="row">
										<div class="col-lg-6">
											<?php if($chamado['status_id'] != 3 && $chamado['status_id'] != 4) { ?>
											<button type="submit" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Salvar</button>
										<?php } ?>
											<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
										</div>							
									</div>
								</div>
							</div>
						</div>
						
					</div>
					
				</form>				
		</div>
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head" >
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Atividades&nbsp;&nbsp;
						</h3>
						<div class="m-portlet__head-caption cadastrar_orcamento">	
							<?php if ($chamado['status_id'] != 3 && $chamado['status_id'] != 4){?>						
								<a data-toggle="modal" href="#modal_add_atividade" class="" style="color: #ffcc00 !important; font-weight: bold;" >
									<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo chamado"></i>
								</a>
							<?php } ?>
						</div>
					</div>
					
				</div>
				
				<div class="m-list__content">
					<div class="m-list-badge" style="margin-top: 25px;">
						<span class="m-list-badge__item">&nbsp;Aberto&nbsp;&nbsp;<span class="m-badge m-badge--secondary"></span></span>				    	
						<span class="m-list-badge__item">&nbsp;Em andamento&nbsp;&nbsp;<span class="m-badge m-badge--info"></span></span>
						<span class="m-list-badge__item">&nbsp;Fechado&nbsp;&nbsp;<span class="m-badge m-badge--success"></span></span>
						
					</div>
				</div>		
			
			</div>
			<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
				
				<!--begin: Datatable -->
				<table class="" id="tabela_atividades" width="100%">
					<thead>
						<tr>
							<th style="text-align: center;">#</th>
							<th style="text-align: center;">Defeito</th>
							<th style="text-align: center;">Modelo</th>											
							<th style="text-align: center;">Número de série</th>						
							<th style="text-align: center;">Status</th>	
							<th style="text-align: center;">Inicio</th>
							<th style="text-align: center;">Cadastrado por</th>
							<th style="text-align: center;">Ações</th>
						</tr>
					</thead>
					<tbody>					
						<?php foreach($atividades as $key => $atividade){ ?>
							<tr>
								<td style="width: 5%;text-align: center;"><?php echo $key+1; ?></td>
								<td style="width: 20%;text-align: center;"><?php echo $atividade['defeito']; ?></td>		
								<td style="width: 15%;text-align: center;"><?php if($atividade['modelo'] != null) echo $atividade['modelo']; else echo $atividade['modelo_manual']; ?></td>		
								<td style="width: 15%;text-align: center;"><?php echo $atividade['numero_serie']; ?></td>	
								<td style="width: 10%;text-align: center;"><?php echo $atividade['status']; ?></td>	
								<td style="width: 20%;text-align: center;">
									<?php echo date('H:i:s d/m/Y',	strtotime($atividade['inicio'])); ?>							
								</td>	
								<td style="width: 10%;text-align: center;"><?php echo $atividade['usuario']; ?></td>
								<td data-field="Actions" class="m-datatable__cell" style="width: 25%;text-align: center !important;">
									<a href="#" onclick="editar_atividade('<?php echo $atividade['id']; ?>');" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" >
										<i class="la la-edit editar"></i>
									</a>
									<a atividade_id="<?php echo $atividade['id']; ?>" indice="<?php echo $key+1; ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill copiar" >
										<i class="la la-copy copiar"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
	</div>		        
</div>

<!--  solicitacao de peças -->
<div class="modal fade" id="modal_solicitacao_pecas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Solicitação de Peças <?php echo isset($solicitacao_pecas[0]['status']) ? ' - <span class="m--font-success"><b>'.$solicitacao_pecas[0]['status'] .'</b></span>' : '';   ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/solicitaPecas');?>" method="post" enctype="multipart/form-data">	
			<div class="modal-body">														
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 62px;">
						<div class="m-portlet__body">								
							<div class="form-group m-form__group row col-lg-12" style="border-top: 1px #f0f0f0 dotted;">
								<div class="col-lg-6">
									<div class="col-lg-12">
										<label>Descrição / Observação</label> 
										<div class="m-input-icon m-input-icon--right">
											<input type="text" id="descricao_pecas" value="" name="descricao_pecas" class="form-control m-input" placeholder="" required >
											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
										</div>
									</div>
									<div class="col-lg-12">
										<label>Modo Envio</label> 
										<div class="m-input-icon m-input-icon--right">
											<select id="modo_envio" name="modo_envio" class="form-control m-input" required >
												<option value="">Selecione um modo de envio</option>
												<option value="Aéreo">Aéreo</option>
												<option value="Rodoviário">Rodoviário</option>
												<option value="Técnico">Técnico</option>
												<option value="Motoboy">Motoboy</option>
												<option value="Correios">Correios</option>
											</select>
											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-car"></i></span></span>
										</div>
									</div>
								</div>	
								<div class="col-lg-6">
									<div class="col-lg-12">
										<label>Endereço de entrega</label>
										<div class="m-input-icon m-input-icon--right">
											<input type="hidden" name="destinatario" id="destinatario" />
											<select name="empresa_entrega" id="empresa_entrega" class="form-control" required="">
												<option value="">Selecione uma empresa</option>													
												<?php foreach( 	$endereco_entrega as $endereco 	){ 	?>
												<option value="<?=$endereco['cliente_id']?>" ><?=$endereco['razao_social']?></option>
												<?php } ?>
											</select> 
											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
										</div>
									</div>
									<div class="col-lg-12">
										<label></label>
										<div class="m-input-icon m-input-icon--right">
											<textarea type="text" id="endereco_entrega" name="endereco_entrega" class="form-control m-input" required ></textarea>
											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group m-form__group row col-lg-12" style="border-top: 1px #f0f0f0 dotted;">
								<input type="hidden" name="usuario_id" 		value="<?= $usuario_id?>">									
								<input type="hidden" name="nome_cliente" 	value="<?=$chamado['cliente']?>">
								<input type="hidden" name="chamado_id" 		value="<?= $chamado['id']?>" id="chamado_id_pecas">
								<input type="hidden" name="novo" 			value="0" id="novo">
								<!--<input type="hidden" name="solicitacao_pecas_id" value="<?php echo (isset($solicitacao_pecas[0]['id'])) ? $solicitacao_pecas[0]['id'] : '0'; ?>" >-->
								<table class="table m-table m-table--head-separator-warning table-itens-pecas">
								  	<thead>
								    	<tr>			      		
								      		<th>Peça</th>
								      		<th>Quantidade</th>									      											      		
								      		<th>
								      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add_pecas" data-toggle="m-tooltip" data-placement="top" title="Adicionar produto">
														<i class="la la-plus"></i>
													</a>
											</th>				      		
								    	</tr>
								  	</thead>
								  	<tbody>
								  		
									  		<tr>
									  			<td style="border-top: 1px solid #ffcc00;border-bottom: 1px solid #ffcc00;padding: 0;" colspan="5"></td>
									  			
									  			
									  		</tr>
								  			<tr id="modelo_peca" indice='0' total_indice='0'>
					                            <td>					                                
					                                <input type="text" name="peca[]" 	class="form-control pecas" indice='0' id="peca_0" />
					                                <input type="hidden" name="peca_id[]" class="form-control peca_id" value="0"  id="peca_id_0" />
					                            </td>
					                            <td>
					                                <input type="text" id="qtd_0" maxlength="4" class="form-control qtd_pecas" placeholder="qtd" style="width: 90px; text-align: center;" name="qtd[]" />
					                            </td>						                            						                            
					                            <td>
					                                <i class="fa fa-minus remover_pecas" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
					                            </td>
					                        </tr>						                        
					                    </tbody>
					                </table> 
					              
				            </div>  
					
						
					
				</div>

			<div class="modal-footer">
				<button type="submit" class="btn btn-primary"  name="salvar" value="1">Salvar</button>										
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>

			</div>
			</div>
			<?php 	if(	count($solicitacao_pecas) >= 1 && isset($solicitacao_pecas[0]['id']) 	) { 	?>
			<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 40px;">
				
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="flaticon-statistics"></i>
							</span>								
							<h2 class="m-portlet__head-label m-portlet__head-label--primary">
								<span>
									Solicitação de peças enviadas					
								</span>
							</h2>
						</div>
					</div>
					
				</div>
				<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
					
					<!--begin: Datatable -->
					<table class="" id="table" width="100%">
						<thead>
							<tr>
								<th style="text-align: center;">#</th>
								<th style="text-align: center;">Descrição</th>									
								<th style="text-align: center;">Status</th>	
								<th style="text-align: center;">Dthr. Solicitação</th>	
								<th style="text-align: center;">Responsável</th>
								<th style="text-align: center;">Ações</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ( $solicitacao_pecas as $solicita ) { ?>																			
								<tr>
									<td style="text-align: center; padding-bottom: 10px;"><?php echo $solicita['id']; ?></td>
									<td style="text-align: center; padding-bottom: 10px;"><?php echo $solicita['descricao']; ?></td>											
									<td style="text-align: center; padding-bottom: 10px;"><?php echo $solicita['status']; ?></td>
									<td style="text-align: center; padding-bottom: 10px;"><?php echo ($solicita['dthr_solicitacao'] != '0000-00-00 00:00:00') ? date('d/m/Y H:i:s',	strtotime($solicita['dthr_solicitacao'])) : ''; ?></td>
									<td style="text-align: center; padding-bottom: 10px;"><?php echo $solicita['responsavel']; ?></td>	
									<td style="text-align: center; padding-bottom: 10px;">
										<a class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air andamento_tour" style="margin-right: 5px;">
											<i title="Andamento da Solicitação" onclick="andamento(<?php echo $solicita['id']; ?>,<?php echo $solicita['status_id']; ?>);" class="flaticon-list-3 andamento" style="cursor: pointer;"></i>
										</a>
										<a href="#" onclick="visualizarPecas(<?php echo $solicita['id']; ?>);" class="btn btn-outline-primary m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="la la-eye"></i>&nbsp;
											
									</td>	
								</tr>
						<?php } ?>
						</tbody>
					</table>
					<!--end: Datatable -->
				</div>
			</div>	
		<?php } ?>	
			</div>	
			</form>

		</div>
	</div>
</div>

<!--  Orçamentos pagamentos -->
<div class="modal fade" id="modal_ordem_pagto" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" >Ordem de pagamento <?php if(count($ordem_pagamento) > 0) echo ($ordem_pagamento[0]['status_id'] == 3) ? ' - <span class="m--font-success"><b> 	'.$ordem_pagamento[0]['status'] .' - '.date('d/m/Y H:i:s',strtotime($ordem_pagamento[0]['dthr_pagamento'])) .' - <a href="'.base_url('nfe/'.$ordem_pagamento[0]['nota_fiscal']).'" target="_blank"  class="link_arquivo btn btn-outline-success m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air" style="float: right;margin-top: -10px;margin-left: 5px;"><i class="la la-file-pdf-o"></i></a></b></span>' : '- <span class="m--font-success"><b>'.$ordem_pagamento[0]['status'] .'</b></span>';   ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/ordemPagtoAssist');?>" method="post" enctype="multipart/form-data">	
				<div class="modal-body">														
					<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 62px;">
							<div class="m-portlet__body">								
								<div class="form-group m-form__group row col-lg-12" style="border-top: 1px #f0f0f0 dotted;">
									<div class="col-lg-6">
										<label>Descrição</label> 
										<div class="m-input-icon m-input-icon--right">
											<input type="text" id="descricao_ops" value="<?php echo (isset($ordem_pagamento[0]['descricao'])) ? $ordem_pagamento[0]['descricao'] : ''; ?>" name="descricao" class="form-control m-input" placeholder="" required >
											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
										</div>
									</div>	
									<div class="col-lg-6">
										<label>Técnico Favorecido</label>
										<div class="m-input-icon m-input-icon--right">
											<select name="favorecido_id" id="favorecido_id" class="form-control" required="">
												<?php foreach( $tecnicos as $tecnico ){
													if( isset($ordem_pagamento[0]['favorecido_id']) ){
														if($ordem_pagamento[0]['favorecido_id'] == $tecnico['empresa_id']){
															$selected = "selected='selected'";
														
														}else{
															$selected = "";

														}
													}else{
														$selected = "";

													}

												?>
												<option value="<?php echo $tecnico['empresa_id']?>" <?php echo $selected; ?>>
													<?php echo strtoupper($tecnico['empresa']." | ".$tecnico['tecnico']); ?>

												</option>
												<?php } ?>
											</select>
											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
										</div>
									</div>
								</div>
								<div class="form-group m-form__group row col-lg-12" style="border-top: 1px #f0f0f0 dotted;">
									<input type="hidden" name="usuario_id" 	value="<?= $usuario_id?>">									
									<input type="hidden" name="chamado_id" 	value="<?= $chamado['id']?>" id="chamado_id_ops">
									<input type="hidden" name="novo" 		value="<?php echo (isset($ordem_pagamento[0]['id'])) ? '1' : '0'; ?>" id="novo">
									<input type="hidden" name="ordem_pagamento_id" value="<?php echo (isset($ordem_pagamento[0]['id'])) ? $ordem_pagamento[0]['id'] : '0'; ?>" >
									<table class="table m-table m-table--head-separator-warning table-itens">
									  	<thead>
									    	<tr>			      		
									      		<th>Tipo - Descrição</th>
									      		<th>Quantidade</th>
									      		<th style="text-align: center;width: 150px;">Data do Serviço </th>
									      		<th style="text-align: right;">Valor Unitário R$</th>
									      		<th>
									      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add" data-toggle="m-tooltip" data-placement="top" title="Adicionar produto">
														<i class="la la-plus"></i>
													</a>
												</th>				      		
									    	</tr>
									  	</thead>
									  	<tbody>
									  		<?php if( isset($ordem_pagamento) ){	?>
									  			<?php foreach($ordem_pagamento as $op){ ?>
										  		<tr id="<?php echo $op['item_id'];?>">
										  			<td><?php echo strtoupper($op['tipo'].' - '.$op['despesa']); ?></td>
										  			<td><input type="text" value="<?php echo $op['qtd']; ?>" class="form-control qtd" disabled  style="text-align: center;width: 90px;" ></td>
										  			<td><input type="text"  style="text-align: center;width: 110px;"  class="form-control m-input data" placeholder="data"  disabled="disabled" value="<?php echo date('d/m/Y', strtotime($op['dt_servico'])); ?>"/> 
														
													</td>
										  			<td ><input type="text" style="text-align: right;" class="form-control valor_unitario item_inserido" disabled value="<?php echo $op['valor']; ?>" qtd="<?php echo $op['qtd']; ?>"></td>
										  			<td><i class="fa fa-minus excluirItem" item_id="<?php echo $op['item_id']; ?>" style="color: red;margin: 10px 0px;" ></td>
										  		</tr>
									  		<?php } } ?>
										  		<tr>
										  			<td style="border-top: 1px solid #ffcc00;border-bottom: 1px solid #ffcc00;padding: 0;" colspan="5"></td>
										  			
										  			
										  		</tr>
									  			<tr id="modelo" indice='0' total_indice='0'>
						                            <td>					                                
						                                <select  class="form-control despesas" style="font-size: 12px;" name="despesas[]" id="despesas_0" indice='0' required="required">
						                                	<option value="">Selecione um produto</option>
						                                    <?php 
						                                    	foreach($despesas as $despesa){
						                                    ?>
						                                    <option value="<?php echo $despesa['id']; ?>" valor="<?php echo $despesa['valor']; ?>">
						                                    	<?php echo strtoupper($despesa['tipo'])." - ".strtoupper($despesa['descricao']); ?> 
						                                	</option>
						                                    <?php } ?>
						                                </select>
						                            </td>
						                            <td>
						                                <input type="text" id="qtd_0" maxlength="4" class="form-control qtd" placeholder="qtd" style="width: 90px; text-align: center;" name="qtd[]"  />  
						                            </td>
						                            <td style="font-size: 12px;text-align: center;">
						                            	<input type="text" name="dt_servico[]" class="form-control data" placeholder="data do serviço"  style="text-align: center;width: 110px;" required=""  />
						                            </td>
						                            <td style="font-size: 12px;text-align: right;">
						                                <input type="text" name="valor[]" class="form-control valor_unitario" value="" id="valor_unitario_0" indice="0" maxlength="14" style="text-align: right;" placeholder="valor"  />
						                            </td>
						                            <td>
						                                <i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
						                            </td>

						                        </tr>
						                        
						                    </tbody>
						                </table> 
						                <table class="table m-table m-table--head-separator-warning">
						                	<tr>						                		
					                        	<td colspan="4" style="text-align: right;">
					                        		<h3>Total: &nbsp;<span class="total_op moeda"></span>&nbsp;<i class="la la-refresh atualizaTotal m--font-success"></i></h3>
					                        	</td>	
					                        	
					                        </tr>
						                </table>
					            </div>						
					</div>

				<div class="modal-footer">
					
					<button type="submit" class="btn btn-primary"  name="salvar" value="1">Salvar</button>
				
					<?php 
						if( isset($ordem_pagamento[0]['status']) ){
							if( $ordem_pagamento[0]['status'] == 'criada' ) {

					?>		
								<a href="#" class="btn btn-success m-btn m-btn--custom m-btn--icon" id="emitirOp" ordem_pagamento_id="<?php echo (isset($ordem_pagamento[0]['id'])) ? $ordem_pagamento[0]['id'] : "0"; ?>">
									<span>
										<i class="la la-envelope-o"></i>
										<span>
											Emitir Op
										</span>
									</span>
								</a>
					<?php 
							
						}elseif( $ordem_pagamento[0]['status'] == 'emitida' ){
							?>
							<a href="#" class="btn btn-success m-btn m-btn--custom m-btn--icon" id="emitirOp" ordem_pagamento_id="<?php echo (isset($ordem_pagamento[0]['id'])) ? $ordem_pagamento[0]['id'] : "0"; ?>" acao="reemitir">
									<span>
										<i class="la la-envelope-o"></i>
										<span>
											Reemitir Op
										</span>
									</span>
								</a>		
							<?php	
						}

					}
					?>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				</div>	
				<?php 	if(	count($ops_enviadas) >= 1 && isset($ops_enviadas[0]['id']) 	) {	?>
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 40px;">					
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>
								<h2 class="m-portlet__head-label m-portlet__head-label--primary">
									<span>
										Ordens de pagamento emitidas					
									</span>
								</h2>
							</div>
						</div>
						
					</div>
					<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >						
						<!--begin: Datatable -->
						<table class="" id="table" width="100%">
							<thead>
								<tr>
									<th style="text-align: center;">#</th>
									<th style="text-align: center;">Descrição</th>
									<th style="text-align: center;">Dthr. Emissao</th>
									<th style="text-align: center;">Favorecido</th>
									<th style="text-align: center;">Responsável Emissão</th>
									<th style="text-align: center;">E-mail</th>
									<th style="text-align: center;">Valor Total</th>
								</tr>
							</thead>
							<tbody>
								<?php  foreach ( $ops_enviadas as $ops ) {  ?>																			
									<tr>
										<td style="text-align: center; padding-bottom: 10px;"><?php echo $ops['id']; ?></td>
										<td style="text-align: center; padding-bottom: 10px;"><?php echo $ops['descricao']; ?></td>										
										<td style="text-align: center; padding-bottom: 10px;"><?php echo $ops['data_emis']; ?></td>
										<td style="text-align: center; padding-bottom: 10px;"><?php echo $ops['favorecido']; ?></td>
										<td style="text-align: center; padding-bottom: 10px;"><?php echo $ops['nome']; ?></td>
										<td style="text-align: center; padding-bottom: 10px;"><?php echo $ops['email_emissao']; ?></td>
										<td style="text-align: center; padding-bottom: 10px;">R$ <?php echo number_format($ops['valor_op'],2,",","."); ?></td>
									</tr>
							<?php } ?>
							</tbody>
						</table>
						<!--end: Datatable -->
					</div>
				</div>	
			<?php } ?>
					</div>	
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_add_atividade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" >Cadastrar Nova Atividade</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/inserirAtividade');?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="inicio" value="<?= date('Y-m-d H:i:s')?>">
					<input type="hidden" name="chamado_id" value="<?= $chamado['id']?>">
					<input type="hidden" name="usuario_id" value="<?= $usuario_id?>">
					<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 62px;">
							<div class="m-portlet__body">
								<div class="form-group m-form__group row">
									<div class="col-lg-6">
										<label>Defeito constatado</label>
										<div class="m-input-icon m-input-icon--right">
											&nbsp;
											<a href="#" style="color: #ffcc00 !important;font-weight: 100;float: right;" >
												<i class="la la-plus-circle adicionar_defeito" style="font-size: 35px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo defeito"></i>
											</a>
											<select name="defeito_id" id="defeito_id" class="form-control selectpicker" data-live-search="true" required style="width: 90%; float: left;">
												<?php foreach($defeitos as $defeito) { ?>
													<option value="<?=$defeito['id'] ?>"><?=$defeito['descricao'] ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="col-lg-6" id="div_novo_defeito_constatado" style="display: none;">										
										<label>Novo defeito constatado</label>
										<div class="m-input-icon m-input-icon--right">
											<input type="text" id="novo_defeito_constatado" class="form-control m-input" placeholder="" style="width: 90%;float: left;">
											<a href="#" onclick="adiciona_defeito()" style="color: #008000ad;font-weight: 100;float: right;">
												<i class="la la-check-circle" style="font-size: 38px;" title="Adicionar novo defeito"></i>
											</a>
										</div>										
																				
									</div>
								</div>
								<div class="form-group m-form__group row">
									<div class="col-lg-6">
										<label>Modelo</label>
										<select onchange="change_modelo(this)" name="modelo_id" id="modelo_id" class="form-control">
											<?php foreach($modelos as $modelo) { ?>
												<option value="<?=$modelo['id'] ?>" ><?=strtoupper($modelo['modelo']) ?></option>
											<?php } ?>	
											<option value="0">Outro</option>
										</select>
									</div>
									<?php if(count($modelos) > 0) { ?>
									<div class="col-lg-6" style="display: none"  id="div_modelo">
									<?php } else { ?>
									<div class="col-lg-6">
									<?php } ?>
										<label>Novo equipamento</label>
										<div class="m-input-icon m-input-icon--right">
											<input type="text" name="modelo_manual" id="modelo_manual" class="form-control m-input" placeholder="">
											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-build"></i></span></span>
										</div>
									</div>									
								</div>
								<div class="form-group m-form__group row">
									<div class="col-lg-6">
										<label>Número de série</label>
										<div class="m-input-icon m-input-icon--right">
											<!--<input type="text" name="numero_serie" id="numero_serie" class="form-control m-input" placeholder="">-->
											<select  name="numero_serie" id="numero_serie" class="form-control">
												<?php foreach($nr_series as $nr_serie){ ?>
													<option value="<?=$nr_serie['nr_serie']?>"><?=$nr_serie['nr_serie']?></option>	
												<?php	}	?>
											</select>
											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o "></i></span></span>
										</div>
									</div>	
									<div class="col-lg-6">
										<label>Status</label>
										<div class="m-input-icon m-input-icon--right">
											<select name="status_id" id="status_id" class="form-control" disabled>
												<option value="1">Aberta</option>
											</select>
											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
										</div>
									</div>			
								</div>
								<div class="form-group m-form__group row">
									<div class="col-lg-12">
										<label>Observação</label>
										<div class="m-input-icon m-input-icon--right">
											<textarea name="observacao" id="observacao" class="form-control m-input"> </textarea>										
										</div>
									</div>	
								</div>

							</div>
							
						</div>
				</div>

				<div class="modal-footer">
					<?php if($chamado['status_id'] != 3 && $chamado['status_id'] != 4) { ?>
					<button type="submit" class="btn btn-primary"  onclick="return valida_form_adiciona_atividade()">Salvar</button>
					<?php } ?>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>

					

				</div>
				
				</form>

			</div>

		</div>

	</div>
	
	
	<div class="modal fade " id="modal_edita_atividade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" >Editar Atividade</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
									
						<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
							<div class="m-portlet__body">								
								<ul class="nav nav-tabs" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" data-toggle="tab" href="#tab_atividade">
											<i class="la la-file-text"></i>
											Dados Atividade
										</a>
									</li>									
									<li class="nav-item">
										<a class="nav-link" data-toggle="tab" href="#tab_causa_solucao">
											<i class="la la-cogs"></i>
											Defeitos, Causas e soluções
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" data-toggle="tab" href="#tab_atendimento">
											<i class="la la-phone"></i>
											Atendimentos
										</a>
									</li>		
									<li class="nav-item">
										<a class="nav-link" data-toggle="tab" href="#tab_anexos">
											<i class="la la-upload"></i>
											Anexos
										</a>
									</li>							
								</ul>
								<div class="tab-content">
										
									<div class="tab-pane active" id="tab_atividade" role="tabpanel" style="background: #fff !important;">
										<form id="form1" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarAtividade');?>" method="post" enctype="multipart/form-data">
										<input type="hidden" name="chamado_id" value="<?= $chamado['id']?>">
										<input type="hidden" name="id" id="atividade_id">
										<input type="hidden" name="usuario_id" id="edit_usuario_id" value="<?= $usuario_id?>">
										<div class="m-portlet m-portlet--mobile">		
											<div class="form-group m-form__group row">
												<div class="col-lg-6">
													<label>Modelo</label>
													<select onchange="change_edit_modelo(this)" name="modelo_id" id="edit_modelo_id" class="form-control">
														<?php foreach($modelos as $modelo) { ?>
															<option value="<?=$modelo['id'] ?>"><?=$modelo['modelo'] ?></option>
														<?php } ?>	
														<option value="0">Outro</option>
													</select>
												</div>
												<?php if(count($modelos) > 0) { ?>
													<div class="col-lg-6" style="display: none"  id="div_edit_modelo">
													<?php } else { ?>
														<div class="col-lg-6">
														<?php } ?>
														<label>Novo equipamento</label>
														<div class="m-input-icon m-input-icon--right">
															<input type="text" name="modelo_manual" id="edit_modelo_manual" class="form-control m-input" placeholder="">
															<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-build"></i></span></span>
														</div>
													</div>
												</div>	
													
												<div class="form-group m-form__group row">
													<div class="col-lg-6">
														<label>Número de série</label>
														<div class="m-input-icon m-input-icon--right">
															<input type="text" name="numero_serie" id="edit_numero_serie" class="form-control m-input" placeholder="" >
															<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o "></i></span></span>
														</div>
													</div>	
													<div class="col-lg-6">
														<label>Status</label>
														<div class="m-input-icon m-input-icon--right">
															<select name="status_id" id="edit_status_id" class="form-control">
																<?php foreach($atividade_status as $status) { ?>
																	<option value="<?=$status['id'] ?>"><?=$status['descricao'] ?></option>
																<?php } ?>	
															</select>
															<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
														</div>
													</div>			
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-12">
														<label>Observação</label>
														<div class="m-input-icon m-input-icon--right">
															<textarea name="observacao" id="edit_observacao" class="form-control m-input"> </textarea>
															
														</div>
													</div>	
												</div>
												<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
													<div class="m-form__actions m-form__actions--solid">
														<div class="form-group m-form__group row">
															<div class="col-lg-6">
																<?php if($chamado['status_id'] != 3 && $chamado['status_id'] != 4) { ?>
																<button type="submit" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase" onclick="return valida_form_edita_atividade()">Salvar</button>
																<?php }	?>
																<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
															</div>							
														</div>
													</div>
												</div>
											
										</div>
										</form>			
									</div>
								
									<div class="tab-pane" id="tab_causa_solucao" role="tabpanel" style="background: #fff !important;">
										<div class="m-portlet m-portlet--mobile">																						
											<div class='m-portlet__body' style="padding-top: 20px;padding-bottom: 50px;">
												<div id='div_causas_solucoes' class="row" >
													
												</div>
												<hr style="border-bottom: 1px dashed #f0f0f0;" />
												<div class="form-group m-form__group row" id="div_defeitos">
													<div class="col-lg-6">
														<label>Defeito constatado</label>
														<div class="m-input-icon m-input-icon--right">
															&nbsp;
															<a href="#" style="color: #ffcc00 !important;font-weight: 100;float: right;" >
																<i class="la la-plus-circle edit_adicionar_defeito" style="font-size: 35px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo defeito"></i>
															</a>
															<select name="defeito_id" onchange="listar_causas(this.value)" id="edit_defeito_id" class="form-control selectpicker edit_defeito_id" data-live-search="true"></select>											
														</div>
													</div>
													<div class="col-lg-6" id="div_edit_novo_defeito_constatado" style="display: none;"> 										
														<label>Novo defeito constatado</label>
														<div class="m-input-icon m-input-icon--right">
															<input type="text"  id="edit_novo_defeito_constatado" class="form-control m-input" placeholder="" style="width: 90%; float: left;">
															<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-build"></i></span></span>
															<a href="#" onclick="adiciona_defeito_edicao()" style="color: #008000ad !important;font-weight: 100;float: right;">
																<i class="la la-check-circle" style="font-size: 38px;" title="Adicionar novo defeito"></i>
															</a>
														</div>										
													</div>
												</div>
												
												<div class="form-group m-form__group row" id="div_causas">
													
												</div>
												<div class="form-group m-form__group row" id="div_solucoes">
													
												</div>
												<!--<div class="form-group m-form__group row" id="div_causa_solucao">									
												</div>-->
												<div class="form-group m-form__group row" id="div_confirma_causas_solucoes">
												</div>
											</div>	
										</div>
									</div>								
									<div class="tab-pane" id="tab_atendimento" role="tabpanel" style="background: #fff !important;">
											
											<div class="m-portlet m-portlet--mobile">
												<div class="m-portlet__head" >
													<div class="m-portlet__head-caption">
														<div class="m-portlet__head-title">
															<h3 class="m-portlet__head-text">
																Atendimentos&nbsp;&nbsp;
															</h3>
															<div class="m-portlet__head-caption cadastrar_orcamento">	
																<?php if ($chamado['status_id'] != 3 && $chamado['status_id'] != 4){?>											
																	<a data-toggle="modal" href="#modal_add_subatividade" id="div_add_atendimento" class="" style="color: #ffcc00 !important; font-weight: bold;" >
																		<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo atendimento"></i>
																	</a>
																<?php } ?>
															</div>
														</div>
													</div>
												</div>
												<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" style="display: block !important; ">
													<!--begin: Datatable -->
													<table id="tabela_subatividades" class="" width="100%" >
														<thead>
															<tr>
																<th style="text-align: center;">#</th>
																<th style="text-align: center;">Observação</th>
																<th style="text-align: center;">Inicio</th>											
																<th style="text-align: center;">Fim</th>
																<th style="text-align: center;">Ações</th>
															</tr>
														</thead>
													</table>
													<!--end: Datatable -->
												</div>
											</div>				
											
										</div>	
										
										<div class="tab-pane" id="tab_anexos" role="tabpanel" style="background: #fff !important;">
											<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/uploadAtividades');?>" method="post" enctype="multipart/form-data" id="form2">
											<input type="hidden" name="atividade_id" id="id_atividade" />	
											<input type="hidden" name="chamado_id" id="chamado_id" value="<?= $chamado['id']?>" />	
											<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" style="display: block !important; ">
												<div class="form-group m-form__group row" id="div_defeitos">
													<div class="col-lg-12">
														<label>Selecione os Arquivos</label>
														<div class="m-input-icon m-input-icon--right">
															<input type="file" name="anexos_atividade[]" id="anexos_atividade" class="form-control m-input" placeholder="" multiple="" required="required" />
															<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-upload"></i></span></span>
														</div>
													</div>
												</div>
												<div class="form-group m-form__group row" id="anexos">
													
												</div>
											</div>
											<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
													<div class="m-form__actions m-form__actions--solid">
														<div class="form-group m-form__group row">
															<div class="col-lg-6">
																<?php if ($chamado['status_id'] != 3 && $chamado['status_id'] != 4){?>
																<a href="#" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase" onclick="" id="enviar1">Salvar</a>
																<?php } ?>
																<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
															</div>							
														</div>
													</div>
												</div>
											</form>
										</div>
										
									</div>
								</div>	
							</div>
						</div>	


							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
							</div>
						</div>
					</div>
				</div>

	
<div class="modal fade " id="modal_add_subatividade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/inserirSubAtividade');?>" method="post" enctype="multipart/form-data">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" >Cadastrar Novo Atendimento</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">	
					<input type="hidden" name="inicio" value="<?= date('Y-m-d H:i:s')?>">
					<input type="hidden" name="atividade_id" id="subatividade_atividade_id"">
					<input type="hidden" name="chamado_id" value="<?= $chamado['id']?>">
					<input type="hidden" name="usuario_id" value="<?= $usuario_id?>">
					<div class="col-lg-12" >
						<div class="m-portlet__body">
							<div class="row">
								<div class="col-lg-12 ">
									<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 25px;">
										<div class="m-portlet__body">
											<div class="col-lg-12">
												<label>Técnico</label>
												<div class="m-input-icon m-input-icon--right">
													<input type="text" required name="tecnico" id="tecnico" class="form-control m-input" placeholder="" required>
													<input type="hidden" required name="parceiro_id" id="subatividade_parceiro_id" class="form-control m-input" placeholder="" required>
													<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-build"></i></span></span>
												</div>
											</div>
											<div class="col-lg-12">
												<label>Endereço</label>
												<div class="m-input-icon m-input-icon--right">
													<input type="text" id="endereco_tecnico" class="form-control m-input" placeholder="" disabled="disabled"> 
													<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
												</div>
											</div>
											<div class="col-lg-12">
												<label>Cidade/UF</label>
												<div class="m-input-icon m-input-icon--right">
													<input type="text" id="cidade_tecnico" class="form-control m-input" placeholder="" disabled="disabled"> 
													<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
												</div>
											</div>
											<div class="col-lg-12" style="height: 65px">
												<label>Pessoa de Contato</label>
												<div class="m-input-icon m-input-icon--right">
													<select name="contato_id" id="contato_id" class="form-control" style="width: 90% !important;float: left;">
														<option value="">Informe o parceiro</option>
													</select>
													<button type="button" data-toggle="modal" href="#modal_add_usuario" class="btn btn-outline-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" style="float: right;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo contato">
														<i class="la la-plus "></i>
													</button>
												</div>
											</div>
											<div class="col-lg-12">
												<label>Validade Cartão</label>
												<div class="m-input-icon m-input-icon--right">
													<input type="text" id="cartao_validade" class="form-control m-input" placeholder="" disabled="disabled"> 
													<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-credit-card"></i></span></span>
												</div>
											</div>								
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 ">
									<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 25px;">
										<div class="m-portlet__body">
											<label>Observação</label>
											<div class="m-input-icon m-input-icon--right">
												<textarea name="observacao" id="subatividade_observacao" rows="8" class="form-control m-input"></textarea>
												
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
					</div>				
				</div>
				<div class="modal-footer">
					<?php if($chamado['status_id'] != 3 && $chamado['status_id'] != 4) { ?>
					<button type="submit" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase" onclick="return valida_form_subatividade()">Salvar</button>
					<?php } ?>
					<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
					<button type="button" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Fechar</button>
				</div>
			</form>
		</div>		
	</div>
</div>

<div class="modal fade " id="modal_edita_subatividade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarSubAtividade');?>" method="post" enctype="multipart/form-data">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" >Editar Atendimento</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">	
					<input type="hidden" name="id" id="subatividade_id"">
					<input type="hidden" name="chamado_id" 		value="<?= $chamado['id']?>">
					<input type="hidden" name="atividade_id" id="atendimento_atividade_id" >
					<input type="hidden" name="usuario_id" 		value="<?= $usuario_id?>">
					<div class="col-lg-12" >
						<div class="m-portlet__body">
							<div class="row">
								<div class="col-lg-12 ">
									<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 25px;">
										<div class="m-portlet__body">
											<div class="col-lg-12">
												<label>Técnico</label>
												<div class="m-input-icon m-input-icon--right">
													<input type="text" required name="tecnico" id="edit_tecnico" class="form-control m-input" placeholder="" required>
													<input type="hidden" required name="parceiro_id" id="edit_parceiro_id" class="form-control m-input" placeholder="" required>
													<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-build"></i></span></span>
												</div>
											</div>
											<div class="col-lg-12">
												<label>Endereço</label>
												<div class="m-input-icon m-input-icon--right">
													<input type="text" id="edit_endereco_tecnico" class="form-control m-input" placeholder="" disabled="disabled"> 
													<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
												</div>
											</div>
											<div class="col-lg-12">
												<label>Cidade/UF</label>
												<div class="m-input-icon m-input-icon--right">
													<input type="text" id="edit_cidade_tecnico" class="form-control m-input" placeholder="" disabled="disabled"> 
													<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
												</div>
											</div>
											<div class="col-lg-12" style="height: 65px">
												<label>Pessoa de Contato</label>
												<div class="m-input-icon m-input-icon--right">
													<select name="contato_id" id="edit_contato_id" class="form-control" style="width: 90% !important;float: left;">
													</select>
													<button type="button" data-toggle="modal" href="#modal_add_usuario" class="btn btn-outline-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" style="float: right;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo contato">
														<i class="la la-plus "></i>
													</button>
												</div>
											</div>
											<div class="col-lg-12">
												<label>Validade Cartão</label>
												<div class="m-input-icon m-input-icon--right">
													<input type="text" id="edit_cartao_validade" class="form-control m-input" placeholder="" disabled="disabled"> 
													<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-credit-card"></i></span></span>
												</div>
											</div>								
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 ">
									<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 25px;">
										<div class="m-portlet__body">
											<label>Observação</label>
											<div class="m-input-icon m-input-icon--right">
												<textarea name="observacao" id="edit_subatividade_observacao" rows="8" class="form-control m-input"></textarea>
												
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
					</div>				
				</div>
				<div class="modal-footer">
					<?php if($chamado['status_id'] != 3 && $chamado['status_id'] != 4) { ?>
					<button type="submit" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase" onclick="return valida_form_edit_subatividade()">Salvar</button>
				<?php } ?>
					<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
					<button type="button" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom" data-dismiss="modal">Fechar</button>
				</div>
			</form>
		</div>		
	</div>
</div>

<div class="modal fade" id="modal_add_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Cadastrar Novo Funcionário</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">
					<div class="form-group m-form__group col-lg-9" >
						<label for="exampleSelect1">Nome</label>							
						<input type="text" class="form-control m-input m-input--air sub" placeholder="Nome do funcionário" name="nome" id="nome" style="margin-bottom: 10px;float: left;" />
					</div>
					<div class="form-group m-form__group col-lg-3" >
						<label for="exampleSelect1">CPF</label>							
						<input type="text"  class="form-control m-input m-input--air qtd" placeholder="CPF do funcionário" name="cpf" id="cpf"  style="margin-bottom: 10px;float: right;" /> 
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="form-group m-form__group col-lg-9" >
						<label for="exampleSelect1">Email</label>							
						<input type="email" class="form-control m-input m-input--air sub" placeholder="E-mail do funcionário" name="email" id="email" style="margin-bottom: 10px;float: left;" />
					</div>
					<div class="form-group m-form__group col-lg-3" >
						<label for="exampleSelect1">Telefone</label>							
						<input type="text"  class="form-control m-input m-input--air qtd" placeholder="Telefone do funcionário" name="telefone" id="telefone"  style="margin-bottom: 10px;float: right;" /> 
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="adicionar_funcionario()">Adicionar Funcionário</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_emitir_op" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/emitirOp');?>" method="post" enctype="multipart/form-data">
		<div class="modal-content" style="width: 700px;">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Emitir ordem de pagamento</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">				
				<div class="form-group m-form__group col-lg-12" >
					<label for="exampleSelect1">E-mail </label>							
					<input type="text" class="form-control m-input m-input--air sub" placeholder="E-mail" name="email_tecnico" id="email_tecnico" style="" value="<?php echo $ordem_pagamento[0]['email_tecnico']; ?>" />
					<input type="hidden" name="ordem_pagamento_id" value="<?php echo (isset($ordem_pagamento[0]['id'])) ? $ordem_pagamento[0]['id'] : '0'; ?>" />
					<input type="hidden" name="chamado_id" value="<?=$chamado['id']?>" />
					<input type="hidden" name="acao" id="acao_emissao" value="emitir" />
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>				
				<button type="submit" class="btn btn-primary" >Enviar</button>				
			</div>
		</div>
		</form>
	</div>
</div>

<div class="modal fade" id="modal_autorizacao" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="overflow: auto !important; ">
	<div class="modal-dialog modal-lg" role="document">
		
		<div class="modal-content " style="">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Autorização de Serviço <?php 						
						if((count($autoriz_servico) >= 1 && isset($autoriz_servico[0]['dthr_solicitacao']))) {								
							echo '<input type="hidden" id="solicitacao_emitida" value="1" />';
							if($total_aceite['total'] == 0){
							echo ' - <span style="color: #ffcc00;">Solicitação Emitida</span >'; ?>
						<button type="button" class="btn btn-warning" id="libera_emissao_aut" >Enviar Solicitação Novamente</button>	
						
						<?php }elseif( $total_aceite['total'] >= 1 ){ ?>
					
						<span class="m--font-success"> - Solicitação Emitida e Resposta Técnico Realizada </span >	
						<button type="button" class="btn btn-warning" id="libera_emissao_aut" >Enviar Solicitação Novamente</button>
							
				<?php } } ?>	
				</h5>				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">												
				</button>				
			</div>
			<div class="form_autorizacao">
			<form id="form_autorizacao" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/autorizacaoServico');?>" method="post" enctype="multipart/form-data">
			<div class="modal-body">				
				<div class="row">
					<div class="col-lg-12">
						<p id="link_solicitacao" style="text-transform: lowercase !important;">
				<?=(count($autoriz_servico) > 0) ? '<B>LINK CONFIRMAÇÃO:</B> https://www.wertco.com.br/home/confirmaRecebimentoTecnico?chamado_id='.$chamado['id'].'&tecnico_id='.$autoriz_servico[0]['id'].'&opcao=1' : '' ?></p>
					</div>
				</div>
				<div class="row">
					<div class="form-group m-form__group col-lg-6" >
						<label>Técnico</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required name="tecnico" id="tecnico_autoriz" class="form-control m-input" placeholder="" >
							<input type="hidden" name="tecnico_id" id="tecnico_id" class="form-control m-input" placeholder="" >
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
						</div>
					</div>
					<div class="form-group m-form__group col-lg-6" >
						<label>E-mail Envio</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required name="email_tecnico" id="email_autoriz" class="form-control m-input" placeholder="" >							
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-send"></i></span></span>
						</div>
					</div>	
				</div>
				<div class="row">
					<div class="form-group m-form__group col-lg-4" >
						<label for="tipo">Tipo</label>							
						<select name="tipo" id="tipo" class="form-control">
							<option value="Assistência Técnica">Assistência Técnica</option>							
							<option value="Startup">Startup</option>
						</select>
					</div>
					<div class="form-group m-form__group col-lg-4" >
						<label for="Cep">Cep</label>							
						<input type="text" required class="form-control m-input m-input--air sub" name="cep_cliente" id="cep_cliente" value="<?php echo $cliente['cep']; ?>" />
					</div>
					<div class="form-group m-form__group col-lg-4" >
						<label for="Endereco">Endereço Cliente</label>
						<input type="text" required class="form-control m-input m-input--air sub" name="endereco_cliente" id="endereco_cliente" value="<?php echo $cliente['endereco']; ?>" />
						<input type="hidden" name="cliente_id" value="<?php echo $cliente['id']; ?>" >
						<input type="hidden" name="chamado_id" value="<?=$chamado['id']?>" >
						<input type="hidden" value="<?=$chamado['cliente']?>" name="razao_social">
						<input type="hidden" value="<?=$chamado['cnpj']?>" name="cnpj">
					</div>					
				</div>
				<div class="row">
					<div class="form-group m-form__group col-lg-4" >
						<label for="Bairro">Bairro</label>							
						<input type="text" required class="form-control m-input m-input--air sub" name="bairro_cliente" id="bairro_cliente" value="<?php echo $cliente['bairro']; ?>" >
					</div>
					<div class="form-group m-form__group col-lg-4" >
						<label for="Bairro">Cidade</label>							
						<input type="text"  required class="form-control m-input m-input--air sub" name="cidade_cliente" id="cidade_cliente" value="<?php echo $cliente['cidade']; ?>" >
					</div>
					<div class="form-group m-form__group col-lg-4" >
						<label for="Estado">Estado(UF)</label>							
						<input type="text" required class="form-control m-input m-input--air sub" name="estado_cliente" id="cidade_estado" value="<?php echo $cliente['estado']; ?>" >
					</div>
				</div>
				<div class="row">
					<div class="form-group m-form__group col-lg-6" >
						<label for="solicitacao">Solicitação</label>							
						<textarea required class="form-control m-input m-input--air sub" name="solicitacao" id="solicitacao"  ></textarea>
					</div>
					<div class="form-group m-form__group col-lg-6">
						<label for="Anexo">Anexo</label>							
						<input type="file" class="form-control m-input m-input--air sub" name="anexo" id="anexo_autoriz" />
					</div>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>				
				<button type="submit" class="btn btn-primary" >Enviar</button>				
			</div>
			</form>
			<?php 	if(	count($autoriz_servico) >= 1 && isset($autoriz_servico[0]['dthr_solicitacao']) 	) { 	?>
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head" >
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Solicitações Enviadas&nbsp;&nbsp;
							</h3>
							<div class="m-portlet__head-caption"></div>
						</div>					
					</div>

				</div>
				<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
					
					<!--begin: Datatable -->
					<table class="" id="tabela_atividades" width="100%">
						<thead>
							<tr>
								<th style="text-align: center;">#</th>
								<th style="text-align: center;">Técnico</th>
								<th style="text-align: center;">Empresa</th>
								<th style="text-align: center;">Email</th>
								<th style="text-align: center;">Dthr. Solicitação</th>	
								<th style="text-align: center;">Aceite/Recusa</th>	
								<th style="text-align: center;">Dthr. Aceite/Recusa</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ( $autoriz_servico as $autoriz ) { ?>
																		
								<tr>
									<td style="text-align: center;"><?php echo $autoriz['solicitacao_id']; ?></td>
									<td style="text-align: center;"><?php echo $autoriz['nome']; ?></td>	
									<td style="text-align: center;"><?php echo $autoriz['razao_social']; ?></td>
									<td style="text-align: center;"><?php echo $autoriz['email_tecnico']; ?></td>
									<td style="text-align: center;"><?php echo ($autoriz['dthr_solicitacao'] != '0000-00-00 00:00:00') ? date('d/m/Y H:i:s',	strtotime($autoriz['dthr_solicitacao'])) : ''; ?></td>
									<td style="text-align: center;">
										<?php if($autoriz['fl_aceite'] == '1' && $autoriz['dthr_aceite'] != '0000-00-00 00:00:00'){
												echo 'Aceito';
											}elseif($autoriz['fl_aceite'] == '0' && $autoriz['dthr_aceite'] != '0000-00-00 00:00:00') {
												echo 'Não Aceito'; 
											}elseif($autoriz['fl_aceite'] == NULL){
												echo 'Não visualizado';
											}
										?>
									</td>
									<td style="text-align: center;"><?php echo ($autoriz['dthr_aceite'] != '0000-00-00 00:00:00') ? date('d/m/Y H:i:s', 	strtotime($autoriz['dthr_aceite'])) : ''; ?></td>	
								</tr>
						<?php } ?>
						</tbody>
					</table>
					<!--end: Datatable -->
				</div>
			</div>	
		<?php } ?>
		</div>
		</div>
	

	</div>
</div>
<div class="modal fade" id="modal_listar_pecas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">		
		<div class="modal-content" style="width: 700px;">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title m--font-primary" >Listagem de Peças #<span class="id_solicitacao_pecas"></span></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">	
				<div class="row">			
					<div class="form-group m-form__group col-lg-6" >
						<h4 class="m--font-primary">Destinatário:</h5>
						<h6 id="solicitacaoPecasDest"></h6>
					</div>
					<div class="form-group m-form__group col-lg-6" >
						<h4 class="m--font-primary">Endereço de entrega:</h5>
						<h6 id="solicitacaoPecasEnd"></h6>
					</div>
				</div>
				<div class="row">			
					<div class="form-group m-form__group col-lg-6" >
						<h4 class="m--font-primary">Rastreio:</h5>
						<h6 id="solicitacaoPecasRastreio"></h6>
					</div>
					<div class="form-group m-form__group col-lg-6" >
						<h4 class="m--font-primary">Arquivo NFe:</h5>
						<h6 id="solicitacaoPecasNfe"></h6>
					</div>
				</div>
				<div class="row">			
					<div class="form-group m-form__group col-lg-6" >
						<h4 class="m--font-primary">Mode de Envio:</h4>
						<h6 id="txt_modo_envio"></h6>
					</div>
				</div>
				<hr/>
				<h4 class="m--font-primary" style="text-align: center;">Peças</h4>
				<hr/>
				<div class="row">			
					<div class="form-group m-form__group col-lg-12" >
						<table class="table" id="tabela_atividades" width="100%">
							<thead>
								<tr>
									<th >Descrição</th>
									<th style="text-align: center;">Quantidade</th>
								</tr>
							</thead>
						
							<tbody id="listaPecasSolicitacao">
								
							</tbody>
						</table>				
					</div>
				</div>									
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>				
			</div>
		</div>		
	</div>
</div>
<!-- Modal Andamento -->
<div class="modal fade" id="modal-andamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">		
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header" style="height: 100px;">
				
				<h5 class="modal-title modal-andamento-title" ></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<br />
				<div style="position: absolute;top: 15%;">
					<span class="m-badge m-badge--secondary" style="min-width: 10px;min-height: 10px;"></span>
					<span class="text"> Peças Solicitadas </span>
					<span class="m-badge m-badge--warning" style="min-width: 10px;min-height: 10px;"></span>
					<span class="text"> Peças Entregue </span>
					<span class="m-badge m-badge--success" style="min-width: 10px;min-height: 10px;"></span>
					<span class="text"> Peças Enviadas </span>
					<span class="m-badge m-badge--info" style="min-width:10px ;min-height: 10px;"></span>
					<span class="text"> Solicitação Concluída </span>
				</div>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="m-list-timeline">
					<div class="m-list-timeline__items">                
					</div>
				</div> 
				<hr/>
				<div class="form-group m-form__group">
					<div class="form-group m-form__group" >
						<label for="exampleSelect1">Adicionar Andamento da Solicitação</label>
						<textarea type="text" class="form-control m-input m-input--air" id="andamento_texto"></textarea>					
					</div>
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" id ="adicionar_andamento" class="btn btn-primary" status_id="" solicitacao_id="">Adicionar Andamento</button>
			</div>
		</div>
	</div>		
</div>

<!-- Modal Andamento -->
<div class="modal fade" id="modal-copiar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">		
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header" style="height: 100px;">				
				<h5 class="modal-title modal-andamento-title" >Copiar Atividade #<span id="indice_atividade"></span></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<br>				
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
						<label>Modelo</label>
						<select onchange="change_modelo(this)" name="modelo_id_c" id="modelo_id_c" class="form-control">
							<?php foreach($modelos as $modelo) { ?>
								<option value="<?=$modelo['id'] ?>"><?=strtoupper($modelo['modelo']) ?></option>
							<?php } ?>	
							<option value="0">Outro</option>
						</select>
					</div>
				</div>
				<!--<div class="col-lg-6" style="display: none;">	
					<label>Novo equipamento</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="modelo_manual" id="modelo_manual" class="form-control m-input" placeholder="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-build"></i></span></span>
					</div>
				</div>-->
				<div class="form-group m-form__group row">
					<div class="col-lg-6">
						<label>Número de Série</label>
						<input class="form-control" name="nr_serie_c" id="nr_serie_c" />
					</div>					
					<div class="col-lg-6">
						<label>Observação</label>
						<textarea class="form-control"  name="obs_c" id="obs_c"></textarea>	
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" id ="copiar_atividade" class="btn btn-primary" status_id="" ativiade_id="">Copiar Atividade</button>
			</div>
		</div>
	</div>		
</div>

<?php if ($this->session->flashdata('erro') == TRUE){?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, Tente novamente!',
			'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "OK!",
               	text: 'Chamado editado com sucesso',
               	type: "success"
        }); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>
<?php if ($this->session->flashdata('nenhuma') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "Sem alterações!",
               	text: 'Não houve nenhuma alteração no chamado',
               	type: "warning"
        }); 
	</script>	
<?php unset($_SESSION['nenhuma']); } ?>
<?php if ($this->session->flashdata('erro_atividade') == TRUE){?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, Tente novamente!',
			'error'
		);
	</script>
<?php unset($_SESSION['erro_atividade']);} ?>
<?php if ($this->session->flashdata('sucesso_atividade') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "OK!",
               	text: 'Atividade aberta com sucesso',
               	type: "success"
        }).then(function() {
			editar_atividade(<?php echo $this->session->flashdata('atividade_id'); ?>);
		});  
	</script>	
<?php 	unset($_SESSION['sucesso_atividade']); 
		unset($_SESSION['atividade_id']);	
	} ?>
<?php if ($this->session->flashdata('edit_erro_atividade') == TRUE){?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, Tente novamente!',
			'error'
		);
	</script>
<?php unset($_SESSION['edit_erro_atividade']);} ?>
<?php if ($this->session->flashdata('edit_sucesso_atividade') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "OK!",
               	text: 'Atividade alterada com sucesso',
               	type: "success"
        }).then(function() {
			editar_atividade(<?php echo $this->session->flashdata('atividade_id'); ?>);
		}); ; 
	</script>	
<?php 	unset($_SESSION['edit_sucesso_atividade']);
		unset($_SESSION['atividade_id']);
 } ?>
<?php if ($this->session->flashdata('edit_nenhum_atividade') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "Sem alterações!",
               	text: 'Não houve nenhuma alteração na atividade',
               	type: "warning"
        }); 
	</script>	
<?php unset($_SESSION['edit_nenhum_atividade']); } ?>
<?php if ($this->session->flashdata('erro_subatividade') == TRUE){?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, Tente novamente!',
			'error'
		);
	</script>
<?php unset($_SESSION['erro_subatividade']);} ?>
<?php if ($this->session->flashdata('sucesso_subatividade') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "OK!",
               	text: 'Atendimento aberto com sucesso',
               	type: "success"
        }).then(function() {
			editar_atividade(<?php echo $this->session->flashdata('atividade_id'); ?>);
		}); 
	</script>	
<?php 	unset($_SESSION['sucesso_subatividade']);
		unset($_SESSION['atividade_id']); } ?>
<?php if ($this->session->flashdata('edit_erro_subatividade') == TRUE){?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, Tente novamente!',
			'error'
		);
	</script>
<?php unset($_SESSION['edit_erro_subatividade']);} ?>
<?php if ($this->session->flashdata('edit_sucesso_subatividade') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "OK!",
               	text: 'Atendimento alterado com sucesso',
               	type: "success"
        }).then(function() {
			editar_atividade(<?php echo $this->session->flashdata('atividade_id'); ?>);
		}); 
	</script>	
<?php 	unset($_SESSION['edit_sucesso_subatividade']); 
		unset($_SESSION['atividade_id']); } ?>
<?php if ($this->session->flashdata('edit_nenhum_subatividade') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "Sem alterações!",
               	text: 'Não houve nenhuma alteração no atendimento',
               	type: "warning"
        }).then(function() {
			editar_atividade(<?php echo $this->session->flashdata('atividade_id'); ?>);
		}); 
	</script>	
<?php 	unset($_SESSION['edit_nenhum_subatividade']);
		unset($_SESSION['atividade_id']); } ?>
<?php if ($this->session->flashdata('upload_atividade') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "OK!",
               	text: 'Anexos salvos com sucesso',
               	type: "success"
        }).then(function() {
			editar_atividade(<?php echo $this->session->flashdata('atividade_id'); ?>);
		});  
	</script>	
<?php 	unset($_SESSION['upload_atividade']); 
		unset($_SESSION['atividade_id']);	} ?>
<?php if ($this->session->flashdata('upload_atividade_erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "Erro!",
               	text: 'Não foi possivel salvar o upload',
               	type: "error"
        }); 
	</script>	
<?php unset($_SESSION['upload_atividade_erro']); } ?>
<?php if ($this->session->flashdata('sucesso_op') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "OK!",
               	text: 'Ordem de pagamento salva!',
               	type: "success"
        }); 
	</script>	
<?php unset($_SESSION['sucesso_op']); } ?>
<?php if ($this->session->flashdata('erro_op') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "Ops!",
               	text: 'Erro em gerar ordem de pagamento, entre em contato com o webmaster!',
               	type: "error"
        }); 
	</script>	
<?php unset($_SESSION['sucesso_op']); } ?>
<?php if ($this->session->flashdata('sucesso_pecas') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "OK!",
               	text: 'Solicitação de peças enviada com sucesso!',
               	type: "success"
        }); 
	</script>	
<?php unset($_SESSION['sucesso_pecas']); } ?>
<?php if ($this->session->flashdata('erro_pecas') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
               	title: "Ops!",
               	text: 'Erro em gerar a solicitação de peças, entre em contato com o webmaster!',
               	type: "error"
        }); 
	</script>	
<?php unset($_SESSION['erro_op']); } ?>