<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head" >
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Chamados&nbsp;&nbsp;
					</h3>
					<div class="m-portlet__head-caption cadastrar_orcamento">													
						<a href="<?php echo base_url('AreaAdministrador/cadastraChamado')?>" class="" style="color: #ffcc00; font-weight: bold;" >
							<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo chamado"></i>
						</a>
					</div>
				</div>
				
			</div>
			
			<div class="m-list__content">
                <div class="m-list-badge" style="margin-top: 25px;">
                	<!--<span class="m-list-badge__item">&nbsp;Aberto&nbsp;&nbsp;<span class="m-badge m-badge--secondary"></span></span>				    	
					<span class="m-list-badge__item">&nbsp;Em andamento&nbsp;&nbsp;<span class="m-badge m-badge--info"></span></span>
					<span class="m-list-badge__item">&nbsp;Fechado&nbsp;&nbsp;<span class="m-badge m-badge--success"></span></span>
				-->
					
				</div>
			</div>		
			<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: -25px;" data-toggle="m-tooltip" data-placement="top" title="Tour Virtual" id="tour_virtual">
				<i class="la la-info"></i>
			</a>
		
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center; width: 5%;"># Chamado</th>
						<th style="text-align: center; width: 45%;">Cliente</th>						
						<th style="text-align: center; width: 10%;">Situação</th>						
						<th style="text-align: center; width: 10%;">Tipo</th>	
						<th style="text-align: center; width: 20%;">Data Criação</th>
						<th style="text-align: center; width: 10%;">Ações</th>
					</tr>
				</thead>
				<tbody>					
					<?php foreach($dados as $dado){
						

						?>
						<tr>
							<td style="width: 5%;text-align: center;"><?php echo $dado['id']; ?></td>
							<td style="width: 40%;text-align: center;"><?php echo $dado['cliente']; ?></td>											
							<td style="width: 10%;text-align: center; text-transform: capitalize;">
								<?php if( $dado['status_id'] == 1 ){
									$status = "btn m-btn--pill m-btn--air btn-success";
								}elseif( $dado['status_id'] == 2 ){
									$status = "btn m-btn--pill m-btn--air btn-info";
								}elseif( $dado['status_id'] == 3 ){
									$status = "btn m-btn--pill m-btn--air btn-warning";
								}elseif( $dado['status_id'] == 4 ){
									$status = "btn m-btn--pill m-btn--air btn-danger";
								}
								?>
								<button class="status <?php echo $status; ?>" status="<?php echo $dado['status'];?>" title="Status do chamado" onclick="status(<?php echo $dado['id']; ?>);">
									<?php echo ucfirst($dado['status']); ?>								
								</button>
							</td>
							<td style="width: 40%;text-align: center;"><?php echo $dado['tipo']; ?></td>	
							<td style="width: 10%;text-align: center;">
								<?php echo date('d/m/Y H:i:s',strtotime($dado['inicio'])); ?>							
							</td>							
							<td data-field="Actions" class="m-datatable__cell " style="width: 20%;text-align: center !important;">
								<a href="<?php echo base_url('AreaAdministrador/editaChamado/'.$dado['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" >
									<i class="la la-edit editar"></i>
								</a>
								<a href="<?php echo base_url('AreaAdministrador/geraChamadoPdf/'.$dado['id'].'/post')?>" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
									<i class="la la-print"></i>
								</a>
								<a href="<?php echo base_url('AreaAdministrador/geraChamadoCompletoPdf/'.$dado['id'].'/post')?>" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
									<i class="la la-file-pdf-o"></i>
								</a>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group status_orcamento_" >
						<label for="exampleSelect1">Situação do Chamado</label>
						<select class="form-control m-input m-input--air" id="status_chamado">
							<option value="">Selecione o Status do Pedido</option>						
							<option value="1">AGUARDANDO TÉCNICO</option>
							<option value="2">AGUARDANDO PEÇAS</option>
							<option value="3">FECHADO</option>
							<option value="4">CANCELADO</option>
							<option value="5">AGUARDANDO PARTIDA INICIAL</option>
							<option value="6">EM OBSERVAÇÃO</option>
							<option value="7">AGUARDANDO DOCUMENTO</option>
							<option value="8">AGUARDANDO CLIENTE</option>
							<option value="9">SOLICITAÇÃO PENDENTE</option>
							<option value="10">ABERTO PELO CLIENTE</option>
							<option value="11">AGUARDANDO PAGAMENTO</option>
							<option value="12">PENDÊNCIA DE OS</option>
						</select>
					</div>					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" class="btn btn-primary" chamado_id="">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>	

	<!-- Modal Finalizar pedido -->
	<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-finalizar-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" >
							<label for="exampleSelect1">E-mail do cliente</label>
							<input type="email" style="text-transform: lowercase;" class="form-control m-input--air" value="" id="email" placeholder="E-mail" />
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="finalizar_pedido" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
			</div>
		</div>
	</div>
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){

				if($this->session->flashdata('pedidoPdf')){ ?>
					<script type="text/javascript">
						window.open('http://www.wertco.com.br/pedidos/<?php echo $this->session->flashdata('pedidoPdf'); ?>');
					</script>	
			<?php	} ?>		 
		<script type="text/javascript">
			
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaAdministrador/pedidos';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>