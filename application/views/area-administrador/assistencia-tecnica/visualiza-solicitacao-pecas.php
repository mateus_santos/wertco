<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-building-o"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Solicitação  de Peças #<?php  echo $dados[0]['id']; ?>&nbsp;&nbsp;
					</h3>
					<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('AreaAssistencia/solicitacaoPecas'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
					<h3 class="m-portlet__head-text">	
						<a href="<?php echo base_url('AreaAssistencia/solicitacaoPecas'); ?>">Voltar</a>
					</h3>
				</div>			
			</div>
			
		</div>
		<div class="m-portlet__body">
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>Descrição/Observação: 
						<small class="text-muted"> <?php echo $dados[0]['descricao'];?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>Cliente: 
						<small class="text-muted"> <?php echo $dados[0]['cliente'];?></small>
					</h5>
				</div>
				<input type="hidden" value="<?php echo $dados[0]['id'];?>" id="empresa_id">
				
				<div class="col-lg-4 col-xs-12">
					<h5>Destinatário: 
						<small class="text-muted"> <?php echo $dados[0]['destinatario'];?></small>
					</h5>
				</div>				
			</div>
			<hr/>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>Endereço de Entrega: 
						<small class="text-muted"> <?php echo $dados[0]['endereco_entrega'];?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5># Chamado: 
						<small class="text-muted"> <a href="<?php echo base_url('AreaAssistencia/geraChamadoPdf/'.$dados[0]['chamado_id'].'/post')?>" target="_blank"><?php echo $dados[0]['chamado_id'];?></a></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						Status da Solicitação: <small class="text-muted"> <?php echo $dados[0]['status'];?></small>
					</h5>
				</div>				
			</div>
			<hr/>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Data/Hora Solicitação: <small class="text-muted"> <?php echo date('d/m/Y H:i:s', strtotime($dados[0]['dthr_solicitacao']));?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Nota Fiscal Vinculada: <small class="text-muted"> <a target="_blank" href="<?php echo base_url('nfe-assistencia/'.$dados[0]['arquivo_nfe']); ?>"><?php echo $dados[0]['nr_nf'];?></a></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Data Envio: <small class="text-muted"> 	<?php echo ($dados[0]['dthr_envio'] == 'null' || $dados[0]['dthr_envio'] == '') ? '' : date('d/m/Y', strtotime($dados[0]['dthr_envio']));?> </small>
					</h5>
				</div>							
			</div>
			<hr/>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Modo Envio: <small class="text-muted"> <?php echo $dados[0]['modo_envio'];?></small>
					</h5>
				</div>
			</div>
			
		</div>
	</div>
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>Peças</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">			
			<table class="table m-table m-table--head-separator-warning">
			  	<thead>
			    	<tr>
			    		<th>#</th>
			      		<th>Peça</th>
			      		<th>Qtd</th>			      		
			      		<th>Custo (R$)</th>
			    	</tr>
			  	</thead>
			  	<tbody>

			  		<?php $i=1; $aux = 0;  foreach($dados as $dado){  ?>
			    	<tr>
			    		<td><?php echo $i; ?></td>
				      	<td scope="row"><?php echo $dado['pecas'];?></td>
				      	<td><?php echo $dado['qtd']; ?></td>
				      	<td>
				      		<input type="text" class="form-control m-input custo" name="custo" id="custo-<?php echo $i;?>" maxlength="10" solicitacao_pecas_id="<?php echo $dado['solicitacao_itens_id']; ?>" value="<?php echo $dado['custo']; ?>" solicitacao_id="<?php  echo $dados[0]['id']; ?>" status_solicitacao_id="<?php  echo $dados[0]['status_id']; ?>" descricao='<?php echo $dado['pecas'];?>' />
				      	</td>
				    </tr>
				    <?php  if( $aux != $dado['solicitacao_itens_id'] ){
				    			$n = 1;		
				    			foreach ($itens as $item) {

				    				if ($item['solicitacao_peca_item_id'] == $dado['solicitacao_itens_id']) {
				    					
				    		?>
				    	<tr>
				    		<td colspan="4" style="text-align: center;background: #f0f0f0;">
				    			<div style="width: 35%; margin: 0 auto;">
				    			<span style="float: left;margin-top: 10px;"><?=$n?> -</span> 
				    			<input class="form-control nr_serie" type="text" name="nr_serie" id="<?=$item['id']?>" placeholder="Nr. Série"  solicitacao_peca_item_id="<?php echo $item['solicitacao_peca_item_id']; ?>" style="width: 60%;margin-left: 35px;" value="<?=$item['nr_serie']?>" />
				    			</div>
				    		</td>
				    	</tr>
				    <?php $n++;}  } 	} ?>
				    
				    <?php $aux = $dado['solicitacao_itens_id'];  $i++; } ?> 
			  	</tbody>
			</table>			
		</div>
	</div>

</div>

<div class="modal fade" id="modal_add_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

		<div class="modal-dialog modal-dialog-centered" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h5 class="modal-title modal-andamento-title" >Cadastrar Novo Funcionário</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body" style="width: 750px;">

					

					<div class="form-group m-form__group row">

						<div class="form-group m-form__group col-lg-9" >

							<label for="exampleSelect1">Nome</label>							

							<input type="text" class="form-control m-input m-input--air sub" placeholder="Nome do funcionário" name="nome" id="nome" style="margin-bottom: 10px;float: left;" />

						</div>

						<div class="form-group m-form__group col-lg-3" >

							<label for="exampleSelect1">CPF</label>							

							<input type="text"  class="form-control m-input m-input--air qtd" placeholder="CPF do funcionário" name="cpf" id="cpf"  style="margin-bottom: 10px;float: right;" />

						</div>

					</div>
					
					<div class="form-group m-form__group row">

						<div class="form-group m-form__group col-lg-9" >

							<label for="exampleSelect1">Email</label>							

							<input type="email" class="form-control m-input m-input--air sub" placeholder="E-mail do funcionário" name="email" id="email" style="margin-bottom: 10px;float: left;" />

						</div>

						<div class="form-group m-form__group col-lg-3" >

							<label for="exampleSelect1">Telefone</label>							

							<input type="text"  class="form-control m-input m-input--air qtd" placeholder="Telefone do funcionário" name="telefone" id="telefone"  style="margin-bottom: 10px;float: right;" /> 

						</div>

					</div>

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>

					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="adicionar_funcionario()">Adicionar Funcionário</button>

				</div>

			</div>

		</div>

	</div>