<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-diagram" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Relatório de Chamados
					</h3>
					
				</div>

			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
			<div class="form-group m-form__group row">				
				<div class="col-lg-4">
					<label class="">Status:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="status_id" placeholder="Status Chamado" id="status_id" >
                        	<option value="">Selecione um status</option>
                        <?php 	foreach($status as $sts ){	?>       
                        	<option value="<?php echo $sts['id']; ?>"><?php echo $sts['descricao']; ?></option>
                        <?php } ?>
                       	</select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
					</div>							
				</div>					
				<div class="col-lg-4">
					<label>Técnico:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" 	 name="tecnico" 	id="tecnico" 	class="form-control m-input" placeholder="Selecione um técnico" />
						<input type="hidden" name="tecnico_id" 	id="tecnico_id" class="form-control m-input" placeholder="" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-4">
					<label>Posto:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" 		name="posto" 	id="posto" 		class="form-control m-input" placeholder="Selecione um cliente" />
						<input type="hidden" 	name="posto_id" id="posto_id" 	class="form-control m-input" placeholder="" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
					</div>
				</div>
			</div>			
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="button" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>							
					</div>
				</div>
			</div>	
		</div>						
	</div>
	<!--end::Portlet-->	

	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" style="display: none;">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--danger">
						<span>
							Chamados
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->			
		</div>
	</div>

	
	<!--end::Portlet-->	
</div>	
<div class="modal fade" id="m_lista_anexo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_lista_anexo_title" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
				<div class="row anexos">
					
				</div>				
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
