<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-diagram" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Relatório de Chamados de Clientes
					</h3>					
				</div>
			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >		
			<form class="" action="<?php echo base_url('areaAdministrador/gerarRelatorioChamadosCliente');?>" method="post" target="_blank">	
			<div class="form-group m-form__group row filtros">
				<div class="col-lg-12">
					<label>Posto:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" id="posto" class="form-control" placeholder="Pesquise por um posto" />
						<input type="hidden" name="posto_id" id="posto_id"  />
						<input type="hidden" name="tp" id="tp" value="pdf"  />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
					</div>
				</div>
			</div>			
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>							
					</div>
				</div>
			</div>			
			</form>	
		</div>						
	</div>	
</div>	
