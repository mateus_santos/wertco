<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 10px;
 	color: #333;
 	width: 100%;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px;
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
	background-color: #fff !important;
}
table{	
	padding: 0;
	width: 100%;
	/*border: 1px solid #ffcc00;*/
	
}
table tr{	
	
}

table td{
	padding: 2px !important;
	/*border-bottom: 1px solid #f0f0f0;
	border-right: 1px solid #f0f0f0;*/
	font-size: 8px;	
	text-transform: uppercase;
}

table#encerrante > tbody > tr > td {
	padding: 10px; 
	border-bottom: 1px solid #000;
	border-right: 1px solid #000;
	height: 10px;
}

</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">
			<table border="0">
				<tr>
					<td style="width: 33%;text-align: left;"><img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;"></td>
					<td style="width: 33%;text-align: center;"><b>Formulário de Startup</b></td>
					<td style="width: 33%;text-align: right;">
											FAT 03<br>
											REVISÃO: 02<br>
											30/10/2018<br>
					</td>						
				</tr>	
			</table>
			<br><br>			
			<table >
				<tr>
					<td><b>Modelo: 	<?php echo $modelos[0]; 	?></b></td>
					<td><b>Nr. Série: 	<?php echo $nr_series[0]; ?></b></td>
					<td style="text-align: right;"><b>Data: ___/___/______</b></td>
				</tr>	
			</table>
			<hr style="border-bottom: 1px solid #000; height: 1px; background-color: #000; " />
			<table cellspacing="0" style="margin-top: 5px; margin-bottom: 5px;">
				<tbody>
				
				<tr>	
					<td colspan="2"><b>Razão Social: </b><?php echo $empresa[0]['razao_social']; ?></td>
					<td><b>COD:</b><?php echo $empresa[0]['id']; ?></td>
				</tr>
				<tr>
					<td><b>CNPJ: </b><?php echo $empresa[0]['cnpj']; ?></td>
					<td><b>I.E: </b><?php echo $empresa[0]['insc_estadual']; ?></td>					
					<td></td>
				</tr>								
				<tr>
					<td colspan="2"><b>Endereço:</b> <?php echo $empresa[0]['endereco']; ?></td>										
					<td><b>Bairro:</b> <?php echo $empresa[0]['bairro']; ?></td>	
				</tr>
				<tr>
					<td><b>Cidade:</b><?php echo $empresa[0]['cidade']; ?></td>
					<td><b>UF:</b><?php echo $empresa[0]['estado']; ?></td>					
					<td><b>CEP:</b><?php echo $empresa[0]['cep']; ?></td>							
				</tr>
				<tr>
					<td><b>Telefone: </b><?php echo $empresa[0]['telefone']; ?> </td>					
					<td><b>E-mail: </b><?php echo strtolower($empresa[0]['email']); ?></td>		
				</tr>
				
			</tbody>
		</table>
		</div>
		<div class="corpo">	
			<?php $i=0; $pagina = 1;

				foreach($modelos as $modelo){
				if( count($modelos) > 1 && $i > 0){ ?>

				
				<div class="cabecalho">			
				<table border="0">
					<tr>
						<td style="width: 33%;text-align: left;"><img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;"></td>
						<td style="width: 33%;text-align: center;"><b>Formulário de Startup</b></td>
						<td style="width: 33%;text-align: right;">
												FAT 03<br>
												REVISÃO: 02<br>
												30/10/2018<br>
						</td>						
					</tr>
				</table>	

			<table>
				<tr>
					<td><b>Modelo: 	<?php echo $modelo; 	?></b></td>
					<td style="text-align: center;"><b>Nr. Série: 	<?php echo $nr_series[$i]; ?></b></td>
					<td style="text-align: right;"><b>Data:  ___/___/______</b></td>
				</tr>	
			</table>
			<hr style="border-bottom: 1px solid #000; height: 1px; background-color: #000;" />
			<table cellspacing="0" style="margin-top: 5px; margin-bottom: 5px;">
				<tbody>
				
				<tr>	
					<td colspan="2"><b>Razão Social: </b><?php echo $empresa[0]['razao_social']; ?></td>
					<td><b>COD:</b><?php echo $empresa[0]['id']; ?></td>
				</tr>
				<tr>
					<td><b>CNPJ: </b><?php echo $empresa[0]['cnpj']; ?></td>
					<td><b>I.E: </b><?php echo $empresa[0]['insc_estadual']; ?></td>					
					<td></td>
				</tr>								
				<tr>
					<td><b>Endereço:</b> <?php echo $empresa[0]['endereco']; ?></td>
					<td><b>Bairro:</b> <?php echo $empresa[0]['bairro']; ?></td>					
					<td></td>	
				</tr>
				<tr>
					<td><b>Cidade:</b><?php echo $empresa[0]['cidade']; ?></td>
					<td><b>UF:</b><?php echo $empresa[0]['estado']; ?></td>					
					<td><b>CEP:</b><?php echo $empresa[0]['cep']; ?></td>							
				</tr>
				<tr>
					<td><b>Telefone: </b><?php echo $empresa[0]['telefone']; ?> </td>					
					<td><b>E-mail: </b><?php echo strtolower($empresa[0]['email']); ?></td>		
				</tr>
				
			</tbody>
		</table>
		</div>
			 <?php } ?>
			 <table border="0" bgcolor="#ffcc00" style="color: #000;padding-top: 175px;">
				<tr>
					<td style="text-align: center;">
						<b>VALIDAÇÃO</b>
					</td>
				</tr>
			</table>
			<table border="0">
				<tr>
					<td>
						<b>ATENÇÃO:</b> Para a validação da Garantia de Fábrica, é obrigatório que o Start-up seja realizado por um Mantenedor Credenciado
pela Wertco e que atenda às condições mínimas de instalação
					</td>
				</tr>					
			</table>
			<table border="0" bgcolor="#ffcc00" style="color: #000;">
				<tr>
					<td style="text-align: center;">
						<b>IDENTIFICAÇÃO DO MANTENEDOR (credenciado)</b>
					</td>
				</tr>
			</table>
			<table border="0">
				<tr>
					<td width="5%">						
						EMPRESA:
					</td>
					<td width="45%" style="border-bottom: 1px solid #000;"></td>
					<td width="5%">TÉCNICO: </td>
					<td width="45%"style="border-bottom: 1px solid #000;"></td>
				</tr>
				<tr>
					<td width="5%">						
						TELEFONE:
					</td>
					<td width="45%" style="border-bottom: 1px solid #000;"></td>
					<td width="5%">CELULAR:</td>
					<td width="45%" style="border-bottom: 1px solid #000;"></td>
				</tr>					
			</table>
			<table border="0" bgcolor="#ffcc00" style="color: #000;">
				<tr>
					<td style="text-align: center;">
						<b>IDENTIFICAÇÃO DA INSTALADORA</b>
					</td>
				</tr>
			</table>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="5%">EMPRESA:</td>
					<td width="45%" style="border-bottom: 1px solid #000;" colspan="3"></td>
					
					<td width="5%">TÉCNICO: </td>
					<td width="45%"style="border-bottom: 1px solid #000;"></td>
				</tr>
			</table>		
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="5%">TELEFONE:</td>
					<td width="15%" style="border-bottom: 1px solid #000;"></td>
					<td width="5%" >CELULAR:</td>
					<td width="19%" style="border-bottom: 1px solid #000;"></td>
					<td width="6%">E-MAIL:</td>
					<td width="40%"style="border-bottom: 1px solid #000;"></td>
				</tr>
			</table>
			<table border="0" bgcolor="#ffcc00" style="color: #000;">
				<tr>
					<td style="text-align: center;">
						<b>VERIFICAÇÕES DA INSTALAÇÃO</b>
					</td>
				</tr>
			</table>
			<table border="0" cellspacing="2" cellpadding="0">
				<tr>
					<td style="width: 5% !important;"><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /></td>
					<td style="width: 45%;"> Ancoragem da Bomba </td>					
					<td style="width: 5% !important;"><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;"/></td>					
					<td style="width: 45% !important;"> Disjuntor tripolar (motores): __________________A </td>					
				</tr>
				<tr>
					<td  ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /></td>
					<td > Instalações Elétricas </td>					
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /></td>					
					<td > Disjuntor (Cabeça Eletrônica): _________________A </td>					
				</tr>
				<tr>
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /> </td>
					<td > Aterramento _____________Ohms </td>					
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /> </td>					
					<td > Quadro de força com identifação das bombas</td>					
				</tr>
				<tr>
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /></td>
					<td > Sump da Bomba </td>					
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /> </td>					
					<td > Capacidade do tanque (L): ___________________ </td>					
				</tr>
				<tr>
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /></td>
					<td > <b>Serviço não executado</b> </td>					
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /></td>					
					<td >Check Valve externa (Nova)</td>					
				</tr>

			</table>		
			<table>
				<tr>
					<td style="width: 11%;">Observação:</td>
					<td style="width: 89%;border-bottom: 1px solid #000;"></td>					
									
				</tr>
			</table>
			<table border="0" bgcolor="#ffcc00" style="color: #000;">
				<tr>
					<td style="text-align: center;">
						<b>VERIFICAÇÕES DOS EQUIPAMENTOS</b>
					</td>
				</tr>
			</table>
			<table border="0" cellspacing="1" cellpadding="0">

				<tr>
					<td style="width: 5% !important;"><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /></td>
					<td style="width: 45%;"> Programação dos parâmetros </td>					
					<td style="width: 5% !important;"><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;"/></td>					
					<td style="width: 45% !important;"> Verificação da correia </td>					
				</tr>
				<tr>
					<td  ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /></td>
					<td > Bicos e Mangueiras </td>					
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /></td>					
					<td > Treinamento de operador (conforme o manual de serviços) </td>					
				</tr>
				<tr>
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /> </td>
					<td > Adesivos </td>					
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /> </td>					
					<td > Manual do proprietário </td>					
				</tr>
				<tr>
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /></td>
					<td > Densímetro para bicos Etanol </td>					
					<td ><img src="./bootstrap/img/selecao.jpg" style="width: 15px !important;" /> </td>					
					<td > Periféricos Ligados a Bomba (Swivel e Breakaway) </td>					
				</tr>
			</table>

			<table border="0" bgcolor="#ffcc00" style="color: #000;">
				<tr>
					<td style="text-align: center;">
						<b>VERIFICAÇÃO DE FUNCIONAMENTO</b>
					</td>
				</tr>
			</table>
			<table id="encerrante">
				<thead>
					<tr>
						<th>Bico</th>
						<th>Combustível</th>
						<th>Encerrante</th>
						<th>Aferição Lenta</th>
						<th>Aferição Rápida</th>
						<th>Vazão</th>
					</tr>					
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>	
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>	
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>	
				</tbody>
			</table>
			<table border="0" bgcolor="#ffcc00" style="color: #000;">
				<tr>
					<td style="text-align: center;">
						<b>VERSÕES</b>
					</td>
				</tr>
			</table>
			<table border="0" style="margin-top: 10px;">
				<tr>
					<td colspan="4"><b>Consulte as versões com o suporte técnico da WERTCO.</b></td>

				</tr>
				<tr>
					<td>Controlador:__________________ </td>
					<td>Indicador:__________________ </td>
					<td>Transdutor:__________________ </td>
					<td>Interface:__________________ </td>
				</tr>	
				
			</table>
			<table border="0" bgcolor="#ffcc00" style="color: #000;">
				<tr>
					<td style="text-align: center;">
						<b>ASSINATURAS</b>
					</td>
				</tr>
			</table>
			<table style="margin-top: 20px;margin-bottom: 45px;">
				<tr>
					<td style="text-align: left;width: 40%;">_______________________________________</td>
					<td width="20%"></td>	
					<td style="text-align: left;width: 40%;padding-left: 60px !important;">_______________________________________</td>	
				</tr>
				<tr>
					<td style="text-align: left;">Assinatura responsável do posto</td>	
					<td></td>
					<td style="padding-left: 60px !important;text-align: left;">Assinatura mantenedor / técnico</td>	
				</tr>	
				<tr>
					<td style="text-align: left;">CPF:</td>	
					<td></td>
					<td style="padding-left: 60px !important;text-align: left;">CPF:</td>	
				</tr>	
			</table>
			
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			
			<p style="text-align: right;border-radius: 10px 10px 10px 10px solid #000;">\\192.168.88.1\dados\qualidade\assistencia tecnica</p>
			
			<?php $i++;$pagina++; 
				if(  $i < count($modelos) ){
			?>
			<div style="PAGE-BREAK-AFTER: always"></div> 
		<?php } }?>
		</div>
	</div>	
</body>
</html>