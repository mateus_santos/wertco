<table class="" id="html_table" width="100%">
<thead>
	<tr>
		<th style="width: 5%;"># Chamado</th>
		<th style="width: 30%;">Cliente</th>						
		<th style="width: 15%;">Situação</th>						
		<th style="width: 25%;">Técnicos</th>	
		<th style="width: 15%;">Data Criação</th>
		<th style="width: 10%;">Ações</th>
	</tr>
</thead>
<tbody>					
	<?php foreach($dados as $dado){	?>
		<tr>
			<td style="text-align: center;"><?php echo $dado['id']; ?></td>
			<td style=""><?php echo $dado['clientes']; ?></td>											
			<td style="text-transform: capitalize;">
				<?php if( $dado['status_id'] == 1 ){
					$status = "btn m-btn--pill m-btn--air btn-success";
				}elseif( $dado['status_id'] == 2 ){
					$status = "btn m-btn--pill m-btn--air btn-info";
				}elseif( $dado['status_id'] == 3 ){
					$status = "btn m-btn--pill m-btn--air btn-warning";
				}elseif( $dado['status_id'] == 4 ){
					$status = "btn m-btn--pill m-btn--air btn-danger";
				}else{
					$status = "btn m-btn--pill m-btn--air btn-primary";
				}

				?>
				<button class="status <?php echo $status; ?>" status="<?php echo $dado['status'];?>" title="Status do chamado" onclick="status(<?php echo $dado['id']; ?>);">
					<?php echo ucfirst($dado['status']); ?>								
				</button>
			</td>
			<td style=""><?php echo $dado['tecnicos']; ?></td>	
			<td style="">
				<?php echo date('d/m/Y H:i:s',strtotime($dado['inicio'])); ?>							
			</td>							
			<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
				<a href="<?php echo base_url('AreaAdministrador/editaChamado/'.$dado['id'])?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-edit"></i>
				</a>
				<a href="<?php echo base_url('AreaAdministrador/geraChamadoPdf/'.$dado['id'].'/post')?>" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-print"></i>
				</a>
				<a href="<?php echo base_url('AreaAdministrador/geraChamadoCompletoPdf/'.$dado['id'].'/post')?>" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" target="blank">
					<i class="la la-file-pdf-o"></i>
				</a>
				<a onclick="listaAnexos(<?php echo $dado['id']; ?>);" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill">
					<i class="flaticon-tool-1"></i>
				</a>
			</td>
		</tr>
		<?php } ?>
	</tbody>
</table>