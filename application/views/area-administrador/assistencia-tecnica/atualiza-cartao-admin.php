<?php 	//$nova_data = "+".$dados->periodo_renovacao." month"; ?>
<!--begin::Base Scripts -->        
<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Atualizar data vencimento cartão técnico</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group m-form__group row">
					<div class="col-lg-12">
						<label>Cartão Técnico</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" id="tecnico" class="form-control m-input" placeholder="Selecione um técnico"> 
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
						</div>
					</div>
					<label class="col-form-label col-lg-12 col-sm-12">Validade/Última Atualização</label>
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="input-group">
							<input type="text" class="form-control m-input" name="date" placeholder="Selecione uma data" id="ultima_atualizacao" value="" disabled="disabled">
							<input type="hidden" class="form-control m-input" name="vencimento" placeholder="Selecione uma data" id="vencimento" value="<?php //echo date('d/m/y',strtotime($nova_data, strtotime($dados->ultima_atualizacao))); ?>" disabled="disabled">
							<input type="hidden" class="form-control m-input" name="vencimento" placeholder="Selecione uma data" id="maior_vencimento" value="<?php //echo (strtotime(date('Y-m-d',strtotime($dados->ultima_atualizacao))) < strtotime(date('Y-m-d'))) ? 0 : 1; ?>" disabled="disabled">	
							<input id="tagMecanico" type="hidden" maxlength="16" class="form-control m-input" placeholder="Digite a tag" value="<?php //echo $dados->tag; ?>" disabled="disabled">
							<input type="hidden" name="diff" id="diferenca_dias" value="<?php //echo $diferenca_dias->d; ?>"> 
							<input type="hidden" name="diff" id="diferenca_mes"  value="<?php //echo $diferenca_dias->m; ?>"> 
							<input type="hidden" name="diff" id="diferenca_ano"  value="<?php //echo $diferenca_dias->m; ?>"> 
							<input type="hidden" name="ativo" id="ativo"  value="<?php //echo $dados->ativo; ?>">
							<input type="hidden" name="id" id="id"  value="<?php //echo $dados->id; ?>"> 
							<input type="hidden" name="periodo_renovacao" id="periodo_renovacao"  value="<?php //echo $dados->periodo_renovacao; ?>"> 
							<input type="hidden" name="dt_vencimento" id="dt_vencimento"  value="<?php //echo $dados->data_renovacao_vencimento; ?>">
							<div class="input-group-append">
								<span class="input-group-text">
									<i class="la la-calendar-check-o"></i>
								</span>
							</div>
						</div>					
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-12 col-sm-12">Última Senha</label>
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="input-group">
							<input type="text" class="form-control m-input" name="date" placeholder="Selecione uma data" id="ultima_senha" value="" disabled="disabled">
						</div>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-12 col-sm-12">Nova Data de vencimento</label>
					<div class="col-lg-12 col-md-12 col-sm-12">						
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon2">
									<i class="fa fa-arrow-right" style="color: green;"></i>
								</span>
							</div>
							<input type="text" class="form-control m-input" name="date" placeholder="Selecione uma data" id="m_datepicker" value="" disabled="disabled" />
							<div class="input-group-append">
								<span class="input-group-text">
									<i class="la la-calendar-check-o"></i>
								</span>
							</div>
						</div>					
					</div>
				</div>
				<div class="form-group m-form__group">
					<label for="example_input_full_name">Senha gerada:</label>
					<input type="text" class="form-control m-input" id="aesSenha" disabled style="color: green;font-size: 16px; font-weight: 800;" />
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>				
				<button type="button" onclick="geraSenhaValidade();" class="btn btn-warning">Gerar senha</button>
			</div>
		</div>
	</div>
</div>