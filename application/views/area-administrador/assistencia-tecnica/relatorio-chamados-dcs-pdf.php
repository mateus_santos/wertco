<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{	
	border-bottom: 1px solid #f0f0f0;	
	font-size: 9px;	
	padding: 5px;
}


</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br/>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br/>
										07400-230 - Arujá - SP<br/>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br/>
			</p>

		</div>
		<div class="corpo">	
			<table style="border: none;" class="t_cabecalho" style="width: 100%;">
				<tr>
					<td colspan="3" style="text-align: center;">
						<h3 style="text-align: center;margin-top: 15px;	">Relatório Chamados - Defeitos, Causas e Soluções</h3>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="text-align: center;">
						<h4><?=$periodo?></h4>
					</td>					
				</tr>
			</table>
			
			<?php 
				if(count($dados) > 0){

			?>
			<hr  />
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;">
				<tr>
					<td style="text-align: right;border-bottom: 1px solid #ffcc00;border-right: 1px solid #ffcc00;padding: 1px;">Chamados Abertos no Período</td>				
					<td style="text-align: right;border-bottom: 1px solid #ffcc00;border-right: 1px solid #ffcc00;padding: 1px;">Total Startup</td>				
					<td style="text-align: right;border-bottom: 1px solid #ffcc00;border-right: 1px solid #ffcc00;padding: 1px;">Total Manutenção</td>				
					<td style="text-align: right;border-bottom: 1px solid #ffcc00;border-right: 1px solid #ffcc00;padding: 1px;">Total Informação</td>
				</tr>
				<tr>
					<td style="text-align: right;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 1px;"><?=$total['total']?></td>
					<td style="text-align: right;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 1px;"><?=$total['startup']?></td>
					<td style="text-align: right;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 1px;"><?=$total['manutencao']?></td>
					<td style="text-align: right;border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 1px;"><?=$total['informacao']?></td>
				</tr>
			</table>
			<h4 style="text-align: center;"><b>Defeitos Encontrados - Total: <?=$total_dcs['total_geral']?></b></h4>
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 1px;">

				<tr>
					<td style="border-bottom: 1px solid #ffcc00;border-right: 1px solid #ffcc00;padding: 1px;"><b>Defeito</b></td>
					<td style="border-bottom: 1px solid #ffcc00;border-right: 1px solid #ffcc00;padding: 1px;"><b>Causa</b></td>
					<td style="border-bottom: 1px solid #ffcc00;border-right: 1px solid #ffcc00;padding: 1px;"><b>Solução</b></td>
					<td style="border-bottom: 1px solid #ffcc00;border-right: 1px solid #ffcc00;padding: 1px;"><b>Total</b></td>
					<td style="border-bottom: 1px solid #ffcc00;border-right: 1px solid #ffcc00;padding: 1px;text-align: right;"><b>%</b></td>
				</tr>				
				<?php foreach($dados as $dado){ ?>
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 1px;"><?=mb_strtoupper($dado['defeito'])?> </td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 1px;"><?=mb_strtoupper($dado['causa'])?> </td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 1px;"><?=mb_strtoupper($dado['solucao'])?> </td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 1px;"><?=$dado['total']?> </td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 1px;text-align: right;"><?=round(($dado['total'] * 100 ) / $total_dcs['total_geral'],2)?> </td>
				</tr>
			<?php }  ?>
									
			</table>
			<?php }else { ?>	
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc; margin-top: 300px;" />								
				<h2 style="text-align: center;">Nenhum resultado encontrado!</h2>
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc;" />							
			<?php } ?>	
		</div>	
	</div>
</body>
</html>