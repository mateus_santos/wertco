<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-diagram" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Relatório de Ordens de Pagamento
					</h3>					
				</div>
			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >		
			<form class="" action="<?php echo base_url('areaAdministrador/gerarRelatorioOrdemPagto');?>" method="post" target="_blank">
			
			<div class="form-group m-form__group row filtros" >
				<div class="col-lg-3">
					<label class="">Tipo:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  name="tipo_id" placeholder="Tipo" id="tipo_id" >
                        	<option value="">SELECIONE UM TIPO</option>                        
                        	<option value="1">STARTUP</option>                        
                        	<option value="2">MANUTENÇÃO</option>
                       	</select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-corgs"></i></span></span>
						
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Período:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" 	 name="dt_ini" 	id="dt_ini"	class="form-control m-input datepicker" placeholder="data inicial criação" 	style="width: 49%;float: left;" /> 
						<input type="text" 	 name="dt_fim" 	id="dt_fim"	class="form-control m-input datepicker" placeholder="data final criação" 	style="width: 49%;float: right;"/>
						
					</div>
				</div>					
				<div class="col-lg-3">
					<label>Estado do cliente:</label>
					<div class="m-input-icon m-input-icon--right">		
						<select  class="form-control"  maxlength="250" name="estado" placeholder="Estado" id="estado">
                            <option value="">Selecione um Estado</option>
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AP">Amapá</option>
                            <option value="AM">Amazonas</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PR">Paraná</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SP">São Paulo</option>
                            <option value="SE">Sergipe</option>
                            <option value="TO">Tocantins</option>
                        </select>										
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
					</div>
				</div>
				<div class="col-lg-3">
					<label>Região:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="regiao" placeholder="" id="regiao" >
							<option value="">SELECIONE UMA REGIÃO</option>
							<option value="NORTE">NORTE</option>
							<option value="NORDESTE">NORDESTE</option>
							<option value="CENTRO-OESTE">CENTRO-OESTE</option>
							<option value="SUDESTE">SUDESTE</option>
							<option value="SUL">SUL</option>
						</select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-o"></i></span></span>
					</div>
				</div>
			</div>
			<div class="form-group m-form__group row filtros">
				<div class="col-lg-3">
					<label>Posto:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" id="posto" class="form-control" placeholder="Pesquise por um posto" />
						<input type="hidden" name="posto_id" id="posto_id"  />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
					</div>
				</div>
				<div class="col-lg-3">
					<label>Técnico:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" id="tecnico" class="form-control" placeholder="Pesquise por um Técnico" />
						<input type="hidden" name="tecnico_id" id="tecnico_id"  />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
					</div>
				</div>
				<div class="col-lg-3">
					<label>Chamado:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" id="chamado" class="form-control" placeholder="Id do chamado ou Cliente" />
						<input type="hidden" name="chamado_id" id="chamado_id"  />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-search"></i></span></span>
					</div>
				</div>
				<div class="col-lg-3">
					<label>Formato:</label>
					<div class="m-radio-inline">
						<label class="m-radio">
							<input type="radio" value="pdf" class="tp" name="tp" checked="checked" />
							PDF
							<span></span>
						</label>
						<label class="m-radio">
							<input type="radio" value="excel" class="tp" name="tp" />
							EXCEL
							<span></span>
						</label>						
					</div>
				</div>
			</div>			
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>							
					</div>
				</div>
			</div>			
			</form>	
		</div>						
	</div>	
</div>	
