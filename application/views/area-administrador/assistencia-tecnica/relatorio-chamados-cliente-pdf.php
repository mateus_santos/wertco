<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{	
	border-bottom: 1px solid #f0f0f0;	
	font-size: 9px;	
	padding: 5px;
}


</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br/>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br/>
										07400-230 - Arujá - SP<br/>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br/>
			</p>

		</div>
		<div class="corpo">	
			<table style="border: none;" class="t_cabecalho" style="width: 100%;">
				<tr>
					<td colspan="3" style="text-align: center;">
						<h4 style="text-align: center;margin-top: 15px;	">Relatório Chamados Cliente</h3>
					</td>
				</tr>
				<tr>
					<td>
						<h3>Cliente: <?=$cliente[0]['cnpj'].' - '.strtoupper($cliente[0]['razao_social'])?></h3>
					</td>
					<td >					
						<h3>Cep: <?=$cliente[0]['cep']?></h3>
					</td>
					<td >
						<h3>Cidade/UF: <?=strtoupper($cliente[0]['cidade'])."/".strtoupper($cliente[0]['estado'])?></h3>
					</td>
				</tr>
			</table>
			
			<?php 
				if(count($dados) > 0){

			?>
			<hr  />
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;">
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>#</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Início</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Fim</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Tipo</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Status</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Modelo</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Nº de serie </b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Atividade</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Início Atividade</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Fim Atividade</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Defeito</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Causa</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Solução</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Técnico</b></td>
				</tr>				
				<?php $total = 0; $chamado_aux = 0; $count = 1; foreach($dados as $dado){ 
					$count = ($chamado_aux != $dado['chamado']) ? 1 : $count+1;
					?>
				<tr>
					<td ><?=$dado['chamado']?> </td>
					<td ><?=$dado['inicio']?> </td>
					<td ><?=$dado['fim']?> </td>
					<td ><?=$dado['tipo']?> </td>
					<td ><?=$dado['status']?> </td>
					<td ><?=$dado['modelo']?> </td>
					<td ><?=$dado['numero_serie']?> </td>
					<td ><?=$count?> </td>					
					<td ><?=$dado['inicio_atividade']?> </td>
					<td ><?=$dado['fim_atividade']?> </td>
					<td ><?=$dado['defeito']?> </td>
					<td ><?=$dado['causa']?> </td>
					<td ><?=$dado['solucao']?> </td>
					<td ><?=$dado['tecnico']?> </td>
				</tr>
			<?php 					
				
				
				$chamado_aux = $dado['chamado'];
			} 
			?>
									
			</table>
			<?php }else { ?>	
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc; margin-top: 300px;" />								
				<h2 style="text-align: center;">Nenhum resultado encontrado!</h2>
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc;" />							
			<?php } ?>	
		</div>	
	</div>
</body>
</html>