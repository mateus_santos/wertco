<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/chamados'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/chamados'); ?>">Voltar</a>&nbsp;&nbsp;
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/inserirChamado');?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="inicio" value="<?= date('Y-m-d H:i:s')?>">
					<input type="hidden" name="usuario_id" value="<?= $usuario_id?>">
					<input type="hidden" name="tipo_cadastro_id" id="tipo_cadastro_id">
					<input type="hidden" name="fl_inadimplencia" id="fl_inadimplencia">
					<div class="col-lg-12" >
						<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 62px;">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon m--hide">
											<i class="flaticon-statistics"></i>
										</span>										
										<h2 class="m-portlet__head-label m-portlet__head-label--success">
											<span>
												Cliente
											</span>
										</h2>
									</div>
								</div>								
							</div>
							<div class="m-portlet__body">
								<div class="col-lg-12">
									<label>Cliente</label>
									<div class="m-input-icon m-input-icon--right">
										<input type="text" required id="cliente" name="cliente" class="form-control m-input" placeholder="" required>
										<input type="hidden" required name="cliente_id" id="cliente_id" class="form-control m-input" placeholder="" required>
										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i onclick="redireciona_configuracao()" class="la la-cogs "></i></span></span>
									</div>
								</div>
								<div class="col-lg-12">
									<label>Endereço</label>
									<div class="m-input-icon m-input-icon--right">
										<input type="text" required id="endereco" class="form-control m-input" placeholder="" disabled="disabled"> 
										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
									</div>
								</div>
								<div class="col-lg-12">
									<label>Cidade/UF</label>
									<div class="m-input-icon m-input-icon--right">
										<input type="text" required id="cidade" class="form-control m-input" placeholder="" disabled="disabled"> 
										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
									</div>
								</div>
								<div class="col-lg-12">
									<label>Pessoa de Contato</label>
									<div class="m-input-icon m-input-icon--right">
										<select name="contato_id" id="contato_id" class="form-control" style="width: 90% !important;float: left;">
											<option value="0">Informe o cliente</option>
										</select>
										<button type="button" data-toggle="modal" href="#modal_add_usuario" class="btn btn-outline-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" style="float: right;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo contato" cliente_id="" id="novo_usuario">
											<i class="la la-plus "></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr/>
					<div class="m-portlet__body row" style="margin: 15px;">	
					<div class="form-group m-form__group row col-lg-12" style="border-top: 1px #f0f0f0 dotted;">
						<div class="col-lg-4">
							<label>Tipo</label>
							<div class="m-input-icon m-input-icon--right">
								<select name="tipo_id" id="tipo_id" class="form-control">
									<?php foreach( $tipos as $tipo ){ ?>
										<option value="<?php echo $tipo['id']; ?>"><?php echo $tipo['descricao']; ?></option>												
									<?php } ?>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
							</div>
						</div>	
						<div class="col-lg-4">
							<label>Prioridade</label>
							<div class="m-input-icon m-input-icon--right">
								<select name="prioridade_id" id="prioridade_id" class="form-control">
									<?php foreach($prioridades as $prioridade) { ?>
										<option value="<?=$prioridade['id'] ?>"><?=$prioridade['descricao'] ?></option>
									<?php } ?>	
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
							</div>
						</div>
						<div class="col-lg-4">
							<label>Status</label>
							<div class="m-input-icon m-input-icon--right">
								<select name="status_id" id="status_id" class="form-control" disabled>
									<option value="1">Aberto</option>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
							</div>
						</div>						
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase" onclick="return valida_form()">Salvar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
				<!-- <div class="col-lg-12">
					<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 25px;">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<span class="m-portlet__head-icon m--hide">
										<i class="flaticon-statistics"></i>
									</span>										
									<h2 class="m-portlet__head-label m-portlet__head-label--warning">
										<span>
											Equipamentos
										</span>
									</h2>
								</div>
							</div>								
						</div>
						<div class="m-portlet__body">
							<table ></table>									
						</div>
					</div>	
				</div> -->
			</form>
			<!--end::Form-->
		</div>
</div>

<div class="modal fade" id="modal_add_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">

					<h5 class="modal-title modal-andamento-title" >Cadastrar Novo Funcionário</h5>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">

						<span aria-hidden="true">&times;</span>

					</button>

				</div>

				<div class="modal-body" style="width: 750px;">

					

					<div class="form-group m-form__group row">
						<div class="form-group m-form__group col-lg-9" >
							<label for="exampleSelect1">Nome</label>					

							<input type="text" class="form-control m-input m-input--air sub" placeholder="Nome do funcionário" name="nome" id="nome" style="margin-bottom: 10px;float: left;" />

						</div>

						<div class="form-group m-form__group col-lg-3" >
							<label for="exampleSelect1">CPF</label>
							<input type="text"  class="form-control m-input m-input--air qtd" placeholder="CPF do funcionário" name="cpf" id="cpf"  style="margin-bottom: 10px;float: right;" /> 
						</div>

					</div>
					
					<div class="form-group m-form__group row">
						<div class="form-group m-form__group col-lg-9" >
							<label for="exampleSelect1">Email</label>
							<input type="email" class="form-control m-input m-input--air sub" placeholder="E-mail do funcionário" name="email" id="email" style="margin-bottom: 10px;float: left;" />

						</div>

						<div class="form-group m-form__group col-lg-3" >

							<label for="exampleSelect1">Telefone</label>							

							<input type="text"  class="form-control m-input m-input--air qtd" placeholder="Telefone do funcionário" name="telefone" id="telefone"  style="margin-bottom: 10px;float: right;" /> 

						</div>

					</div>

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>

					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="adicionar_funcionario()">Adicionar Funcionário</button>

				</div>

			</div>

		</div>

	</div>
	
	
<?php if ($this->session->flashdata('erro') == TRUE){?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, Tente novamente!',
			'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>