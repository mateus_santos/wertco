<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{	
	border-bottom: 1px solid #f0f0f0;	
	font-size: 9px;	
	padding: 5px;
}


</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br/>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br/>
										07400-230 - Arujá - SP<br/>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br/>
			</p>

		</div>
		<div class="corpo">	
			<table style="border: none;" class="t_cabecalho" style="width: 100%;">
				<tr>
					<td colspan="3" style="text-align: center;">
						<h3 style="text-align: center;margin-top: 15px;	">Relatório Ordens de Pagamento</h3>
					</td>
				</tr>
				<tr>
					<td>
					<?php  if($periodo != ''){  ?>			
						Período: <?php echo $periodo; ?>
					<?php } ?>						
					</td>
					<td style="text-align: center;">					
					</td>
					<td style="text-align: right;">
						<?php echo date('d/m/Y H:i:s'); ?>
					</td>
				</tr>
			</table>
			
			<?php 
				if(count($dados) > 0){

			?>
			<hr  />
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;">
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Chamado#</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>O.Pagto#</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0; text-align: center;"><b>Descrição</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0; text-align: center;" ><b>Tipo</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0; text-align: center;"><b>Favorecido</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0; text-align: center;"><b>Cliente</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0; text-align: center;"><b>Estado</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0; text-align: center;max-width: 50px !important;"><b>Despesas</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0; text-align: right;"><b>Valor</b></td>
				</tr>				
				<?php $total = 0; $qtd = 0; foreach($dados as $dado){ ?>
				<tr>
					<td style="padding: 2px;"><?=$dado['chamado_id']?> </td>
					<td style="padding: 2px;"><?=$dado['id']?> </td>
					<td style="padding: 2px;"><?=$dado['descricao']?> </td>
					<td style="padding: 2px;"><?=$dado['tipo_chamado']?> </td>
					<td style="padding: 2px;"><?=$dado['favorecido']?> </td>
					<td style="padding: 2px;"><?=$dado['cliente']?> </td>
					<td style="padding: 2px;text-align: center;"><?=$dado['estado_cli']?> </td>
					<td style="padding: 2px; max-width: 50px !important;"><?=$dado['despesas']?> </td>
					<td style="padding: 2px;text-align: right;">R$ <?=$dado['valor']?> </td>
				</tr>
			<?php 	
				
				$total = $dado['valor_n'] + $total;
				$qtd++;

			} 
			?>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan="9">Total de Op's:<?=$qtd;?> </td>
				</tr>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan="9">Valor Total: R$ <?php echo number_format($total, 2, ',', '.');?> </td>
				</tr>						
			</table>
			<?php }else { ?>	
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc; margin-top: 300px;" />								
				<h2 style="text-align: center;">Nenhum resultado encontrado!</h2>
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc;" />							
			<?php } ?>	
		</div>	
	</div>
</body>
</html>