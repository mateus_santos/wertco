<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Solicitação de Cartões Técnicos
					</h3>
				</div>				
			</div>
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;">ID</th>
						<th style="text-align: center;">Técnico</th>
						<th style="text-align: center;">Empresa</th>
						<th style="text-align: center;">Data/Hora Solicitação</th>
						<th style="text-align: center;">Ações?</th>
					</tr>
				</thead>
				<tbody>					
					<?php foreach($dados as $dado){ ?>
						<tr>
							<td style="width: 5%;text-align: center;">  <?php echo $dado->id; ?></td>
							<td style="width: 30%;text-align: center;"> <?php echo $dado->nome; ?></td>
							<td style="width: 35%;text-align: center;"> <?php  echo $dado->empresa; ?></td>		
							<td style="width: 10%;text-align: center;" > <?php echo date('d/m/Y H:i:s',strtotime($dado->dthr_solicitacao)); ?></td>							
							<td style="width: 20%;text-align: center;">
								<button type="button" class="btn m-btn--pill m-btn--air btn-outline-info confirmar" usuario_id="<?php echo $_SESSION['usuario_id'];?>" solicitacao_id="<?php echo $dado->id;?>" >Confirmar</button>
								<button type="button" class="btn m-btn--pill m-btn--air btn-outline-warning excluir" usuario_id="<?php echo $_SESSION['usuario_id'];?>" solicitacao_id="<?php echo $dado->id;?>" >Excluir</button>
							</td>							
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group" >
						<label for="exampleSelect1">Situação do Orçamento</label>
						<select class="form-control m-input m-input--air" id="status_orcamento">
							<option value="">Selecione o status do orçamento</option>						
							<option value="1">Aberto</option>
							<option value="5">Orçamento entregue</option>
							<option value="6">Em negociação</option>
							<option value="2">Fechado</option>
							<option value="3">Perdido</option>
							<option value="4">Cancelado</option>													
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" class="btn btn-primary" orcamento_id="">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>	

	<!-- Modal Andamento -->
	<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="m-list-timeline">
						<div class="m-list-timeline__items">                
						</div>
					</div> 
					<hr/>
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" >
							<label for="exampleSelect1">Adicionar Andamento do venda</label>
							<textarea type="text" class="form-control m-input m-input--air" id="andamento_texto"></textarea>					
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="adicionar_andamento" class="btn btn-primary" status_orcamento="" orcamento_id="">Adicionar Andamento</button>
				</div>
			</div>
		</div>
	</div>	

	<!-- Modal Responsavel -->
	<div class="modal fade" id="m_modal_8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-responsavel-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="form-group m-form__group">
						<label>Alterar Responsável</label>
						<div class="m-input-icon m-input-icon--left">
							<input type="text" class="form-control m-input--air" id="responsavel" placeholder="Pesquisar Representante" />
							<input type="hidden" class="form-control m-input--air" id="responsavel_id" placeholder="Pesquisar Representante" />
							<span class="m-input-icon__icon m-input-icon__icon--left">
								<span>
									<i class="la la-search"></i>
								</span>
							</span>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_responsavel" class="btn btn-primary" responsavel_id="" orcamento_id="" status_orcamento_id="">Alterar Responsável</button>
				</div>
			</div>
		</div>
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaAdministrador/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>