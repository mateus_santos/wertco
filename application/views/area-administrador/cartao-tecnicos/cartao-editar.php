<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/cartaoTecnicos'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/cartaoTecnicos'); ?>">Voltar</a>						
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarCartao');?>" method="post" enctype="multipart/form-data">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-6">
							<label>Técnico</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="usuario" id="usuario" class="form-control m-input" placeholder="" required value="<?php echo $dados->usuario_id.' - '.$dados->nome; ?>">
								<input type="hidden" required name="usuario_id" id="usuario_id" class="form-control m-input" placeholder="" required value="<?php echo $dados->usuario_id; ?>">
								<input type="hidden" required name="cartao_id" id="usuario_id" class="form-control m-input" placeholder="" required value="<?php echo $dados->id; ?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
						<div class="col-lg-6">
							<label>Tag:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="tag" id="tag" class="form-control m-input" placeholder="" required value="<?php echo $dados->tag; ?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-key"></i></span></span>
							</div>							
						</div>
					</div>				
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Período de renovação (Meses):</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="periodo_renovacao" class="form-control m-input" placeholder="" required maxlength="2" value="<?php echo $dados->periodo_renovacao; ?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar-check-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">
							<label>Data Vencimento:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="dt_vencimento" id="m_datepicker" class="form-control m-input" placeholder="" required value="<?php echo date_format(date_create($dados->dt_vencimento),'d/m/Y'); ?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar-check-o"></i></span></span>
							</div>							
						</div>
						<div class="col-lg-4">														
							<label>Ativo:</label>	
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" name="ativo" value="1" <?php echo ($dados->ativo == 1) ? 'checked="checked"' : '' ?> />
									Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" name="ativo" value="0" <?php echo ($dados->ativo == 0) ? 'checked="checked"' : '' ; ?>>
									Não
									<span></span>
								</label>								
							</div>						
						</div>
					</div>	
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</div>
			</form>
			<!--end::Form-->
		</div>
</div>	