<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Cartões Técnicos
					</h3>
				</div>
			</div>
			<div class="col-md-4 m-portlet__head-caption">													
				<a href="<?php echo base_url('AreaAdministrador/cadastraCartao')?>" class="" style="color: #ffcc00; font-weight: bold;">
					<i class="la la-plus-circle" style="font-size: 38px;"></i>
				</a>
			</div>					
		</div>
		<div class="m-portlet__body">			

			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th title="id" width="10%">ID</th>
						<th title="nome_arquivo"width="30%">Usuário</th>												
						<th title="descricao" width="30%">Tag</th>						
						<th title="" width="10%">Vencimento</th>
						<th title="" width="10%">Ativo</th>
						<th title="" width="10%">Ações</th>
					</tr>
				</thead>
				<tbody>
					<tr>
					<?php foreach($dados as $dado){ ?>
						<td style="text-align: center;">
							<?php echo $dado->id; ?>							
						</td>						
						<td>
							<?php echo $dado->nome; ?>							
						</td>	
						<td>
							<?php echo $dado->tag; ?>
						</td>
						<td>
							<?php echo date_format(date_create($dado->dt_vencimento), 'd/m/Y'); ?>
						</td>
						<td>
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" name="ativo<?php echo $dado->id;?>" onclick="atualizarStatus('1',<?php echo $dado->id?>);" class="ativo" <?php if($dado->ativo=='1') echo 'checked="checked"' ?> value="1" user_id="<?php echo $dado->id; ?>"> Sim
									<span></span>
								</label>									
								<label class="m-radio">
									<input type="radio" name="ativo<?php echo $dado->id;?>" class="ativo" <?php if($dado->ativo=='0') echo 'checked="checked"' ?> value="0" user_id="<?php echo $dado->id; ?>" onclick="atualizarStatus('0',<?php echo $dado->id?>);" > Não
									<span></span>
								</label>								
							</div>
						</td>	
						<td data-field="Actions" class="m-datatable__cell " style="width: 5%;">
							<span style="overflow: visible; width: 110px;" class="">								
								<a href="<?php echo base_url('AreaAdministrador/editarCartao/'.$dado->id); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Editar">
									<i class="la la-edit"></i>
								</a>
								<button  data-toggle="m-tooltip"  id="excluir" onclick="excluirCartao(<?php echo $dado->id; ?>);" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="excluir" cartao_id="<?php echo $dado->id; ?>">
									<i class="la la-trash"></i>
								</button>
							</span>
						</td>
					</tr>
						<?php } ?>
				</tbody>
			</table>
			<!--end: Datatable -->
		</div>
	</div>		        
</div>				
<!-- end:: Body -->
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']); } ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
            title: "OK!",
            text: 'Cadastro realizado com sucesso!',
            type: "success"
        }).then(function() {
		 	window.location = base_url+'AreaAdministrador/cartaoTecnicos';
		}); 
	</script>	
<?php unset($_SESSION['sucesso']); } ?>