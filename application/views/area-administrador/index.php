<div class="m-content">
	
	<?php 	if($usuario_id == 66 || $usuario_id == 2 || $usuario_id == 12 || $usuario_id == 645 || $usuario_id == 2465){ ?>
	<div class="row">					
		<?php include('dashboard-admin/dashboard-abertos.php'); ?>
	</div>
	<div class="row">					
		<?php include('dashboard-admin/dashboard-negociacao.php'); ?>
	</div>	
	<div class="row">					
		<?php include('dashboard-admin/dashboard-fechados.php'); ?>
	</div>
	<div class="row">					
		<?php include('dashboard-admin/dashboard-pedidos.php'); ?>
	</div>
	<?php } ?>
	<div class="row">
	<?php 	if($tipo_acesso == 'administrador geral' ||	$tipo_acesso == 'administrador comercial'){
				//Itiel, eu, daniel e seu luiz
				if($usuario_id == 2465 || $usuario_id == 2 || $usuario_id == 66 || $usuario_id == 28){
		?>
	<div class="col-xl-6">			
		<div class="m-portlet m-portlet--bordered-semi">
			<div class="m-portlet__head" style="border-bottom: 1px solid #f0f0f0;">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Orçamentos Necessitando Aprovação
						</h3>
					</div>
				</div>										
			</div>
			<div class="m-portlet__body">
			<?php
				if( count($orcamento_aguardando) > 0 ){

				foreach( $orcamento_aguardando as $aprovacao ){	?>
					<div class="m-widget4 " style="overflow-x: auto;">
						<div class="m-widget4__item">							
							<div class="m-widget4__info">
								<span class="m-widget4__title" >
									Orçamento # 
									<a href="<?php echo base_url('AreaAdministrador/visualizarOrcamento/'.$aprovacao['id']); ?>" target="_blank">
										 <?php echo $aprovacao['id'];?>
									</a>
								</span>
								<br>	
								<span class="m-widget4__sub">
									Cliente: <?php echo $aprovacao['cliente']; 	?>
								</span>							
							</div>
							<span class="m-widget4__ext" style="min-width: 180px;">
								<span class="m-widget4__number m--font-brand">
									Total: R$ <?php echo number_format($aprovacao['total'],2,',','.');	?>
								</span>
							</span>
							<span class="m-widget4__ext" style="min-width: 30px;">
								<span class="m-widget4__number m--font-brand">
									<a class="btn btn-outline-success btn-sm m-btn m-btn--icon m-btn--outline-2x" onclick="aprovarOrcamento(<?php echo $aprovacao['id'];?>)">
										<span>
											<i class="la la-check"></i>
											<span>Aprovar</span>
										</span>
									</a>
								</span>
							</span>
						</div>
					</div>
			<?php }
			}else{ ?>
				<h4 class="m--font-warning">Não há solicitações de orçamentos a serem aprovados!</h4>
			<?php } ?>	
			</div>
		</div>
		<!--end:: Widgets/Authors Profit-->
	</div>
	<div class="col-xl-6" >			
		<div class="m-portlet m-portlet--bordered-semi" style="background: #ffcc00;">
			<div class="m-portlet__head" style="border-bottom: 1px solid #f0f0f0;">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text" style="color: #fff;">
							Pedidos Necessitando Aprovação
						</h3>
					</div>
				</div>										
			</div>
			<div class="m-portlet__body pedidos_a_confirmar">
			<?php
				if( count($pedidos_aguardando) > 0 ){

				foreach( $pedidos_aguardando as $pedidos ){	?>
					<div class="m-widget4 pedidos_a_aprovar" pedido_id="<?php echo $pedidos['id'];?>" style="overflow-x: auto;">
						<div class="m-widget4__item">							
							<div class="m-widget4__info">
								<span class="m-widget4__title"  >
									<a onclick="geraPedidosPdf(<?php echo $pedidos['id']; ?>)" target="_blank" style="cursor: pointer;">
										Pedido # <?php echo $pedidos['id'];?>
									</a>
								</span>
								<br>	
								<span class="m-widget4__sub">
									Cliente: <?php echo $pedidos['cliente']; 	?>
								</span>							
							</div>
							<span class="m-widget4__ext" style="min-width: 180px;">
								<span class="m-widget4__number m--font-brand">
								
								</span>
							</span>
							<span class="m-widget4__ext" style="min-width: 30px;">
								<span class="m-widget4__number m--font-brand">
									<a class="btn btn-outline-success btn-sm m-btn m-btn--icon m-btn--outline-2x" onclick="aprovarPedido(<?php echo $pedidos['id'];?>)">
										<span>
											<i class="la la-check"></i>
											<span>Aprovar</span>
										</span>
									</a>
								</span>
							</span>
						</div>
					</div>
			<?php }
			}else{ ?>
				<h4 style="color: #fff;">Não há solicitações de pedidos a serem aprovados!</h4>
			<?php } ?>	
			</div>
		</div>
		<!--end:: Widgets/Authors Profit-->
	</div>
	<?php } ?>	
</div>	
	<div class="row">
		<div class="col-xl-4">
			<!--begin:: Widgets/Sales Stats-->
			<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Orçamentos - <?=date('Y')?>
							</h3>
						</div>						
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl">
									<i class="fa fa-calendar m--font-brand"></i>
								</a>
								<div class="m-dropdown__wrapper" style="width: 150px !important;">
									<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 14.5px;"></span>
									<div class="m-dropdown__inner">
										<div class="m-dropdown__body">
											<div class="m-dropdown__content">
												<ul class="m-nav">
													<li class="m-nav__section m-nav__section--first">
														<span class="m-nav__section-text">
															Gráfico por período
														</span>
													</li>
													<li class="m-nav__item">
																													
														<span class="m-nav__link-text orc_periodo" periodo="30" style="cursor: pointer;">
															30 dias
														</span>
														
													</li>
													<li class="m-nav__item">
																													
														<span class="m-nav__link-text orc_periodo" periodo="60" style="cursor: pointer;">
															60 dias
														</span>
														
													</li>
													<li class="m-nav__item">
														<span class="m-nav__link-text orc_periodo" periodo="120" style="cursor: pointer;">
															120 dias
														</span>
														
													</li>
													<li class="m-nav__item">
														<span class="m-nav__link-text orc_periodo" periodo="180" style="cursor: pointer;">
															180 dias
														</span>
														
													</li>
													<li class="m-nav__separator m-nav__separator--fit"></li>
													<li class="m-nav__item">
														
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>				
				<div class="m-portlet__body">
					<!--begin::Widget 6-->
					<div class="m-widget15">
						<div class="m-widget15__chart" style="height:260px; cursor: pointer;">
							<canvas  id="m_status_total"></canvas>
						</div>
						<div class="m-widget15__items">
							<div class="row">

								<?php $i=1; if(is_array($total_status)){ foreach($total_status['dados'] as $dados){
									switch ($dados['descricao']) {
										case 'aberto':
											$back = "bg-secondary";
											break;
										case 'em negociação':
											$back = "bg-primary";
											break;
										case 'orçamento entregue':
											$back = "bg-info";
											break;			
										case 'fechado':
											$back = "bg-success";
											break;				
										case 'cancelado':
											$back = "bg-warning";
											break;																	
										default:
											$back = "bg-danger";
											break;
									}

									?>
								<div class="col">
									<div class="m-widget15__item">
										<span class="m-widget15__stats">															
											<?php 																
											echo round(($dados['valor']*100)/$total_status['total'],2).'%';
											?>
										</span>
										<span class="m-widget15__text">
											<?php echo ucfirst($dados['descricao']); ?>
										</span>
										<div class="m--space-10"></div>
										<div class="progress m-progress--sm">
											<div  class="progress-bar" role="progressbar" style="width: <?php echo round(($dados['valor']*100)/$total_status['total'],2).'%';?>; background: <?php echo $total_status['cores'][$i-1];?>;" aria-valuenow="<?php echo round(($dados['valor']*100)/$total_status['total'],2).'%';?>" aria-valuemin="0" aria-valuemax="100">
												
											</div>
										</div>
									</div>
								</div>
								<?php if( ($i%2) == 0 ){ ?>
								</div>
								<div class="row">

								<?php } $i++; } } ?>													
								
							</div>
						</div>
						<div class="m-widget15__desc">
							* Total de orçamentos: <b><?php echo (is_array($total_status)) ? $total_status['total'] : '0'; ?></b>
						</div>
					</div>
					<!--end::Widget 6-->
				</div>
			</div>
			<!--end:: Widgets/Sales Stats-->
		</div>
		<div class="col-xl-4">
			<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit " style="min-height: 385px; background: #abcdb038;">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Total Orçamentos Fechados - <?php echo date('Y');?>
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="Total de orçamentos fechados no ano de <?=date('Y')?>. Os orçamentos foram filtrados pela data de fechamento." >
									<i class="fa fa-exclamation-circle m--font-brand"></i>
								</a>
							</li>
						</ul>
					</div>											
				</div>
				<div class="m-portlet__body">
					<!--begin::Widget5-->
					<div class="m-widget20" style="">
						<div class="m-widget20__number m--font-success" style="font-size: 1.0rem !important;">
						Total: <b>R$ <?php echo number_format($total_valor_mes['total'], 2, ',', '.');?></b>
						</div>
						<div class="m-widget20__number m--font-success" style="font-size: 1.0rem !important;">
						VL. Total de Bombas: 	<b>R$ <?php echo number_format($valor_bombas['valor'], 2, ',', '.');?></b>
						</div>
						<div class="m-widget20__number m--font-success" style="font-size: 1.0rem !important;">
						Qtd. Total de Bombas:  	<b><?php echo $total_bombas['total']; ?></b>
						</div>
						<div class="m-widget20__number m--font-success" style="font-size: 1.0rem !important;">
						VL. Médio Bomba: <b>R$ <?php echo ($total_bombas['total'] > 0) ? number_format($valor_bombas['valor'] / $total_bombas['total'], 2, ',', '.') : 'R$ 0,00'; ?></b>
						</div>
						<div class="m-widget20__chart" style="height:160px;margin-top: 39px;">
							<canvas id="m_valor_mes"></canvas>
						</div>
					</div>
					<!--end::Widget 5-->
				</div>
			</div>	
			<!--begin:: Widgets/Inbound Bandwidth-->			
			<div class="m--space-30"></div>
			<!--begin:: Widgets/Inbound Bandwidth-->			
			<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit " style="min-height: 385px; background: #ff99005e !important;">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Total Pedidos Confirmados - <?php echo date('Y'); ?>
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
								<a  target="_blank" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="Relatório mensal de vendas por modelo de Bombas, referente ao ano corrente." onclick="abre_modal_tp_conf();">
									<i class="fa fa-print m--font-brand"></i>
								</a>
							</li>
						</ul>
					</div>											
				</div>
				<div class="m-portlet__body">
					<!--begin::Widget5-->
					<div class="m-widget20" style="">
						<div class="m-widget20__number " style="font-size: 1.0rem !important;">
						Total: <b>R$ <?php echo number_format($total_valor_mes_ped_c['total'], 2, ',', '.');?></b>
						</div>
						<div class="m-widget20__number " style="font-size: 1.0rem !important;">
						VL. Total de Bombas: 	<b>R$ <?php echo $total_bombas_ped_c['valor_total']; ?></b>
						</div>
						<div class="m-widget20__number " style="font-size: 1.0rem !important;">
						Qtd. Total de Bombas:  	<b><?php echo $total_bombas_ped_c['qtd_total']; ?></b>
						</div>
						<div class="m-widget20__number " style="font-size: 1.0rem !important;">
						VL. Médio Bomba: <b>R$ <?php echo $total_bombas_ped_c['media']; ?></b>
						</div>
						<div class="m-widget20__chart" style="margin-top: 50px;">
							<canvas id="m_valor_mes_ped_c"></canvas>
						</div>
					</div>
					<!--end::Widget 5-->
				</div>
			</div>
			<!--end:: Widgets/Inbound Bandwidth-->
		</div>
		<!-- Colar aqui os pedidos totais -->
		<div class="col-xl-4">
			<!--begin:: Widgets/Sales Stats-->
			<div class="m-portlet m-portlet--full-height m-portlet--fit  ">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Status Pedidos - <?=date('Y')?>
							</h3>
						</div>						
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl">
									<i class="fa fa-calendar m--font-brand"></i>
								</a>
								<div class="m-dropdown__wrapper" style="width: 150px !important;">
									<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 14.5px;"></span>
									<div class="m-dropdown__inner">
										<div class="m-dropdown__body">
											<div class="m-dropdown__content">
												<ul class="m-nav">
													<li class="m-nav__section m-nav__section--first">
														<span class="m-nav__section-text">
															Gráfico por período
														</span>
													</li>
													<li class="m-nav__item">
																													
														<span class="m-nav__link-text ped_periodo" periodo="30" style="cursor: pointer;">
															30 dias
														</span>
														
													</li>
													<li class="m-nav__item">
																													
														<span class="m-nav__link-text ped_periodo" periodo="60" style="cursor: pointer;">
															60 dias
														</span>
														
													</li>
													<li class="m-nav__item">
														<span class="m-nav__link-text ped_periodo" periodo="120" style="cursor: pointer;">
															120 dias
														</span>
														
													</li>
													<li class="m-nav__item">
														<span class="m-nav__link-text ped_periodo" periodo="180" style="cursor: pointer;">
															180 dias
														</span>
														
													</li>
													<li class="m-nav__separator m-nav__separator--fit"></li>
													<li class="m-nav__item">
														
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>				
				<div class="m-portlet__body">
					<!--begin::Widget 6-->
					<div class="m-widget15">
						<div class="m-widget15__chart" style="height:260px; cursor: pointer;">
							<canvas  id="m_status_total_ped"></canvas>
						</div>
						<div class="m-widget15__items">
							<div class="row">
								<?php $i=1; if(is_array($total_status_ped)){ foreach($total_status_ped['dados'] as $dados){
									switch ($dados['descricao']) {
										case 'PEDIDO GERADO':
											$back = "bg-secondary";
											break;
										case "PEDIDO CONFIRMADO PELO CLIENTE E OP'S GERADAS":
											$back = "bg-primary";
											break;
										case 'PEDIDO EM PRODUÇÃO':
											$back = "bg-info";
											break;			
										case 'PEDIDO CONCLUÍDO (PRODUZIDO)':
											$back = "bg-success";
											break;				
										case 'PEDIDO EM TRANSPORTE':
											$back = "bg-success";
											break;
										case 'PEDIDO ENTREGUE':
											$back = "bg-success";
											break;	
										case 'EM FUNCIONAMENTO':
											$back = "bg-success";
											break;																	
										default:
											$back = "bg-danger";
											break;
									}

									?>
								<div class="col">
									<div class="m-widget15__item">
										<span class="m-widget15__stats">															
											<?php 																
											echo round(($dados['valor']*100)/$total_status_ped['total'],2).'%';
											?>
										</span>
										<span class="m-widget15__text">
											<?php echo ucfirst($dados['descricao']); ?>
										</span>
										<div class="m--space-10"></div>
										<div class="progress m-progress--sm">
											<div  class="progress-bar" role="progressbar" style="width: <?php echo round(($dados['valor']*100)/$total_status_ped['total'],2).'%';?>; background: <?php echo $total_status_ped['cores'][$i-1];?>;" aria-valuenow="<?php echo round(($dados['valor']*100)/$total_status_ped['total'],2).'%';?>" aria-valuemin="0" aria-valuemax="100">
												
											</div>
										</div>
									</div>
								</div>
								<?php if( ($i%2) == 0 ){ ?>
								</div>
								<div class="row">

								<?php } $i++; } } ?>													
								
							</div>
						</div>
						<div class="m-widget15__desc">
							* Total de pedidos: <b><?php echo (is_array($total_status_ped)) ? $total_status_ped['total'] : '0'; ?></b>
						</div>
					</div>
					<!--end::Widget 6-->
				</div>
			</div>
			<!--end:: Widgets/Sales Stats-->
		</div>
	<div class="row">
		<div class="col-xl-4">
			<!--begin:: Widgets/Top Products-->
			<div class="m-portlet m-portlet--full-height m-portlet--fit ">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Produtos Mais Orçados - Top 5
							</h3>
						</div>
					</div>										
				</div>
				<div class="m-portlet__body">
					<!--begin::Widget5-->
					<div class="m-widget4 m-widget4--chart-bottom" style="min-height: 480px">
						<?php foreach($produtos_orcados['dados'] as $produtos_orcado){ ?>
						<div class="m-widget4__item">												
							<div class="m-widget4__info">
								<span class="m-widget4__title">
								<?php echo $produtos_orcado['codigo'].' - '.$produtos_orcado['descricao'];?>	
								</span>
								<br>
								<span class="m-widget4__sub">
								
								</span>
							</div>
							<span class="m-widget4__ext">
								<span class="m-widget4__number m--font-brand">
								<?php echo $produtos_orcado['total'];?>		
								</span>
							</span>
						</div>
						<?php } ?>
						<div class="m-widget4__chart m-portlet-fit--sides m--margin-top-20" style="height:260px;">
							<canvas id="top_produtos_orcados"></canvas>
						</div>
					</div>
					<!--end::Widget 5-->
				</div>
			</div>
			<!--end:: Widgets/Top Products-->
		</div>
		<div class="col-xl-4">		
			<!--<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit bg-warning" style="min-height: 300px;background: #ff9900 !important;">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text" >
								Pedidos Confirmados Por Zona - <?php echo date('Y');?>
							</h3>
							<hr/>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="Total de Pedidos Realizados Por Zona comercial, referentes ao ano corrente." >
									<i class="fa fa-exclamation-circle m--font-brand"></i>
								</a>
							</li>
						</ul>
					</div>											
				</div>
				<div class="m-portlet__body">					
					<div class="m-widget20" style="top: 70px;">
						<div class="m-widget20__number m--font-secondary" style="font-size: 1.4em;font-weight: 300;">
							<table style="width: 100%;">
								<thead>
									<th style="text-align: center;font-size: 0.9em;">Zona</th>
									<th style="text-align: center;font-size: 0.9em;padding-right: 25px;">Qtd</th>
									<th style="font-size: 0.9em;">Valor Total</th>
								</thead>
								<tbody>
						<?php 	$valor_total = $qtd = 0;  
								foreach($total_pedidos as $total_pedido){ ?>
									<tr>
										<td style="text-align: center;padding-top: 10px;"><?php echo $total_pedido['zona']; ?></td>
										<td style="text-align: center;padding-right: 25px;padding-top: 10px;"><?php echo $total_pedido['total']; ?></td>
								 		<td style="padding-top: 10px;">R$ <?php echo $total_pedido['total_valor']; ?></td>
								 	</tr>
						<?php 		$valor_total = $valor_total + $total_pedido['valor_total'];
									$qtd = $qtd + $total_pedido['total'];
								} 						?>	
									
								</tbody>
							</table>
							<hr style="margin-left: -27px;"/>
							<table style="width: 100%;margin-top: -7px;font-weight: 600;">
								<tr>
									<td style="text-align: center;padding-top: 10px;">Total</td>
									<td style="text-align: center;padding-right: 25px;padding-top: 10px;"><?=$qtd?></td>
									<td style="padding-top: 10px;">R$ <?=number_format($valor_total, 2, ',', '.');?></td>
								</tr>
							</table>
						</div>
						
					</div>					
				</div>
			</div>	-->			
			<!--begin:: Widgets/Inbound Bandwidth-->
			<div class="m-portlet m-portlet--bordered-semi m-portlet--fit " style="min-height: 385px;background: #716acaba !important;">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text m--font-light" >
								Total Pedidos Gerados - <?php echo date('Y'); ?>
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="Total de pedidos gerados no ano de <?php echo date('Y'); ?>." >
									<i class="fa fa-exclamation-circle m--font-warning"></i>
								</a>
							</li>
						</ul>
					</div>											
				</div>
				<div class="m-portlet__body">
					<!--begin::Widget5-->
					<div class="m-widget20" style="top: 100px;">
						<div class="m-widget20__number m--font-light" style="font-size: 1.0rem !important;">
						Total: <b>R$ <?php echo number_format($total_valor_mes_ped['total'], 2, ',', '.');?></b>
						</div>
						<div class="m-widget20__number m--font-light" style="font-size: 1.0rem !important;">
						VL. Total de Bombas: 	<b>R$ <?php echo number_format($valor_bombas_ped['valor'], 2, ',', '.');?></b>
						</div>
						<div class="m-widget20__number m--font-light" style="font-size: 1.0rem !important;">
						Qtd. Total de Bombas:  	<b><?php echo $total_bombas_ped['total']; ?></b>
						</div>
						<div class="m-widget20__number m--font-light" style="font-size: 1.0rem !important;">
						VL. Médio Bomba: <b>R$ <?php echo ($total_bombas_ped['total'] > 0) ? number_format($valor_bombas_ped['valor'] / $total_bombas_ped['total'], 2, ',', '.') : 'R$ 0,00'; ?></b>
						</div>
						<div class="m-widget20__chart" style="height:160px;margin-top: 40px;">
							<canvas id="m_valor_mes_ped"></canvas>
						</div>
					</div>
					<!--end::Widget 5-->
				</div>
			</div>			
			<div class="m-portlet m-portlet--bordered-semi m-portlet--fit bg-warning" style="min-height: 300px">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Orçamentos em aberto (aberto, entregue, em negociação)
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="Total de orçamentos que possuem status iguais a aberto, entregue ao cliente e em negociação e diferente de fechado." >
									<i class="fa fa-exclamation-circle m--font-brand"></i>
								</a>
							</li>
						</ul>
					</div>											
				</div>
				<div class="m-portlet__body">
					<!--begin::Widget5-->
					<div class="m-widget20" style="top: 95px;">
						<div class="m-widget20__number m--font-secondary" style="font-weight: 300;font-size: 1.7em;">
							<b style="font-weight: 600;">Total:</b> <?php echo $total_aberto['qtd']; ?>
						</div>
						<div class="m-widget20__number m--font-secondary" style="font-weight: 300;font-size: 1.7em;">
							<b style="font-weight: 600;">Valor:</b> R$ <?php echo number_format($total_aberto['total'],2,',','.'); ?>	
						</div>
						<div class="m-widget20__chart" style="height:160px;">
							
						</div>											
					</div>
					<!--end::Widget 5-->
				</div>
			</div>
			<!--end:: Widgets/Authors Profit-->
			
			<!--end:: Widgets/Outbound Bandwidth-->
			
		</div>
		<div class="col-xl-4">		
			<div class="m-portlet m-portlet--bordered-semi m-portlet--fit " style="min-height: 300px; background: #ff99005e !important;">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Comparativo Pedidos Confirmados - <?php echo date('Y',strtotime('-2 year')).'-'.date('Y',strtotime('-1 year')).'-'.date('Y'); ?>
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
								<!--<a  target="_blank" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="Relatório mensal de vendas por modelo de Bombas, referente ao ano corrente." onclick="abre_modal_tp_conf();">
									<i class="fa fa-print m--font-brand"></i>
								</a>-->
							</li>
						</ul>
					</div>											
				</div>
				<div class="m-portlet__body">
					<!--begin::Widget5-->
					<div class="m-widget20" style="">						
						<div class="m-widget20__chart" style="">
							<canvas id="m_mes_comparativo"></canvas>
						</div>
					</div>
					<!--end::Widget 5-->
				</div>
			</div>
			<div class="m-portlet m-portlet--bordered-semi m-portlet--fit " style="min-height: 350px; background: #ff99005e !important;">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Comparativo Acumulado - Pedidos Confirmados 
							</h3>
						</div>
					</div>
					<hr>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
								<!--<a  target="_blank" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="Relatório mensal de vendas por modelo de Bombas, referente ao ano corrente." onclick="abre_modal_tp_conf();">
									<i class="fa fa-print m--font-brand"></i>
								</a>-->
							</li>
						</ul>
					</div>											
				</div>
				<div class="m-portlet__body">
					<!--begin::Widget5-->
					<div class="m-widget20" style="">						
						<div class="m-widget20__chart" style="max-height: 250px;margin-bottom: 50px;">
							<canvas id="m_ano_comparativo"></canvas>
						</div>
					</div>
					<!--end::Widget 5-->
				</div>
			</div>
				
		</div>
		</div>
	</div>		
	<div class="row">	
		<div class="col-xl-6">
			<!--begin:: Widgets/Sales Stats-->
			<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								COMPARATIVO Preço Médio Top 3 - <?=date('Y')-1?>
							</h3>
						</div>						
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl">
									<i class="fa fa-calendar m--font-brand"></i>
								</a>
								<div class="m-dropdown__wrapper" style="">
									<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 14.5px;"></span>
									<div class="m-dropdown__inner" style="width: 250px;">
										<div class="m-dropdown__body">
											<div class="m-dropdown__content">
												<ul class="m-nav">
													<li class="m-nav__section m-nav__section--first">
														<span class="m-nav__section-text">
															Gráfico por período
														</span>
													</li>
													<li class="m-nav__item">
																													
														<span class="m-nav__link-text" periodo="30" style="cursor: pointer;">
															<input type="text" name="periodo_ini" id="periodo_ini_c" class="form-control date" placeholder="Início"> 
															a	
															<input type="text" name="periodo_fim" id="periodo_fim_c" class="form-control date" placeholder="Fim"><br/>
															<button class="btn btn-outline-success" id="enviar_periodo_top3_c">Enviar</button>
														</span>
														
													</li>
													
													<li class="m-nav__separator m-nav__separator--fit"></li>
													<li class="m-nav__item">
														
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>				
				<div class="m-portlet__body">
					<!--begin::Widget 6-->
					
					<div class="m-widget15">
						<div class="m-widget15__chart" style="height:260px; cursor: pointer;">
							<canvas  id="m_top_3_comparativo"></canvas>
						</div>	
					</div>
					<!--end::Widget 6-->
				</div>
			</div>
			<!--end:: Widgets/Sales Stats-->
		</div>
		<div class="col-xl-6">
			<!--begin:: Widgets/Sales Stats-->
			<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Preço Médio Top 3 - <?=date('Y')?>
							</h3>
						</div>						
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl">
									<i class="fa fa-calendar m--font-brand"></i>
								</a>
								<div class="m-dropdown__wrapper" style="">
									<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 14.5px;"></span>
									<div class="m-dropdown__inner" style="width: 250px;">
										<div class="m-dropdown__body">
											<div class="m-dropdown__content">
												<ul class="m-nav">
													<li class="m-nav__section m-nav__section--first">
														<span class="m-nav__section-text">
															Gráfico por período
														</span>
													</li>
													<li class="m-nav__item">
																													
														<span class="m-nav__link-text" periodo="30" style="cursor: pointer;">
															<input type="text" name="periodo_ini" id="periodo_ini" class="form-control date" placeholder="Início"> 
															a	
															<input type="text" name="periodo_fim" id="periodo_fim" class="form-control date" placeholder="Fim"><br/>
															<button class="btn btn-outline-success" id="enviar_periodo_top3">Enviar</button>
														</span>
														
													</li>
													
													<li class="m-nav__separator m-nav__separator--fit"></li>
													<li class="m-nav__item">
														
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>				
				<div class="m-portlet__body">
					<!--begin::Widget 6-->
					
					<div class="m-widget15">
						<div class="m-widget15__chart" style="cursor: pointer;">
							<canvas  id="m_top_3_confirmados"></canvas>
						</div>
						
						<div class="m-widget15__desc">
							<table class="table table-striped" >
							<?php foreach ($top_3['dados'] as $dado) { ?>
								<tr style="border-top: 1px solid #ffcc00">
									<td><b><?=$dado['descricao']?></b></td>
									<td>&nbsp;</td>
									<td>Qtd.: <b><?=$dado['qtd']?></b></td>
									<td>&nbsp;</td>
									<td>Preço Médio: <b>R$ <?php echo number_format($dado['total'], 2, ',', '.');?></b></td>
								</tr>
							 
							<?php } ?>
							</table>
						</div>
					</div>
					<!--end::Widget 6-->
				</div>
			</div>
			<!--end:: Widgets/Sales Stats-->
		</div>		
	</div>
	<div class="row">
		<div class="col-xl-12">
			<!--begin:: Widgets/Sales Stats-->
			<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								COMPARATIVO TOTAL DE BOMBAS VENDIDAS MÊS
							</h3>
						</div>						
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl">
									<i class="fa fa-calendar m--font-brand"></i>
								</a>
								<div class="m-dropdown__wrapper" style="">
									<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 14.5px;"></span>
									<div class="m-dropdown__inner" style="width: 250px;">
										<div class="m-dropdown__body">
											<div class="m-dropdown__content">
												<ul class="m-nav">
													<li class="m-nav__section m-nav__section--first">
														<span class="m-nav__section-text">
															Gráfico por período
														</span>
													</li>
													<li class="m-nav__item">
																													
														<span class="m-nav__link-text" periodo="30" style="cursor: pointer;">
															<input type="text" name="periodo_ini" id="periodo_ini_total" class="form-control date" placeholder="Início"> 
															a	
															<input type="text" name="periodo_fim" id="periodo_fim_total" class="form-control date" placeholder="Fim"><br/>
															<button class="btn btn-outline-success" id="enviar_periodo_total">Enviar</button>
														</span>
														
													</li>
													
													<li class="m-nav__separator m-nav__separator--fit"></li>
													<li class="m-nav__item">
														
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>				
				<div class="m-portlet__body">
					<!--begin::Widget 6-->
					
					<div class="m-widget15">
						<div class="m-widget15__chart" style="cursor: pointer;">
							<canvas  id="m_total_ano"></canvas>
						</div>	
					</div>
					<div class="m-widget15__desc">
							<table class="table table-striped" >							
								<thead>
									<tr>
										<th></th>
										<th style="text-align: right;">Janeiro</th>
										<th style="text-align: right;">Fevereiro</th>
										<th style="text-align: right;">Março</th>
										<th style="text-align: right;">Abril</th>
										<th style="text-align: right;">Maio</th>
										<th style="text-align: right;">Junho</th>
										<th style="text-align: right;">Julho</th>
										<th style="text-align: right;">Agosto</th>
										<th style="text-align: right;">Setembro</th>
										<th style="text-align: right;">Outubro</th>
										<th style="text-align: right;">Novembro</th>
										<th style="text-align: right;">Dezembro</th>
									</tr>
								</thead>
								<tbody>								
									<tr style="border-top: 1px solid #ffcc00">
										<td>2019</td>
										<?php foreach( $total_ano_2019['dados'] as $t_2019){?>
										<td style="text-align: right;"><?=$t_2019['total']?></td>	
										<?php }?>
									</tr>
									<tr>
										<td>2020</td>
										<?php foreach( $total_ano_2020['dados'] as $t_2020){?>
										<td style="text-align: right;"><?=$t_2020['total']?></td>	
										<?php }?>
									</tr>
									<tr>
										<td>2021</td>
										<?php foreach( $total_ano_2021['dados'] as $t_2021){?>
										<td style="text-align: right;"><?=$t_2021['total']?></td>	
										<?php }?>
									</tr>
									<tr>
										<td>2022</td>
										<?php foreach( $total_ano_2022['dados'] as $t_2022){?>
										<td style="text-align: right;"><?=$t_2022['total']?></td>	
										<?php }?>
									</tr>
									<tr>
										<td>2023</td>
										<?php foreach( $total_ano_2023['dados'] as $t_2023){?>
										<td style="text-align: right;"><?=$t_2023['total']?></td>	
										<?php }?>									
									</tr>							
								</tbody>	
							</table>
						</div>
					<!--end::Widget 6-->
				</div>
			</div>
			<!--end:: Widgets/Sales Stats-->
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6"> 
			<!--begin:: Widgets/Top Products-->
			<div class="m-portlet m-portlet--full-height m-portlet--fit " style="background: #2c5c79;">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text" style="color: #fff;">
								Valor Pedidos Por Status Atual
							</h3>
						</div>
					</div>										
				</div>
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">						
						<div class="input-group col-md-8 col-lg-8" >
							<select class="form-control status"  tipo="status" id="status" multiple style=" border: 3px solid #ffcc00; height: 300px;">						
						<?php foreach( $status_ as $sts ){ 	?>
								<option value="<?php echo $sts['id'];?>"><?php echo $sts['descricao'];?></option>
						<?php } ?>
							</select>
							<i class="la la-usd" id="calcular_total" style="float: right;margin-top: 11px;margin-left: 11px;color: #ffcc00;font-weight: 600;"></i>
						</div>
						<div class="input-group col-md-4 col-lg-4 valor_total" style="display: none;padding-top: 0.3em;">
							<h3 style="color: #ffcc00;">R$ <span id="valor"></span></h3>
						</div>						
					</div>
				</div>
			</div>			
		</div>
		<div class="col-xl-6">
			<div class="m-portlet m-portlet--fit " >
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Produtos Mais Vendidos - Top 5
							</h3>
						</div>
					</div>										
				</div>
				<div class="m-portlet__body" style="min-height: 400px;">
					<!--begin::Widget5-->
					<div class="m-widget4 m-widget4--chart-bottom" style="">
						<?php if(is_array($produtos_vendidos)){ foreach($produtos_vendidos['dados'] as $produtos_vendido){ ?>
						<div class="m-widget4__item">												
							<div class="m-widget4__info">
								<span class="m-widget4__title">
								<?php echo $produtos_vendido['modelo'];?>	
								</span>
								<br>
								<span class="m-widget4__sub">
								
								</span>
							</div>
							<span class="m-widget4__ext">
								<span class="m-widget4__number m--font-brand">
								<?php echo $produtos_vendido['total'];?>		
								</span>
							</span>
						</div>
						<?php } } ?>
						<div class="m-widget4__chart m-portlet-fit--sides m--margin-top-20" style="height: 129px;">
							<canvas id="top_produtos_vendidos"></canvas>
						</div>
					</div>
					<!--end::Widget 5-->
				</div>
			</div>
		</div>
	</div>
	<!--End::Section-->
</div>
<!-- Modal Responsavel -->
<div class="modal fade" id="modal_orcamentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3>Orçamentos <small class="text-muted"></small></h3>
				<a href="#" id="pdf_orcamento" class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="position: absolute;right: 80px; display: none;">
					<i class="la la-print"></i>
				</a>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" style="width: 100%;">					
				<table id="example" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
			                <th style="text-align: center;">ID</th>
			                <th>Data</th>
			                <th>Valor</th>
			            </tr>
			        </thead>
			        <tfoot>
			        	
			        </tfoot>			        					    
				</table>
			</div>
			<div class="modal-footer">					
			</div>
		</div>
	</div>
</div>	
<!-- Modal Responsavel -->
<div class="modal fade" id="modal_pedidos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3>Pedidos <small class="text-muted pedidos-titulo"></small></h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" style="width: 100%;">					
				<table id="pedidos_table" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
			                <th style="text-align: center;">ID</th>
			                <th>Cliente</th>
			                <th>Status</th>
			                <th>Valor</th>
			                <th>Ver</th>
			            </tr>
			        </thead>
			        <tfoot>
			        	
			        </tfoot>			        					    
				</table>
			</div>
			<div class="modal-footer">					
			</div>
		</div>
	</div>
</div>
<!-- Modal Gráficos por período -->
<div class="modal fade" id="modal_orcamentos_periodo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content" style="height: 650px;width: 100%;">
			<div class="modal-header">
				<h3>Orçamentos <small class="text-muted"></small>
							</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" id="orcamentos_periodo_pai" style="">									
				<canvas id="orcamentos_periodo" style="cursor: pointer;"></canvas>				
			</div>
			<input type="hidden" name="periodo_status" id="periodo_status">
			<div class="modal-footer">					
			</div>
		</div>
	</div>
</div>	

<!-- Modal Tipo Relatório Pedidos Confirmados -->
<div class="modal fade" id="modal_tp_ped_conf" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content" style="width: 100%;">
			<div class="modal-header">
				<h3> Qual relatório deseja <small class="text-muted">gerar</small> </h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" style="width: 100%;text-align: center;">					<a href="<?php echo base_url('AreaAdministrador/relatorioMensalVendas'); ?>" target="_blank" class="btn" style="color: #000;background: #ffcc00; ">
					<i class="fa fa-area-chart"></i> Relatório de Vendas Mensal 
				</a> 
				<a href="<?php echo base_url('AreaAdministrador/relatorioVendasModelo'); ?>" target="_blank" class="btn" style="background: #000; color: #ffcc00; ">
					<i class="fa fa-line-chart"></i> Relatório de Vendas por Modelo
				</a>
			</div>
			<input type="hidden" name="periodo_status" id="periodo_status">
			<div class="modal-footer">					
			</div>
		</div>
	</div>
</div>

<!-- Modal Gráficos por período -->
<div class="modal fade" id="modal_pedidos_top3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content" style="height: 650px;width: 100%;">
			<div class="modal-header">
				<h3>Preço médio Top 3 <small class="text-muted"></small>
							</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" id="pedidos_periodo_pai" style="">									
				<canvas id="pedidos_periodo" style="cursor: pointer;"></canvas>				
			</div>
			
			<div class="modal-footer">					
			</div>
		</div>
	</div>
</div>

<!-- Modal Gráficos por período comparativo-->
<div class="modal fade" id="modal_pedidos_top3_c" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content" style="height: 650px;width: 100%;">
			<div class="modal-header">
				<h3>Comparativo Preço médio Top 3 <small class="text-muted"></small>
							</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" id="pedidos_periodo_pai_c" style="">									
				<canvas id="m_top_3_comparativo_c" style="cursor: pointer;"></canvas>
			</div>
			
			<div class="modal-footer">					
			</div>
		</div>
	</div>
</div>
<?php //var_dump($total_status['valor']);die;?>
<script type="text/javascript" >
	$(document).ready(function(){		

		$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
		$('.date').mask('00/00/0000');
		$('#calcular_total').bind('click', function(){
	    	
	    	$.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/calcularTotalPedidos", 
				async: true,
				data: { status 	: 	$('#status').val() }
			}).done(function(data) {
				var dados = $.parseJSON(data);
				$('#valor').text(dados.total);
				$('.valor_total').slideDown('slow');
			});

		 });

		$('#pdf_orcamento').bind('click', function(){
					
			var acao 	=	$(this).attr('acao');		
			if(acao == 'fechadosMes'){
				var mes 	= 	$(this).attr('mes');
				window.open(base_url+"AreaAdministrador/geraPdfOrcamentosFechados/"+mes);

			}else if(acao == 'status'){
				var status = $(this).attr('status')
				window.open(base_url+"AreaAdministrador/geraPdfOrcamentosStatus/"+status);
				
			}
			
		});	
		console.log(<?php echo $total_status['descricao'];?> );
		console.log(<?php echo $total_status['cor'];?> );
		console.log(<?php echo $total_status['valor']; ?>);
		var config = {
	            type: 'horizontalBar',
	            data: {
	                labels: <?php echo $total_status['descricao'];?>,
	                datasets: [{
	                    label: "Status Orçamentos",
	                    borderWidth: 2,                         
	                    backgroundColor: <?php echo $total_status['cor'];?>,
	                    data: <?php echo $total_status['valor']; ?>
	                }]
	            },
	            options: {
	                title: {
	                    display: false,
	                },
	                /*tooltips: {
	                    intersect: true,
	                    mode: 'nearest',
	                    xPadding: 10,
	                    yPadding: 10,
	                    caretPadding: 0
	                },
	                legend: {
	                    display: true
	                    
	                },*/
	                responsive: true,
	                maintainAspectRatio: false,
	                hover: {
	                    mode: 'index'
	                },
	                /*scales: {
	                    xAxes: [{
	                        display: true,
	                        gridLines: true,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Qtd'
	                        },
	                        ticks: {
	                    		beginAtZero:true,
	                    		stepSize: 1
	                		}
	                    }],
	                    yAxes: [{
	                        display: false,
	                        gridLines: true,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Status'
	                        }
	                    }]
	                }

	               	/*
	                layout: {
				        padding: {
				            left: 0,
				            right: 0,
				            top: 0,
				            bottom: 0
				        }
				    }*/
	            }
	        };

	    var chart = new Chart($('#m_status_total'), config);
	   
	    var configPed = {
	            type: 'horizontalBar',
	            data: {
	                labels: <?php echo $total_status_ped['descricao'];?>,
	                datasets: [{
	                    label: "Status Pedidos",
	                    borderWidth: 2,                         
	                    backgroundColor: <?php echo $total_status_ped['cor'];?>,                                       
	                    data: <?php echo $total_status_ped['valor']; ?>
	                }]
	            },
	            options: {
	                title: {
	                    display: false,
	                },
	                /*tooltips: {
	                    intersect: true,
	                    mode: 'nearest',
	                    xPadding: 10,
	                    yPadding: 10,
	                    caretPadding: 0
	                },*/
	                legend: {
	                    display: false
	                    
	                },
	                responsive: true,
	                maintainAspectRatio: false,
	                hover: {
	                    mode: 'index'
	                }/*,
	                scales: {
	                    xAxes: [{
	                        display: true,
	                        gridLines: true,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Qtd'
	                        },
	                        ticks: {
	                    		beginAtZero:true,
	                    		stepSize: 1
	                		}
	                    }],
	                    yAxes: [{
	                        display: false,
	                        gridLines: true,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Status'
	                        }
	                    }]
	                },

	               
	                 layout: {
				        padding: {
				            left: 0,
				            right: 0,
				            top: 0,
				            bottom: 0
				        }
				    }*/
	            }
	        };

	    var chartPed = new Chart($('#m_status_total_ped'), configPed);

	    var config1 = {
	            type: 'line',
	            data: {
	                labels: <?php echo $total_valor_mes['mes'];?>,
	                datasets: [{                    
	                    backgroundColor: 'rgba(11,123,23,0.3)',
	                    borderColor: mUtil.getColor('success'),
	                    labels: <?php echo $total_valor_mes['mes'];?>,
	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('danger'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: <?php echo $total_valor_mes['valor']; ?>
	                }]
	            },
	            options: {
	                title: {
	                    display: false,
	                },
	                tooltips: {
	                   intersect: true,
	                    /*mode: 'nearest',
	                    xPadding: 10,
	                    yPadding: 10,
	                    caretPadding: 10,*/
	                    callbacks: {
		                    label: function(tooltipItems, data) { 
		                        return numberToReal(tooltipItems.yLabel);
		                    }
		                }
	                                    
	                },
	                legend: {
	                    display: false
	                },
	                responsive: true,
	                maintainAspectRatio: false,
	                /*hover: {
	                    mode: 'index'
	                },*/
	                scales: {
	                    xAxes: [{
	                        display: false,
	                        gridLines: false,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Mês'
	                        }
	                    }],
	                    yAxes: [{
	                        display: false,
	                        gridLines: false,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Value'
	                        },
	                        ticks: {
			                    // Include a dollar sign in the ticks
			                    callback: function(value, index, values) {
			                        return '$' + value;
	                    		}
	                		}
	                    }]
	                },
	                elements: {
	                    line: {
	                        tension: 0.000001
	                    },
	                    point: {
	                        radius: 6,
	                        borderWidth: 12
	                    }
	                },
	                layout: {
	                    padding: {
	                        left: 5,
	                        right: 5,
	                        top: 10,
	                        bottom: 0
	                    }
	                }
	            }
	        };

	    var chart1 = new Chart($('#m_valor_mes'), config1);

	     
	    var config1 = {
	            type: 'line',
	            data: {
	                labels: <?php echo $total_valor_mes_ped['mes'];?>,
	                datasets: [{                    
	                    backgroundColor: 'rgba(113,106,202,0.9)',
	                    borderColor: mUtil.getColor('warning'),
	                    labels: <?php echo $total_valor_mes_ped['mes'];?>,
	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('danger'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: <?php echo $total_valor_mes_ped['valor']; ?>
	                }]
	            },
	            options: {
	                title: {
	                    display: false,
	                },
	                tooltips: {
	                   intersect: false,
	                    /*mode: 'nearest',*/
	                    xPadding: 10,
	                    yPadding: 10,
	                    caretPadding: 0,
	                    callbacks: {
		                    label: function(tooltipItems, data) { 
		                        return numberToReal(tooltipItems.yLabel);
		                    }
		                }
	                                    
	                },
	                legend: {
	                    display: false
	                },
	                responsive: true,
	                maintainAspectRatio: false,
	                 /*hover: {
	                    mode: 'index'
	                },*/
	                scales: {
	                    xAxes: [{
	                        display: false,
	                        gridLines: false,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Mês'
	                        }
	                    }],
	                    yAxes: [{
	                        display: false,
	                        gridLines: false,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Value'
	                        },
	                        ticks: {
			                    // Include a dollar sign in the ticks
			                    callback: function(value, index, values) {
			                        return '$' + value;
	                    		}
	                		}
	                    }]
	                },
	                elements: {
	                    line: {
	                        tension: 0.000001
	                    },
	                    point: {
	                        radius: 6,
	                        borderWidth: 12
	                    }
	                },
	                layout: {
	                    padding: {
	                        left: 1,
	                        right: 1,
	                        top: 10,
	                        bottom: 0
	                    }
	                }
	            }
	        };

	    var chartMP = new Chart($('#m_valor_mes_ped'), config1);
		
	    var configC = {
	            type: 'line',
	            data: {
	                labels: <?php echo $total_valor_mes_ped_c['mes'];?>,
	                datasets: [{                    
	                    backgroundColor: 'rgba(255,153,0,0.3)',
	                    borderColor: mUtil.getColor('warning'),
	                    labels: <?php echo $total_valor_mes_ped_c['mes'];?>,
	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('danger'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: <?php echo $total_valor_mes_ped_c['valor']; ?>
	                }]
	            },
	            options: {
	                title: {
	                    display: false,
	                },
	                tooltips: {
	                   intersect: false,
	                    /*mode: 'nearest',*/
	                    xPadding: 10,
	                    yPadding: 10,
	                    caretPadding: 0,
	                    callbacks: {
		                    label: function(tooltipItems, data) { 
		                        return numberToReal(tooltipItems.yLabel);
		                    }
		                }
	                                    
	                },
	                legend: {
	                    display: false
	                },
	                responsive: true,
	                maintainAspectRatio: false,
	                /*hover: {
	                    mode: 'index'
	                },*/
	                scales: {
	                    xAxes: [{
	                        display: false,
	                        gridLines: false,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Mês'
	                        }
	                    }],
	                    yAxes: [{
	                        display: false,
	                        gridLines: false,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Value'
	                        },
	                        ticks: {
			                    // Include a dollar sign in the ticks
			                    callback: function(value, index, values) {
			                        return '$' + value;
	                    		}
	                		}
	                    }]
	                },
	                elements: {
	                    line: {
	                        tension: 0.000001
	                    },
	                    point: {
	                        radius: 6,
	                        borderWidth: 12
	                    }
	                },
	                layout: {
	                    padding: {
	                        left: 1,
	                        right: 1,
	                        top: 10,
	                        bottom: 0
	                    }
	                }
	            }
	        };

	    var chartC = new Chart($('#m_valor_mes_ped_c'), configC);
		
	    var config2 = {
	            type: 'line',
	            data: {
	                labels: <?php echo $produtos_orcados['descricao'];?>,
	                datasets: [{
	                    backgroundColor: 'rgba(255, 153, 0, 0.76)',
	                    borderColor: 'rgb(255, 153, 0,1)',

	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('danger'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: <?php echo $produtos_orcados['valor']; ?>
	                }]
	            },
	            options: {
	                title: {
	                    display: false,
	                },
	                tooltips: {
	                   intersect: false,
	                    mode: 'nearest',
	                    xPadding: 10,
	                    yPadding: 10,
	                    caretPadding: 0                                    
	                },
	                legend: {
	                    display: false
	                },
	                responsive: true,
	                maintainAspectRatio: false,
	                 hover: {
	                    mode: 'index'
	                },
	                scales: {
	                    xAxes: [{
	                        display: false,
	                        gridLines: false,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Mês'
	                        }
	                    }],
	                    yAxes: [{
	                        display: false,
	                        gridLines: false,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Value'
	                        },
	                        ticks: {
			                    // Include a dollar sign in the ticks
			                    callback: function(value, index, values) {
			                        return '$' + value;
	                    		}
	                		}
	                    }]
	                },
	                elements: {
	                    line: {
	                        tension: 0.000001
	                    },
	                    point: {
	                        radius: 6,
	                        borderWidth: 12
	                    }
	                },
	                layout: {
	                    padding: {
	                        left: 5,
	                        right: 5,
	                        top: 10,
	                        bottom: 0
	                    }
	                }
	            }
	        };

	    var chart2 = new Chart($('#top_produtos_orcados'), config2);

	    var configV = {
	            type: 'line',
	            data: {
	                labels: <?php echo $produtos_vendidos['descricao'];?>,
	                datasets: [{
	                    backgroundColor: 'rgba(30, 46, 119, 0.4)',
	                    borderColor: 'rgb(87, 89, 98,1)',

	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('danger'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: <?php echo $produtos_vendidos['valor']; ?>
	                }]
	            },
	            options: {
	                title: {
	                    display: false,
	                },
	                tooltips: {
	                   intersect: false,
	                    mode: 'nearest',
	                    xPadding: 10,
	                    yPadding: 10,
	                    caretPadding: 0                                    
	                },
	                legend: {
	                    display: false
	                },
	                responsive: true,
	                maintainAspectRatio: false,
	                 hover: {
	                    mode: 'index'
	                },
	                scales: {
	                    xAxes: [{
	                        display: false,
	                        gridLines: false,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Mês'
	                        }
	                    }],
	                    yAxes: [{
	                        display: false,
	                        gridLines: false,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Value'
	                        },
	                        ticks: {
			                    // Include a dollar sign in the ticks
			                    callback: function(value, index, values) {
			                        return '$' + value;
	                    		}
	                		}
	                    }]
	                },
	                elements: {
	                    line: {
	                        tension: 0.000001
	                    },
	                    point: {
	                        radius: 6,
	                        borderWidth: 12
	                    }
	                },
	                layout: {
	                    padding: {
	                        left: 5,
	                        right: 5,
	                        top: 10,
	                        bottom: 0
	                    }
	                }
	            }
	        };

	    var chartV = new Chart($('#top_produtos_vendidos'), configV);

	   
	   var configComparativo = {
	            type: 'bar',
	            data: {
	                labels: <?php echo $total_valor_mes_ped_ant['mes'];?>,
	                datasets: [
	                {
	                	label: "2021",
	                    borderWidth: 2,                         
	                    backgroundColor: <?php echo $total_valor_mes_ped_ant_ant['cor']; ?>,                                       
	                    data: <?php echo $total_valor_mes_ped_ant_ant['valor']; ?>	
	                },{
	                    label: "2022",
	                    borderWidth: 2,                         
	                    backgroundColor: <?php echo $total_valor_mes_ped_ant['cor']; ?>,                                       
	                    data: <?php echo $total_valor_mes_ped_ant['valor']; ?>
	                },{
	                	label: "2023",
	                    borderWidth: 2,                         
	                    backgroundColor: <?php echo $total_valor_mes_ped_c['cor']; ?>,                                       
	                    data: <?php echo $total_valor_mes_ped_c['valor']; ?>	
	                }]
	            },
	            options: {
	                title: {
	                    display: true,
	                },
	                /*tooltips: {
	                    intersect: true,
	                    mode: 'nearest',
	                    xPadding: 10,
	                    yPadding: 10,
	                    caretPadding: 0,
	                    callbacks: {
		                    label: function(tooltipItems, data) {
		                    	console.log(this)
		                        return numberToReal(tooltipItems.yLabel);
		                    }
		                }
	                },*/
	                legend: {
	                    display: false
	                    
	                },
	                responsive: true,
	                maintainAspectRatio: true,
	                hover: {
	                    mode: 'index'
	                }/*,
	                scales: {
	                    xAxes: [{
	                        display: true,
	                        gridLines: true,
	                        scaleLabel: {
	                            display: true,
	                            labelString: '2019-2020-2021'
	                        },
	                        ticks: {
	                    		beginAtZero:true,
	                    		stepSize: 1
	                		}
	                    }],
	                    yAxes: [{
	                        display: false,
	                        gridLines: true,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Status'
	                        }
	                    }]
	                },

	               
	                 layout: {
				        padding: {
				            left: 0,
				            right: 0,
				            top: 0,
				            bottom: 0
				        }
				    }*/
	            }
	        };

	    var chartComparativo = new Chart($('#m_mes_comparativo'), configComparativo);
		
		console.log(<?=$total_acumulado_ant_ant['valor']?>);
		
		console.log(<?=$total_acumulado_ant['valor']?>);
		
		console.log(<?=$total_acumulado_atual['valor']?>);
	    var configComparativoAcumulado = {
	            type: 'bar',
	            data: {
	                labels: ['2021','2022','2023'],
	                datasets: [
	                {
	                	label: "Total",
	                    borderWidth: 2,                         
	                    backgroundColor: ['#FFCC00','#CCC','#1761AC'],                                       
	                    data: ['<?php echo $total_acumulado_ant_ant['valor']; ?>','<?php echo $total_acumulado_ant['valor']; ?>','<?php echo $total_acumulado_atual['valor']; ?>']	
	                }]
	            },
	            options: {
	                title: {
	                    display: false,
	                },
	                tooltips: {                                                       
	                    callbacks: {
	                        label: function(tooltipItems, data) { 
	                			return numberToReal(tooltipItems.yLabel);
	                        }
	                    }

	                },                
	                legend: {
	                    display: true
	                    
	                },
	                responsive: true,  
	                maintainAspectRatio: false	 
	            }
	        };

	    var chartComparativoAno = new Chart($('#m_ano_comparativo'), configComparativoAcumulado);		  
		//var chartComparativo = new Chart($('#m_mes_comparativo'), configComparativo);
		var table = $('#example').DataTable({"scrollX": true,
					"language": {
					"sEmptyTable": "Nenhum registro encontrado",
				    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
				    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
				    "sInfoPostFix": "",
				    "sInfoThousands": ".",
				    "sLengthMenu": "_MENU_    &nbsp;&nbsp;&nbsp;&nbsp;Resultados por página",
				    "sLoadingRecords": "Carregando...",
				    "sProcessing": "Processando...",
				    "sZeroRecords": "Nenhum registro encontrado",
				    "sSearch": "Pesquisar",
				    "oPaginate": {
				        "sNext": '<i class="la la-angle-double-right"></i>',
				        "sPrevious": '<i class="la la-angle-double-left"></i>',
				        "sFirst": "Primeiro",
				        "sLast": "Último"
				    },
				    "oAria": {
				        "sSortAscending": ": Ordenar colunas de forma ascendente",
				        "sSortDescending": ": Ordenar colunas de forma descendente"
					}
					}
				});

			var table_p = $('#pedidos_table').DataTable({"scrollX": true,
					"language": {
					"sEmptyTable": "Nenhum registro encontrado",
				    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
				    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
				    "sInfoPostFix": "",
				    "sInfoThousands": ".",
				    "sLengthMenu": "_MENU_    &nbsp;&nbsp;&nbsp;&nbsp;Resultados por página",
				    "sLoadingRecords": "Carregando...",
				    "sProcessing": "Processando...",
				    "sZeroRecords": "Nenhum registro encontrado",
				    "sSearch": "Pesquisar",
				    "oPaginate": {
				        "sNext": '<i class="la la-angle-double-right"></i>',
				        "sPrevious": '<i class="la la-angle-double-left"></i>',
				        "sFirst": "Primeiro",
				        "sLast": "Último"
				    },
				    "oAria": {
				        "sSortAscending": ": Ordenar colunas de forma ascendente",
				        "sSortDescending": ": Ordenar colunas de forma descendente"
					}
					}
				});


	    $('#m_status_total').bind('click', function(evt){

		   var activePoint = chart.getElementAtEvent(evt)[0];
		   var data = activePoint._chart.data;
		   var status = activePoint._view.label;
		   $('.text-muted').text(status).removeClass('m--font-success');
		   $.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/buscaOrcamentosPorStatus", 
				async: true,
				data: { status 	: 	status}
			}).done(function(data) {
				
				var dados = $.parseJSON(data);					
				var geral 	= [];
				var temp 	= [];
				table.clear();
				for (var i = 0; i< dados.length; i++) {
					var temp 	= [];
					id = '<a href="'+base_url+'AreaAdministrador/visualizarOrcamento/'+dados[i].id+'">'+dados[i].id+'</a>';
					valor = numberToReal(parseFloat(dados[i].valor_total));
					temp.push(id);
					temp.push(dados[i].emissao);				
					temp.push(valor);
					table.row.add(temp);
				}
				$('#pdf_orcamento').attr('status', status);
				$('#pdf_orcamento').attr('acao', 'status');
				$('#pdf_orcamento').show();

				table.draw();
				$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
				$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
				$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
				$('.dataTables_filter').css('float','right');
				$('.dataTables_scroll').attr('style','overflow: auto !important;max-height: 350px !important;');
				$('.dataTables_scrollHead').removeAttr('style'); 
				$('.dataTables_scrollHead').attr('style','overflow: auto; position: relative; border: 0px; width: 700px !important;');
				$('table thead').css('background-color', '#f4f3f8');
				$('.no-footer').removeAttr('style');
				$('.no-footer').attr('style','margin-left: 0px; width: 700px;');
				//$('table thead').css('height', '150px');
				$('table tbody tr:odd').addClass('zebraUm');
				$('table tbody tr:even').addClass('zebraDois');
				$('table tr').css('text-align','center');

			});	

		   $("#modal_orcamentos").modal({
		   	 show: true
			});
		});

		$('#m_valor_mes').bind('click', function(evt){

		   var activePoint = chart1.getElementAtEvent(evt)[0];	
		   console.log(activePoint);   
		   var mes = activePoint._xScale.ticks[activePoint._index]; 	   
		   console.log(mes);
		   $('.text-muted').text('Fechados em '+mes);
		   $('.text-muted').addClass('m--font-success');

		   $.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/buscaOrcamentosFechadosMes/", 
				async: true,
				data: { mes : mes}
			}).done(function(data) {
				
				var dados = $.parseJSON(data);					
				var geral 	= [];
				var temp 	= [];
				
				table.clear();
				for (var i = 0; i< dados.length; i++) {
					var temp 	= [];
					id = '<a href="'+base_url+'AreaAdministrador/visualizarOrcamento/'+dados[i].id+'">'+dados[i].id+'</a>';
					var valor = numberToReal(parseFloat(dados[i].valor_total));
					temp.push(id);
					temp.push(dados[i].dthr_andamento);				
					temp.push(valor);				
					table.row.add(temp);
				}

				$('#pdf_orcamento').attr('mes',mes);
				$('#pdf_orcamento').show();
				$('#pdf_orcamento').attr('acao', 'fechadosMes');
				table.draw();			
				$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
				$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
				$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
				$('.dataTables_filter').css('float','right');
				$('.dataTables_scrollHead').removeAttr('style'); 
				$('.dataTables_scrollHead').attr('style','overflow: hidden; position: relative; border: 0px; width: 700px !important;');
				$('table thead').css('background-color', '#f4f3f8');
				$('.no-footer').removeAttr('style');
				$('.no-footer').attr('style','margin-left: 0px; width: 700px;');
				//$('table thead').css('height', '150px');
				$('table tbody tr:odd').addClass('zebraUm');
				$('table tbody tr:even').addClass('zebraDois');
				$('table tr').css('text-align','center');

			});	

		   $("#modal_orcamentos").modal({
		   	 show: true
			});
		});

		$('#m_valor_mes_ped_c').bind('click', function(evt){

		   var activePoint = chartC.getElementAtEvent(evt)[0];		      
		   var mes = activePoint._xScale.ticks[activePoint._index]; 	   
		   console.log(mes);
		   $('.pedidos-titulo').text('Confirmados em '+mes);
		   $('.pedidos-titulo').addClass('m--font-success');

		   $.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/buscaPedidosConfirmadosMes/", 
				async: true,
				data: { mes : mes}
			}).done(function(data) {
				
				var dados = $.parseJSON(data);					
				var geral 	= [];
				var temp 	= [];
				
				table_p.clear();
				for (var i = 0; i< dados.length; i++) {
					var temp 	= [];
					id = '<a onclick="geraPedidosPdf('+dados[i].id+')">'+dados[i].id+'</a>';
					visualizacao = '<a onclick="geraPedidosPdf('+dados[i].id+')" target="_blank" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" style="margin-left: 1.2em;"><i class="fa fa-print m--font-brand"></i></a>';
					var valor = dados[i].valor_total;
					temp.push(id);
					temp.push(dados[i].cliente);				
					temp.push(dados[i].status);				
					temp.push(dados[i].valor);				
					temp.push(visualizacao);
					table_p.row.add(temp);
				}

				
				table_p.draw();			
				$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
				$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
				$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
				$('.dataTables_filter').css('float','right');
				$('.dataTables_scrollHead').removeAttr('style'); 
				$('.dataTables_scrollHead').attr('style','overflow: hidden; position: relative; border: 0px; width: 700px !important;');
				$('table thead').css('background-color', '#f4f3f8');
				$('.no-footer').removeAttr('style');
				$('.no-footer').attr('style','margin-left: 0px; width: 700px;');
				//$('table thead').css('height', '150px');
				$('table tbody tr:odd').addClass('zebraUm');
				$('table tbody tr:even').addClass('zebraDois');
				$('table tr').css('text-align','center');

			});	

		   $("#modal_pedidos").modal({
		   	 show: true
			});
		});

	    $('.orc_periodo').bind('click', function(){
	    	var periodo = $(this).attr('periodo');
	    	$('.text-muted').text('últimos '+periodo+' dias');
	    	$('#periodo_status').val(periodo);

	    	$("#modal_orcamentos_periodo").modal({
		   		show: true
			});

	    	$.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/buscaOrcamentosPorPeriodo", 
				async: false,
				data: { periodo 	: 	periodo	}
			}).done(function(datas) {
				
				var dados = $.parseJSON(datas);		
				console.log(dados);
				var descricao = [];		
				var total = [];		
				for (var i = 0; i< dados.length; i++) {				
					descricao.push("'"+dados[i].descricao+"'");				
					total.push(dados[i].valor);
				}
				$('#orcamentos_periodo').remove();
				$('#orcamentos_periodo_pai').append('<canvas id="orcamentos_periodo" style="cursor: pointer;"></canvas>');

				geraGraficoPeriodo(descricao,total)

			});
			
			/**/
	    });

	    $('#enviar_periodo_top3').bind('click', function(){
	    	var periodo_ini = $('#periodo_ini').val();
	    	var periodo_fim = $('#periodo_fim').val();
	    	$('.text-muted').text(periodo_ini+' a '+periodo_fim);
	    	//$('#periodo_status').val(periodo);

	    	$("#modal_pedidos_top3").modal({
		   		show: true
			});

	    	$.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/buscaPedidosTop3Periodo", 
				async: false,
				data: { periodo_ini 	: 	periodo_ini,
						periodo_fim 	: 	periodo_fim		}
			}).done(function(datas) {
				
				var dados = $.parseJSON(datas);		
				
				var descricao = [];		
				var total = [];		
				var cor = [];
				console.log(dados.cores);		
				for (var i = 0; i< dados.dados.length; i++) {				
					descricao.push(dados.dados[i].descricao);				
					total.push(dados.dados[i].total);
					cor.push(dados.cores[i]);
				}
				
				$('#pedidos_periodo').remove();
				$('#top3_periodo').remove();
				$('#pedidos_periodo_pai').append('<canvas id="top3_periodo" style="cursor: pointer;"></canvas>');
				
				geraTop3Periodo(descricao,cor,total);
				
			});
			
			/**/
	    });

	    $('#enviar_periodo_top3_c').bind('click', function(){
	    	var periodo_ini = $('#periodo_ini_c').val();
	    	var periodo_fim = $('#periodo_fim_c').val();
	    	$('.text-muted').text(periodo_ini+' a '+periodo_fim);
	    	//$('#periodo_status').val(periodo);

	    	$("#modal_pedidos_top3_c").modal({
		   		show: true
			});

	    	$.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/buscaPedidosTop3PeriodoC", 
				async: false,
				data: { periodo_ini 	: 	periodo_ini,
						periodo_fim 	: 	periodo_fim		}
			}).done(function(datas) {
				
				var dados = $.parseJSON(datas);		
				console.log(dados.clh2);		
				$('#m_top_3_comparativo_c').remove();
				$('#pedidos_periodo_pai_c').append('<canvas id="m_top_3_comparativo_c" style="cursor: pointer;"></canvas>');
				clh2_mes 	= 	[];
				clh2_valor 	= 	[];
				for (var i = 0; i< dados.clh2.dados.length; i++) {						
					clh2_mes.push(ajustaMes(dados.clh2.dados[i].mes));				
					clh2_valor.push(dados.clh2.dados[i].total);
					
				}
				chh6_mes 	= 	[];
				chh6_valor 	= 	[];
				for (var i = 0; i< dados.chh6.dados.length; i++) {						
					chh6_mes.push(ajustaMes(dados.chh6.dados[i].mes));				
					chh6_valor.push(dados.chh6.dados[i].total);
					
				}
				chhs44_mes 	= 	[];
				chhs44_valor 	= 	[];
				for (var i = 0; i< dados.chhs44.dados.length; i++) {						
					chhs44_mes.push(ajustaMes(dados.chhs44.dados[i].mes));				
					chhs44_valor.push(dados.chhs44.dados[i].total);
					
				}					
				
				geraTop3PeriodoC(clh2_mes,clh2_valor,chh6_mes,chh6_valor,chhs44_mes,chhs44_valor );
				
			});
			
			/**/
	    });
	
	    var config_top3 = {
	            type: 'bar',
	            data: {
	                labels: <?php echo $top_3['descricao'];?>,
	                datasets: [{
	                    label: "Total",
	                    borderWidth: 2,                         
	                    backgroundColor: ['#ffcc00','#ccc','#000'],                                       
	                    data: <?php echo $top_3['valor']; ?>
	                }]
	            },
	            options: {
	                title: {
	                    display: false,
	                },
					tooltips: {                                                       
	                    callbacks: {
	                        label: function(tooltipItems, data) { 
	                			return numberToReal(tooltipItems.yLabel);
	                        }
	                    }

	                }
	                /*tooltips: {
	                    intersect: true,
	                    mode: 'nearest',
	                    xPadding: 10,
	                    yPadding: 10,
	                    caretPadding: 0,
	                    callbacks: {
	                        label: function(tooltipItems, data) { 
	                        	console.log(tooltipItems);
	                            return numberToReal(tooltipItems.xLabel);
	                        }
	                    }

	                },
	                legend: {
	                    display: false
	                    
	                },
	                responsive: true,
	                maintainAspectRatio: false,
	                hover: {
	                    mode: 'index'
	                }/*,
	                scales: {
	                    xAxes: [{
	                        display: true,
	                        gridLines: true,
	                        scaleLabel: {
	                            display: true,
	                            labelString: 'Valor'
	                        },
	                        ticks: {
	                    		beginAtZero:true,
	                    		stepSize: 1
	                		}
	                    }],
	                    yAxes: [{
	                        display: true,
	                        gridLines: true,
	                         scaleLabel: {
	                            display: true,
	                            labelString: 'Modelos'
	                        },
	                        ticks: {
			                    // Include a dollar sign in the ticks
			                    callback: function(value, index, values) {
			                        return value;
	                    		}
	                		}
	                    }]
	                },
	               
	                 layout: {
				        padding: {
				            left: 0,
				            right: 0,
				            top: 0,
				            bottom: 0
				        }
				    }*/
	            }
	        };

	    var chart = new Chart($('#m_top_3_confirmados'), config_top3);

	    //comparativo top3 mes a mes m_top_3_comparativo
	    var config_top3_c = {
	            type: 'line',
	            data: {
	                labels: <?php echo $top_3_clh2['mes'];?>,
	                datasets: [{                    
	                	label: 'CLH-2',
	                    backgroundColor: 'rgba(100,149,237,0.3)',
	                    borderColor: mUtil.getColor('silver'),
	                    labels: <?php echo $top_3_clh2['mes'];?>,
	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('primary'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: <?php echo $top_3_clh2['valor']; ?>
	                },
	                {
	                	label: 'CHH-6/4',
	                    backgroundColor: 'rgba(72,209,204,0.3)',
	                    borderColor: mUtil.getColor('silver'),
	                    labels: <?php echo $top_3_chh6['mes'];?>,
	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('primary'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: <?php echo $top_3_chh6['valor']; ?>
	                },
	                {
	                	label: 'CHHS-4/4',
	                    backgroundColor: 'rgba(255,160,122,0.3)',
	                    borderColor: mUtil.getColor('silver'),
	                    labels: <?php echo $top_3_chhs44['mes'];?>,
	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('primary'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: <?php echo $top_3_chhs44['valor']; ?>
	                }]
	            },
	             options: {
	                title: {
	                    display: false,
	                },
	                tooltips: {                                                       
	                    callbacks: {
	                        label: function(tooltipItems, data) { 
	                			return numberToReal(tooltipItems.yLabel);
	                        }
	                    }

	                },                
	                legend: {
	                    display: true
	                    
	                },
	                responsive: true,  
	                maintainAspectRatio: false	                
	            	    
	            }
	       		     
	       };

	    var charts = new Chart($('#m_top_3_comparativo'), config_top3_c);

	    //comparativo top3 mes a mes m_top_3_comparativo
	    var config_total_ano = {
	            type: 'line',
	            data: {
	                labels: <?php echo $total_ano_2021['mes'];?>,
	                datasets: [/*{                    
	                	label: '2018',
	                	fill: false,
	                    backgroundColor: 'rgba(100,149,237,0.3)',
	                    borderColor: 'rgba(100,149,237,0.3)',	                    
	                    labels: <?php echo $total_ano_2018['mes'];?>,
	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: 'rgba(100,149,237,0.3)',
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: <?php echo $total_ano_2018['valor']; ?>
	                },*/
	                {
	                	label: '2019',
	                	fill: false,
	                    backgroundColor: 'rgb(255,0,0)',
	                    borderColor: 'rgb(255,0,0)',
	                    labels: <?php echo $total_ano_2019['mes'];?>,
	                   /* pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('primary'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  */
	                    data: <?php echo $total_ano_2019['valor']; ?>
	                },
	                {
	                	label: '2020',
	                	fill: false,
	                    backgroundColor: 'rgb(255,255,10)',
	                    borderColor: 'rgba(255,160,122,0.3)',
	                    labels: <?php echo $total_ano_2020['mes'];?>,
	                    /*pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('primary'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  */
	                    data: <?php echo $total_ano_2020['valor']; ?>
	                },
	                {
	                	label: '2021',
	                	fill: false,
	                    backgroundColor: 'rgb(0,255,0)',
	                    borderColor: 'rgb(0,255,0)',
	                    labels: <?php echo $total_ano_2021['mes'];?>,
	                   /* pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('primary'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  */
	                    data: <?php echo $total_ano_2021['valor']; ?>
	                },
	                {
	                	label: '2022',
	                	fill: false,
	                    backgroundColor: 'rgb(0,255,255)',
	                    borderColor: 'rgb(0,255,255)',
	                    labels: <?php echo $total_ano_2022['mes'];?>,
	                    /*pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('primary'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),*/                  
	                    data: <?php echo $total_ano_2022['valor']; ?>
	                },
	                {
	                	label: '2023',
	                	fill: false,
	                    backgroundColor: 'rgb(180,180,180)',
	                    borderColor: 'rgb(180,180,180)',
	                    labels: <?php echo $total_ano_2023['mes'];?>,
	                    /*pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('primary'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  */
	                    data: <?php echo $total_ano_2023['valor']; ?>
	                }]
	            },
	             options: {
	                title: {
	                    display: false,
	                },
	                tooltips: {                                                       
	                    callbacks: {
	                        label: function(tooltipItems, data) { 
	                			return tooltipItems.yLabel;
	                        }
	                    }

	                },                
	                legend: {
	                    display: true
	                    
	                },
	                responsive: true,  
	                maintainAspectRatio: false	                
	            	    
	            }
	       		     
	       };

	    var charts = new Chart($('#m_total_ano'), config_total_ano);

	});

    function listar_orcamentos_abertos(periodo_ini,periodo_fim){
    	$.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/buscaOrcamentosAbertos", 
			async: true,
			data: { periodo_ini 	: 	periodo_ini,
					periodo_fim 	: 	periodo_fim }
		}).done(function(data) {
			
			var dados = $.parseJSON(data);					
			var geral 	= [];
			var temp 	= [];
			table.clear();
			for (var i = 0; i< dados.length; i++) {
				var temp 	= [];
				id = '<a href="'+base_url+'AreaAdministrador/visualizarOrcamento/'+dados[i].id+'" target="_blank">'+dados[i].id+'</a>';
				valor = numberToReal(parseFloat(dados[i].valor));
				temp.push(id);
				temp.push(dados[i].emissao);				
				temp.push(valor);
				table.row.add(temp);
			}			

			table.draw();
			$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
			$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
			$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
			$('.dataTables_filter').css('float','right');
			$('.dataTables_scrollHead').removeAttr('style'); 
			$('.dataTables_scrollHead').attr('style','overflow: hidden; position: relative; border: 0px; width: 700px !important;');
			$('table thead').css('background-color', '#f4f3f8');
			$('.no-footer').removeAttr('style');
			$('.no-footer').attr('style','margin-left: 0px; width: 700px;');
			//$('table thead').css('height', '150px');
			$('table tbody tr:odd').addClass('zebraUm');
			$('table tbody tr:even').addClass('zebraDois');
			$('table tr').css('text-align','center');

		});	
		$("#modal_orcamentos").find('h3').text('ORÇAMENTOS');
	   	$("#modal_orcamentos").modal({
	   	 show: true
		});
    }

    function listar_orcamentos_negociacao(periodo_ini,periodo_fim){
    	$.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/buscaOrcamentosNegociacao", 
			async: true,
			data: { periodo_ini 	: 	periodo_ini,
					periodo_fim 	: 	periodo_fim }
		}).done(function(data) {
			
			var dados = $.parseJSON(data);					
			var geral 	= [];
			var temp 	= [];
			table.clear();
			for (var i = 0; i< dados.length; i++) {
				var temp 	= [];
				id = '<a href="'+base_url+'AreaAdministrador/visualizarOrcamento/'+dados[i].id+'" target="_blank">'+dados[i].id+'</a>';
				valor = numberToReal(parseFloat(dados[i].valor));
				temp.push(id);
				temp.push(dados[i].emissao);				
				temp.push(valor);
				table.row.add(temp);
			}			

			table.draw();
			$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
			$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
			$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
			$('.dataTables_filter').css('float','right');
			$('.dataTables_scrollHead').removeAttr('style'); 
			$('.dataTables_scrollHead').attr('style','overflow: hidden; position: relative; border: 0px; width: 700px !important;');
			$('table thead').css('background-color', '#f4f3f8');
			$('.no-footer').removeAttr('style');
			$('.no-footer').attr('style','margin-left: 0px; width: 700px;');
			//$('table thead').css('height', '150px');
			$('table tbody tr:odd').addClass('zebraUm');
			$('table tbody tr:even').addClass('zebraDois');
			$('table tr').css('text-align','center');

		});	
		$("#modal_orcamentos").find('h3').text('ORÇAMENTOS');
	   	$("#modal_orcamentos").modal({
	   	 show: true
		});
    }

    function listar_orcamentos_fechados(periodo_ini,periodo_fim){
    	$.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/buscaOrcamentosFechados", 
			async: true,
			data: { periodo_ini 	: 	periodo_ini,
					periodo_fim 	: 	periodo_fim }
		}).done(function(data) {
			
			var dados = $.parseJSON(data);					
			var geral 	= [];
			var temp 	= [];
			table.clear();
			for (var i = 0; i< dados.length; i++) {
				var temp 	= [];
				id = '<a href="'+base_url+'AreaAdministrador/visualizarOrcamento/'+dados[i].id+'" target="_blank">'+dados[i].id+'</a>';
				valor = numberToReal(parseFloat(dados[i].valor));
				temp.push(id);
				temp.push(dados[i].emissao);				
				temp.push(valor);
				table.row.add(temp);
			}			

			table.draw();
			$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
			$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
			$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
			$('.dataTables_filter').css('float','right');
			$('.dataTables_scrollHead').removeAttr('style'); 
			$('.dataTables_scrollHead').attr('style','overflow: hidden; position: relative; border: 0px; width: 700px !important;');
			$('table thead').css('background-color', '#f4f3f8');
			$('.no-footer').removeAttr('style');
			$('.no-footer').attr('style','margin-left: 0px; width: 700px;');
			//$('table thead').css('height', '150px');
			$('table tbody tr:odd').addClass('zebraUm');
			$('table tbody tr:even').addClass('zebraDois');
			$('table tr').css('text-align','center');

		});	
		$("#modal_orcamentos").find('h3').text('ORÇAMENTOS');
	   	$("#modal_orcamentos").modal({
	   	 show: true
		});
    }

    function listar_pedidos_abertos(periodo_ini,periodo_fim){
    	$.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/buscaPedidosAbertos", 
			async: true,
			data: { periodo_ini 	: 	periodo_ini,
					periodo_fim 	: 	periodo_fim }
		}).done(function(data) {
			
			var dados = $.parseJSON(data);					
			var geral 	= [];
			var temp 	= [];
			table.clear();
			for (var i = 0; i< dados.length; i++) {
				var temp 	= [];
				id = '<a href="'+base_url+'AreaAdministrador/visualizarOrcamento/'+dados[i].id+'" target="_blank">'+dados[i].id+'</a>';
				valor = numberToReal(parseFloat(dados[i].valor));
				temp.push(id);
				temp.push(dados[i].emissao);				
				temp.push(valor);
				table.row.add(temp);
			}			

			table.draw();
			$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
			$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
			$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
			$('.dataTables_filter').css('float','right');
			$('.dataTables_scrollHead').removeAttr('style'); 
			$('.dataTables_scrollHead').attr('style','overflow: hidden; position: relative; border: 0px; width: 700px !important;');
			$('table thead').css('background-color', '#f4f3f8');
			$('.no-footer').removeAttr('style');
			$('.no-footer').attr('style','margin-left: 0px; width: 700px;');
			//$('table thead').css('height', '150px');
			$('table tbody tr:odd').addClass('zebraUm');
			$('table tbody tr:even').addClass('zebraDois');
			$('table tr').css('text-align','center');

		});	
		$("#modal_orcamentos").find('h3').text('PEDIDOS');
	   	$("#modal_orcamentos").modal({
	   	 show: true
		});
    }

    function numberToReal(numero) {
	    var numero = numero.toFixed(2).split('.');
	    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
	    return numero.join(',');
	}

	function geraGraficoPeriodo(descricao,total){

			var config3 = {
            type: 'horizontalBar',
            data: {
                labels: descricao,
                datasets: [{
                    label: "Status Orçamentos",
                    borderWidth: 2,                                            
                    data: total
                }]
            },
            options: {
                title: {
                    display: false,
                },
                
                legend: {
                    display: false
                    
                },
                responsive: true,                            
                hover: {
                    mode: 'index'
                }
                
            }
        	};
		/*var config3 = {
	      type: 'horizontalBar',
	      data: {
	        datasets: [{
	          data: total,
	          
	          label: descricao
	        }],
	        labels: 
	          descricao
	        
	      },
	      options: {
	        responsive: true
	      }
	    };*/

    	var chart5	=	new Chart($('#orcamentos_periodo'), config3);

    	$('#orcamentos_periodo').bind('click', function(evt){
		   var activePoint 	=	chart5.getElementAtEvent(evt)[0];
		   var data 		=	activePoint._chart.data;
		   var status 		=	activePoint._view.label;
		   var periodo 		= 	$('#periodo_status').val();
		   $('.text-muted').text('Status: '+status+' | Período: '+periodo+' dias').removeClass('m--font-success');

		   $.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/buscaOrcamentosPorStatusPeriodo", 
				async: true,
				data: { status 	: 	status,
						periodo : 	periodo	 }

			}).done(function(data) {
				
				var dados = $.parseJSON(data);					
				var geral 	= [];
				var temp 	= [];
				table.clear();
				for (var i = 0; i< dados.length; i++) {
					var temp 	= [];
					id = '<a href="'+base_url+'AreaAdministrador/visualizarOrcamento/'+dados[i].id+'">'+dados[i].id+'</a>';
					valor = numberToReal(parseFloat(dados[i].valor_total));
					temp.push(id);
					temp.push(dados[i].emissao);				
					temp.push(valor);
					table.row.add(temp);
				}

				table.draw();

				$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
				$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
				$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
				$('.dataTables_filter').css('float','right');
				$('.dataTables_scrollHead').removeAttr('style');
				$('.dataTables_scrollHead').attr('style','overflow: auto; position: relative; border: 0px; width: 700px !important;');
				$('table thead').css('background-color', '#f4f3f8');
				$('.no-footer').removeAttr('style');
				$('.no-footer').attr('style','margin-left: 0px; width: 700px;');
				$('table tbody tr:odd').addClass('zebraUm');
				$('table tbody tr:even').addClass('zebraDois');
				$('table tr').css('text-align','center');

			});	
			$("#modal_orcamentos_periodo").modal('hide');
		   	$("#modal_orcamentos").modal({
		   		show: true
			});
		});

	}

	function aprovarOrcamento(orcamento_id){
		$.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/confirmaOrcamentoGestor", 
			async: true,
			data: { orcamento_id 	: 	orcamento_id 	}

		}).done(function(data) {
			var dados = $.parseJSON(data);	
			if( dados.retorno == 'sucesso'){
				swal({
					title: 	"OK!",
					text: 	'Orçamento aprovado com sucesso!',
					type: 	"success"
				}).then(function() {
			      	window.location = base_url+'AreaAdministrador/';
			   	}); 
			}

		});
	}

	function geraPedidosPdf(pedido_id){

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/geraPedidosPdf/"+pedido_id+"/ajax/", 
			async: true,
			data: { pedido_id : pedido_id}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno != 'erro')
			{
				window.open(base_url+'pedidos/'+dados.retorno);

			}else{
				swal({
		   			title: "Atenção!",
		   			text: "Erro ao gerar pdf do pedido, entre em contato com o webmaster",
		   			type: 'warning'
		    	}).then(function() {
		    	   	
		    	});
			}

		});	
		
	}

	function aprovarPedido(pedido_id){
		$.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/solicitaLiberacao", 
			async: true,
			data: { pedido_id 	: 	pedido_id }

		}).done(function(data) {
			var dados = $.parseJSON(data);	
			if( dados.retorno == 'sucesso'){
				swal({
					title: 	"OK!",
					text: 	'Pedido aprovado com sucesso e enviado ao financeiro para aguardar liberação do pedido!',
					type: 	"success"
				}).then(function() {
			      	$('.m-widget4[pedido_id="'+pedido_id+'"]').fadeOut('slow', function(){
			      		$(this).remove();
			      		console.log($('.pedidos_a_aprovar' ).length);
			      		if( $('.pedidos_a_aprovar' ).length == 0 ){
			      			$('.pedidos_a_confirmar').append('<h4 class="m--font-warning">Não há solicitações de pedidos a serem aprovados!</h4>');
			      		}
			      	});
			      	
			   	}); 
			}

		});
	}

	function abre_modal_tp_conf(){
		$('#modal_tp_ped_conf').modal({
		   		show: true
		});
	}
	
	function geraTop3Periodo(descricao, cor, valor ){
		console.log(descricao);
		console.log(cor);
		console.log(valor);
		var config_top_3 = {
            type: 'horizontalBar',
            data: {
                labels: descricao,
                datasets: [{
                    label: "Total",
                    borderWidth: 2,                                            
                    backgroundColor: ['#ffcc00','#ccc','#000'],                                       
                    data: valor
                }]
            },
            options: {
                title: {
                    display: false,
                },
                tooltips: {                                                       
                    callbacks: {
                        label: function(tooltipItems, data) { 
                        	console.log(tooltipItems);
                            return numberToReal(tooltipItems.xLabel);
                        }
                    }

                },                
                legend: {
                    display: false
                    
                },
                responsive: true,  
                maintainAspectRatio: false,                          
                hover: {
                    mode: 'index'
                }
                
            }
            
        };
		

    	var chart10	=	new Chart($('#top3_periodo'), config_top_3);		
        
	}

	function geraTop3PeriodoC(clh2_mes,clh2_valor, chh6_mes,chh6_valor, chhs4_mes,chhs4_valor ){
		
		var config_top3_c = {
	            type: 'line',
	            data: {
	                labels: clh2_mes,
	                datasets: [{                    
	                	label: 'CLH-2',
	                    backgroundColor: 'rgba(100,149,237,0.3)',
	                    borderColor: mUtil.getColor('silver'),
	                    labels: clh2_mes,
	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('primary'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: clh2_valor
	                },
	                {
	                	label: 'CHH-6/4',
	                    backgroundColor: 'rgba(72,209,204,0.3)',
	                    borderColor: mUtil.getColor('silver'),
	                    labels: chh6_mes,
	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('primary'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: chh6_valor
	                },
	                {
	                	label: 'CHHS-4/4',
	                    backgroundColor: 'rgba(255,160,122,0.3)',
	                    borderColor: mUtil.getColor('silver'),
	                    labels: chhs4_mes,
	                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
	                    pointHoverBackgroundColor: mUtil.getColor('primary'),
	                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
	                    data: chhs4_valor
	                }]
	            },
	             options: {
	                title: {
	                    display: false,
	                },
	                tooltips: {                                                       
	                    callbacks: {
	                        label: function(tooltipItems, data) { 
	                			return numberToReal(tooltipItems.yLabel);
	                        }
	                    }

	                },                
	                legend: {
	                    display: true
	                    
	                },
	                responsive: true,  
	                maintainAspectRatio: false	                
	            	    
	            }
	       		     
	       };
	    console.log(config_top3_c);
	    var charts = new Chart($('#m_top_3_comparativo_c'), config_top3_c);
	   
	}

	function ajustaMes(mes){
		var label_mes = "";
		switch(mes) {
		case '1':
			label_mes = 'Janeiro';
		    break;
		case '2':
		    label_mes = 'Fevereiro';
		    break;
		case '3':
			label_mes = 'Março';
		  break;
		case '4':
			label_mes = 'Abril';
		    break;
		case '5':
			label_mes = 'Maio';
		    break;
		case '6':
			label_mes = 'Junho';
		    break;
		case '7':
			label_mes = 'Julho';
		    break;
		case '8':
			label_mes = 'Agosto';
		    break;
		case '9':
			label_mes = 'Setembro';
		    break;
		case '10':
			label_mes = 'Outubro';
		    break;
		case '11':
		    label_mes = 'Novembro';
		    break;
		case '12':
		    label_mes = 'Dezembro';
		    break;
		  
		}
		return label_mes;
	}
</script>
<?php } ?>