<div class="m-content">	
	<form accept-charset="utf-8" action="<?php echo base_url('AreaAdministrador/orcamentosAdm/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;" id="formulario">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-list-2"></i>
					</span>
					<h3 class="m-portlet__head-text"> 
						Solicitação de Orçamento 
					</h3>
				</div>			
			</div> 
			<div class="m-portlet__head-tools">
				
			</div>
		</div>
				
		<div class="m-portlet__body">
			<div class="form-group m-form__group m--margin-top-10">
				<div class="alert m-alert m-alert--default" role="alert">
					Insira os dados da empresa/pessoa interessada nos produtos WERTCO!
				</div>
			</div>		
			<div class="form-group m-form__group row">									
				<div class="col-lg-12">
					<label><b>Tipo de Cadastro:</b></label>
					<div class="m-radio-inline">
						<label class="m-radio">
							<input type="radio" name="tp_cadastro" value="PJ" class="tp_cadastro" />
							Pessoa Jurídica
							<span></span>
						</label>
						<label class="m-radio">
							<input type="radio" name="tp_cadastro" value="PF" class="tp_cadastro" />
							Pessoa Física
							<span></span>
						</label>
						<label class="m-radio">
							<input type="radio" name="tp_cadastro" value="ES" class="tp_cadastro" />
							Estrangeiro
							<span></span>
						</label>
					</div>											
				</div>	
			</div>
			<div id="cadastro">
			<div class="form-group m-form__group row">
				<div class="col-lg-3">
					<label id='label_documento'>Documento (CNPJ/CPF):</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="cnpj" id="cnpj" class="form-control m-input" placeholder="Insira um cnpj" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
						<input type="hidden"  id="salvar" maxlength="15" name="salvar" value="1"    required />
						<input type="hidden"  id="cartao_cnpj" name="cartao_cnpj"  />
                        <input type="hidden"  id="empresa_id" maxlength="15" name="empresa_id" value=""  />
                        <input type="hidden"  id="pesquisa" name="pesquisa" value="<?php echo $pesquisa; ?>"  />
					</div>					
				</div>
				<div class="col-lg-3">
					<label id="label_razao">Razão Social:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required	name="razao_social" id="razao_social" class="form-control m-input" placeholder="" required value="">
						<input type="hidden" name="id" 	class="form-control m-input" placeholder="Insira uma razão social" required value="">								
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
						</span>
					</div>
				</div>
				<div class="col-lg-3" id="campo_fantasia">
					<label class="">Fantasia:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="fantasia" id="fantasia" class="form-control m-input" placeholder="Insira uma fantasia" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>	
				</div>
				<div class="col-lg-3" id="campo_ie">
					<label class="">Inscrição Estadual:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control m-input" placeholder="Inscrição Estadual" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>	
				</div>					
				
			</div>	 
			<div class="form-group m-form__group row">	
				<div class="col-lg-3">
					<label>E-mail:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="email" required name="email" id="email" class="form-control m-input" placeholder="Insira um email" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>							
				</div>			
				<div class="col-lg-3">
					<label>Telefone:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="telefone" id="telefone" class="form-control m-input" placeholder="Insira um telefone" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Cep:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="cep" id="cep" class="form-control m-input" placeholder="Insira um cep" required value="" >
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Endereço:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="endereco" id="endereco" class="form-control m-input" placeholder="Insira um endereço" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
					</div>							
				</div>
				
			</div>
			<div class="form-group m-form__group row">
				<div class="col-lg-3">
					<label>Bairro:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="bairro" id="bairro" class="form-control m-input" placeholder="Insira o bairro" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
					</div>							
				</div>	
				<div class="col-lg-3">
					<label class="">Cidade:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" 		name="cidade" required 		id="cidade" 	class="form-control m-input" placeholder="Insira um cidade" value="">
						<input type="hidden"	name="codigo_ibge_cidade" 	id="codigo_ibge_cidade" 	value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
					</div>					
				</div>
				<div class="col-lg-3">
					<label class="">Estado:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="hidden" id="estadoE" class="form-control" placeholder="Insira um estado" />
						<select  class="form-control"  maxlength="250" name="estado" placeholder="Estado" id="estado" required>
                            <option value="">Selecione um Estado</option>
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AP">Amapá</option>
                            <option value="AM">Amazonas</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PR">Paraná</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SP">São Paulo</option>
                            <option value="SE">Sergipe</option>
                            <option value="TO">Tocantins</option>
                        </select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
					</div>							
				</div>					
				<div class="col-lg-3">
					<label>País:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="pais" id="pais" class="form-control m-input" placeholder="Insira um pais" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map"></i></span></span>
					</div>							
				</div>
			</div>	
			<hr/> 
			<div class="form-group m-form__group row">
				<div class="col-lg-4">
					<label>Contato do Posto:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="contato_posto" id="contato_posto" class="form-control m-input" placeholder="Contato do Posto" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>							
				</div>			
				<div class="col-lg-4">
					<label>Celular/Telefone:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="tel_contato_posto" id="tel_contato_posto" class="form-control m-input" placeholder="Insira um telefone/celular" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-4">
					<label>E-mail Contato do posto:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="email" required name="email_contato_posto" id="email_contato_posto" class="form-control m-input" placeholder="Insira um e-mail" required value="" >
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>							
				</div>	
			</div>
			<hr/>	
			<div class="form-group m-form__group row">
				<div class="col-lg-4">
					<label><b>Houve Indicação:</b></label>
					<div class="m-radio-inline">
						<label class="m-radio">
							<input type="radio" name="indicacao" value="1" class="indicacao" />
							Sim
							<span></span>
						</label>
						<label class="m-radio">
							<input type="radio" name="indicacao" value="0" class="indicacao" />
							Não
							<span></span>
						</label>
					</div>							
				</div>
				<div class="col-lg-4">
					<label><b>Previsão de Fechamento:</b></label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="dt_previsao" id="dt_previsao" class="form-control m-input date" placeholder="Insira uma data" value="<?=date('d/m/Y',strtotime('+68 days'))?>" required="required">	
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar-o"></i></span></span>
					</div>
				</div>	
				<div class="col-lg-4">
					<label><b>Previsão de Produção:</b></label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="dt_previsao_prod" id="dt_previsao_prod" class="form-control m-input date" placeholder="Insira uma data" value="<?=date('d/m/Y',strtotime('+68 days'))?>" required="required">	
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar-o"></i></span></span>
					</div>
				</div>			
			</div>				
			
			<div class="form-group m-form__group row">
				<div class="col-lg-12 nome_indicador" style="display: none;">
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="nome_indicador" id="nome_indicador" class="form-control m-input" placeholder="Insira o nome/razão social do indicador" value="" />
						<input type="hidden" name="indicador_id" id="indicador_id" class="form-control m-input" placeholder="Insira o nome/razão social do indicador" value="" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>					
				</div>
			</div>
			<hr/>
			<div class="form-group m-form__group row">
				<div class="col-lg-12">
					<label><b>Observação:</b></label>
					<div class="m-input-icon m-input-icon--right">
						<textarea  name="observacao" id="observacao" class="form-control m-input" ></textarea>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>												
				</div>				
			</div>
			<div class="form-group m-form__group row">
				<div class="col-lg-12">
					<label class="">Consultor Responsável:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="responsavel_id" placeholder="Responsavel" id="responsavel" required>
							<option value=""> -- Selecione um consultor Wertco --</option>
							<option value="7167">CINDIEL</option>
							<option value="3386">FÁBIO</option>
							<option value="2465">LEONARDO</option>
							<option value="10">ODAIR</option>
							<option value="5533">SEBASTIAN</option>
						</select>
					</div>
				</div>
			</div>	
			</div>		
		</div>
	</div>
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	***************************************** Forma de entrega, fretes e forma de pagto *******************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">					
					<h2 class="m-portlet__head-label m-portlet__head-label--warning" style="width: 320px;">
						<span>
							Forma de Pagto, Entrega e Frete
						</span>
					</h2>
				</div>
			</div>			
		</div>
		<div class="m-portlet__body">
			<div class="form-group m-form__group">
				<label for="Frete">
					Forma de Pagamento
				</label>
				<select class="form-control m-input m-input--square" id="forma_pagto" name="forma_pagto">
					<option value="0">Selecione a forma de pagamento</option>					
					<?php foreach ($formaPagto as $pagto) { ?>
						<option value="<?php echo $pagto->id;?>"><?php echo $pagto->descricao; ?></option>								
					<?php } ?>
				</select>
			</div>
			<div class="form-group m-form__group">
				<label for="Frete">
					Entrega
				</label>
				<select class="form-control m-input m-input--square" id="entregas" name="entrega">
					<option value="0">Selecione como vai ser realizado a entrega do(s) produto(s)</option>					
					<?php foreach ($entregas as $entrega) { ?>
						<option value="<?php echo $entrega->id;?>" ><?php echo $entrega->descricao; ?></option>								
					<?php } ?>
				</select>
			</div>
			<div class="form-group m-form__group">
				<label for="Frete">
					Frete
				</label>
				<select class="form-control m-input m-input--square" id="frete" name="frete">
					<option value="0">Selecione o tipo de frete utilizado</option>		
					<?php foreach ($fretes as $frete) { ?>
						<option value="<?php echo $frete->id;?>"  <?=($frete->id == 3) ? 'selected="selected"' : ''?> ><?php echo $frete->descricao; ?></option>			
					<?php } ?>
				</select>
			</div>
			<div class="form-group m-form__group">
				<label for="Garantia">
					Garantia
				</label>
				<select class="form-control m-input m-input--square" id="garantia" name="garantia">
					<option value="0">Selecione o tipo de garantia utilizado</option>		
					<option value="12">12 Meses</option>			
					<option value="24">24 Meses</option>
				</select>
			</div>
		</div>
	</div>	
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	********************************************************* Produtos ************************************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>Produtos</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<table class="table m-table m-table--head-separator-warning">
			  	<thead>
			    	<tr>			      		
			      		<th>Modelo | Descrição</th>
			      		<th>Quantidade</th>
			      		<th>Valor Unitário R$</th>
			      		<th>
			      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add" data-toggle="m-tooltip" data-placement="top" title="Adicionar produto">
								<i class="la la-plus"></i>
							</a>
						</th>				      		
			    	</tr>
			  	</thead>
			  	<tbody>
			  		 <tr id="modelo" indice='0' total_indice='0'>
                            <td>
                                <?php 
                                        $bombas_op     		=   "";
                                        $opcionais_op  		=   "";
                                        $bombas_op_slim  	=   "";
                                        $bombas_op_baixa  	=   "";
                                        $bombas_dispenser 	= 	"";

                                        foreach( $bombas as $bomba )
                                        {
                                            if( $bomba->tipo_produto_id == 4 ){
                                                $opcionais_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'>".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }elseif($bomba->tipo_produto_id == 1){
                                                $bombas_op_baixa.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'>".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }elseif($bomba->tipo_produto_id == 2){
                                                $bombas_op_slim.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'>".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }elseif($bomba->tipo_produto_id == 3){
                                                $bombas_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'>".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }elseif($bomba->tipo_produto_id == 6){
                                                $bombas_dispenser.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'>".$bomba->modelo." | ".$bomba->descricao."</option>";
                                            }
                                        } 
                                ?>
                                <select  class="form-control bombas" style="font-size: 15px;" name="produto[]" indice='0'>
                                	<option value="">Selecione um produto</option>
                                    <optgroup label="Bombas Mangueira Alta" data-subtext="optgroup subtext">
                                        <?php echo $bombas_op; ?>
                                    </optgroup>    
                                    <optgroup label="Bombas Mangueira Slim" data-subtext="optgroup subtext">
                                        <?php echo $bombas_op_slim; ?>
                                    </optgroup>    
                                    <optgroup label="Bombas Mangueira Baixa" data-subtext="optgroup subtext">
                                        <?php echo $bombas_op_baixa; ?>
                                    </optgroup>
                                    <optgroup label="Dispensers" data-subtext="optgroup subtext">
                                        <?php echo $bombas_dispenser; ?>
                                    </optgroup>
                                    <optgroup label="Opcionais" data-subtext="optgroup subtext">
                                        <?php echo $opcionais_op; ?>
                                    </optgroup>
                                </select>
                            </td>
                            <td>
                                <input type="text" maxlength="3" class="form-control qtd" style="width: 70px;" name="quantidade[]" required />
                            </td>
                            <td style="font-size: 15px;">
                                <input type="text" name="valor[]" class="form-control valor_unitario" value="" id="valor_unitario_0" indice="0" maxlength="14" />
                            </td>
                            <td>
                                <i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
                            </td>

                        </tr>
                    </tbody>
                </table> 
                <div style="text-align: center;">
                    <button type="submit" id="enviar" class="btn btn-warning" >Solicitar orçamento</button>    
                </div>             			
		</div>
	</div>
	</form>
</div>
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Orçamento solicitado com sucessoo, em breve entraremos em contato!',
                type: 'success'
            }).then(function() {
                window.location = base_url+'AreaAdministrador/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>