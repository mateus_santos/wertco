<table class="" id="html_table" width="100%">
	<thead class="thead-dark">
		<tr>
			<th style="text-align: center;">Número</th>
			<th style="text-align: center;">Solicitante</th>
			<th style="text-align: center;">Emissão</th>											
			<th style="text-align: center;">Status</th>																		
			<th style="text-align: center;">Ações</th>
		</tr>
	</thead>
	<tbody >		
		<tr>
			<td style="width: 5%;text-align: center;"><?php echo $dados->id; ?></td>
			<td style="width: 30%;text-align: center;"> <?php echo $dados->cliente; ?></td>						
			<td style="width: 10%;text-align: center;"><?php echo date('d/m/Y H:i:s', strtotime($dados->emissao)).'- '.$dados->criado_por; ?></td>	
			<td style="width: 15%;text-align: center; text-transform: capitalize;">
				<button class="btn m-btn--pill m-btn--air btn-secondary" title="Status do orçamento">
				 <?php echo ucfirst($dados->status); ?>   
				</button>
				
			</td>
			<td data-field="Actions" class="m-datatable__cell " style="width:5%;">
				<span >
					<a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill visualizar_orcamento" onclick="visualizar_orcamento('<?php echo $dados->id; ?>')" title="Visualizar" id="'<?php echo $dados->id; ?>'" target="_blank" >
                                                    <i class="la la-eye visualizar"></i>
                                                </a>							
					
                     										
				</span>
			</td>
		</tr>
		
		</tbody>
	</table>