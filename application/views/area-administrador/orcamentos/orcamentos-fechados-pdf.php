<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px;
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}

table td{		
	font-size: 9px;	
}



</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
										07400-230 - Arujá - SP<br>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br>
			</p>
		</div>
		<div class="corpo">				
		<h3 style="text-align: center;margin-top: 15px;	">Orçamentos Fechados em <?php echo $mes; ?></h3>	

		<table class="table" cellspacing="0" style="margin-top: 15px;">
			<thead bgcolor="#ccc" style="background-color: #ccc;font-size: 10px;">
				<tr>
					<th>#</th>
					<th>CLIENTE</th>
					<th>ESTADO</th>
					<th>INDICADOR</th>
					<th>PRODUTOS</th>
					<th>EMISSÃO</th>
					<th>VALOR</th>
				</tr>
			</thead>
			<tbody>

				<?php $i=0;	foreach( $orcamentos as $orcamento ){ 	?>

				<tr bgcolor="<?php echo (($i%2) == 0) ? '#f0f0f0' : '#ffffff'; ?>" >
					<td cellspacing="0" cellpadding="0" style="text-align: center;">
						<?php echo $orcamento['id'];?>
					</td>										
					<td cellspacing="0" style="text-align: left;">
						<?php echo strtoupper($orcamento['cliente']);	?>	
					</td>
					<td cellspacing="0" style="text-align: center;">
						<?php echo $orcamento['estado']; ?>							
					</td>
					<td cellspacing="0"  style="text-align: center;">
						<?php echo strtoupper($orcamento['indicador']);?> 				
					</td>
					<td cellspacing="0" style="text-align: center;">
						<?php echo utf8_decode($orcamento['produtos']);?>					
					</td>
					<td style="text-align: center;">
						<?php echo date('d/m/Y', strtotime($orcamento['emissao'])) ; ?>
					</td>
					<td cellspacing="0" style="text-align: center;"><?php echo 'R$ '.number_format($orcamento['valor_total'],2,',','.'); ?></td>
					
				</tr>
				<?php $i++; } ?>				
			</tbody>
		</table>
		
	</div>	
</div>
</body>
</html>