<style type="text/css">

ul.timeline {
    list-style-type: none;
    position: relative;
}

ul.timeline:before {
    content: ' ';
    background: #d4d9df;
    display: inline-block;
    position: absolute;
    left: 29px;
    width: 2px;
    height: 100%;
    z-index: 400;
}

ul.timeline > li {
    margin: 20px 0;
    padding-left: 20px;
}

ul.timeline > li:before {
    content: ' ';
    background: white;
    display: inline-block;
    position: absolute;
    border-radius: 50%;
    border: 3px solid #ffcc00;
    left: 20px;
    width: 20px;
    height: 20px;
    z-index: 400;
}

</style>
<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-diagram" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Pesquisar Orçamentos
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >				
			<div class="form-group m-form__group row">								
				<div class="col-lg-4">
					<label class="">Clientes: </label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" class="form-control" id="clientes" placeholder="pesquise um cliente..." />
						<input type="hidden" name="cliente_id" id="cliente_id" />
						<input type="hidden" id="pesquisa" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>							
				</div>		
			</div>			
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="button" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>							
					</div>
				</div>
			</div>	
		</div>						
	</div>
	<!--end::Portlet-->	

	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" style="display: none;">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>
							Negociações
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->			
		</div>
	</div>

	
	<!--end::Portlet-->	
</div>	
<div class="modal fade" id="m_andamentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_andamento_titulo" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
							
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group status_orcamento_" >
						<label for="exampleSelect1">Situação do Orçamento</label>
						<select class="form-control m-input m-input--air" id="status_orcamento">
							<option value="">Selecione o status do orçamento</option>						
							<option value="1">Aberto</option>
							<option value="5">Orçamento entregue</option>
							<option value="6">Em negociação</option>
							<option value="2">Fechado</option>							
							<option value="10">Perdido para Wayne</option>
							<option value="11">Perdido para Gilbarco</option>
							<option value="3">Perdido Outros</option>
							<option value="4">Cancelado</option>
							<option value="9">Em Testes</option>

						</select>
					</div>
					<div class="form-group m-form__group aprova_indicacao" style="display: none;" >
						<label for="exampleSelect1">Aprovar esta Indicação?</label>
						<div class="m-radio-inline">
							<label class="m-radio">
								<input type="radio" name="aprovar_indicacao" class="aprova_inidicacao" value="1" checked="checked" />
								Sim
								<span></span>
							</label>
							<label class="m-radio">
								<input type="radio" name="aprovar_indicacao" class="aprova_inidicacao" value="0" />
								Não
								<span></span>
							</label>							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" class="btn btn-primary" orcamento_id="" indicacao="0">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaFeira/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>
		<?php if ($this->session->flashdata('exclusao') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Orçamento excluído com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaAdministrador/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['exclusao']); } ?>