<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

	<style type="text/css">

		body {
			background-color: #fff;
			margin: 0 auto;
			position: relative;
			font-family: Lucida Grande, Verdana, Sans-serif;
			font-size: 12px;
			color: #333;
			width: 100%;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 14px; 
			font-weight: bold;
			margin: 24px 0 2px 0; 	
		}
		.cabecalho{
			margin: 0 auto;
			position: relative;
		}
		table{	
			padding: 0;			
			border: 1px solid #ffcc00;

		}
		table tr{	

		}
		table td{	
			border-bottom: 1px solid #f0f0f0;	
			font-size: 9px;	
			padding: 5px;
		}


	</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
				CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
				Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
				07400-230 - Arujá - SP<br>
				Tel: +55 11 3900-2565 | www.wertco.com.br<br>
			</p>
		</div>
		<div class="corpo" >	
			<h3 style="text-align: center;margin-top: 15px;	">Relatório Orçamentos </h3>		
			<p style="text-align: right;font-size: 8px;">Gerado por <?=mb_strtoupper($this->session->userdata('nome')).' - '. date('d/m/Y H:i:s')?> </p>
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;border: 1px solid #ffcc00;width: 100% !important;">
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>#ID</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Cliente</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Cidade</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>UF</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Contato</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Produtos</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Valor</b></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Status</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Emissão</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Indicador</b></td>										
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Telefone</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Responsável</b></td>					
				</tr>				
				<?php $total = 0; $total_bombas = 0; foreach($dados['orcamento'] as $dado){ ?>
					<tr>
						<td style="border-top: 1px solid #ccc;"> <?php echo $dado['orcamento_id']?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo mb_strtoupper($dado['cliente']); ?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo mb_strtoupper($dado['cidade']); ?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo mb_strtoupper($dado['estado']); ?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo mb_strtoupper($dado['contato']) . ' / '. $dado['contato_tel'].' / '.$dado['telefone'] . ' / '.$dado['email'] . ' / '. $dado['contato_email']; ?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo $dado['bombas']?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo 'R$ '. number_format($dado['valor_total'], 2, ',', '.');?> </td>						
						<td style="border-top: 1px solid #ccc;"> <?php echo mb_strtoupper($dado['status']); ?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo date('d/m/Y H:i:s', strtotime($dado['emissao'])); ?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo $dado['indicador'].' - '.$dado['indi_cidade']; ?> </td>
						<td style="border-top: 1px solid #ccc;"> <?php echo $dado['indi_telefone']; ?> </td>
						<td style="border-top: 1px solid #ccc;border-right: 1px solid #ffcc00 !important;"> <?php echo mb_strtoupper($dado['consultor_resp']); ?> </td>
					</tr>
					<?php 	
					$total 			= 	$dado['valor_total'] + $total;
					$total_bombas 	= 	$dado['qtd_bombas'] + $total_bombas;
				} 
				?>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;border-right: 1px solid #ffcc00;" colspan="12">Total: R$ <?php echo number_format($total,2,',','.');?> </td>
				</tr>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;border-right: 1px solid #ffcc00;" colspan="12">Total de Bombas: <?php echo $total_bombas;?> </td>
				</tr>
			</table>			
		</div>	
	</div>
</body>
</html>