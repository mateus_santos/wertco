<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	padding: 0;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px;
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
	padding: 0;
}
table{	
	padding: 5px;
	width: 100%;
	
	
}
table tr{	
	padding: 5px !important;	
}
table td{
	padding: 2px !important;	
	font-size: 9px;	
}

.borda{
	border: 1px solid #ffcc00;
}

</style>
</head>
<body>
	<div class="conteudo"  >
		<img src="./bootstrap/img/capa.jpg" width="100%" style="">
		<div class="cabecalho" style="PAGE-BREAK-BEFORE: always">			
			<table border="0">
				<tr>
					<td style="margin: 5px auto;">
						<img STYLE="width: 150px;" src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" >
					</td>
					<td style="text-align: right; font-size: 8px;margin-top: -10px;">
						<p > WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
										07400-230 - Arujá - SP<br>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br>
						</p>
					</td>			
				</tr>
			</table>
		</div>
		<div class="corpo">	
			<h3 style="text-align: center;margin-top: 75px;	">PROPOSTA PARA O FORNECIMENTO DE BOMBA COMERCIAL</h3>		
			<table cellspacing="0" style="" class="borda" >
				<tbody >
				<tr>
					<td style=" width: 15% !important;">ORÇAMENTO Nº: </td>
					<td><?php echo $empresa['orcamento_id']; ?></td>
					<td style="text-align: left;">GERAÇÃO:	</td>
					<td style="width: 15% !important;"><?php echo $empresa['emissao']; ?></td>
					<td style="text-align: left;">EMISSÃO: </td>
					<td style="width: 15% !important;"><?php echo date('d/m/Y H:i:s');?> </td>
					<td style="text-align: right;width: 8% !important;">VALIDADE: </td>
					<td><?php echo date('d/m/Y',strtotime($empresa['emissao'].' +15 days')) /*if($empresa['dt_previsao'] == '0000-00-00') {
								echo (($empresa['fl_especial'] == 1) ? 5 : 15) - $emissao['dias'] .' dias' ;
							}else{
								echo date('d/m/Y',strtotime($empresa['dt_previsao']));
							}*/
							
							?> 
					</td>
				</tr>
				<tr>
					<td><?php echo ($empresa['tp_cadastro'] == 'PJ' || $empresa['tp_cadastro'] == '') ? 'RAZÃO SOCIAL' : 'NOME'; ?>: </td>
					<td colspan="7"><?php echo mb_strtoupper($empresa['razao_social']); ?> </td>					
				</tr>
				<tr>
					<td><?PHP echo ($empresa['tp_cadastro'] == 'PJ' || $empresa['tp_cadastro'] == '') ? 'CNPJ' : 'CPF'; ?>: </td>
					<td><?php echo $empresa['cnpj']; ?> </td>					
					<td><?PHP echo ($empresa['tp_cadastro'] == 'PJ' || $empresa['tp_cadastro'] == '') ? 'I.E.:' : ''; ?> </td>					
					<td colspan="5"><?php echo $empresa['inscricao_estadual']; ?> </td>					
				</tr>
				<tr>
					<td>TELEFONE: </td>
					<td><?php echo $empresa['telefone']; ?> </td>					
					<td>ENDEREÇO: </td>					
					<td colspan="5"><?php echo mb_strtoupper($empresa['endereco']);?> </td>					
				</tr>
				<tr>
					<td>CELULAR: </td>
					<td><?php echo $empresa['celular']; ?></td>					
					<td>BAIRRO: </td>					
					<td colspan="5"><?php echo mb_strtoupper($empresa['bairro']);?></td>					
				</tr>
				<tr>
					<td>CONTATO:</td>
					<td><?php echo mb_strtoupper($info['contato']); ?></td>					
					<td>CIDADE:</td>					
					<td><?php echo mb_strtoupper($empresa['cidade']);?></td>					
					<td style="text-align: right;">CEP:</td>					
					<td colspan="3"><?php echo $empresa['cep'];?></td>					

				</tr>
				<tr>
					<td>E-MAIL: </td>
					<td style="text-transform: lowercase !important;"><?php echo $empresa['email'];?></td>					
					<td>UF: </td>					
					<td><?php echo mb_strtoupper($empresa['estado']);?></td>					
					<td style="text-align: right;"></td>					
					<td></td>					
					<td></td>
					<td></td>		
				</tr>
			</tbody></table>	
			<img src="./bootstrap/img/imgOrcamento.jpg" width="100%" style="margin-top: 15px;">
		</div>
		<div class="cabecalho" style="PAGE-BREAK-BEFORE: always">
			<table border="0">
				<tr>
					<td style="margin: 5px auto;">
						<img STYLE="width: 150px;" src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" >
					</td>
					<td style="text-align: right; font-size: 8px;margin-top: -10px;">
						<p > WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
										07400-230 - Arujá - SP<br>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br>
						</p>
					</td>			
				</tr>
			</table>
		</div>
		<br>
		<table class="table borda" cellspacing="0" style="margin-top: 75px;">
			<thead bgcolor="#ccc" style="background-color: #ccc;font-size: 10px;">
				<tr  >
					<th style="text-align: center;">#</th>					
					<th style="text-align: center;">MODELO</th>
					<th style="text-align: center;">DESCRIÇÃO</th>
					<th style="text-align: center;">QTD</th>
					<th style="text-align: right;">PREÇO UNITÁRIO</th>
					<th style="text-align: right;">TOTAL P/ ITEM</th>
				</tr>
			</thead>
			<tbody>
				<?php $i=1; $total = 0; foreach( $produtos as $produto ){ ?>
				<tr>
					<td cellspacing="0" cellpadding="0" style="text-align: center;"><?php echo $i; ?></td>					
					<td cellspacing="0" style="text-align: center;"><?php echo strtoupper($produto['modelo']); ?></td>
					<td cellspacing="0"><?php echo $produto['descricao']; ?></td>
					<td cellspacing="0" style="text-align: center;"><?php echo $produto['qtd']; ?></td>
					<td style="text-align: right;">R$ <span class="valor_unitario"><?php echo number_format($produto['valor_produto'], 2, ',', '.'); ?></span></td>
					<td cellspacing="0" style="text-align: right;">R$ <?php echo number_format($produto['qtd'] * $produto['valor_produto'],2,',','.'); ?></td>
				<?php $i++; $total = ( $produto['qtd'] * $produto['valor_produto'] ) + $total; } ?>
				
				</tr>
				<tr>
					
					<td colspan="5" style="text-align: right;"><b>Total</b></td>
					
					<td style="text-align: right;"><b class="valor_unitario">R$ <?php echo number_format($total, 2, ',', '.');?> </b></td>
				</tr>
				<!--<tr>
					
					
					<td colspan="4" style="text-align: right;"><b>Total (Sub-total + <?php echo $empresa['valor_tributo']."%";?> de ICMS + 5% de IPI)</b></td>
					
					<td style="text-align: right;"><b class="valor_unitario">R$ <?php echo number_format($total+round($total/100 * $empresa['valor_tributo'],2)+round($total/100 * 5,2),2, ',', '.');?> </b></td>
				</tr>-->				
			</tbody>
		</table>
		<div class="info">
			<p>* Preços com ICMS <!--de <?php echo $empresa['valor_tributo']; ?>%--> e IPI <!--de 5%--> inclusos</p>			
			<p>
				<br><br><b>CONDIÇÃO DE PAGAMENTO (sujeito à análise de crédito):</b><br/>
					<?php echo $formaPagto; ?><br/>

			</p>
			<p>
				<b>OUTRAS OPÇÕES DE CRÉDITO:</b><br/>
				Cartão BNDES e Proger;<br>
				Finame, Leasing (sob consulta).<br>

			</p>
			<p><b>PRAZO DE ENTREGA:</b><br/>
				<?php echo $entrega; ?><br>
			</p>
			
			<p>
				<b>GARANTIA:</b><br>
				BOMBAS: <?=$empresa['garantia']?> meses - À contar da data de emissão da nota fiscal.<br>
				DISPENSER ELETRÔNICO PARA COMBOIO: 12 meses - À contar da data de emissão da nota fiscal.
			</p>
			<p>
				<b>FRETE:</b><br>
				<?php echo $frete; ?><br>				 
			</p>
			<p>
				<b>START-UP:</b><br>
				O start-up somente poderá ser realizado por um técnico autorizado pela Wertco.<br>
				Esse serviço é gratuito e deverá ser solicitado somente após a instalação completa do posto e da bomba,
com combustível no tanque.<br>
A realização deste procedimento por pessoas não autorizadas, acarretará na perda da garantia dos equipamentos.<br>
			</p>
			<p>
				<b>ASSISTÊNCIA TÉCNICA:</b><br>
				A Wertco disponibiliza uma ampla rede de técnicos autorizados em todo o território nacional.<br>
				Central de Atendimento:  (11) 3900-2565 <br>
				Assistência Técnica - WhatsApp: (11) 95837-8006 <br>
				Suporte Técnico - WhatsApp: (11) 95340-0031
			</p>
			<p>
				<b>OBSERVAÇÃO:</b><br>
				<?php echo $empresa['observacao']; ?>
			</p>
		</div>			 
			<p style="position: fixed; bottom: 150;">
				<br/><br/>Estamos a sua disposição para esclarecer quaisquer dúvidas.<br><br>
				Atenciosamente,<br>
				<?php echo ucwords($info['nome']); ?><br>				
				Departamento de Vendas<br>
				Email: <?php echo $info['email']; ?><br>
				Telefone: <?php echo $info['telefone']; ?><br>							
				<?php if($indicador['indicador']!='') { ?>
					Indicador: <?php echo ucwords($indicador['indicador']); ?><br>
				<?php } ?>
			</p>		
	</div>

</body>
</html>