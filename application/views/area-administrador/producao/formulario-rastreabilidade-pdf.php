<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Formulário de Rastreabilidade</title>

<style type="text/css">
*{margin:1px;padding:1px;}
body {
 	background-color: #fff;
 	margin: 0;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 10px;
 	color: #333;
 	width: 100%;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
    position: relative;
    background: #ccc;
    height: 55px;
    border: 1px solid #000;
}
table {
  width: 100%;
  border-spacing: 0;
  
}
table.cliente{	
	position: relative;
	padding: 0px;
	margin: -3px 0px 0px 0px;
	width: 100%;		
}
table tr{	
	
}
table td{	
	border-left: 1px solid #000;	
	border-top: 1px solid #000;	
	border-bottom: 1px solid #000;	
	font-size: 11px;	
	padding: 1px;
}
.cliente td, .celula td{
	padding-bottom: 15px;	
}
table.titulo{
	border: 0px;
	width: 100%;
	position: relative;
	padding: 0px;
	margin: 0px;
}
table.celula{
	width: 100%;
	position: relative;
	padding: 0px;
	margin: 0px;	
}

.quadro{
	width: 5px;
	height: 5px;
	border: 1px solid #000;
	float: left;
}

	

</style>

</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;" />
			<p style="text-align: center;font-size: 14px;margin-top: -30px;font-weight: bold;">FORMULÁRIO DE RASTREABILIDADE</p>
			<p style="text-align: center;font-size: 12px;margin-top: -12px;font-weight: bold;color: red;">OBSERVAÇÃO: OCORRENDO ALGUM PROBLEMA NA MONTAGEM DOS EQUIPAMENTOS OU NO TESTE FINAL, INFORMAR LÍDER DA PRODUÇÃO URGENTEMENTE.</p>
		</div>
		<table class="cliente">
			<tr>
				<td>CLIENTE:<br><b><?php echo $dados['cliente']; ?></b> </td>
				<td>CÓDIGO: <br><b><?php echo $dados['codigo']; ?></b> </td>
				<td>OPV: <br><b><?php echo $dados['pedido']; ?></b> </td>
				<td>MODELO:<br><b><?php echo $dados['modelo']; ?></b></td>
				<td>PADRÃO PINTURA:<br><b><?php echo mb_strtoupper($dados['pintura']); ?></b></td>
				<td style="border-right: 1px solid #000;">COMBUSTÍVEL:<br><b><?php echo mb_strtoupper($dados['combustivel']); ?></b></td>
			</tr>			
		</table>
		<table class="titulo">
			<tr>
				<td style="width: 15%;background-color: #ccc;padding-top: 5px;padding-bottom: 5px;">NÚMERO DE SÉRIE:<b><?php echo $dados['nr_serie']; ?></b></td>
				<td style="width: 70%;background-color: #ccc;text-align: center;padding-top: 5px;padding-bottom: 5px;"><b>CELULA 01: VALIDAÇÃO DA MONTAGEM</b></td>
				<td style="width: 15%; border-right: 1px solid #000; border-left: 0px; background: #ccc;">&nbsp;</td>
			</tr>
		</table>
		<table class="celula">
			<tr>
				<td style="width: 10%;">Data:</td>
				<td style="width: 45%;border-right: 1px solid #000;">Nome Legível do Colaborador:</td>
				<td style="width: 45%;border-right: 1px solid #000;">Nome Legível do Colaborador:</td>
			</tr>
		</table>
		<table class="titulo">
			<tr>
				<td style="width: 15%;background-color: #ccc;padding-top: 5px;padding-bottom: 5px;">&nbsp;</td>
				<td style="width: 70%;background-color: #ccc;text-align: center;padding-top: 5px;padding-bottom: 5px;border-left: 0px;"><b>CELULA 02: VALIDAÇÃO DA MONTAGEM</b></td>
				<td style="width: 15%; border-right: 1px solid #000; border-left: 0px; background: #ccc;">&nbsp;</td>
			</tr>
		</table>
		<table class="celula">
			<tr>
				<td style="width: 10%;">Data:</td>
				<td style="width: 45%;border-right: 1px solid #000;">Nome Legível do Colaborador:</td>
				<td style="width: 45%;border-right: 1px solid #000;">Nome Legível do Colaborador:</td>
			</tr>
		</table>
		<table class="titulo">
			<tr>
				<td style="width: 15%;background-color: #ccc;padding-top: 5px;padding-bottom: 5px;">&nbsp;</td>
				<td style="width: 70%;background-color: #ccc;text-align: center;padding-top: 5px;padding-bottom: 5px;border-left: 0px;"><b>CELULA 03: VALIDAÇÃO DA MONTAGEM</b></td>
				<td style="width: 15%; border-right: 1px solid #000; border-left: 0px; background: #ccc;">&nbsp;</td>
			</tr>
		</table>
		<table class="celula">
			<tr>
				<td style="width: 10%;">Data:</td>
				<td style="width: 45%;border-right: 1px solid #000;">Nome Legível do Colaborador:</td>
				<td style="width: 45%;border-right: 1px solid #000;">Nome Legível do Colaborador:</td>
			</tr>
		</table>
		<table class="titulo">
			<tr>
				<td style="width: 15%;background-color: #ccc;padding-top: 5px;padding-bottom: 5px;">&nbsp;</td>
				<td style="width: 70%;background-color: #ccc;text-align: center;padding-top: 5px;padding-bottom: 5px;border-left: 0px;"><b>CELULA 04: TESTE FINAL</b></td>
				<td style="width: 15%; border-right: 1px solid #000; border-left: 0px; background: #ccc;">&nbsp;</td>
			</tr>
		</table>
		<table class="teste_final">
			<tr>
				<TD style="text-align:center;"><b>FASES DO MOTOR 1</b></TD>
				<TD style="text-align:center;"><b>VAZÃO REQUERIDA</b></TD>
				<TD style="text-align:center;"><b>Nº SERIE ROTATIVA T75</b></TD>
				<TD style="text-align:center;"><b>Nº SERIE ROTATIVA T140</b></TD>
				<TD style="text-align:center;"><b>FASES DO MOTOR</b></TD>			
			</tr>
			<tr>
				<td>Potência: <img src="./bootstrap/img/quadrado.png" /> 1CV | <img src="./bootstrap/img/quadrado.png" /> 2CV</td>
				<td><img src="./bootstrap/img/quadrado.png" />40L/Min - 05L/Min </td>
				<td>1</td>
				<td>1</td>
				<td>Monofásico 1Ø <img src="./bootstrap/img/quadrado.png" /> | Trifásico 3Ø <img src="./bootstrap/img/quadrado.png" /></td>
			</tr>
			<tr>
				<td>Voltagem: <img src="./bootstrap/img/quadrado.png" /> 110CV | <img src="./bootstrap/img/quadrado.png" /> 220CV <br/>
					<img src="./bootstrap/img/quadrado.png" /> 380CV | <img src="./bootstrap/img/quadrado.png" /> 440CV</td>
				<td><img src="./bootstrap/img/quadrado.png" />75L/Min - 05L/Min </td>
				<td>2</td>
				<td>2</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Frequência: <img src="./bootstrap/img/quadrado.png" /> 50Hz | <img src="./bootstrap/img/quadrado.png" /> 60Hz </td>
				<td><img src="./bootstrap/img/quadrado.png" />130L/Min - 15L/Min </td>
				<td>3</td>
				<td>3</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>4</td>
				<td>4</td>
				<td>&nbsp;</td>
			</tr>
			
		</table>
	</div> 
</body>
</html>
