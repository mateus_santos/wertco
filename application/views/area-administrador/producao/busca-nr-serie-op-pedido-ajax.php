<table class="" id="html_table" width="100%">
<thead>
	<tr>
		<th style="width: 15%;text-align: center;">Nr. Série</th>
		<th style="width: 15%;text-align: center;">Modelo</th>
		<th style="width: 15%;text-align: center;">Pedido</th>						
		<th style="width: 10%;text-align: center;">Ordem Produção</th>						
		<th style="width: 40%;">Empresa</th>
		<th style="width: 5%;">Ações</th>
	</tr>
</thead>
<tbody>					
	<?php foreach($dados as $dado){	?>
		<tr>
			<td style="text-align: center;">
				<?php echo $dado['id']; ?>					
			</td>		
			<td style="text-align: center;">
				<?php echo $dado['modelo']; ?>					
			</td>				
			<td style="text-align: center;">
				<?php echo $dado['pedido_id']; ?> <i class="fa fa-plus-circle pedido_id" style="color: #ffcc00;" onclick="abrirModalPedido(<?php echo $dado['pedido_id']; ?>);"></i>
			</td>
			<td style="text-align: center;">
				<?php echo $dado['op_id']; ?> <i class="fa fa-plus-circle op_id" style="color: #ffcc00;" onclick="abrirModalOp(<?php echo $dado['op_id']; ?>,<?php echo $dado['id']; ?>);"></i>
			</td>	
			<td style="text-transform: uppercase; "> 
				<?php echo $dado['cliente']; ?>							
			</td>
			<td>
				<a href="<?php echo base_url('AreaAdministrador/editarNrSerie/'.$dado['pedido_id']); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" >
					<i class="la la-edit editar"></i>
				</a>
			</td>	
		</tr>
		<?php } ?> 
	</tbody>
</table>