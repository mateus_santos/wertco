<style type="text/css">
	.etapa{
		text-align: center;
		width: 90%;
		margin: 0 auto;
		min-height: 300px;
	}
	#chart-area, #chart-area-orcamento, #chart-area-pedidos{
		min-height: 300px;	
	}
</style>
<div class="m-content">
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-line-chart"></i>
					</span>
					<h3 class="m-portlet__head-text">	
						Desempenho comercial
					</h3>
				</div>
			</div> 
		</div> 
		<div class="m-portlet__body">
			<div class="row ">
				<div class="col-xl-4">
					<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit " style="min-height: 385px; background: #abcdb038;">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Funil de vendas - (Orçamentos x Pedidos)
									</h3>
								</div>
							</div>
							<div class="m-portlet__head-tools">
								<ul class="m-portlet__nav">								
									<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
										<select class="form-control" id="mes">
											<option value="01" <?=(date('m') == '01') ? 'selected=selected' : ''?> >Janeiro</option>
											<option value="02" <?=(date('m') == '02') ? 'selected=selected' : ''?> >Fevereiro</option>
											<option value="03"  <?=(date('m') == '03') ? 'selected=selected' : ''?>>Março</option>
											<option value="04"  <?=(date('m') == '04') ? 'selected=selected' : ''?>>Abril</option>
											<option value="05"  <?=(date('m') == '05') ? 'selected=selected' : ''?>>Maio</option>
											<option value="06"  <?=(date('m') == '06') ? 'selected=selected' : ''?>>Junho</option>
											<option value="07"  <?=(date('m') == '07') ? 'selected=selected' : ''?>>Julho</option>
											<option value="08"  <?=(date('m') == '08') ? 'selected=selected' : ''?>>Agosto</option>
											<option value="09"  <?=(date('m') == '09') ? 'selected=selected' : ''?>>Setembro</option>
											<option value="10"  <?=(date('m') == '10') ? 'selected=selected' : ''?>>Outubro</option>
											<option value="11"  <?=(date('m') == '11') ? 'selected=selected' : ''?>>Novembro</option>
											<option value="12"  <?=(date('m') == '12') ? 'selected=selected' : ''?>>Dezembro</option>
										</select>
									</li>
									<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
										<select class="form-control" id="ano">
											<option value="2022" selected>2022</option>
											<option value="2021" >2021</option>
											<option value="2020" >2020</option>
											<option value="2019" >2019</option>										
										</select>
									</li>
									<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
										<i class="la la-search" id="gera_funil"></i>
									</li>
								</ul>
							</div>											
						</div>
						<div class="m-portlet__body">
							<!--begin::Widget5-->
							<div id="canvas-holder" class="etapa">
								<canvas id="chart-area" height="450"></canvas>
							</div>
							<!--end::Widget 5-->
						</div>
					</div>	
					<!--begin:: Widgets/Inbound Bandwidth-->			
					<div class="m--space-30"></div>
					<!--begin:: Widgets/Inbound Bandwidth-->			
					
					<!--end:: Widgets/Inbound Bandwidth-->
				</div>
				<div class="col-xl-4">
					<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit " style="min-height: 385px; background: #abcdb038;">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Funil de vendas - (Orçamentos)
									</h3>
								</div>
							</div>
							<div class="m-portlet__head-tools">
								<ul class="m-portlet__nav">								
									<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
										<select class="form-control" id="mes_orcamento">
											<option value="01" <?=(date('m') == '01') ? 'selected=selected' : ''?> >Janeiro</option>
											<option value="02" <?=(date('m') == '02') ? 'selected=selected' : ''?> >Fevereiro</option>
											<option value="03"  <?=(date('m') == '03') ? 'selected=selected' : ''?>>Março</option>
											<option value="04"  <?=(date('m') == '04') ? 'selected=selected' : ''?>>Abril</option>
											<option value="05"  <?=(date('m') == '05') ? 'selected=selected' : ''?>>Maio</option>
											<option value="06"  <?=(date('m') == '06') ? 'selected=selected' : ''?>>Junho</option>
											<option value="07"  <?=(date('m') == '07') ? 'selected=selected' : ''?>>Julho</option>
											<option value="08"  <?=(date('m') == '08') ? 'selected=selected' : ''?>>Agosto</option>
											<option value="09"  <?=(date('m') == '09') ? 'selected=selected' : ''?>>Setembro</option>
											<option value="10"  <?=(date('m') == '10') ? 'selected=selected' : ''?>>Outubro</option>
											<option value="11"  <?=(date('m') == '11') ? 'selected=selected' : ''?>>Novembro</option>
											<option value="12"  <?=(date('m') == '12') ? 'selected=selected' : ''?>>Dezembro</option>
										</select>
									</li>
									<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
										<select class="form-control" id="ano_orcamento">
											<option value="2022" selected>2022</option>
											<option value="2021" >2021</option>
											<option value="2020" >2020</option>
											<option value="2019" >2019</option>										
										</select>
									</li>
									<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
										<i class="la la-search" id="gera_funil_orcamento"></i>
									</li>
								</ul>
							</div>											
						</div>
						<div class="m-portlet__body">
							<!--begin::Widget5-->
							<div id="canvas-holder-orcamento" class="etapa">
								<canvas id="chart-area-orcamento" height="450"></canvas>
							</div>
							<!--end::Widget 5-->
						</div>
					</div>	
					<!--begin:: Widgets/Inbound Bandwidth-->			
					<div class="m--space-30"></div>
					<!--begin:: Widgets/Inbound Bandwidth-->			
					
					<!--end:: Widgets/Inbound Bandwidth-->
				</div>
				<div class="col-xl-4">
					<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit " style="min-height: 385px; background: #abcdb038;">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Funil de vendas - (Pedidos)
									</h3>
								</div>
							</div>
							<div class="m-portlet__head-tools">
								<ul class="m-portlet__nav">								
									<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
										<select class="form-control" id="mes_pedidos">
											<option value="01" 	<?=(date('m') == '01') ? 'selected=selected' : ''?>>Janeiro</option>
											<option value="02" 	<?=(date('m') == '02') ? 'selected=selected' : ''?>>Fevereiro</option>
											<option value="03"  <?=(date('m') == '03') ? 'selected=selected' : ''?>>Março</option>
											<option value="04"  <?=(date('m') == '04') ? 'selected=selected' : ''?>>Abril</option>
											<option value="05"  <?=(date('m') == '05') ? 'selected=selected' : ''?>>Maio</option>
											<option value="06"  <?=(date('m') == '06') ? 'selected=selected' : ''?>>Junho</option>
											<option value="07"  <?=(date('m') == '07') ? 'selected=selected' : ''?>>Julho</option>
											<option value="08"  <?=(date('m') == '08') ? 'selected=selected' : ''?>>Agosto</option>
											<option value="09"  <?=(date('m') == '09') ? 'selected=selected' : ''?>>Setembro</option>
											<option value="10"  <?=(date('m') == '10') ? 'selected=selected' : ''?>>Outubro</option>
											<option value="11"  <?=(date('m') == '11') ? 'selected=selected' : ''?>>Novembro</option>
											<option value="12"  <?=(date('m') == '12') ? 'selected=selected' : ''?>>Dezembro</option>
										</select>
									</li>
									<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
										<select class="form-control" id="ano_pedidos">
											<option value="2022" selected>2022</option>
											<option value="2021" >2021</option>
											<option value="2020" >2020</option>
											<option value="2019" >2019</option>										
										</select>
									</li>
									<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
										<i class="la la-search" id="gera_funil_pedidos"></i>
									</li>
								</ul>
							</div>											
						</div>
						<div class="m-portlet__body">
							<!--begin::Widget5-->
							<div id="canvas-holder-pedidos" class="etapa">
								<canvas id="chart-area-pedidos" height="450"></canvas>
							</div>
							<!--end::Widget 5-->
						</div>
					</div>	
					<!--begin:: Widgets/Inbound Bandwidth-->			
					<div class="m--space-30"></div>
					<!--begin:: Widgets/Inbound Bandwidth-->			
					
					<!--end:: Widgets/Inbound Bandwidth-->
				</div>
			</div>
		</div>
	</div>
</div>