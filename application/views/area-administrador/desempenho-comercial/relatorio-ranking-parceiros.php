<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-diagram" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Ranking Parceiros
					</h3>					
				</div>
			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >		
			<form class="" action="<?php echo base_url('areaAdministrador/geraRelatorioRankingParceiros');?>" method="post" target="_blank"> 		
			<div class="form-group m-form__group row">				
				<div class="col-lg-3">
					<label>Tipo de Parceiro:</label>
					<div class="m-radio-inline">
						<label class="m-radio">
							<input type="radio" value="i" class="tp" name="tp_parceiro" checked="checked">
							Indicadores
							<span></span>
						</label>
						<label class="m-radio">
							<input type="radio" value="r" class="tp" name="tp_parceiro">
							Representantes
							<span></span>
						</label>						
					</div>											
				</div>
				<div class="col-lg-3">
					<label>Período:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" 	 name="dt_ini" 	id="dt_ini"	class="form-control m-input datepicker" placeholder="data inicial" style="width: 49%;float: left;" required /> 
						<input type="text" 	 name="dt_fim" 	id="dt_fim"	class="form-control m-input datepicker" placeholder="data final" style="width: 49%;float: right;" required/>
					</div>							
				</div>			
				<div class="col-lg-3">
						<label class="">Modelo de bomba:</label>
						<div class="m-input-icon m-input-icon--right">
							<select  class="form-control"  maxlength="250" name="produto_id" placeholder="" id="produto" >
	                        	<option value="">SELECIONE UM MODELO</option>
	                        <?php 	foreach($produtos as $produto ){
	                        		if( $produto->tipo_produto_id != 4 ){
	                        	?>       
	                        	<option value="<?php echo $produto->id; ?>"><?php echo mb_strtoupper($produto->codigo).' - '.mb_strtoupper($produto->modelo); ?></option>
	                        <?php } } ?>
	                       	</select>
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
						</div>							
				</div>
				<div class="col-lg-3">
					<label class="">Parceiro: </label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" class="form-control" name="parceiro" id="parceiro" placeholder="pesquise um parceiro..." />
						<input type="hidden" name="parceiro_id" id="parceiro_id" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>
				</div>
			</div>	
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<input type="hidden" id="modelo" name="modelo" />
							<button type="submit" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>							
					</div>
				</div>
			</div>
			</form>	
		</div>						
	</div>	
	<!--end::Portlet-->	
</div>	
