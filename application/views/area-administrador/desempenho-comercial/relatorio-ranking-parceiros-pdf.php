<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

	<style type="text/css">

		body {
			background-color: #fff;
			margin: 0 auto;
			position: relative;
			font-family: Lucida Grande, Verdana, Sans-serif;
			font-size: 12px;
			color: #333;
			width: 100%;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 14px; 
			font-weight: bold;
			margin: 24px 0 2px 0; 	
		}
		.cabecalho{
			margin: 0 auto;
			position: relative;
		}
		table{	
			padding: 0;
			width: 100%;
			border: 1px solid #ffcc00;

		}
		table tr{	

		}
		table td{	
			border-bottom: 1px solid #f0f0f0;	
			font-size: 9px;	
			padding: 5px;
		}


	</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
				CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
				Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
				07400-230 - Arujá - SP<br>
				Tel: +55 11 3900-2565 | www.wertco.com.br<br>
			</p>
		</div>
		<div class="corpo">	
			<h3 style="text-align: center;margin-top: 15px;	">Ranking dos Parceiros</h3>	
			<p style="text-align: center;"><?php echo $subtitulo;?></p>	
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;border: 1px solid #ffcc00;">
				<tr>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Empresa</b></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;"><b>Estado</b></td>									
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;text-align: right;"><b>Orçamentos Total</b></td>			
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;text-align: right;"><b>Orçamentos Fechados</b></td>			
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;text-align: right;"><b>Bombas Total</b></td>				
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;text-align: right;"><b>Bombas Vendidas</b></td>				
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;text-align: right;"><b>Valor Total Orçado</b></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ffcc00;padding: 0;text-align: right;"><b>Valor Total Fechado</b></td>
				</tr>				
			<?php foreach($dados as $dado){ 	?>
					<tr>
						<td style="border-top: 1px solid #ccc;"> <?php echo $dado['empresa'];  ?></td>
						<td style="border-top: 1px solid #ccc;"> <?php echo $dado['estado']; ?></td>												
						<td style="border-top: 1px solid #ccc;text-align: right;"> <?php echo $dado['total']; ?></td>	
						<td style="border-top: 1px solid #ccc;text-align: right;"> <?php echo $dado['fechados']; ?></td>
						<td style="border-top: 1px solid #ccc;text-align: right;"> <?php echo $dado['total_bombas']; ?></td>
						<td style="border-top: 1px solid #ccc;text-align: right;"> <?php echo $dado['bombas_fechadas']; ?></td>
						<td style="border-top: 1px solid #ccc;text-align: right;"> <?php echo $dado['valor_total']; ?></td>						
						<td style="border-top: 1px solid #ccc;text-align: right;"> <?php echo $dado['valor_fechado']; ?></td>
					</tr>					
			<?php 	}	?>		 		
			</table>			
		</div>	
	</div>
</body>
</html>