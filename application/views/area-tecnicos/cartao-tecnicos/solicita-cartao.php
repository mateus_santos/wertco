<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">						
						<h3 class="m-portlet__head-text">	
							Solicitar Cartão Técnico
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form id='form' class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaTecnicos/solicitaCartaoTecnico');?>" method="post">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">						
						<?php if($solicitacao->total > 0 ){ ?>
							<h3>
								Cartão solicitado, Aguarde retorno da WERTCO!<br/>
								<a href="<?php echo base_url('/areaTecnicos/index');?>">voltar</a>
							</h3>	

						<?php }else{	?>
						<div class="col-lg-12">
							<label>Pesquise pelo CNPJ do Posto onde irá ser realizado o Startup:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text"  name="empresa" id="empresa" class="form-control m-input" placeholder=""  style=""  required maxlength="550">
								<input type="hidden" name="empresa_id" id="empresa_id" class="form-control m-input" placeholder="" required style="" required>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-building-o"></i></span></span>
							</div>							
						</div>
					</div>				
		            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
						<div class="m-form__actions m-form__actions--solid">
							<div class="row">
								<div class="col-lg-6">
									<button type="submit" name="salvar" id="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
									<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
								</div>							
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
</div>	
<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cartão solicitado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaTecnicos/solicitaCartaoTecnico';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>