<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 0 auto;
 position: relative;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 12px;
 color: #333;
 width: 650px;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px;
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{
	border: 1px;
}

table td{
	padding: 8px;
	border-bottom: 1px solid #ffcc00;
}

table{
	
}

</style>
</head>
<body>
	<div class="conteudo" style="border: 1px solid #ffcc00;padding: 30px;"">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 280px;margin: 25px 31%;" />
			<h3>Orçamento # <?php echo $empresa['orcamento_id']; ?></h3>		
			<p >Emissão:<?php echo date('d/m/Y H:i:s', strtotime($empresa['emissao']));?></p>	
			<p>Empresa: <?php echo $empresa['razao_social'] . ' | '.  $empresa['cnpj'] .' | '. $empresa['fantasia']; ?> </p>
			<p>Telefone: <?php echo $empresa['telefone']; ?></p>
			<p>E-mail: <?php echo $empresa['email']; ?></p>
			<p>Enderço: <?php echo $empresa['endereco']; ?></p>
			<p>Cidade: <?php echo $empresa['cidade']; ?></p>
			<p>Estado: <?php echo $empresa['estado']; ?></p>

		</div>
		<hr>
		<h3>Produtos</h3>
		<table class="table" cellspacing="0">
			<thead>
				<tr><th>#</th>
				<th>Código</th>
				<th>Modelo</th>
				<th>Descrição</th>
				<th>Qtd</th>				
			</tr></thead>
			<tbody>
				<?php $i=1; $total = 0; foreach( $produtos as $produto ){?>
				<tr>
					<td cellspacing="0" cellpadding="0" ><?php echo $i; ?></td>
					<td cellspacing="0"><?php echo $produto['codigo']; ?></td>
					<td cellspacing="0"><?php echo $produto['modelo']; ?></td>
					<td cellspacing="0"><?php echo $produto['descricao']; ?></td>
					<td cellspacing="0"><?php echo $produto['qtd']; ?></td>					
				<?php $i++;} ?>
			</tr>
			
			</tbody>
		</table>
	</div>
</body>
</html>