<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Indicações de negócios
					</h3> 
				</div>
				
			</div>
			
			<div class="m-portlet__head-caption ">													
				<!--<a href="<?php echo ''; //base_url('AreaRepresentantes/cadastraOrcamentos')?>" class="cadastrar_orcamento" style="color: #ffcc00; font-weight: bold;">
					<i class="la la-plus-circle" style="font-size: 38px;"></i>
				</a>-->
				<button class="cadastrar_orcamento" style="color: #ffcc00;font-weight: bold;border: none;background: #fff;">
					<i class="la la-plus-circle" style="font-size: 38px;"></i>
				</button>

			</div>
			<div class="">
					<div style="float: right;margin-top: 25px;">
					Aguardando aprovação&nbsp;&nbsp;<span class="m-badge" style="background: #ff9800;"></span>
					&nbsp;Indicação não aprovada&nbsp;&nbsp;<span class="m-badge" style="background: #999;"></span>
					&nbsp;Aberto&nbsp;&nbsp;<span class="m-badge m-badge--secondary"></span>				    	
					&nbsp;Orçamento Entregue&nbsp;&nbsp;<span class="m-badge m-badge--info"></span>
					&nbsp;Em negociação&nbsp;&nbsp;<span class="m-badge m-badge--primary"></span>
					&nbsp;Fechado&nbsp;&nbsp;<span class="m-badge m-badge--success"></span>				    		
					&nbsp;Cancelado&nbsp;&nbsp;<span class="m-badge m-badge--warning"></span>				    	
				    &nbsp;Perdido&nbsp;&nbsp;<span class="m-badge m-badge--danger"></span>			    
					</div> 
				</div>					
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;">Número</th>
						<th style="text-align: center;">Cliente</th>
						<th style="text-align: center;">Emissão</th>
						<th style="text-align: center;">Validade</th>						
						<th style="text-align: center;">Status</th>
						<th style="text-align: center;">Andamento</th>
						<th style="text-align: center;">Responsável</th>
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				<tbody>					
					
					<?php foreach($dados as $dado){
						
						if( $dado->status == "aberto" ){
							$status = "btn m-btn--pill m-btn--air btn-secondary";
							$background = '';
						}elseif( $dado->status == "fechado" ){
							$status = "btn m-btn--pill m-btn--air btn-success";
							$background = '';
						}elseif( $dado->status == "perdido" ){
							$status = "btn m-btn--pill m-btn--air btn-danger";
							$background = '';
						}elseif( $dado->status == "cancelado" ){
							$status = "btn m-btn--pill m-btn--air btn-warning";
							$background = '';
						}elseif( $dado->status == "orçamento entregue" ){
							$status = "btn m-btn--pill m-btn--air btn-info";
							$background = '';
						}elseif( $dado->status == "em negociação"){
							$status = "btn m-btn--pill m-btn--air btn-primary";
							$background = '';
						}elseif($dado->status == "Solicitação de Orçamentos - Indicador"){
							$status = "btn m-btn--pill m-btn--air";
							$background = "style='background: #ff9800;'";
						}else{
							$status = "btn m-btn--pill m-btn--air";
							$background = "style='background: #999;'";
						}
						
						?>
						<tr>
							<td style="width: 5%;text-align: center;"><?php echo $dado->orcamento_id; ?></td>
							<td style="width: 30%;text-align: center;"> <?php echo $dado->cnpj." | ".$dado->razao_social; ?></td>
							<td style="width: 10%;text-align: center;"><?php echo date('d/m/Y H:i:s', strtotime($dado->emissao)); ?></td>
							<td style="width: 5%;text-align: center;"><?php echo $dado->validade; ?></td>						
							<td style="width: 20%;text-align: center; text-transform: capitalize;">
								<button class="status <?php echo $status; ?>" <?php echo $background;?> title="Status do orçamento">
									<?php echo ($dado->status == 'Solicitação de Orçamentos - Indicador') ? 'Aguardando Aprovação' : $dado->status; ?>								
								</button>
							</td>														
							<td style="width: 5%;text-align: center;">
								<i title="Andamento do Orçamento" onclick="andamento(<?php echo $dado->orcamento_id; ?>,<?php echo $dado->status_orcamento_id; ?>);" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i>
							</td>
							<td style="width: 25%;text-align: center;">
								<?php echo $dado->responsavel; ?>								
							</td>				
							<td data-field="Actions" class="m-datatable__cell" style="width: 5%;">
								<span style="overflow: visible; width: 110px;">								
									<a href="<?php echo base_url('AreaTecnicos/visualizarOrcamentoInd/'.$dado->orcamento_id); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Visualizar" orcamento_id="<?php echo $dado->orcamento_id; ?>" >
										<i class="la la-eye"></i>
									</a> 									
								</span>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
				
			</div>
		</div>		        
	</div>	
	
	<!-- 

			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- Modal Andamento -->
	<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="m-list-timeline">
						<div class="m-list-timeline__items">                
						</div>
					</div> 					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				</div>
			</div>
		</div>
	</div>			
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaTecnicos/orcamentosTec';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>