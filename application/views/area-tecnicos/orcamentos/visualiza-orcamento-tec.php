<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-list-2"></i>
					</span>
					<h3 class="m-portlet__head-text">
						
						Orçamento #<?php echo $dados[0]->id_orcamento; ?>
					</h3>
				</div>			
			</div>
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<!--<li class="m-portlet__nav-item">
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="geraPdf" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>" data-toggle="m-tooltip" data-placement="top" title="Gerar PDF do Orçamento">
							<i class="la la-print"></i>
						</a>	
					</li>-->
				</ul>
			</div>
		</div>
		<div class="m-portlet__body">
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>Empresa: 
						<small class="text-muted"> <?php echo $dados[0]->cnpj.' | '.$dados[0]->razao_social.' | '.$dados[0]->fantasia;?></small>
						<input type="hidden" id="status_orcamento_id" value="<?php echo $dados[0]->status_orcamento_id; ?>">
					</h5>
				</div>							
				<div class="col-lg-4 col-xs-12">
					<h5>Telefone: 
						<small class="text-muted"> <?php echo $dados[0]->telefone;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>E-mail: 
						<small class="text-muted"> <?php echo $dados[0]->email;?></small>
					</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Enderço: <small class="text-muted"> <?php echo $dados[0]->endereco;?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Cidade: <small class="text-muted"> <?php echo $dados[0]->cidade;?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						Estado: <small class="text-muted"> <?php echo $dados[0]->estado;?></small>
					</h5>
				</div>							
			</div>
		</div>
	</div>
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>Produtos</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<table class="table m-table m-table--head-separator-warning">
			  	<thead>
			    	<tr>
			      		<th>#</th>
			      		<th>Código</th>
			      		<th>Modelo</th>
			      		<th>Descrição</th>
			      		<th>Qtd</th>			      					      		
			    	</tr>
			  	</thead>
			  	<tbody>
			  		<?php $i=1;$total=0; foreach($dados as $dado){?>
			    	<tr>
				      	<th scope="row"><?php echo $i;?></th>
				      	<td><?php echo $dado->codigo; ?></td>
				      	<td><?php echo $dado->modelo; ?></td>
				      	<td><?php echo $dado->descricao; ?></td>
				      	<td><?php echo $dado->qtd; ?></td>				      	
			    	</tr>
			    	<?php $i++; } ?>			    	
			  	</tbody>
			</table>
		</div>
	</div>

</div>	