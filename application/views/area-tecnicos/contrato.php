<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">						
						<h3 class="m-portlet__head-text">	
							Contrato Prestação de Serviço
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->			
			<div class="m-portlet__body">	
				<p>
					<input type="hidden" id="usuario_id" value="<?php echo $usuario_id; ?>" />
					<input type="hidden" id="fl_aceite" value="<?php echo $dados->fl_termos; ?>" />
					<span style="text-align: center;"><b>CONTRATO DE PRESTAÇÃO DE SERVIÇOS DE ASSISTÊNCIA TÉCNICA EM GARANTIA</b></span><br/><br/>
					Pelo presente instrumento particular, as partes: <br/>


					-WERTCO INDÚSTRIA, COMÉRCIO E SERVIÇOS DE MANUTENÇÃO DE BOMBAS DE ABASTECIMENTO DE COMBUSTÍVEIS, DESENVOLVIMENTO DE PRODUTOS INDUSTRIAIS, IMPORTAÇÃO E EXPORTAÇÃO LTDA., com sede na Avenida Getúlio Vargas, nº 280, Galpão nº 415, Parque Industrial de Arujá, Arujá, SP, CEP 07400-050, devidamente inscrita no CNPJ/MF sob nº 27.314.980/0001-53, neste ato representada nos termos de seu contrato social, doravante denominada WERTCO;<br/>

					- <?php echo $dados->razao_social; ?>, com sede na Rua <?php echo $dados->endereco; ?>, inscrita no CNPJ sob nº. <?php echo $dados->cnpj; ?>, neste ato representada por <?php echo $dados->nome; ?>, CPF nº. <?php echo $dados->cpf; ?>, RG nº. ______________________, doravante denominada <?php echo $dados->fantasia; ?>;<br/>

					Resolvem, de comum acordo, firmar o presente CONTRATO DE PRESTAÇÃO DE SERVIÇOS, que se regerá pelas cláusulas e disposições abaixo.<br/><br/>

					<b>1. DO OBJETO</b><br/><br/>

					1.1. O objeto do presente contrato é a prestação, pela CONTRATADA, de serviços de Startup e Assistência Técnica em GARANTIA, das bombas abastecedoras de Combustíveis instaladas nos clientes da WERTCO, previamente estabelecidos.<br/>

					1.2. A WERTCO fornecerá à contratada os nomes dos clientes, respectivos endereços, quantidade de bombas e bicos, que serão objeto de Stratup ou Assistência Técnica em Garantia.<br/><br/>

					<b>2. OBRIGAÇÕES DA CONTRATADA</b><br/><br/>

					2.1. Startup<br/>

					2.1.1. Será executado pela CONTRATADA em todas as bombas indicadas pela WERTCO, e consiste em seguir os procedimentos<br/>

					2.2. Da Assistência Técnica em GARANTIA<br/>

					2.2.1. Quando for solicitada Assistência Técnica em GARANTIA, a CONTRATADA deverá providenciar os reparos ou substituições necessárias para restabelecer as perfeitas condições de funcionamento do equipamento. <br/>

					2.2.2. Nesta oportunidade, a CONTRATADA deverá verificar não somente o equipamento especificado, como também todas as demais bombas abastecedoras de combustíveis deste estabelecimento (desde que produzidas pela WERTCO e dentro do prazo de garantia), fazendo aferições, testes de vazão, examinando as correias, anotando os totalizadores na ordem de serviço.<br/>

					2.3. Ordem de Serviço<br/>

					2.3.1. A CONTRATADA deverá relatar na ordem de serviço a data e hora do atendimento da Assistência Técnica em Garantia, bem como anotar o totalizador e a espécie de serviço efetuado. Referida ordem obrigatoriamente será assinada e carimbada pelo cliente que solicitou a manutenção, ou por quem este expressamente autorizar.<br/>

					2.3.2. A CONTRATADA deverá informar a WERTCO sobre quaisquer anormalidades ou irregularidades encontradas durante as visitas de Assistência Técnica em Garantia.<br/>

					2.4. Horário de Atendimento<br/>

					2.4.1. A CONTRATADA prestará os serviços objeto deste Contrato de Segunda à Sexta-feira, das 08h00min às 17h00min, e aos sábados das 08h00min às 12h00min, realizando os serviços necessários dentro do prazo máximo de 24 (vinte e quatro) horas, a partir do recebimento da solicitação pela WERTCO.<br/>

					2.4.2. Obriga-se a CONTRATADA a fornecer à WERTCO todas as informações relativas ao andamento dos serviços objeto deste contrato.<br/>

					2.5.  Serviços Extras<br/>

					2.5.1. Serviços não compreendidos na Assistência Técnica em Garantia - classificados como excluídos da garantia, serão cobrados pela CONTRATADA de acordo com os valores previamente acordado entre  a WERTCO e a CONTRATADA.<br/>


					2.6. Da conduta geral<br/>

					2.6.1. A CONTRATADA prestará os serviços contratados de acordo com a legislação vigente, abstendo-se de praticar quaisquer atos que possam colocar em risco a qualidade dos serviços e idoneidade da WERTCO, sob pena de rescisão do presente contrato e ressarcimento dos danos decorrentes.<br/>

					2.6.2. A CONTRATADA responsabiliza-se pelo fiel cumprimento, por seus empregados e/ou terceiros sob sua responsabilidade, de todas as cláusulas aqui pactuadas, bem como por quaisquer danos ocasionados aos equipamentos e instalações dos clientes. <br/>

					2.6.3. A CONTRATADA deverá fazer os seguros que lhe sejam exigidos por lei, ou que julgar necessário, devendo apresentar as respectivas apólices anuais, juntamente com comprovante de inscrição estadual e municipal, certidões negativas federais, estaduais e municipais. <br/>

					2.6.4. A CONTRATADA assume integral responsabilidade por eventuais multas decorrentes de imperfeições ou atrasos nos serviços ora contratados, excetuando-se os casos de força maior ou caso fortuito. <br/>

					2.6.5. A CONTRATADA responsabiliza-se por todos os documentos, peças e equipamentos a ela entregues pela WERTCO para a consecução dos serviços pactuados, respondendo pelo seu mau uso, perda, extravio ou inutilização, enquanto permanecerem sob sua guarda, salvo comprovado caso fortuito ou força maior. <br/>

					2.6.6. A CONTRATADA responsabiliza-se por todos os tributos incidentes sobre os serviços ora contratados, inclusive ART´s junto às unidades do CREA das áreas de atuação, sob pena de responsabilizar-se por eventuais autuações pela ausência de referida obrigação legal e contratual. <br/>

					2.6.7. É responsabilidade da CONTRATADA o fiel cumprimento de todas as obrigações trabalhistas e previdenciárias referentes a seus sócios-proprietários, empregados e/ou terceiros sob sua responsabilidade, comprometendo-se a mantê-los devidamente registrados conforme estabelece a legislação em vigor. <br/>

					§ único: A CONTRATADA deverá apresentar mensalmente à WERTCO cópia autenticada das guias de recolhimento de todos os encargos trabalhistas e previdenciários de seus funcionários, relativos ao mês anterior. <br/>

					2.6.8. A WERTCO poderá solicitar formalmente a substituição de qualquer funcionário da CONTRATADA para execução dos serviços, desde que justifique as razões da solicitação, devendo a substituição ser efetuada no prazo máximo de 30(trinta) dias. <br/>

					2.6.9. Os veículos e ferramentas utilizados na prestação dos serviços ora pactuados serão de propriedade da CONTRATADA, a qual também é responsável pela manutenção e respectivas despesas com os deslocamentos necessários. <br/>

					2.6.10. Compromete-se a CONTRATADA a cumprir integralmente as disposições do Manual de Serviço disponível, mediante acesso restrito, em www.wertco.com.br. <br/>

					2.6.11. Responsabiliza-se a contratada a utilizar todos os Equipamentos de Proteção Individual exigidos pela Norma específica, durante a execução dos serviços ora contratados. <br/><br/>

					<b>3. OBRIGAÇÕES DA WERTCO</b> <br/><br/>

					3.1. Fornecer as especificações, instruções e documentos que se façam necessários ao bom desempenho dos serviços contratados, em tempo hábil.<br/>

					3.2. Quando houver abertura de chamados, a WERTCO informará à CONTRATADA o local onde o equipamento (bomba) encontra-se instalado, a identificação deste e o defeito apresentado.<br/><br/>

					<b>4. DO PREÇO E FORMA DE PAGAMENTO</b><br/><br/>

					4.1. Pelos serviços ora contratados, a WERTCO pagará à CONTRATADA os valores constantes na política de serviços constante no site da WERTCO, sujeita a alterações que serão disponibilizadas no mesmo site.<br/>

					4.2. O pagamento será realizado em até 10 (dez) dias úteis após a apresentação dos documentos previstos no item 2.6.7, parágrafo único, mediante apresentação da Nota Fiscal, acompanhada das ordens de serviço. <br/>

					4.3. Nenhum pagamento efetuado pela WERTCO limitará os direitos desta de posteriormente discordar dos valores faturados ou de reclamar pelo desempenho insatisfatório de qualquer serviço executado, e o pagamento não será considerado como uma aceitação dos serviços por parte da WERTCO.<br/>

					4.4. Possíveis serviços adicionais, ou ainda, havendo a necessidade de qualquer das hipóteses relacionadas no item 2.5., os valores deverão ser previamente orçados e autorizados pela WERTCO, devendo ser pagos em separado e contra apresentação das respectivas Notas Fiscais.<br/>

					4.5. Os valores constantes do presente contrato poderão ser reajustados anualmente, mediante acordo entre as partes, o qual será formalizado através de aditivo contratual. <br/>

					4.6. O pagamento dos valores ora ajustados, será efetuado mediante depósito bancário na conta corrente indicada pela CONTRATADA, servindo o comprovante de depósito como recibo de quitação, ou através de boleto bancário.<br/><br/>


					<b>5. DO PRAZO DE VIGÊNCIA</b><br/><br/>

					5.1. O prazo de vigência deste acordo é de 12 (doze) meses, a contar da data de sua assinatura, podendo ser renovado automaticamente por igual período, caso não haja manifestação contrária formalizada com antecedência de 30 (trinta) dias da data de seu término. <br/>

					5.2. O presente instrumento poderá ser rescindido, sem ônus, a qualquer tempo, mediante prévio aviso escrito – via correio, fax ou e-mail - de 30 (trinta) dias à outra parte.  <br/>

					5.3. Na hipótese de infração a qualquer das obrigações ora contratadas, a parte inocente deverá notificar a outra para que corrija a infração no prazo de 15 (quinze) dias. Em não ocorrendo, a parte infratora pagará à inocente indenização equivalente ao valor total das perdas e danos resultantes de seu ato.<br/>

					5.4. Permanecerão em vigor as cláusulas deste contrato que, por sua natureza, produzam efeitos após o seu término ou rescisão.<br/>

					<b>6. FISCALIZAÇÃO</b><br/><br/>

					6.1. A WERTCO poderá, a seu exclusivo critério, no exercício do poder/dever de fiscalizar, designar representantes legais, que terão livre acesso a qualquer local onde estejam sendo executados ou já concluídos serviços de manutenção pela CONTRATADA, para fiscalização e verificação quanto ao exato cumprimento das cláusulas aqui pactuadas.<br/><br/>

					<b>7. DO MEIO AMBIENTE</b><br/><br/>

					7.1. As partes contratantes se responsabilizam pelo cumprimento das leis e regulamentos referentes à proteção do meio ambiente.<br/>

					7.2. A CONTRATADA compromete-se a obter e manter válidas todas as licenças, autorizações e estudos exigidos para o pleno desenvolvimento de suas atividades, afastando assim, qualquer risco ou dano ao meio ambiente.<br/><br/>

					<b>8. DO SIGILO DE INFORMAÇÕES</b><br/><br/>

					8.1. São consideradas confidenciais e sigilosas as informações comerciais, técnicas e estratégicas trocadas pelas partes contratantes.<br/>

					8.2. A CONTRATADA compromete-se a utilizar as informações e/ou documentos obtidos da WERTCO exclusivamente para prestação dos serviços ora contratados. <br/>

					8.3. Fica expressamente vedada às partes a utilização dos termos deste contrato, seja em divulgação ou propaganda de qualquer espécie, salvo quando autorizado expressamente pela outra parte.<br/><br/>

					<b>9. NÃO EXCLUSIVIDADE</b><br/><br/>

					9.1. A prestação dos serviços objeto deste contrato não é exclusiva, podendo a CONTRATADA prestar tais serviços a terceiros ou realizar negócios permitidos pelo seu objeto social, desde que, em qualquer caso, não prejudique ou comprometa a qualidade dos serviços e demais obrigações do presente contrato.<br/><br/>

					<b>10. DO VÍNCULO EMPREGATÍCIO</b><br/>

					10.1. O presente acordo não estabelece qualquer vínculo empregatício entre as partes contratantes, incluindo seus sócios-proprietários, funcionários e/ou terceiros contratados.<br/>

					10.2. Os prepostos e funcionários da CONTRATADA deverão obedecer aos regulamentos e normas de segurança do estabelecimento onde serão prestados os serviços contratados.<br/><br/>

					<b>11. SUBCONTRATAÇÃO</b><br/><br/>

					11.1. Este contrato não poderá ser cedido ou transferido, no todo ou em parte, ressalvada a concordância expressa, escrita, de ambas as partes.<br/><br/>

					<b>12. DISPOSIÇÕES GERAIS</b><br/><br/>

					12.1. Não constituirá novação ou renúncia de direitos a abstenção, por qualquer das partes, do exercício de qualquer direito, poder, recurso ou faculdade assegurados por lei ou por este contrato, nem a eventual tolerância quanto a eventuais infrações ou atraso no cumprimento de quaisquer obrigações ora contratadas.<br/>

					12.2. Fica eleito o Foro da Comarca da cidade de Arujá/SP para dirimir quaisquer dúvidas ou questões oriundas do presente contrato.<br/><br/>


					<b>E, por assim estarem justos e contratados, firmam este instrumento digitalmente, através de aceite de seus termos e condições, conforme disposições contidas no site, considerando-se como data de início de vigência, o respectivo aceite.</b><br/><br/>

					<span style="text-align: center;"><?php setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');date_default_timezone_set('America/Sao_Paulo'); echo strftime('%d de %B de %Y', strtotime('today')); ?></span>

					
				</p>
				<div>
					<?php if (  $dados->fl_termos == 0 ){ ?>
					<a href="#" class="btn btn-success m-btn m-btn--icon aceite" id="aceitar" valor="1">
					<span>
						<i class="la la-check"></i>
						<span>
							Aceitar os termos e condições
						</span>
					</span>
					</a>
					<a href="#" class="btn btn-danger m-btn m-btn--icon aceite" id="recusar" valor="0">
						<span>
							<i class="la la-warning"></i>
							<span>
								Recusar
							</span>
						</span>
					</a>
				<?php } ?>
				</div>	
			</div>
			
			<!--end::Form-->
		</div>
</div>	

	<script type="text/javascript"> 	
		$(document).ready(function(){
			if( $('#fl_aceite').val() == 0 ){

				$('.aceite').bind('click', function(){
					var valor = $(this).attr('valor');
					var usuario_id = $('#usuario_id').val();

					$.ajax({
				        method: "POST",
				        url: base_url+'AreaTecnicos/aceiteContrato',
				        async: true,
				        data: { valor    	:   valor,		        		
				        		usuario_id	: 	usuario_id 	}
			        }).done(function( data ) {
			            var dados = $.parseJSON(data);
			            if( dados.retorno == '1')
			            {
			            	swal({
			                    title: "OK!",
			                    text: "Termos e condições aceitas!",
			                    type: 'success'
		                    }).then(function() {
		                  		window.location = base_url+'AreaTecnicos/';
								
		                    });
			            }else{
			            	swal({
			                    title: "Atenção!",
			                    text: "Você não aceitou os termos e as condições do contrato de prestação de serviço, por isso seu acesso ao sistema será desativado!",
			                    type: 'warning'
		                    }).then(function() {
		                  		window.location = base_url+'usuarios/login';  	    
		                    });  
			            }
			        });

				});
			}
		});
	</script>	
	