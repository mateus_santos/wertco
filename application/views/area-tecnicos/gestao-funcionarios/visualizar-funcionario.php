<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<a href="<?php echo base_url('AreaTecnicos/gestaoFuncionarios/');?>"><i class="la la-arrow-left"></i></a>
					</span>
					<h3 class="m-portlet__head-text" style="padding-right: 10px;">	
						<a href="<?php echo base_url('AreaTecnicos/gestaoFuncionarios/');?>">Voltar</a>						
					</h3>
					<span class="m-portlet__head-icon">
						<i class="la la-user" style="color: #ffcc00;"></i>
					</span>

					<h3 class="m-portlet__head-text">
						Funcionário #<?php  echo $dados['id']; ?>
					</h3>
				
				</div>	

			</div>
			<a href="<?php echo base_url('AreaTecnicos/editarFuncionario/'.$dados['id']);?>" class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" style="margin-top: 15px;">
				<i class="la la-edit"></i>
			</a>
			<a href="#" onclick="excluirFuncionario(<?php echo $dados['id']; ?>)" class="btn btn-outline-danger m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" style="margin-left: 35px;margin-top: 15px;">
				<i class="la la-trash"></i>
			</a>
			
		</div>
		<div class="m-portlet__body">
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>Funcionario: 
						<small class="text-muted"> <?php echo $dados['nome'];?></small>
					</h5>
				</div>					
				<input type="hidden" value="<?=$dados['tipo_cadastro_id']?>" id="tipo_cadastro_id">
				<div class="col-lg-4 col-xs-12">
					<h5>CPF: 
						<small class="text-muted"> <?php echo $dados['cpf'];?></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>E-mail: 
						<small class="text-muted"> <?php echo $dados['email'];?></small>
					</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Telefone: <small class="text-muted"> <?php echo $dados['telefone'];?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Celular: <small class="text-muted"> <?php echo $dados['celular'];?></small>
					</h5>
				</div>
				
			</div>
			
		</div>
	</div>
</div>