<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-users" style="color: #ffcc00;"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Gestão de Funcionários - <span style="color: #ffcc00; font-weight: bold;"><?php echo $razao_social; ?></span> 
					</h3>
				</div>
				
			</div>
			
			<div class="m-portlet__head-caption">													
				<a href="<?php echo base_url('AreaTecnicos/cadastraFuncionario')?>" class="" style="color: #ffcc00; font-weight: bold;" >
					<i class="la la-plus-circle" style="font-size: 38px;"></i>
				</a>
			</div>			
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">Número</th>
						<th style="width: 20%;">Nome</th>
						<th style="width: 10%;">CPF</th>
						<th style="width: 10%;">E-mail</th>						
						<th style="width: 15%;">Telefone</th>
						<th style="width: 10%;">Celular</th>
						<th style="width: 10%;">Tipo de Acesso</th>
						<th style="width: 10%;">Ações</th>
					</tr>
				</thead>
				<tbody>					
					
					<?php foreach($dados as $dado){
							if($dado['id'] != $usuario_id){

					 ?>
						<tr>
							<td style="text-align: center;"><?php echo $dado['id']; ?></td>
							<td> <?php echo $dado['nome']; ?></td>
							<td><?php echo $dado['cpf']; ?></td>
							<td><?php echo $dado['email']; ?></td>						
							<td><?php echo $dado['telefone']; ?></td>
							<td><?php echo $dado['celular']; ?></td>														
							<td><?php echo $dado['tp_acesso']; ?></td>
							
							<td data-field="Actions" class="m-datatable__cell" >
								<span style="overflow: visible;">								
								<?php if($dado['solicitacao'] == 0 && $dado['tipo_cadastro_id'] == 4){  ?>
									<a href="#" onclick="solicitaCartao('<?php echo $dado['id']; ?>','<?php echo $dado['nome']; ?>');" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" title="Solicitar Cartão Técnico">
										<i class="la la-credit-card"></i>
									</a>
								<?php  } 	?>
									<a href="<?php echo base_url('AreaTecnicos/visualizarFuncionario/'.$dado['id']); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Visualizar">
										<i class="la la-eye"></i>
									</a>
									<a href="<?php echo base_url('AreaTecnicos/editarFuncionario/'.$dado['id']); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Editar" >
										<i class="la la-edit"></i>
									</a>
									<a href="#" onclick="excluirFuncionario('<?php echo $dado['id']; ?>');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Excluir Funcionário" >
										<i class="la la-trash"></i>
									</a>
								</span>
							</td>
						</tr>
					<?php 
							} 
						} 
					?>
					</tbody>
				</table>
				<!--end: Datatable -->
				
			</div>
		</div>		        
	</div>	
	
	<!-- 

			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- Modal Andamento -->
	<div class="modal fade" id="solicitarCartao" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" >Solicitação de cartão técnico para o Funcionário <span id="titulo_modal" style="color: #ffcc00;"></span></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="col-lg-12">
						<label>Pesquise pelo CNPJ do Posto onde o técnico irá realizar o Startup:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text"  	name="empresa" 		id="empresa" class="form-control m-input" placeholder="insira o cnpj do posto"	style=""  required maxlength="550">
							<input type="hidden" 	name="empresa_id" 	id="empresa_id" class="form-control m-input" placeholder="" required style="" required>
							<input type="hidden" 	name="usuario_id" 	id="usuario_id" class="form-control m-input" placeholder="" required style="" required>
							<input type="hidden" 	name="nome" 		id="nome" class="form-control m-input" placeholder="" required style="" required>
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-building-o"></i></span></span>
						</div>
					</div>					
				</div>
				<div class="modal-footer">					
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" class="btn btn-success" id="salvar">Enviar</button>					
				</div>
			</div>
		</div>
	</div>			
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso_cadastro') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaTecnicos/gestaoFuncion';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>
		<?php if ($this->session->flashdata('sucesso_editar') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Edição de funcionário realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaTecnicos/gestaoFuncion';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso_editar']); } ?>