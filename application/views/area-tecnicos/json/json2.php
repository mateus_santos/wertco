		<!-- END: Subheader -->
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		<div class="m-content">
			<div class="row">
				<div class="col-lg-12">
					<!--begin::Portlet-->
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
									</span>
									<h3 class="m-portlet__head-text">
										Gerador de JSON
									</h3>
								</div>
							</div>
						</div>

						<div class="m-portlet__body">
							<div class="row">
								<div class="col-lg-3 col-sm-6">
									<label for="titulo">Escolher tipo de Família</label>
									<select class="form-control" id="jsonTipoSelect" name="jsonTipoSelect">
										<option value="1" disabled selected>Selecione uma opção</option>
										<option value="CLH">CLH</option>
										<option value="CHH">CHH</option>
										<option value="CHHS">CHHS</option>
										<option value="CHI">CHI</option>

									</select>
								</div>
								<div class="col-lg-3 col-sm-6">
									<label for="titulo">Escolher Comercial ou Industrial</label>
									<select class="form-control" disabled id="jsoncomind" name="jsoncomind">
										<option value="0" disabled selected>Selecione uma opção</option>
										<option value="S">Comercial</option>
										<option value="N">Industrial</option>
									</select>
								</div>
								<div class="col-lg-3 col-sm-6">
									<label for="titulo">Escolha o Tipo</label>
									<select class="form-control" disabled id="jsontipo" name="jsontipo">
										<option value="0" disabled selected>Selecione uma opção</option>
										<option value="C">Bomba</option>
										<option value="M">Dispenser</option>
										<option value="C/M">Mista Bomba/Dispenser</option>
									</select>
								</div>
								<div class="col-lg-3 col-sm-6">
									<label for="titulo">Escolha a quantidade de Bicos</label>
									<select class="form-control" disabled id="jsonqntbicos" name="jsonqntbicos">
										<option value="0" disabled selected>Selecione uma opção</option>
										<option value="1">1</option>
										<option value="2,F">2 - Frontal</option>
										<option value="2,L">2 - Lateral</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="6">6</option>
										<option value="8">8</option>
										<option value="10">10</option>
									</select>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-lg-3 col-sm-6">
									<label for="titulo">Número de abastecimentos simultaneos</label>
									<select class="form-control" disabled id="jsonabastsimul" name="jsonabastsimul">
										<option value="0" disabled selected>Selecione uma opção</option>
									</select>
								</div>
								<div class="col-lg-3 col-sm-6">
									<label for="titulo">Escolha o conjunto Hidráulico</label>
									<select class="form-control" disabled id="jsonhidraulico" name="jsonhidraulico">
										<option value="0" disabled selected>Selecione uma opção</option>
									</select>
								</div>
								<div class="col-lg-3 col-sm-6">
									<label for="titulo">Escolha a Vazão</label>
									<select class="form-control" disabled id="jsonvazao" name="jsonvazao">
										<option value="0" disabled selected>Selecione uma opção</option>
									</select>
								</div>
								<div class="col-lg-3 col-sm-6">
									<label for="titulo">Escolha o Modelo da Bomba</label>
									<select class="form-control" disabled id="jsonmodelo" name="jsonmodelo">
										<option value="0" disabled selected>Selecione uma opção</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row col-lg-12">
							<hr>
							<div class="m-form__actions m-form__actions">
								<button id="gerarJson" type="submit" disabled class="btn btn-primary">Gerar JSON</button>
							</div>
						</div>
						<div class="m-portlet__body">
							<form id="formJsn" action="#" method="POST" novalidate>

							</form>
						</div>
					</div>
					<!--end::Portlet-->
					<!--end::Portlet-->
				</div>
			</div>
		</div>
		<!-- Modal -->