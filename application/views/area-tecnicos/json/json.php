		<!-- END: Subheader -->
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />		        
		<div class="m-content">
			<div class="row">
				<div class="col-lg-12">
					<!--begin::Portlet-->
					<div class="m-portlet">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
									</span>
									<h3 class="m-portlet__head-text">
										Gerador de JSON
									</h3>
								</div>
							</div>
						</div>

						<div class="m-portlet__body">
							<div class="row">
								<div class="col-lg-4">
									<label for="titulo">Escolher tipo do modelo</label>
									<select class="form-control" id="jsonTipoSelect" name="jsonTipoSelect">
										<option value="1" disabled selected>Selecione uma opção</option>
										<optgroup label="BOMBA - CLH">
											<option value="1">CLH - SIMPLES</option>
											<option value="2">CLH - DUAL FRONTAL</option>
											<option value="3">CLH - DUAL LATERAL</option>
											<option value="4">CLH - DUPLA LATERAL</option>
											<option value="5">CLH - DUPLA FRONTAL</option>
											<option value="6">CLH - QUADRUPLA 4X2</option>
											<option value="7">CLH - QUADRUPLA 4X4</option>
										</optgroup>
										<optgroup label="BOMBA - CHHS">
											<option value="8">CHHS - DUAL</option>
											<option value="9">CHHS - DUPLA</option>
											<option value="10">CHHS - QUADRUPLA 4X2</option>
											<option value="11">CHHS - QUADRUPLA 4X4</option>
										</optgroup>
										<optgroup label="BOMBA - CHH">
											<option value="12">CHH - DUAL</option>
											<option value="13">CHH - QUADRUPLA 4X2</option>
											<option value="14">CHH - QUADRUPLA 4X4</option>
											<option value="15">CHH - SEXTUPLA 6X2</option>
											<option value="16">CHH - SEXTUPLA 6X4</option>
											<option value="17">CHH - OCTUPLA 8X4</option>
										</optgroup>
										<optgroup label="DISPENSER - CLH">
											<option value="18">CLH - SIMPLES D</option>
											<option value="19">CLH - DUAL FRONTAL D</option>
											<option value="20">CLH - DUAL LATERAL D</option>
											<option value="21">CLH - DUPLA LATERAL D</option>
											<option value="22">CLH - DUPLA FRONTAL D</option>
											<option value="23">CLH - QUADRUPLA 4X2 D</option>
											<option value="24">CLH - QUADRUPLA 4X4 D</option>
										</optgroup>
										<optgroup label="DISPENSER - CHHS">
											<option value="25">CHHS - DUAL D</option>
											<option value="26">CHHS - DUPLA D</option>
											<option value="27">CHHS - QUADRUPLA 4X2 D</option>
											<option value="28">CHHS - QUADRUPLA 4X4 D</option>
										</optgroup>
										<optgroup label="DISPENSER - CHH">
											<option value="29">CHH - DUAL</option>
											<option value="30">CHH - QUADRUPLA 4X2 D</option>
											<option value="31">CHH - QUADRUPLA 4X4 D</option>
											<option value="32">CHH - SEXTUPLA 6X2 D</option>
											<option value="33">CHH - SEXTUPLA 6X4 D</option>
											<option value="34">CHH - OCTUPLA 8X4 D</option>
										</optgroup>
									</select>
								</div>

								<div class="col-lg-4">
									<label for="titulo">Escolher modelo</label>

									<select class="form-control jsnSelect" id="jsonSelect" name="jsonSelect">
										<option value="1" disabled selected>Selecione uma opção</option>
									</select>

									<select class="form-control jsnSelect" id="jsonSelect1" name="jsonSelect1" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCL11PA">CCL11PA</option>
											<option value="CCL11PB">CCL11PB</option>
											<option value="CCL11EC">CCL11EC</option>
											<option value="CCL11GC">CCL11GC</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CIL11PA">CIL11PA</option>
											<option value="CIL11PB">CIL11PB</option>
											<option value="CIL11EC">CIL11EC</option>
											<option value="CIL11GC">CIL11GC</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->

									<select class="form-control jsnSelect" id="jsonSelect2" name="jsonSelect2" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCFD2PA">CCFD2PA</option>
											<option value="CCFD2PB">CCFD2PB</option>
											<option value="CCFD2EC">CCFD2EC</option>
											<option value="CCFD2GC">CCFD2GC</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CIFD2PA">CIFD2PA</option>
											<option value="CIFD2PB">CIFD2PB</option>
											<option value="CIFD2EC">CIFD2EC</option>
											<option value="CIFD2GC">CIFD2GC</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect3" name="jsonSelect3" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCLD2PA">CCLD2PA</option>
											<option value="CCLD2PB">CCLD2PB</option>
											<option value="CCLD2EC">CCLD2EC</option>
											<option value="CCLD2GC">CCLD2GC</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CILD2PA">CILD2PA</option>
											<option value="CILD2PB">CILD2PB</option>
											<option value="CILD2EC">CILD2EC</option>
											<option value="CILD2GC">CILD2GC</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect4" name="jsonSelect4" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCL22GA">CCL22GA</option>
											<option value="CCL22GB">CCL22GB</option>
											<option value="CCL22EC">CCL22EC</option>
											<option value="CCL22GD">CCL22GD</option>
											<option value="CCL22JE">CCL22JE</option>
											<option value="CCL22JF">CCL22JF</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CIL22GA">CIL22GA</option>
											<option value="CIL22GB">CIL22GB</option>
											<option value="CIL22EC">CIL22EC</option>
											<option value="CIL22GD">CIL22GD</option>
											<option value="CIL22JE">CIL22JE</option>
											<option value="CIL22JF">CIL22JF</option>

										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect5" name="jsonSelect5" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCF22GA">CCF22GA</option>
											<option value="CCF22GB">CCF22GB</option>
											<option value="CCF22EC">CCF22EC</option>
											<option value="CCF22GD">CCF22GD</option>
											<option value="CCF22JE">CCF22JE</option>
											<option value="CCR22JF">CCR22JF</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CIF22GA">CIF22GA</option>
											<option value="CIF22GB">CIF22GB</option>
											<option value="CIF22EC">CIF22EC</option>
											<option value="CIF22GD">CIF22GD</option>
											<option value="CIF22JE">CIF22JE</option>
											<option value="CIF22JF">CIF22JF</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect6" name="jsonSelect6" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCF42GA">CCF42GA</option>
											<option value="CCF42GB">CCF42GB</option>
											<option value="CCF42GD">CCF42GD</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect7" name="jsonSelect7" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCF44GA">CCF44GA</option>
											<option value="CCF44GB">CCF44GB</option>
											<option value="CCF44GD">CCF44GD</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect8" name="jsonSelect8" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCSD2PA">CCSD2PA</option>
											<option value="CCSD2PB">CCSD2PB</option>
											<option value="CCSD2EC">CCSD2EC</option>
											<option value="CCSD2GC">CCSD2GC</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CISD2PA">CISD2PA</option>
											<option value="CISD2PB">CISD2PB</option>
											<option value="CISD2EC">CISD2EC</option>
											<option value="CISD2GC">CISD2GC</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect9" name="jsonSelect9" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCS22GA">CCS22GA</option>
											<option value="CCS22GB">CCS22GB</option>
											<option value="CCS22IC">CCS22IC</option>
											<option value="CCS22GD">CCS22GD</option>
											<option value="CCS22JE">CCS22JE</option>
											<option value="CCS22JF">CCS22JF</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CIS22GA">CIS22GA</option>
											<option value="CIS22GB">CIS22GB</option>
											<option value="CIS22IC">CIS22IC</option>
											<option value="CIS22GD">CIS22GD</option>
											<option value="CIS22JE">CIS22JE</option>
											<option value="CIS22JF">CIS22JF</option>

										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect10" name="jsonSelect10" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCS42GA">CCS42GA</option>
											<option value="CCS42GB">CCS42GB</option>
											<option value="CCS42GD">CCS42GD</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect11" name="jsonSelect11" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCS44GA">CCS44GA</option>
											<option value="CCS44GB">CCS44GB</option>
											<option value="CCS44GD">CCS44GD</option>
										</optgroup>

									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect12" name="jsonSelect12" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCHD2PA">CCHD2PA</option>
											<option value="CCHD2PB">CCHD2PB</option>
											<option value="CCHD2EC">CCHD2EC</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect13" name="jsonSelect13" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCH42GA">CCH42GA</option>
											<option value="CCH42GB">CCH42GB</option>
											<option value="CCH42CC">CCH42CC</option>
											<option value="CCH42IC">CCH42IC</option>
											<option value="CCH42GD">CCH42GD</option>
											<option value="CCH42JE">CCH42JE</option>
											<option value="CCH42HE">CCH42HE</option>
											<option value="CCH42JF">CCH42JF</option>
											<option value="CCH42HF">CCH42HF</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect14" name="jsonSelect14" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCH44GA">CCH44GA</option>
											<option value="CCH44GB">CCH44GB</option>
											<option value="CCH44CC">CCH44CC</option>
											<option value="CCH44IC">CCH44IC</option>
											<option value="CCH44GD">CCH44GD</option>
											<option value="CCH44JE">CCH44JE</option>
											<option value="CCH44HE">CCH44HE</option>
											<option value="CCH44JF">CCH44JF</option>
											<option value="CCH44HF">CCH44HF</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect15" name="jsonSelect15" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCH62QA">CCH62QA</option>
											<option value="CCH62QB">CCH62QB</option>
											<option value="CCH62SC">CCH62SC</option>
											<option value="CCH62QM">CCH62QM</option>
											<option value="CCH62QN">CCH62QN</option>
											<option value="CCH62UG">CCH62UG</option>
											<option value="CCH62UP">CCH62UP</option>
											<option value="CCH62HQ">CCH62HQ</option>
											<option value="CCH62H6">CCH62H6</option>
											<option value="CCH62H7">CCH62H7</option>
											<option value="CCH62H8">CCH62H8</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect16" name="jsonSelect16" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCH64QA">CCH64QA</option>
											<option value="CCH64QB">CCH64QB</option>
											<option value="CCH64SC">CCH64SC</option>
											<option value="CCH64QM">CCH64QM</option>
											<option value="CCH64QN">CCH64QN</option>
											<option value="CCH64UG">CCH64UG</option>
											<option value="CCH64UP">CCH64UP</option>
											<option value="CCH64HQ">CCH64HQ</option>
											<option value="CCH64H6">CCH64H6</option>
											<option value="CCH64H7">CCH64H7</option>
											<option value="CCH64H8">CCH64H8</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect17" name="jsonSelect17" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CCH84RA">CCH84RA</option>
											<option value="CCH84RB">CCH84RB</option>
											<option value="CCH84CC">CCH84CC</option>
											<option value="CCH84RH">CCH84RH</option>
											<option value="CCH84RI">CCH84RI</option>
											<option value="CCH84RJ">CCH84RJ</option>
											<option value="CCH84VU">CCH84VU</option>
											<option value="CCH84VV">CCH84VV</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect18" name="jsonSelect18" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDL11DA">CDL11DA</option>
											<option value="CDL11DB">CDL11DB</option>
											<option value="CDL11DC">CDL11DC</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CEL11DA">CEL11DA</option>
											<option value="CEL11DB">CEL11DB</option>
											<option value="CEL11DC">CEL11DC</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect19" name="jsonSelect19" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDFD2DA">CDFD2DA</option>
											<option value="CDFD2DB">CDFD2DB</option>
											<option value="CDFD2DC">CDFD2DC</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CEFD2DA">CEFD2DA</option>
											<option value="CEFD2DB">CEFD2DB</option>
											<option value="CEFD2DC">CEFD2DC</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect20" name="jsonSelect20" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDLD2DA">CDLD2DA</option>
											<option value="CDLD2DB">CDLD2DB</option>
											<option value="CDLD2DC">CDLD2DC</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CELD2DA">CELD2DA</option>
											<option value="CELD2DB">CELD2DB</option>
											<option value="CELD2DC">CELD2DC</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect21" name="jsonSelect21" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDL22DA">CDL22DA</option>
											<option value="CDL22DB">CDL22DB</option>
											<option value="CDL22DC">CDL22DC</option>
											<option value="CDL22DD">CDL22DD</option>
											<option value="CDL22DE">CDL22DE</option>
											<option value="CDL22DF">CDL22DF</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CEL22DA">CEL22DA</option>
											<option value="CEL22DB">CEL22DB</option> 
											<option value="CEL22DC">CEL22DC</option>
											<option value="CEL22DD">CEL22DD</option>
											<option value="CEL22DE">CEL22DE</option>
											<option value="CEL22DF">CEL22DF</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect22" name="jsonSelect22" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDF22DA">CDF22DA</option>
											<option value="CDF22DB">CDF22DB</option>
											<option value="CDF22DC">CDF22DC</option>
											<option value="CDF22DD">CDF22DD</option>
											<option value="CDF22DE">CDF22DE</option>
											<option value="CDF22DF">CDF22DF</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CEF22DA">CEF22DA</option>
											<option value="CEF22DB">CEF22DB</option>
											<option value="CEF22DC">CEF22DC</option>
											<option value="CEF22DD">CEF22DD</option>
											<option value="CEF22DE">CEF22DE</option>
											<option value="CEF22DF">CEF22DF</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect23" name="jsonSelect23" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDF42DA">CDF42DA</option>
											<option value="CDF42DB">CDF42DB</option>
											<option value="CDF42DD">CDF42DD</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect24" name="jsonSelect24" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CEF44DA">CEF44DA</option>
											<option value="CEF44DB">CEF44DB</option>
											<option value="CEF44DD">CEF44DD</option>
										</optgroup>

									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect25" name="jsonSelect25" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDSD2DA">CDSD2DA</option>
											<option value="CDSD2DB">CDSD2DB</option>
											<option value="CDSD2DC">CDSD2DC</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CESD2DA">CESD2DA</option>
											<option value="CESD2DB">CESD2DB</option>
											<option value="CESD2DC">CESD2DC</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect26" name="jsonSelect26" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDS22DA">CDS22DA</option>
											<option value="CDS22DB">CDS22DB</option>
											<option value="CDS22DC">CDS22DC</option>
											<option value="CDS22DD">CDS22DD</option>
											<option value="CDS22DE">CDS22DE</option>
											<option value="CDS22DF">CDS22DF</option>
										</optgroup>

										<optgroup label="INDUSTRIAL">
											<option value="CES22DA">CES22DA</option>
											<option value="CES22DB">CES22DB</option>
											<option value="CES22DC">CES22DC</option>
											<option value="CES22DD">CES22DD</option>
											<option value="CES22DE">CES22DE</option>
											<option value="CES22DF">CES22DF</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect27" name="jsonSelect27" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDS42DA">CDS42DA</option>
											<option value="CDS42DB">CDS42DB</option>
											<option value="CDS42DD">CDS42DD</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect28" name="jsonSelect28" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDS44DA">CDS44DA</option>
											<option value="CDS44DB">CDS44DB</option>
											<option value="CDS44DD">CDS44DD</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect29" name="jsonSelect29" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDHD2DA">CDHD2DA</option>
											<option value="CDHD2DB">CDHD2DB</option>
											<option value="CDHD2DC">CDHD2DC</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect30" name="jsonSelect30" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDH42DA">CDH42DA</option>
											<option value="CDH42DB">CDH42DB</option>
											<option value="CDH42DC">CDH42DC</option>
											<option value="CDH42DD">CDH42DD</option>
											<option value="CDH42DE">CDH42DE</option>
											<option value="CDH42DF">CDH42DF</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect31" name="jsonSelect31" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDH44DA">CDH44DA</option>
											<option value="CDH44DB">CDH44DB</option>
											<option value="CDH44DC">CDH44DC</option>
											<option value="CDH44DD">CDH44DD</option>
											<option value="CDH44DE">CDH44DE</option>
											<option value="CDH44DF">CDH44DF</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect32" name="jsonSelect32" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDH62DA">CDH62DA</option>
											<option value="CDH62DB">CDH62DB</option>
											<option value="CDH62DC">CDH62DC</option>
											<option value="CDH62DM">CDH62DM</option>
											<option value="CDH62DN">CDH62DN</option>
											<option value="CDH62DG">CDH62DG</option>
											<option value="CDH62DP">CDH62DP</option>
											<option value="CDH62DQ">CDH62DQ</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
									<select class="form-control jsnSelect" id="jsonSelect33" name="jsonSelect33" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDH64DA">CDH64DA</option>
											<option value="CDH64DB">CDH64DB</option>
											<option value="CDH64DM">CDH64DM</option>
											<option value="CDH64DN">CDH64DN</option>
											<option value="CDH64DG">CDH64DG</option>
											<option value="CDH64DP">CDH64DP</option>
										</optgroup>
									</select>

									<select class="form-control jsnSelect" id="jsonSelect34" name="jsonSelect34" hidden>
										<option value="1" disabled selected>Selecione uma opção</option>

										<optgroup label="COMERCIAL">
											<option value="CDH84DA">CDH84DA</option>
											<option value="CDH84DB">CDH84DB</option>
											<option value="CDH84DH">CDH84DH</option>
											<option value="CDH84DI">CDH84DI</option>
											<option value="CDH84DJ">CDH84DJ</option>
										</optgroup>
									</select>
									<!-- ######################################################################### -->
								</div>
								<div class="col-lg-4">
									<label for="titulo">Carregar json existente </label>
									<input type="file" class="form-control" id="txtfiletoread" accept=".json,.jsn" placeholder="json">
								</div>
							</div>
							<div class=" explic" hidden>
								<br>
								<div class="row">
									<div class="form-group col-md-12 col-sm-12 col-lg-4">
										<h5 id="exAno" for="titulo"></h5>
										<h5 id="exUtilizacao" for="titulo"></h5>
										<h5 id="exTipo" for="titulo"></h5>
										<h5 id="exVersao" for="titulo"></h5>
										<h5 id="exNumeroAbast" for="titulo"></h5>
										<h5 id="exRotativa" for="titulo"></h5> 
										<h5 id="exVazao" for="titulo"></h5>
									</div>
									<div class="form-group col-md-12 col-sm-12 col-lg-8">
										<a href="#" onclick="showModelImg()"><center><img width="50%" class="img-fluid" id="img-esquema-bomba"></center></a>
										<br><br>
										<center><h6>Clique na imagem para visualizar em alta resolução.</h6></center>
									</div>
								</div>
							</div>
						</div>
						<!--begin::Modal-->
						<div class="modal fade" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Esquema de ligação</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<img  class="img-fluid" id="img-esquema-bomba-2">
									</div>
								</div>
							</div>
						</div>
						<!--end::Modal-->
						<script type="text/javascript">
							
							function showModelImg(){
								$("#m_modal_4").modal();
							}
							function closeModalimg(){
								$('#m_modal_4').modal('hide');	
							}
						</script>
						<div class="nenh" hidden>
							<center><label for="titulo">Arquivo não disponível!</label></center>
						</div>
						<div class="m-portlet__body">
							<form id="formJsn" action="#" method="POST" novalidate>
								<div class="jsn" id="jsn" hidden>
									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="titulo">Título</label>
												<input type="text" class="form-control" id="titulo"  name="titulo" placeholder="Título" required>
											</div>
											<div class="form-group col-md-2">
												<label for="titulo">Versão</label>
												<input type="text" class="form-control" id="versao"  name="versao" placeholder="versao" required>
											</div>
											<div class="form-group col-md-2">
												<label for="titulo">Unidade</label>
												<select class="form-control" id="unidade" name="unidade" required>
													<option value="litros">Litros</option>
													<option value="galoes">Galões</option>
												</select>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Modelo</label>
												<input type="text" class="form-control" id="modelo" disabled="true" name="modelo" placeholder="modelo" required>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="titulo">CNPJ</label>
												<input type="text" class="form-control" id="cnpj" name="cnpj" placeholder="CNPJ" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">IP</label>
												<input type="text" class="form-control" id="ip" name="ip" placeholder="IP" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Gateway</label>
												<input type="text" class="form-control" id="gw" name="gw" placeholder="Gateway" required>
											</div>
										</div>
									</div>

									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="titulo">Máscara de rede</label>
												<input type="text" class="form-control" id="Mask" name="Mask" placeholder="Máscara de rede" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">DNS</label>
												<input type="text" class="form-control" id="dns" name="dns" placeholder="DNS" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">DHCP</label>
												<select class="form-control" id="dhcp" name="dhcp" required>
													<option value="on">ON</option>
													<option value="off">OFF</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="titulo">Hostname</label>
												<input type="text" class="form-control" id="hostname" name="hostname" placeholder="Hostname" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo" id="industrialT" hidden>Industrial</label>
												<input type="text" class="form-control" disabled="true" id="industrial" name="industrial" hidden>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">BLE Id</label>
												<input type="text" class="form-control" id="ble_id" name="ble_id" placeholder="BLE Id" required>
											</div>
										</div>
									</div>

									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="titulo">Protocolo 1</label>
												<select class="form-control" id="protocolo1" name="protocolo1" required>
													<option value="wayne">Wayne</option>
													<option value="wertco">Wertco</option>
												</select>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Protocolo 2</label>
												<select class="form-control" id="protocolo2" name="protocolo2" required>
													<option value="wayne">Wayne</option>
													<option value="wertco">Wertco</option>
												</select>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Automação</label>

												<select  class="form-control" id="Automacao" name="Automacao" required>
													<option value="Remoto">Remoto</option>
													<option value="Local">Local</option>
													<option value="Remoto">RemotoIDF</option>
													<option value="Local">LocalIDF</option>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="titulo">Identfid</label>
												<input type="text" class="form-control" id="Identfid" name="Identfid" placeholder="Identfid" required>
											</div>
											<div class="form-group col-md-4">
												<label for="valvula">Válvula</label>
												<input type="text" class="form-control" id="valvula" name="valvula" placeholder="Válvula" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Falta de pulsos</label>
												<input type="number" class="form-control" id="falta_de_pulsos" name="falta_de_pulsos" placeholder="Falta de pulsos" required>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="titulo">Tempo de start</label>
												<input type="number" class="form-control" id="tempo_de_start" name="tempo_de_start" placeholder="Tempo de start" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Beep</label>
												<select class="form-control" id="beep" name="beep" required>
													<option value="Ligado">Ligado</option>
													<option value="Desligado">Desligado</option>
												</select>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Tempo desliga</label>
												<input type="number" class="form-control" id="Tempo_Desliga" name="Tempo_Desliga" placeholder="Tempo desliga" required>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-8">
												<label for="titulo">Horário de funcionamento</label>

												<div class="input-group timepicker">
													<div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-clock-o"></i>
														</span>
													</div>
													<input type="text" class="form-control m-input" readonly="" id="Timer_Ligar" name="Timer_Ligar" placeholder="Timer ligar" required>
												</div>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Ponto baixa vazão</label>
												<input type="text" class="form-control" id="ponto_baixa_vazao" name="ponto_baixa_vazao" placeholder="Ponto baixa vazão" required>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="titulo">Limite Volume</label>
												<input type="number" class="form-control" id="Limite_Volume" name="Limite_Volume" placeholder="Limite volume" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Total decimal</label>
												<input type="number" class="form-control" id="total_Decimal" name="total_Decimal" placeholder="Total decimal" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Volume decimal</label>
												<input type="number" class="form-control" id="volume_Decimal" name="volume_Decimal" placeholder="Volume decimal" required>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="titulo">Preço decimal</label>
												<input type="number" class="form-control" id="preco_Decimal" name="preco_Decimal" placeholder="Preço decimal" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Decimais no encerrante</label>
												<input type="number" class="form-control" id="encerrante_dec" name="encerrante_dec" placeholder="Encerrante decimal" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Decimais no cálculo encerrante</label>
												<input type="number" class="form-control" id="encerrante_calc" name="encerrante_calc" placeholder="Encerrante calc" required>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="titulo">Função da tecla P</label>
												<input type="text" class="form-control" id="preset_P1" name="preset_P1" placeholder="Preset 1" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Função tecla M</label>

												<select  class="form-control" id="preset_P2" name="preset_P2" required>
													<option>$xxx</option>
													<option>Vxxx</option>
													<option value="M2">M2</option>
													<option value="M3">M3</option>
													<option value="A2">A2</option>
													<option value="A3">A3</option>
													<option value="B2">B2</option>
													<option value="B3">B3</option>
													<option value="C2">C2</option>
													<option value="C3">C3</option>
													<option value="D2">D2</option>
													<option value="D3">D3</option>
												</select>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Arredonda</label>
												<input type="number" class="form-control" id="Arredonda" name="Arredonda" placeholder="Arredonda" required>
											</div>
										</div>
									</div>
									<div class="form-group col-md-12">
										<div class="row">
											<div class="form-group col-md-4">
												<label for="titulo">Texto 1</label>
												<input type="text" class="form-control" id="texto_1" name="texto_1" placeholder="Texto 1" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Texto 2</label>
												<input type="text" class="form-control" id="texto_2" name="texto_2" placeholder="Texto 2" required>
											</div>
											<div class="form-group col-md-4">
												<label for="titulo">Texto 3</label>
												<input type="text" class="form-control" id="texto_3" name="texto_3" placeholder="Texto 3" required>
											</div>
										</div>
									</div>
									<!-- ## COMECO SLOT 1 -->
									<div class="m-portlet__body_slot">                                           
										<div class="m-accordion m-accordion--default m-accordion--solid" id="SLOT1" role="tablist">               
											<div class="m-accordion__item">
												<div class="m-accordion__item-head collapsed"  role="tab" id="SLOT1_head" data-toggle="collapse" href="#SLOT1_body" aria-expanded="    false">
													<span class="m-accordion__item-title">SLOT 1</span>

													<span class="m-accordion__item-mode"></span>     
												</div>

												<div class="m-accordion__item-body collapse" id="SLOT1_body" class=" " role="tabpanel" aria-labelledby="SLOT1_head" data-parent="#SLOT1"> 
													<div class="m-accordion__item-content">

														<div class="row form-group col-md-12">
															<div class="form-group col-md-4">
																<label for="titulo">Lado Inmetro</label>
																<input type="text" class="form-control" id="ladoInmetro1" name="ladoInmetro" placeholder="Lado Inmetro" required>
															</div>
															<div class="form-group col-md-4">
																<label>Lógico</label>
																<input type="number" class="form-control" id="logico1" name="logico1" placeholder="Lógico" required>
															</div>
															<div class="form-group col-md-4">
																<label>Display A</label>
																<input type="text" class="form-control" id="dspA1" name="dspA1" placeholder="dspA" required>
															</div>
														</div>
														<div class="row form-group col-md-12">
															<div class="form-group col-md-4">
																<label>Display B</label>
																<input type="text" class="form-control" id="dspB1" name="dspB1" placeholder="dspB" required>
															</div>
															<div class="form-group col-md-4">
																<label>Endereço do display eletrômec.</label>
																<input type="text" class="form-control" id="eltm1" name="eltm1" placeholder="eltm" required>
															</div>
															<div class="form-group col-md-4">
																<label>LCD ordem</label>
																<input type="text" class="form-control" id="LcdOrdem1" name="LcdOrdem1" placeholder="LCD ordem" required>
															</div>
														</div>
														<!-- ## COMECO POS 1 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS1" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS1_head" data-toggle="collapse" href="#POS1_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 1</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS1_body" class=" " role="tabpanel" aria-labelledby="POS1_head" data-parent="#POS1"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico1" name="bico1" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode1" name="mode1" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl1" name="eltmCnl1" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA1" name="valvA1" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA1" name="pulserA1" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA1" name="motorA1" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav1" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB1" name="valvB1" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB1" name="pulserB1" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB1" name="motorB1" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V1" name="ppl_V1" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C1" name="ppl_C1" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P1" name="ppl_P1" placeholder="ppl_P" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P1" name="rv_P1" placeholder="rv_P" required>
																			</div>
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P1" name="pwm_a_P1" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm1">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P1" name="pwm_b_P1" placeholder="pwm b" required>
																			</div>
																		</div>
																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 1 -->
														<!-- ## COMECO POS 2 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS2" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS2_head" data-toggle="collapse" href="#POS2_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 2</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS2_body" class=" " role="tabpanel" aria-labelledby="POS2_head" data-parent="#POS2"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico2" name="bico2" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode2" name="mode2" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl2" name="eltmCnl2" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA2" name="valvA2" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA2" name="pulserA2" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA2" name="motorA2" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav2" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB2" name="valvB2" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB2" name="pulserB1" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB2" name="motorB2" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V2" name="ppl_V2" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C2" name="ppl_C2" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P2" name="ppl_P2" placeholder="ppl_P" required>
																			</div>																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P2" name="rv_P2" placeholder="ppl_P" required>
																			</div>
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P2" name="pwm_a_P2" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm2">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P2" name="pwm_b_P2" placeholder="pwm b" required>
																			</div>
																		</div>
																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 2 -->
														<!-- ## COMECO POS 3 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS3" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS3_head" data-toggle="collapse" href="#POS3_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 3</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS3_body" class=" " role="tabpanel" aria-labelledby="POS3_head" data-parent="#POS2"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico3" name="bico3" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode3" name="mode3" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl3" name="eltmCnl3" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA3" name="valvA3" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA3" name="pulserA3" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA3" name="motorA3" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav3" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB3" name="valvB3" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB3" name="pulserB1" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB3" name="motorB3" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V3" name="ppl_V3" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C3" name="ppl_C3" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P3" name="ppl_P3" placeholder="ppl_P" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P3" name="rv_P3" placeholder="ppl_P" required>
																			</div>
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P3" name="pwm_a_P3" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm3">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P3" name="pwm_b_P3" placeholder="pwm b" required>
																			</div>
																		</div>
																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 3 -->
													</div>
												</div>
											</div>                    
										</div>
									</div>
									<!-- ## FIM SLOT 1 -->

									<!-- ## COMECO SLOT 2 -->
									<div class="m-portlet__body_slot">                                           
										<div class="m-accordion m-accordion--default m-accordion--solid" id="SLOT2" role="tablist">               
											<div class="m-accordion__item">
												<div class="m-accordion__item-head collapsed"  role="tab" id="SLOT2_head" data-toggle="collapse" href="#SLOT2_body" aria-expanded="    false">
													<span class="m-accordion__item-title">SLOT 2</span>

													<span class="m-accordion__item-mode"></span>     
												</div>

												<div class="m-accordion__item-body collapse" id="SLOT2_body" class=" " role="tabpanel" aria-labelledby="SLOT2_head" data-parent="#SLOT2"> 
													<div class="m-accordion__item-content">

														<div class="row form-group col-md-12">
															<div class="form-group col-md-4">
																<label for="titulo">Lado Inmetro</label>
																<input type="text" class="form-control" id="ladoInmetro2" name="ladoInmetro2" placeholder="Lado Inmetro" required>
															</div>
															<div class="form-group col-md-4">
																<label>Lógico</label>
																<input type="number" class="form-control" id="logico2" name="logico2" placeholder="Lógico" required>
															</div>
															<div class="form-group col-md-4">
																<label>Display A</label>
																<input type="text" class="form-control" id="dspA2" name="dspA2" placeholder="dspA" required>
															</div>
														</div>
														<div class="row form-group col-md-12">
															<div class="form-group col-md-4">
																<label>Display B</label>
																<input type="text" class="form-control" id="dspB2" name="dspB2" placeholder="dspB" required>
															</div>
															<div class="form-group col-md-4">
																<label>Endereço do display eletrômec.</label>
																<input type="text" class="form-control" id="eltm2" name="eltm2" placeholder="eltm" required>
															</div>
															<div class="form-group col-md-4">
																<label>LCD ordem</label>
																<input type="text" class="form-control" id="LcdOrdem2" name="LcdOrdem2" placeholder="LCD ordem" required>
															</div>
														</div>
														<!-- ## COMECO POS 1 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS4" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS4_head" data-toggle="collapse" href="#POS4_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 1</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS4_body" class=" " role="tabpanel" aria-labelledby="POS4_head" data-parent="#POS1"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico4" name="bico4" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode4" name="mode4" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl4" name="eltmCnl4" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA4" name="valvA4" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA4" name="pulserA4" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA4" name="motorA4" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav4" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB4" name="valvB4" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB4" name="pulserB4" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB4" name="motorB4" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V4" name="ppl_V4" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C4" name="ppl_C4" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P4" name="ppl_P4" placeholder="ppl_P" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P4" name="rv_P4" placeholder="ppl_P" required>
																			</div>																			
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P4" name="pwm_a_P4" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm4">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P4" name="pwm_b_P4" placeholder="pwm b" required>
																			</div>
																		</div>

																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 1 -->
														<!-- ## COMECO POS 2 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS5" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS5_head" data-toggle="collapse" href="#POS5_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 2</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS5_body" class=" " role="tabpanel" aria-labelledby="POS5_head" data-parent="#POS2"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico5" name="bico5" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode5" name="mode5" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl5" name="eltmCnl5" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA5" name="valvA5" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA5" name="pulserA5" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA5" name="motorA5" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav5" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB5" name="valvB5" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB5" name="pulserB5" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB5" name="motorB5" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V5" name="ppl_V5" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C5" name="ppl_C5" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P5" name="ppl_P5" placeholder="ppl_P" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P5" name="rv_P5" placeholder="ppl_P" required>
																			</div>
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P5" name="pwm_a_P5" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm5">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P5" name="pwm_b_P5" placeholder="pwm b" required>
																			</div>
																		</div>

																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 2 -->
														<!-- ## COMECO POS 3 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS6" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS6_head" data-toggle="collapse" href="#POS6_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 3</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS6_body" class=" " role="tabpanel" aria-labelledby="POS6_head" data-parent="#POS2"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico6" name="bico3" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode6" name="mode6" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl6" name="eltmCnl6" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA6" name="valvA6" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA6" name="pulserA6" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA6" name="motorA6" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav6" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB6" name="valvB6" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB6" name="pulserB6" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB6" name="motorB6" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V6" name="ppl_V6" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C6" name="ppl_C6" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P6" name="ppl_P6" placeholder="ppl_P" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P6" name="rv_P6" placeholder="ppl_P" required>
																			</div>
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P6" name="pwm_a_P6" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm6">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P6" name="pwm_b_P6" placeholder="pwm b" required>
																			</div>
																		</div>

																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 3 -->
													</div>
												</div>
											</div>                    
										</div>
									</div>
									<!-- ## FIM SLOT 2 -->

									<!-- ## COMECO SLOT 3 -->
									<div class="m-portlet__body_slot">                                           
										<div class="m-accordion m-accordion--default m-accordion--solid" id="SLOT3" role="tablist">               
											<div class="m-accordion__item">
												<div class="m-accordion__item-head collapsed"  role="tab" id="SLOT3_head" data-toggle="collapse" href="#SLOT3_body" aria-expanded=" false">
													<span class="m-accordion__item-title">SLOT 3</span>

													<span class="m-accordion__item-mode"></span>     
												</div>

												<div class="m-accordion__item-body collapse" id="SLOT3_body" class=" " role="tabpanel" aria-labelledby="SLOT3_head" data-parent="#SLOT3"> 
													<div class="m-accordion__item-content">

														<div class="row form-group col-md-12">
															<div class="form-group col-md-4">
																<label for="titulo">Lado Inmetro</label>
																<input type="text" class="form-control" id="ladoInmetro3" name="ladoInmetro3" placeholder="Lado Inmetro" required>
															</div>
															<div class="form-group col-md-4">
																<label>Lógico</label>
																<input type="number" class="form-control" id="logico3" name="logico3" placeholder="Lógico" required>
															</div>
															<div class="form-group col-md-4">
																<label>Display A</label>
																<input type="text" class="form-control" id="dspA3" name="dspA3" placeholder="dspA" required>
															</div>
														</div>
														<div class="row form-group col-md-12">
															<div class="form-group col-md-4">
																<label>Display B</label>
																<input type="text" class="form-control" id="dspB3" name="dspB3" placeholder="dspB" required>
															</div>
															<div class="form-group col-md-4">
																<label>Endereço do display eletrômec.</label>
																<input type="text" class="form-control" id="eltm3" name="eltm3" placeholder="eltm" required>
															</div>
															<div class="form-group col-md-4">
																<label>LCD ordem</label>
																<input type="text" class="form-control" id="LcdOrdem3" name="LcdOrdem3" placeholder="LCD ordem" required>
															</div>
														</div>
														<!-- ## COMECO POS 1 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS7" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS7_head" data-toggle="collapse" href="#POS7_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 1</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS7_body" class=" " role="tabpanel" aria-labelledby="POS7_head" data-parent="#POS1"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico7" name="bico7" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode7" name="mode7" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl7" name="eltmCnl7" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA7" name="valvA7" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA7" name="pulserA7" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA7" name="motorA7" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav7" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB7" name="valvB7" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB7" name="pulserB7" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB7" name="motorB7" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V7" name="ppl_V7" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C7" name="ppl_C7" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P7" name="ppl_P7" placeholder="ppl_P" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P7" name="rv_P7" placeholder="ppl_P" required>
																			</div>
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P7" name="pwm_a_P7" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm7">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P7" name="pwm_b_P7" placeholder="pwm b" required>
																			</div>
																		</div>

																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 1 -->
														<!-- ## COMECO POS 2 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS8" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS8_head" data-toggle="collapse" href="#POS8_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 2</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS8_body" class=" " role="tabpanel" aria-labelledby="POS8_head" data-parent="#POS2"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico8" name="bico8" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode8" name="mode8" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl8" name="eltmCnl8" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA8" name="valvA8" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA8" name="pulserA8" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA8" name="motorA8" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav8" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB8" name="valvB8" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB8" name="pulserB8" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB8" name="motorB8" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V8" name="ppl_V8" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C8" name="ppl_C8" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P8" name="ppl_P8" placeholder="ppl_P" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P8" name="rv_P8" placeholder="ppl_P" required>
																			</div>
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P8" name="pwm_a_P8" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm8">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P8" name="pwm_b_P8" placeholder="pwm b" required>
																			</div>
																		</div>																		
																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 2 -->
														<!-- ## COMECO POS 3 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS9" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS9_head" data-toggle="collapse" href="#POS9_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 3</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS9_body" class=" " role="tabpanel" aria-labelledby="POS9_head" data-parent="#POS2"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico9" name="bico9" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode9" name="mode9" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl9" name="eltmCnl9" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA9" name="valvA9" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA9" name="pulserA9" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA9" name="motorA9" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav9" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB9" name="valvB9" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB9" name="pulserB9" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB9" name="motorB9" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V9" name="ppl_V9" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C9" name="ppl_C9" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P9" name="ppl_P9" placeholder="ppl_P" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P9" name="rv_P9" placeholder="ppl_P" required>
																			</div>
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P9" name="pwm_a_P9" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm9">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P9" name="pwm_b_P9" placeholder="pwm b" required>
																			</div>
																		</div>
																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 3 -->
													</div>
												</div>
											</div>                    
										</div>
									</div>
									<!-- ## FIM SLOT 3 -->



									<!-- ## COMECO SLOT 4 -->
									<div class="m-portlet__body_slot">                                           
										<div class="m-accordion m-accordion--default m-accordion--solid" id="SLOT4" role="tablist">               
											<div class="m-accordion__item">
												<div class="m-accordion__item-head collapsed"  role="tab" id="SLOT4_head" data-toggle="collapse" href="#SLOT4_body" aria-expanded=" false">
													<span class="m-accordion__item-title">SLOT 4</span>

													<span class="m-accordion__item-mode"></span>     
												</div>

												<div class="m-accordion__item-body collapse" id="SLOT4_body" class=" " role="tabpanel" aria-labelledby="SLOT4_head" data-parent="#SLOT4"> 
													<div class="m-accordion__item-content">

														<div class="row form-group col-md-12">
															<div class="form-group col-md-4">
																<label for="titulo">Lado Inmetro</label>
																<input type="text" class="form-control" id="ladoInmetro4" name="ladoInmetro4" placeholder="Lado Inmetro" required>
															</div>
															<div class="form-group col-md-4">
																<label>Lógico</label>
																<input type="number" class="form-control" id="logico4" name="logico4" placeholder="Lógico" required>
															</div>
															<div class="form-group col-md-4">
																<label>Display A</label>
																<input type="text" class="form-control" id="dspA4" name="dspA4" placeholder="dspA" required>
															</div>
														</div>
														<div class="row form-group col-md-12">
															<div class="form-group col-md-4">
																<label>Display B</label>
																<input type="text" class="form-control" id="dspB4" name="dspB4" placeholder="dspB" required>
															</div>
															<div class="form-group col-md-4">
																<label>Endereço do display eletrômec.</label>
																<input type="text" class="form-control" id="eltm4" name="eltm4" placeholder="eltm" required>
															</div>
															<div class="form-group col-md-4">
																<label>LCD ordem</label>
																<input type="text" class="form-control" id="LcdOrdem4" name="LcdOrdem4" placeholder="LCD ordem" required>
															</div>
														</div>
														<!-- ## COMECO POS 1 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS10" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS10_head" data-toggle="collapse" href="#POS10_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 1</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS10_body" class=" " role="tabpanel" aria-labelledby="POS10_head" data-parent="#POS1"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico10" name="bico10" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode10" name="mode10" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl10" name="eltmCnl10" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA10" name="valvA10" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA10" name="pulserA10" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA10" name="motorA10" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav10" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB10" name="valvB10" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB10" name="pulserB10" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB10" name="motorB10" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V10" name="ppl_V10" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C10" name="ppl_C10" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P10" name="ppl_P10" placeholder="ppl_P" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P10" name="rv_P10" placeholder="ppl_P" required>
																			</div>
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P10" name="pwm_a_P10" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm10">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P10" name="pwm_b_P10" placeholder="pwm b" required>
																			</div>
																		</div>
																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 1 -->
														<!-- ## COMECO POS 2 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS11" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS11_head" data-toggle="collapse" href="#POS11_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 2</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS11_body" class=" " role="tabpanel" aria-labelledby="POS11_head" data-parent="#POS2"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico11" name="bico11" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode11" name="mode11" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl11" name="eltmCnl11" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA11" name="valvA11" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA11" name="pulserA11" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA11" name="motorA11" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav11" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB11" name="valvB11" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB11" name="pulserB11" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB11" name="motorB11" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V11" name="ppl_V11" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C11" name="ppl_C11" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P11" name="ppl_P11" placeholder="ppl_P" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P11" name="rv_P11" placeholder="ppl_P" required>
																			</div>
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P11" name="pwm_a_P11" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm11">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P11" name="pwm_b_P11" placeholder="pwm b" required>
																			</div>
																		</div>
																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 2 -->
														<!-- ## COMECO POS 3 -->
														<div class="m-accordion m-accordion--default m-accordion--solid" id="POS12" role="tablist">               
															<div class="m-accordion__item">
																<div class="m-accordion__item-head collapsed"  role="tab" id="POS12_head" data-toggle="collapse" href="#POS12_body" aria-expanded="    false">
																	<span class="m-accordion__item-title">POSIÇÃO 3</span>

																	<span class="m-accordion__item-mode"></span>     
																</div>

																<div class="m-accordion__item-body collapse" id="POS12_body" class=" " role="tabpanel" aria-labelledby="POS12_head" data-parent="#POS2"> 
																	<div class="m-accordion__item-content">
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label>Bico</label>
																				<input type="text" class="form-control" id="bico12" name="bico12" placeholder="Bico" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="mode">Modo, L: BAIXA VAZÃO, H: ALTA VAZÃO</label>
																				<input type="text" class="form-control" id="mode12" name="mode12" placeholder="mode" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="titulo">Canal do contador do eletrômec.</label>
																				<input type="text" class="form-control" id="eltmCnl12" name="eltmCnl12" placeholder="eltmCnl" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-4">
																				<label for="valvA">Válvula A</label>
																				<input type="text" class="form-control" id="valvA12" name="valvA12" placeholder="Válvula A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserA">Pulser A</label>
																				<input type="text" class="form-control" id="pulserA12" name="pulserA12" placeholder="Pulser A" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorA">Motor A</label>
																				<input type="text" class="form-control" id="motorA12" name="motorA12" placeholder="Motor A" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12" id="altav12" hidden>
																			<div class="form-group col-md-4">
																				<label for="valvB">Válvula B</label>
																				<input type="text" class="form-control" id="valvB12" name="valvB12" placeholder="Válvula B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="pulserB">Pulser B</label>
																				<input type="text" class="form-control" id="pulserB12" name="pulserB12" placeholder="Pulser B" required>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="motorB">Motor B</label>
																				<input type="text" class="form-control" id="motorB12" name="motorB12" placeholder="Motor B" required>
																			</div>
																		</div>

																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>Preço por litro à vista</label>
																				<input type="text" class="form-control" id="ppl_V12" name="ppl_V12" placeholder="ppl_V" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro crédito</label>
																				<input type="text" class="form-control" id="ppl_C12" name="ppl_C12" placeholder="ppl_C" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Preço por litro parcelado</label>
																				<input type="text" class="form-control" id="ppl_P12" name="ppl_P12" placeholder="ppl_P" required>
																			</div>
																			<div class="form-group col-md-3">
																				<label>Recuperação de Vapor</label>
																				<input type="text" class="form-control" id="rv_P12" name="rv_P12" placeholder="ppl_P" required>
																			</div>
																		</div>
																		<div class="row form-group col-md-12">
																			<div class="form-group col-md-3">
																				<label>PWM A</label>
																				<input type="text" class="form-control" id="pwm_a_P12" name="pwm_a_P12" placeholder="pwm a" required>
																			</div>
																			<div class="form-group col-md-3" id="labelpwm12">
																				<label>PWM B</label>
																				<input type="text" class="form-control" id="pwm_b_P12" name="pwm_b_P12" placeholder="pwm b" required>
																			</div>
																		</div>
																	</div>
																</div>
															</div>                    
														</div>
														<!-- ## FIM POS 3 -->
													</div>
												</div>
											</div>                    
										</div>
									</div>
									<!-- ## FIM SLOT 4 -->

									<div class="row col-lg-12">
										<hr>
										<div class="m-form__actions m-form__actions">
											<label class="m-checkbox m-checkbox--state-brand">
												<input id="check_cruzado" type="checkbox" >Gerar código de bico cruzado     <span></span>
											</label>
											<button type="submit" class="btn btn-primary">Gerar JSON</button>
										</div>
									</div>
								</div>
							</form>
						</div>


					</div>
					<!--end::Portlet-->
					<!--end::Portlet-->
				</div>
			</div>	
		</div>	
		<!-- Modal -->

		<!--begin::Modal-->
		<div class="modal fade" id="m_timepicker_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="">Horário de funcionamento do posto</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true" class="la la-remove"></span>
						</button>
					</div>
					<form class="m-form m-form--fit m-form--label-align-right">
						<div class="modal-body">
							<div class="form-group m-form__group row">
								<label class="col-form-label col-lg-3 col-sm-12">Domingo:</label>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Início do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="ini_dom" readonly placeholder="Select time" type="text"/>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label  col-sm-12">Fim do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="fim_dom" readonly placeholder="Select time" type="text"/>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-form-label col-lg-3 col-sm-12">Segunda:</label>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Início do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="ini_seg" readonly placeholder="Select time" type="text"/>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Fim do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="fim_seg" readonly placeholder="Select time" type="text"/>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-form-label col-lg-3 col-sm-12">Terça:</label>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Início do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="ini_ter" readonly placeholder="Select time" type="text"/>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Fim do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="fim_ter" readonly placeholder="Select time" type="text"/>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-form-label col-lg-3 col-sm-12">Quarta:</label>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Início do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="ini_qua" readonly placeholder="Select time" type="text"/>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Fim do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="fim_qua" readonly placeholder="Select time" type="text"/>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-form-label col-lg-3 col-sm-12">Quinta:</label>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Início do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="ini_qui" readonly placeholder="Select time" type="text"/>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Fim do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="fim_qui" readonly placeholder="Select time" type="text"/>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-form-label col-lg-3 col-sm-12">Sexta:</label>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Início do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="ini_sex" readonly placeholder="Select time" type="text"/>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Fim do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="fim_sex" readonly placeholder="Select time" type="text"/>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-form-label col-lg-3 col-sm-12">Sábado:</label>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Início do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="ini_sab" readonly placeholder="Select time" type="text"/>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12">
									<label class="col-form-label col-sm-12">Fim do funcionamento:</label>
									<input type='text' class="form-control m_timepicker_2_modal" id="fim_sab" readonly placeholder="Select time" type="text"/>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger m-btn" data-dismiss="modal">Sair</button>
							<button type="button" onclick="geraTimer();" data-dismiss="modal" class="btn btn-warning m-btn">Aplicar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
<!--end::Modal-->