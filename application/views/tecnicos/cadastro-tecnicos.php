<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8" />
    <title>WERTCO - Cadastro de Técnicos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon.png')?>">
    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>
</head>

<body class="double-diagonal blog-page dark">
    <!-- Preloader Starts -->
    <!--<div class="preloader" id="preloader">
        <div class="logopreloader">
            <img src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-black">
        </div>
        <div class="loader" id="loader"></div>
    </div>
    <!-- Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
    	<div id="loading" style=" text-align: center;width: 100%;height: 100%; display: none; background: #cccccc45;z-index: 9;/* top: 0; */position: absolute;">
    		<div style="position: relative;top: 63%;  background: #ccc; padding: 20px; text-align: center;">
	    		<h2 style="color: #000;" id="msg_loading"> Aguarde, estamos enviando seu arquivo... </h2>
	    		<img src="<?php echo base_url('bootstrap/img/loading.gif'); ?>" style="height: 70px;">
    		</div>
    	</div>
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                    <div class="logo">
                        <a class="navbar-brand link-menu nav-external" >
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->                            
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external"  href="<?=base_url('/home/index')?>"><i class="fa fa-arrow-left"></i> Voltar ao Site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		<!-- Banner Starts -->
        <section class="banner">
			<div class="content text-center">
				<div class="text-center top-text">
                    <h1>Cadastro de Técnicos/Mecânicos</h1>
					<hr>
                    <h4>Preencha o formulário abaixo para registrar seu interesse no curso de capacitação da Wertco.</h4>
                </div>
			</div>
		</section>
		<!--    Banner Ends        -->
        <!--    Services Begin     -->
        <section id="clientes" class="services">
            <!-- Container Starts -->
            <div class="container" style="background: #333;border-radius: 10px;">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h2><span style="color: #ffcc00;">Dados</span> da empresa</h2>
                    <h4></h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
					<span class="outer-line"></span>
					<img src="<?=base_url('bootstrap/img/bombaIcoG.png')?>" />
					<span class="outer-line"></span>
				</div>
                <!-- Divider Ends -->
                <!-- Services Starts -->
				<div class="cadastros">
					<form accept-charset="utf-8" id="formulario" action="<?php echo base_url('tecnicos/add/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;">
					
					<div class="row dados">  						
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" 		class="form-control cnpj" 	id="cnpj" maxlength="15" name="cnpj" placeholder="cnpj" required />	
							<input type="hidden"  	id="salvar" 				maxlength="15" name="salvar" value="1"	required />
							<input type="hidden"  	id="empresa_id" 			maxlength="15" name="empresa_id" value=""  />
							<input type="hidden"  	id="cartao_cnpj"  			name="cartao_cnpj"  />
						</div>                    
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control" id="razao_social" maxlength="250" name="razao_social" placeholder="Razão Social" value="" required />	
						</div>                    
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control" id="fantasia" maxlength="250" name="fantasia" placeholder="fantasia" placeholder="Nome Fantasia" value="" />	
						</div>                    
					</div>
					<div class="row dados">  						

						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control telefone" id="telefone" maxlength="250" name="telefone" placeholder="telefone" value=""  />	
						</div>   
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="email" class="form-control" id="email" maxlength="250" name="email" placeholder="E-mail" value="" required />	
						</div>                    
						<div class="col-md-3 col-lg-3 col-sm-12 ">
							<input type="text" class="form-control" id="endereco" maxlength="250" name="endereco" placeholder="Endereço" value="" required />	
						</div> 		
						<div class="col-md-3 col-lg-3 col-sm-12 ">
							<input type="text" class="form-control" id="bairro" maxlength="250" name="bairro" placeholder="Bairro" value="" required />	
						</div> 						
					</div>					
					<div class="row dados">  
						<div class="col-md-3 col-lg-3 col-sm-12 ">
							<input type="text" class="form-control" id="cep" maxlength="250" name="cep" placeholder="CEP" value="" required />	
						</div>						
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" id="cidade" name="cidade" placeholder="Cidade" value="" required />	
						</div>                    
						<div class="col-md-3 col-lg-3 col-sm-12">
							<select  class="form-control"  maxlength="250" name="estado" placeholder="Estado" required id="estado">
                                <option value="">Estado</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espírito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>
                            </select> 
						</div>		
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" id="pais" name="pais" placeholder="País" value="" required />	
						</div>				
					</div>
					<div class="row dados">  						
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" id="regiao_atua" name="regiao_atua" placeholder="Região que Atua" value="" required />	
						</div>
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control" minlength="8"	maxlength="250" id="credenciamento_inmetro" name="credenciamento_inmetro" placeholder="Credenciamento Inmetro" value="" required="required" />	
						</div>
						<div class="col-md-4 col-lg-4 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" id="credenciamento_crea" name="credenciamento_crea" placeholder="Credenciamento Crea" value=""  />	
							<input class="hidden" type="hidden" name="tipo_cadastro_id" value="4" />
						</div>										
					</div>					
					<?php if($fl_interesse != 1){	?>
					<div class="row dados">  					
						<div class="col-md-6 col-lg-6 col-sm-6">	
							<label > <h4 style="color: #ffcc00 !important;">Contrato Social:</h4> </label>
							<input type="file" class="form-control"  maxlength="250" id="contrato_social" name="contrato_social" placeholder="Cópia do Contrato Social" />		
							<input type="hidden" name="arquivo_upload" 	id="arquivo_upload" />	
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6">
							<label > <h4 style="color: #ffcc00 !important;">Formatos Aceitos (Tipos):</h4> </label>
							<h4 class=""> .pdf | .doc | .docx | .jpg | .png |  </h4>							
						</div>
					</div>
					<?php } ?>
					<div class="text-center top-text" style="margin-top: 50px;">
						<h2><span style="color: #ffcc00;">Dados</span> Pessoais</h2>
						<h4></h4>
					</div>
					<div class="divider text-center">
						<span class="outer-line"></span>
						<span class="fa fa-user" aria-hidden="true"></span>
						<span class="outer-line"></span>
					</div>
					<div class="row dados">  						
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" name="pessoal_nome" placeholder="Nome" value="" required />	
							<input type="hidden" name="subtipo_cadastro_id" value="1" />
						</div>  

						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control" id="cpf" maxlength="250" name="pessoal_cpf" placeholder="CPF" value="" required />	
						</div>
						<div class="col-md-2 col-lg-2 col-sm-12">
							<input type="text" class="form-control telefone" id="pessoal_telefone" maxlength="250" name="pessoal_telefone" placeholder="Telefone" value="" />	
						</div>
						<div class="col-md-2 col-lg-2 col-sm-12">
							<input type="text" class="form-control celular" id="pessoal_celular" maxlength="250" name="pessoal_celular" placeholder="Celular" value="" />
						</div>
						<div class="col-md-2 col-lg-2 col-sm-12">
							<input type="email" class="form-control" id="email_pessoal" maxlength="250" name="pessoal_email" placeholder="E-mail para Acesso" value="" required />	
						</div>                     
					</div>					
					<div class="row dados"> 
						<div class="col-md-6 col-lg-6 col-sm-12">
							<input type="password" class="form-control" id="senha" maxlength="250" name="senha" placeholder="Senha" value="" required />	
						</div>                    
						<div class="col-md-6 col-lg-6 col-sm-12">
							<input type="password" class="form-control" id="senha2" maxlength="250" name="senha2" placeholder="Confirme sua senha" value="" required />	
						</div>		
					</div>					
					<?php if($fl_interesse == 1){	?>
					<div class="row dados"> 
						<div class="col-md-12 col-lg-12 col-sm-12">
							<label><i class="fa fa-check" style="color: #ffcc00;"></i>&nbsp;&nbsp;Possuo interesse nos treinamentos da Wertco.</label>
							<input type="hidden" class="form-control" id="fl_interesse" maxlength="250" name="fl_interesse" placeholder="Senha" value="1" required />	
						</div>
					</div>	
					<?php } ?>
					<div class="row dados">						                   
						<div class="col-md-4 col-lg-4 col-sm-12 offset-md-4" style="text-align: center; margin: 0 auto;">
							<button type="submit" class="btn btn-outline-secondary btn-lg"  maxlength="250" name="enviar" id="enviar" placeholder="Enviar" value="" required style="padding: 0.4rem 1rem;font-size: 1rem;"> Enviar </button>
							<button type="reset"  class="btn btn-outline-secondary btn-lg"  maxlength="250" name="limpar" placeholder="Enviar" value="" required style="padding: 0.4rem 1rem;font-size: 1rem;"> Limpar </button>

						</div>		
						
					</div>
					</form>
				</div>
            </div>
                <!-- Services Ends -->
        </section>
        <!-- Service Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center footer-area-restrita" style="border-top: 2px solid #ffcc00 !important;">
            <!-- Container Starts -->
            <div class="container">
               <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/0001-53 </p>
                <p>
                    &copy; Copyright <?php echo date('Y'); ?> Wertco 
                </p>
              
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
    </div>
    <div class="modal fade contrato_social" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content" style="max-height: 750px;overflow: auto;padding: 100px;">      
		<p>
			<input type="hidden" id="usuario_id" value="<?php echo $usuario_id; ?>" />
			<input type="hidden" id="fl_aceite" value="<?php echo $dados->fl_termos; ?>" />
			<span style="text-align: center;"><b>CONTRATO DE PRESTAÇÃO DE SERVIÇOS DE ASSISTÊNCIA TÉCNICA EM GARANTIA</b></span><br/><br/>
			Pelo presente instrumento particular, as partes: <br/>


			-WERTCO INDÚSTRIA, COMÉRCIO E SERVIÇOS DE MANUTENÇÃO DE BOMBAS DE ABASTECIMENTO DE COMBUSTÍVEIS, DESENVOLVIMENTO DE PRODUTOS INDUSTRIAIS, IMPORTAÇÃO E EXPORTAÇÃO LTDA., com sede na Avenida Getúlio Vargas, nº 280, Galpão nº 415, Parque Industrial de Arujá, Arujá, SP, CEP 07400-050, devidamente inscrita no CNPJ/MF sob nº 27.314.980/0001-53, neste ato representada nos termos de seu contrato social, doravante denominada WERTCO;<br/>

			- <?php echo $dados->razao_social; ?>, com sede na Rua <?php echo $dados->endereco; ?>, inscrita no CNPJ sob nº. <?php echo $dados->cnpj; ?>, neste ato representada por <?php echo $dados->nome; ?>, CPF nº. <?php echo $dados->cpf; ?>, RG nº. ______________________, doravante denominada <?php echo $dados->fantasia; ?>;<br/>

			Resolvem, de comum acordo, firmar o presente CONTRATO DE PRESTAÇÃO DE SERVIÇOS, que se regerá pelas cláusulas e disposições abaixo.<br/><br/>

			<b>1. DO OBJETO</b><br/><br/>

			1.1. O objeto do presente contrato é a prestação, pela CONTRATADA, de serviços de Startup e Assistência Técnica em GARANTIA, das bombas abastecedoras de Combustíveis instaladas nos clientes da WERTCO, previamente estabelecidos.<br/>

			1.2. A WERTCO fornecerá à contratada os nomes dos clientes, respectivos endereços, quantidade de bombas e bicos, que serão objeto de Stratup ou Assistência Técnica em Garantia.<br/><br/>

			<b>2. OBRIGAÇÕES DA CONTRATADA</b><br/><br/>

			2.1. Startup<br/>

			2.1.1. Será executado pela CONTRATADA em todas as bombas indicadas pela WERTCO, e consiste em seguir os procedimentos<br/>

			2.2. Da Assistência Técnica em GARANTIA<br/>

			2.2.1. Quando for solicitada Assistência Técnica em GARANTIA, a CONTRATADA deverá providenciar os reparos ou substituições necessárias para restabelecer as perfeitas condições de funcionamento do equipamento. <br/>

			2.2.2. Nesta oportunidade, a CONTRATADA deverá verificar não somente o equipamento especificado, como também todas as demais bombas abastecedoras de combustíveis deste estabelecimento (desde que produzidas pela WERTCO e dentro do prazo de garantia), fazendo aferições, testes de vazão, examinando as correias, anotando os totalizadores na ordem de serviço.<br/>

			2.3. Ordem de Serviço<br/>

			2.3.1. A CONTRATADA deverá relatar na ordem de serviço a data e hora do atendimento da Assistência Técnica em Garantia, bem como anotar o totalizador e a espécie de serviço efetuado. Referida ordem obrigatoriamente será assinada e carimbada pelo cliente que solicitou a manutenção, ou por quem este expressamente autorizar.<br/>

			2.3.2. A CONTRATADA deverá informar a WERTCO sobre quaisquer anormalidades ou irregularidades encontradas durante as visitas de Assistência Técnica em Garantia.<br/>

			2.4. Horário de Atendimento<br/>

			2.4.1. A CONTRATADA prestará os serviços objeto deste Contrato de Segunda à Sexta-feira, das 08h00min às 17h00min, e aos sábados das 08h00min às 12h00min, realizando os serviços necessários dentro do prazo máximo de 24 (vinte e quatro) horas, a partir do recebimento da solicitação pela WERTCO.<br/>

			2.4.2. Obriga-se a CONTRATADA a fornecer à WERTCO todas as informações relativas ao andamento dos serviços objeto deste contrato.<br/>

			2.5.  Serviços Extras<br/>

			2.5.1. Serviços não compreendidos na Assistência Técnica em Garantia - classificados como excluídos da garantia, serão cobrados pela CONTRATADA de acordo com os valores previamente acordado entre  a WERTCO e a CONTRATADA.<br/>


			2.6. Da conduta geral<br/>

			2.6.1. A CONTRATADA prestará os serviços contratados de acordo com a legislação vigente, abstendo-se de praticar quaisquer atos que possam colocar em risco a qualidade dos serviços e idoneidade da WERTCO, sob pena de rescisão do presente contrato e ressarcimento dos danos decorrentes.<br/>

			2.6.2. A CONTRATADA responsabiliza-se pelo fiel cumprimento, por seus empregados e/ou terceiros sob sua responsabilidade, de todas as cláusulas aqui pactuadas, bem como por quaisquer danos ocasionados aos equipamentos e instalações dos clientes. <br/>

			2.6.3. A CONTRATADA deverá fazer os seguros que lhe sejam exigidos por lei, ou que julgar necessário, devendo apresentar as respectivas apólices anuais, juntamente com comprovante de inscrição estadual e municipal, certidões negativas federais, estaduais e municipais. <br/>

			2.6.4. A CONTRATADA assume integral responsabilidade por eventuais multas decorrentes de imperfeições ou atrasos nos serviços ora contratados, excetuando-se os casos de força maior ou caso fortuito. <br/>

			2.6.5. A CONTRATADA responsabiliza-se por todos os documentos, peças e equipamentos a ela entregues pela WERTCO para a consecução dos serviços pactuados, respondendo pelo seu mau uso, perda, extravio ou inutilização, enquanto permanecerem sob sua guarda, salvo comprovado caso fortuito ou força maior. <br/>

			2.6.6. A CONTRATADA responsabiliza-se por todos os tributos incidentes sobre os serviços ora contratados, inclusive ART´s junto às unidades do CREA das áreas de atuação, sob pena de responsabilizar-se por eventuais autuações pela ausência de referida obrigação legal e contratual. <br/>

			2.6.7. É responsabilidade da CONTRATADA o fiel cumprimento de todas as obrigações trabalhistas e previdenciárias referentes a seus sócios-proprietários, empregados e/ou terceiros sob sua responsabilidade, comprometendo-se a mantê-los devidamente registrados conforme estabelece a legislação em vigor. <br/>

			§ único: A CONTRATADA deverá apresentar mensalmente à WERTCO cópia autenticada das guias de recolhimento de todos os encargos trabalhistas e previdenciários de seus funcionários, relativos ao mês anterior. <br/>

			2.6.8. A WERTCO poderá solicitar formalmente a substituição de qualquer funcionário da CONTRATADA para execução dos serviços, desde que justifique as razões da solicitação, devendo a substituição ser efetuada no prazo máximo de 30(trinta) dias. <br/>

			2.6.9. Os veículos e ferramentas utilizados na prestação dos serviços ora pactuados serão de propriedade da CONTRATADA, a qual também é responsável pela manutenção e respectivas despesas com os deslocamentos necessários. <br/>

			2.6.10. Compromete-se a CONTRATADA a cumprir integralmente as disposições do Manual de Serviço disponível, mediante acesso restrito, em www.wertco.com.br. <br/>

			2.6.11. Responsabiliza-se a contratada a utilizar todos os Equipamentos de Proteção Individual exigidos pela Norma específica, durante a execução dos serviços ora contratados. <br/><br/>

			<b>3. OBRIGAÇÕES DA WERTCO</b> <br/><br/>

			3.1. Fornecer as especificações, instruções e documentos que se façam necessários ao bom desempenho dos serviços contratados, em tempo hábil.<br/>

			3.2. Quando houver abertura de chamados, a WERTCO informará à CONTRATADA o local onde o equipamento (bomba) encontra-se instalado, a identificação deste e o defeito apresentado.<br/><br/>

			<b>4. DO PREÇO E FORMA DE PAGAMENTO</b><br/><br/>

			4.1. Pelos serviços ora contratados, a WERTCO pagará à CONTRATADA os valores constantes na política de serviços constante no site da WERTCO, sujeita a alterações que serão disponibilizadas no mesmo site.<br/>

			4.2. O pagamento será realizado em até 10 (dez) dias úteis após a apresentação dos documentos previstos no item 2.6.7, parágrafo único, mediante apresentação da Nota Fiscal, acompanhada das ordens de serviço. <br/>

			4.3. Nenhum pagamento efetuado pela WERTCO limitará os direitos desta de posteriormente discordar dos valores faturados ou de reclamar pelo desempenho insatisfatório de qualquer serviço executado, e o pagamento não será considerado como uma aceitação dos serviços por parte da WERTCO.<br/>

			4.4. Possíveis serviços adicionais, ou ainda, havendo a necessidade de qualquer das hipóteses relacionadas no item 2.5., os valores deverão ser previamente orçados e autorizados pela WERTCO, devendo ser pagos em separado e contra apresentação das respectivas Notas Fiscais.<br/>

			4.5. Os valores constantes do presente contrato poderão ser reajustados anualmente, mediante acordo entre as partes, o qual será formalizado através de aditivo contratual. <br/>

			4.6. O pagamento dos valores ora ajustados, será efetuado mediante depósito bancário na conta corrente indicada pela CONTRATADA, servindo o comprovante de depósito como recibo de quitação, ou através de boleto bancário.<br/><br/>


			<b>5. DO PRAZO DE VIGÊNCIA</b><br/><br/>

			5.1. O prazo de vigência deste acordo é de 12 (doze) meses, a contar da data de sua assinatura, podendo ser renovado automaticamente por igual período, caso não haja manifestação contrária formalizada com antecedência de 30 (trinta) dias da data de seu término. <br/>

			5.2. O presente instrumento poderá ser rescindido, sem ônus, a qualquer tempo, mediante prévio aviso escrito – via correio, fax ou e-mail - de 30 (trinta) dias à outra parte.  <br/>

			5.3. Na hipótese de infração a qualquer das obrigações ora contratadas, a parte inocente deverá notificar a outra para que corrija a infração no prazo de 15 (quinze) dias. Em não ocorrendo, a parte infratora pagará à inocente indenização equivalente ao valor total das perdas e danos resultantes de seu ato.<br/>

			5.4. Permanecerão em vigor as cláusulas deste contrato que, por sua natureza, produzam efeitos após o seu término ou rescisão.<br/>

			<b>6. FISCALIZAÇÃO</b><br/><br/>

			6.1. A WERTCO poderá, a seu exclusivo critério, no exercício do poder/dever de fiscalizar, designar representantes legais, que terão livre acesso a qualquer local onde estejam sendo executados ou já concluídos serviços de manutenção pela CONTRATADA, para fiscalização e verificação quanto ao exato cumprimento das cláusulas aqui pactuadas.<br/><br/>

			<b>7. DO MEIO AMBIENTE</b><br/><br/>

			7.1. As partes contratantes se responsabilizam pelo cumprimento das leis e regulamentos referentes à proteção do meio ambiente.<br/>

			7.2. A CONTRATADA compromete-se a obter e manter válidas todas as licenças, autorizações e estudos exigidos para o pleno desenvolvimento de suas atividades, afastando assim, qualquer risco ou dano ao meio ambiente.<br/><br/>

			<b>8. DO SIGILO DE INFORMAÇÕES</b><br/><br/>

			8.1. São consideradas confidenciais e sigilosas as informações comerciais, técnicas e estratégicas trocadas pelas partes contratantes.<br/>

			8.2. A CONTRATADA compromete-se a utilizar as informações e/ou documentos obtidos da WERTCO exclusivamente para prestação dos serviços ora contratados. <br/>

			8.3. Fica expressamente vedada às partes a utilização dos termos deste contrato, seja em divulgação ou propaganda de qualquer espécie, salvo quando autorizado expressamente pela outra parte.<br/><br/>

			<b>9. NÃO EXCLUSIVIDADE</b><br/><br/>

			9.1. A prestação dos serviços objeto deste contrato não é exclusiva, podendo a CONTRATADA prestar tais serviços a terceiros ou realizar negócios permitidos pelo seu objeto social, desde que, em qualquer caso, não prejudique ou comprometa a qualidade dos serviços e demais obrigações do presente contrato.<br/><br/>

			<b>10. DO VÍNCULO EMPREGATÍCIO</b><br/>

			10.1. O presente acordo não estabelece qualquer vínculo empregatício entre as partes contratantes, incluindo seus sócios-proprietários, funcionários e/ou terceiros contratados.<br/>

			10.2. Os prepostos e funcionários da CONTRATADA deverão obedecer aos regulamentos e normas de segurança do estabelecimento onde serão prestados os serviços contratados.<br/><br/>

			<b>11. SUBCONTRATAÇÃO</b><br/><br/>

			11.1. Este contrato não poderá ser cedido ou transferido, no todo ou em parte, ressalvada a concordância expressa, escrita, de ambas as partes.<br/><br/>

			<b>12. DISPOSIÇÕES GERAIS</b><br/><br/>

			12.1. Não constituirá novação ou renúncia de direitos a abstenção, por qualquer das partes, do exercício de qualquer direito, poder, recurso ou faculdade assegurados por lei ou por este contrato, nem a eventual tolerância quanto a eventuais infrações ou atraso no cumprimento de quaisquer obrigações ora contratadas.<br/>

			12.2. Fica eleito o Foro da Comarca da cidade de Arujá/SP para dirimir quaisquer dúvidas ou questões oriundas do presente contrato.<br/><br/>


			<b>E, por assim estarem justos e contratados, firmam este instrumento digitalmente, através de aceite de seus termos e condições, conforme disposições contidas no site, considerando-se como data de início de vigência, o respectivo aceite.</b><br/><br/>

			<span style="text-align: center;"><?php setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');date_default_timezone_set('America/Sao_Paulo'); echo strftime('%d de %B de %Y', strtotime('today')); ?></span>

			
		</p>
			<div>
				<a href="#" class="btn btn-success m-btn m-btn--icon aceite" id="aceitar" valor="1">
				<span>
					<i class="la la-check"></i>
					<span>
						Aceitar os termos e condições
					</span>
				</span>
			</a>
			<a href="#" class="btn btn-danger m-btn m-btn--icon aceite" id="recusar" valor="0">
				<span>
					<i class="la la-warning"></i>
					<span>
						Recusar
					</span>
				</span>
			</a>
			<a href="#" class="btn btn-metal m-btn m-btn--icon fechar">
				<span>
					<i class="la la-mail-forward"></i>
					<span>
						Ver Depois
					</span>
				</span>
			</a>

			</div>

	    </div>
	  </div>
	</div>
    <!-- Wrapper Ends -->
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>       
	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>		
	<script type="text/javascript" src="<?=base_url('bootstrap/js/sweetalert.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/AjaxFileUpload.js')?>"></script>

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({
		   		title: "OK!",
		   		text: 'Cadastro realizado com sucesso, aguarde um e-mail de confirmação!',
		   		type: 'success'
	    	}).then(function() {
	    	   	window.location = '<?=base_url("home/index") ?>';
    	}); 
	</script>	
<?php unset($_SESSION['sucesso']); }  else { ?>

	<script type="text/javascript">	$('#cnpj').focus();</script>

<?php } ?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#cnpj').mask('00.000.000/0000-00');
			$('#cpf').mask('000.000.000-00');
			$('#cep').mask('00000-000');
			$('#bicos').mask('00000');				
			$('.celular').mask('(00) 00000 - 0000');

			$('#formulario').submit(function(){
					 
				$('#enviar').attr('disabled', 'disabled');
					 
			});
			
			$('#cnpj').bind('focusout', function(){
				var cnpj = $(this).val(); 
				if(cnpj != ''){
					if(isCNPJValid(cnpj)){
						$('#loading').attr('style','text-align: center; width: 100%; height: 100%; background: rgba(204, 204, 204, 0.27); z-index: 9; position: absolute; display: block;');
						$('#msg_loading').text('Validando CNPJ');
						$.ajax({
							method: "POST",
							url: "<?php echo base_url('clientes/verifica_cnpj');?>",
							async: true,
							data: { cnpj 	:	cnpj }
							}).success(function( data ) {
								var dados = $.parseJSON(data);		 

								if(dados.length > 0){
									var texto = 'Por favor, entre em contato com o representante legal da sua empresa para ele realizar seu cadastro no sistema da WERTCO';
									
									if(	$('#fl_interesse').val() == 1	){
										texto = 'Por favor, entre em contato sobre seu interesse em capacitação técnica pelo e-mail marketing@companytec.com.br ou pelo telefone (53) 3284-8140.';
									}
									
									swal({
										title: 'CNPJ já cadastrado no sistema!',
										text: texto,
										type:'warning'
									}).then(function(isConfirm) {
										$('#loading').fadeOut('slow');
										$('#cnpj').val('');								
										$('#razao_social').val('');									
										$('#fantasia').val('');									
										$('#telefone').val('');									
										$('#endereco').val('');										
										$('input[name="posto_rede"]').val('');									
										$('#nr_bombas').val('');												
										$('#bicos').val('');									
										$('input[type="checkbox"]').val('');									
										$('input[name="software_utilizado"]').val('');
										$('input[name="mecanico_atende"]').val('');									
										
									});
									

								}else{
									
									var cnpj_sem_mascara = cnpj.replace(/[^\d]+/g,'');
									
	                                $.ajax({
	                                    method: "POST",
	                                    url: "<?php echo base_url('Clientes/ValidaCnpj'); ?>",
	                                    async: true,
	                                    data: {	cnpj 	: 	cnpj_sem_mascara }
	                                }).success(function( data ) {
	                                    var dados = $.parseJSON(data);   
	                                    
	                                    if (dados.receita.erro == 0 && dados.receita.situacao == "ATIVA" ){
	                                        $('#razao_social').attr('disabled', false);     
	                                        $('#razao_social').val(dados.receita.retorno.razao_social);
	                                        if(dados.receita.retorno.razao_social != ''){    
	                                            $('#razao_social').attr('style','border-color: #5fda17;');
	                                        }
	                                        $('#fantasia').attr('disabled', false);
	                                        $('#fantasia').val(dados.receita.retorno.nome_fantasia );
	                                        if(dados.receita.retorno.nome_fantasia != ''){
	                                            $('#fantasia').attr('style','border-color: #5fda17;');
	                                        }
	                                        $('#telefone').attr('disabled', false);
	                                        $('#telefone').val(dados.receita.retorno.telefone);
	                                        if(dados.receita.retorno.telefone != ''){
	                                            $('#telefone').attr('style','border-color: #5fda17;');
	                                        }
	                                        $('#endereco').attr('disabled', false); 
	                                        $('#endereco').val(dados.receita.retorno.logradouro+', '+dados.receita.retorno.numero+' '+dados.receita.retorno.complemento);                                
	                                        $('#endereco').attr('style','border-color: #5fda17;');
	                                        
	                                        $('#email').attr('disabled', false); 
	                                        $('#email').val('');                                                                                                
	                                        $('#cidade').attr('disabled', false); 
	                                        $('#cidade').val(dados.receita.retorno.municipio_ibge);  
	                                        if(dados.receita.retorno.municipio_ibge != ''){
	                                            $('#cidade').attr('style','border-color: #5fda17;');
	                                        }
	                                        $('#estado').attr('disabled', false); 
	                                        $('#estado').val(dados.receita.retorno.uf);
	                                        if(dados.receita.retorno.uf != ''){
	                                            $('#estado').attr('style','border-color: #5fda17;');
	                                        }
	                                        $('#bairro').attr('disabled', false); 
	                                        $('#bairro').val(dados.receita.retorno.bairro);
	                                        if(dados.receita.retorno.bairro != ''){
	                                            $('#bairro').attr('style','border-color: #5fda17;');
	                                        }    
	                                        $('#cep').attr('disabled', false); 
	                                        $('#cep').val(dados.receita.retorno.cep);                          
	                                        if(dados.receita.retorno.bairro != ''){
	                                            $('#cep').attr('style','border-color: #5fda17;');
	                                        }
	                                        $('#pais').attr('disabled', false);
	                                        $('#pais').attr('style','border-color: #5fda17;');                                                
	                                        $('#inscricao_estadual').attr('disabled', false);
	                                        $('#inscricao_estadual').val('');                        
	                                        
	                                        $('#cartao_cnpj').val(dados.receita.save);
	                                        
	                                        $('#pais').val('Brasil');         
	                                        $('#salvar').val('1');
	                                    }else{
	                                       swal({
	                                            title: "Atenção!",
	                                            text: dados.receita.msg,
	                                            type: 'warning'
	                                        }).then(function() {
	                                            $('#cnpj').val('');
	                                        });

	                                    }
	                                });  
	                                $('#loading').fadeOut('slow');  
								}							
						});	
					} else {
						swal({
							title: 'CNPJ inválido!',
							text: 'Favor verificar o CNPJ informado',
							type:'error'
						}).then(function(isConfirm) {
							$('#cnpj').val('');
							$('#cnpj').focus();
						})
					}
				}
			});
			
			function isCNPJValid(cnpj) {  
			
				var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
				if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
					return false;
				for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
				if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
					return false; 
				for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
				if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
					return false; 
				return true; 
			};

			$('#cpf').bind('focusout', function(){
				var cpf = $(this).val();
				if(cpf != ""){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_cpf');?>",
						async: true,
						data: { cpf 	: 	cpf }
					}).success(function( data ) {
						var dados = $.parseJSON(data);
							if(dados.status == 'erro'){
								swal(
							  		'Ops!',
							  		dados.mensagem,
							  		'error'
								)
								$('#cpf').val('');	
							}
						
					});
				}
			});

			$('#email_pessoal').bind('focusout', function(){
				var email = $(this).val();
				if(email != ""){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_email');?>",
						async: true,
						data: { email 	: 	email }
					}).success(function( data ) {
						var dados = $.parseJSON(data);
							if(dados.status == 'erro'){
								swal(
							  		'Ops!',
							  		dados.mensagem,
							  		'error'
								)
								$('#email_pessoal').val('');	
							}
						
					});
				}
			});

			$('#senha2').bind('focusout', function(){
				var senha 	= 	$('#senha').val();
				var senha2 	= 	$('#senha2').val();
				if( senha != senha2 ){
					swal(
				  		'Ops!',
				  		'Senha não confere, tente novamente',
				  		'error'
					)
					$('#senha').val('');
					$('#senha2').val('');
				}
			});

			$('#contrato_social').on('change', function(){
					
				$('#loading').show();
				$('#msg_loading').text('Aguarde o envio do arquivo...'); 
				$.ajaxFileUpload({
					url 			: 	'<?php echo base_url('Tecnicos/uploadArquivo'); ?>', 
					secureuri		: 	false,
					fileElementId	: 	'contrato_social',
					dataType		:  	'json',						
					success	: function (data)
					{
						var dados = $.parseJSON(data);
						if(  typeof dados.msg.file_name !== 'undefined' ){			
							$('#arquivo_upload').val(dados.msg.file_name);
							$('#msg_loading').text('Aguarde o envio do arquivo...');
							$('#loading').hide();
							swal(
						  		'Ok!',
						  		'Arquivo enviado, continue com o cadastro.',
						  		'success'
							);
						}else{ 

							$('#loading').hide();
							swal(
						  		'Atenção!',
						  		'Não possível enviar o arquivo, verifique o tamanho e o tipo do arquivo e tente novamente.',
						  		'warning'
							);

							$('#contrato_social').val('');
						}
					}
				});
				//return false;
				
			});

		});
	</script>
    <!-- Main JS Initialization File -->
    <!--<script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>-->
    
    
</body>

</html>