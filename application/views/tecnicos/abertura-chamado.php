<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8" />
    <title>WERTCO - Abertura de Chamado</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon.png')?>">
    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>
    <style type="text/css">
    	.dados{
    		/*display: none;*/
    		padding-bottom:  0px !important;
    		margin-top: 0px !important;
    	}
    	.row{
    		padding-bottom:  15px !important;
    	}
    </style>
</head>

<body class="double-diagonal blog-page dark">
    <!-- Preloader Starts -->
    <!--<div class="preloader" id="preloader">
        <div class="logopreloader">
            <img src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-black">
        </div>
        <div class="loader" id="loader"></div>
    </div>
     Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
    	<div id="loading" style=" text-align: center;width: 100%;height: 100%; display: none; background: #cccccc45;z-index: 9;/* top: 0; */position: absolute;">
    		<div style="position: relative;top: 63%;  background: #ccc; padding: 20px; text-align: center;">
	    		<h2 style="color: #000;" id="msg_loading"> Aguarde, estamos enviando seu arquivo... </h2>
	    		<img src="<?php echo base_url('bootstrap/img/loading.gif'); ?>" style="height: 70px;">
    		</div>
    	</div>
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                    <div class="logo">
                        <a class="navbar-brand link-menu nav-external" >
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->                            
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external"  href="<?=base_url('/home/index')?>"><i class="fa fa-arrow-left"></i> Voltar ao Site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		<!-- Banner Starts -->
        <section class="banner">
			<div class="content text-center">
				<div class="text-center top-text">
                    <h1>Abertura de Chamados Wertco</h1>
					<hr>                   
                </div>
			</div>
		</section>
		<!--    Banner Ends        -->
        <!--    Services Begin     -->
        <section id="clientes" class="services">
            <!-- Container Starts -->
            <div class="container" style="background: #333;border-radius: 10px;">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h2><span style="color: #ffcc00;">Dados</span> do Posto</h2>
                    <h4></h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
					<span class="outer-line"></span>
					<img src="<?=base_url('bootstrap/img/bombaIcoG.png')?>" />
					<span class="outer-line"></span>
				</div>
                <!-- Divider Ends -->
                <!-- Services Starts -->
				<div class="cadastros">
					<form accept-charset="utf-8" id="formulario" action="<?php echo base_url('tecnicos/abrirChamado/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;">
					<div class="row ">  						
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" 		class="form-control cnpj" 	id="cnpj" maxlength="15" name="cnpj" placeholder="cnpj" required />								
							<input type="hidden"  	id="empresa_id" 			maxlength="15" name="empresa_id" value=""  />
						</div>						                   
					</div>
					<div class="row dados">
						<div class="col-md-6 col-lg-6 col-sm-12">
							<input type="text" class="form-control" id="razao_social" maxlength="250" name="razao_social" placeholder="Razão Social" value="" disabled="disabled" />	
						</div>                    
						<div class="col-md-6 col-lg-6 col-sm-12 ">
							<input type="text" class="form-control" id="fantasia" maxlength="250" name="fantasia" placeholder="fantasia" placeholder="Nome Fantasia" value="" disabled="disabled" />	
						</div> 
					</div>
					<div class="row dados">  						

						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control telefone" id="telefone" maxlength="250" name="telefone" placeholder="telefone" value="" disabled="disabled" />	
						</div>   
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="email" class="form-control" id="email" maxlength="250" name="email" placeholder="E-mail" value="" disabled="disabled" />	
						</div>                    
						<div class="col-md-3 col-lg-3 col-sm-12 ">
							<input type="text" class="form-control" id="endereco" maxlength="250" name="endereco" placeholder="Endereço" value="" disabled="disabled"  />	
						</div> 		
						<div class="col-md-3 col-lg-3 col-sm-12 ">
							<input type="text" class="form-control" id="bairro" maxlength="250" name="bairro" placeholder="Bairro" value="" disabled="disabled"  />	
						</div> 						
					</div>					
					<div class="row dados">  
						<div class="col-md-3 col-lg-3 col-sm-12 ">
							<input type="text" class="form-control" id="cep" maxlength="250" name="cep" placeholder="CEP" value="" disabled="disabled"  />	
						</div>						
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" id="cidade" name="cidade" placeholder="Cidade" value="" disabled="disabled"  />	
						</div>                    
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" name="estado" placeholder="Estado"  id="estado" disabled="disabled">                               
						</div>		
						<div class="col-md-3 col-lg-3 col-sm-12">
							<input type="text" class="form-control"  maxlength="250" id="pais" name="pais" placeholder="País" value="" disabled="disabled"  />	
						</div>				
					</div>
					<div class="row dados">
						<div class="col-md-6 col-lg-6 col-sm-12">
							<select class="form-control" id="nr_serie" name="nr_serie[]" placeholder="Número de série" multiple  required>	

							</select>	
						
						</div>
						
					</div>				
					<div class="text-center top-text" style="margin-top: 3rem;">
		                    <h2><span style="color: #ffcc00;">Dados</span> do Contato</h2>
		                    <h4></h4>
	                	</div>
	                	<!-- Main Heading Starts -->
		                <!-- Divider Starts -->
		                <div class="divider text-center">
							<span class="outer-line"></span>
							<img src="<?=base_url('bootstrap/img/bombaIcoG.png')?>" />
							<span class="outer-line"></span>
						</div>
					<div class="row ">  						
						<div class="col-md-6 col-lg-6 col-sm-12">
							<input type="text" class="form-control" id="nome" maxlength="250" name="nome" placeholder="Nome Contato" value="" required="required" />														
						</div>                    
						<div class="col-md-6 col-lg-6 col-sm-12">
							<input type="text" class="form-control" id="cpf" maxlength="250" name="cpf" placeholder="cpf" placeholder="CPF" required="required" value="" />	
						</div>						
					</div>
					<div class="row ">
						<div class="col-md-6 col-lg-6 col-sm-12">
							<input type="email" class="form-control" id="email" maxlength="250" name="email" placeholder="E-mail" required="required" value="" />	
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12">
							<input type="text" class="form-control" id="telefone" maxlength="250" name="telefone" placeholder="Telefone" required="required"  value="" />	
						</div>                                        
					</div>
					
					
						<div class="text-center top-text" style="margin-top: 3rem;">
		                    <h2><span style="color: #ffcc00;">Informações</span> do Chamado</h2>
		                    <h4></h4>
	                	</div>
	                	<!-- Main Heading Starts -->
		                <!-- Divider Starts -->
		                <div class="divider text-center">
							<span class="outer-line"></span>
							<img src="<?=base_url('bootstrap/img/bombaIcoG.png')?>" />
							<span class="outer-line"></span>
						</div>
					
					<div class="row ">
						<div class="col-md-6 col-lg-6 col-sm-12">
							<select name="tipo_id" id="tipo_id" class="form-control" required="required">
								<option value=""> Selecione o tipo de chamado </option>
								<option value="1"> Startup </option>
								<option value="2"> Manutenção </option>
								<option value="3"> Informação </option>
							</select>
						</div> 						
						<div class="col-md-6 col-lg-6 col-sm-12">
							<textarea name="descricao" class="form-control" id="descricao" required="required" placeholder="INFORME O MOTIVO DO CHAMADO"></textarea>
						</div>
					</div>						
					<div class="row ">						                   
						<div class="col-md-4 col-lg-4 col-sm-12 offset-md-4" style="text-align: center; margin: 0 auto;">
							<button type="submit" class="btn btn-outline-secondary btn-lg"  maxlength="250" name="enviar" id="enviar" placeholder="Enviar" value="" required style="padding: 0.4rem 1rem;font-size: 1rem;"> Enviar </button>
							<button type="reset"  class="btn btn-outline-secondary btn-lg"  maxlength="250" name="limpar" placeholder="Enviar" value="" required style="padding: 0.4rem 1rem;font-size: 1rem;"> Limpar </button>

						</div>								
					</div>
					</form>
				</div>
            </div>
                <!-- Services Ends -->
        </section>
        <!-- Service Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center footer-area-restrita" style="border-top: 2px solid #ffcc00 !important;">
            <!-- Container Starts -->
            <div class="container">
               <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/0001-53 </p>
                <p>
                    &copy; Copyright <?php echo date('Y'); ?> Wertco 
                </p>
              
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
    </div>  

	   
    <!-- Wrapper Ends -->
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>       
	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>		
	<script type="text/javascript" src="<?=base_url('bootstrap/js/sweetalert.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/AjaxFileUpload.js')?>"></script>

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
	  		'Ops!',
	  		'Aconteceu algum problema, reveja seus dados e tente novamente!',
	  		'error'
		);
	</script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal({ 
		   		title: "OK!",
		   		text: 'Chamado aberto com sucesso, aguarde o retorno de nossa equipe técnica!',
		   		type: 'success'
	    	}).then(function() { 
	    	   	window.location = '<?=base_url("home/index") ?>';
    	}); 
	</script>	
<?php unset($_SESSION['sucesso']); }  else { ?>

	<script type="text/javascript">	$('#cnpj').focus();</script>

<?php } ?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#cnpj').mask('00.000.000/0000-00');
			$('#cpf').mask('000.000.000-00');
			$('#cep').mask('00000-000');
			$('#bicos').mask('00000');				
			$('#telefone').mask('(00)00000-0000');
			
			$('.dados').fadeOut('fast');
			$('#formulario').submit(function(){
					 
				$('#enviar').attr('disabled', 'disabled');
					 
			});
			
			$('#cnpj').bind('focusout', function(){
				var cnpj = $(this).val(); 
				if(cnpj != ''){
					if(isCNPJValid(cnpj)){
						$('#loading').attr('style','text-align: center; width: 100%; height: 100%; background: rgba(204, 204, 204, 0.27); z-index: 9; position: absolute; display: block;');
						$('#msg_loading').text('Validando CNPJ');
						$.ajax({
							method: "POST",
							url: "<?php echo base_url('tecnicos/verifica_cnpj_cliente');?>",
							async: true,
							data: { cnpj 	:	$('#cnpj').val() }
							}).success(function( data ) {
								var dados = $.parseJSON(data);		 

								if(dados.length > 0){
										
										$('#loading').fadeOut('slow');
										$('#empresa_id').val(dados[0].id );																
										$('#razao_social').val(dados[0].razao_social);
										$('#fantasia').val(dados[0].fantasia);									
										$('#telefone').val(dados[0].telefone);									
										$('#email').val(dados[0].email);									
										$('#endereco').val(dados[0].endereco);
										$('#bairro').val(dados[0].bairro);
										$('#cep').val(dados[0].cep);
										$('#cidade').val(dados[0].cidade);
										$('#estado').val(dados[0].estado);
										$('#pais').val(dados[0].pais);

										$('.dados').slideDown('slow');
										html="<option value=''> Selecione as bombas para abrir o chamado </option>";
										for (var i = 0; i < dados.length; i++) {
											modelo = '#'+dados[i].nr_serie+' | '+ dados[i].modelo +' | '+ dados[i].combustivel;
											html+="<option value='"+modelo+"'>"+modelo+"<option>";
										}
										$('#nr_serie').append(html);

								}else{
									$('.dados').slideUp('slow');
									swal({
										title: 'CNPJ já não encontrado em nosso sistema, verifique novamente o CNPJ do posto.',
										text: texto,
										type:'warning'
									}).then(function(isConfirm) {
										$('#loading').fadeOut('slow');
										$('#cnpj').val('');								
										$('#razao_social').val('');									
										$('#fantasia').val('');									
										$('#telefone').val('');									
										$('#endereco').val('');										
										$('input[name="posto_rede"]').val('');									
										$('#nr_bombas').val('');												
										$('#bicos').val('');									
										$('input[type="checkbox"]').val('');									
										$('input[name="software_utilizado"]').val('');
										$('input[name="mecanico_atende"]').val('');									
										
									});

								}
									
														
						});	
					
					} else {
						swal({
							title: 'CNPJ inválido!',
							text: 'Favor verificar o CNPJ informado',
							type:'error'
						}).then(function(isConfirm) {
							$('#cnpj').val('');
							$('#cnpj').focus();
						})
					}
				}
				
			});
			
			function isCNPJValid(cnpj) {  
			
				var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
				if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
					return false;
				for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
				if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
					return false; 
				for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
				if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
					return false; 
				return true; 
			};

			/*$('#cpf').bind('focusout', function(){
				var cpf = $(this).val();
				if(cpf != ""){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_cpf');?>",
						async: true,
						data: { cpf 	: 	cpf }
					}).success(function( data ) {
						var dados = $.parseJSON(data);
							if(dados.status == 'erro'){
								swal(
							  		'Ops!',
							  		dados.mensagem,
							  		'error'
								)
								$('#cpf').val('');	
							}
						
					});
				}
			});*/

			$('#email_pessoal').bind('focusout', function(){
				var email = $(this).val();
				if(email != ""){
					$.ajax({
						method: "POST",
						url: "<?php echo base_url('clientes/verifica_email');?>",
						async: true,
						data: { email 	: 	email }
					}).success(function( data ) {
						var dados = $.parseJSON(data);
							if(dados.status == 'erro'){
								swal(
							  		'Ops!',
							  		dados.mensagem,
							  		'error'
								)
								$('#email_pessoal').val('');	
							}
						
					});
				}
			});

			$('#senha2').bind('focusout', function(){
				var senha 	= 	$('#senha').val();
				var senha2 	= 	$('#senha2').val();
				if( senha != senha2 ){
					swal(
				  		'Ops!',
				  		'Senha não confere, tente novamente',
				  		'error'
					)
					$('#senha').val('');
					$('#senha2').val('');
				}
			});

			$('#contrato_social').on('change', function(){
					
				$('#loading').show();
				$('#msg_loading').text('Aguarde o envio do arquivo...'); 
				$.ajaxFileUpload({
					url 			: 	'<?php echo base_url('Tecnicos/uploadArquivo'); ?>', 
					secureuri		: 	false,
					fileElementId	: 	'contrato_social',
					dataType		:  	'json',						
					success	: function (data)
					{
						var dados = $.parseJSON(data);
						if(  typeof dados.msg.file_name !== 'undefined' ){			
							$('#arquivo_upload').val(dados.msg.file_name);
							$('#msg_loading').text('Aguarde o envio do arquivo...');
							$('#loading').hide();
							swal(
						  		'Ok!',
						  		'Arquivo enviado, continue com o cadastro.',
						  		'success'
							);
						}else{ 

							$('#loading').hide();
							swal(
						  		'Atenção!',
						  		'Não possível enviar o arquivo, verifique o tamanho e o tipo do arquivo e tente novamente.',
						  		'warning'
							);

							$('#contrato_social').val('');
						}
					}
				});
				//return false;
				
			});

		});
	</script>
    <!-- Main JS Initialization File -->
    <!--<script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>-->
    
    
</body>

</html>