<div class="m-content">
	<?php 	if($tipo_acesso == 'administrador geral' ||	$tipo_acesso == 'administrador comercial'){ ?>
	<div class="row">
		<div class="col-xl-4">
			<!--begin:: Widgets/Sales Stats-->
			<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Orçamentos
							</h3>
						</div>						
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl">
									<i class="fa fa-calendar m--font-brand"></i>
								</a>
								<div class="m-dropdown__wrapper" style="width: 150px !important;">
									<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 14.5px;"></span>
									<div class="m-dropdown__inner">
										<div class="m-dropdown__body">
											<div class="m-dropdown__content">
												<ul class="m-nav">
													<li class="m-nav__section m-nav__section--first">
														<span class="m-nav__section-text">
															Gráfico por período
														</span>
													</li>
													<li class="m-nav__item">
																													
														<span class="m-nav__link-text orc_periodo" periodo="30" style="cursor: pointer;">
															30 dias
														</span>
														
													</li>
													<li class="m-nav__item">
																													
														<span class="m-nav__link-text orc_periodo" periodo="60" style="cursor: pointer;">
															60 dias
														</span>
														
													</li>
													<li class="m-nav__item">
														<span class="m-nav__link-text orc_periodo" periodo="120" style="cursor: pointer;">
															120 dias
														</span>
														
													</li>
													<li class="m-nav__item">
														<span class="m-nav__link-text orc_periodo" periodo="180" style="cursor: pointer;">
															180 dias
														</span>
														
													</li>
													<li class="m-nav__separator m-nav__separator--fit"></li>
													<li class="m-nav__item">
														
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>				
				<div class="m-portlet__body">
					<!--begin::Widget 6-->
					<div class="m-widget15">
						<div class="m-widget15__chart" style="height:260px; cursor: pointer;">
							<canvas  id="m_status_total"></canvas>
						</div>
						<div class="m-widget15__items">
							<div class="row">
								<?php $i=1; foreach($total_status['dados'] as $dados){
									switch ($dados['descricao']) {
										case 'aberto':
											$back = "bg-secondary";
											break;
										case 'em negociação':
											$back = "bg-primary";
											break;
										case 'orçamento entregue':
											$back = "bg-info";
											break;			
										case 'fechado':
											$back = "bg-success";
											break;				
										case 'cancelado':
											$back = "bg-warning";
											break;																	
										default:
											$back = "bg-danger";
											break;
									}

									?>
								<div class="col">
									<div class="m-widget15__item">
										<span class="m-widget15__stats">															
											<?php 																
											echo round(($dados['valor']*100)/$total_status['total'],2).'%';
											?>
										</span>
										<span class="m-widget15__text">
											<?php echo ucfirst($dados['descricao']); ?>
										</span>
										<div class="m--space-10"></div>
										<div class="progress m-progress--sm">
											<div  class="progress-bar" role="progressbar" style="width: <?php echo round(($dados['valor']*100)/$total_status['total'],2).'%';?>; background: <?php echo $total_status['cores'][$i-1];?>;" aria-valuenow="<?php echo round(($dados['valor']*100)/$total_status['total'],2).'%';?>" aria-valuemin="0" aria-valuemax="100">
												
											</div>
										</div>
									</div>
								</div>
								<?php if( ($i%2) == 0 ){ ?>
								</div>
								<div class="row">

								<?php } $i++; } ?>													
								
							</div>
						</div>
						<div class="m-widget15__desc">
							* Total de orçamentos: <b><?php echo $total_status['total']; ?></b>
						</div>
					</div>
					<!--end::Widget 6-->
				</div>
			</div>
			<!--end:: Widgets/Sales Stats-->
		</div>
		<div class="col-xl-4">
			<!--begin:: Widgets/Inbound Bandwidth-->
			<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit " style="min-height: 300px; background: #abcdb038;">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Total Orçamentos Fechados - <?php echo date('Y');?>
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="Total de orçamentos fechados no ano de 2018. Os orçamentos foram filtrados pela data de fechamento." >
									<i class="fa fa-exclamation-circle m--font-brand"></i>
								</a>
							</li>
						</ul>
					</div>											
				</div>
				<div class="m-portlet__body">
					<!--begin::Widget5-->
					<div class="m-widget20">
						<div class="m-widget20__number m--font-success">
						Total: R$ <?php echo number_format($total_valor_mes['total'], 2, ',', '.');?>
						</div>
						<div class="m-widget20__chart" style="height:160px;">
							<canvas id="m_valor_mes"></canvas>
						</div>
					</div>
					<!--end::Widget 5-->
				</div>
			</div>
			<!--end:: Widgets/Inbound Bandwidth-->
			<div class="m--space-30"></div>
			<!--begin:: Widgets/Outbound Bandwidth-->
			<div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit bg-warning" style="min-height: 300px">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Orçamentos em aberto (aberto, entregue, em negociação)
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item m-portlet__nav-item--last m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
								<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl" data-toggle="m-tooltip" data-placement="top" title="Total de orçamentos que possuem status iguais a aberto, entregue ao cliente e em negociação e diferente de fechado." >
									<i class="fa fa-exclamation-circle m--font-brand"></i>
								</a>
							</li>
						</ul>
					</div>											
				</div>
				<div class="m-portlet__body">
					<!--begin::Widget5-->
					<div class="m-widget20" style="top: 95px;">
						<div class="m-widget20__number m--font-secondary">
							Total: <?php echo $total_aberto['qtd']; ?>
						</div>
						<div class="m-widget20__number m--font-secondary">
							Valor: R$ <?php echo number_format($total_aberto['total'],2,',','.'); ?>	
						</div>
						<div class="m-widget20__chart" style="height:160px;">
							
						</div>											
					</div>
					<!--end::Widget 5-->
				</div>
			</div>
			<!--end:: Widgets/Outbound Bandwidth-->
		</div>
		<div class="col-xl-4">
			<!--begin:: Widgets/Top Products-->
			<div class="m-portlet m-portlet--full-height m-portlet--fit ">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Produtos Mais Orçados - Top 5
							</h3>
						</div>
					</div>										
				</div>
				<div class="m-portlet__body">
					<!--begin::Widget5-->
					<div class="m-widget4 m-widget4--chart-bottom" style="min-height: 480px">
						<?php foreach($produtos_orcados['dados'] as $produtos_orcado){ ?>
						<div class="m-widget4__item">												
							<div class="m-widget4__info">
								<span class="m-widget4__title">
								<?php echo $produtos_orcado['codigo'].' - '$produtos_orcado['descricao'];?>	
								</span>
								<br>
								<span class="m-widget4__sub">
								
								</span>
							</div>
							<span class="m-widget4__ext">
								<span class="m-widget4__number m--font-brand">
								<?php echo $produtos_orcado['total'];?>		
								</span>
							</span>
						</div>
						<?php } ?>
						<div class="m-widget4__chart m-portlet-fit--sides m--margin-top-20" style="height:260px;">
							<canvas id="top_produtos_orcados"></canvas>
						</div>
					</div>
					<!--end::Widget 5-->
				</div>
			</div>
			<!--end:: Widgets/Top Products-->
		</div>
		<div class="col-xl-4">			
			<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Orçamentos com maior inatividade - Top 10
							</h3>
						</div>
					</div>										
				</div>
				<div class="m-portlet__body">
				<?php foreach( $orcamento_inativo as $inativo ){?>
					<div class="m-widget4">
						<div class="m-widget4__item">							
							<div class="m-widget4__info">
								<span class="m-widget4__title" >
									Orçamento # 
									<a href="AreaAdministrador/visualizarOrcamento/<?php echo $inativo['orcamento_id'];?>">
										 <?php echo $inativo['orcamento_id'];?>
									</a>
								</span>
								<br>	
								<span class="m-widget4__sub">
									Cliente: <?php echo $inativo['cliente']; ?>
								</span>							
							</div>
							<span class="m-widget4__ext">
								<span class="m-widget4__number m--font-brand">
									<?php echo $inativo['dias_ultima_atu'];?> dias
								</span>
							</span>
						</div>
					</div>
				<?php } ?>	
				</div>
			</div>
			<!--end:: Widgets/Authors Profit-->
		</div>
	</div>						<!--End::Section-->
</div>
<!-- Modal Responsavel -->
<div class="modal fade" id="modal_orcamentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3>Orçamentos <small class="text-muted"></small></h3>
				<a href="#" id="pdf_orcamento" class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="position: absolute;right: 80px; display: none;">
					<i class="la la-print"></i>
				</a>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" style="width: 100%;">					
				<table id="example" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
			                <th style="text-align: center;">ID</th>
			                <th>Criado</th>
			                <th>Valor</th>
			            </tr>
			        </thead>
			        <tfoot>
			        	
			        </tfoot>			        					    
				</table>
			</div>
			<div class="modal-footer">					
			</div>
		</div>
	</div>
</div>	
<!-- Modal Gráficos por período -->
<div class="modal fade" id="modal_orcamentos_periodo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content" style="height: 650px;width: 100%;">
			<div class="modal-header">
				<h3>Orçamentos <small class="text-muted"></small>
							</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" style="width: 100%;">					
				<canvas id="orcamentos_periodo" style="cursor: pointer;"></canvas>

			</div>
			<input type="hidden" name="periodo_status" id="periodo_status">
			<div class="modal-footer">					
			</div>
		</div>
	</div>
</div>	
<?php //var_dump($total_status['valor']);die;?>
<script type="text/javascript" >
	//geraGraficos(['teste','teste'],[1,2]);

	$('#pdf_orcamento').bind('click', function(){
				
		var acao 	=	$(this).attr('acao');		
		if(acao == 'fechadosMes'){
			var mes 	= 	$(this).attr('mes');
			window.open(base_url+"AreaAdministrador/geraPdfOrcamentosFechados/"+mes);

		}else if(acao == 'status'){
			var status = $(this).attr('status')
			window.open(base_url+"AreaAdministrador/geraPdfOrcamentosStatus/"+status);
			
		}
		
	});

	var config = {
            type: 'horizontalBar',
            data: {
                labels: <?php echo $total_status['descricao'];?>,
                datasets: [{
                    label: "Status Orçamentos",
                    borderWidth: 2,                         
                    backgroundColor: <?php echo $total_status['cor'];?>,                                       
                    data: <?php echo $total_status['valor']; ?>
                }]
            },
            options: {
                title: {
                    display: false,
                },
                tooltips: {
                    intersect: true,
                    mode: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 0
                },
                legend: {
                    display: false
                    
                },
                responsive: true,
                maintainAspectRatio: false,
                hover: {
                    mode: 'index'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Qtd'
                        },
                        ticks: {
                    		beginAtZero:true,
                    		stepSize: 1
                		}
                    }],
                    yAxes: [{
                        display: false,
                        gridLines: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Status'
                        }
                    }]
                },

               
                 layout: {
			        padding: {
			            left: 0,
			            right: 0,
			            top: 0,
			            bottom: 0
			        }
			    }
            }
        };

    var chart = new Chart($('#m_status_total'), config);
    /* segundo gráfico */
    var config1 = {
            type: 'line',
            data: {
                labels: <?php echo $total_valor_mes['mes'];?>,
                datasets: [{                    
                    backgroundColor: 'rgba(11,123,23,0.3)',
                    borderColor: mUtil.getColor('success'),
                    labels: <?php echo $total_valor_mes['mes'];?>,
                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointHoverBackgroundColor: mUtil.getColor('danger'),
                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
                    data: <?php echo $total_valor_mes['valor']; ?>
                }]
            },
            options: {
                title: {
                    display: false,
                },
                tooltips: {
                   intersect: false,
                    mode: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 0,
                    callbacks: {
	                    label: function(tooltipItems, data) { 
	                        return numberToReal(tooltipItems.yLabel);
	                    }
	                }
                                    
                },
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
                 hover: {
                    mode: 'index'
                },
                scales: {
                    xAxes: [{
                        display: false,
                        gridLines: false,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mês'
                        }
                    }],
                    yAxes: [{
                        display: false,
                        gridLines: false,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        },
                        ticks: {
		                    // Include a dollar sign in the ticks
		                    callback: function(value, index, values) {
		                        return '$' + value;
                    		}
                		}
                    }]
                },
                elements: {
                    line: {
                        tension: 0.000001
                    },
                    point: {
                        radius: 6,
                        borderWidth: 12
                    }
                },
                layout: {
                    padding: {
                        left: 5,
                        right: 5,
                        top: 10,
                        bottom: 0
                    }
                }
            }
        };

    var chart1 = new Chart($('#m_valor_mes'), config1);
	/*	Gráfico sobre dos produtos mais Orçados 	*/
	/* segundo gráfico */
    var config2 = {
            type: 'line',
            data: {
                labels: <?php echo $produtos_orcados['descricao'];?>,
                datasets: [{
                    backgroundColor: 'rgba(54, 163, 247, 0.76)',
                    borderColor: 'rgb(89, 88, 105,1)',

                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointHoverBackgroundColor: mUtil.getColor('danger'),
                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
                    data: <?php echo $produtos_orcados['valor']; ?>
                }]
            },
            options: {
                title: {
                    display: false,
                },
                tooltips: {
                   intersect: false,
                    mode: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 0                                    
                },
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
                 hover: {
                    mode: 'index'
                },
                scales: {
                    xAxes: [{
                        display: false,
                        gridLines: false,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mês'
                        }
                    }],
                    yAxes: [{
                        display: false,
                        gridLines: false,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        },
                        ticks: {
		                    // Include a dollar sign in the ticks
		                    callback: function(value, index, values) {
		                        return '$' + value;
                    		}
                		}
                    }]
                },
                elements: {
                    line: {
                        tension: 0.000001
                    },
                    point: {
                        radius: 6,
                        borderWidth: 12
                    }
                },
                layout: {
                    padding: {
                        left: 5,
                        right: 5,
                        top: 10,
                        bottom: 0
                    }
                }
            }
        };

    var chart2 = new Chart($('#top_produtos_orcados'), config2);
	var table = $('#example').DataTable({"scrollX": true,
				"language": {
				"sEmptyTable": "Nenhum registro encontrado",
			    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
			    "sInfoPostFix": "",
			    "sInfoThousands": ".",
			    "sLengthMenu": "_MENU_    &nbsp;&nbsp;&nbsp;&nbsp;Resultados por página",
			    "sLoadingRecords": "Carregando...",
			    "sProcessing": "Processando...",
			    "sZeroRecords": "Nenhum registro encontrado",
			    "sSearch": "Pesquisar",
			    "oPaginate": {
			        "sNext": '<i class="la la-angle-double-right"></i>',
			        "sPrevious": '<i class="la la-angle-double-left"></i>',
			        "sFirst": "Primeiro",
			        "sLast": "Último"
			    },
			    "oAria": {
			        "sSortAscending": ": Ordenar colunas de forma ascendente",
			        "sSortDescending": ": Ordenar colunas de forma descendente"
				}
				}
			});
    $('#m_status_total').bind('click', function(evt){

	   var activePoint = chart.getElementAtEvent(evt)[0];
	   var data = activePoint._chart.data;
	   var status = activePoint._view.label;
	   $('.text-muted').text(status).removeClass('m--font-success');
	   $.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/buscaOrcamentosPorStatus", 
			async: true,
			data: { status 	: 	status}
		}).done(function(data) {
			
			var dados = $.parseJSON(data);					
			var geral 	= [];
			var temp 	= [];
			table.clear();
			for (var i = 0; i< dados.length; i++) {
				var temp 	= [];
				id = '<a href="'+base_url+'AreaAdministrador/visualizarOrcamento/'+dados[i].id+'">'+dados[i].id+'</a>';
				valor = numberToReal(parseFloat(dados[i].valor_total));
				temp.push(id);
				temp.push(dados[i].emissao);				
				temp.push(valor);
				table.row.add(temp);
			}
			$('#pdf_orcamento').attr('status', status);
			$('#pdf_orcamento').attr('acao', 'status');
			$('#pdf_orcamento').show();

			table.draw();
			$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
			$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
			$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
			$('.dataTables_filter').css('float','right');
			$('.dataTables_scrollHead').removeAttr('style'); 
			$('.dataTables_scrollHead').attr('style','overflow: hidden; position: relative; border: 0px; width: 700px !important;');
			$('table thead').css('background-color', '#f4f3f8');
			$('.no-footer').removeAttr('style');
			$('.no-footer').attr('style','margin-left: 0px; width: 700px;');
			//$('table thead').css('height', '150px');
			$('table tbody tr:odd').addClass('zebraUm');
			$('table tbody tr:even').addClass('zebraDois');
			$('table tr').css('text-align','center');

		});	

	   $("#modal_orcamentos").modal({
	   	 show: true
		});
	});

	$('#m_valor_mes').bind('click', function(evt){

	   var activePoint = chart1.getElementAtEvent(evt)[0];	   
	   var mes = activePoint._xScale.ticks[activePoint._index]; 	   
	   console.log(mes);
	   $('.text-muted').text('Fechados em '+mes);
	   $('.text-muted').addClass('m--font-success');

	   $.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/buscaOrcamentosFechadosMes/", 
			async: true,
			data: { mes : mes}
		}).done(function(data) {
			
			var dados = $.parseJSON(data);					
			var geral 	= [];
			var temp 	= [];
			
			table.clear();
			for (var i = 0; i< dados.length; i++) {
				var temp 	= [];
				id = '<a href="'+base_url+'AreaAdministrador/visualizarOrcamento/'+dados[i].id+'">'+dados[i].id+'</a>';
				var valor = numberToReal(parseFloat(dados[i].valor_total));
				temp.push(id);
				temp.push(dados[i].dthr_andamento);				
				temp.push(valor);				
				table.row.add(temp);
			}

			$('#pdf_orcamento').attr('mes',mes);
			$('#pdf_orcamento').show();
			$('#pdf_orcamento').attr('acao', 'fechadosMes');
			table.draw();			
			$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
			$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
			$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
			$('.dataTables_filter').css('float','right');
			$('.dataTables_scrollHead').removeAttr('style'); 
			$('.dataTables_scrollHead').attr('style','overflow: hidden; position: relative; border: 0px; width: 700px !important;');
			$('table thead').css('background-color', '#f4f3f8');
			$('.no-footer').removeAttr('style');
			$('.no-footer').attr('style','margin-left: 0px; width: 700px;');
			//$('table thead').css('height', '150px');
			$('table tbody tr:odd').addClass('zebraUm');
			$('table tbody tr:even').addClass('zebraDois');
			$('table tr').css('text-align','center');

		});	

	   $("#modal_orcamentos").modal({
	   	 show: true
		});
	});

    $('.orc_periodo').bind('click', function(){
    	var periodo = $(this).attr('periodo');
    	$('.text-muted').text('últimos '+periodo+' dias');
    	$('#periodo_status').val(periodo);

    	$.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/buscaOrcamentosPorPeriodo", 
			async: true,
			data: { periodo 	: 	periodo	}
		}).done(function(datas) {
			
			var dados = $.parseJSON(datas);		
			console.log(dados);
			var descricao = [];		
			var total = [];		
			for (var i = 0; i< dados.length; i++) {				
				descricao.push("'"+dados[i].descricao+"'");				
				total.push(dados[i].valor);
			}
			
			geraGraficoPeriodo(descricao,total)

		});
		
		$("#modal_orcamentos_periodo").modal({
	   		show: true
		});
    });

    function numberToReal(numero) {
	    var numero = numero.toFixed(2).split('.');
	    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
	    return numero.join(',');
	}

	function geraGraficoPeriodo(descricao,total){

			var config3 = {
            type: 'horizontalBar',
            data: {
                labels: descricao,
                datasets: [{
                    label: "Status Orçamentos",
                    borderWidth: 2,                         
                    backgroundColor: ['rgb(234, 234, 234,0.5)','rgb(54, 163, 247,0.5)',
                    'rgb(88, 103, 221,0.5)','rgb(52, 191, 163,0.5)','rgb(255, 204, 0,0.5)','rgb(244, 81, 108,0.5)'],                                       
                    data: total
                }]
            },
            options: {
                title: {
                    display: false,
                },
                tooltips: {
                    intersect: true,
                    mode: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 0
                },
                legend: {
                    display: false
                    
                },
                responsive: true,
                maintainAspectRatio: false,               
                hover: {
                    mode: 'index'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Qtd'
                        },
                        ticks: {
                    		beginAtZero:true,
                    		stepSize: 1
                		}
                    }],
                    yAxes: [{
                        display: true,
                        gridLines: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Status'
                        },
                        ticks: {
                        	padding: 10
                        }
                    }]
                },

               
                 layout: {
			        padding: {
			            left: 5,
			            right: 5,
			            top: 0,
			            bottom: 10
			        }
			    }
            }
        	};

    	var chart5	=	new Chart($('#orcamentos_periodo'), config3);

    	$('#orcamentos_periodo').bind('click', function(evt){
		   var activePoint 	=	chart5.getElementAtEvent(evt)[0];
		   var data 		=	activePoint._chart.data;
		   var status 		=	activePoint._view.label;
		   var periodo 		= 	$('#periodo_status').val();
		   $('.text-muted').text('Status: '+status+' | Período: '+periodo+' dias').removeClass('m--font-success');

		   $.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/buscaOrcamentosPorStatusPeriodo", 
				async: true,
				data: { status 	: 	status,
						periodo : 	periodo	 }

			}).done(function(data) {
				
				var dados = $.parseJSON(data);					
				var geral 	= [];
				var temp 	= [];
				table.clear();
				for (var i = 0; i< dados.length; i++) {
					var temp 	= [];
					id = '<a href="'+base_url+'AreaAdministrador/visualizarOrcamento/'+dados[i].id+'">'+dados[i].id+'</a>';
					valor = numberToReal(parseFloat(dados[i].valor_total));
					temp.push(id);
					temp.push(dados[i].emissao);				
					temp.push(valor);
					table.row.add(temp);
				}

				table.draw();

				$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
				$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
				$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
				$('.dataTables_filter').css('float','right');
				$('.dataTables_scrollHead').removeAttr('style');
				$('.dataTables_scrollHead').attr('style','overflow: hidden; position: relative; border: 0px; width: 700px !important;');
				$('table thead').css('background-color', '#f4f3f8');
				$('.no-footer').removeAttr('style');
				$('.no-footer').attr('style','margin-left: 0px; width: 700px;');
				$('table tbody tr:odd').addClass('zebraUm');
				$('table tbody tr:even').addClass('zebraDois');
				$('table tr').css('text-align','center');

			});	
			$("#modal_orcamentos_periodo").modal('hide');
		   	$("#modal_orcamentos").modal({
		   		show: true
			});
		});

		}
	
</script>
<?php } ?>