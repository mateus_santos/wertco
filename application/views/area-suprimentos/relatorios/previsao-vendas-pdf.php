<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 14px;
 	color: #333;
 	width: 100%;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 16px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{	
	border-bottom: 1px solid #f0f0f0;	
	font-size: 14px;	
	padding: 5px;
}


</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 10px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br/>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br/>
										07400-230 - Arujá - SP<br/>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br/>
			</p>

		</div>
		<div class="corpo">	
			<h3 style="text-align: center;margin-top: 15px;	">Relatório de Previsão de Vendas</h3>
			<h5 style="text-align: center;margin-top: 15px;	">Produtos em orçamentos fechados e pedidos abertos</h5>
			<hr  />
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;">
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Modelo</b></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;text-align: right;"><b>Nr. Bombas</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;text-align: right;"><b>Nr. Bicos</b></td>					
				</tr>
				<?php $total = $total_bombas = $total_bicos = 0; foreach($dados['orcamento'] as $dado){ ?>
				<tr>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo $dado['modelo']?> </td>					
					<td style="border: 1px solid #ccc;padding: 10px;text-align: right;"><?php echo $dado['total_bombas'];?> </td>
					<td style="border: 1px solid #ccc;padding: 10px;text-align: right;"><?php echo $dado['total_bicos'];?> </td>					
				</tr>
				<?php 	
						
						$total_bombas 	= 	$dado['total_bombas'] + $total_bombas;
						$total_bicos 	= 	$dado['total_bicos'] + $total_bicos;
					} 
				?>	
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" ><b>TOTAL</b></td>
					<td style="border: 1px solid #ccc;text-align: right;" ><b><?php echo $total_bombas;?></b></td>
					<td style="border: 1px solid #ccc;text-align: right;" ><b><?php echo $total_bicos;?></b></td>
				</tr>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan="3">&nbsp;</td>
				</tr>				
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan="3"><b>Total de bombas Vendidas nos últimos 3 meses: <?php echo $media['qtd_total'];?></b> </td>
				</tr>
				<tr>
					<td style="border: 1px solid #ccc;text-align: right;" colspan="3"><b>Média dos últimos 3 meses: <?php echo $media['media'];?></b> </td>
				</tr>
			</table>			
		</div>
	</div>
</body>
</html>