<style type="text/css">
	#html_table tbody > tr > td{
		text-align: center;
	}
	#html_table_processing{
		position: absolute;
		top: 0;
		font-size: 21px;
		color: #000;
		font-weight: 400;
		text-align: center;
		width: 98%;
		background: #ffcc0075;
		height: 100%;
		padding-top: 18%;
	}
	
</style>
<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Orçamentos
					</h3>
				</div>				
			</div>
			<div class="m-portlet__head-caption cadastrar_orcamento">													
				<a  class="novo_orcamento" style="color: #ffcc00; font-weight: bold;" >
					<i class="la la-plus-circle" style="font-size: 38px;"></i>
				</a>
			</div>
			<div class="m-list__content">
                <div class="m-list-badge" style="margin-top: 25px;">
                	<span class="m-list-badge__item">Aguardando aprovação&nbsp;&nbsp;<span class="m-badge" style="background: #ff9800;"></span>					                  
					<span class="m-list-badge__item">&nbsp;Aberto&nbsp;&nbsp;<span class="m-badge m-badge--secondary"></span></span>				    						
					<span class="m-list-badge__item">&nbsp;Em negociação&nbsp;&nbsp;<span class="m-badge m-badge--primary"></span></span>
					<span class="m-list-badge__item">&nbsp;Fechado&nbsp;&nbsp;<span class="m-badge m-badge--success"></span></span>				    		
					<span class="m-list-badge__item">&nbsp;Cancelado&nbsp;&nbsp;<span class="m-badge m-badge--warning"></span></span>				    	
				    <span class="m-list-badge__item">&nbsp;Perdido&nbsp;&nbsp;<span class="m-badge m-badge--danger"></span></span>		
				    <input type="hidden" id="zona_atuacao" value="<?=$zona_atuacao?>">			
				</div>
			</div>		
			<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: -25px;" data-toggle="m-tooltip" data-placement="top" title="Tour Virtual" id="tour_virtual">
				<i class="la la-info"></i>
			</a> 	
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">			
			<div class="form-group m-form__group row">
				<div class="form-group col-md-4 col-lg-4">
					<select class="form-control filtro"  tipo="representante" id="representante">
						<option value="">Filtro por Representantes</option>
					<?php 	foreach($representantes as $representante){	?>
						<option value="<?php echo $representante['id']; ?>" >
							<?php echo mb_strtoupper($representante['nome']).' - '.mb_strtoupper($representante['empresa']); ?>
						</option>
					<?php } ?>
					</select>
				</div>				
				<div class="form-group col-md-4 col-lg-4">
					<select class="form-control filtro"  tipo="indicadores" id="indicadores">
						<option value="">Filtro por Indicadores</option>
					<?php 	foreach($indicadores as $indicador){	?>
						<option value="<?php echo $indicador['id']; ?>" >
							<?php echo mb_strtoupper($indicador['nome']).' - '.mb_strtoupper($indicador['razao_social']); ?>
						</option>
					<?php } ?>
					</select>
				</div>			
				<div class="form-group col-lg-4 col-md-4">
					<select class="form-control filtro"  tipo="estado" id="estado">
						<option value="">Filtro por Estado</option>	
						<option value="AC">Acre</option>
                        <option value="AL">Alagoas</option>
                        <option value="AP">Amapá</option>
                        <option value="AM">Amazonas</option>
                        <option value="BA">Bahia</option>
                        <option value="CE">Ceará</option>
                        <option value="DF">Distrito Federal</option>
                        <option value="ES">Espírito Santo</option>
                        <option value="GO">Goiás</option>
                        <option value="MA">Maranhão</option>
                        <option value="MT">Mato Grosso</option>
                        <option value="MS">Mato Grosso do Sul</option>
                        <option value="MG">Minas Gerais</option>
                        <option value="PA">Pará</option>
                        <option value="PB">Paraíba</option>
                        <option value="PR">Paraná</option>
                        <option value="PE">Pernambuco</option>
                        <option value="PI">Piauí</option>
                        <option value="RJ">Rio de Janeiro</option>
                        <option value="RN">Rio Grande do Norte</option>
                        <option value="RS">Rio Grande do Sul</option>
                        <option value="RO">Rondônia</option>
                        <option value="RR">Roraima</option>
                        <option value="SC">Santa Catarina</option>
                        <option value="SP">São Paulo</option>
                        <option value="SE">Sergipe</option>
                        <option value="TO">Tocantins</option>					
					</select>
				</div>			
			</div>
			<div class="form-group m-form__group row">
				<div class="input-group col-md-6 col-lg-6" >
					<select class="form-control status"  tipo="status" id="status" multiple>						
				<?php foreach( $status as $sts ){ 	?>
						<option value="<?php echo $sts['id'];?>"><?php echo $sts['descricao'];?></option>
				<?php } ?>
					</select>
					<i class="la la-search" id="pesquiar_status" style="float: right;margin-top: 11px;margin-left: 11px;"></i>
				</div>	
				<div class="input-group col-md-6 col-lg-6" >
					<input type="text" class="form-control m-input data" name="periodo_ini" id="periodo_ini" style="height: 40px;" placeholder="PERÍODO INICIAL" />
					<div class="input-group-append" style="height: 40px;">
						<span class="input-group-text" >
							<i class="la la-calendar" ></i>
						</span>
					</div>
					<input type="text" class="form-control data" name="periodo_fim" style="height: 40px;" placeholder="PERÍODO FINAL" id="periodo_fim">
					<i class="la la-search" id="pesquiar_periodo" style="float: right;/* width: 15%; */margin-top: 11px;margin-left: 11px;"></i>
				</div>	
				<hr style="border: 1px solid #ffcc00;width: 97%;margin-top: 20px;margin-bottom: 30px;" />
			</div>
			<input type="hidden" name="pesquisa_ativa" id="pesquisa_ativa" value="<?php echo urldecode($pesquisa);?>" />

			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;">Número</th>
						<th style="text-align: center;">Solicitante</th>
						<th style="text-align: center;">Emissão</th>											
						<th style="text-align: center;">Status</th>
						<th style="text-align: center;">Andamento</th>
						<th style="text-align: center;">Responsável</th>
						<th style="text-align: center;">Estado</th>
						<th style="text-align: center;" class="origem">Origem</th>
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				
				</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group status_orcamento_" >
						<label for="exampleSelect1">Situação do Orçamento</label>
						<select class="form-control m-input m-input--air" id="status_orcamento">
							<option value="">Selecione o status do orçamento</option>						
							<option value="1">Aberto</option>							
							<option value="6">Em negociação</option>
							<option value="2">Fechado</option>							
							<option value="10">Perdido para Wayne</option>
							<option value="11">Perdido para Gilbarco</option>
							<option value="3">Perdido Outros</option>
							<option value="4">Cancelado</option>
							<option value="14">Sem Retorno</option>
							<option value="15">Sem Técnicos na Região</option>
						</select>
					</div>
					<div class="form-group m-form__group dt_previsao" style="display: none;">
						<label for="exampleSelect1">Previsão de Fechamento</label>
						<input type="text" class="form-control datepicker" name="dt_previsao" id="dt_previsao" />
					</div>
					<div class="form-group m-form__group aprova_indicacao" style="display: none;" >
						<label for="exampleSelect1">Aprovar esta Indicação?</label>
						<div class="m-radio-inline">
							<label class="m-radio">
								<input type="radio" name="aprovar_indicacao" class="aprova_inidicacao" value="1" checked="checked" />
								Sim
								<span></span>
							</label>
							<label class="m-radio">
								<input type="radio" name="aprovar_indicacao" class="aprova_inidicacao" value="0" />
								Não
								<span></span>
							</label>							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" class="btn btn-primary" orcamento_id="" indicacao="0">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>
		
	<!-- Modal Andamento -->
	<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">		
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">	
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="m-list-timeline andamento_list">
						<div class="m-list-timeline__items">                
						</div>
					</div> 
					<hr/>
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" style="margin-bottom: 2rem;">
							<label for="exampleSelect1">Adicionar Andamento da venda</label>
							<textarea type="text" class="form-control m-input m-input--air" id="andamento_texto"></textarea><br/>
							<span style="float: right;">Receber alerta&nbsp;<input type="checkbox" class="m-input m-input--air receber_alerta"></span>
						</div>
						<hr class="esconde_alerta" style="display: none;margin-top: 10px;"/>
						<div class="form-group m-form__group row esconde_alerta" style="display: none;float: right;">
							<label for="example-text-input" class="col-form-label">A partir da data: &nbsp;&nbsp;</label>
							<input type="text" class="data dthr_alerta form-control m-input m-input--air" name="dthr_alerta" value="<?php echo date('d/m/Y');?>" style="width: 200px;" 	/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="adicionar_andamento" class="btn btn-primary" status_orcamento="" orcamento_id="">Adicionar Andamento</button>
				</div>
			</div>
		</div>
	</div>	

	<!-- Modal Responsavel -->
	<div class="modal fade" id="m_modal_8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-responsavel-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="form-group m-form__group">
						<label>Alterar Responsável</label>
						<div class="m-input-icon m-input-icon--left">
							<input type="text" class="form-control m-input--air" id="responsavel" placeholder="Pesquisar Representante" />
							<input type="hidden" class="form-control m-input--air" id="responsavel_id" placeholder="Pesquisar Representante" />
							<span class="m-input-icon__icon m-input-icon__icon--left">
								<span>
									<i class="la la-search"></i>
								</span>
							</span>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_responsavel" class="btn btn-primary" responsavel_id="" orcamento_id="" status_orcamento_id="">Alterar Responsável</button>
				</div>
			</div>
		</div>
	</div>	

	<!-- Modal Excluir -->
	<div class="modal fade" id="m_excluir_orcamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form accept-charset="utf-8" action="<?php echo base_url('AreaAdministrador/excluirOrcamento/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;">	
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title modal-excluir-title" ></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" style="width: 750px;">					
						<div class="form-group m-form__group">
							<label>Motivo da Exclusão</label>
							<div class="m-input-icon m-input-icon--left">
								<textarea class="form-control m-input--air" name="motivo_exclusao" id="motivo_exclusao" required="required"></textarea> 
								<input type="hidden" name="orcamento_id" class="form-control m-input--air" id="orcamento_id" />
								<input type="hidden" name="tabela" class="form-control m-input--air" id="tabela" value="orcamentos" />
								<span class="m-input-icon__icon m-input-icon__icon--left">
									<span>
										<i class="la la-warning"></i>
									</span>
								</span>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
						<button type="submit" id ="excluir_orcamento" name="enviar_exclusao" value="1" class="btn btn-success" responsavel_id="" orcamento_id="" status_orcamento_id="">Excluir</button>
					</div>
				</div>
			</div>
		</form>
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ 	?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaAdministradorComercial/orcamentosStatus/1';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>
		<?php if ($this->session->flashdata('exclusao') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Orçamento excluído com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaAdministrador/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['exclusao']); } ?>