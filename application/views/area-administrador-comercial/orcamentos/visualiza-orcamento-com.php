<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-list-2"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Orçamento #<?php echo $dados[0]->orcamento_id; ?> &nbsp;&nbsp; <?php if( $dados[0]->status == "aberto" ){
							echo '<button class="btn m-btn--pill m-btn--air btn-secondary" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Aberto </button>';
						}elseif( $dados[0]->status == "fechado" ){
							$status = "";
							echo '<button class="btn m-btn--pill m-btn--air btn-success" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Fechado </button>';
						}elseif( $dados[0]->status == "perdido" ){
							echo '<button class="btn m-btn--pill m-btn--air btn-danger" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Perdido </button>';
						}elseif( $dados[0]->status == "cancelado" ){
							
							echo '<button class="btn m-btn--pill m-btn--air btn-warning" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Cancelado </button>';
						}elseif( $dados[0]->status == "orçamento entregue" ){							
							echo '<button class="btn m-btn--pill m-btn--air btn-info" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Orçamento Entregue </button>';
						}else{
							echo '<button class="btn m-btn--pill m-btn--air btn-primary" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> '.$dados[0]->status.' </button>';
							
						} 
						
						?>
						
						<input type="hidden" id="zona_atuacao" 		value="<?=($usuario_id == $zona_atuacao['usuario_id'] || $usuario_id == 7167) ? '1' : '0'?>">
						<input type="hidden" id="permite_alterar" 	value="<?php echo ($responsavel->id == $usuario_id) ? 1 : 0; ?>">
					</h3>
				</div>			
			</div>
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<!-- GERAR DESCONTO 
					<li class="m-portlet__nav-item">
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="geraDesconto" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>" data-toggle="modal" data-target="#m_modal_8" data-placement="top" title="Alterar Comissão">
							<i class="la la-dollar"></i>
						</a>	
					</li>-->
					<input type="hidden" id="fl_especial" value="<?php echo $dados[0]->fl_especial; ?>">
					<?php if($dados[0]->status_orcamento_id != 13 && $dados[0]->status_orcamento_id != 12 ){ ?>
					<li class="m-portlet__nav-item">
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="" orcamento_id="<?php echo $dados[0]->id_orcamento; ?>" data-toggle="modal" data-target="#m_modal_10"  title="Gerar PDF do Orçamento">
							<i class="la la-print"></i>
						</a>	
					</li>
					<li class="m-portlet__nav-item">						
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="emitirOrcamento" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>" data-toggle="modal" data-target="#m_modal_7" title="Emitir orçamento ao cliente">
							<i class="la la-send"></i>
						</a>							
					</li>
				<?php } ?>
				</ul>
			</div>
		</div>
		<div class="m-portlet__body">
			<?php if($dados[0]->origem != "Site" && $dados[0]->origem != ""){ ?>
			<div class="row">
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest"> Solicitante: </span>
						 <?php echo $solicitante[0]->nome.' | '.$solicitante[0]->razao_social.' | '.$solicitante[0]->cnpj;?>
					</h5>
				</div>							
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest"> Telefone: </span>
						<?php echo $solicitante[0]->telefone;?>
					</h5>
				</div>
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">E-mail: </span>
						<?php echo $solicitante[0]->email;?>
					</h5>
				</div>
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">Local: </span>
						<?php echo $solicitante[0]->local;?>
					</h5>
				</div>
			</div>
			<?php }else{ ?>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">Solicitante: </span>
						Orçamento realizado no site 
					</h5>
				</div>							
				
			</div>
			<?php } ?>
			<hr/>
			<div class="row">
				<div class="col-lg-4 col-xs-12"> 
					<h5><span class="m--font-warning m--font-boldest">
						<?php if(count($solicitante) > 0)  echo ($solicitante[0]->tp_cadastro == 'PJ') ? 'Empresa' : 'Nome'; ?>: </span>
						
							<a target="blank" href="<?php echo base_url('AreaAdministrador/visualizarEmpresa/'.$dados[0]->empresa_id); ?>" class="m-link m--font-transform-u">
								<?php echo $dados[0]->cnpj.' | '.$dados[0]->razao_social.' | '.$dados[0]->fantasia;?>
							</a>
						
					</h5>
				</div>							
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">Telefone: </span>
						<?php echo $dados[0]->telefone;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">E-mail: </span>
						<?php echo $dados[0]->email;?>
					</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Enderço:</span>
						 <?php echo $dados[0]->endereco;?>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Cidade:</span>
						 <?php echo $dados[0]->cidade;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Estado:</span>
						 <?php echo $dados[0]->estado;?>
						 <input type="hidden" id="estado" value="<?php echo strtoupper($dados[0]->estado);?>">
					</h5>
				</div>							
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-12 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Contato do Posto:</span>
						<?php if($dados[0]->contato_do_posto == "") {?>
							<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="cadastrarContato" orcamento_id="<?php echo  $dados[0]->id_orcamento; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>' data-toggle="modal" data-target="#modal_add_usuario" title="cadastrar contato">
								<i class="la la-user"></i>
							</a>
						<?php }else{ ?>	
							
							<a target="blank" href="<?php echo base_url('AreaAdministrador/editarUsuario/'.$dados[0]->contato_id); ?>" class="m-link m--font-transform-u">
								<?php echo ($dados[0]->contato_do_posto == "") ? $dados[0]->contato_posto : $dados[0]->contato_do_posto; ?>
							</a>
							<input type="hidden" id="contato_posto" name="contato_posto" value="<?php echo ($dados[0]->contato_do_posto == "") ? $dados[0]->contato_posto : $dados[0]->contato_do_posto; ?>">
						<?php } ?> 
					</h5>
				</div>	
			</div>
			<hr>
			
			<div class="row">				
				<div class="col-lg-4" style="<?php echo ( !isset($indicador)  ) ? 'display:none;' : 'display: block;';?>" >
					<div class="m-input-icon m-input-icon--right">
						<h5>
							<span class="m--font-warning m--font-boldest">Indicador:</span>
						 	<span id="alterar_indicador"><?php echo  (isset($indicador[0]->id)) ? $indicador[0]->indicador : 'Não houve indicação para este orçamento'; ?>
						 		
						 	</span>
						 	<i class="la la-minus excluir_indicador" data-toggle="m-tooltip"  data-original-title="Excluir Indicador" style="color: #ffcc00; font-size: 16px;" orcamento_id = "<?php echo $dados[0]->id_orcamento; ?>" title="Excluir Indicador"></i>
						</h5>						
					</div>
				</div>
				<div class="col-lg-4">
					<label><b>Previsão de Fechamento:</b></label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="dt_previsao" id="dt_previsao" class="form-control m-input date" placeholder="Insira uma data" value="<?=date('d/m/Y',strtotime($dados[0]->dt_previsao))?>" required="required">	
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar-o"></i></span></span>
					</div>
				</div>
				<div class="col-lg-4">
					<label><b>Previsão de Produção:</b></label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="dt_previsao_prod" id="dt_previsao_prod" class="form-control m-input date" placeholder="Insira uma data" value="<?=date('d/m/Y',strtotime($dados[0]->dt_previsao_prod))?>" required="required">	
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-calendar-o"></i></span></span>
					</div>
				</div>
			</div>	
			<hr>
			<div class="row">
				<div class="col-lg-12">
					<div class="m-input-icon m-input-icon--right">
						<h5>
							<span class="m--font-warning m--font-boldest"><b>Alterar/Inserir Indicador: </b>
								<input type="checkbox" name="indicacao" value="1" class="indicacao" />
							</span>
						</h5>
								
					</div>
				</div>
			</div>	
			<div class="form-group m-form__group row">
				<div class="col-lg-12 nome_indicador" style="display: none;">
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="nome_indicador" id="nome_indicador" orcamento_id = "<?php echo $dados[0]->id_orcamento; ?>" class="form-control m-input" placeholder="Insira o nome/razão social do indicador" value="" />
						<input type="hidden" name="indicador_id" id="indicador_id" value="<?php echo (isset($indicador[0]->id)) ? $indicador[0]->id : ''; ?>" />
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>
				</div>
			</div>
			<hr/>
			<div class="form-group m-form__group row">
				<div class="col-lg-12">
					<label><b>Observacao:</b></label>
					<div class="m-input-icon m-input-icon--right">
						<textarea  name="observacao" id="observacao" class="form-control m-input" ><?php echo $dados[0]->observacao; ?></textarea>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>												
				</div>				
			</div>			
			<!--<div class="row">
				<div class="col-lg-12 col-xs-12">
					<h4>
						<span class="m--font-warning m--font-boldest">Indicador: </span>
						<?php if( count($indicador) > 0 ) {?>

							<?php echo $indicador['indicador'];?>

						<?php }else{ ?> 

							<?php echo "Não houve indicação para este orçamento"; ?>

						<?php } ?>	
					</h4>
				</div>											
			</div>-->
		</div>
	</div>
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	***************************************** Forma de entrega, fretes e forma de pagto *******************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">					
					<h2 class="m-portlet__head-label m-portlet__head-label--warning" style="width: 320px;">
						<span>
							Forma de Pagto, Entrega e Frete
						</span>
					</h2>
				</div>
			</div>			
		</div>
		<div class="m-portlet__body">
			<div class="form-group m-form__group">
				<label for="Frete">
					Forma de Pagamento
				</label>
				<select class="form-control m-input m-input--square entregaFretePagto" id="forma_pagto" name="forma_pagto" orcamento_id='<?php echo $dados[0]->orcamento_id; ?>' status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>
					<option value="0">Selecione a forma de pagamento</option>					
					<?php 	foreach ($formaPagto as $pagto) { ?>
						<option value="<?php echo $pagto->id;?>" <?php echo ( $dados[0]->forma_pagto_id == $pagto->id  ) ? 'selected="selected"' : '';?>><?php echo $pagto->descricao; ?></option>								
					<?php } ?>
				</select>
			</div>
			<div class="form-group m-form__group">
				<label for="Frete">
					Entrega
				</label>
				<!--<select class="form-control m-input m-input--square entregaFretePagto" id="entregas" name="entrega" orcamento_id='<?php echo $dados[0]->orcamento_id; ?>' status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>
					<option value="0">Selecione como vai ser realizado a entrega do(s) produto(s)</option>					
					<?php foreach ($entregas as $entrega) { ?>
						<option value="<?php echo $entrega->id;?>"<?php echo ( $dados[0]->entrega_id == $entrega->id  ) ? 'selected="selected"' : '';?>><?php echo $entrega->descricao; ?></option>								
					<?php } ?>
				</select>-->
				<select class="form-control m-input m-input--square entregaFretePagto" id="entregas" name="entrega" orcamento_id='<?php echo $dados[0]->orcamento_id; ?>' status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>

					<?php 	foreach ($entregas as $entrega) {
								if($entrega->id == 17 || $dados[0]->entrega_id == $entrega->id){	?>
									<option value="<?php echo $entrega->id;?>" <?=($dados[0]->entrega_id== $entrega->id) ? 'selected=selected' : ''?>><?php echo $entrega->descricao; ?></option>
					<?php 		}
							}
					?>
				</select>
			</div>
			<div class="form-group m-form__group">
				<label for="Frete">
					Frete
				</label>
				<select class="form-control m-input m-input--square entregaFretePagto" id="frete" name="frete" orcamento_id='<?php echo $dados[0]->orcamento_id; ?>' status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>
					<option value="0">Selecione o tipo de frete utilizado</option>		
					<?php foreach ($fretes as $frete) { ?>
						<option value="<?php echo $frete->id;?>" <?php echo ( $dados[0]->frete_id == $frete->id  ) ? 'selected="selected"' : '';?>><?php echo $frete->descricao; ?></option>								
					<?php } ?>			
				</select>
			</div>
			<div class="form-group m-form__group">
				<label for="Garantia">
					Garantia
				</label>
				<select class="form-control m-input m-input--square entregaFretePagto" id="garantia" name="garantia" orcamento_id='<?php echo $dados[0]->id_orcamento; ?>' status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>
					<option value="0">Selecione o tipo de garantia utilizado</option>		
					<option value="12" <?=($dados[0]->garantia == 12) ? 'selected=selected' : ''?>>12 Meses</option>			
					<option value="24" <?=($dados[0]->garantia == 24) ? 'selected=selected' : ''?>>24 Meses</option>
				</select>
			</div>
		</div>
	</div>	
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	********************************************************* Produtos ************************************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi" id="produtos_anchor">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>Produtos</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<table class="table m-table m-table--head-separator-warning">
			  	<thead>
			    	<tr>
			      		<th>#</th>
			      		<th>Código | Modelo | Descrição</th>			      		
			      		<th>Quantidade</th>			      		
			      		<th style="text-align: right;">Valor Unitário R$</th>
			      		<th style="text-align: right;">
			      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" data-toggle="modal" data-target="#m_modal_9"  title="Adicionar produto">
								<i class="la la-plus"></i>
							</a>
						</th>	
			    	</tr>
			  	</thead>
			  	<tbody total_indice='<?php echo count($dados); ?>'>
			  		<?php $i=1;$total=0; foreach($dados as $dado){ ?>
			    	<tr indice='<?php echo $i; ?>' class="novo_produto_orc">
				      	<th scope="row"><?php echo $i;?></th>
				      	<td>
				      		<?php 
                                    $bombas_op     =   "";
                                    $opcionais_op  =   "";
                                    $bombas_op_slim  =   "";
                                    $bombas_op_baixa  =   "";
                                    $bombas_ds 	= "";

                                    foreach( $bombas as $bomba )
                                    {
                                        if( $bomba->tipo_produto_id == 4 ){
                                        	if( $dado->produto_id == $bomba->id ){
                                        		$selected = 'selected="selected"';
                                        		$valor_base = $bomba->valor_unitario;
                                        		$reajuste = $bomba->reajuste;
                                        		$tipo = 'opcionais';

                                        	}else{
                                        		$selected = '';
                                        	}

                                            $opcionais_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'".$selected." tipo='opcionais' reajuste='".$bomba->reajuste."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif($bomba->tipo_produto_id == 1){
                                        	if( $dado->produto_id == $bomba->id ){
                                        		$selected = 'selected="selected"';
                                        		$valor_base = $bomba->valor_unitario;
                                        		$reajuste = $bomba->reajuste;
                                        		$tipo = 'bomba_baixa';
                                        	}else{
                                        		$selected = '';
                                        	}
                                            $bombas_op_baixa.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'".$selected." tipo='bomba_baixa' reajuste='".$bomba->reajuste."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif($bomba->tipo_produto_id == 2){

                                        	if( $dado->produto_id == $bomba->id ){
                                        		$selected = 'selected="selected"';
                                        		$valor_base = $bomba->valor_unitario;
                                        		$reajuste = $bomba->reajuste;
                                        		$tipo = 'bomba_baixa_slim';
                                        	}else{
                                        		$selected = '';
                                        	}
                                            $bombas_op_slim.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'".$selected." tipo='bomba_baixa_slim' reajuste='".$bomba->reajuste."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif ($bomba->tipo_produto_id == 3){
                                        	if( $dado->produto_id == $bomba->id ){
                                        		$selected = 'selected="selected"';
                                        		$valor_base = $bomba->valor_unitario;
                                        		$reajuste = $bomba->reajuste;
                                        		$tipo = 'bomba_alta';
                                        	}else{
                                        		$selected = '';
                                        	}
                                            $bombas_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'".$selected." tipo='bomba_alta' reajuste='".$bomba->reajuste."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif ($bomba->tipo_produto_id == 6){
                                        	if( $dado->produto_id == $bomba->id ){
                                        		$selected = 'selected="selected"';
                                        		$valor_base = $bomba->valor_unitario;
                                        		$reajuste = $bomba->reajuste;
                                        		$tipo = 'bomba_alta';
                                        	}else{
                                        		$selected = '';
                                        	}
                                            $bombas_ds.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'".$selected." tipo='bomba_alta' reajuste='".$bomba->reajuste."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }    
                                    } 
                            ?>
                            <select  class="form-control bombas" style="font-size: 15px;" name="produto[]" indice='<?php echo $i;?>' orcamento_produto_id="<?php echo $dado->orcamento_produto_id;?>" id="bombas_<?php echo $i;?>">
                            	<option value="">Selecione um produto</option>
                                <optgroup label="Bombas Mangueira Alta" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op; ?>
                                </optgroup>    
                                <optgroup label="Bombas Mangueira Slim" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op_slim; ?>
                                </optgroup>    
                                <optgroup label="Bombas Mangueira Baixa" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op_baixa; ?>
                                </optgroup>
                                <optgroup label="Dispensers" data-subtext="optgroup subtext">
                                    <?php echo $bombas_ds; ?>
                                </optgroup>
                                <optgroup label="Opcionais" data-subtext="optgroup subtext">
                                    <?php echo $opcionais_op; ?>
                                </optgroup>
                            </select>

				      	</td>
				      	<td>
				      		<input type="text" name="qtd[]" indice='<?php echo $i;?>' class='form-control qtd' orcamento_produto_id="<?php echo $dado->orcamento_produto_id;?>" value='<?php echo $dado->qtd; ?>' style="width: 50px;    float: left;" maxlength="3" id="qtd_<?php echo $i;?>" disabled='disabled' />
				      		<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill editar_qtd" data-toggle="m-tooltip"  data-original-title="Editar Quantidade" style="float: right;margin-top: -5px;"  indice='<?php echo $i;?>'>
								<i class="la la-edit"></i>
							</button>
				      	</td>
				      	<td style="text-align: right;">
				      		<b class="valor_unitario valor_unitario_<?php echo $i;?>" valor_base="<?php echo $valor_base; ?>" orcamento_produto_id='<?php echo $dado->orcamento_produto_id; ?>' orcamento_id = "<?php echo $dados[0]->orcamento_id;?>" tipo='<?php echo $tipo; ?>' reajuste="<?=$reajuste?>"><?php echo $dado->valor; ?></b>

							<input type="hidden" name="valor[]" id="valor_produto_<?php echo $i;?>" class="valor_produto" value="<?php echo $dado->valor; ?>" required="required" maxlength="14" disabled="disabled" style="width: 71%;text-align: right;padding: 8px;float: right;" qtd="<?php echo $dado->qtd; ?>" orcamento_produto_id='<?php echo $dado->orcamento_produto_id; ?>'>	
				      	</td>
				      	<td colspan="2" style="text-align: right;">
				      		<i class="fa fa-minus excluir_produto" data-toggle="m-tooltip"  data-original-title="Excluir Produto" onclick="excluirProduto(<?php echo $dado->orcamento_produto_id; ?>,<?php echo $i; ?>,<?php echo $dados[0]->orcamento_id; ?>);" style="color: red;font-size: 16px; cursor: pointer; margin-top: 10px;" indice='0'></i>
				      	</td>
			    	</tr>
			    	<?php $i++; $total = ($dado->qtd * $dado->valor) + $total; } ?>
			    	<tr>			    		
			    		<td></td>
						<td></td>
						<td></td>
						<!-- <?php echo $dados[0]->valor_tributo."%";?> de ICMS + 5% de IPI + <?php echo $desconto[0]['valor_desconto'] ."%";?> de Comissão -->
			    		<td><b style="text-align: right;">Total: </b></td>			    		
			    		<td style="text-align: right;">R$ <b class="valor_subtotal" style="margin-right: 10px;"><?php echo number_format($total,2,",",".");?> </b>
			    			<!--<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill atualiza_valor" data-toggle="m-tooltip"  data-original-title="Atualizar Valores" style="float: right;margin-top: -5px;"  indice='<?php echo $i;?>'>
								<i class="la la-refresh"></i>
							</button>-->
							<input type="hidden" name="valor_total" id="valor_total"	 	value="<?php echo $total;?>" />
			    			<input type="hidden" name="valor_tributo" id="valor_tributo" 	value="<?php echo ($dados[0]->valor_tributo == '') ? '7.00' : $dados[0]->valor_tributo; ?>">
			    			<input type="hidden" name="orcamento_id" id="orcamento_id" 		value="<?php echo $dados[0]->orcamento_id; ?>">
			    			<input type="hidden" name="status_id" id="status_id" 			value="<?php echo $dados[0]->status_orcamento_id; ?>">
			    			<?php
                    			$comissao = (isset($desconto[0]['valor_desconto'])) ? $desconto[0]['valor_desconto'] : '0.01';
                    		?>
			    			<input type="hidden" name="comissao" id="comissao" 	value="<?php echo $comissao; ?>"	/>
			    			<input type="hidden" name="fator" id="fator" value="<?php echo $dados[0]->fator; ?>"	/>			    			  
			    		</td>
			    	</tr>
			  	</tbody>
			</table>
			 <div style="text-align: center;">			 	
                    <div class="m-radio-inline">
                    	<input type="hidden" name="reajuste" id="reajuste" />
                    	<label>Selecione a comissão: &nbsp;</label>
                    	<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="Inserir valores manuais" >
							<input type="radio" name="valor_desconto_orc" class="comissao" value="outro" <?php echo ($comissao == '0.01') ? 'checked=checked' : '';  ?> />
							Outro Valor
							<span></span>
						</label>
						<!--<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="TB2" class="comissao">
							<input type="radio" name="valor_desconto_orc" value="TB2" class="comissao" <?php echo ($comissao == '0.02') ? 'checked=checked' : '';  ?>/>
							TB2  
							<span></span>
						</label>-->
						<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="0% de comissão" class="comissao">
							<input type="radio" name="valor_desconto_orc" value="0" class="comissao" <?php echo ($comissao == '0.00') ? 'checked=checked' : '';  ?>/>
							0%  
							<span></span>
						</label>
						<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="2% de comissão" >
							<input type="radio" name="valor_desconto_orc"  id="comissao2" value="2" class="comissao" <?php echo ($comissao == '2.00') ? 'checked=checked' : '';  ?>/>
							2%  
							<span></span>
						</label>
						<label class="m-radio" data-toggle="m-tooltip"  data-placement="top" title="3% de comissão">
							<input type="radio" name="valor_desconto_orc" id="comissao3" value="3" class="comissao" <?php echo ($comissao == '3.00') ? 'checked=checked' : '';  ?>/>
							3%  
							<span></span>
						</label>
						<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="4% de comissão" >
							<input type="radio" name="valor_desconto_orc" id="comissao4"  value="4" class="comissao" <?php echo ($comissao == '4.00') ? 'checked=checked' : ''; ?>/>
							4%
							<span></span>
						</label>
						<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="5% de comissão" >
							<input type="radio" name="valor_desconto_orc"   id="comissao5"class="comissao" value="5" <?php echo ($comissao == '5.00') ? 'checked=checked' : ''; ?> />
							5%
							<span></span>
						</label>
						<!--<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="Tabela Especial" id="tn" style="">
							<input type="radio" name="valor_desconto_orc" class="comissao" value="TN" <?php echo ($comissao == '6.00') ? 'checked=checked' : ''; ?>  />
							Especial
							<span></span>
						</label>-->
						<button class="btn btn-success m-btn m-btn--icon m-btn--wide" id="aprovacao" style="float: right;display: none;" orcamento_id="<?php echo $dados[0]->orcamento_id;?>">
							<span>
								<i class="fa fa-check"></i>
								<span>
									Alterar valor fora das tabelas
								</span>
							</span>
						</button>
					</div>
					
                    
                </div>       
		</div>
	</div>
</div>	
<!-- Modal inserir representante e contato -->
<div class="modal fade" id="m_modal_10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<form action="<?php echo base_url('areaAdministradorComercial/geraOrcamentoPdf');?>" method="post" target="_blank">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Dados Contato do posto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">																
					<div class="col-lg-12">
						<label>Contato do posto:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required="" name="contato_posto" id="contato_posto" value="<?php echo $dados[0]->contato_posto;?>" class="form-control m-input" placeholder="" style="" maxlength="" >
							<input type="hidden" name="orcamento_id" value="<?=$dados[0]->orcamento_id;?>">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-user"></i></span></span>
						</div>
					</div>	
				</div>										
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="submit" class="btn btn-info" id="" orcamento_id="<?php echo $dados[0]->id_orcamento; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>Enviar</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- Modal Emitir orçamento -->
<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Emitir orçamento para cliente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">						
					<div class="col-lg-12">
						<label>Enviar para o e-mail:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="email" required="" name="email_destino" id="email_destino" class="form-control m-input" placeholder="" style="text-align: right;"  value="<?php echo $dados[0]->email;?>">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-envelope"></i></span></span>
						</div>							
					</div>
					
				</div>
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="emitir_orcamento" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>Enviar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Desconto -->
<div class="modal fade" id="m_modal_8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Selecionar Comissão</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">											
					<div class="col-lg-6">
						<label>Comissão:</label>
						<div class="m-input-icon m-input-icon--right">
							<input id="valor_promo_orc" name="valor_promo_orc" data-slider-tooltip="show" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="<?php if( count($desconto) > 0) echo $desconto[0]['valor_desconto']; else echo 0; ?>"/>
							<input type="text" required="" name="valor_promo_orc" id="valor_promo_orc" value="<?php if( count($desconto) > 0) echo $desconto[0]['valor_desconto']; ?>" class="form-control m-input" placeholder="" style="text-align: right;" maxlength="5">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-percent"></i></span></span>
							
						</div>							
					</div>
					<div class="col-lg-6">
						<label>Motivo do desconto:</label>
						<div class="m-input-icon m-input-icon--right">							
							<textarea name="motivo_desconto_orc" id="motivo_desconto_orc" class="form-control m-input" disabled="disabled"><?php if( count($desconto) > 0) echo $desconto[0]['motivo_desconto']; ?></textarea>
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-comment"></i></span></span>
						</div>							
					</div>
				</div>							
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="adicionar_desconto_orc" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>">Enviar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Adiciona produto -->
<div class="modal fade" id="m_modal_9" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Adicionar Produto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 850px;">
				<div class="m-portlet__body">
					
					<form id="produtos_novos">
					<table class="table m-table m-table--head-separator-warning novo">
						
				  	<thead>
				    	<tr>				      		
				      		<th>Código | Modelo | Descrição</th>			      		
				      		<th>Quantidade</th>			      		
				      		<th style="text-align: right;">Valor Unitário R$</th>
				      		<th>
				      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add" data-toggle="m-tooltip" data-placement="top" title="Adicionar produto">
									<i class="la la-plus"></i>
								</a>
							</th>	
				    	</tr>
				  	</thead>
				  	<tbody >
			  		<?php $i=0;?>
			    	<tr id="modelo" indice='0' total_indice='0'>	
				      	<td>
				      		<?php 
                                    $bombas_op     =   "";
                                    $opcionais_op  =   "";
                                    $bombas_op_slim  =   "";
                                    $bombas_op_baixa  =   "";
                                    $bombas_dispenser 	= 	"";
                                    foreach( $bombas as $bomba )
                                    {
                                        if( $bomba->tipo_produto_id == 4 ){
                                        	$opcionais_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='opcionais' reajuste='".$bomba->reajuste."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif($bomba->tipo_produto_id == 1){
                                        	
                                            $bombas_op_baixa.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='bomba' reajuste='".$bomba->reajuste."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif($bomba->tipo_produto_id == 2){                                        	
                                            $bombas_op_slim.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='bomba' reajuste='".$bomba->reajuste."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif ($bomba->tipo_produto_id == 3){
                                        	$bombas_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='bomba' reajuste='".$bomba->reajuste."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif ($bomba->tipo_produto_id == 6){
                                        	$bombas_dispenser.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='bomba' reajuste='".$bomba->reajuste."'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }    
                                    } 
                            ?>
                            <select  class="form-control bombas_new" style="font-size: 12px;" name="produto_id" indice='0' id="bombas_0">
                            	<option value="">Selecione um produto</option>
                                <optgroup label="Bombas Mangueira Alta" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op; ?>
                                </optgroup>    
                                <optgroup label="Bombas Mangueira Slim" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op_slim; ?>
                                </optgroup>    
                                <optgroup label="Bombas Mangueira Baixa" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op_baixa; ?>
                                </optgroup>
								<optgroup label="Dispensers" data-subtext="optgroup subtext">
                                    <?php echo $bombas_dispenser; ?>
                                </optgroup>
                                <optgroup label="Opcionais" data-subtext="optgroup subtext">
                                    <?php echo $opcionais_op; ?>
                                </optgroup>
                            </select>

				      	</td>
				      	<td>
				      		<input type="text" name="qtd" indice='0' class='form-control'  maxlength="3" id="qtd_new_0" style="width: 50px;"  />
				      		
				      	</td>
				      	<td>				      		
							<b class="valor_unitarios_text valor_unitarios_text_0" ></b>
							<input type="hidden" name="valor_unitarios" id="valor_unitarios_0" class="valor_unitarios form-control" required="required" maxlength="14" orcamento_id = "<?php echo $dados[0]->orcamento_id;?>"/>
				      	</td>
				      	<td>
                        	<i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
                        </td>
			    	</tr>		    	
			    	
			  		</tbody>
					</table>
					 
				</div>										
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="adicionar_produto" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>Enviar</button>
			</div>
			</form>	
		</div>
	</div>
</div>

<!-- Modal Andamento -->
<div class="modal fade" id="modal_reemissao" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">		
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Motivo da reemissão do orçamento</h5>
			</div>
			<div class="modal-body" style="width: 750px;">				
				<div class="form-group m-form__group">
					<div class="form-group m-form__group" >
						<label for="exampleSelect1"></label>
						<textarea type="text" class="form-control m-input m-input--air" id="motivo_emissao" placeholder="Motivo"></textarea>
					</div>
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="button" id ="adicionar_motivo" class="btn btn-primary" status_orcamento="<?php echo $dados[0]->status_orcamento_id; ?>" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>">Adicionar Andamento</button>
			</div>
		</div>
	</div>		
</div>	