<style type="text/css">
	#html_table tbody > tr > td{
		text-align: center;
	}
	#html_table_processing{
		position: absolute;
		top: 0;
		font-size: 21px;
		color: #000;
		font-weight: 400;
		text-align: center;
		width: 98%;
		background: #ffcc0075;
		height: 100%;
		padding-top: 18%;
	}
	
</style>
<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Prospecções &nbsp;&nbsp;<a href="<?php echo base_url('AreaAdministradorComercial/cadastrarProspeccao')?>" class="	novo_orcamento" style="color: #ffcc00; font-weight: bold;" >
						<i class="la la-plus-circle" style="font-size: 38px;"></i>
					</a>
					</h3>
					
				</div>				
			</div>
			<div class="m-portlet__head-caption cadastrar_orcamento"></div>			 	
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;">#</th>
						<th style="text-align: center;">Empresa</th>
						<th style="text-align: center;">Estado</th>						
						<th style="text-align: center;">Dthr. Abertura</th>	
						<th style="text-align: center;">Situção</th>											
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($dados as $dado){ ?>
					<tr>
						<td><?=$dado['id'];?></td>
						<td><?=$dado['cliente'];?></td>
						<td><?=$dado['estado'];?></td>
						<td><?=$dado['data_abertura'];?></td>
						<td><?=$dado['status'];?></td>
						<td>
							<a href="<?php echo base_url('AreaAdministradorComercial/visualizarProspeccao/'.$dado['id'])?>" >
								<i class="la la-edit editar"></i>
							</a> 								
						</td>
					</tr>
				<?php } ?>	
				</tbody>
			</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group status_orcamento_" >
						<label for="exampleSelect1">Situação do Orçamento</label>
						<select class="form-control m-input m-input--air" id="status_orcamento">
							<option value="">Selecione o status do orçamento</option>						
							<option value="1">Aberto</option>							
							<option value="6">Em negociação</option>
							<option value="2">Fechado</option>							
							<option value="10">Perdido para Wayne</option>
							<option value="11">Perdido para Gilbarco</option>
							<option value="3">Perdido Outros</option>
							<option value="4">Cancelado</option>
							<option value="14">Sem Retorno</option>
						</select>
					</div>
					<div class="form-group m-form__group aprova_indicacao" style="display: none;" >
						<label for="exampleSelect1">Aprovar esta Indicação?</label>
						<div class="m-radio-inline">
							<label class="m-radio">
								<input type="radio" name="aprovar_indicacao" class="aprova_inidicacao" value="1" checked="checked" />
								Sim
								<span></span>
							</label>
							<label class="m-radio">
								<input type="radio" name="aprovar_indicacao" class="aprova_inidicacao" value="0" />
								Não
								<span></span>
							</label>							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" class="btn btn-primary" orcamento_id="" indicacao="0">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>
		
	<!-- Modal Excluir -->
	<div class="modal fade" id="m_excluir_orcamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form accept-charset="utf-8" action="<?php echo base_url('AreaAdministrador/excluirOrcamento/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;">	
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title modal-excluir-title" ></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" style="width: 750px;">					
						<div class="form-group m-form__group">
							<label>Motivo da Exclusão</label>
							<div class="m-input-icon m-input-icon--left">
								<textarea class="form-control m-input--air" name="motivo_exclusao" id="motivo_exclusao" required="required"></textarea> 
								<input type="hidden" name="orcamento_id" class="form-control m-input--air" id="orcamento_id" />
								<input type="hidden" name="tabela" class="form-control m-input--air" id="tabela" value="orcamentos" />
								<span class="m-input-icon__icon m-input-icon__icon--left">
									<span>
										<i class="la la-warning"></i>
									</span>
								</span>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
						<button type="submit" id ="excluir_orcamento" name="enviar_exclusao" value="1" class="btn btn-success" responsavel_id="" orcamento_id="" status_orcamento_id="">Excluir</button>
					</div>
				</div>
			</div>
		</form>
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ 	?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ 	?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaAdministradorComercial/presp';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>
		<?php if ($this->session->flashdata('exclusao') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Orçamento excluído com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaAdministrador/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['exclusao']); } ?>