<div class="m-content">	
		
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-list-2"></i>
					</span>
					<h3 class="m-portlet__head-text"> 
						Cadastro de Prospecção 
					</h3>
				</div>			
			</div> 
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<form accept-charset="utf-8" action="<?php echo base_url('AreaAdministradorComercial/insereProspeccao/'); ?>" method="post" id="formulario" class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
		<div class="m-portlet__body">
			<div class="form-group m-form__group row">									
				<div class="col-lg-12">
					<label><b>Tipo de Atendimento:</b></label>
					<div class="m-radio-inline">
						<label class="m-radio">
							<input type="radio" value="a" class="tp" checked="checked" name="tp_atendimento" />
							Atendimento/Redirecionamento
							<span></span>
						</label>
						<label class="m-radio">
							<input type="radio" value="p" class="tp" name="tp_atendimento" />
							Prospecção/Negociação
							<span></span>
						</label>												
					</div>											
				</div>	
			</div>
			<div class="form-group m-form__group row div_cnpj" style="display: none;">
				<div class="col-lg-3">
					<label id='label_documento'>Documento (CNPJ/CPF):</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text"  name="cnpj" id="cnpj" class="form-control m-input" placeholder="Insira um cnpj"  value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
						<input type="hidden"  id="salvar" maxlength="15" name="salvar" value="1"  />
						<input type="hidden"  id="cartao_cnpj" name="cartao_cnpj"  />
                        <input type="hidden"  id="empresa_id" maxlength="15" name="empresa_id" value=""  />
					</div>					
				</div>
				<div class="col-lg-3 esconde">
					<label id="label_razao">Razão Social:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" 	name="razao_social" id="razao_social" class="form-control m-input" placeholder=""  value="">
						<input type="hidden" name="id" 	class="form-control m-input" placeholder="Insira uma razão social"  value="">								
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
						</span>
					</div>
				</div>
				<div class="col-lg-3 esconde" id="campo_fantasia">
					<label class="">Fantasia:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="fantasia" id="fantasia" class="form-control m-input" placeholder="Insira uma fantasia"  value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>	
				</div>
				<div class="col-lg-3 esconde" id="campo_ie">
					<label class="">Inscrição Estadual:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control m-input" placeholder="Inscrição Estadual" value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>	
				</div>					
				
			</div>	 
			<div class="form-group m-form__group row esconde">	
				<div class="col-lg-3">
					<label>E-mail:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="email" name="email" id="email" class="form-control m-input" placeholder="Insira um email" value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>							
				</div>			
				<div class="col-lg-3">
					<label>Telefone:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text"  name="telefone" id="telefone" class="form-control m-input" placeholder="Insira um telefone"  value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Cep:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text"  name="cep" id="cep" class="form-control m-input" placeholder="Insira um cep"  value="" >
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Endereço:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text"  name="endereco" id="endereco" class="form-control m-input" placeholder="Insira um endereço"  value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
					</div>							
				</div>
				
			</div>
			<div class="form-group m-form__group row esconde">
				<div class="col-lg-3">
					<label>Bairro:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text"  name="bairro" id="bairro" class="form-control m-input" placeholder="Insira o bairro"  value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
					</div>							
				</div>	
				<div class="col-lg-3">
					<label class="">Cidade:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" 		name="cidade"  		id="cidade" 	class="form-control m-input" placeholder="Insira um cidade" value="">
						<input type="hidden"	name="codigo_ibge_cidade" 	id="codigo_ibge_cidade" 	value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
					</div>					
				</div>
				<div class="col-lg-3">
					<label class="">Estado:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="hidden" id="estadoE" class="form-control" placeholder="Insira um estado" />
						<select  class="form-control"  maxlength="250" name="estado" placeholder="Estado" id="estado" >
                            <option value="">Selecione um Estado</option>
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AP">Amapá</option>
                            <option value="AM">Amazonas</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PR">Paraná</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SP">São Paulo</option>
                            <option value="SE">Sergipe</option>
                            <option value="TO">Tocantins</option>
                        </select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
					</div>							
				</div>					
				<div class="col-lg-3">
					<label>País:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text"  name="pais" id="pais" class="form-control m-input" placeholder="Insira um pais"  value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map"></i></span></span>
					</div>							
				</div>
			</div>	
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>							
					</div>
				</div>
			</div>			
		</div>	
		</form>
	</div>	
	
</div>