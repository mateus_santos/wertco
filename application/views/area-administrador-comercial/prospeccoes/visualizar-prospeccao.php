<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministradorComercial/prospeccoes'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministradorComercial/prospeccoes'); ?>">Voltar</a>&nbsp;&nbsp;
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->			
			<input type="hidden" name="id" value="<?= $dados['id']?>" id="prospeccao_id">				
			<div class="col-lg-12" >
				<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" style="padding-bottom: 62px;">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="flaticon-statistics"></i>
								</span>										
								<h2 class="m-portlet__head-label m-portlet__head-label--success">
									<span>
										Informações da Prospecção <b>#<?=$dados['id']?></b>
									</span>
								</h2>
							</div>
						</div>								
					</div>
					<div class="m-portlet__body ">
						<?php if($dados['status_id'] == 1){ ?>
						<div class="form-group m-form__group row" style="border-top: 1px #f0f0f0 dotted;">
							<div class="col-lg-12">
								<label>Cliente</label>
								<div class="m-input-icon m-input-icon--right">
									<input type="text" value="<?=$dados['cliente']?>" id="cliente" class="form-control m-input" placeholder="" disabled>
									<span class="m-input-icon__icon m-input-icon__icon--right" style="right: 21px !important;">
										<i onclick="window.open('<?php echo base_url('AreaAdministrador/visualizarEmpresa/'.$dados['cliente_id']); ?>')" class="la la-file-text-o " style="margin-top: 10px;color: #ffcc00;"></i>			
										</span>
								</div>
							</div>											
						</div>
						<?php } ?>
						<div class="form-group m-form__group row" style="border-top: 1px #f0f0f0 dotted;">
							<div class="col-lg-6">
								<label>Status</label>
								<div class="m-input-icon m-input-icon--right">
									<select name="status_id" id="status_id" class="form-control" required="">
										<option value="">Selecione um status</option>
										<?php foreach( $status as $status_ ){ ?>
											<option <?php if($dados['status_id'] == $status_['id']) echo "selected"; ?> value="<?php echo $status_['id']; ?>"><?php echo $status_['descricao']; ?></option>
										<?php } ?>
									</select>
									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
								</div>
							</div>
							<div class="col-lg-6">							
								<label>Cadastrado por:</label>
								<div class="m-input-icon m-input-icon--right">
									<input type="text" name="criado" value="<?php echo $dados['usuario']; ?>" id="" class="form-control m-input" disabled="disabled" >									
								</div>							
							</div>								
						</div>
						<div class="form-group m-form__group row destinatario" style="padding-top: 25px; border-top: 1px #f0f0f0 dotted;<?php echo ($dados['status_id'] == 2) ? 'display: block;' : 'display: none;'; ?>">	
							<div class="col-lg-6">
								<label>Informe o destino</label>
								<div class="m-input-icon m-input-icon--right">
									<select id="setor_destinacao" class="form-control">
										<option value="companytec" <?php echo ($dados['setor_destinacao'] == 'companytec') ? 'selected="selected"' : ''; ?>>Companytec</option>
										<option value="assistencia" <?php echo ($dados['setor_destinacao'] == 'assistencia') ? 'selected="selected"' : ''; ?>>Assistência Técnica</option>
										<option value="suporte" <?php echo ($dados['setor_destinacao'] == 'suporte') ? 'selected="selected"' : ''; ?>>Suporte</option>
										<option value="compras" <?php echo ($dados['setor_destinacao'] == 'compras') ? 'selected="selected"' : '';?>>Compras</option>
										<option value="financeiro" <?php echo ($dados['setor_destinacao'] == 'financeiro') ? 'selected="selected"' : ''; ?>>Financeiro</option>
										<option value="gestor" <?php echo ($dados['setor_destinacao'] == 'gestor') ? 'selected="selected"' : ''; ?>>Gestor</option>
										<option value="produção" <?php echo ($dados['setor_destinacao'] == 'produção') ? 'selected="selected"' : ''; ?>>Produção</option>
										<option value="almoxarifado" <?php echo ($dados['setor_destinacao'] == 'almoxarifado') ? 'selected="selected"' : ''; ?>>Almoxarifado</option>
										<option value="qualidade" <?php echo ($dados['setor_destinacao'] == 'qualidade') ? 'selected="selected"' : ''; ?>>Qualidade</option>
									</select>
									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-send"></i></span></span>
								</div>
							</div>	
							<div class="col-lg-6">
								<label>Mensagem para o setor de destino</label>
								<div class="m-input-icon m-input-icon--right">
									<textarea id="msg_destinacao" class="form-control"><?php echo $dados['msg_destinacao']; ?></textarea>
									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
								</div>
							</div>
							<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit destinatario" style="border-top: 1px #f0f0f0 dotted;margin-top: 15px;margin-left: 15px;">
								<div class="m-form__actions m-form__actions--solid">
									<div class="row">
										<div class="col-lg-6">									
											<button type="button" id="encaminhar" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase" >Encaminhar Atendimento</button>					
										</div>							
									</div>
								</div>
							</div>							
						</div>					
				</div>						
			</div>								
		</div>
	</div>
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head" >
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Atividades&nbsp;&nbsp;
						</h3>
						<div class="m-portlet__head-caption cadastrar_orcamento">	
							<a data-toggle="modal" href="#" class="cadastrar_atividade" style="color: #ffcc00 !important; font-weight: bold;" >
								<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo chamado"></i>
							</a>							
						</div>
					</div>					
				</div>
			</div>
			<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
				
				<!--begin: Datatable -->
				<table class="" id="tabela_atividades" width="100%">
					<thead>
						<tr>
							<th style="text-align: center;background: #ffcc00;">#</th>
							<th style="text-align: center;background: #ffcc00;">Contato</th>
							<th style="text-align: center;background: #ffcc00;">Telefone</th>
							<th style="text-align: center;background: #ffcc00;">Celular</th>
							<th style="text-align: center;background: #ffcc00;">Cargo</th>	
							<th style="text-align: center;background: #ffcc00;">Inicio</th>
							<th style="text-align: center;background: #ffcc00;">Ações</th>
						</tr>
					</thead>
					<tbody>					
						<?php foreach($atividades as $key => $atividade){ ?>
							<tr>
								<td style="width: 5%;text-align: center;"><?php echo $key+1; ?></td>
								<td style="width: 20%;text-align: center;"><?php echo $atividade['nome']; ?></td>
								<td style="width: 15%;text-align: center;"><?php echo $atividade['telefone']; ?></td>		
								<td style="width: 15%;text-align: center;"><?php echo $atividade['celular']; ?></td>	
								<td style="width: 10%;text-align: center;"><?php echo $atividade['cargo']; ?></td>
								<td style="width: 20%;text-align: center;">
									<?php echo date('d/m/Y H:i:s',	strtotime($atividade['dthr_atividade'])); ?>
								</td>
								<td data-field="Actions" class="m-datatable__cell" style="width: 25%;text-align: center !important;">
									<a href="#" onclick="editar_atividade('<?php echo $atividade['id']; ?>');" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" >
										<i class="la la-edit editar"></i>
									</a>									
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
	</div>		        
</div>
<div class="modal fade" id="modal_add_atividade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title" >Cadastrar Nova Atividade</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministradorComercial/inserirAtividade');?>" method="post" enctype="multipart/form-data" id="form_atividade">				
				<input type="hidden" name="prospeccao_id" value="<?=$dados['id']?>">
				<input type="hidden" id="atividade_id"   >				
				<div class="" >
						<div class="m-portlet__body">							
							<div class="form-group m-form__group row">
								<div class="col-lg-6">
									<label>Nome</label>
									<div class="m-input-icon m-input-icon--right">
										<input type="text" name="nome" class="form-control m-input" id="nome" />				
									</div>
								</div>
								<div class="col-lg-6">										
									<label>E-mail</label>
									<div class="m-input-icon m-input-icon--right">
										<input type="email" name="email" id="email" class="form-control m-input" placeholder="">
									</div>										
																			
								</div>
							</div>
							<div class="form-group m-form__group row">
								<div class="col-lg-4">
									<label>Telefone</label>
									<input type="text" id="telefone" name="telefone" class="form-control m-input" placeholder="">
								</div>
								<div class="col-lg-4">
									<label>Celular</label>
									<input type="text" id="celular" name="celular" class="form-control m-input" placeholder="" required="required">
								</div>		
								<div class="col-lg-4">
									<label>Cargo</label>
									<div class="m-input-icon m-input-icon--right">
										<select name="cargo" id="cargo" class="form-control" >
											<option value="">--- Selecione ---</option>
											<option value="Proprietário">Proprietário</option>
											<option value="Gerente">Gerente</option>
											<option value="Técnico">Técnico</option>
											<option value="Outro">Outro</option>
										</select>
									</div>
								</div>						
							</div>
							<div class="form-group m-form__group row">
								<div class="col-lg-12">
									<label>Descrição do Atendimento</label>
									<div class="m-input-icon m-input-icon--right">
										<textarea name="descricao" id="descricao" class="form-control" required="required"></textarea>
									</div>
								</div>			
							</div>							
						</div>						
					</div>
			</div>

			<div class="modal-footer">				
				<button type="submit" class="btn btn-primary" >Salvar</button>				
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
			</div>
			</form>
		</div>
	</div>
</div>
<?php if ($this->session->flashdata('retorno') == 'erro'){ 	?>
	<script type="text/javascript">
	swal(
		'Ops!',
		'Aconteceu algum problema, reveja seus dados e tente novamente!',
		'error'
		);
	</script>
	<?php unset($_SESSION['retorno']);} ?>
	<?php if ($this->session->flashdata('retorno') == 'sucesso'){ 	?>
	<script type="text/javascript"> 	
		swal({
			title: "OK!",
			text: 'Cadastro realizado com sucesso!',
			type: "success"
		}).then(function() {
	      	//window.location = base_url+'AreaAdministradorComercial/presp';
	   	}); 
	</script>	
	<?php unset($_SESSION['retorno']); } ?>
		