			<?php if($usuario_id = 4414) { ?>

				<li class="m-menu__item " >

					<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/treinamentos');?>">

						<i class="m-menu__link-icon flaticon-edit-1" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Treinamentos Técnicos WERTCO </span>

							</span>

						</span>

					</a>

				</li>

			<?php } ?>

				<li class="m-menu__item " >

					<a class="m-menu__link " href="<?php echo base_url('AreaAssistencia/editarCliente');?>">

						<i class="m-menu__link-icon la la-user"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Editar Perfil</span>

							</span>

						</span>

					</a>

				</li>

				<li class="m-menu__item" aria-haspopup="true" >

					<a  href="<?=base_url('AreaAssistencia/atualizaValidadeCartao')?>" class="m-menu__link ">

						<i class="m-menu__link-icon la la-credit-card"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Atualizar Cartão técnico</span>

							</span>

						</span>

					</a>

				</li>

				<li class="m-menu__item" aria-haspopup="true" >

					<a  href="<?=base_url('AreaAdministrador/cartaoTecnicos')?>" class="m-menu__link ">

						<i class="m-menu__link-icon la la-credit-card"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Cartões Técnicos</span>

							</span>

						</span>

					</a>

				</li>

				<li class="m-menu__item" aria-haspopup="true" >

					<a  href="<?=base_url('AreaTecnicos/geradorJson')?>" class="m-menu__link ">

						<i class="m-menu__link-icon flaticon-line-graph"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Gerador Json Tipo C</span>

							</span>

						</span>

					</a>

				</li>

				<li class="m-menu__item" aria-haspopup="true" >

					<a  href="<?=base_url('AreaTecnicos/geradorJsonRtm')?>" class="m-menu__link ">

						<i class="m-menu__link-icon flaticon-line-graph"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Gerador Json Tipo D</span>

							</span>

						</span>

					</a>

				</li>

				<li class="m-menu__item " >

					<a class="m-menu__link " href="<?php echo base_url('AreaAssistencia/index');?>">

						<i class="m-menu__link-icon la la-dashboard"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Dashboard </span>

							</span>

						</span>

					</a>

				</li>				

				<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fa fa-shopping-cart"></i>

						<span class="m-menu__link-text">

							Gestão de Empresas

						</span>

						<i class="m-menu__ver-arrow la la-angle-right"></i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="80" style="">

						<span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">

								<span class="m-menu__link">

									<span class="m-menu__link-text">

										Gestão de Empresas

									</span>

								</span>

							</li>

							

							<li class="m-menu__item" aria-haspopup="true" >

								<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/1')?>" class="m-menu__link ">

									<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>

									<span class="m-menu__link-title">

										<span class="m-menu__link-wrap">

											<span class="m-menu__link-text">Clientes</span>

										</span>

									</span>

								</a>

							</li>

							<li class="m-menu__item" aria-haspopup="true" >

								<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/4') ?>" class="m-menu__link ">

									<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>

									<span class="m-menu__link-title">

										<span class="m-menu__link-wrap">

											<span class="m-menu__link-text">Técnicos</span>

										</span>

									</span>

								</a>

							</li>

							<li class="m-menu__item" aria-haspopup="true" >

								<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/6') ?>" class="m-menu__link ">

									<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>

									<span class="m-menu__link-title">

										<span class="m-menu__link-wrap">

											<span class="m-menu__link-text">Indicadores Nvl.1</span>

										</span>

									</span>

								</a>

							</li>

							<li class="m-menu__item" aria-haspopup="true" >

								<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/10') ?>" class="m-menu__link ">

									<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>

									<span class="m-menu__link-title">

										<span class="m-menu__link-wrap">

											<span class="m-menu__link-text">Indicadores Nvl.2</span>

										</span>

									</span>

								</a>

							</li>													

							<li class="m-menu__item" aria-haspopup="true" >

								<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/2') ?>" class="m-menu__link ">

									<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>

									<span class="m-menu__link-title">

										<span class="m-menu__link-wrap">

											<span class="m-menu__link-text">Representantes Nvl.1</span>

										</span>

									</span>

								</a>

							</li>

							<li class="m-menu__item" aria-haspopup="true" >

								<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/12') ?>" class="m-menu__link ">

									<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>

									<span class="m-menu__link-title">

										<span class="m-menu__link-wrap">

											<span class="m-menu__link-text">Representantes Nvl.2</span>

										</span>

									</span>

								</a>

							</li>											



						</ul>

					</div>

				</li>		

				<li class="m-menu__item " >

					<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/gestaoUsuarios');?>">

						<i class="m-menu__link-icon fa fa-users"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Gestão de Usuários</span>

							</span>

						</span>

					</a>

				</li>

				<li class="m-menu__item" aria-haspopup="true" >

					<a  href="<?=base_url('AreaAdministrador/chamados/1')?>" class="m-menu__link ">

						<i class="m-menu__link-icon la la-phone"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Chamados Abertos</span>

							</span>

						</span>

					</a>

				</li>

				<li class="m-menu__item" aria-haspopup="true" >

					<a  href="<?=base_url('AreaAdministrador/chamados/2')?>" class="m-menu__link ">

						<i class="m-menu__link-icon la la-phone"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Chamados Fechados</span>

							</span>

						</span>

					</a>

				</li>	

				<li class="m-menu__item" aria-haspopup="true" >

					<a  href="<?=base_url('AreaAdministrador/ordensPagto')?>" class="m-menu__link ">

						<i class="m-menu__link-icon la la-money"></i>

						<span class="m-menu__link-title">

							<span class="m-menu__link-wrap">

								<span class="m-menu__link-text">Ordens de Pagamento</span>

							</span>

						</span>

					</a>

				</li>	

				<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">

					<a href="javascript:;" class="m-menu__link m-menu__toggle">

						<i class="m-menu__link-icon fa fa-wrench"></i>

						<span class="m-menu__link-text">

							Solicitação de Peças

						</span>

						<i class="m-menu__ver-arrow la la-angle-right"></i>

					</a>

					<div class="m-menu__submenu " m-hidden-height="80" style="">

						<span class="m-menu__arrow"></span>

						<ul class="m-menu__subnav">

							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">

								<span class="m-menu__link">

									<span class="m-menu__link-text">

										Solicitação de Peças

									</span>

								</span>

							</li>

							<li class="m-menu__item " aria-haspopup="true">

								<a class="m-menu__link " href="<?php echo base_url('AreaAssistencia/solicitacaoPecas/');?>">

									<i class="m-menu__link-bullet m-menu__link-bullet--dot">

										<span></span>

									</i>

									<span class="m-menu__link-text">Pendentes </span>

								</a>

							</li>					

							<li class="m-menu__item " aria-haspopup="true">

								<a class="m-menu__link " href="<?php echo base_url('AreaAssistencia/solicitacaoPecasCompletas/');?>">

									<i class="m-menu__link-bullet m-menu__link-bullet--dot">

										<span></span>

									</i>

									<span class="m-menu__link-text">Fechadas </span>

								</a>

							</li>				

						</ul>

					</div>

				</li>

			

			<li class="m-menu__item " aria-haspopup="true" >

				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/pedidos/1');?>">

					<i class="m-menu__link-icon fa fa-ticket"></i>

					<span class="m-menu__link-title">

						<span class="m-menu__link-wrap">

							<span class="m-menu__link-text">Pedidos - OP's Em andamento</span>

						</span>

					</span>

				</a>

			</li>

			<li class="m-menu__item " aria-haspopup="true" >

				<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/pedidos/2');?>">

					<i class="m-menu__link-icon fa fa-ticket"></i>

					<span class="m-menu__link-title">

						<span class="m-menu__link-wrap">

							<span class="m-menu__link-text">Pedidos - OP's - Concluídos</span>

						</span>

					</span>

				</a>

			</li>

			<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">

				<a href="javascript:;" class="m-menu__link m-menu__toggle">

					<i class="m-menu__link-icon flaticon-line-graph"></i>

					<span class="m-menu__link-text">

						Relatórios

					</span>

					<i class="m-menu__ver-arrow la la-angle-right"></i>

				</a>

				<div class="m-menu__submenu " m-hidden-height="80" style="">

					<span class="m-menu__arrow"></span>

					<ul class="m-menu__subnav">

						<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">

							<span class="m-menu__link">

								<span class="m-menu__link-text">

									Relatórios

								</span>

							</span>

						</li>

						<li class="m-menu__item " aria-haspopup="true">

							<a href="<?=base_url('AreaAdministrador/relatorioChamadosAbertos')?>" class="m-menu__link ">

								<i class="m-menu__link-bullet m-menu__link-bullet--dot">

									<span></span>

								</i>

								<span class="m-menu__link-text">

									Chamados

								</span>

							</a>

						</li>

						<li class="m-menu__item " aria-haspopup="true">

							<a href="<?=base_url('AreaAdministrador/relatorioChamadosDefeitoCausaSolucao')?>" class="m-menu__link ">

								<i class="m-menu__link-bullet m-menu__link-bullet--dot">

									<span></span>

								</i>

								<span class="m-menu__link-text">

									Chamados - Defeitos, causas e Soluções

								</span>

							</a>

						</li>

						<li class="m-menu__item " aria-haspopup="true">

							<a href="<?=base_url('AreaAdministrador/relatorioOrdemPagto')?>" class="m-menu__link ">

								<i class="m-menu__link-bullet m-menu__link-bullet--dot">

									<span></span>

								</i>

								<span class="m-menu__link-text">

									Ordens de pagamento

								</span>

							</a>

						</li>

						<li class="m-menu__item " aria-haspopup="true">

							<a href="<?=base_url('AreaAdministrador/relatorioChamadosCliente')?>" class="m-menu__link ">

								<i class="m-menu__link-bullet m-menu__link-bullet--dot">

									<span></span>

								</i>

								<span class="m-menu__link-text">

									Chamados por Cliente

								</span>

							</a>

						</li>		

						<?php if($usuario_id == 4414){	?>

							<li class="m-menu__item " aria-haspopup="true">

								<a class="m-menu__link" href="<?php echo base_url('AreaQualidade/relatorioChamados');?>">

									<i class="m-menu__link-bullet m-menu__link-bullet--dot">

										<span></span>

									</i>

									<span class="m-menu__link-title">

										<span class="m-menu__link-wrap">

											<span class="m-menu__link-text">Chamados Gestão</span>

										</span>

									</span>

								</a>

							</li>

						<?php } ?>	

						<?php if($usuario_id == 4414){	?>

							<li class="m-menu__item " aria-haspopup="true">

								<a class="m-menu__link" href="<?php echo base_url('AreaQualidade/relatorioMediaChamados');?>">

									<i class="m-menu__link-bullet m-menu__link-bullet--dot">

										<span></span>

									</i>

									<span class="m-menu__link-title">

										<span class="m-menu__link-wrap">

											<span class="m-menu__link-text">Chamados Tempo Médio</span>

										</span>

									</span>

								</a>

							</li>

						<?php } ?>									

						<li class="m-menu__item" aria-haspopup="true" >

							<a  href="<?=base_url('AreaAdministrador/relatorioOrcamentos')?>" class="m-menu__link ">

								<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>

								<span class="m-menu__link-title">

									<span class="m-menu__link-wrap">

										<span class="m-menu__link-text">Orçamentos</span>

									</span>

								</span>

							</a>

						</li>

						<li class="m-menu__item" aria-haspopup="true" >

							<a  href="<?=base_url('AreaAdministrador/relatorioTecnicosRegiao')?>" class="m-menu__link ">

								<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>

								<span class="m-menu__link-title">

									<span class="m-menu__link-wrap">

										<span class="m-menu__link-text">Técnicos por Região</span>

									</span>

								</span>

							</a>

						</li>

						<li class="m-menu__item" aria-haspopup="true" >

							<a  href="<?=base_url('AreaAdministrador/relatorioChamadosOps')?>" class="m-menu__link ">

								<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>

								<span class="m-menu__link-title">

									<span class="m-menu__link-wrap">

										<span class="m-menu__link-text">Chamados x Ops</span>

									</span>

								</span>

							</a>

						</li>
						

					</ul>

				</div>

			</li>		

<li class="m-menu__item " >

	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/mapaUsuarios');?>">

		<i class="m-menu__link-icon la la-map-marker"></i>

		<span class="m-menu__link-title">

			<span class="m-menu__link-wrap">

				<span class="m-menu__link-text">Mapa Técnicos/Clientes</span>

			</span>

		</span>

	</a>

</li>			

<li class="m-menu__item" aria-haspopup="true" >

	<a  href="<?=base_url('AreaAdministrador/garantia')?>" class="m-menu__link ">

		<i class="m-menu__link-icon la la-certificate"></i>

		<span class="m-menu__link-title">

			<span class="m-menu__link-wrap">

				<span class="m-menu__link-text">Gestão de Garantias</span>

			</span>

		</span>

	</a>

</li>