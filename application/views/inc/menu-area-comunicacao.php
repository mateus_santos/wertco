<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaFinanceiro/editarCliente');?>">
		<i class="m-menu__link-icon la la-user"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " aria-haspopup="true" >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/pedidos/1');?>">
		<i class="m-menu__link-icon fa fa-ticket"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Pedidos - OP's</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('AreaAdministrador/programacaoProducao')?>" class="m-menu__link ">
		<i class="m-menu__link-icon la la-cogs"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Programação</span>
			</span>
		</span>
	</a>
</li>