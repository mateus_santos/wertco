<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaFinanceiro/editarCliente');?>">
		<i class="m-menu__link-icon la la-user"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>	
<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
	<a href="javascript:;" class="m-menu__link m-menu__toggle">
		<i class="m-menu__link-icon fa fa-shopping-cart"></i>
		<span class="m-menu__link-text">
			Gestão de Empresas
		</span>
		<i class="m-menu__ver-arrow la la-angle-right"></i>
	</a>
	<div class="m-menu__submenu " m-hidden-height="80" style="">
		<span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
				<span class="m-menu__link">
					<span class="m-menu__link-text">
						Gestão de Empresas
					</span>
				</span>
			</li>
			
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/1')?>" class="m-menu__link ">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Clientes</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/4') ?>" class="m-menu__link ">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Técnicos</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/6') ?>" class="m-menu__link ">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Indicadores Nvl.1</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/10') ?>" class="m-menu__link ">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Indicadores Nvl.2</span>
						</span>
					</span>
				</a>
			</li>													
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/2') ?>" class="m-menu__link ">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Representantes Nvl.1</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/12') ?>" class="m-menu__link ">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Representantes Nvl.2</span>
						</span>
					</span>
				</a>
			</li>
		</ul>
	</div>
</li>
<li class="m-menu__item " aria-haspopup="true" >
	<a class="m-menu__link " href="<?php echo base_url('AreaExpedicao/transportadoras');?>">
		<i class="m-menu__link-icon fa fa-truck"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Transportadoras</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " aria-haspopup="true" >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/pedidos/1');?>">
		<i class="m-menu__link-icon fa fa-ticket"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Pedidos Em andamento</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " aria-haspopup="true" >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/pedidos/2');?>">
		<i class="m-menu__link-icon fa fa-ticket"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Pedidos Concluídos</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
	<a href="javascript:;" class="m-menu__link m-menu__toggle">
		<i class="m-menu__link-icon fa fa-wrench"></i>
		<span class="m-menu__link-text">
			Solicitação de Peças
		</span>
		<i class="m-menu__ver-arrow la la-angle-right"></i>
	</a>
	<div class="m-menu__submenu " m-hidden-height="80" style="">
		<span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
				<span class="m-menu__link">
					<span class="m-menu__link-text">
						Solicitação de Peças
					</span>
				</span>
			</li>
			<li class="m-menu__item " aria-haspopup="true">
				<a class="m-menu__link " href="<?php echo base_url('AreaAssistencia/solicitacaoPecas/');?>">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						<span></span>
					</i>
					<span class="m-menu__link-text">Pendentes </span>
				</a>
			</li>					
			<li class="m-menu__item " aria-haspopup="true">
				<a class="m-menu__link " href="<?php echo base_url('AreaAssistencia/solicitacaoPecasCompletas/');?>">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						<span></span>
					</i>
					<span class="m-menu__link-text">Fechadas </span>
				</a>
			</li>				
		</ul>
	</div>
</li>
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('AreaAdministrador/downloads')?>" class="m-menu__link ">
		<i class="m-menu__link-icon flaticon-download"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gestão de Downloads</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('AreaAdministrador/programacaoProducao')?>" class="m-menu__link ">
		<i class="m-menu__link-icon la la-cogs"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Programação</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
	<a href="javascript:;" class="m-menu__link m-menu__toggle">
		<i class="m-menu__link-icon flaticon-line-graph"></i>
		<span class="m-menu__link-text">
			Relatórios
		</span>
		<i class="m-menu__ver-arrow la la-angle-right"></i>
	</a>
	<div class="m-menu__submenu " m-hidden-height="80" style="">
		<span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
				<span class="m-menu__link">
					<span class="m-menu__link-text">
						Relatórios
					</span>
				</span>
			</li>
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/relatorioPedidos')?>" class="m-menu__link ">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						<span></span>
					</i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Pedidos</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item " aria-haspopup="true">
				<a href="<?=base_url('AreaAdministrador/relatorioChamadosAbertos')?>" class="m-menu__link ">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						<span></span>
					</i>
					<span class="m-menu__link-text">
						Chamados
					</span>
				</a>
			</li>										
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/relatorioOrcamentos')?>" class="m-menu__link ">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						<span></span>
					</i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Orçamentos</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/relatorioTecnicosRegiao')?>" class="m-menu__link ">
					<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						<span></span>
					</i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Técnicos por Região</span>
						</span>
					</span>
				</a>
			</li>
		</ul>
	</div>
</li>	