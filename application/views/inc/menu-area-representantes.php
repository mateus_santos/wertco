<li class="m-menu__item "	>
	<a class="m-menu__link " href="<?php echo base_url('AreaRepresentantes/editarCliente');?>">
		<i class="m-menu__link-icon la la-user"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " aria-haspopup="true" >
	<a class="m-menu__link " href="<?php echo base_url('AreaRepresentantes/orcamentos');?>">
		<i class="m-menu__link-icon fa fa-shopping-cart"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Orçamentos</span>
			</span>
		</span>
	</a>
</li>			
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaRepresentantes/gestaoFuncionarios');?>">
		<i class="m-menu__link-icon la la-users"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gestão de Funcionários	</span>
			</span>
		</span>
	</a>
</li>