<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaFinanceiro/editarCliente');?>">
		<i class="m-menu__link-icon la la-user"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>	
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('AreaTecnicos/geradorJson')?>" class="m-menu__link ">
		<i class="m-menu__link-icon flaticon-line-graph"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gerador JSON</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaQualidade/relatorioChamados');?>">
		<i class="m-menu__link-icon la la-area-chart"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório de Chamados</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link" href="<?php echo base_url('AreaQualidade/relatorioMediaChamados');?>">
		<i class="m-menu__link-icon la la-area-chart"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Chamados Tempo Médio</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " aria-haspopup="true" >
	<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/pedidos/1');?>">
		<i class="m-menu__link-icon fa fa-ticket"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Pedidos - OP's</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " aria-haspopup="true" >
	<a class="m-menu__link " href="<?php echo base_url('AreaQualidade/rastreabilidade');?>">
		<i class="m-menu__link-icon la la-map-marker"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Rastreabilidade</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaQualidade/relatorioRastreabilidade');?>">
		<i class="m-menu__link-icon la la-map-marker"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Relatório de Rastreabilidade</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('AreaAdministrador/relatorioClientesInadimplentes')?>" class="m-menu__link ">
		<i class="m-menu__link-icon la la-usd"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Clientes Inadimplentes</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item" aria-haspopup="true" >
	<a class="m-menu__link " href="<?php echo base_url('AreaSuprimentos/relatorioPrevisaoVendas');?>">
		<i class="m-menu__link-icon flaticon-line-graph"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Previsão de Vendas</span>
			</span>
		</span>
	</a>			
</li>
<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
	<a href="javascript:;" class="m-menu__link m-menu__toggle">
		<i class="m-menu__link-icon la la-cogs"></i>
		<span class="m-menu__link-text">
			Produção
		</span>
		<i class="m-menu__ver-arrow la la-angle-right"></i>
	</a>
	<div class="m-menu__submenu " m-hidden-height="80" style="">
		<span class="m-menu__arrow"></span>
		<ul class="m-menu__subnav">
			<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
				<span class="m-menu__link">
					<span class="m-menu__link-text">
						Produção
					</span>
				</span>
			</li>
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/programacaoProducaoRegras')?>" class="m-menu__link ">
					<i class="m-menu__link-icon la la-lock"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Regras Produção</span>
						</span>
					</span>
				</a>
			</li>									
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/programacaoProducao')?>" class="m-menu__link ">
					<i class="m-menu__link-icon la la-cogs"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Programação</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/buscaNrSerieOpPedido')?>" class="m-menu__link ">
					<i class="m-menu__link-icon fa fa-barcode"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Nr. Série/Op's/Pedidos</span>
						</span>
					</span>
				</a>
			</li>
			<li class="m-menu__item" aria-haspopup="true" >
				<a  href="<?=base_url('AreaAdministrador/relatorioProgramacaoSemanal')?>" class="m-menu__link ">
					<i class="m-menu__link-icon flaticon-line-graph"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">Relatório Programação</span>
						</span>
					</span>
				</a>
			</li>
		</ul>
	</div>
</li>	