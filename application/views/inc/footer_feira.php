<!-- begin::Footer -->
</div>
</div>
<footer class="m-grid__item		m-footer ">
	<div class="m-container m-container--fluid m-container--full-height m-page__container">
		<div class="m-stack m-stack--flex-tablet-and-dbile m-stack--ver m-stack--desktop">
			<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
				<span class="m-footer__copyright">
					2018 &copy; Wertco by <a href="#" class="m-link">P&D Companytec</a>
				</span>
			</div>
		</div>
	</div>
</footer>
<!-- end::Footer -->		

<!-- end:: Page -->	    
<!-- begin::Scroll Top -->
<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
	<i class="la la-arrow-up"></i>
</div>

</div>
<!-- begin::Quick Nav -->
<?php
	// carregar js particular de cada View
$js = explode('/', $view); 
$loadjs = $js[count($js) - 1].'.js';
?>
<script src="<?=base_url('bootstrap/assets/demo/default/custom/components/utils/jquery.js')?>" type="text/javascript"></script>	
<script src="<?=base_url('bootstrap/js/bootstrap.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('bootstrap/js/sweetalert.js')?>" type="text/javascript"></script>


<!--end::Page Vendors -->
<!--begin::Page Snippets -->
<script src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>" type="text/javascript"></script>		
<script src="<?=base_url('bootstrap/assets/demo/default/custom/components/utils/jquery-ui.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('bootstrap/js/plugins/jquery.blockui.js')?>" type="text/javascript"></script>
<script src="<?=base_url('bootstrap/js/jquery.ddslick.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('bootstrap/js/'.$loadjs)?>" type="text/javascript"></script>


<!--end::Base Scripts -->    
</body>
<!-- end::Body -->
</html>