<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaClientes/editarCliente');?>">
		<i class="m-menu__link-icon la la-user"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaClientes/orcamentosCli/');?>">
		<i class="m-menu__link-icon fa fa-shopping-cart"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Orçamentos</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaClientes/chamados/');?>">
		<i class="m-menu__link-icon la la-gears"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Abrir Chamado</span>
			</span>
		</span>
	</a>
</li>