<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('AreaAdministrador/downloads')?>" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-download"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Gestão de Downloads</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('AreaAdministrador/cartaoTecnicos')?>" class="m-menu__link ">
						<i class="m-menu__link-icon la la-credit-card"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Cartões Técnicos</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('AreaTecnicos/atualizaValidadeCartao')?>" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-calendar-1"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Atualizar Validade Cartão</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item " aria-haspopup="true" >
					<a data-toggle="modal" data-target="#m_modal_6" class="m-menu__link ">
						<i class="m-menu__link-icon fa fa-key"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Alterar data CPU</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('AreaTecnicos/geradorJson')?>" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-line-graph"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Gerador JSON</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item " aria-haspopup="true" >
					<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/pedidos/1');?>">
						<i class="m-menu__link-icon fa fa-ticket"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Pedidos - OP's - Em andamento</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item " aria-haspopup="true" >
					<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/pedidos/2');?>">
						<i class="m-menu__link-icon fa fa-ticket"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Pedidos - OP's - Concluídos</span>
							</span>
						</span>
					</a>
				</li>							
				<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
					<a href="javascript:;" class="m-menu__link m-menu__toggle">
						<i class="m-menu__link-icon fa fa-wrench"></i>
						<span class="m-menu__link-text">
							Assistência Técnica
						</span>
						<i class="m-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="m-menu__submenu " m-hidden-height="80" style="">
						<span class="m-menu__arrow"></span>
						<ul class="m-menu__subnav">
							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
								<span class="m-menu__link">
									<span class="m-menu__link-text">
										Assistência Técnica
									</span>
								</span>
							</li>
							
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/chamados')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-phone"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Chamados</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/despesas')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-dollar"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Despesas</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/dashboardChamados')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-dashboard"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Dashboard</span>
										</span>
									</span>
								</a>
							</li>	
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/pecas')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-dashboard"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Solicitação de Peças</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/garantia')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-certificate"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Gestão de Garantias</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" data-menu-submenu-toggle="hover">
								<a href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-line-graph">
										<span></span>
									</i>
									<span class="m-menu__link-text">
										Relatórios
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu " style="">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item " aria-haspopup="true">
											<a href="<?=base_url('AreaAdministrador/relatorioChamadosAbertos')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Chamados
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true">
											<a href="<?=base_url('AreaAdministrador/relatorioChamadosDefeitoCausaSolucao')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Chamados - Defeitos, causas e Soluções
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/relatorioTecnicosRegiao')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Técnicos por Região</span>
													</span>
												</span>
											</a>
										</li>										
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</li>