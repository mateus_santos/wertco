<!DOCTYPE html>
<html lang="en" >
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<!-- /Added by HTTrack -->
<head>
	<meta charset="utf-8" />
	<title>WERTCO - <?php echo $title;?></title>
	<meta name="description" content="Default form examples">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />
	
	<!--begin::Web font -->
	<script src="<?=base_url('bootstrap/assets/webfont/1.6.16/webfont.js')?>"></script>
	<script>
		WebFont.load({
			google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
	<script src="<?=base_url('bootstrap/assets/demo/default/custom/components/utils/jquery.js')?>" type="text/javascript"></script>	
	
	<script src="<?=base_url('bootstrap/js/plugins/bootstrap-datepicker.js')?>" type="text/javascript"></script>

	<script src="<?=base_url('bootstrap/assets/vendors/base/vendors.bundle.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/assets/demo/default/base/scripts.bundle.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/assets/demo/default/custom/components/datatables/base/html-table.js')?>" 	type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/assets/demo/default/custom/components/datatables/datatables.min.js')?>" 	type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/assets/demo/default/custom/components/base/sweetalert2.js')?>" type="text/javascript"></script>	
	<script src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>" type="text/javascript"></script>
	
	
	<script src="<?=base_url('bootstrap/js/plugins/ion.rangeSlider.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/js/plugins/jquery.blockui.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/js/plugins/tether.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/js/plugins/shepherd.min.js')?>" type="text/javascript"></script>

	<script src="<?=base_url('bootstrap/assets/vendors/base/jszip.min.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/assets/vendors/base/FileSaver.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/assets/vendors/base/script.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/assets/vendors/base/googled3193f02.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/assets/demo/default/custom/components/forms/validation/form-widgets.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('bootstrap/assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js')?>" type="text/javascript"></script>

	<script src="<?=base_url('bootstrap/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')?>" type="text/javascript"></script>
	<!--end::Page Vendors -->  
	<!--begin::Page Snippets -->
	<script src="<?=base_url('bootstrap/assets/app/js/dashboard.js')?>" type="text/javascript"></script>

	<!--begin::Base Styles -->  
	<link href="<?=base_url('bootstrap/assets/vendors/base/vendors.bundle.css')?>" rel="stylesheet" type="text/css" />	
	<link href="<?=base_url('bootstrap/assets/demo/default/base/style.bundle.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('bootstrap/assets/vendors/base/datatable.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('bootstrap/assets/demo/default/base/jquery-ui.min.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('bootstrap/css/shepherd-theme-arrows.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('bootstrap/css/shepherd-theme-square-dark.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('bootstrap/css/bootstrap-slider.css')?>" rel="stylesheet" type="text/css" />
	<link href="<?=base_url('bootstrap/css/programacao-semanal.css')?>" rel="stylesheet" type="text/css" />

	<script src="<?=base_url('bootstrap/js/jquery-editable-select.min.js')?>"></script>
	<script src="<?=base_url('bootstrap/assets/demo/default/custom/components/utils/jquery-ui.min.js')?>" type="text/javascript"></script>

	<link href="<?=base_url('bootstrap/css/jquery-editable-select.min.css')?>" rel="stylesheet">
	
	<!--end::Base Styles -->
	<link rel="shortcut icon" href="<?=base_url('bootstrap/assets/demo/default/media/img/logo/favicon1.png')?>" />	
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- BEGIN: Header -->
		<header class="m-grid__item    m-header "  data-minimize-offset="200" data-minimize-mobile-offset="200" >
			<div class="m-container m-container--fluid m-container--full-height">
				<div class="m-stack m-stack--ver m-stack--desktop">
					<!-- BEGIN: Brand -->
					<div class="m-stack__item m-brand  m-brand--skin-dark ">
						<div class="m-stack m-stack--ver m-stack--general">
							<div class="m-stack__item m-stack__item--middle m-brand__logo">								
								<img alt="" src="<?=base_url('bootstrap/assets/demo/default/media/img/logo/wertcofundoescuro.png')?>"/>								
							</div>
							<div class="m-stack__item m-stack__item--middle m-brand__tools">
								<!-- BEGIN: Left Aside Minimize Toggle -->
								<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block 
								">
								<span></span>
							</a>
							<!-- END -->
							<!-- BEGIN: Responsive Aside Left Menu Toggler -->
							<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
								<span></span>
							</a>
							<!-- END -->
						</div>
					</div> 
				</div>
				<!-- END: Brand -->			
				<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
					<!-- BEGIN: Horizontal Menu -->

					<!-- END: Horizontal Menu -->	
					<!-- BEGIN: Topbar -->
					<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general" style="margin-right: 15%;">
						<div class="m-stack__item m-topbar__nav-wrapper">
							<ul class="m-topbar__nav m-nav m-nav--inline">
								<?php if(count($alertas) > 0 ){ ?>
								<li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center 	m-dropdown--mobile-full-width" data-dropdown-toggle="click" data-dropdown-persistent="true">
									<a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
										<span class="m-nav__link-badge m-badge m-badge--dot m-badge--dot-small m-badge--danger"></span>
										<span class="m-nav__link-icon">
											<i class="flaticon-music-2"></i>
										</span>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__header m--align-center" style="background: url(<?=base_url('bootstrap/assets/app/media/img/misc/notification_bg2.png')?>); background-size: cover;">
												<span class="m-dropdown__header-title">
													<span class="numero_alerta" total="<?php echo $naolidos['total_novos'];?>"><?php echo $naolidos['total_novos'];?></span> 
												</span>
												<span class="m-dropdown__header-subtitle">
													Alertas de Follow Up
												</span>
											</div>
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<div class="tab-content">
														<div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel" style="background: #fff;	">
															<div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">
																<div class="m-list-timeline m-list-timeline--skin-light">
																	<div class="m-list-timeline__items">
																		<?php foreach( $alertas as $alerta ){
																			
																			?>
																		<div class="m-list-timeline__item" id="alerta_<?php echo $alerta['alerta_id']; ?>">
																			<span class="m-list-timeline__badge m-list-timeline__badge--<?php echo (strtotime($alerta['dthr_alerta']) < strtotime(date('Y-m-d')) ) ? 'danger' : 'warning'; ?>"></span>
																			<span class="m-list-timeline__text" title="<?php echo $alerta['andamento']; ?>" style="cursor: pointer; font-size: 0.9em;color: <?php echo (strtotime($alerta['dthr_alerta']) < strtotime(date('Y-m-d'))) ? '#f4516c;' : '#999;'; ?>">
																				<?php echo  (strlen($alerta['andamento']) > 60) ? substr($alerta['andamento'], 0, 60).'...' : $alerta['andamento']; ?>
																			</span>																			

																			<span class="m-list-timeline__time" style="width: 50px !important;">
																				<a href="<?php echo base_url('AreaAdministrador/visualizarOrcamento/'.$alerta['orcamento_id'].'/'.$alerta['orcamento_id']); ?>" style="color: <?php echo (strtotime($alerta['dthr_alerta']) < strtotime(date('Y-m-d')) ) ? '#f4516c' : '#999'; ?> !important;" >
																					<?php echo '#'.$alerta['orcamento_id']; ?>
																					
																				</a>
																			</span>
																			<span class="m-list-timeline__time" style="width: 50px !important;font-size: 0.9em;color: <?php echo (strtotime($alerta['dthr_alerta']) < strtotime(date('Y-m-d')) ) ? '#f4516c;' : 'ffcc00;'; ?>">
																				<?php
																					if($this->session->userdata('tipo_acesso') == 'administrador geral'){ 
																						echo $alerta['nome'].'<br/>';
																					}	
																					echo $alerta['dthr_alerta_f']; 
																				?>									
																			</span>
																			<?php if($this->session->userdata('tipo_acesso') != 'administrador geral'){ ?>
																				<span class="m-list-timeline__time" style="width: 50px !important;padding-left: 25px !important;">
																					<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="width: 25px;height: 25px;" data-toggle="tooltip" data-placement="top" title="Confirmar Visualização" onclick="confirma_visualizacao(<?php echo $alerta['alerta_id']; ?>);">
																						<i class="la la-check" style="font-size: 0.8em;color: #000;"></i>
																					</a>																			
																				</span>
																		<?php } ?>
																		</div>
																	<?php } ?>																		
																	</div>
																</div>
															</div>
														</div>														
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							<?php } ?>

								<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
									<a href="#" class="m-nav__link m-dropdown__toggle">
										<span class="m-topbar__userpic">
											<?php if ($this->session->foto == '') $foto = 'bootstrap/assets/app/media/img/users/Avatar.png';else $foto = $this->session->foto;?>
											<img src="<?=base_url($foto);?>" class="m--img-rounded m--marginless m--img-centered" alt=""/>
										</span>
										<span class="m-topbar__username m--hide">
											Nick
										</span>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__header m--align-center" style="background: url(<?=base_url('bootstrap/assets/app/media/img/misc/user.png')?>); background-size: cover;">
												<div class="m-card-user m-card-user--skin-dark">
													<div class="m-card-user__pic">

														<img src="<?=base_url($foto);?>" class="m--img-rounded m--marginless" alt=""/>
													</div>
													<div class="m-card-user__details">
														<span class="m-card-user__name m--font-weight-500">
															<?= $this->session->nome ?>
														</span>
														<a href="" class="m-card-user__email m--font-weight-500 m-link">
															<?= $this->session->email ?>
														</a>
													</div>
												</div>
											</div>
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav m-nav--skin-light">
														<li class="m-nav__section m--hide">
															<span class="m-nav__section-text">
																Section
															</span>
														</li>
														<?php $tipo_acesso = explode(' ',$this->session->tipo_acesso);?>
														<li class="m-nav__item">
															<a href="<?=base_url('Area'.ucfirst($tipo_acesso[0]).'/editarCliente');?>" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-profile-1"></i>
																<span class="m-nav__link-title">
																	<span class="m-nav__link-wrap">
																		<span class="m-nav__link-text">
																			Meu Perfil
																		</span>

																	</span>
																</span>
															</a>
														</li>
														<!--<li class="m-nav__item">
															<a href="header/profile.html" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">
																	Atividade
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="header/profile.html" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">
																	Mensagens
																</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<a href="header/profile.html" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">
																	FAQ
																</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="header/profile.html" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">
																	Suporte
																</span>
															</a>
														</li>-->
														<li class="m-nav__separator m-nav__separator--fit"></li>
														<li class="m-nav__item">
															<a href="<?=base_url('usuarios/loggout')?>" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
																Sair
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
								<!--<li id="m_quick_sidebar_toggle" class="m-nav__item">
									<a href="#" class="m-nav__link m-dropdown__toggle">
										<span class="m-nav__link-icon">
											<i class="flaticon-grid-menu"></i>
										</span>
									</a>
								</li>-->
							</ul>
						</div>
					</div>
					<!-- END: Topbar -->
				</div>
			</div>
		</div>
	</header>
	<!-- END: Header -->		
	<!-- begin::Body -->
	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">