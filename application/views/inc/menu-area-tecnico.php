<li class="m-menu__item " aria-haspopup="true" >
	<a data-toggle="modal" data-target="#m_modal_6" class="m-menu__link ">
		<i class="m-menu__link-icon fa fa-key"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Alterar data CPU</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('AreaTecnicos/geradorJson')?>" class="m-menu__link ">
		<i class="m-menu__link-icon flaticon-line-graph"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gerador JSON</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('AreaTecnicos/downloads')?>" class="m-menu__link ">
		<i class="m-menu__link-icon flaticon-download"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Downloads</span>
			</span>
		</span>
	</a>
</li>				
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('AreaTecnicos/atualizaValidadeCartao')?>" class="m-menu__link ">
		<i class="m-menu__link-icon flaticon-calendar-1"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Atualizar Validade Cartão</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item" aria-haspopup="true" >
	<a  href="<?=base_url('AreaTecnicos/solicitaCartaoTecnico')?>" class="m-menu__link ">
		<i class="m-menu__link-icon flaticon-calendar-1"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Solicitar Cartão</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaTecnicos/orcamentosTec/');?>">
		<i class="m-menu__link-icon fa fa-shopping-cart"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Indicar Negócio </span>
			</span>
		</span>
	</a>
</li>
<!-- RESPONSÁVEL LEGAL -->
<?php if( $subtipo_acesso == 1 ) {	?>

<!--<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaTecnicos/contrato');?>">
		<i class="m-menu__link-icon la la-pencil"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Contrato Prestação de Serviço</span>
			</span>
		</span>
	</a>
</li>	-->
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaTecnicos/gestaoFuncionarios');?>">
		<i class="m-menu__link-icon la la-users"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Gestão de Funcionários	</span>
			</span>
		</span>
	</a>
</li>

<?php } ?>
<!-- GERENTE TÉCNICO -->
<?php if( $subtipo_acesso == 2 ) { 	?>
	<li class="m-menu__item " >
		<a class="m-menu__link " href="<?php echo base_url('AreaTecnicos/gestaoFuncionarios');?>">
			<i class="m-menu__link-icon la la-users"></i>
			<span class="m-menu__link-title">
				<span class="m-menu__link-wrap">
					<span class="m-menu__link-text">Gestão de Funcionários	</span>
				</span>
			</span>
		</a>
	</li>
<?php } ?>	
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('downloads/Parceria-Comercial-Wertco-para-Indicadores_11_2021.pdf/');?>">
		<i class="m-menu__link-icon la la-file-text-o"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Política Comercial </span>
			</span>
		</span>
	</a>
</li>