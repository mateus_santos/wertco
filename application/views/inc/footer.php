<!-- begin::Footer -->
</div>
</div>
<footer class="m-grid__item		m-footer ">
	<div class="m-container m-container--fluid m-container--full-height m-page__container">
		<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
			<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
				<span class="m-footer__copyright">
					<?=date('Y')?> &copy; Wertco by <a href="#" class="m-link">P&D Companytec</a>
				</span>
			</div>
		</div>
	</div>
</footer>
<!-- end::Footer -->		

<!-- end:: Page -->	    
<!-- begin::Scroll Top -->
<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
	<i class="la la-arrow-up"></i>
</div>

</div>
<!-- begin::Quick Nav -->	
<!--begin::Base Scripts -->        
<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Altera data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<div class="form-group m-form__group">
					<label for="example_input_full_name">Tag de Mecânico</label>
					<input id="tagMecanico" type="email" maxlength="16" class="form-control m-input" placeholder="Digite a tag">
				</div>
				<div class="form-group m-form__group row">
					<label class="col-form-label col-lg-12 col-sm-12">Data da CPU*</label>
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="input-group">
							<input type="text" class="form-control m-input" name="date" placeholder="Selecione uma data" id="m_datepicker">
							<div class="input-group-append">
								<span class="input-group-text">
									<i class="la la-calendar-check-o"></i>
								</span>
							</div>
						</div>					
					</div>
				</div>
				<div class="form-group m-form__group">
					<label for="example_input_full_name">Senha gerada:</label>
					<input type="text" class="form-control m-input" id="aesSenha" disabled>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
				<button type="button" onclick="limparAes();" class="btn btn-metal">Limpar</button>
				<button type="button" onclick="geraSenha();" class="btn btn-warning">Gerar senha</button>
			</div>
		</div>
	</div>
</div>

<?php
	// carregar js particular de cada View
$js = explode('/', $view); 
$loadjs = $js[count($js) - 1].'.js';	
?>
<script src="<?=base_url('bootstrap/js/plugins/bootstrap-slider.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('bootstrap/js/plugins/chart.funnel.bundled.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('bootstrap/js/'.$loadjs)?>" type="text/javascript"></script>

<script type="text/javascript">


	$('#preset_P2').editableSelect();
	
	function geraSenha(){

		var tag = document.getElementById('tagMecanico').value;
		var data = (document.getElementById('m_datepicker').value).replace("/", "").replace("/", "");		
		if(tag.length == 16 && data.length >0){
			document.getElementById("aesSenha").value = aesResult(data,tag,1);
		}else{
			document.getElementById("aesSenha").value = "Campos inválidos";
		}
	}

	function limparAes(){
		document.getElementById("aesSenha").value = "";
		document.getElementById('m_datepicker').value = "";
		document.getElementById('tagMecanico').value = "";
	}

	$( "#Timer_Ligar" ).click(function() {
		$('#m_timepicker_modal').modal('show');
	});


	function geraTimer(){


		var ini_dom = document.getElementById("ini_dom").value.replace(":", "");
		var fim_dom = document.getElementById("fim_dom").value.replace(":", "");

		var ini_seg = document.getElementById("ini_seg").value.replace(":", "");
		var fim_seg = document.getElementById("fim_seg").value.replace(":", "");

		var ini_ter = document.getElementById("ini_ter").value.replace(":", "");
		var fim_ter = document.getElementById("fim_ter").value.replace(":", "");

		var ini_qua = document.getElementById("ini_qua").value.replace(":", "");
		var fim_qua = document.getElementById("fim_qua").value.replace(":", "");

		var ini_qui = document.getElementById("ini_qui").value.replace(":", "");
		var fim_qui = document.getElementById("fim_qui").value.replace(":", "");

		var ini_sex = document.getElementById("ini_sex").value.replace(":", "");
		var fim_sex = document.getElementById("fim_sex").value.replace(":", "");

		var ini_sab = document.getElementById("ini_sab").value.replace(":", "");
		var fim_sab = document.getElementById("fim_sab").value.replace(":", "");

		if(ini_dom.length == 3) ini_dom = '0'+ini_dom;
		if(fim_dom.length == 3) fim_dom = '0'+fim_dom;

		if(ini_dom.length == 3) ini_dom = '0'+ini_dom;
		if(fim_dom.length == 3) fim_dom = '0'+fim_dom;

		if(ini_dom.length == 3) ini_dom = '0'+ini_dom;
		if(fim_dom.length == 3) fim_dom = '0'+fim_dom;

		if(ini_dom.length == 3) ini_dom = '0'+ini_dom;
		if(fim_dom.length == 3) fim_dom = '0'+fim_dom;

		if(ini_dom.length == 3) ini_dom = '0'+ini_dom;
		if(fim_dom.length == 3) fim_dom = '0'+fim_dom;

		if(ini_dom.length == 3) ini_dom = '0'+ini_dom;
		if(fim_dom.length == 3) fim_dom = '0'+fim_dom;

		if(ini_dom.length == 3) ini_dom = '0'+ini_dom;
		if(fim_dom.length == 3) fim_dom = '0'+fim_dom;

		
		$("#Timer_Ligar").val(ini_dom+fim_dom+ini_seg+fim_seg+ini_ter+fim_ter+ini_qua+fim_qua+ini_qui+fim_qui+ini_sex+fim_sex+ini_sab+fim_sab);





	}

	function confirma_visualizacao(alerta_id){
		$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
		var fl_visualizado = 1;
		
		$.ajax({
            method: "POST",
            url: base_url+'areaAdministrador/confirmaVisualizacaoAlerta',
            async: true,
            data: { alerta_id    :   alerta_id }
            }).done(function( data ) {
                var dados = $.parseJSON(data);
                if(dados.retorno == 'sucesso'){
                    $('#alerta_'+alerta_id).fadeOut('slow');
                    var total = $('.numero_alerta').attr('total');
                    $('.numero_alerta').text(total-1);
                }else{
                    alert('erro!');
                }

       	}); 
	}

</script>

<!--end::Base Scripts -->   
</body>
<!-- end::Body -->
</html>