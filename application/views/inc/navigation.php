<!-- BEGIN: Left Aside -->
		<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
		<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
			<!-- BEGIN: Aside Menu -->
			<div 
			id="m_ver_menu" 
			class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
			data-menu-vertical="true"
			data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
			>
			
			<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			<?php 

				if($tipo_acesso == "tecnicos"){	
					include('menu-area-tecnico.php');
					
				}elseif($tipo_acesso == "administrador comercial"){	
					include('menu-area-administrador-comercial.php');

				}elseif($tipo_acesso == "administrador tecnico"){	
					include('menu-area-administrador-tecnico.php');

				}elseif($tipo_acesso == "financeiro"){
					include('menu-area-financeiro.php');

				}elseif($tipo_acesso == "administrador geral"){
					include('menu-area-administrador-geral.php');

				}elseif($tipo_acesso == "clientes"){
					include('menu-area-cliente.php');
								
				}elseif($tipo_acesso == "indicadores3"){
					include('menu-area-indicadores3.php');

				}elseif($tipo_acesso == "representantes"){ 
					include('menu-area-representantes.php');
				
				}elseif($tipo_acesso == "representantes2"){
					include('menu-area-representantes2.php');

				}elseif($tipo_acesso == "indicadores2"){ 
					include('menu-area-indicadores2.php');

				}elseif($tipo_acesso == "indicadores"){	
					include('menu-area-indicadores.php');

				}elseif($tipo_acesso == "assistencia tecnica"){	
					include('menu-area-assistencia-tecnica.php');

				}elseif($tipo_acesso == "almoxarifado"){
					include('menu-area-almoxarifado.php');

				}elseif($tipo_acesso == "feira"){ 
					include('menu-area-feira.php');
				
				}elseif($tipo_acesso == "qualidade"){ 
					include('menu-area-qualidade.php');

				}elseif($tipo_acesso == "suprimentos"){ 
					include('menu-area-suprimentos.php');

				}elseif($tipo_acesso == "expedicao"){ 
					include('menu-area-expedicao.php');
					
				}elseif($tipo_acesso == "suporte"){ 
					include('menu-area-suporte.php');

				}elseif($tipo_acesso == "comunicacao"){ 
					include('menu-area-comunicacao.php');
					
				}
			?>

			</ul>
			
		</div>
		<!-- END: Aside Menu -->
	</div>
	<!-- END: Left Aside -->							
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->