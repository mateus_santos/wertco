	<li class="m-menu__item " >
					<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/editarCliente');?>">
						<i class="m-menu__link-icon la la-user" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Editar Perfil</span>
							</span>
						</span>
					</a>
				</li>	
				<li class="m-menu__item " >
					<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/');?>">
						<i class="m-menu__link-icon la la-dashboard" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Dashboard</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item " >
					<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/treinamentos');?>">
						<i class="m-menu__link-icon flaticon-edit-1" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Treinamentos Técnicos WERTCO </span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item " >
					<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/mapaClientesIndRep');?>">
						<i class="m-menu__link-icon flaticon-map-location" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Mapa </span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
					<a href="javascript:;" class="m-menu__link m-menu__toggle">
						<i class="m-menu__link-icon la la-shopping-cart" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
						<span class="m-menu__link-text">
							Comercial
						</span>
						<i class="m-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="m-menu__submenu " m-hidden-height="80" style="">
						<span class="m-menu__arrow"></span>
						<ul class="m-menu__subnav">
							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
								<span class="m-menu__link">
									<span class="m-menu__link-text">
										Comercial
									</span>
								</span>
							</li>
						<?php if($usuario_id == 12 || $usuario_id == 2 || $usuario_id == 66){ ?>
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/desempenhoComercial');?>">
									<i class="m-menu__link-icon fa fa-line-chart"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Desempenho Comercial</span>
										</span>
									</span>
								</a>
							</li>
						<?php } ?>							
							<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
								<a href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa fa-shopping-cart"></i>
									<span class="m-menu__link-text">
										Gestão de Empresas
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu " m-hidden-height="80" style="">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Gestão de Empresas
												</span>
											</span>
										</li>
										
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/1')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Clientes</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/4') ?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Técnicos</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/6') ?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Indicadores Nvl.1</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/10') ?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Indicadores Nvl.2</span>
													</span>
												</span>
											</a>
										</li>													
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/2') ?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Representantes Nvl.1</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/gestaoEmpresas/12') ?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Representantes Nvl.2</span>
													</span>
												</span>
											</a>
										</li>											

									</ul>
								</div>
							</li>			
							<li class="m-menu__item " >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/gestaoUsuarios');?>">
									<i class="m-menu__link-icon fa fa-users"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Gestão de Usuários</span>
										</span>
									</span>
								</a>
							</li>													
							
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/zonaAtuacao');?>">
									<i class="m-menu__link-icon fa fa-map"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Zonas de Atuação</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/carteiraEquipe');?>">
									<i class="m-menu__link-icon fa fa-credit-card-alt"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Gestão de Carteiras</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/produtos');?>">
									<i class="m-menu__link-icon fa fa-product-hunt"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Gestão de Produtos</span>
										</span>
									</span>
								</a>
							</li>						
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/fretes');?>">
									<i class="m-menu__link-icon la la-truck"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Fretes</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/entregas');?>">
									<i class="m-menu__link-icon flaticon-open-box"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Entregas</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/formaPagto');?>">
									<i class="m-menu__link-icon la la-cc-mastercard"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Forma Pgto.</span>
										</span>
									</span>
								</a>
							</li>	
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/icms');?>">
									<i class="m-menu__link-icon fa fa-percent"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">ICMS</span>
										</span>
									</span>
								</a>
							</li>			
							<!--<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/orcamentos');?>">
									<i class="m-menu__link-icon fa fa-shopping-cart"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Orçamentos</span>
										</span>
									</span>
								</a>
							</li>-->
							<li class="m-menu__item " >
								<a class="m-menu__link " href="<?php echo base_url('downloads/Parceria-Comercial-Wertco-para-Indicadores_10_2020.pdf');?>">
									<i class="m-menu__link-icon fa fa-shopping-cart"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Política Indicadores </span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
								<a href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa fa-edit"></i>
									<span class="m-menu__link-text">
										Prospecções
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu " m-hidden-height="80" style="">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Prospecções
												</span>
											</span>
										</li>							
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/prospeccoesStatus')?>" class="m-menu__link ">
												<i class="m-menu__link-icon la la-warning"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Status Prospecção</span>
													</span>
												</span>
											</a>
										</li>	
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/prospeccoes')?>" class="m-menu__link ">
												<i class="m-menu__link-icon fa fa-edit"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Gerir Prospecções</span>
													</span>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
								<a href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon fa fa-shopping-cart"></i>
									<span class="m-menu__link-text">
										Orçamentos
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu " m-hidden-height="80" style="">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Orçamentos
												</span>
											</span>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/cadastraOrcamentos/')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Cadastrar</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/pesquisarOrcamentos')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Pesquisar</span>
													</span>
												</span>
											</a>
										</li>
										<?php if($usuario_id == 7167){ ?>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/orcamentosStatus/12')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Aguardando Aprovação</span>
													</span>
												</span>
											</a>
										</li>
										<?php } ?>							
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/orcamentosStatus/1')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Abertos</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/orcamentosStatus/6')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Em negociação</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/orcamentosStatus/16')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Reta Final</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/orcamentosStatus/2')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Fechados</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/orcamentosStatus/99')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Perdidos/Cancelados</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/orcamentosExpirados') ?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Expirados</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/orcamentosStatus/15')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Sem Técnico na Região</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministradorComercial/orcamentosStatus/14')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Sem Retorno</span>
													</span>
												</span>
											</a>
										</li>												
									</ul>
								</div>
							</li>
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/programacaoComercial');?>">
									<i class="m-menu__link-icon fa fa-calendar"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Programação Comercial</span>
										</span>
									</span>
								</a>
							</li>	
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/pedidos/1');?>">
									<i class="m-menu__link-icon fa fa-ticket"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Pedidos - OP's - Em andamento</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/pedidos/2');?>">
									<i class="m-menu__link-icon fa fa-ticket"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Pedidos - OP's - Concluídos</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/areaAtuacaoAdm');?>">
									<i class="m-menu__link-icon la la-map-marker"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Áreas de Atuação - Representantes</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item " aria-haspopup="true" >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/faturamento');?>">
									<i class="m-menu__link-icon la la-dollar"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Faturamento</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
								<a href="javascript:;" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-line-graph"></i>
									<span class="m-menu__link-text">
										Relatórios
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu " m-hidden-height="80" style="">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Relatórios
												</span>
											</span>
										</li>										
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/relatorioOrcamentos')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Orçamentos</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/relatorioTecnicosRegiao')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Técnicos por Região</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/relatorioPedidos')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Pedidos</span>
													</span>
												</span>
											</a>
										</li>										
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/relatorioRepresentantes')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Representantes</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/relatorioHistoricoNegociacoes')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Histórico Negociações</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/relatorioClientesInadimplentes')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Clientes Inadimplentes</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a class="m-menu__link " href="<?php echo base_url('AreaSuprimentos/relatorioPrevisaoVendas');?>">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Previsão de Vendas</span>
													</span>
												</span>
											</a>			
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/previsaoVendasDetalhado');?>">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Previsão de Vendas Detalhado</span>
													</span>
												</span>
											</a>			
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/relatorioVendasModeloFiltro')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Vendas por Modelo</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/relatorioRankingParceiros')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Ranking Parceiros</span>
													</span>
												</span>
											</a>
										</li>
										
									</ul>
								</div>
							</li>			
						</ul>
					</div>
				</li>
				<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
					<a href="javascript:;" class="m-menu__link m-menu__toggle">
						<i class="m-menu__link-icon la la-gear" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
						<span class="m-menu__link-text">
							Técnico
						</span>
						<i class="m-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="m-menu__submenu " m-hidden-height="80" style="">
						<span class="m-menu__arrow"></span>
						<ul class="m-menu__subnav">
							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
								<span class="m-menu__link">
									<span class="m-menu__link-text">
										Técnico
									</span>
								</span>
							</li>
							<li class="m-menu__item " aria-haspopup="true" >
					<a data-toggle="modal" data-target="#m_modal_6" class="m-menu__link ">
						<i class="m-menu__link-icon fa fa-key"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Alterar data CPU</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('AreaTecnicos/geradorJson')?>" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-line-graph"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Gerador Json Tipo C</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('AreaTecnicos/geradorJsonRtm')?>" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-line-graph"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Gerador Json Tipo D</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('AreaAdministrador/downloads')?>" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-download"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Gestão de Downloads</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('AreaAdministrador/cartaoTecnicos')?>" class="m-menu__link ">
						<i class="m-menu__link-icon la la-credit-card"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Cartões Técnicos</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('AreaAdministrador/solicitacoesCartaoTecnico')?>" class="m-menu__link ">
						<i class="m-menu__link-icon la la-credit-card"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Solicitações de Cartão Técnico</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item" aria-haspopup="true" >
					<a  href="<?=base_url('AreaAdministrador/atualizaValidadeCartao')?>" class="m-menu__link ">
						<i class="m-menu__link-icon la la-credit-card"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Atualizar Cartão técnico</span>
							</span>
						</span>
					</a>
				</li>
			</ul>
			</div>
			</li>
				<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
					<a href="javascript:;" class="m-menu__link m-menu__toggle">
						<i class="m-menu__link-icon fa fa-wrench" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
						<span class="m-menu__link-text">
							Assistência Técnica
						</span>
						<i class="m-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="m-menu__submenu " m-hidden-height="80" style="">
						<span class="m-menu__arrow"></span>
						<ul class="m-menu__subnav">
							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
								<span class="m-menu__link">
									<span class="m-menu__link-text">
										Assistência Técnica
									</span>
								</span>
							</li>							
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/chamados/1')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-phone"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Chamados Abertos</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/chamados/2')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-phone"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Chamados Fechados</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/ordensPagto/2')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-money"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Ordens de Pagamento - Abertas</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/ordensPagto/3')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-money"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Ordens de Pagamento - Pagas</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/despesas')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-dollar"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Despesas</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAssistencia/')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-dashboard"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Dashboard</span>
										</span>
									</span>
								</a>
							</li>	
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/pecas')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-bullhorn"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Solicitações de Peças</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item " >
								<a class="m-menu__link " href="<?php echo base_url('AreaAdministrador/mapaUsuarios');?>">
									<i class="m-menu__link-icon la la-map-marker"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Mapa Técnicos/Clientes</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/garantia')?>" class="m-menu__link ">
									<i class="m-menu__link-icon la la-certificate"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Gestão de Garantias</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" data-menu-submenu-toggle="hover">
								<a href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-line-graph">
										<span></span>
									</i>
									<span class="m-menu__link-text">
										Relatórios
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu " style="">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item " aria-haspopup="true">
											<a href="<?=base_url('AreaAdministrador/relatorioChamadosAbertos')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Chamados
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true">
											<a href="<?=base_url('AreaAdministrador/relatorioOrdemPagto')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Ordens de pagamento
												</span>
											</a>
										</li>	
										<li class="m-menu__item " aria-haspopup="true">
											<a href="<?=base_url('AreaAdministrador/relatorioChamadosDefeitoCausaSolucao')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Chamados - Defeitos, causas e Soluções
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true">
											<a href="<?=base_url('AreaAdministrador/relatorioChamadosCliente')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Chamados por Cliente
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true">
											<a class="m-menu__link" href="<?php echo base_url('AreaQualidade/relatorioChamados');?>">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Chamados Gestão</span>
													</span>
												</span>
											</a>
										</li>	
										<li class="m-menu__item " aria-haspopup="true">
											<a class="m-menu__link" href="<?php echo base_url('AreaQualidade/relatorioMediaChamados');?>">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Chamados Tempo Médio</span>
													</span>
												</span>
											</a>
										</li>	
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="<?=base_url('AreaAdministrador/relatorioChamadosOps')?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">Chamados x Ops</span>
													</span>
												</span>
											</a>
										</li>								
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</li>
				<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
					<a href="javascript:;" class="m-menu__link m-menu__toggle">
						<i class="m-menu__link-icon la la-cogs" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
						<span class="m-menu__link-text">
							Produção
						</span>
						<i class="m-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="m-menu__submenu " m-hidden-height="80" style="">
						<span class="m-menu__arrow"></span>
						<ul class="m-menu__subnav">
							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
								<span class="m-menu__link">
									<span class="m-menu__link-text">
										Produção
									</span>
								</span>
							</li>
							
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/Ops')?>" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-line-graph"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Op's Avulsas</span>
										</span>
									</span>
								</a>
							</li>
							<?php if($usuario_id == 12 || $usuario_id == 2){ ?>
								<li class="m-menu__item" aria-haspopup="true" >
									<a  href="<?=base_url('AreaAdministrador/programacaoProducaoRegras')?>" class="m-menu__link ">
										<i class="m-menu__link-icon la la-lock"></i>
										<span class="m-menu__link-title">
											<span class="m-menu__link-wrap">
												<span class="m-menu__link-text">Regras Produção</span>
											</span>
										</span>
									</a>
								</li>
							<?php } ?>		
							<li class="m-menu__item" aria-haspopup="true" >
									<a  href="<?=base_url('AreaAdministrador/programacaoProducao')?>" class="m-menu__link ">
										<i class="m-menu__link-icon la la-cogs"></i>
										<span class="m-menu__link-title">
											<span class="m-menu__link-wrap">
												<span class="m-menu__link-text">Programação</span>
											</span>
										</span>
									</a>
								</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/buscaNrSerieOpPedido')?>" class="m-menu__link ">
									<i class="m-menu__link-icon fa fa-barcode"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Nr. Série/Op's/Pedidos</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaAdministrador/relatorioProgramacaoSemanal')?>" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-line-graph"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Relatório Programação</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaSuprimentos/relatorioProgramacaoSemanalPorModelo')?>" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-line-graph"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Relatório Resumido Programação</span>
										</span>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
					<a href="javascript:;" class="m-menu__link m-menu__toggle">
						<i class="m-menu__link-icon la la-file" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
						<span class="m-menu__link-text">
							Almoxarifado
						</span>
						<i class="m-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="m-menu__submenu " m-hidden-height="80" style="">
						<span class="m-menu__arrow"></span>
						<ul class="m-menu__subnav">
							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
								<span class="m-menu__link">
									<span class="m-menu__link-text">
										Almoxarifado
									</span>
								</span>
							</li>
							<li class="m-menu__item " >
								<a class="m-menu__link " href="<?php echo base_url('AreaAlmoxarifado/solicitacaoPecas/');?>">
									<i class="m-menu__link-icon fa fa-wrench"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Solicitação de Peças </span>
										</span>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
					<a href="javascript:;" class="m-menu__link m-menu__toggle">
						<i class="m-menu__link-icon la la-certificate" style="color: #ffcc00;text-shadow: 2px 6px 4px #000;"></i>
						<span class="m-menu__link-text">
							Qualidade
						</span>
						<i class="m-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="m-menu__submenu " m-hidden-height="80" style="">
						<span class="m-menu__arrow"></span>
						<ul class="m-menu__subnav">
							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
								<span class="m-menu__link">
									<span class="m-menu__link-text">
										Qualidade
									</span>
								</span>
							</li>
							<li class="m-menu__item " >
								<a class="m-menu__link " href="<?php echo base_url('AreaQualidade/relatorioChamados');?>">
									<i class="m-menu__link-icon la la-area-chart"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Relatório de Chamados</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item " >
								<a class="m-menu__link " href="<?php echo base_url('AreaQualidade/relatorioRastreabilidade');?>">
									<i class="m-menu__link-icon la la-map-marker"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Relatório de Rastreabilidade</span>
										</span>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</li>