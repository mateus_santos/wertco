<li class="m-menu__item "	>
					<a class="m-menu__link " href="<?php echo base_url('AreaRepresentantes2/editarCliente');?>">
						<i class="m-menu__link-icon la la-user"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Editar Perfil</span>
							</span>
						</span>
					</a>
				</li>			
				<li class="m-menu__item m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
					<a href="javascript:;" class="m-menu__link m-menu__toggle">
						<i class="m-menu__link-icon fa fa-shopping-cart"></i>
						<span class="m-menu__link-text">
							Orçamentos
						</span>
						<i class="m-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="m-menu__submenu " m-hidden-height="80" style="">
						<span class="m-menu__arrow"></span>
						<ul class="m-menu__subnav">
							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
								<span class="m-menu__link">
									<span class="m-menu__link-text">
										Orçamentos
									</span>
								</span>
							</li>
							
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaRepresentantes2/orcamentos')?>" class="m-menu__link ">
									<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Em negociação</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  href="<?=base_url('AreaRepresentantes2/orcamentosOutros') ?>" class="m-menu__link ">
									<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">Outros</span>
										</span>
									</span>
								</a>
							</li>												
						</ul>
					</div>
				</li>
				<!--<li class="m-menu__item " aria-haspopup="true" >
					<a class="m-menu__link " href="<?php echo base_url('AreaRepresentantes2/pedidos');?>">
						<i class="m-menu__link-icon fa fa-ticket"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Pedidos</span>
							</span>
						</span>
					</a>
				</li>-->
				<li class="m-menu__item " aria-haspopup="true" >
					<a class="m-menu__link " href="<?php echo base_url('AreaRepresentantes2/areaAtuacao');?>">
						<i class="m-menu__link-icon la la-map-marker"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Áreas de Atuação</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item " >
					<a class="m-menu__link " href="<?php echo base_url('AreaRepresentantes2/gestaoFuncionarios');?>">
						<i class="m-menu__link-icon la la-users"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Gestão de Funcionários	</span>
							</span>
						</span>
					</a>
				</li>	