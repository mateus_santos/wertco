<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaIndicadores/editarCliente');?>">
		<i class="m-menu__link-icon la la-user"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Editar Perfil</span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('AreaIndicadores/orcamentosInd/');?>">
		<i class="m-menu__link-icon fa fa-shopping-cart"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Indicar Negócio </span>
			</span>
		</span>
	</a>
</li>
<li class="m-menu__item " >
	<a class="m-menu__link " href="<?php echo base_url('downloads/Parceria-Comercial-Wertco-para-Indicadores_11_2021.pdf');?>">
		<i class="m-menu__link-icon fa fa-shopping-cart"></i>
		<span class="m-menu__link-title">
			<span class="m-menu__link-wrap">
				<span class="m-menu__link-text">Política Comercial </span>
			</span>
		</span>
	</a>
</li>