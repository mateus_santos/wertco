<li class="m-menu__item " aria-haspopup="true" >
					<a class="m-menu__link " href="<?php echo base_url('AreaFeira/orcamentos');?>">
						<i class="m-menu__link-icon" style="color: #ffcc00;">></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Listar Orçamentos</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item " aria-haspopup="true" >
					<a class="m-menu__link " href="<?php echo base_url('AreaFeira/pesquisarOrcamentos');?>">
						<i class="m-menu__link-icon" style="color: #ffcc00;">></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Pesquisar Orçamentos</span>
							</span>
						</span>
					</a>
				</li>
				<li class="m-menu__item " aria-haspopup="true" >
					<a class="m-menu__link " href="<?php echo base_url('AreaFeira/cadastraOrcamentos');?>">
						<i class="m-menu__link-icon"style="color: #ffcc00;">></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Cadastrar Orçamentos</span>
							</span>
						</span>
					</a>
				</li>
				<hr style="background: #ffcc00;" />
				<li class="m-menu__item " aria-haspopup="true" >
					<a class="m-menu__link " href="<?php echo base_url('AreaFeira/pedidos');?>">
						<i class="m-menu__link-icon"style="color: #ffcc00;">></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Listar Pedidos</span>
							</span>
						</span>
					</a>
				</li>	
				<li class="m-menu__item " aria-haspopup="true" >
					<a class="m-menu__link " href="<?php echo base_url('AreaFeira/cadastraPedidos');?>">
						<i class="m-menu__link-icon"style="color: #ffcc00;">></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text">Cadastrar Pedidos</span>
							</span>
						</span>
					</a>
				</li>	