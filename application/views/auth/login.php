<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from celtano.top/salimo/demos/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
<head>
	<meta charset="utf-8" />
	<title>WERTCO - Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
-->
<!-- Favicon -->
<link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon.png')?>">

<!-- Template CSS Files -->
<link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />
<style type="text/css">
    #box{
	    border: 1px solid rgb(200, 200, 200);
	    
	    border-radius: 4px;
	    top:50px;
	}

	h2{
	    text-align:center;
	    color:#fff;
	}
    #box a:hover{
        background: #ffcc00;
        color: #000 !important;
    }
</style>
<!-- Template JS Files -->
<script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

</head>

<body class="double-diagonal blog-page dark" style="background-color: #111111; ">
	<!-- Preloader Starts -->
    <!--<div class="preloader" id="preloader">
        <div class="logopreloader">
            <img src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-black">
        </div>
        <div class="loader" id="loader"></div>
    </div>
    <!-- Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
    	<!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                    <div class="logo">
                        <a class="navbar-brand link-menu nav-external" >
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external"  href="<?=base_url('/home/index')?>"><i class="fa fa-arrow-left"></i> Voltar ao Site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
    	<section id="clientes" class="services" style="padding-bottom: 0 !important;">
    		<div class="container" style="text-align: center;">
    			<div class="row">
    				<div class="col-md-offset-6 col-md-6" id="box" style="margin: 0 auto;padding: 20px;">
    					<h2>Acessar</h2>
    					<hr>
    					<form class="form-horizontal" action="<?php echo base_url('usuarios/autenticacao'); ?>" method="post" id="login_form">
    						<fieldset>
    							<div class="form-group">
    								<div class="col-md-12">
    									<div class="input-group">
    										<span class="input-group-addon"><i class="fa fa-user"></i></span>
    										<input name="email" placeholder="E-mail" class="form-control" type="email" required type="text">
    									</div>
    								</div>
    							</div>

    							<!-- Text input-->
    							<div class="form-group">
    								<div class="col-md-12">
    									<div class="input-group">
    										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
    										<input name="senha" placeholder="Senha" class="form-control" type="password" required >
    									</div>
    								</div>
    							</div>

    							<div class="form-group">
    								<div class="col-md-12">
    									<button type="submit" class="btn btn-md btn-warning pull-right">Entrar </button>
    								</div>
    							</div>    						
                            </fieldset>        
    					   </form> 
                           <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <a href="<?php echo base_url('usuarios/esqueciMinhaSenha');?>" style="color: #ffcc00;border: 1px solid #fff;padding: 10px;  ">Esqueci minha senha </a>
                                    </div>
                                </div>                         
                           </div> 
                        </div>        
    				</div>
                    
    			</div>            
    		</div>
    	</section>
    		<!-- Service Section Ends -->
    		<!-- Footer Section Starts -->
    		<footer class="footer text-center footer-area-restrita" style="border-top: 2px solid #ffcc00 !important;width: 100%;position: fixed;bottom: 0;">
    			<!-- Container Starts -->
    			<div class="container">
    				<!-- Copyright Text Starts -->
    				<p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br>CNPJ.: 27.314.980/0001-53 </p>
    				<p>
    					© Copyright 2017 Wertco 
    				</p>

    			</div>
    			<!-- Container Ends -->
    		</footer>
    		<!-- Footer Section Starts -->
    	</div>
    	<!-- Wrapper Ends -->
    	<!-- Template JS Files -->
    	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    	<script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    	<script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    	<script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    	<script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>       
    	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>		
    	<script type="text/javascript" src="<?=base_url('bootstrap/js/sweetalert.js')?>"></script>

    	<!-- Main JS Initialization File -->
    	<!--<script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>-->
    	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal(
		  		'Ops!',
		  		'E-mail ou senha inválido!',
		  		'error'
			)
		</script>
		<?php unset($_SESSION['erro']);} ?>

    </body>

    </html>