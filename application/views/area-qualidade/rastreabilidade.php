<style type="text/css">
	#html_table tbody > tr > td{
		text-align: center;
	}
	#html_table_processing{
		position: absolute;
		top: 0;
		font-size: 21px;
		color: #000;
		font-weight: 400;
		text-align: center;
		width: 98%;
		background: #ffcc0075;
		height: 100%;
		padding-top: 18%;
	}
	
</style>
<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-map-marker"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Rastreabilidade
					</h3>
				</div>				
			</div>
			<div class="m-portlet__head-caption "></div>
			<div class="m-list__content"></div>						
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">			
			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style=""	>Nº Serie</th>
						<th style=""	>Modelo</th>
						<th style=""	>Combustivel</th>
						<th style="text-align: center;"	>Pedido</th>
						<th style="text-align: center;"	>O.P</th>
						<th style="text-align: center;"	>Cliente</th>						
						<th style="text-align: center;"	>Ações</th>
					</tr>
				</thead>
				
				</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	
	<!-- Modal afericao -->
	<div class="modal fade" id="modal_afericao" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">		
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-afericao-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">	
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					 					
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" style="margin-bottom: 2rem;">
							<label for="exampleSelect1">Data Aferição</label>
							<input type="text" class="form-control m-input m-input--air data" id="dt_afericao">							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_afericao" class="btn btn-primary" status_orcamento="" nr_serie_id="">Alterar Dt. Aferição</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Qtd lacres -->
	<div class="modal fade" id="modal_qtd_lacres" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">		
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-qtd-lacres-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">	
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					 					
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" style="margin-bottom: 2rem;">
							<label for="exampleSelect1">Qtd Lacres</label>
							<input type="text" class="form-control m-input m-input--air" id="qtd_lacres">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_qtd_lacres" class="btn btn-primary" status_orcamento="" nr_serie_id="">Alterar Qtd Lacres</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal observação -->
	<div class="modal fade" id="modal_obs" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">		
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-obs-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">	
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					 					
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" style="margin-bottom: 2rem;">
							<label for="exampleSelect1">Observação</label>
							<textarea  class="form-control m-input m-input--air" id="obs"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_obs" class="btn btn-primary" status_orcamento="" nr_serie_id="">Alterar Observação</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Responsavel -->
	<div class="modal fade" id="m_modal_8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-responsavel-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="form-group m-form__group">
						<label>Alterar Responsável</label>
						<div class="m-input-icon m-input-icon--left">
							<input type="text" class="form-control m-input--air" id="responsavel" placeholder="Pesquisar Representante" />
							<input type="hidden" class="form-control m-input--air" id="responsavel_id" placeholder="Pesquisar Representante" />
							<span class="m-input-icon__icon m-input-icon__icon--left">
								<span>
									<i class="la la-search"></i>
								</span>
							</span>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_responsavel" class="btn btn-primary" responsavel_id="" orcamento_id="" status_orcamento_id="">Alterar Responsável</button>
				</div>
			</div>
		</div>
	</div>	

	<!-- Modal Excluir -->
	<div class="modal fade" id="m_excluir_orcamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form accept-charset="utf-8" action="<?php echo base_url('AreaAdministrador/excluirOrcamento/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;">	
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title modal-excluir-title" ></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" style="width: 750px;">					
						<div class="form-group m-form__group">
							<label>Motivo da Exclusão</label>
							<div class="m-input-icon m-input-icon--left">
								<textarea class="form-control m-input--air" name="motivo_exclusao" id="motivo_exclusao" required="required"></textarea> 
								<input type="hidden" name="orcamento_id" class="form-control m-input--air" id="orcamento_id" />
								<input type="hidden" name="tabela" class="form-control m-input--air" id="tabela" value="orcamentos" />
								<span class="m-input-icon__icon m-input-icon__icon--left">
									<span>
										<i class="la la-warning"></i>
									</span>
								</span>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
						<button type="submit" id ="excluir_orcamento" name="enviar_exclusao" value="1" class="btn btn-success" responsavel_id="" orcamento_id="" status_orcamento_id="">Excluir</button>
					</div>
				</div>
			</div>
		</form>
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaAdministrador/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>
		<?php if ($this->session->flashdata('exclusao') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Orçamento excluído com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaAdministrador/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['exclusao']); } ?>