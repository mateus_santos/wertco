<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Relatório de Rastreabilidade</title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{	
	border-bottom: 1px solid #f0f0f0;	
	font-size: 9px;	
	padding: 5px;
}


</style>
<script type="text/javascript">	
	window.print();	
</script>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="<?php echo base_url('/bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png'); ?>" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br/>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br/>
										07400-230 - Arujá - SP<br/>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br/>
			</p>

		</div>
		<div class="corpo">	
			<h3 style="text-align: center;margin-top: 15px;	">Relatório de Rastreabilidade </h3>
			<?php if($periodo != ''){ ?>
			<h4 style="text-align: center;margin-top: 15px;	">Período <?php echo $periodo; ?></h4>
			<?php } 
				if(isset($dados) ){

			?>
			<hr  />
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;">
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Nº Série</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Nº Pedido</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Cliente</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Modelo</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Combustível</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Entrega</b></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Nr. Bicos</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Qtd. Lacres</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Dt. Aferição</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Startup</b></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Observações</b></td>
				</tr>				
				<?php foreach($dados as $dado){ ?>
				<tr>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo $dado['id']?> </td>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo $dado['pedido_id']?> </td>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo mb_strtoupper($dado['cliente']); ?> </td>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo $dado['modelo']?> </td>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo $dado['combustivel']; ?> </td>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo $dado['dthr_entrega'];?> </td>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo $dado['nr_bicos'];?> </td>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo $dado['qtd_lacres'];?> </td>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo $dado['dt_afericao'];?> </td>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo $dado['startup'];?> </td>
					<td style="border: 1px solid #ccc;padding: 10px;"><?php echo $dado['observacoes'];?> </td>					
				</tr>
			<?php } ?>				
			</table>
			<?php }else { ?>	
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc; margin-top: 300px;" />								
				<h2 style="text-align: center;">Nenhum resultado encontrado!</h2>
			<hr style="border-top: 1px solid #cccccc; background-color: #cccccc;" />							
			<?php } ?>		
	</div>	
</div>
</body>
</html>
