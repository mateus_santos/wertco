<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 10px;
 	color: #333;
 	width: 100%;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 12px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{	
	border-bottom: 1px solid #f0f0f0;	
	font-size: 10px;	
	padding: 5px;
}


</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br/>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br/>
										07400-230 - Arujá - SP<br/>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br/>
			</p>

		</div>
		<div class="corpo">	
			<h3 style="text-align: center;margin-top: 15px;	">Relatório de Tempo Médio de Chamados Atendidos</h3>
			<?php if($periodo != ''){ ?>
			<h4 style="text-align: center;margin-top: 15px;	">Período <?php echo $periodo; ?></h4>
			<?php } ?>
			<hr  />
			<table cellspacing="0" cellpadding="0" style="margin-top: 15px; margin-bottom: 0;">
				<tr>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Chamados Abertos Período</b></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Chamados Fechados no Período</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Média Fechamento (Dias)</b></td>
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Percentual Chamados Atendidos</b></td>					
					<!--<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Abertos Geral</b></td>					
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Fechados Geral</b></td>	
					<td style="border-bottom: 1px solid #ccc;border-right: 1px solid #ccc;padding: 0;"><b>Percentual Chamados Atendidos Geral</b></td>-->				
				</tr>				
				<?php foreach( $dados as $dado ) { ?>
				<tr>					
					<td style=""><?php echo $dado['aberto_periodo'];?> </td>
					<td style=""><?php echo $dado['fechados'];?> </td>
					<td style=""><?php echo number_format($dado['diferenca'] / $dado['fechados'],2,',','');?> </td>
					<td style=""><?php echo ($dado['abertos'] > 0) ? round(($dado['fechados']*100)/$dado['abertos'],2).' %' : '0%'; ?> </td>	
					<!--<td style=""><?php echo $dado['abertos_geral'];?> </td>
					<td style=""><?php echo $dado['fechados_geral'];?> </td>				
					<td style=""><?php echo ($dado['abertos_geral'] > 0) ? round(($dado['fechados_geral']*100)/$dado['abertos_geral'],2).' %' : '0%'; ?> </td>				-->
				</tr>
				<?php } ?>	
			
				
			</table>		
		
	</div>	
</div>
</body>
</html>