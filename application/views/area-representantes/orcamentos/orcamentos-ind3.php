<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Orçamentos
					</h3>
				</div>
				
			</div>
			<div class="m-portlet__head-caption cadastrar_orcamento">													
				<a href="<?php echo base_url('AreaIndicadores3/cadastraOrcamentos')?>" class="" style="color: #ffcc00; font-weight: bold;">
					<i class="la la-plus-circle" style="font-size: 38px;"></i>
				</a>
			</div>
			<div class="">
					<div style="text-align: center;margin-top: 25px;">
					Aberto&nbsp;&nbsp;<span class="m-badge m-badge--secondary"></span>				    	
					&nbsp;Orçamento Entregue&nbsp;&nbsp;<span class="m-badge m-badge--info"></span>
					&nbsp;Em negociação&nbsp;&nbsp;<span class="m-badge m-badge--primary"></span>
					&nbsp;Fechado&nbsp;&nbsp;<span class="m-badge m-badge--success"></span>				    		
					&nbsp;Cancelado&nbsp;&nbsp;<span class="m-badge m-badge--warning"></span>				    	
				    &nbsp;Perdido&nbsp;&nbsp;<span class="m-badge m-badge--danger"></span>	
				    <a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: -10px;" data-toggle="m-tooltip" data-placement="top" title="Tour Virtual" id="tour_virtual">
						<i class="la la-info"></i>
					</a> 		    
					</div>

			</div>					

		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;">Número</th>
						<th style="text-align: center;">Solicitante</th>
						<th style="text-align: center;">Emissão</th>											
						<th style="text-align: center;">Status</th>
						<th style="text-align: center;">Andamento (Follow Up)</th>												
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				<tbody>
					
					<?php foreach($dados as $dado){
						if( $dado->status == "aberto" ){
							$status = "btn m-btn--pill m-btn--air btn-secondary";
						}elseif( $dado->status == "fechado" ){
							$status = "btn m-btn--pill m-btn--air btn-success";
						}elseif( $dado->status == "perdido outros" || $dado->status == "perdido para wayne" || $dado->status == "perdido para gilbarco" ){
							$status = "btn m-btn--pill m-btn--air btn-danger";							
						}elseif( $dado->status == "cancelado" ){
							$status = "btn m-btn--pill m-btn--air btn-warning";
						}elseif( $dado->status == "orçamento entregue" ){
							$status = "btn m-btn--pill m-btn--air btn-info";
						}else{
							$status = "btn m-btn--pill m-btn--air btn-primary";
						}
						?>
						<tr>
							<td style="width: 5%;text-align: center;"><?php echo $dado->orcamento_id; ?></td>
							<td style="width: 35%;text-align: center;"> <?php echo $dado->cnpj." | ".$dado->razao_social; ?></td>
							<td style="width: 15%;text-align: center;"><?php echo date('d/m/Y H:i:s', strtotime($dado->emissao)); ?></td>		
							<td style="width: 25%;text-align: center; text-transform: capitalize;">
								<button class="status <?php echo $status; ?>" title="Status do orçamento" onclick="status(<?php echo $dado->orcamento_id; ?>);">
									<?php echo ucfirst($dado->status); ?>
								</button>
							</td>														
							<td style="width: 15%;text-align: center;">
								<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour flaticon-list-3 andamento" onclick="andamento(<?php echo $dado->orcamento_id; ?>,<?php echo $dado->status_orcamento_id; ?>);"  style="cursor: pointer;" ></button>
							</td>																		
							<td data-field="Actions" class="m-datatable__cell " style="width:10%;">
								<span style="overflow: visible; width: 110px;" class="">								
									<a href="<?php echo base_url('AreaIndicadores3/visualizarOrcamento/'.base64_encode($dado->orcamento_id)); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" orcamento_id="<?php echo $dado->orcamento_id; ?>" >
										<i class="la la-eye visualizar"></i>
									</a> 									
								</span>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group" >
						<label for="exampleSelect1">Situação do Orçamento</label>
						<select class="form-control m-input m-input--air" id="status_orcamento">
													
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" class="btn btn-primary" orcamento_id="">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>	

	<!-- Modal Andamento -->
	<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="m-list-timeline">
						<div class="m-list-timeline__items">                
						</div>
					</div> 
					<hr/>
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" >
							<label for="exampleSelect1">Adicionar Andamento do venda</label>
							<textarea type="text" class="form-control m-input m-input--air" id="andamento_texto"></textarea>					
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="adicionar_andamento" class="btn btn-primary" status_orcamento="" orcamento_id="">Adicionar Andamento</button>
				</div>
			</div>
		</div>
	</div>	

	<!-- Modal Responsavel -->
	<div class="modal fade" id="m_modal_8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-responsavel-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="form-group m-form__group">
						<label>Alterar Responsável</label>
						<div class="m-input-icon m-input-icon--left">
							<input type="text" class="form-control m-input--air" id="responsavel" placeholder="Pesquisar Representante" />
							<input type="hidden" class="form-control m-input--air" id="responsavel_id" placeholder="Pesquisar Representante" />
							<span class="m-input-icon__icon m-input-icon__icon--left">
								<span>
									<i class="la la-search"></i>
								</span>
							</span>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_responsavel" class="btn btn-primary" responsavel_id="" orcamento_id="" status_orcamento_id="">Alterar Responsável</button>
				</div>
			</div>
		</div>
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaIndicadores3/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>