<div class="m-content">	
	<form accept-charset="utf-8" id="formulario" action="<?php echo base_url('AreaRepresentantes/orcamentosInd3/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-list-2"></i>
					</span>
					<h3 class="m-portlet__head-text"> 
						Solicitação de Orçamento 
					</h3>
				</div>			
			</div> 
			<div class="m-portlet__head-tools">
				
			</div>
		</div>
				
		<div class="m-portlet__body">
			<div class="form-group m-form__group m--margin-top-10">
				<div class="alert m-alert m-alert--default" role="alert">
					Insira os dados da empresa interessada nos produtos WERTCO!
				</div>
			</div>		
			<div class="form-group m-form__group row">
				<div class="col-lg-3">
					<label>CNPJ:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="cnpj" id="cnpj" class="form-control m-input" placeholder="Insira um cnpj" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-file-text-o"></i></span></span>
						<input type="hidden"  id="salvar" maxlength="15" name="salvar" value="1"    required />
                        <input type="hidden"  id="empresa_id" maxlength="15" name="empresa_id" value=""  />
                        <input type="hidden"  id="cartao_cnpj" name="cartao_cnpj"  />
					</div>					
				</div>
				<div class="col-lg-3">
					<label>Razão Social:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required	name="razao_social" id="razao_social" class="form-control m-input" placeholder="" required value="">
						<input type="hidden" name="id" 	class="form-control m-input" placeholder="Insira uma razão social" required value="">								
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
						</span>
					</div>
				</div>
				<div class="col-lg-3">
					<label class="">Fantasia:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="fantasia" id="fantasia" class="form-control m-input" placeholder="Insira uma fantasia" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>	
				</div>
				<div class="col-lg-3">
					<label class="">I.E:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="insc_estadual" id="insc_estadual" class="form-control m-input" placeholder="Insira uma inscrição estadual" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map"></i></span></span>
					</div>	
				</div>
			</div>	 
			<div class="form-group m-form__group row">
				<div class="col-lg-3">
					<label>E-mail:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="email" required name="email" id="email" class="form-control m-input" placeholder="Insira um email" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Telefone:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="telefone" id="telefone" class="form-control m-input" placeholder="Insira um telefone" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Endereço:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="endereco" id="endereco" class="form-control m-input" placeholder="Insira um endereço" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label>Cep:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="cep" id="cep" class="form-control m-input" placeholder="Insira um cep" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
					</div>							
				</div>
				
			</div>
			<div class="form-group m-form__group row">	
				<div class="col-lg-3">
					<label>Bairro:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="bairro" id="bairro" class="form-control m-input" placeholder="Insira o bairro" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-street-view"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-3">
					<label class="">Cidade:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="cidade" required id="cidade" class="form-control m-input" placeholder="Insira um cidade" value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
					</div>
					
				</div>
				<div class="col-lg-3">
					<label class="">Estado:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="estado" placeholder="Estado" id="estado" required>
                                <option value="">Selecione um Estado</option>
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>
                                <option value="ES">Espírito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MS">Mato Grosso do Sul</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>
                                <option value="RJ">Rio de Janeiro</option>
                                <option value="RN">Rio Grande do Norte</option>
                                <option value="RS">Rio Grande do Sul</option>
                                <option value="RO">Rondônia</option>
                                <option value="RR">Roraima</option>
                                <option value="SC">Santa Catarina</option>
                                <option value="SP">São Paulo</option>
                                <option value="SE">Sergipe</option>
                                <option value="TO">Tocantins</option>
                            </select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
					</div>							
				</div>					
				<div class="col-lg-3">
					<label>País:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="pais" id="pais" class="form-control m-input" placeholder="Insira um pais" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map"></i></span></span>
					</div>							
				</div>
			</div>
			<hr/> 
			<div class="form-group m-form__group row">
				<div class="col-lg-4">
					<label>Contato do Posto:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="contato_posto" id="contato_posto" class="form-control m-input" placeholder="Contato do Posto" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
					</div>							
				</div>			
				<div class="col-lg-4">
					<label>Celular/Telefone:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" required name="tel_contato_posto" id="tel_contato_posto" class="form-control m-input" placeholder="Insira um telefone/celular" required value="">
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
					</div>							
				</div>
				<div class="col-lg-4">
					<label>E-mail Contato do posto:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="email" required name="email_contato_posto" id="email_contato_posto" class="form-control m-input" placeholder="Insira um e-mail" required value="" >
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
					</div>							
				</div>	
			</div>
			<hr/>	
		</div>
	</div>
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	***************************************** Forma de entrega, fretes e forma de pagto *******************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">					
					<h2 class="m-portlet__head-label m-portlet__head-label--warning" style="width: 320px;">
						<span>
							Forma de Pagto, Entrega e Frete
						</span>
					</h2>
				</div>
			</div>			
		</div>
		<div class="m-portlet__body">
			<div class="form-group m-form__group">
				<label for="Frete">
					Forma de Pagamento
				</label>
				<select class="form-control m-input m-input--square" id="forma_pagto" name="forma_pagto">
					<option value="0">Selecione a forma de pagamento</option>					
					<?php foreach ($formaPagto as $pagto) { ?>
						<option value="<?php echo $pagto->id;?>"><?php echo $pagto->descricao; ?></option>								
					<?php } ?>
				</select>
			</div>
			<div class="form-group m-form__group">
				<label for="Frete">
					Entrega
				</label>
				<select class="form-control m-input m-input--square" id="entregas" name="entrega">
					<option value="0">Selecione como vai ser realizado a entrega do(s) produto(s)</option>					
					<?php foreach ($entregas as $entrega) { ?>
						<option value="<?php echo $entrega->id;?>"><?php echo $entrega->descricao; ?></option>								
					<?php } ?>
				</select>
			</div>
			<div class="form-group m-form__group">
				<label for="Frete">
					Frete
				</label>
				<select class="form-control m-input m-input--square" id="frete" name="frete">
					<option value="0">Selecione o tipo de frete utilizado</option>		
					<?php foreach ($fretes as $frete) { ?>
						<option value="<?php echo $frete->id;?>"><?php echo $frete->descricao; ?></option>								
					<?php } ?>			
				</select>
			</div>
		</div>
	</div>	
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	********************************************************* Produtos ************************************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>Produtos</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<h4 style="text-align: center;" id="msg_produtos">Para prosseguir com a solicitação de orçamento, <b>SELECIONE O ESTADO</b> do cliente.</h4>
			<div class="esconde">
			<table class="table m-table m-table--head-separator-warning">
			  	<thead>
			    	<tr>			      		
			      		<th>Modelo | Descrição</th>
			      		<th>Quantidade</th>
			      		<th>Valor Unitário R$</th>
			      		<th>
			      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add" data-toggle="m-tooltip" data-placement="top" title="Adicionar produto">
								<i class="la la-plus"></i>
							</a>
						</th>				      		
			    	</tr>
			  	</thead>
			  	<tbody>
			  		 <tr id="modelo" indice='0' total_indice='0'>
                            <td>
                                <?php 
                                        $bombas_op     =   "";
                                        $opcionais_op  =   "";
                                        $bombas_op_slim  =   "";
                                        $bombas_op_baixa  =   "";

                                        foreach( $bombas as $bomba )
                                        {
                                            if( $bomba->tipo_produto_id == 4 ){
                                                $opcionais_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='opcionais'>".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }elseif($bomba->tipo_produto_id == 1){
                                                $bombas_op_baixa.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='bomba_baixa'>".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }elseif($bomba->tipo_produto_id == 2){
                                                $bombas_op_slim.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='bomba_baixa_slim'>".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }elseif ($bomba->tipo_produto_id == 3){
                                                $bombas_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='bomba_alta'>".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }    
                                        } 
                                ?>
                                <select  class="form-control bombas" style="font-size: 15px;" name="produto[]" indice='0' required >
                                	<option value="">Selecione um produto</option>
                                    <optgroup label="Bombas Mangueira Alta" data-subtext="optgroup subtext">
                                        <?php echo $bombas_op; ?>
                                    </optgroup>    
                                    <optgroup label="Bombas Mangueira Slim" data-subtext="optgroup subtext">
                                        <?php echo $bombas_op_slim; ?>
                                    </optgroup>    
                                    <optgroup label="Bombas Mangueira Baixa" data-subtext="optgroup subtext">
                                        <?php echo $bombas_op_baixa; ?>
                                    </optgroup>
                                    <optgroup label="Opcionais" data-subtext="optgroup subtext">
                                        <?php echo $opcionais_op; ?>
                                    </optgroup>
                                </select>
                            </td>
                            <td>
                                <input type="text" maxlength="3" class="form-control qtd" style="width: 70px;" name="quantidade[]" required  />
                            </td>
                            <td style="font-size: 15px;"><b class="valor_unitario valor_unitario_0" indice="0" data-thousands="." data-decimal="," data-prefix="R$ " ></b>
                                <input type="hidden" name="valor[]" class="form-control valor_unitario"  indice="0" maxlength="14" id="valor_unitario_0" />
                            </td>
                            <td>
                                <i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
                            </td>

                        </tr>
                    </tbody>
                </table> 
                <div style="text-align: center;">
                    <div class="m-radio-inline">
                    	<label>Selecione a comissão: &nbsp;</label>
						<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="3% de comissão" class="comissao">
							<input type="radio" name="valor_desconto_orc" value="3" class="comissao" />
							3%  
							<span></span>
						</label>
						<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="4% de comissão" class="comissao">
							<input type="radio" name="valor_desconto_orc" value="4" class="comissao" />
							4%
							<span></span>
						</label>
						<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="5% de comissão" >
							<input type="radio" name="valor_desconto_orc" class="comissao" value="5" checked="checked" />
							5%
							<span></span>
						</label>
					</div>
					<input type="hidden" name="fator" id="fator" />
                    <button type="submit" id="enviar" class="btn btn-warning salvar" disabled="disabled" >Solicitar orçamento</button>    
                </div>             			
		</div>
		</div>
	</div>
	</form>
</div>	
<script type="text/javascript">
	$('#formulario').submit(function(){
					 
		$('#enviar').attr('disabled', 'disabled');
					 
	});
</script>
<?php if ($this->session->flashdata('erro') == TRUE){ ?> 
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Orçamento solicitado com sucessoo, em breve entraremos em contato!',
                type: 'success'
            }).then(function() {
                window.location = base_url+'AreaRepresentantes/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>