<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-map" style="color: #ffcc00;"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Áreas de Atuação - <span style="color: #ffcc00; font-weight: bold;"><?php echo $razao_social; ?></span> 
					</h3>
				</div>
				
			</div>
			
			<div class="m-portlet__head-caption">													
				<a href="<?php echo base_url('AreaRepresentantes/cadastraArea')?>" class="" style="color: #ffcc00; font-weight: bold;" >
					<i class="la la-plus-circle" style="font-size: 38px;"></i>
				</a>
			</div>			
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">#</th>
						<th style="width: 45%;">Cidade</th>
						<th style="width: 20%;">Código</th>
						<th style="width: 15%;">Estado</th>												
						<th style="width: 15%;">Ações</th>			
					</tr>
				</thead>
				<tbody>					
					
					<?php foreach($dados as $dado){
							

					 ?>
						<tr>
							<td style="text-align: center;"><?php echo $dado['id']; ?></td>							
							<td><?php echo $dado['cidade']; ?></td>
							<td><?php echo $dado['cidade_cod_ibge']; ?></td>						
							<td><?php echo $dado['estado']; ?></td>							
							<td data-field="Actions" class="m-datatable__cell" >
								<span style="overflow: : visible;">									
									<a href="#" onclick="excluir('<?php echo $dado['id']; ?>');" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Excluir Funcionário" >
										<i class="la la-trash"></i>
									</a>
								</span>
							</td>
						</tr>
					<?php 
							
						} 
					?>
					</tbody>
				</table>
				<!--end: Datatable -->
				
			</div>
		</div>		        
	</div>		
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'<?=$this->session->flashdata('msg_erro'); ?>',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso_cadastro') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaTecnicos/gestaoFuncion';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>
		<?php if ($this->session->flashdata('sucesso_editar') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Edição de funcionário realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaTecnicos/gestaoFuncion';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso_editar']); } ?>