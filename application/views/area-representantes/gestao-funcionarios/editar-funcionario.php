<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('AreaRepresentantes/gestaoFuncionarios/');?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text" style="padding-right: 10px;">	
							<a href="<?php echo base_url('AreaRepresentantes/gestaoFuncionarios/');?>">Voltar</a>						
						</h3>
						<span class="m-portlet__head-icon">
						<i class="la la-user" style="color: #ffcc00;"></i>
						</span>
						<h3 class="m-portlet__head-text">							
							Editar Funcionário
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('AreaRepresentantes/editarFuncionario/'.$dados['id']);?>" method="post" id="formulario">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Nome:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required	name="nome" class="form-control m-input" placeholder="Insira o Nome do Funcionário"  value="<?php echo $dados['nome']; ?>">
								<input type="hidden" name="empresa_id" 	class="form-control m-input" required value="<?php echo $empresa_id;?>">
								<input type="hidden" name="id" 	class="form-control m-input" required value="<?php echo $dados['id'];?>">								
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span>
								</span>
							</div>
						</div>
						<div class="col-lg-4">
							<label>CPF:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="cpf" value="<?php echo $dados['cpf'];?>" id="cpf" required class="form-control m-input" placeholder="Insira o cpf"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-newspaper-o"></i></span></span>
							</div>
							
						</div>
						<div class="col-lg-4">
							<label class="">E-mail/Login de acesso:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="email" required name="email" id="email" value="<?php echo $dados['email'];?>" class="form-control m-input" placeholder="E-mail do funcionário">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-envelope-o"></i></span></span>
							</div>	
						</div>						
					</div>	 
					<div class="form-group m-form__group row">						
						<div class="col-lg-6">
							<label class="">Telefone:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="telefone" required id="telefone" class="form-control m-input" placeholder="Insira telefone" value="<?php echo $dados['telefone'];?>" >
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
							</div>
							
						</div>
						<div class="col-lg-6">
							<label class="">Celular:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="celular" value="<?php echo $dados['celular'];?>" id="celular" class="form-control m-input" placeholder="Insira Celular">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-phone"></i></span></span>
							</div>
							
						</div>
					</div>	 					
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label for="Acesso">Tipo de Acesso</label>							
							<select class="form-control m-input" name="tipo_cadastro_id" required>
								<option value="">Selecione o nível de acesso</option>
								<option value="6" <?php echo ($dados['tipo_cadastro_id'] == 6) 	? 'selected="seleted"' : ''; ?> >Nível 1</option>
								<option value="10"<?php echo ($dados['tipo_cadastro_id'] == 10) ? 'selected="seleted"' : ''; ?> >Nível 2</option>
								<option value="2" <?php echo ($dados['tipo_cadastro_id'] == 2) 	? 'selected="seleted"' : ''; ?> >Nível 3</option>
							</select>
						</div>
						<div class="col-lg-3" style="padding-top: 15px;padding-left: 15px;">
							
							<p> 
								<span style="color: #ffcc00;font-weight: bold;">Nível 1:</span><br/>
							  	- Lançamentos de orçamentos;<br/>
								- Visualização de fallow-ups;<br/>
								- Impressão do orçamento;	
							</p>
						</div>																	
						<div class="col-lg-3" style="padding-top: 15px;padding-left: 15px;">
							<p> 
								<span style="color: #ffcc00;font-weight: bold;">Nível 2:</span> <br/> 
								- Lançamentos de orçamentos;<br/>
                            	- Visualização dos preços cheios dos produtos;<br/>
								- Visualização de fallow-ups;<br/>
								- Emissão de orçamentos por e-mail;<br/>
								- Alteração do modo de pagamento, do frete e da entrega;<br/>
								- Alteração dos Produtos e Quantidades;
							</p>
						</div>
						<div class="col-lg-3" style="padding-top: 15px;padding-left: 15px;">
							<p> 
								<span style="color: #ffcc00;font-weight: bold;">Nível 3:</span> <br/> 
								- Lançamentos de orçamentos;<br/>
								- Visualização e edição dos orçamentos da empresa;<br/>
								- Visualização de todas as tabelas de preço dos produtos;<br/>
								- Visualização de fallow-ups;<br/>
								- Inserção de fallow-ups;<br/>
								- Emissão de orçamentos por e-mail;<br/>
								- Alteração do modo de pagamento, do frete e da entrega;<br/>
								- Alteração dos Produtos e Quantidades e Preço;<br />
								- Escolha de comissão por orçamento;
							</p>							
						</div>
					</div>	           	            		        					
					<div class="form-group m-form__group row"> 
						<div class="col-lg-6">
							<label>Redefinir senha:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="password" name="senha" class="form-control m-input" placeholder=""  value="" id="senha"  pattern=".{6,}"  title="Digite no mínimo 6 caracteres"><span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-key"></i></span>
								</span>
							</div>
						</div>
						<div class="col-lg-6">
							<label class="">Confirmar Senha de acesso do funcionário:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="password" name="senha2" id="senha2" class="form-control m-input" value="">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-key"></i></span></span>
							</div>	
						</div>
					</div>
					<div class="form-group m-form__group row">
			            <div class="col-lg-12">	
		            		<div class="m-radio-inline">	            		
			            		<label for="Empresa">Ativo</label><br/>
								<label class="m-radio">
									<input type="radio" name="ativo" class="ativo" value="1" <?php echo ($dados['ativo'] == 1) ? 'checked="cheked"' : ''; ?> > Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" name="ativo" class="ativo" value="0" <?php echo ($dados['ativo'] == 0) ? 'checked="cheked"' : ''; ?>> Não
									<span></span>
								</label>
							</div>
						</div>
					</div>	
			</div>	 
            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>							
					</div>
				</div>
			</div>
			</form>
			<!--end::Form-->
		</div>
</div>	