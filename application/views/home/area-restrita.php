<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from celtano.top/salimo/demos/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
<head>
    <meta charset="utf-8" />
    <title>Área Restrita - WERTCO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    -->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url('bootstrap/img/favicon.png'); ?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/font-awesome.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/magnific-popup.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/style.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/skins/yellow.css'); ?>" />
    <script type="text/javascript">
        var base_url = "<?php echo base_url();?>";
    </script>

    <!-- Template JS Files -->
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/modernizr.js'); ?>"></script>

</head>

<body class="double-diagonal blog-page dark">    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
                    <!-- Logo Starts -->
                    <div class="logo">
                        <a class="navbar-brand link-menu nav-external" href="<?=base_url();?>">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?php echo base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png'); ?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            <!-- Logo Black Starts -->
                            <img id="logo-dark" class="logo-dark" src="<?php echo base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png'); ?>" alt="logo-dark" />
                            <!-- Logo Black Ends -->
                        </a>
                    </div>
                    <!-- Logo Ends -->
                    <!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span id="icon-toggler">
                          <span></span>
                          <span></span>
                          <span></span>
                          <span></span>
                        </span>
                    </button>
                    <!-- Hamburger Icon Ends -->
                    <!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external" href="<?php echo base_url('home/index'); ?>"><i class="fa fa-arrow-left"></i> Voltar ao Site</a></li>
                        </ul>
                    </div>
                    <!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
        <!-- Banner Starts -->
        <section class="banner" >
            <div class="content text-center">
                <div class="text-center top-text">
                    <h1>Área Restrita</h1>
                    <hr>
                    <h4>Área destinada aos clientes, representantes comerciais, Mecânicos de bombas e Técnicos.</h4>
                </div>
            </div>
        </section>
        <!--    Banner Ends        -->
        <!--    Services Begin     -->
        <section id="services" class="services">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Escolha</span> o seu canal</h1>
                    <h4></h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-caret-down" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- Services Starts -->
                <div class="row services-box">
                     <!-- Service Item Starts -->
                    <div class="col-lg-4 col-md-4 col-sm-12 services-box-item clientes_menu inativo" style="margin-bottom: 80px;">
                        <!-- Service Item Cover Starts -->
                        <button class="">
                            <span class="services-box-item-cover2 fa fa-handshake-o" style="color: #fff;" data-headline="Clientes"></span>
                        </button>
                        <img class="" id="img_clientes" src="<?=base_url('bootstrap/img/down.png')?>" style="position: fixed;width: 21px;margin: 163px auto;left: 48%;cursor: pointer;" />
                        <ul class="menu_area_restrita" id="menu_clientes">
                            <li class="login" onclick="abrir('<?php echo base_url('usuarios/login');?>')">Entrar</li>
                            <li class="cadastrar_cliente" onclick="abrir('<?php echo base_url('clientes/cadastro');?>')">Cadastrar</li>
                        </ul>
                    </div>
                    <!-- Service Item Ends -->
                    <!-- Service Item Starts -->
                    <!-- Service Item Starts -->
                    <!--<div class="col-lg-3 col-md-3 col-sm-12 services-box-item rep_menu inativo" style="cursor:initial !important;">
                        
                        <button ><span class="services-box-item-cover2 fa fa-users" style="color: #fff;" data-headline="Representantes"></span></button>
                        <img src="<?=base_url('bootstrap/img/down.png')?>" id="img_rep" style="position: fixed;width: 21px;margin: 163px auto;left: 48%;cursor: pointer;" />
                         <ul class="menu_area_restrita" id="menu_rep">
                            <li class="login" onclick="abrir('<?php echo base_url('usuarios/login');?>')">Entrar</li>

                        </ul>
                    </div> -->
                    <!-- Service Item Ends -->                  
                     <!-- Service Item Starts -->
                    <div class="col-lg-4 col-md-4 col-sm-12 services-box-item tecnicos_menu inativo"  style="margin-bottom: 80px;" >
                        <!-- Service Item Cover Starts -->
                        <button ><span class="services-box-item-cover2 fa fa-microchip" style="color: #fff;" data-headline="Técnicos/Mecânicos"></span></button>
                        <img src="<?=base_url('bootstrap/img/down.png')?>" id="img_tecnicos" style="position: fixed;width: 21px;margin: 163px auto;left: 48%;cursor: pointer;" />
                        <ul class="menu_area_restrita" id="menu_tecnicos">
                            <li class="login" onclick="abrir('<?php echo base_url('usuarios/login');?>')">Entrar</li>
                            <li class="cadastrar_mecanicos" onclick="abrir('<?php echo base_url('tecnicos/cadastro');?>')">Cadastrar</li>
                        </ul>
                    </div>
                    <!-- Service Item Ends -->
                    <!-- Service Item Starts -->
                    <div class="col-lg-4 col-md-4 col-sm-12 services-box-item indicador_menu inativo"  style="margin-bottom: 80px;" >
                        <!-- Service Item Cover Starts -->
                        <button ><span class="services-box-item-cover2 fa fa-hand-o-right" style="color: #fff;" data-headline="Indicadores de Negócios"></span></button>
                        <img src="<?=base_url('bootstrap/img/down.png')?>" id="img_indicador" style="position: fixed;width: 21px;margin: 163px auto;left: 48%;cursor: pointer;" />
                        <ul class="menu_area_restrita" id="menu_indicador">
                            <li class="login" onclick="abrir('<?php echo base_url('usuarios/login');?>')">Entrar</li>
                            <li class="cadastrar_indicador" onclick="abrir('<?php echo base_url('indicadores/cadastro');?>')">Cadastrar</li>
                        </ul>
                    </div>
                    <!-- Service Item Ends -->
                </div>
                

                </div>
                <!-- Services Ends -->
            </section>
        <!-- Service Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center footer-area-restrita" style="border-top: 2px solid #ffcc00 !important;">
            <!-- Container Starts -->
            <div class="container">
               <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/0001-53 </p>
                <p>
                    &copy; Copyright 2017 Wertco. &nbsp;<a href="<?php echo base_url('usuarios/login'); ?>"><i class="fa fa-lock"></i></a>  
                </p>                
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
    </div>
    <!-- Wrapper Ends -->
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/jquery-2.2.4.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.easing.1.3.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/bootstrap.bundle.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.bxslider.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.filterizr.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js'); ?>"></script>
    
    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/area-restrita.js'); ?>"></script>

</body>

</html>