<!DOCTYPE html>
<html lang="eng">

<head>
    <meta charset="utf-8" />
    <title>WERTCO - FUELING SOLUTIONS </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	--> 
	

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon1.png')?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />

    <!-- Revolution Slider CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/settings.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/layers.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/navigation.css')?>" />

    <!-- Live Style Switcher - demo only --> 
    <link rel="alternate stylesheet" type="text/css" title="yellow" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen" href="<?=base_url('bootstrap/css/skins/yellowgreen.css')?>" />
    
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

    <!-- Accordion TTU -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/accordion.css')?>" />
    <script type="text/javascript">
        var base_url = "<?php echo base_url();?>"; 
    </script>
</head>

<body class="dark double-diagonal">
    <!-- Preloader Starts -->
    <div class="preloader" id="preloader">
        <div class="logopreloader">
            <img class="img-fluid" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png')?>" alt="logo-black" />
        </div>
        <div class="loader" id="loader"></div>
    </div>
    <!-- Preloader Ends -->    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">                
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
                    <!-- Logo Starts -->
                    <div class="logo">
                        <a data-toggle="collapse" data-target=".navbar-collapse.show" class="navbar-brand link-menu scroll-to-target" href="#mainslider">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />                            
                        </a>
                    </div>
                    <!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span id="icon-toggler">
                          <span></span>
                          <span></span>
                          <span></span>
                          <span></span>
                        </span>
                    </button>
                    <!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#mainslider"><i class="fa fa-home"></i> Home</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#about"><i class="fa fa-user"></i> WERTCO</a></li>
                            <li id='bombas_ico'><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#testimonials"><img src="<?=base_url('bootstrap/img/bombabranca.png')?>" class="bombas_ico"> FUEL PUMPS</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#services"><i class="fa fa-arrow-up"></i> DIFFERENTIAL</a></li>
							<li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#inovadoras"><i class="fa fa-microchip"></i> CHARACTERISTICS</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#certificacoes"><i class="fa fa-certificate"></i> CERTIFICATIONS</a></li>                            
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#contact"><i class="fa fa-envelope"></i> CONTACT</a></li>    
                           <li class="bombas_menu"><a href="<?php echo base_url('usuarios/login');?>" target="_blank" class="link-menu nav-link" onclick="window.open('https://www.wertco.com.br/usuarios/login');"><i class="fa fa-sign-in"></i> LOGIN</a></li>                                                 
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		<!-- Header Ends -->
        <!-- Main Slider Section Starts -->
        <section class="mainslider" id="mainslider">
            <!-- Slider Hero Starts -->
            <div class="rev_slider_wrapper fullwidthbanner-container dark-slider" data-alias="vimeo-hero" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
                <div id="rev_slider" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-index="rs-18" data-transition="zoomin" data-slotamount="7" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/fundopostowertco.jpg')?>" data-rotate="0" data-saveperformance="off" data-title="Ken Burns" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/fundopostowertco.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">WERTCO 
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap; font-size: 22px !important; height: 35px !important; margin-top: 10px !important; ">SUPPLYING SOLUTIONS
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#about" class="custom-button slider-button scroll-to-target">Learn More About WERTCO</a></div>
                        </li>
                        <!-- SLIDE  -->
                        <li data-index="rs-2" data-transition="zoomin" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/quatroBombas1.jpg')?>" data-rotate="0" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/quatroBombas1.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">RELIABILITY
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#testimonials" class="custom-button slider-button scroll-to-target">Learn more about WERTCO pumps</a></div>
                        </li>
                        <!-- SLIDE  -->
                        <li data-index="rs-3" data-transition="zoomin" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/duasBombas.jpg')?>" data-rotate="0" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/duasBombas.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">DURABILITY
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#testimonials" class="custom-button slider-button scroll-to-target">Learn more about WERTCO pumps</a></div>
                        </li>
                        <li data-index="rs-4" data-transition="zoomin" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/tresBombas1.jpg')?>" data-rotate="0" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/tresBombas1.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">POSSIBILITIES
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#testimonials" class="custom-button slider-button scroll-to-target">Learn more about WERTCO pumps</a></div>
                        </li>
                    </ul>
                    <div class="tp-static-layers"></div>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->
            <!-- Slider Hero Ends -->
        </section>
        <!-- Main Slider Section Ends -->
        <!-- About Section Starts -->
        <section id="about" class="about">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>About</span> US</h1>
                    <!--<h4></h4>-->
                </div>
                <!-- Main Heading Ends -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-user" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- About Content Starts -->
                <div class="row about-content">
                    <div class="col-sm-12 col-md-12 col-lg-12 about-left-side">
                        <h3 class="title-about"><strong>WERTCO</strong></h3>
                        <hr>
                        <p>WERTCO is a Brazilian company, manufacturer of supply pumps designed to meet the new MTPS regulation 1109 and other requirements of the market, respecting the norms of health, metrology and, mainly, inhibition of supply fraud.
                        </p>
                        <p>Addressing the market's need for high safety, connectivity and sustainability pumps, Wertco innovates with reliable, durable, modern, easy-to-integrate products and with more possibilities and functionality for the business.
                        </p>
                        <p>Formed by professionals with knowhow in the market of manufacturing of pumps of supply and in addition to the group of electronics development of Companytec, Wertco develops products based on three important values: Reliability, Durability and Possibilities.
                        </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <a class="custom-button slider-button scroll-to-target" href="#testimonials">Know our Fuel Pumps</a>
                    </div>                   
                <!-- About Content Ends -->
            </div>
            <!-- Container Ends -->
        </section>
        <!-- About Section Ends -->
        <!-- BOMBAS DE ABASTECIMENTO Section Starts -->
        <section id="testimonials" class="testimonials">
           <div class="section-overlay">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Supply </span>Pumps</h1>
                    <h4></h4>
                </div>
                <!-- Main Heading Ends -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <img src="<?=base_url('bootstrap/img/bombaIcoG.png')?>" />
                    <span class="outer-line"></span>
                </div>               
                
                <div class="row team-members">
				
                    <!-- Team Member Starts -->
                    <div class="col-lg-4 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <div class="team-member">
							<div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CLH (Low Hose)<br/></h4>                              
                                <ul class="list list-inline social">
                                   <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="fa fa-search-plus" href="<?=base_url('bootstrap/img/bombas/infoCLH.png')?>"></a>
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraisclh.png')?>" style='display: none;'></a>
                                    </li>
                                </ul>
								 <a title="CLH Mangueira baixa" href="#" class="team-member-img-wrap">
								<img src="<?=base_url('bootstrap/img/bombas/CLH.png')?>" alt="team member" /></a>
                            </div>
                            <!-- Team Member Picture Starts -->
                           
                            <!-- Team Member Picture Ends -->
                            <!-- Team Member Details Starts -->
                            
                            <!-- Team Member Details Starts -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->
					 <!-- Team Member Starts -->
                    <div class="col-lg-4 col-md-6 col-sm-12 magnific-popup-gallery">
                        <div class="team-member">                           
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CLH With Gantry Option</h4>                               
                                <ul class="list list-inline social">
                                   <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="fa fa-search-plus mfp-iframe" href="http://vimeo.com/251343305"></a>
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="mfp-iframe" href="http://vimeo.com/251343305" style='display: none;'></a>
                                    </li>
									<li>
										<a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraisclh.png')?>" style='display: none;'></a>
									</li>
                                </ul>
								 <!-- Team Member Picture Starts -->
								<a title="CLH Mangueira baixa Com opção de pórtico" href="#" class="team-member-img-wrap mfp-iframe">
								<img src="<?=base_url('bootstrap/img/bombas/CLHPORTICO.png')?>" alt="team member" /></a>
                            <!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Starts -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->
                    <!-- Team Member Starts -->
                    <div class="col-lg-4 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <div class="team-member">
                            
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CHHS (High Hose Slim)</h4>									
                                <ul class="list list-inline social">
                                    <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas fa fa-search-plus" href="<?=base_url('bootstrap/img/bombas/infoCHHS.png')?>"></a>
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraischhs.png')?>" style='display: none;'></a>
                                    </li>
                                </ul>
								<!-- Team Member Picture Starts -->
								<a title="CHHS | Mangueira Alta" href="#" class="bombas team-member-img-wrap">
									<img src="<?=base_url('bootstrap/img/bombas/CHHS.png')?>" alt="team member">
								</a>
								<!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Ends -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->
                    <!-- Team Member Starts -->
                    <div class="col-lg-4 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <!-- Team Member-->
                        <div class="team-member">                            
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CHH (High Hose) </h4>									
                                <ul class="list list-inline social">
                                    <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas fa fa-search-plus" href="<?=base_url('bootstrap/img/bombas/infoCHH.png')?>"></a>
										
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraischh.png')?>" style='display: none;'></a>
                                    </li>
                                </ul>
								<!-- Team Member Picture Starts -->
								<a title="CHH (Mangueira Alta)" href="#" class="bombas team-member-img-wrap">
									<img src="<?=base_url('bootstrap/img/bombas/CHH.png')?>" alt="">
								</a>
								<!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Ends -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->   
                    <!-- Team Member Starts -->
                    <div class="col-lg-4 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <!-- Team Member-->
                        <div class="team-member">                            
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">  Electronic Dispenser for Train   </h4>
                                <ul class="list list-inline social">
                                    <li>
                                        &nbsp;
                                    </li>  
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas fa fa-search-plus " href="<?=base_url('bootstrap/img/bombas/medidor_info.png')?>">                                            
                                        </a>                     
                                    </li>                                  
                                    <!--<li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas team-member-img-wrap" href="<?=base_url('bootstrap/img/bombas/medidor_info.png')?>" style='display: none;'></a>
                                    </li>-->
                                </ul>
                                <!-- Team Member Picture Starts -->
                                <a title="CHH (Mangueira Alta)" href="#" class="bombas team-member-img-wrap">
                                    <img src="<?=base_url('bootstrap/img/bombas/medidor.png')?>" alt="Dispensador electronico para tren" title="Dispensador electronico para tren " >
                                </a>
                                <!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Ends -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->                 
                </div>
                <!-- Team Members Ends -->				
            </div>
            </div>
            <!-- Container Ends -->
          </div>  
        </section>
        <!-- Team Section Ends -->
        <!-- Services Section Starts -->
        <section id="services" class="services">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>OUR</span> DIFFERENTIALS</h1>
                    <h4><!--PRODUCTS DESIGNED FOR THE BRAZILIAN MARKET --></h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-arrow-up" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- Services Starts -->
                <div class="row services-box">
                     <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-handshake-o" data-headline="Reliability"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-handshake-o">
                            <h2>Reliability</h2>
                            <p>Electronics developed by Companytec Automação e Controle, leader in the Brazilian automation market for gas stations. <br/>
Commercial partnership with North American company Bennett for supply of hydraulic assemblies that are reference of high performance, easy maintenance and super-resistance to the characteristics of operation of the pumps in Brazil.</p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                    <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-cubes" data-headline="Durability"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-cubes">
                            <h2>Durability</h2>
                            <p>Products designed with materials used in the aeronautics and automotive industry, which guarantee greater resistance to wear and tear resulting from the use of the equipment, besides a differentiated design and great finishing.</p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                </div>
                <!-- Services Starts -->
                <div class="row services-box">    
                     <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-random" data-headline="Possibilities"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-random">
                            <h2>Possibilities</h2>
                            <p>Connectivity and Integration with systems and devices. <br/>
Preparation for receiving steam extraction systems.<br/>			Modular and rational design.</p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                     <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-lock" data-headline="High Security"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-lock">
                            <h2>High Security</h2>
                            <p>Electronics developed to meet electrical safety requirements and robustness. Designed to make it difficult, to the maximum, the most diverse attempts of fraud that exist in the market.</p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                   
                </div>

                </div>
                <!-- Services Ends -->
            </section>
             <!-- begin bomba mutcho -->            
            <section class="contactform" id="inovadoras" style="margin-top: -55px;">
                <div class="section-overlay">
                    <div class="container">
                        <!-- Main Heading Starts -->
                        <div class="text-center top-text">
                            <h1><span>Features </span> Innovative</h1>                            
                        </div>
                         <!-- Divider Starts -->
                        <div class="divider text-center">
                            <span class="outer-line"></span>
                            <span class="fa fa-microchip" aria-hidden="true"></span>
                            <span class="outer-line"></span>
                        </div>
                <!-- Divider Ends -->
                        <div class="row" style="margin-top: 45px;">
                                <div class="col-lg-6 col-md-12 col-sm-12 bombas_abastecimentos">                                    
									<img class="img-fluid" id="altera_img" />
                                </div>
								<div class="col-sm-12 col-md-12 col-lg-6" >
                                    <div class="panel-group" id="accordion">
										 <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="eletronica" data-parent="#accordion" href="#collapse13" aria-expanded="false">
                                                        <i class="fa fa-plus-circle eletronica_i" ></i>&nbsp;&nbsp;Electronics Companytec</a>
													
                                                </h4>
												
                                            </div>
                                            <div id="collapse13" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Simple, modern and easy integration and connectivity was designed and developed using as reference the Inmetro ordinance nº 559.
                                                        Electronics composed of 7 parts: <br/> <br/>
                                                        • Controller (separate from the rest to minimize possible substitutions); <br/>
                                                        • Interface / Source / Drive; <br/>
                                                        • Indicator - LCD (Display); <br/>
                                                        • Touch keys + RFID reader;
                                                        • Transducer (pulser) / Nozzle sensor; <br/>
                                                        • Loop current compatible with most automation systems on the market; <br/>
                                                        • Media Module (Optional). <br/> <br/>
                                                        Other electronics differentials: <br/> <br/>

                                                        • Standardized Electronics - All pump models have the same components. Lower cost of spare parts and agility in replacement; <br/> <br/>
                                                        • Operation with up to 3 price levels - cash (sight), debit card, credit card; <br/> <br/>
                                                        • Automatic hardware identification - no jumpers or part configurations during a part change. The controller identifies the "plug and play" components; <br/> <br/>
                                                        • Distributed intelligence - independent internal data network on the side, reducing connections; ease of maintenance and reduction of general situations (one side does not interfere in the other); possibility to extend new functions in the system just by adding modules; <br/> <br/>
                                                        • System allows configuration and update by pendrive - agility in loading configurations through model files and reducing maintenance time; <br/> <br/>
                                                        • Backup configuration of the controller in the interface - in case of extreme change of the controller there is no loss of time of the mechanic./>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="segura" data-parent="#accordion" href="#collapse30" aria-expanded="false"><i class="fa fa-plus-circle segura_i" ></i>&nbsp;&nbsp;Secure Configuration </a>
                                                </h4>
                                            </div>
                                            <div id="collapse30" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> Configuration and management through contactless cards: <br/> - Management Card - allows you to configure the pump operation options; <br/> - Technical Card - technical level access that allows changing the operating profiles; </ br> - System Card - management of parameters related to communication with the management system of the station.

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="identfid" data-parent="#accordion" href="#collapse31" aria-expanded="false"><i class="fa fa-plus-circle identfid_i" ></i>&nbsp;&nbsp;Identification of Customers and Clients </a>
                                                </h4>
                                            </div>
                                            <div id="collapse31" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> 
                                                        System Identification Identfid® * from Companytec. It allows the unlocking of the pump by means of identification, it allows the control of productivity for bracers and the implementation of loyalty programs. Compatible with other automations. <br/> * The system requires license to use.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="armazenamento" data-parent="#accordion" href="#collapse32" aria-expanded="false"><i class="fa fa-plus-circle armazenamento_i" ></i>&nbsp;&nbsp;Data storage </a>
                                                </h4>
                                            </div>
                                            <div id="collapse32" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> 
                                                        Recording memory of the last 50 complete supplies.

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="portico" data-parent="#accordion" href="#collapse12" aria-expanded="false"><i class="fa fa-plus-circle portico_i" ></i>&nbsp;&nbsp;Portico </a>
                                                </h4>
                                            </div>
                                            <div id="collapse12" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> Portico is a design solution in the pumps for the standardization of the environment of the station when there is a need to use high and low hose pumps in the same supply coverage or for the replacement of the product, island or supply position indicators.</p>
                                                </div>
                                            </div>
                                        </div>
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="bicos" data-parent="#accordion" href="#collapseTwo" aria-expanded="false"><i class="fa fa-plus-circle bicos_i" ></i>&nbsp;&nbsp;Nozzle holder</a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Nozzle holder with polycarbonate product indicator ensures durability and facilitates cleaning and maintenance.
                                                    </p>
                                                </div>
                                            </div>
                                        </div> 
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="base" data-parent="#accordion" href="#collapseFour" aria-expanded="false"><i class="fa fa-plus-circle base_i" ></i>&nbsp;&nbsp;Robust Base</a>
                                                </h4>
                                            </div>
                                            <div id="collapseFour" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Robust base built with materials that do not suffer from corrosion.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
										
										
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="mostrador" data-parent="#accordion" href="#collapse6" aria-expanded="false"><i class="fa fa-plus-circle mostrador_i" ></i>&nbsp;&nbsp;Display</a>
                                                </h4>
                                            </div>
                                            <div id="collapse6" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Tempered and screened glass display internally with white digits on black background for easy reading day or night.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="display" data-parent="#accordion" href="#collapse5" aria-expanded="false"><i class="fa fa-plus-circle display_i" ></i>&nbsp;&nbsp;Product indicators (Display)</a>
                                                </h4>
                                            </div>
                                            <div id="collapse5" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        1.5 "digit display with LED backlight and black background. Energy saving and longer lighting life.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="totalizador" data-parent="#accordion" href="#collapse7" aria-expanded="false"><i class="fa fa-plus-circle totalizador_i" ></i>&nbsp;&nbsp;Totalizer</a>
                                                </h4>
                                            </div>
                                            <div id="collapse7" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Perpetual totalizer with e-ink technology (the image does not disappear even with the pump off). Agility in the conference of electronic totalizers, informing number of the nozzle and complete totalizer.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="teclado" data-parent="#accordion" href="#collapse8" aria-expanded="false"><i class="fa fa-plus-circle teclado_i" ></i>&nbsp;&nbsp;Keyboard</a>
                                                </h4>
                                            </div>
                                            <div id="collapse8" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Touch keypad on the display glass itself, with illuminated LED numbers. Durability and tightness (no water enters).
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="indicadores" data-parent="#accordion" href="#collapse9" aria-expanded="false"><i class="fa fa-plus-circle indicadores_i" ></i>&nbsp;&nbsp;Product indicators</a>
                                                </h4>
                                            </div>
                                            <div id="collapse9" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Product indicators adhesive behind the glass; better finish and durability.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="multimidia" data-parent="#accordion" href="#collapse10" aria-expanded="false"><i class="fa fa-plus-circle multimidia_i" ></i>&nbsp;&nbsp;Multimedia</a>
                                                </h4>
                                            </div>
                                            <div id="collapse10" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Multimedia (optional) developed and managed by the station, with LCD monitor of 10.4 "and connections via cable or pendrive. The multimedia of the WERTCO pumps, can be used for: <br/> - Promotion of products and services; <br/> - Exclusive communication channel with clients; <br/> - Generation of additional revenue;
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <!--<div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="antifraude" data-parent="#accordion" href="#collapse14" aria-expanded="false"><i class="fa fa-plus-circle" ></i>&nbsp;&nbsp;Antifraude</a>
                                                </h4>
                                            </div>
                                            <div id="collapse14" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                         Transdutor sem partes móveis, com encoder magnético absoluto que detecta a posição instantânea do medidor sem possibilidade de perder pulsos como em tecnologias de bombas atuais. Esta tecnologia, com alta resolução, inibe e dificulta qualquer tipo de fraude.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>-->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="conectividade" data-parent="#accordion" href="#collapse15" aria-expanded="false"><i class="fa fa-plus-circle conectividade_i" ></i>&nbsp;&nbsp;Connectivity</a>
                                                </h4>
                                            </div>
                                            <div id="collapse15" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                       - Ethernet connection; <br/> - Bluetooth optional; <br/> - Loop current (compatible with most automation systems on the market).
                                                    </p>
                                                </div>
                                            </div>
										</div>                                                                              
                                       
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="smc" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="fa fa-plus-circle smc_i"  ></i>&nbsp;&nbsp;Bases, Side Columns, Lids, Nozzle Holder and Finishes manufactured in SMC</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <p>
                                                    SMC (Sheet Molding Compoud) is a composite consisting of a thermosetting resin, reinforcing fibers, mineral fillers and thermoplastic additives.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="revestimentos" data-parent="#accordion" href="#collapseFive" aria-expanded="false"><i class="fa fa-plus-circle revestimentos_i" ></i>&nbsp;&nbsp;Front, side, gantry and closure panel coverings</a>
                                                </h4>
                                            </div>
                                            <div id="collapseFive" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Front panel, side panels, door frames and closures made of aluminum, high corrosion resistant material.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>     
                                         <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="teclado" data-parent="#accordion" href="#collapsesFive" aria-expanded="false"><i class="fa fa-plus-circle teclado_i" ></i>&nbsp;&nbsp;Pre-set keyboard</a>
                                                </h4>
                                            </div>
                                            <div id="collapsesFive" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Up to three configurable price levels at the pump (cash, credit card and debit card).
                                                    </p>
                                                </div>
                                            </div>
                                        </div>  
                                         <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="certificacoes" data-parent="#accordion" href="#collapseses" aria-expanded="false"><i class="fa fa-plus-circle certificacoes_i" ></i>&nbsp;&nbsp;Certifications</a>
                                                </h4>
                                            </div>
                                            <div id="collapseses" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Certified by UL do Brasil and approved by Inmetro, the pumps comply with the latest Brazilian technical standards for protection and safety.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="manutencao" data-parent="#accordion" href="#facilmanut" aria-expanded="false"><i class="fa fa-plus-circle manutencao_i"  ></i>&nbsp;&nbsp;Easy Maintenance</a>
                                                </h4>
                                            </div>
                                            <div id="facilmanut" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Designed to facilitate and reduce service and maintenance time.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>                                        
                                        
                                        </div>
                                      </div>      
                                    </div>
                            </div>                                          
                </div>            
        </section>
        <!-- end Accordion Section -->       
        <!-- Blog Section Starts -->
        <section id="certificacoes" class="blog">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Certifications</span></h1>
                    <h4></h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-certificate" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- Latest Blog Posts Starts -->
                <div class="row" style="margin-top: 50px; ">
                    <div class="col-sm-4">
                        <div class="card">
                            <div style="height: 100px;position: absolute;top: 45%;left: 50%;margin-top: -44px;margin-left: -35px;text-align: center;">
                                <a href="<?php echo base_url('bootstrap/certificados/certificadoUL.pdf');?>" target="blank"><img src="<?php echo base_url('bootstrap/img/ubl.png');?>" height="100"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div style="margin: auto;">
                                <a href="<?php echo base_url('Home/qualidade');?>" target="blank"><img src="<?php echo base_url('bootstrap/certificados/iso.png');?>" height="100%" class="img-fluid"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div style="width: 228px;height: 100px;position: absolute;top: 50%;left: 50%;margin-top: -44px;margin-left: -116px;text-align: center;">
                                <a href="<?php echo base_url('bootstrap/certificados/inmetro.pdf');?>" target="blank"><p>Ordinance Inmetro/Dimel n.º 11, de 26th January
 2018.</p></a>
                            </div>    
                        </div>
                    </div>
                </div>
                <!-- Latest Blog Posts Ends -->
            </div>
        </section>        
        <!-- Blog Section Ends -->    
            
        <!-- Newsletter Section Starts -->
        <section class="newsletter">
            <div class="section-overlay">
                <!-- Container Starts -->
                <div class="container">
                    <!-- Main Heading Starts -->
                    <div class="text-center top-text">
                        <h1><span> newsletter </span></h1>
                        <h4></h4>
                    </div>
                    <!-- Main Heading Ends -->
                    <div class="newsletter-content">
                        <p class="text-center">Subscribe to the newsletter and receive information on the market for gas stations, <br/> releases and other news from Wertco</p>
                        <!-- Newsletter Form Starts -->
                        <form class="form-inputs">
                            <!-- Newsletter Form Input Field Starts -->
                            <div class="col-md-12 form-group custom-form-group p-0">
                                <span class="input custom-input">
                                    <input placeholder="E-mail" class="input-field custom-input-field" required id="email_newsletter" type="text" />
                                    <label class="input-label custom-input-label" >
                                        <i class="fa fa-envelope-open-o icon icon-field"></i>
                                    </label>
                                </span>
								 <!-- Form Submit Message Starts -->
                                <div class="col-sm-12 text-center output_message_holder">
                                    <p class="output_message" id="output_message_newsletter"></p>
                                </div>
                                <!-- Form Submit Message Ends -->
                            </div>
                            <!-- Newsletter Form Input Field Ends -->
                            <!-- Newsletter Form Submit Button Starts -->
                            <button id="enviar_newsletter" name="submit" type="button" class="custom-button" title="Send">Send</button>
                            <!-- Newsletter Form Submit Button Ends -->
                        </form>
                        <!-- Newsletter Form Ends -->
                    </div>
                </div>
                <!-- Container Ends -->
            </div>
        </section>
        <!-- Newsletter Section Ends -->
        <!-- Contact Section Starts -->
        <section id="contact" class="contact">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Contact</span></h1>                   
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-envelope" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
            </div>
            <!-- Container Ends -->
            <!-- Map Section Starts -->
            <div class="info-map">
                <div class="google-map">                    
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3661.687696628723!2d-46.3295730844523!3d-23.399512661521648!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce8781654bd9bb%3A0xf0545ab63ac06be0!2sAv.+Get%C3%BAlio+Vargas%2C+280+-+Jardim+Vitoria%2C+Aruj%C3%A1+-+SP%2C+07432-575!5e0!3m2!1spt-BR!2sbr!4v1516099257753" width="2000" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <!-- Info Map Boxes Starts -->
            <div class="container">
                <div class="row info-map-boxes">
					<!-- left Info Map Box Starts -->
                    <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-map-marker">
                            <h1>Address</h1>
								<p>Avenida Getúlio Vargas, 280 
								<br/>Quadra C - Lote 4  - Jardim Ângelo 
								<br/>07400-230 - Arujá - SP - BR																	
								</p>
                        </div>
                    </div>
					
                    <!-- left Info Map Box Ends -->
                    <!-- center Info Map Box Starts -->
                    <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-phone">
                            <h1>Phone &amp; E-mail</h1>
								<p>Phone : +55 11 3900-2565									
									<br><a href="#">vendas@wertco.com.br</a><br/>
								</p>
                        </div>
                    </div>
                    <!-- center Info Map Box Ends -->
                    <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-wrench">
                            <h1>Technical assistance</h1>
                            <p>Phone: +55 11 3900 - 2565 <br/>
                            <a href="#">servicos@wertco.com.br</a><br/>    
                            </p>
                        </div>
                    </div>    
					 <!-- Right Info Map Box Starts -->
					 <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-clock-o">
                            <h1>Attendance</h1>
                            <p>Monday-Friday : 08:00–17:00                                
                                </p>
                        </div>
                    </div>
                    
                    <!-- Right Info Map Box Ends -->

                </div>
            </div>
            <!-- Info Map Boxes Ends -->
        </section>
        <!-- Contact Section Ends -->
        <!-- Contact Form Section Starts -->
        <section class="contactform" style="-webkit-transform: skewY(0deg) !important;transform: none !important;">
            <div class="section-overlay">
                <div class="container" style="-webkit-transform: skewY(0deg) !important;transform: skewY(0deg) !important;">
                    <!-- Main Heading Starts -->
                    <div class="text-center top-text">
                        <h1><span>Contact Us</span></h1>                        
                    </div>
                    <!-- Main Heading Ends -->
                    <div class="form-container">
                        <!-- Contact Form Starts -->
                        <form class="formcontact" method="post" >
                            <div class="row form-inputs">
                                <!-- First Name Field Starts -->
                                <div class="col-md-6 form-group custom-form-group">
                                    <span class="input custom-input">
										<input placeholder="Name" class="input-field custom-input-field" id="firstname" name="firstname" type="text" required data-error="NEW ERROR MESSAGE">
										<label class="input-label custom-input-label" >
											<i class="fa fa-user icon icon-field"></i>
										</label>
									</span>
                                </div>
                                <!-- First Name Field Ends -->
                                <!-- Last Name Field Starts -->
                                <div class="col-md-6 form-group custom-form-group">
                                    <span class="input custom-input">
										<input placeholder="Last Name" class="input-field custom-input-field" id="lastname" name="lastname" type="text" required>
										<label class="input-label custom-input-label" >
											<i class="fa fa-user-o icon icon-field"></i>
										</label>
									</span>
                                </div>
                                <!-- Last Name Field Ends -->
                                <!-- Phone Field Starts -->
                                <div class="col-md-12 form-group custom-form-group">
                                    <span class="input custom-input">
                                        <input placeholder="Phone" class="input-field custom-input-field" id="telefone" name="phone" type="text" required>
                                        <label class="input-label custom-input-label" >
                                            <i class="fa fa-phone icon icon-field"></i>
                                        </label>
                                    </span>
                                </div>
                                <!-- Phone Field Ends -->
                                <!-- Message Field Starts -->
                                <div class="form-group custom-form-group col-md-12">
                                    <textarea placeholder="Message" id="message" name="message" cols="45" rows="7" required></textarea>
                                </div>
                                <!-- Message Field Ends -->
                                <!-- Email Name Field Starts -->
                                <div class="col-md-6 form-group custom-form-group">
                                    <span class="input custom-input">
										<input placeholder="E-mail" class="input-field custom-input-field" id="email_contact" name="email_contact" type="text" required>
										<label class="input-label custom-input-label" >
											<i class="fa fa-envelope icon icon-field"></i>
										</label>
									</span>
                                </div>
                                <!-- Email Name Field Ends -->
                                <!-- Submit Button Starts -->
                                <div class="col-md-6 submit-form">
                                    <button id="form-submit" name="submit" type="button" class="custom-button" title="Send">Send</button>
                                </div>
                                <!-- Submit Button Ends -->
                                <!-- Form Submit Message Starts -->
                                <div class="col-sm-12 text-center output_message_holder"> 
                                    <p class="output_message" id="contato"></p>
                                </div>
                                <!-- Form Submit Message Ends -->
                            </div>
                        </form>
                        <!-- Contact Form Ends -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact Form Section Ends -->
        <!-- Logos Section Starts -->
        <section class="logos">
          <!-- Footer Section Starts -->
        <footer class="footer text-center">
            <!-- Container Starts -->
            <div class="container">                
                <p>Wertco Ind. Com. and Maintenance Services in Pumps LTDA.<br/>CNPJ.: 27.314.980/0001-53 </p>
                <p>
                    &copy; Copyright <?php echo date('Y')?> Wertco 
                </p>                
            </div>
            <!-- Container Ends -->
            <div class="social-icons" style="margin-bottom: 40px;">
                <ul class="social">
                    <!--<li>
                        <a class="twitter" href="#" title="twitter"></a>
                    </li>-->
                    <li>
                        <a class="facebook" href="https://www.facebook.com/Wertco-146223742724238/" title="facebook"></a>
                    </li>
                    <!--<li>
                        <a class="google" href="#" title="google"></a>
                    </li>
                    <li>
                        <a class="skype" href="#" title="skype"></a>
                    </li>-->
                    <li>
                        <a class="instagram" href="https://www.instagram.com/wertco_bombas/" title="instagram"></a>
                    </li>
                    <!--<li>
                        <a class="linkedin" href="#" title="linkedin"></a>
                    </li>
                    <li>
                        <a class="youtube" href="#" title="youtube"></a>
                    </li>-->
                </ul>
            </div>
            <div style="margin-right: 13px; ">
                <a href="http://www.companytec.com.br" target="blank">
                    <img src="<?php echo base_url('bootstrap/img/logoCompanytec.png')?>" /> 
                </a>
            </div>
        </footer>
        <!-- Footer Section Starts -->
        </section>
        <!-- Logos Section Ends -->
        
        <!-- Back To Top Starts -->
        <div id="back-top-wrapper" class="d-none d-sm-block">
            <p id="back-top">
                <a href="index.html#top"><span></span></a>
            </p>
        </div>
        <!-- Back To Top Ends -->
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyBpN0oc0NeZv9JolJSIzQRNW9IkUOfKrxw"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <!--<script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>-->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js')?>"></script>

    <!-- Revolution Slider Main JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/jquery.themepunch.tools.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/jquery.themepunch.revolution.min.js')?>"></script>

    <!-- Revolution Slider Extensions -->

    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.actions.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.carousel.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.migration.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.navigation.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.parallax.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.video.min.js')?>"></script>
    
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>
    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>

    <!-- Revolution Slider Initialization Starts -->
    <script type="text/javascript">
	(function(){
		"use strict";
        var tpj = jQuery;
        var revapi4;
        tpj(document).ready(function() {
            if (tpj("#rev_slider").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider");
            } else {
                revapi4 = tpj("#rev_slider").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "js/plugins/revolution/js/",
                    dottedOverlay: "none",
					sliderLayout:"fullscreen",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "zeus",
                            enable: true,
                            hide_onmobile: true,
                            hide_under: 600,
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            tmp: '<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 90,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 90,
                                v_offset: 0
                            }
                        },
                        bullets: {
                            enable: false,
                            hide_onmobile: true,
                            hide_under: 600,
                            style: "metis",
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            direction: "horizontal",
                            h_align: "center",
                            v_align: "bottom",
                            h_offset: 0,
                            v_offset: 30,
                            space: 5,
                            tmp: '<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span>'
                        }
                    },
                    viewPort: {
                        enable: true,
                        outof: "pause",
                        visible_area: "80%"
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [600, 600, 500, 400],
                    lazyType: "none",
                    parallax: {
                        type: "mouse",
                        origo: "slidercenter",
                        speed: 2000,
                        levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
                    },
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    hideThumbsOnMobile: "off",
                    autoHeight: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        });
		
		// GOOGLE MAP
		function init_map() {
			
			var myOptions = {
				scrollwheel: false,
				zoom: 12,
				center: new google.maps.LatLng(-23.3994819,-46.3279319,19),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
			var marker = new google.maps.Marker({
				map: map,
				icon: "img/markers/yellow.png",
				position: new google.maps.LatLng(-23.3994819,-46.3279319,19)
			});
			var infowindow = new google.maps.InfoWindow({
				content: "<strong>WERTCO</strong><br>São Paulo<br>"
			});
			google.maps.event.addListener(marker, "click", function() {
				infowindow.open(map, marker);
			});
		}
		google.maps.event.addDomListener(window, "load", init_map);
			
	})(jQuery);
    </script>
    <!-- Revolution Slider Initialization Ends -->
</body>

</html>