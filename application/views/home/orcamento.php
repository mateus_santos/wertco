<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from celtano.top/salimo/demos/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
<head>
    <meta charset="utf-8" />
    <title>Solicitação de Orçamentos - WERTCO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    -->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url('bootstrap/img/favicon.png'); ?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/font-awesome.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/magnific-popup.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/style.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/skins/yellow.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/bootstrap-select.min'); ?>" />
    <script type="text/javascript">
        var base_url = "<?php echo base_url();?>";
    </script>

    <!-- Template JS Files -->
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/modernizr.js'); ?>"></script>

</head>

<body class="double-diagonal blog-page dark">    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
                    <!-- Logo Starts -->
                    <div class="logo">
                        <a class="navbar-brand link-menu nav-external" href="index-slideshow.html">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?php echo base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png'); ?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            <!-- Logo Black Starts -->
                            <img id="logo-dark" class="logo-dark" src="<?php echo base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png'); ?>" alt="logo-dark" />
                            <!-- Logo Black Ends -->
                        </a>
                    </div>
                    <!-- Logo Ends -->
                    <!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span id="icon-toggler">
                          <span></span>
                          <span></span>
                          <span></span>
                          <span></span>
                        </span>
                    </button>
                    <!-- Hamburger Icon Ends -->
                    <!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external" href="<?php echo base_url('home/index'); ?>"><i class="fa fa-arrow-left"></i> Voltar ao Site</a></li>
                        </ul>
                    </div>
                    <!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
        <!-- Banner Starts -->
        <section class="banner" >
            <div class="content text-center">
                <div class="text-center top-text">
                    <h1>POLÍTICA DE PRIVACIDADE - LGPD</h1>
                    <hr>                    
                </div>
            </div>
        </section>
        
        <!--    Banner Ends        -->
        <!--    Services Begin     -->
        <section id="services" class="services">
            
            <!-- Container Starts -->
            <div class="container" style="background: #333;border-radius: 10px;">
                    <div class="modal-content " style="padding: 20px;">               
<p style="font-size: 10px;color: #000;">A Wertco Indústria, Comércio e Serviços em Bombas de Abastecimento de Combustíveis, Importação e Exportação LTDA. não quer assediar ninguém, muito menos usar seus dados para fins indevidos, sejam eles quais forem.<br>

No entanto, é necessário deixar claro que coletaremos alguns de seus dados em nosso site, sejam eles COMPULSÓRIOS ou ESPONTÂNEA, o que explicaremos a seguir.<br>

Se você tem menos de 18 anos, infelizmente, não será possível utilizar nossas plataformas, pois todas elas capturam, de alguma forma, seus dados. Portanto, se seguir, você atesta sua maioridade.<br>

Então, estando ciente de nossa POLÍTICA DE PRIVACIDADE, você declara que concorda com todas as implicações — que verá, não são nada demais.<br>

O recolhimento de dados de forma COMPULSÓRIA é feito através de cookies de quaisquer tipos e outras ferramentas tecnológicas. Mas ele serve para sabermos que páginas foram visitadas por você, quantas vezes, horários e durações. Se algum dia, você se cadastrou em nossa plataforma, a gente tem a possibilidade de saber que foi você que nos visitou novamente e como o fez. Porém, se você nunca se cadastrou conosco, a gente não faz ideia de quem você seja, e só temos acesso a dados anônimos de uso.<br>

O recolhimento de dados de forma ESPONTÂNEA é aquele quando você preenche um formulário. Neste momento, você nos informa que concordou com nossa Política de Privacidade também. Quando isso acontece, você o faz por interesse próprio em nossos produtos, serviços ou informações. Nesse caso, para conseguirmos lhe oferecer o que deseja, adicionamos você a nossa base. Chamamos as pessoas em nossa base de “leads”. Assim, quando alguém cadastrado visita nossas páginas novamente, estaremos registrando seus passos conosco, da forma COMPULSÓRIA expressa acima, para compreendermos que tipo de conteúdo nossos leads mais visitam e podermos criar conteúdos mais eficientes. É só isso.<br>

Quando a gente percebe que um lead da base, que consuma nosso conteúdo ou não, é um potencial cliente, talvez a gente envie (caso ele consinta no formulário) alguns e-mails esporádicos sobre novidades da empresa. Nesses emails sempre há um convite ao descadastramento da base. No caso mais extremo, é possível também que a gente entre em contato para oferecer um serviço ou convidá-lo para ser parceiro em algum projeto.<br>

Claro que também iremos usar o contato fornecido em questões contratuais de clientes por legítimo interesse, em casos que a qualidade da entrega de nosso trabalho exija contato ou mesmo repassar os dados para um fornecedor realizar uma entrega, por exemplo.<br>

É bom citar também, que todas as plataformas (sejam as contratadas por nós, sejam as redes sociais) podem coletar também dados como localização e até alguns que provavelmente não teremos interesse, como IP etc. Fazem isso por uma questão de segurança deles e, até mesmo sua, caso alguém esteja tentando se cadastrar com o seu nome.<br>

Você pode ter total certeza que a gente não utiliza, nem NUNCA utilizará, seus dados para quaisquer outros fins, muito menos cederemos a terceiros que nada tenham a ver com eles.<br>

É bom lembrar também que em nossas mídias sociais (salvo quando publicarmos algum formulário de captação, no qual você mesmo insere os dados) a gente não transfere nenhum tipo de informação sua para nossas bases de dados. Por exemplo, se você comentou em um post do Facebook, curtiu uma foto no Instagram ou retuitou algo pelo Twitter, esses dados ficam apenas lá nessas redes, seguindo a política de privacidade de cada uma. Agora, se a gente colocar algum formulário de captação por lá, aí sim, você concorda com a Política de Privacidade específica da rede em questão e com a nossa, funcionando conforme falamos acima sobre coleta ESPONTÂNEA.<br>

Esses dados ficam armazenados nessas plataformas com acesso online, e pelo tempo necessário para o interesse do cadastro, seguindo todos os protocolos de segurança que elas oferecem.<br>

Como ninguém fica parado no tempo, a gente se vê no direito de alterar esta POLÍTICA DE PRIVACIDADE toda vez que se fizer necessário. Por exemplo, quando adotarmos alguma ferramenta nova com novas funções que obriguem a ajustes por aqui. Mas tenha sempre a certeza de que faremos dentro dos limites de bom senso e zelando ao extremo pela sua privacidade e segurança.<br>

Por último, resta reforçar que você tem o direito a qualquer momento de solicitar todo e qualquer dado que armazenemos a seu respeito em nossas plataformas, bem como retificar, requerer exclusão, transferir, limitar, se opor ao tratamento ou retirar o consentimento. Esse procedimento pode ser feito através do email marketing[arroba]companytec.com.br (escrevemos assim para o endereço não ser capturado por robôs e ser usado sem o nosso consentimento. Você entendeu, né? Caso não tenha entendido, nos procure por qualquer outro meio disponível em nossos pontos de contato.<br>

Sendo assim, gostaríamos de finalizar dizendo que prezamos fortemente pela ética, segurança e privacidade dos usuários de nossas plataformas.<br>

Obrigado</p>
            </div>      
            </div>
        </div>        
        <!-- Services Ends -->
        </section>
        <!-- Service Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center footer-area-restrita" style="border-top: 2px solid #ffcc00 !important;">
            <!-- Container Starts -->
            <div class="container">
               <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/0001-53 </p>
                <p>
                    &copy; Copyright <?=date('d/m/Y')?> Wertco. &nbsp;<a href="<?php echo base_url('usuarios/login'); ?>"><i class="fa fa-lock"></i></a>  
                </p>                
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
    </div>
    <!-- Wrapper Ends -->
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/jquery-2.2.4.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.easing.1.3.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/bootstrap.bundle.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.bxslider.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.filterizr.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js'); ?>"></script>    
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>     
    <script type="text/javascript" src="<?=base_url('bootstrap/js/sweetalert.js')?>"></script>
    <script src="<?=base_url('bootstrap/js/plugins/jquery.blockui.js')?>" type="text/javascript"></script>

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Orçamento solicitado com sucessoo, em breve entraremos em contato!',
                type: 'success'
            }).then(function() {
                window.location = base_url+'Home/orcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>

    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/orcamento.js'); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
            $('.add').bind('click', function(){
                var html = $('#modelo').clone();                
                var indice = parseInt($('#modelo').attr('total_indice')) + 1;
                $(html).find('.remover').css('display', 'block');
                $(html).find('.remover').attr('indice', indice);
                body = "<tr indice='"+indice+"'>";
                body+= $(html).html();                
                body+="</tr>";                
                $('.table').append(body);                
                $('#modelo').attr('total_indice',indice);

                excluir();
            });

            $('#cnpj').mask('00.000.000/0000-00');
            $('#cpf').mask('000.000.000-00');
            $('#bombas').mask('00000');
            $('#bicos').mask('00000');  
            $('.qtd').mask('000');

            $('#cnpj').bind('focusout', function(){
                var cnpj = $(this).val(); 

                if(cnpj != ""){
                    if(isCNPJValid(cnpj)) {
                        $.ajax({
                            method: "POST",
                            url: base_url+'clientes/verificaOrcamento',
                            async: true,
                            data: { cnpj    :   cnpj }
                            }).done(function( data ) {
                                var dados = $.parseJSON(data);
                                if( dados.total == 0){

                                    $.ajax({
                                        method: "POST",
                                        url: base_url+'clientes/verifica_cnpj',
                                        async: false,
                                        data: { cnpj    :   cnpj }
                                        }).done(function( data ) {
                                            var dados = $.parseJSON(data);                          
                                            if(dados.length > 0){
                                                
                                                $('#razao_social').val(dados[0].razao_social);      
                                                $('#razao_social').attr('disabled', true);      
                                                $('#fantasia').val(dados[0].fantasia);      
                                                $('#fantasia').attr('disabled', true);
                                                $('#telefone').val(dados[0].telefone);
                                                $('#telefone').attr('disabled', true);
                                                $('#endereco').val(dados[0].endereco);      
                                                $('#endereco').attr('disabled', true);                                  
                                                $('#email').attr('disabled', true);
                                                $('#email').val(dados[0].email);
                                                $('#cidade').attr('disabled', true);
                                                $('#cidade').val(dados[0].cidade);  
                                                $('#estado').attr('disabled', true);
                                                $('#estado').val(dados[0].estado);                        
                                                $('#pais').attr('disabled', true);
                                                $('#pais').val(dados[0].pais);                        
                                                $('#salvar').val('0');
                                                $('#empresa_id').val(dados[0].id);
                                                $('#inscricao_estadual').attr('disabled', true);
                                                $('#inscricao_estadual').val(dados[0].insc_estadual);
                                                $('#cep').attr('disabled', true);
                                                $('#cep').val(dados[0].cep);
                                                $('#bairro').attr('disabled', true);
                                                $('#bairro').val(dados[0].bairro);

                                                
                                            }else{

                                                var cnpj_sem_mascara = cnpj.replace(/[^\d]+/g,'');

                                                $.ajax({
                                                    method: "POST",
                                                    url: base_url+'Home/ValidaCnpj',
                                                    async: false,
                                                    data: { cnpj    :   cnpj_sem_mascara }
                                                }).done(function( data ) {
                                                    var dados = $.parseJSON(data);   
                                                    
                                                    if (dados.receita.erro == 0 && dados.receita.situacao == "ATIVA" ){
                                                        $('#razao_social').attr('disabled', false);     
                                                        $('#razao_social').val(dados.receita.retorno.razao_social);
                                                        $('#razao_social').attr('style','border-color: #5fda17;');
                                                        $('#fantasia').attr('disabled', false);
                                                        $('#fantasia').val(dados.receita.retorno.nome_fantasia );
                                                        $('#fantasia').attr('style','border-color: #5fda17;');
                                                        $('#telefone').attr('disabled', false);
                                                        $('#telefone').val(dados.receita.retorno.telefone);
                                                        $('#telefone').attr('style','border-color: #5fda17;');
                                                        $('#endereco').attr('disabled', false); 
                                                        $('#endereco').val(dados.receita.retorno.logradouro+', '+dados.receita.retorno.numero+' '+dados.receita.retorno.complemento);                                
                                                        $('#endereco').attr('style','border-color: #5fda17;');
                                                        $('#email').attr('disabled', false); 
                                                        $('#email').val('');                                                                                                
                                                        $('#cidade').attr('disabled', false); 
                                                        $('#cidade').val(dados.receita.retorno.municipio_ibge);  
                                                        $('#cidade').attr('style','border-color: #5fda17;');
                                                        $('#estado').attr('disabled', false); 
                                                        $('#estado').val(dados.receita.retorno.uf);
                                                        $('#estado').attr('style','border-color: #5fda17;');
                                                        $('#bairro').attr('disabled', false); 
                                                        $('#bairro').val(dados.receita.retorno.bairro);
                                                        $('#bairro').attr('style','border-color: #5fda17;');
                                                        $('#cep').attr('disabled', false); 
                                                        $('#cep').val(dados.receita.retorno.cep);                          
                                                        $('#cep').attr('style','border-color: #5fda17;');
                                                        $('#pais').attr('disabled', false);
                                                        $('#pais').attr('style','border-color: #5fda17;');                                                
                                                        $('#inscricao_estadual').attr('disabled', false);
                                                        $('#inscricao_estadual').val('');                        
                                                        $('#cartao_cnpj').val(dados.receita.save);   
                                                        $('#codigo_ibge_cidade').val(dados.receita.retorno.codigo_ibge);                  
                                                        
                                                        $('#pais').val('Brasil');         
                                                        $('#salvar').val('1');
                                                    }else{
                                                       swal({
                                                            title: "Atenção!",
                                                            text: dados.msg,
                                                            type: 'warning'
                                                        }).then(function() {
                                                            $('#cnpj').val('');
                                                        });

                                                    }
                                                });

                                        }                           
                                    });  
                                }else{

                                    swal({
                                        title: "Atenção!",
                                        text: "Já existe um orçamento em andamento para este cliente, entre em contato com a WERTCO!",
                                        type: 'warning'
                                    }).then(function() {
                                        $('#cnpj').val('');
                                    }); 
                               }
                            });       
                    }else{

                        swal({
                            title: 'CNPJ inválido!',
                            text: 'Favor verificar o CNPJ informado',
                            type:'error'
                        }).then(function(isConfirm) {
                            $('#cnpj').val('');
                            $('#cnpj').focus();
                        })
                    
                    }
            }
        });
    });       

        function excluir()
        {
            $('.remover').bind('click', function(){
                var indice = $(this).attr('indice');
                $('tr[indice="'+indice+'"]').remove();
            });
        }

        function isCNPJValid(cnpj) {  
            
            var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
            if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
                return false;
            for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
            if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
                return false; 
            for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
            if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
                return false; 
            return true; 
        };

	
    </script>
	<!--Start of Tawk.to Script   -->
	<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5defe65043be710e1d2179f6/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
	</script>
	<!--End of Tawk.to Script-->
</body>

</html>
