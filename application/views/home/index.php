<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8" />
    <title>WERTCO - BOMBAS DE GASOLINA - BOMBAS DE COMBUSTÍVEIS - ABASTECENDO SOLUÇÕES</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    -->     
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon1.png')?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />

    <!-- Revolution Slider CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/settings.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/layers.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/navigation.css')?>" />

    <!-- Live Style Switcher - demo only --> 
    <link rel="alternate stylesheet" type="text/css" title="yellow" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen" href="<?=base_url('bootstrap/css/skins/yellowgreen.css')?>" />
    
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

    <!-- Accordion TTU -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/accordion.css')?>" />
    <script type="text/javascript">
        var base_url = "<?php echo base_url();?>"; 
    </script>
</head>

<body class="dark double-diagonal">
    <!-- Preloader Starts -->
    <div class="preloader" id="preloader">
        <div class="logopreloader">
            <img class="img-fluid" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png')?>" alt="logo-black" />
        </div>
        <div class="loader" id="loader"></div>
    </div>    
    <!-- Preloader Ends -->    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">                
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
                    <!-- Logo Starts -->
                    <div class="logo">
                        <a data-toggle="collapse" data-target=".navbar-collapse.show" class="navbar-brand link-menu scroll-to-target" href="#mainslider">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />                            
                        </a>
                    </div>
                    <!-- Logo Ends -->
                    <!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span id="icon-toggler">
                          <span></span>
                          <span></span>
                          <span></span>
                          <span></span>
                        </span>
                    </button>
                    <!-- Hamburger Icon Ends -->
                    <!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a href="<?php echo base_url('home/index/esp'); ?>" id="espanhol"><img src="<?php echo base_url('bootstrap/img/espanhol.png');?>"></a></li>
                            <li><a href="<?php echo base_url('home/index/eng'); ?>"  id="ingles"><img src="<?php echo base_url('bootstrap/img/ingles.png');?>"></a></li>
                            <li>&nbsp;</li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#mainslider"><i class="fa fa-home"></i> Home</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#about"><i class="fa fa-user"></i> WERTCO</a></li>
                            <li id='bombas_ico'><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#testimonials"><img src="<?=base_url('bootstrap/img/bombabranca.png')?>" class=" bombas_ico"> BOMBAS</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#services"><i class="fa fa-arrow-up "></i> Diferenciais</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#inovadoras"><i class="fa fa-microchip "></i> Características</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#certificacoes"><i class="fa fa-certificate "></i> Certificações</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#tutoriais"><i class="fa fa-play "></i> Tutoriais</a></li>                            
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#blog"><i class="fa fa-comments "></i> Notícias</a></li>                                                     
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#contact"><i class="fa fa-envelope "></i> Contato</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#trabalhe_conosco"><i class="fa fa-id-badge "></i> Trabalhe Conosco</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" href="<?php echo base_url('home/orcamento');?>" target="_blank" class="link-menu link-externo" link="orcamento"><i class="fa fa-th-list"></i> Orçamento</a></li>
                             <li class="bombas_menu"><a href="<?php echo base_url('tecnicos/cadastro/1');?>" target="_blank" class="link-menu link-externo" link="capacitacao"><i class="fa fa-gear fa-spin"></i> Capacitação</a></li>
                            <li class="bombas_menu"><a href="<?php echo base_url('home/area_restrita');?>" target="_blank" class="link-menu link-externo" link="area_restrita"><i class="fa fa-sign-in"></i> Área Restrita</a></li>
                        </ul>
                    </div>
                    <!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
        <!-- Header Ends -->
        <!-- Main Slider Section Starts -->
        <section class="mainslider" id="mainslider">
            <!-- Slider Hero Starts -->
            <div class="rev_slider_wrapper fullwidthbanner-container dark-slider" data-alias="vimeo-hero" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
                <div id="rev_slider" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-index="rs-18" data-transition="zoomin" data-slotamount="7" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/fundopostowertco.jpg')?>" data-rotate="0" data-saveperformance="off" data-title="Ken Burns" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/fundopostowertco.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">WERTCO 
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap; font-size: 22px !important; height: 35px !important; margin-top: 10px !important; ">ABASTECENDO SOLUÇÕES
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#about" class="custom-button slider-button scroll-to-target">Saiba Mais Sobre a WERTCO</a></div>
                        </li>
                        <!-- SLIDE  -->
                        <li data-index="rs-2" data-transition="zoomin" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/quatroBombas1.jpg')?>" data-rotate="0" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/quatroBombas1.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">CONFIABILIDADE
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#testimonials" class="custom-button slider-button scroll-to-target">Saiba mais sobre as bombas WERTCO</a></div>
                        </li>
                        <!-- SLIDE  -->
                        <li data-index="rs-3" data-transition="zoomin" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/duasBombas.jpg')?>" data-rotate="0" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/duasBombas.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">DURABILIDADE
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#testimonials" class="custom-button slider-button scroll-to-target">Saiba mais sobre as bombas WERTCO</a></div>
                        </li>
                        <li data-index="rs-4" data-transition="zoomin" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/tresBombas1.jpg')?>" data-rotate="0" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/tresBombas1.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">POSSIBILIDADES
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#testimonials" class="custom-button slider-button scroll-to-target">Saiba mais sobre as bombas WERTCO</a></div>
                        </li>
                    </ul>
                    <div class="tp-static-layers"></div>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->
            <!-- Slider Hero Ends -->
        </section>
        <!-- Main Slider Section Ends -->
        <!-- About Section Starts -->
        <section id="about" class="about">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Sobre</span> Nós</h1>
                    <!--<h4></h4>-->
                </div>
                <!-- Main Heading Ends -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-user" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- About Content Starts -->
                <div class="row about-content">
                    <div class="col-sm-12 col-md-12 col-lg-12 about-left-side">
                        <h3 class="title-about"><strong>WERTCO</strong></h3>
                        <hr>
                        <p>A WERTCO é uma empresa brasileira do grupo COMPANYTEC, fabricante de bombas de combustíveis <span style="display: none;">companytec, breakaway e e bicos de abastecimento</span>
                            projetadas para atender a nova portaria MTPS nº 1109 e outras exigências do
                            mercado, respeitando as normas de saúde, metrologia e, principalmente, inibição de
                            fraudes de abastecimento.
                        </p>
                        <p>Suprindo a necessidade do mercado por bombas de combustíveis de alta segurança, conectividade e
                            sustentabilidade, a Wertco inova com produtos confiáveis, duráveis, modernos, de
                            fácil integração e com mais possibilidades e funcionalidades para o negócio.
                        </p>
                        <p>Formada por profissionais com knowhow no mercado de fabricação de bombas de
                            combustíveis e somados ao grupo de desenvolvimento de eletrônica da Companytec,
                            a Wertco desenvolve produtos baseada em três valores importantes: Confiabilidade,
                            Durabilidade e Possibilidades.
                        </p>                       
                       
                        <div class="row">                            
                            <figure class="figure col-lg-12 col-xs-12">
                                <img src="<?=base_url('bootstrap/img/fachadaWERTCO2.jpg')?>" class="figure-img img-fluid rounded" alt="Fabrica WERTCO. Companytec e bombas de GASOLINA">
                                <!--<figcaption class="figure-caption">Fabrica WERTCO.</figcaption>-->
                            </figure>
                            <!--<figure class="figure col-lg-6 col-xs-12">
                                <img src="<?=base_url('bootstrap/img/equipe_wertco.png')?>" class="figure-img img-fluid rounded" alt="Equipe WERTCO. Companytec e bombas de GASOLINA">
                                <figcaption class="figure-caption">Fabrica WERTCO.</figcaption>
                                </figure>-->
                        </div>
                        <a class="custom-button slider-button scroll-to-target" href="#testimonials">Conheça nossas Bombas de Combustíveis</a>
                    </div>                   
                <!-- About Content Ends -->
            </div>
            <!-- Container Ends -->
        </section>
        <!-- About Section Ends -->
        <!-- BOMBAS DE ABASTECIMENTO Section Starts -->
        <section id="testimonials" class="testimonials">
           <div class="section-overlay">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Bombas </span>de COMBUSTÍVEIS</h1>
                    <h4></h4>
                </div>
                <!-- Main Heading Ends -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <img src="<?=base_url('bootstrap/img/bombaIcoG.png')?>" />
                    <span class="outer-line"></span>
                </div>               
                
                <div class="row team-members">
                
                    <!-- Team Member Starts -->
                    <div class="col-lg-3 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <div class="team-member">
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CLH (Mangueira Baixa)<br/></h4>                              
                                <ul class="list list-inline social">
                                   <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="fa fa-search-plus" href="<?=base_url('bootstrap/img/bombas/infoCLH.png')?>"></a>
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraisclh.png')?>" style='display: none;'></a>
                                    </li>
                                </ul>
                                 <a title="CLH Mangueira baixa" href="#" class="team-member-img-wrap">
                                <img src="<?=base_url('bootstrap/img/bombas/CLH.png')?>" alt="Bomba de gasolina - CLH - bombas de combustíveis" title="Bomba de gasolina - CLH"/></a>
                            </div>
                            <!-- Team Member Picture Starts -->
                           
                            <!-- Team Member Picture Ends -->
                            <!-- Team Member Details Starts -->
                            
                            <!-- Team Member Details Starts -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->
                     <!-- Team Member Starts -->
                    <div class="col-lg-3 col-md-6 col-sm-12 magnific-popup-gallery">
                        <div class="team-member">                           
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CLH com Opção de Pórtico</h4>                               
                                <ul class="list list-inline social">
                                   <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="fa fa-search-plus mfp-iframe" href="https://player.vimeo.com/video/340531905"></a>
                                    </li>
                                    
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraisclh.png')?>" style='display: none;'></a>
                                    </li>
                                </ul>
                                 <!-- Team Member Picture Starts -->
                                <a title="CLH Mangueira baixa Com opção de pórtico" href="#" class="team-member-img-wrap mfp-iframe">
                                <img src="<?=base_url('bootstrap/img/bombas/CLHPORTICO.png')?>" alt="Bomba de combustíveis - CLH com pórtico - Bomba de gasolina" title="Bomba de combustíveis - CLH com pórtico - Bomba de gasolina" /></a>
                            <!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Starts -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->
                    <!-- Team Member Starts -->
                    <div class="col-lg-3 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <div class="team-member">
                            
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CHHS (Mangueira Alta Slim)</h4>                                   
                                <ul class="list list-inline social">
                                    <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas fa fa-search-plus" href="<?=base_url('bootstrap/img/bombas/infoCHHS.png')?>"></a>
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraischhs.png')?>" style='display: none;'></a>
                                    </li>
                                </ul>
                                <!-- Team Member Picture Starts -->
                                <a title="CHHS | Mangueira Alta" href="#" class="bombas team-member-img-wrap">
                                    <img src="<?=base_url('bootstrap/img/bombas/CHHS.png')?>" alt="Bomba de gasolina - Bombas de combustíveis - CHHS" title="Bombas de combustíveis - CHHS - Bomba de gasolina">
                                </a>
                                <!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Ends -->
                        </div>
                    </div>
                <!--
                </div>
                <div class="row team-members">-->    
                    <!-- Team Member Ends -->
                    <!-- Team Member Starts -->
                    <div class="col-lg-3 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <!-- Team Member-->
                        <div class="team-member">                            
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CHH (Mangueira Alta) </h4>                                    
                                <ul class="list list-inline social">
                                    <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas fa fa-search-plus" href="<?=base_url('bootstrap/img/bombas/infoCHH.png')?>">  </a>
                                        
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraischh.png')?>" style='display: none;'></a>
                                    </li>
                                </ul>
                                <!-- Team Member Picture Starts -->
                                <a title="CHH (Mangueira Alta)" href="#" class="bombas team-member-img-wrap">
                                    <img src="<?=base_url('bootstrap/img/bombas/CHH.png')?>" alt="Bombas de combustíveis - CHH" title="Bomba de combustíveis - CHH">
                                </a>
                                <!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Ends -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->                    
                    <?php /* <!-- Team Member Starts -->
                    <div class="col-lg-4 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <!-- Team Member-->
                        <div class="team-member">                            
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">Dispenser Eletrônico para Comboio </h4>
                                <ul class="list list-inline social">
                                    <li>
                                        &nbsp;
                                    </li>  
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas fa fa-search-plus " href="<?=base_url('bootstrap/img/bombas/medidor_info.png')?>">                                            
                                        </a>                     
                                    </li>                                  
                                    <!--<li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas team-member-img-wrap" href="<?=base_url('bootstrap/img/bombas/medidor_info.png')?>" style='display: none;'></a>
                                    </li>-->
                                </ul>
                                <!-- Team Member Picture Starts -->
                                <a title="CHH (Mangueira Alta)" href="#" class="bombas team-member-img-wrap">
                                    <img src="<?=base_url('bootstrap/img/bombas/medidor.png')?>" alt="Electronic Dispenser for Train" title="Electronic Dispenser for Train " >
                                </a>
                                <!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Ends -->
                        </div>
                    </div>
                    */ ?>
                    <!-- Team Member Ends -->                    
                </div>
                <!-- Team Members Ends -->              
            </div>
            </div>
            <!-- Container Ends -->
          </div>  
        </section>
        <!-- Team Section Ends -->
        <!-- Services Section Starts -->
        <section id="services" class="services">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Nossos</span> Diferenciais</h1>
                    <h4>Produtos pensados para o mercado brasileiro</h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-arrow-up" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- Services Starts -->
                <div class="row services-box">
                     <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-handshake-o" data-headline="Confiabilidade"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-handshake-o">
                            <h2>Confiabilidade</h2>
                            <p>Eletrônica desenvolvida pela Companytec Automação e Controle, líder no mercado brasileiro de automação para postos de combustíveis.<br/>
Parceria comercial com empresa Norte Americana Bennett para fornecimento de conjuntos hidráulicos que são referência de alto desempenho, fácil manutenção e super-resistência às características de operação das bombas no Brasil.</p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                    <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-cubes" data-headline="Durabilidade"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-cubes">
                            <h2>Durabilidade</h2>
                            <p>Produtos concebidos com materiais utilizados na indústria aeronáutica e automobilística, que garantem maior resistência aos desgastes decorrentes do uso dos equipamentos, além de um design diferenciado e ótimo acabamento.</p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                </div>
                <!-- Services Starts -->
                <div class="row services-box">    
                     <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-random" data-headline="Possibilidades"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-random">
                            <h2>Possibilidades</h2>
                            <p>Conectividade e Integração com sistemas e dispositivos.<br/>
                                Preparação para receber sistemas de extração de vapor.<br/>
                                Design modular e racional.</p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                     <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-lock" data-headline="Alta Segurança"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-lock">
                            <h2>Alta Segurança</h2>
                            <p>Eletrônica desenvolvida para atender requisitos de segurança elétrica e robustez. Pensada para dificultar, ao máximo, as mais diversas tentativas de fraude que existem no mercado.</p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                   
                </div>

                </div>
                <!-- Services Ends -->
            </section>
             <!-- begin bomba mutcho -->            
            <section class="contactform" id="inovadoras" style="margin-top: -55px;">
                <div class="section-overlay">
                    <div class="container">
                        <!-- Main Heading Starts -->
                        <div class="text-center top-text">
                            <h1><span>Características</span> Inovadoras</h1>                            
                        </div>
                         <!-- Divider Starts -->
                        <div class="divider text-center">
                            <span class="outer-line"></span>
                            <span class="fa fa-microchip" aria-hidden="true"></span>
                            <span class="outer-line"></span>
                        </div>
                <!-- Divider Ends -->
                        <div class="row" style="margin-top: 45px;">
                                <div class="col-lg-6 col-md-12 col-sm-12 bombas_abastecimentos">                                    
                                    <img class="img-fluid" id="altera_img" />
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6" >
                                    <div class="panel-group" id="accordion">
                                         <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="eletronica" data-parent="#accordion" href="#collapse13" aria-expanded="false">
                                                        <i class="fa fa-plus-circle eletronica_i" ></i>&nbsp;&nbsp;Eletrônica Companytec</a>
                                                    
                                                </h4>
                                                
                                            </div>
                                            <div id="collapse13" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Simples, moderna e de fácil integração e conectividade foi projetada e desenvolvida utilizando como referência a portaria Inmetro nº 559.
                                                        Eletrônica composta por 7 partes:<br/><br/>
                                                        •Controladora (separada do restante para minimizar possíveis substituições);<br/>
                                                        •Interface/Fonte/Acionamento;<br/>
                                                        •Indicador – LCD (Display);<br/>
                                                        •Teclas Touch + Leitor RFID;<br/>
                                                        •Transdutor (pulser)/Sensor de bico;<br/>
                                                        •Loop de corrente compatível com a maioria dos sistemas de automação do mercado;<br/>                                                       
                                                        •Módulo de Mídia (Opcional).<br/><br/>
                                                        Outros diferenciais da eletrônica:<br/><br/>

                                                        •Eletrônica Padronizada – todos os modelos de bombas possuem os mesmos componentes. Menor custo de banca de peças e agilidade na reposição;<br/><br/>
                                                        •Operação com até 3 níveis de preços  - à vista (dinheiro), cartão de débito, cartão de crédito;<br/><br/>
                                                        •Identificação de hardware automático – sem jumpers ou configurações das partes durante uma troca de peças. A controladora identifica os componentes “plug and play”;<br/><br/>
                                                        •Inteligência distribuída – rede de dados interna independente por lado, reduzindo conexões; facilidade de manutenção e redução de situações de pane geral (um lado não interfere no outro); possibilidade de ampliar novas funções no sistema apenas acrescentando módulos;<br/><br/>
                                                        •Sistema permite configuração e atualização por pendrive - agilidade na carga de configurações através de arquivos modelos e redução do tempo de manutenção;<br/><br/>                                                      
                                                        •Backup de configuração da controladora na interface – em caso extremo de troca da controladora não existe perda de tempo do mecânico.<br/><br/>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="segura" data-parent="#accordion" href="#collapse30" aria-expanded="false"><i class="fa fa-plus-circle segura_i" ></i>&nbsp;&nbsp;Configuração Segura </a>
                                                </h4>
                                            </div>
                                            <div id="collapse30" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> Configuração e gerenciamento através de cartões sem contato:<br/>
                                                        - Cartão Gerencial - permite configurar as opções de operação da bomba;<br/>
                                                        - Cartão Técnico - acesso de nível técnico que permite alterar os perfis de funcionamento;</br>
                                                        - Cartão Sistema - gerenciamento de parâmetros relativos à comunicação com o sistema gerencial do posto.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="identfid" data-parent="#accordion" href="#collapse31" aria-expanded="false"><i class="fa fa-plus-circle identfid_i" ></i>&nbsp;&nbsp;Identificação de Frentistas e Clientes </a>
                                                </h4>
                                            </div>
                                            <div id="collapse31" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> 
                                                        Sistema de identificação Identfid®* da Companytec.<br/>
                                                        Permite  o desbloqueio da bomba mediante identificação, possibilita o controle de produtividade para frentista e a implementação de programas de fidelidade.
                                                        Compatível com outras automações.<br/>
                                                        *O sistema requer licença para o uso.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="armazenamento" data-parent="#accordion" href="#collapse32" aria-expanded="false"><i class="fa fa-plus-circle armazenamento_i" ></i>&nbsp;&nbsp;Armazenamento de Dados </a>
                                                </h4>
                                            </div>
                                            <div id="collapse32" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> 
                                                        Memória de gravação dos últimos 64 abastecimentos completos.

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="portico" data-parent="#accordion" href="#collapse12" aria-expanded="false"><i class="fa fa-plus-circle portico_i" ></i>&nbsp;&nbsp;Pórtico </a>
                                                </h4>
                                            </div>
                                            <div id="collapse12" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> Pórtico é uma solução de design nas bombas para a uniformização do ambiente decór do posto quando existe a necessidade de utilizar bombas de mangueira alta e baixa na mesma cobertura de abastecimento ou para a substituição dos indicadores produtos, de Ilha ou posição de abastecimento.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="bicos" data-parent="#accordion" href="#collapseTwo" aria-expanded="false"><i class="fa fa-plus-circle bicos_i" ></i>&nbsp;&nbsp;Suporte de bicos</a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Suporte de bicos com indicador de produto em policarbonato que garante durabilidade e facilita a limpeza e manutenção.
                                                    </p>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="base" data-parent="#accordion" href="#collapseFour" aria-expanded="false"><i class="fa fa-plus-circle base_i" ></i>&nbsp;&nbsp;Base Robusta</a>
                                                </h4>
                                            </div>
                                            <div id="collapseFour" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Base robusta construída com materias que não sofrem com a corrosão.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="mostrador" data-parent="#accordion" href="#collapse6" aria-expanded="false"><i class="fa fa-plus-circle mostrador_i" ></i>&nbsp;&nbsp;Mostrador</a>
                                                </h4>
                                            </div>
                                            <div id="collapse6" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Mostrador em vidro temperado e serigrafado internamente com dígitos branco em fundo preto para facilitar a leitura de dia ou de noite.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="display" data-parent="#accordion" href="#collapse5" aria-expanded="false"><i class="fa fa-plus-circle display_i" ></i>&nbsp;&nbsp;Indicadores de produto (Display)</a>
                                                </h4>
                                            </div>
                                            <div id="collapse5" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Indicador (Display) com dígito de 1.5” com iluminação em LED e fundo preto. Economia de energia e maior vida útil de iluminação.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="totalizador" data-parent="#accordion" href="#collapse7" aria-expanded="false"><i class="fa fa-plus-circle totalizador_i" ></i>&nbsp;&nbsp;Totalizador</a>
                                                </h4>
                                            </div>
                                            <div id="collapse7" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Totalizador perpétuo com tecnologia e-ink (a imagem não desaparece até mesmo com a bomba desligada). Agilidade na conferência dos totalizadores eletrônicos, informando número do bico e totalizador completo.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="teclado" data-parent="#accordion" href="#collapse8" aria-expanded="false"><i class="fa fa-plus-circle teclado_i" ></i>&nbsp;&nbsp;Teclado Touch</a>
                                                </h4>
                                            </div>
                                            <div id="collapse8" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Teclado Touch no próprio vidro do mostrador, com números iluminados em LED. Durabilidade e estanqueidade (não entra água).
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="indicadores" data-parent="#accordion" href="#collapse9" aria-expanded="false"><i class="fa fa-plus-circle indicadores_i" ></i>&nbsp;&nbsp;Indicadores de produto</a>
                                                </h4>
                                            </div>
                                            <div id="collapse9" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Indicadores de produto adesivados por trás do vidro; melhor acabamento e durabilidade.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="multimidia" data-parent="#accordion" href="#collapse10" aria-expanded="false"><i class="fa fa-plus-circle multimidia_i" ></i>&nbsp;&nbsp;Multimídia</a>
                                                </h4>
                                            </div>
                                            <div id="collapse10" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Multimídia(opcional) desenvolvida e gerenciada pelo posto, com monitor em LCD de 10.4" e conexões via cabo ou pendrive. A multimídia das bombas WERTCO, podem servir para: <br/>
                                                        - Promoção de produtos e serviços;<br/>
                                                        - Canal exclusivo de comunicação com os clientes;<br/>
                                                        - Geração de receita adicional;                                                        
                                                    </p>
                                                </div>
                                            </div>
                                        </div>                                       
                                        <!--<div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="antifraude" data-parent="#accordion" href="#collapse14" aria-expanded="false"><i class="fa fa-plus-circle" ></i>&nbsp;&nbsp;Antifraude</a>
                                                </h4>
                                            </div>
                                            <div id="collapse14" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                         Transdutor sem partes móveis, com encoder magnético absoluto que detecta a posição instantânea do medidor sem possibilidade de perder pulsos como em tecnologias de bombas atuais. Esta tecnologia, com alta resolução, inibe e dificulta qualquer tipo de fraude.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>-->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="conectividade" data-parent="#accordion" href="#collapse15" aria-expanded="false"><i class="fa fa-plus-circle conectividade_i" ></i>&nbsp;&nbsp;Conectividade</a>
                                                </h4>
                                            </div>
                                            <div id="collapse15" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        - Conexão Ethernet;<br/>
                                                        - Bluetooth opcional;<br/>
                                                        - Loop de corrente( compatível com a maioria dos sistemas de automação do mercado).
                                                        
                                                    </p>
                                                </div>
                                            </div>
                                        </div>                                                                              
                                       
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="smc" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="fa fa-plus-circle smc_i"  ></i>&nbsp;&nbsp;Bases, Colunas Laterais, Tampas, Suporte de Bicos e Acabamentos fabricados em SMC</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <p>
                                                        O SMC (Sheet Molding Compoud) é um composto que consiste de uma resina termofixa, fibras de reforço, cargas minerais e aditivos termoplásticos.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="revestimentos" data-parent="#accordion" href="#collapseFive" aria-expanded="false"><i class="fa fa-plus-circle revestimentos_i" ></i>&nbsp;&nbsp;Revestimentos dos painéis frontais, laterais, pórticos e fechamentos</a>
                                                </h4>
                                            </div>
                                            <div id="collapseFive" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Revestimentos dos painéis frontais, laterais, pórticos e fechamentos em chapa de alumínio, material de alta resistência à corrosão.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>     
                                         <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="teclado" data-parent="#accordion" href="#collapsesFive" aria-expanded="false"><i class="fa fa-plus-circle teclado_i" ></i>&nbsp;&nbsp;Teclado de predeterminação</a>
                                                </h4>
                                            </div>
                                            <div id="collapsesFive" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> 
                                                        Até três níveis de preços configuráveis na bomba (à vista, cartão de crédito e cartão de débito). <br/>
                                                        Ideal pra clientes:&nbsp;&nbsp;<img src="<?php echo base_url('bootstrap/img/logoclientes2.png'); ?>" />
                                                    </p>
                                                </div>
                                            </div>
                                        </div>  
                                         <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="certificacoes" data-parent="#accordion" href="#collapseses" aria-expanded="false"><i class="fa fa-plus-circle certificacoes_i" ></i>&nbsp;&nbsp;Certificações</a>
                                                </h4>
                                            </div>
                                            <div id="collapseses" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Certificadas pela UL do Brasil e homologadas pelo Inmetro, as bombas estão em conformidade com as mais modernas normas técnicas brasileiras de proteção e segurança.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="manutencao" data-parent="#accordion" href="#facilmanut" aria-expanded="false"><i class="fa fa-plus-circle manutencao_i"  ></i>&nbsp;&nbsp;Fácil Manutenção</a>
                                                </h4>
                                            </div>
                                            <div id="facilmanut" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Projetada para facilitar e diminuir o tempo de serviço e manutenção.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>                                        
                                        
                                        </div>
                                      </div>      
                                    </div>
                            </div>                                          
                </div>            
        </section>
        <!-- end Accordion Section -->       
        <!-- Blog Section Starts -->
        <section id="certificacoes" class="blog">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Certificações</span></h1>
                    <h4></h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-certificate" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- Latest Blog Posts Starts -->
                <div class="row" style="margin-top: 50px; ">
                    <div class="col-sm-4">
                        <div class="card">
                            <div style="height: 100px;position: absolute;top: 45%;left: 50%;margin-top: -44px;margin-left: -35px;text-align: center;">
                                <a href="<?php echo base_url('bootstrap/certificados/CertificadoUL-BR17090X-2024rev06.pdf');?>" target="blank"><img src="<?php echo base_url('bootstrap/img/ubl.png');?>" height="100"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div style="margin: auto;">
                                <a href="<?php echo base_url('bootstrap/certificados/CertificadoISO90012020-2024.pdf');?>" target="blank"><img src="<?php echo base_url('bootstrap/certificados/iso.png');?>" height="100%" class="img-fluid"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div style="width: 228px;height: 100px;position: absolute;top: 50%;left: 50%;margin-top: -44px;margin-left: -116px;text-align: center;">
                                <a href="<?php echo base_url('bootstrap/certificados/inmetro.pdf');?>" target="blank"><p>Portaria Inmetro/Dimel n.º 11, de 26 de janeiro de 2018.</p></a>
                            </div>    
                        </div>
                    </div>
                </div>
                <!-- Latest Blog Posts Ends -->
            </div>
        </section>
        <!--    Blog Section Ends      -->   
        <section id="tutoriais" class="blog">
            <!-- Container Starts -->
            <div class="container" style="margin-bottom: 150px;">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Tutoriais</span></h1>
                    <h4></h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-play" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- Latest Blog Posts Starts -->
                <div class="row blog-content" style="margin-top: 100px;">  
                     <!-- Article Starts -->
                    <article class="col-lg-3 col-md-12" > 
                        <!-- Figure Starts -->
                        <a href="https://www.youtube.com/watch?v=KFD1Hp2y87E&feature=youtu.be" target="_blank">
                            <img  style="border: 2px solid #000000;padding: 4px;box-shadow: 0px 0px 10px 1px #060606;" src="<?php echo base_url('bootstrap/img/tutorial1.png');?>" class="img-fluid" />
                        </a>
                        <!-- Figure Ends -->
                        <!-- Excerpt Starts -->
                        <div class="blog-excerpt">
                            <a href="https://www.youtube.com/watch?v=KFD1Hp2y87E&feature=youtu.be" target="_blank">
                                <h3>Como Se Cadastrar</h3>
                            </a>                            
                        </div>
                        <!-- Excerpt Ends -->
                    </article>
                    <!-- Article Ends -->                    
                    <!-- Article Starts -->
                    <article class="col-lg-3 col-md-12" > 
                        <!-- Figure Starts -->
                        <a href="https://www.youtube.com/watch?v=uAImWRjac00&feature=youtu.be" target="_blank">
                            <img style="border: 2px solid #000000;padding: 4px;box-shadow: 0px 0px 10px 1px #060606;" src="<?php echo base_url('bootstrap/img/tutorial2.png');?>" class="img-fluid">
                         </a>
                        <!-- Figure Ends -->
                        <!-- Excerpt Starts -->
                        <div class="blog-excerpt">
                            <a href="https://www.youtube.com/watch?v=uAImWRjac00&feature=youtu.be" target="_blank">
                                <h3>Como Indicar um Negócio - Área do Indicador</h3>
                            </a>                            
                        </div>
                        <!-- Excerpt Ends -->
                    </article>
                    <!-- Article Ends -->     
                     <!-- Article Starts -->
                    <article class="col-lg-3 col-md-12" > 
                        <!-- Figure Starts -->
                        <a href="https://www.youtube.com/watch?v=S5C2ROjj1Eg&feature=youtu.be" target="_blank">
                            <img src="<?php echo base_url('bootstrap/img/tutorial3.png');?>" style="border: 2px solid #000000;padding: 4px;box-shadow: 0px 0px 10px 1px #060606;"  class="img-fluid" />
                        </a>
                        <!-- Figure Ends -->
                        <!-- Excerpt Starts -->
                        <div class="blog-excerpt">
                            <a href="https://www.youtube.com/watch?v=S5C2ROjj1Eg&feature=youtu.be" target="_blank">
                                <h3>Como Indicar um Negócio - Área do Técnico</h3>
                            </a>                            
                        </div>
                        <!-- Excerpt Ends -->
                    </article>
                    <!-- Article Ends -->
                      <!-- Article Starts -->
                    <article class="col-lg-3 col-md-12" > 
                        <!-- Figure Starts -->
                        <a href="https://www.youtube.com/watch?v=H5JXfVjktA4&feature=youtu.be" target="_blank">
                            <img src="<?php echo base_url('bootstrap/img/tutorial3.png');?>" style="border: 2px solid #000000;padding: 4px;box-shadow: 0px 0px 10px 1px #060606;"  class="img-fluid" />
                        </a>
                        <!-- Figure Ends -->
                        <!-- Excerpt Starts -->
                        <div class="blog-excerpt">
                            <a href="https://www.youtube.com/watch?v=S5C2ROjj1Eg&feature=youtu.be" target="_blank">
                                <h3>Como Abrir um Chamado - Área do Cliente</h3>
                            </a>                            
                        </div>
                        <!-- Excerpt Ends -->
                    </article>
                    <!-- Article Ends -->
                </div>
                <!-- Latest Blog Posts Ends -->
            </div>
        </section>
        <!-- Blog Section Ends -->   
        <!--    Blog Section Starts    -->
        <section id="blog" class="blog">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Notícias</span></h1>
                    <h4></h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-comments" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- Latest Blog Posts Starts -->
                <div class="row blog-content">
                     <!-- Article Starts -->
                    <article class="col-lg-4 col-md-12"> 
                        <!-- Figure Starts -->
                        <figure class="blog-figure">
                            <a target="_blank" href="<?=base_url('noticias/wertcoSurpreendeMercado')?>">
                                <img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/wertcoSurpreendeSite.jpg')?>" alt="">
                            </a>
                        </figure> 
                        <!-- Figure Ends -->
                        <!-- Excerpt Starts -->
                        <div class="blog-excerpt">
                            <a href="<?=base_url('noticias/wertcoSurpreendeMercado')?>" target="_blank">
                                <h3>Wertco surpreende mercado com crescimento acima das expectativas</h3>
                            </a>
                            <p> 😁 É com muita satisfação que anunciamos que nossa empresa cresceu 63% em...</p>
                            <div class="meta">
                                <span class="date"><i class="fa fa-calendar"></i> 08 Março 2023</span>
                                <a class="pull-right" href="<?=base_url('noticias/wertcoSurpreendeMercado')?>" target="_blank">Leia Mais <i class="fa fa-caret-right"></i></a>
                            </div>
                        </div>
                        <!-- Excerpt Ends -->
                    </article>
                     <!-- Article Starts -->
                    <article class="col-lg-4 col-md-12"> 
                        <!-- Figure Starts -->
                        <figure class="blog-figure">
                            <a target="_blank" href="<?=base_url('noticias/exponline')?>">
                                <img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/NoticiasWertcoExponline.png')?>" alt="">
                            </a>
                        </figure> 
                        <!-- Figure Ends -->
                        <!-- Excerpt Starts -->
                        <div class="blog-excerpt">
                            <a href="<?=base_url('noticias/exponline')?>" target="_blank">
                                <h3>Começou a maior feira online de automação e bombas do brasil</h3>
                            </a>
                            <p> Você que achou que não teria uma feira este ano. Você que estava espera...</p>
                            <div class="meta">
                                <span class="date"><i class="fa fa-calendar"></i> 08 Setembro 2020</span>
                                <a class="pull-right" href="<?=base_url('noticias/exponline')?>" target="_blank">Leia Mais <i class="fa fa-caret-right"></i></a>
                            </div>
                        </div>
                        <!-- Excerpt Ends -->
                    </article> 
                    <!-- Article Starts -->
                    <!-- Article Starts -->
                    <article class="col-lg-4 col-md-12"> 
                        <!-- Figure Starts -->
                        <figure class="blog-figure">
                            <a target="_blank" href="<?=base_url('noticias/brasilTemMaiorNrlacradas')?>">
                                <img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/brasilTemMaiorNrlacradas.png')?>" alt="">
                            </a>
                        </figure> 
                        <!-- Figure Ends -->
                        <!-- Excerpt Starts -->
                        <div class="blog-excerpt">
                            <a href="<?=base_url('noticias/brasilTemMaiorNrlacradas')?>" target="_blank">
                                <h3>Brasil tem maior número de bombas de combustível lacradas desde 2009</h3>
                            </a>
                            <p>A quantidade de bombas de combustíveis lacradas nos postos brasileiros saltou...</p>
                            <div class="meta">
                                <span class="date"><i class="fa fa-calendar"></i> 04 Fevereiro 2020</span>
                                <a class="pull-right" href="<?=base_url('noticias/brasilTemMaiorNrlacradas')?>" target="_blank">Leia Mais <i class="fa fa-caret-right"></i></a>
                            </div>
                        </div>
                        <!-- Excerpt Ends -->
                    </article> 
                    <!-- Article Starts -->
                                                       
                </div>
                <div class="row blog-content"> 
                    <!-- Article Starts -->
                    <article class="col-lg-4 col-md-12"> 
                        <!-- Figure Starts -->
                        <figure class="blog-figure">
                            <a target="_blank" href="<?=base_url('noticias/concursoPostoBonito')?>">
                                <img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/noticiaPostoBonitoWertco.png')?>" alt="">
                            </a>
                        </figure> 
                        <!-- Figure Ends -->
                        <!-- Excerpt Starts -->
                        <div class="blog-excerpt">
                            <a href="<?=base_url('noticias/concursoPostoBonito')?>" target="_blank">
                                <h3>Companytec e Wertco apoiam o XX concurso 2019 o posto mais bonito do brasil</h3>
                            </a>
                            <p>Promovido pela Revista Posto de Observação e Brascombustíveis (Associação Brasileira...</p>
                            <div class="meta">
                                <span class="date"><i class="fa fa-calendar"></i> 28 Outubro 2019</span>
                                <a class="pull-right" href="<?=base_url('noticias/concursoPostoBonito')?>" target="_blank">Leia Mais <i class="fa fa-caret-right"></i></a>
                            </div>
                        </div>
                        <!-- Excerpt Ends -->
                    </article>  
                    <!-- Article Starts -->
                    <article class="col-lg-4 col-md-12"> 
                        <!-- Figure Starts -->
                        <figure class="blog-figure">
                            <a target="_blank" href="<?=base_url('noticias/WertcoeCompanytecExpopostos')?>">
                                <img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/WertcoeCompanytecExpopostos.png')?>" alt="">
                            </a>
                        </figure> 
                        <!-- Figure Ends -->
                        <!-- Excerpt Starts -->
                        <div class="blog-excerpt">
                            <a href="<?=base_url('noticias/WertcoeCompanytecExpopostos')?>" target="_blank">
                                <h3>Companytec e Wertco marcam presença no Expopetro</h3>
                            </a>
                            <p>A Companytec e a Wertco participarão do 20° Congresso de Revendedores de Combustiveis da...</p>
                            <div class="meta">
                                <span class="date"><i class="fa fa-calendar"></i> 01 Outubro 2019</span>
                                <a class="pull-right" href="<?=base_url('noticias/WertcoeCompanytecExpopostos')?>" target="_blank">Leia Mais <i class="fa fa-caret-right"></i></a>
                            </div>
                        </div>
                        <!-- Excerpt Ends -->
                    </article> 
                     <!-- Article Starts -->
                    <!-- Article Starts -->
                    <article class="col-lg-4 col-md-12"> 
                        <!-- Figure Starts -->
                        <figure class="blog-figure">
                            <a target="_blank" href="<?=base_url('noticias/WertcoeAbiepsIniciam')?>">
                                <img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/AbiepsWertco.png')?>" alt="">
                            </a>
                        </figure> 
                        <!-- Figure Ends -->
                        <!-- Excerpt Starts -->
                        <div class="blog-excerpt">
                            <a href="<?=base_url('noticias/WertcoeAbiepsIniciam')?>" target="_blank">
                                <h3>Wertco e Abieps iniciam a qualificação técnica no Brasil</h3>
                            </a>
                            <p>Com o objetivo de oferecer capacitação profissional na instalação e manutenção de suas bombas...</p>
                            <div class="meta">
                                <span class="date"><i class="fa fa-calendar"></i> 05 Maio 2019</span>
                                <a class="pull-right" href="<?=base_url('noticias/WertcoeAbiepsIniciam')?>" target="_blank">Leia Mais <i class="fa fa-caret-right"></i></a>
                            </div>
                        </div>
                        <!-- Excerpt Ends -->
                    </article> 
                     <!-- Article Starts -->
                   
                <!-- Latest Blog Posts Ends -->
            </div>
        </section>
              
        <!-- Blog Section Ends -->
        <section class="newsletter">
            <div class="section-overlay">
                <!-- Container Starts -->
                <div class="container">
                    <!-- Main Heading Starts -->
                    <div class="text-center top-text">
                        <h1><span> newsletter </span></h1>
                        <h4></h4>
                    </div>
                    <!-- Main Heading Ends -->
                    <div class="newsletter-content">
                        <p class="text-center">Assine a newsletter e receba informações sobre o mercado de postos de combustíveis, <br/>lançamentos e demais novidades da Wertco</p>
                        <!-- Newsletter Form Starts -->
                        <form class="form-inputs">
                            <!-- Newsletter Form Input Field Starts -->
                            <div class="col-md-12 form-group custom-form-group p-0">
                                <span class="input custom-input">
                                    <input placeholder="Insira seu E-mail" class="input-field custom-input-field" required id="email_newsletter" type="text" />
                                    <label class="input-label custom-input-label" >
                                        <i class="fa fa-envelope-open-o icon icon-field"></i>
                                    </label>
                                </span>
                                 <!-- Form Submit Message Starts -->
                                <div class="col-sm-12 text-center output_message_holder">
                                    <p class="output_message" id="output_message_newsletter"></p>
                                </div>
                                <!-- Form Submit Message Ends -->
                            </div>
                            <!-- Newsletter Form Input Field Ends -->
                            <!-- Newsletter Form Submit Button Starts -->
                            <button id="enviar_newsletter" name="submit" type="button" class="custom-button" title="Send">Enviar</button>
                            <!-- Newsletter Form Submit Button Ends -->
                        </form>
                        <!-- Newsletter Form Ends -->
                    </div>
                </div>
                <!-- Container Ends -->
            </div>
        </section>
        <!-- Newsletter Section Ends -->
        <!-- Contact Section Starts -->
        <section id="contact" class="contact">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Contato</span></h1>                   
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-envelope" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
            </div>
            <!-- Container Ends -->
            <!-- Map Section Starts -->
            <div class="info-map">
                <div class="google-map">                    
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3661.687696628723!2d-46.3295730844523!3d-23.399512661521648!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce8781654bd9bb%3A0xf0545ab63ac06be0!2sAv.+Get%C3%BAlio+Vargas%2C+280+-+Jardim+Vitoria%2C+Aruj%C3%A1+-+SP%2C+07432-575!5e0!3m2!1spt-BR!2sbr!4v1516099257753" width="2000" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <!-- Info Map Boxes Starts -->
            <div class="container">
                <div class="row info-map-boxes">
                    <!-- left Info Map Box Starts -->
                    <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-map-marker">
                            <h1>Endereço</h1>
                                <p>Avenida Getúlio Vargas, 280
                                <br/>Via de Circulação, 330 - Jardim Ângelo
                                <br/>07400-230 - Arujá - SP - BR                                                                    
                                </p>
                        </div>
                    </div>
                    
                    <!-- left Info Map Box Ends -->
                    <!-- center Info Map Box Starts -->
                    <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-phone">
                            <h1>Telefone &amp; E-mail</h1>
                                <p>Telefone : +55 11 3900-2565                                  
                                    <br /><a href="#">vendas@wertco.com.br</a><br/>
                                </p>
                        </div>
                    </div>
                    <!-- center Info Map Box Ends -->
                    <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-wrench">
                            <h1>Assistência Técnica</h1>
                            <p>Telefone: +55 11 3900 - 2565 <br/>
                            <a href="#">servicos@wertco.com.br</a><br/>    
                            </p>
                        </div>
                    </div>    
                     <!-- Right Info Map Box Starts -->
                     <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-clock-o">
                            <h1>Atendimento</h1>
                            <p>Segunda-Sexta : 08:00–17:00                                
                                </p>
                        </div>
                    </div>
                    
                    <!-- Right Info Map Box Ends -->

                </div>
            </div>
            <!-- Info Map Boxes Ends -->
        </section>
        <!-- Contact Section Ends -->
        <!-- Contact Form Section Starts -->
        <section class="contactform" style="-webkit-transform: skewY(0deg) !important;transform: none !important;">
            <div class="section-overlay">
                <div class="container" style="-webkit-transform: skewY(0deg) !important;transform: skewY(0deg) !important;">
                    <!-- Main Heading Starts -->
                    <div class="text-center top-text">
                        <h1><span>Fale conosco</span></h1>                        
                    </div>
                    <!-- Main Heading Ends -->
                    <div class="form-container">
                        <!-- Contact Form Starts -->
                        <form class="formcontact" method="post" >
                            <div class="row form-inputs">
                                <!-- First Name Field Starts -->
                                <div class="col-md-6 form-group custom-form-group">
                                    <span class="input custom-input">
                                        <input placeholder="Nome" class="input-field custom-input-field" id="firstname" name="firstname" type="text" required data-error="NEW ERROR MESSAGE">
                                        <label class="input-label custom-input-label" >
                                            <i class="fa fa-user icon icon-field"></i>
                                        </label>
                                    </span>
                                </div>
                                <!-- First Name Field Ends -->
                                <!-- Last Name Field Starts -->
                                <div class="col-md-6 form-group custom-form-group">
                                    <span class="input custom-input">
                                        <input placeholder="Sobrenome" class="input-field custom-input-field" id="lastname" name="lastname" type="text" required>
                                        <label class="input-label custom-input-label" >
                                            <i class="fa fa-user-o icon icon-field"></i>
                                        </label>
                                    </span>
                                </div>
                                <!-- Last Name Field Ends -->
                                <!-- Phone Field Starts -->                                
                                <div class="col-md-12 form-group custom-form-group">
                                    <span class="input custom-input">
                                        <input placeholder="Telefone" class="input-field custom-input-field" id="telefone" name="phone" type="text" required />
                                        <label class="input-label custom-input-label" >
                                            <i class="fa fa-phone icon icon-field"></i>
                                        </label>
                                    </span>
                                </div>
                                <!-- Phone Field Ends -->
                                <!-- Message Field Starts -->
                                <div class="form-group custom-form-group col-md-12">
                                    <textarea placeholder="Mensagem" id="message" name="message" cols="45" rows="7" required></textarea>
                                </div>
                                <!-- Message Field Ends -->
                                <!-- Email Name Field Starts -->
                                <div class="col-md-6 form-group custom-form-group">
                                    <span class="input custom-input">
                                        <input placeholder="E-mail" class="input-field custom-input-field" id="email_contact" name="email_contact" type="text" required>
                                        <label class="input-label custom-input-label" >
                                            <i class="fa fa-envelope icon icon-field"></i>
                                        </label>
                                    </span>
                                </div>
                                <!-- Email Name Field Ends -->
                                <!-- Submit Button Starts -->
                                <div class="col-md-6 submit-form">
                                    <button id="form-submit" name="submit" type="button" class="custom-button" title="Send">Enviar</button>
                                </div>
                                <!-- Submit Button Ends -->
                                <!-- Form Submit Message Starts -->
                                <div class="col-sm-12 text-center output_message_holder"> 
                                    <p class="output_message" id="contato"></p>
                                </div>
                                <!-- Form Submit Message Ends -->
                            </div>
                        </form>
                        <!-- Contact Form Ends -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact Form Section Ends -->
         <!-- Contact Form Section Starts -->
        <section id="trabalhe_conosco"class="blog">
            <!-- Container Starts -->
            <div class="container">
                    <div class="container">
                        <!-- Main Heading Starts -->
                       <div class="text-center top-text">
                         <h1><span>Trabalhe Conosco</span></h1>                   
                        </div>
                               <!-- Divider Starts -->
                        <div class="divider text-center">
                            <span class="outer-line"></span>
                            <span class="fa fa-id-badge" aria-hidden="true"></span>
                            <span class="outer-line"></span>
                        </div>
                    </div>
                    <!-- Divider Ends -->
                    <!-- Main Heading Ends -->
                    <div class="form-container">
                        <!-- Contact Form Starts -->
                        <form  method="post" id="form_curriculo" enctype="multipart/form-data">
                            <div class="row form-inputs">
                                <!-- First Name Field Starts -->
                                <div class="col-md-4 form-group custom-form-group">
                                    <span class="input custom-input">
                                        <input placeholder="Nome" class="input-field custom-input-field" id="nome" name="nome" type="text" required data-error="NEW ERROR MESSAGE">
                                        <label class="input-label custom-input-label" >
                                            <i class="fa fa-user icon icon-field"></i>
                                        </label>
                                    </span>
                                </div>
                                <!-- First Name Field Ends -->
                                <!-- Last Name Field Starts -->
                                <div class="col-md-4 form-group custom-form-group">
                                    <span class="input custom-input">
                                        <input placeholder="Sobrenome" class="input-field custom-input-field" id="sobrenome" name="sobrenome" type="text" required>
                                        <label class="input-label custom-input-label" >
                                            <i class="fa fa-user-o icon icon-field"></i>
                                        </label>
                                    </span>
                                </div>
                                <!-- Last Name Field Ends -->
                                <!-- Phone Field Starts -->
                                <div class="col-md-4 form-group custom-form-group">
                                    <span class="input custom-input">
                                        <input placeholder="Telefone" class="input-field custom-input-field" id="telefone_curriculo" name="phone_curriculo" type="text" required>
                                        <label class="input-label custom-input-label" >
                                            <i class="fa fa-phone icon icon-field"></i>
                                        </label>
                                    </span>
                                </div>
                                <!-- Phone Field Ends -->
                                <!-- Message Field Starts -->
                                <div class="form-group custom-form-group col-md-12">
                                    <textarea placeholder="Mensagem" id="message_curriculo" name="message_curriculo" cols="45" rows="7" required></textarea>
                                </div>
                            </div>
                            <div class="row form-inputs">
                                
                                <!-- Message Field Ends -->
                                <!-- Email Name Field Starts -->
                                <div class="col-md-4 form-group custom-form-group">
                                    <span class="input custom-input">
                                        <input placeholder="E-mail" class="input-field custom-input-field" id="email_curriculo" name="email_curriculo" type="email" required>
                                        <label class="input-label custom-input-label" >
                                            <i class="fa fa-envelope icon icon-field"></i>
                                        </label>
                                    </span>
                                </div>
                                <!-- Email Name Field Ends -->
                                <!-- Email Name Field Starts -->
                                <div class="col-md-4 form-group custom-form-group">
                                    <span class="input custom-input">
                                        <input placeholder="Currículo" class="input-field custom-input-field" id="curriculo" name="curriculo" type="file" required>
                                        <label class="input-label custom-input-label" >
                                            <i class="fa fa-id-badge icon icon-field"></i>
                                        </label>
                                    </span>
                                </div>
                                <!-- Email Name Field Ends -->
                                <!-- Submit Button Starts -->
                                <div class="col-md-4 submit-form">
                                    <button class="custom-button" title="Send" >Enviar</button>
                                </div>
                                <!-- Submit Button Ends -->
                                <!-- Form Submit Message Starts -->
                                <div class="col-sm-12 text-center output_message_holder"> 
                                    <p class="output_message" id="trabalhe"></p>
                                </div>
                                <!-- Form Submit Message Ends -->
                            </div>
                        </form>
                        <!-- Contact Form Ends -->
                    </div>
                </div>            
        </section>
        <!-- Contact Form Section Ends -->
        <!-- Logos Section Starts -->
        <section class="logos">
          <!-- Footer Section Starts -->
        <footer class="footer text-center">
            <!-- Container Starts -->
            <div class="container">                
                <p>Wertco Indústria, Comércio e Serviços em Bombas de Abastecimento de Combustíveis, Importação e Exportação LTDA.<br/>CNPJ.: 27.314.980/0001-53 </p>
                <p>
                    &copy; Copyright <?php echo date('Y'); ?> Wertco 
                </p>                
            </div>
            <!-- Container Ends -->
            <div class="social-icons" style="margin-bottom: 40px;">
                <ul class="social">
                    <!--<li>
                        <a class="twitter" href="#" title="twitter"></a>
                    </li>-->
                    <li>
                        <a class="facebook" href="https://www.facebook.com/Wertco-146223742724238/" title="facebook"></a>
                    </li>
                    <!--<li>
                        <a class="google" href="#" title="google"></a>
                    </li>
                    <li>
                        <a class="skype" href="#" title="skype"></a>
                    </li>-->
                    <li>
                        <a class="instagram" href="https://www.instagram.com/wertco_bombas/" title="instagram"></a>
                    </li>
                    <!--<li>
                        <a class="linkedin" href="#" title="linkedin"></a>
                    </li>
                    <li>
                        <a class="youtube" href="#" title="youtube"></a>
                    </li>-->
                </ul>
            </div>
            <div style="margin-right: 13px; ">
                <a href="http://www.companytec.com.br" target="blank">
                    <img src="<?php echo base_url('bootstrap/img/logoCompanytec.png')?>" /> 
                </a>
            </div>
        </footer>
        <!-- Footer Section Starts -->
        </section>
        <!-- Logos Section Ends -->
        
        <!-- Back To Top Starts -->
        <div id="back-top-wrapper" class="d-none d-sm-block">
            <p id="back-top">
                <a href="index.html#top"><span></span></a>
            </p>
        </div>
        <!-- Back To Top Ends -->
    </div>
    <!-- Wrapper Ends -->
    <!-- Modal -->
    <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-color: #cccccc78;">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 600px;">
            <a href="#" style="cursor: pointer;height: 50px;width: 50px;color: #ffcc00;font-size: 22px;font-weight: bolder;">X</a> 
            <div class="modal-content ">                
                    <!--<a href="https://www.wertco.com.br/noticias/wertcoSurpreendeMercado" target="blank">-->
                        <img src="<?=base_url('bootstrap/img/popup/49.jpg')?>" class="img-fluid"  />
                    <!--</a>-->
                <!--<div class="popup-left">
                    <img src="<?php echo base_url('bootstrap/img/pop_texto.png'); ?>" />
                    <p>
                       Clique <a href="http://www.wertco.com.br/home/campanhaIndicador">aqui</a> e saiba mais! 
                    </p>
                </div>
                <div class="popup-left">
                    <img src="<?php echo base_url('bootstrap/img/pop_logo.png'); ?>" />
                </div>-->
            </div>
        </div>
    </div>
     <div class="modal fade" id="politica_privacidade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="background-color: #cccccc78;">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 670px;">
            <a href="#" style="cursor: pointer;height: 50px;width: 50px;color: #ffcc00;font-size: 22px;font-weight: bolder;">X</a> 
            <div class="modal-content " style="padding: 20px;">
               <p style="font-size: 12px;color: #000;"><b>POLÍTICA DE PRIVACIDADE - LGPD</b></p>
<p style="font-size: 10px;color: #000;">A Wertco Indústria, Comércio e Serviços em Bombas de Abastecimento de Combustíveis, Importação e Exportação LTDA. não quer assediar ninguém, muito menos usar seus dados para fins indevidos, sejam eles quais forem.<br>

No entanto, é necessário deixar claro que coletaremos alguns de seus dados em nosso site, sejam eles COMPULSÓRIOS ou ESPONTÂNEA, o que explicaremos a seguir.<br>

Se você tem menos de 18 anos, infelizmente, não será possível utilizar nossas plataformas, pois todas elas capturam, de alguma forma, seus dados. Portanto, se seguir, você atesta sua maioridade.<br>

Então, estando ciente de nossa POLÍTICA DE PRIVACIDADE, você declara que concorda com todas as implicações — que verá, não são nada demais.<br>

O recolhimento de dados de forma COMPULSÓRIA é feito através de cookies de quaisquer tipos e outras ferramentas tecnológicas. Mas ele serve para sabermos que páginas foram visitadas por você, quantas vezes, horários e durações. Se algum dia, você se cadastrou em nossa plataforma, a gente tem a possibilidade de saber que foi você que nos visitou novamente e como o fez. Porém, se você nunca se cadastrou conosco, a gente não faz ideia de quem você seja, e só temos acesso a dados anônimos de uso.<br>

O recolhimento de dados de forma ESPONTÂNEA é aquele quando você preenche um formulário. Neste momento, você nos informa que concordou com nossa Política de Privacidade também. Quando isso acontece, você o faz por interesse próprio em nossos produtos, serviços ou informações. Nesse caso, para conseguirmos lhe oferecer o que deseja, adicionamos você a nossa base. Chamamos as pessoas em nossa base de “leads”. Assim, quando alguém cadastrado visita nossas páginas novamente, estaremos registrando seus passos conosco, da forma COMPULSÓRIA expressa acima, para compreendermos que tipo de conteúdo nossos leads mais visitam e podermos criar conteúdos mais eficientes. É só isso.<br>

Quando a gente percebe que um lead da base, que consuma nosso conteúdo ou não, é um potencial cliente, talvez a gente envie (caso ele consinta no formulário) alguns e-mails esporádicos sobre novidades da empresa. Nesses emails sempre há um convite ao descadastramento da base. No caso mais extremo, é possível também que a gente entre em contato para oferecer um serviço ou convidá-lo para ser parceiro em algum projeto.<br>

Claro que também iremos usar o contato fornecido em questões contratuais de clientes por legítimo interesse, em casos que a qualidade da entrega de nosso trabalho exija contato ou mesmo repassar os dados para um fornecedor realizar uma entrega, por exemplo.<br>

É bom citar também, que todas as plataformas (sejam as contratadas por nós, sejam as redes sociais) podem coletar também dados como localização e até alguns que provavelmente não teremos interesse, como IP etc. Fazem isso por uma questão de segurança deles e, até mesmo sua, caso alguém esteja tentando se cadastrar com o seu nome.<br>

Você pode ter total certeza que a gente não utiliza, nem NUNCA utilizará, seus dados para quaisquer outros fins, muito menos cederemos a terceiros que nada tenham a ver com eles.<br>

É bom lembrar também que em nossas mídias sociais (salvo quando publicarmos algum formulário de captação, no qual você mesmo insere os dados) a gente não transfere nenhum tipo de informação sua para nossas bases de dados. Por exemplo, se você comentou em um post do Facebook, curtiu uma foto no Instagram ou retuitou algo pelo Twitter, esses dados ficam apenas lá nessas redes, seguindo a política de privacidade de cada uma. Agora, se a gente colocar algum formulário de captação por lá, aí sim, você concorda com a Política de Privacidade específica da rede em questão e com a nossa, funcionando conforme falamos acima sobre coleta ESPONTÂNEA.<br>

Esses dados ficam armazenados nessas plataformas com acesso online, e pelo tempo necessário para o interesse do cadastro, seguindo todos os protocolos de segurança que elas oferecem.<br>

Como ninguém fica parado no tempo, a gente se vê no direito de alterar esta POLÍTICA DE PRIVACIDADE toda vez que se fizer necessário. Por exemplo, quando adotarmos alguma ferramenta nova com novas funções que obriguem a ajustes por aqui. Mas tenha sempre a certeza de que faremos dentro dos limites de bom senso e zelando ao extremo pela sua privacidade e segurança.<br>

Por último, resta reforçar que você tem o direito a qualquer momento de solicitar todo e qualquer dado que armazenemos a seu respeito em nossas plataformas, bem como retificar, requerer exclusão, transferir, limitar, se opor ao tratamento ou retirar o consentimento. Esse procedimento pode ser feito através do email marketing[arroba]companytec.com.br (escrevemos assim para o endereço não ser capturado por robôs e ser usado sem o nosso consentimento. Você entendeu, né? Caso não tenha entendido, nos procure por qualquer outro meio disponível em nossos pontos de contato.<br>

Sendo assim, gostaríamos de finalizar dizendo que prezamos fortemente pela ética, segurança e privacidade dos usuários de nossas plataformas.<br>

Obrigado</p>
            </div>
        </div>
    </div>
    <div id="politica" class="container" style=""> 
        <div class="center-block texto-politica" >
            <p style="font-size: 1.2em;">Olá, tudo bem? <br/>Informamos que atualizamos nossa Política de Privacidade. 
            Clique aqui e conheça a nossa nova Política.<a style="cursor: pointer; border: 1px solid #ffcc00;padding: 5px;margin-left: 15px;color: #fff;" id="mostrar_politica" target="_blankk">Prosseguir</a></p>
        </div>
    </div>
    <!-- whatsApp click to call -->
    <a class="scroll-top-arrow-wp" target="_blank" href="https://api.whatsapp.com/send?phone=5511958378918&text=Bem%20vindo%20%C3%A0%20Wertco.%20J%C3%A1%20iremos%20atend%C3%AA-lo" title="Fale com a nossa equipe" style="background: #12b535;">
        <i class="fa fa-whatsapp"></i>
    </a>
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>
    <script type="text/javascript">
        var base_url = "<?php echo base_url();?>"; 
    </script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyBpN0oc0NeZv9JolJSIzQRNW9IkUOfKrxw"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>    
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js')?>"></script>
    <!-- Revolution Slider Main JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/jquery.themepunch.tools.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/jquery.themepunch.revolution.min.js')?>"></script>
    <!-- Revolution Slider Extensions -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.actions.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.carousel.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.migration.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.navigation.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.parallax.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.video.min.js')?>"></script>    
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>
    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>
    <!-- Revolution Slider Initialization Starts -->
    <script type="text/javascript">
    (function(){
        "use strict";
        var tpj = jQuery;
        var revapi4;
        tpj(document).ready(function() {
            if (tpj("#rev_slider").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider");
            } else {
                revapi4 = tpj("#rev_slider").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "js/plugins/revolution/js/",
                    dottedOverlay: "none",
                    sliderLayout:"fullscreen",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "zeus",
                            enable: true,
                            hide_onmobile: true,
                            hide_under: 600,
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            tmp: '<div class="tp-title-wrap">   <div class="tp-arr-imgholder"></div> </div>',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 90,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 90,
                                v_offset: 0
                            }
                        },
                        bullets: {
                            enable: false,
                            hide_onmobile: true,
                            hide_under: 600,
                            style: "metis",
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            direction: "horizontal",
                            h_align: "center",
                            v_align: "bottom",
                            h_offset: 0,
                            v_offset: 30,
                            space: 5,
                            tmp: '<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span>'
                        }
                    },
                    viewPort: {
                        enable: true,
                        outof: "pause",
                        visible_area: "80%"
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [600, 600, 500, 400],
                    lazyType: "none",
                    parallax: {
                        type: "mouse",
                        origo: "slidercenter",
                        speed: 2000,
                        levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
                    },
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    hideThumbsOnMobile: "off",
                    autoHeight: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        });
        
        // GOOGLE MAP
        function init_map() {
            
            var myOptions = {
                scrollwheel: false,
                zoom: 12,
                center: new google.maps.LatLng(-23.3994819,-46.3279319,19),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
            var marker = new google.maps.Marker({
                map: map,
                icon: "img/markers/yellow.png",
                position: new google.maps.LatLng(-23.3994819,-46.3279319,19)
            });
            var infowindow = new google.maps.InfoWindow({
                content: "<strong>WERTCO</strong><br>São Paulo<br>"
            });
            google.maps.event.addListener(marker, "click", function() {
                infowindow.open(map, marker);
            });
        }
        google.maps.event.addDomListener(window, "load", init_map);
            
    })(jQuery);

    $('#espanhol').bind('click', function(){
        window.location.href='<?php echo base_url('home/index/esp'); ?>';
    });
    
    $('#ingles').bind('click', function(){
        window.location.href='<?php echo base_url('home/index/eng'); ?>';
    });
    
    //$('#popUp').modal(); 

    $('#mostrar_politica').bind('click', function(){
        $('#politica').remove();
        $('#politica_privacidade').modal('show');
    });

    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5defe65043be710e1d2179f6/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();


    </script>
    <!-- Revolution Slider Initialization Ends -->
</body>

</html>