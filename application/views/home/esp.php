<!DOCTYPE html>
<html lang="esp">

<head>
    <meta charset="utf-8" />
    <title>WERTCO - ABASTECIENDO SOLUCIONES</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	--> 
	

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon1.png')?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />

    <!-- Revolution Slider CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/settings.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/layers.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/navigation.css')?>" />

    <!-- Live Style Switcher - demo only --> 
    <link rel="alternate stylesheet" type="text/css" title="yellow" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen" href="<?=base_url('bootstrap/css/skins/yellowgreen.css')?>" />
    
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

    <!-- Accordion TTU -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/accordion.css')?>" />
    <script type="text/javascript">
        var base_url = "<?php echo base_url();?>"; 
    </script>
</head>

<body class="dark double-diagonal">
    <!-- Preloader Starts -->
    <div class="preloader" id="preloader">
        <div class="logopreloader">
            <img class="img-fluid" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png')?>" alt="logo-black" />
        </div>
        <div class="loader" id="loader"></div>
    </div>
    <!-- Preloader Ends -->    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">                
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
                    <!-- Logo Starts -->
                    <div class="logo">
                        <a data-toggle="collapse" data-target=".navbar-collapse.show" class="navbar-brand link-menu scroll-to-target" href="#mainslider">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />                            
                        </a>
                    </div>
                    <!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span id="icon-toggler">
                          <span></span>
                          <span></span>
                          <span></span>
                          <span></span>
                        </span>
                    </button>
                    <!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#mainslider"><i class="fa fa-home"></i> Home</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#about"><i class="fa fa-user"></i> WERTCO</a></li>
                            <li id='bombas_ico'><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#testimonials"><img src="<?=base_url('bootstrap/img/bombabranca.png')?>" class="bombas_ico"> BOMBAS</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#services"><i class="fa fa-arrow-up"></i> DIFERENCIALES</a></li>
							<li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#inovadoras"><i class="fa fa-microchip"></i> carácter</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#certificacoes"><i class="fa fa-certificate"></i> CERTIFICACIONES</a></li>
                            <li class="bombas_menu"><a data-toggle="collapse" data-target=".navbar-collapse.show" class="link-menu" href="#contact"><i class="fa fa-envelope"></i> CONTACTO</a></li>
                            <li class="bombas_menu"><a href="<?php echo base_url('usuarios/login');?>" target="_blank" class="link-menu nav-link" onclick="window.open('https://www.wertco.com.br/usuarios/login');"><i class="fa fa-sign-in"></i> LOGIN</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		<!-- Header Ends -->
        <!-- Main Slider Section Starts -->
        <section class="mainslider" id="mainslider">
            <!-- Slider Hero Starts -->
            <div class="rev_slider_wrapper fullwidthbanner-container dark-slider" data-alias="vimeo-hero" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
                <div id="rev_slider" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-index="rs-18" data-transition="zoomin" data-slotamount="7" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/fundopostowertco.jpg')?>" data-rotate="0" data-saveperformance="off" data-title="Ken Burns" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/fundopostowertco.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">WERTCO 
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap; font-size: 22px !important; height: 35px !important; margin-top: 10px !important; ">ABASTECIENDO SOLUCIONES
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#about" class="custom-button slider-button scroll-to-target">Más información WERTCO</a></div>
                        </li>
                        <!-- SLIDE  -->
                        <li data-index="rs-2" data-transition="zoomin" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/quatroBombas1.jpg')?>" data-rotate="0" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/quatroBombas1.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">CONFIABILIDAD
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#testimonials" class="custom-button slider-button scroll-to-target">Más información sobre las bombas WERTCO</a></div>
                        </li>
                        <!-- SLIDE  -->
                        <li data-index="rs-3" data-transition="zoomin" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/duasBombas.jpg')?>" data-rotate="0" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/duasBombas.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">DURABILIDAD
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#testimonials" class="custom-button slider-button scroll-to-target">Más información sobre las bombas WERTCO</a></div>
                        </li>
                        <li data-index="rs-4" data-transition="zoomin" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?=base_url('bootstrap/img/revolution-slider/kenburns/tresBombas1.jpg')?>" data-rotate="0" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off" data-title="Intro" data-description="">
                            <!-- MAIN IMAGE -->
                            <img alt="" src="<?=base_url('bootstrap/img/revolution-slider/kenburns/tresBombas1.jpg')?>" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="180" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-fontsize="['70','70','70','45']" data-lineheight="['70','70','70','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="z-index: 5; white-space: nowrap;">POSIBILIDADES
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['52','52','52','51']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                            </div>

                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption NotGeneric-Icon   tp-resizeme rs-parallaxlevel-0" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-68','-68','-68','-68']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-style_hover="cursor:default;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="2000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><i class="pe-7s-refresh"></i>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['150','97','90','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:100px;sX:1;sY:1;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="y:50px;opacity:0;s:1000;e:Power2.easeInOut;" data-start="750" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 11; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"><a href="#testimonials" class="custom-button slider-button scroll-to-target">Más información sobre las bombas WERTCO</a></div>
                        </li>
                    </ul>
                    <div class="tp-static-layers"></div>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->
            <!-- Slider Hero Ends -->
        </section>
        <!-- Main Slider Section Ends -->
        <!-- About Section Starts -->
        <section id="about" class="about">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Sobre </span> Nosotros </h1>                    <!--<h4></h4>-->
                </div>
                <!-- Main Heading Ends -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-user" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- About Content Starts -->
                <div class="row about-content">
                    <div class="col-sm-12 col-md-12 col-lg-12 about-left-side">
                        <h3 class="title-about"><strong>WERTCO</strong></h3>
                        <hr>
                        <p>WERTCO es una empresa brasileña, fabricante de bombas de abastecimiento proyectadas para atender la nueva portería MTPS nº 1109 y otras exigencias del mercado, respetando las normas de salud, metrología y, principalmente, inhibición de fraudes de abastecimiento.
                        </p>
                        <p>Supresión de la necesidad del mercado por bombas de alta seguridad, conectividad y sostenibilidad, Wertco innova con productos confiables, duraderos, modernos, de fácil integración y con más posibilidades y funcionalidades para el negocio.
                        </p>
                        <p> Formada por profesionales con know-how en el mercado de fabricación de bombas de agua abastecimiento y sumados al grupo de desarrollo de electrónica de la Compañíatec, Wertco desarrolla productos basados ​​en tres valores importantes: Confiabilidad, Durabilidad y Posibilidades.
                        </p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <a class="custom-button slider-button scroll-to-target" href="#testimonials">Conozca nuestras Bombas de Combustibles</a>
                    </div>                   
                <!-- About Content Ends -->
            </div>
            <!-- Container Ends -->
        </section>
        <!-- About Section Ends -->
        <!-- BOMBAS DE ABASTECIMENTO Section Starts -->
        <section id="testimonials" class="testimonials">
           <div class="section-overlay">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Bombas </span>de Combustibles</h1>
                    <h4></h4>
                </div>
                <!-- Main Heading Ends -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <img src="<?=base_url('bootstrap/img/bombaIcoG.png')?>" />
                    <span class="outer-line"></span>
                </div>               
                
                <div class="row team-members">
				
                    <!-- Team Member Starts -->
                    <div class="col-lg-4 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <div class="team-member">
							<div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CLH (Manguera Baja)<br/></h4>                              
                                <ul class="list list-inline social">
                                   <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="fa fa-search-plus" href="<?=base_url('bootstrap/img/bombas/infoCLH.png')?>"></a>
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraisclh.png')?>" style='display: none;'></a>
                                    </li>
                                </ul>
								<a title="CLH Mangueira baixa" href="#" class="team-member-img-wrap">
								<img src="<?=base_url('bootstrap/img/bombas/CLH.png')?>" alt="team member" /></a>
                            </div>
                            <!-- Team Member Picture Starts -->
                           
                            <!-- Team Member Picture Ends -->
                            <!-- Team Member Details Starts -->
                            
                            <!-- Team Member Details Starts -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->
					<!-- Team Member Starts -->
                    <div class="col-lg-4 col-md-6 col-sm-12 magnific-popup-gallery">
                        <div class="team-member">                           
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CLH con Opción de Pórtico</h4>                               
                                <ul class="list list-inline social">
                                   <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="fa fa-search-plus mfp-iframe" href="http://vimeo.com/251343305"></a>
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="mfp-iframe" href="http://vimeo.com/251343305" style='display: none;'></a>
                                    </li>
									<li>
										<a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraisclh.png')?>" style='display: none;'></a>
									</li>
                                </ul>
								 <!-- Team Member Picture Starts -->
								<a title="CLH Mangueira baixa Com opção de pórtico" href="#" class="team-member-img-wrap mfp-iframe">
								<img src="<?=base_url('bootstrap/img/bombas/CLHPORTICO.png')?>" alt="team member" /></a>
                            <!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Starts -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->
                    <!-- Team Member Starts -->
                    <div class="col-lg-4 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <div class="team-member">
                            
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CHHS (Manguera Alta Slim)</h4>									
                                <ul class="list list-inline social">
                                    <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas fa fa-search-plus" href="<?=base_url('bootstrap/img/bombas/infoCHHS.png')?>"></a>
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraischhs.png')?>" style='display: none;'></a>
                                    </li>
                                </ul>
								<!-- Team Member Picture Starts -->
								<a title="CHHS | Mangueira Alta" href="#" class="bombas team-member-img-wrap">
									<img src="<?=base_url('bootstrap/img/bombas/CHHS.png')?>" alt="team member">
								</a>
								<!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Ends -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->
                    <!-- Team Member Starts -->
                    <div class="col-lg-4 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <!-- Team Member-->
                        <div class="team-member">                            
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">CHH (Manguera Alta) </h4>									
                                <ul class="list list-inline social">
                                    <li>
                                        &nbsp;
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas fa fa-search-plus" href="<?=base_url('bootstrap/img/bombas/infoCHH.png')?>"></a>
										
                                    </li>
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="" href="<?=base_url('bootstrap/img/bombas/dimensoesgeraischh.png')?>" style='display: none;'></a>
                                    </li>
                                </ul>
								<!-- Team Member Picture Starts -->
								<a title="CHH (Mangueira Alta)" href="#" class="bombas team-member-img-wrap">
									<img src="<?=base_url('bootstrap/img/bombas/CHH.png')?>" alt="">
								</a>
								<!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Ends -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->
                    <!-- Team Member Starts -->
                    <div class="col-lg-4 col-md-6 col-sm-12 magnific-popup-gallery" >
                        <!-- Team Member-->
                        <div class="team-member">                            
                            <!-- Team Member Details Starts -->
                            <div class="team-member-caption social-icons">
                                <h4 style="font-size: 15px;text-transform: none;">  Dispensador electronico para tren   </h4>
                                <ul class="list list-inline social">
                                    <li>
                                        &nbsp;
                                    </li>  
                                    <li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas fa fa-search-plus " href="<?=base_url('bootstrap/img/bombas/medidor_info.png')?>">                                            
                                        </a>                     
                                    </li>                                  
                                    <!--<li>
                                        <a data-gal="magnific-pop-up[team]" class="bombas team-member-img-wrap" href="<?=base_url('bootstrap/img/bombas/medidor_info.png')?>" style='display: none;'></a>
                                    </li>-->
                                </ul>
                                <!-- Team Member Picture Starts -->
                                <a title="CHH (Mangueira Alta)" href="#" class="bombas team-member-img-wrap">
                                    <img src="<?=base_url('bootstrap/img/bombas/medidor.png')?>" alt="Dispensador electronico para tren" title="Dispensador electronico para tren " >
                                </a>
                                <!-- Team Member Picture Ends -->
                            </div>
                            <!-- Team Member Details Ends -->
                        </div>
                    </div>
                    <!-- Team Member Ends -->                      
                </div>
                <!-- Team Members Ends -->				
            </div>
            </div>
            <!-- Container Ends -->
          </div>  
        </section>
        <!-- Team Section Ends -->
        <!-- Services Section Starts -->
        <section id="services" class="services">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Nuestros </span> Diferenciales </h1>
                     <h4> Productos pensados para el mercado brasileño </h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-arrow-up" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- Services Starts -->
                <div class="row services-box">
                     <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-handshake-o" data-headline="Fiabilidad"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-handshake-o">
                            <H2> Fiabilidad </ h2>
                             <p> Electrónica desarrollada por Companytec Automatización y Control, líder en el mercado brasileño de automatización para puestos de combustibles. <br/>
La asociación comercial con la empresa norteamericana Bennett para el suministro de conjuntos hidráulicos que son referencia de alto rendimiento, fácil mantenimiento y super resistencia a las características de operación de las bombas en Brasil. </p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                    <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-cubes" data-headline="Durabilidad"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-cubes">
                           <h2> Durabilidad </h2>
                             <p> Productos diseñados con materiales utilizados en la industria aeronáutica y automovilística, que garantizan mayor resistencia a los desgastes derivados del uso de los equipos, además de un diseño diferenciado y óptimo acabado.</p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                </div>
                <!-- Services Starts -->
                <div class="row services-box">    
                     <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-random" data-headline="Posibilidades"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-random">
                            <h2>Posibilidades</h2>
                            <p>Conectividad e Integración con sistemas y dispositivos. <br/>
Preparación para recibir sistemas de extracción de vapor. <br/>
Diseño modular y racional.</p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                     <!-- Service Item Starts -->
                    <div class="col-lg-6 col-md-6 col-sm-12 services-box-item">
                        <!-- Service Item Cover Starts -->
                        <span class="services-box-item-cover fa fa-lock" data-headline="Alta seguridad"></span>
                        <!-- Service Item Cover Ends -->
                        <!-- Service Item Content Starts -->
                        <div class="services-box-item-content fa fa-lock">
                            <h2>Alta seguridad</h2>
                            <p>Electrónica diseñada para satisfacer los requisitos de seguridad eléctrica y robustez. Pensada para dificultar, al máximo, los más diversos intentos de fraude que existen en el mercado.</p>
                        </div>
                        <!-- Service Item Content Ends -->
                    </div>
                    <!-- Service Item Ends -->
                   
                </div>

                </div>
                <!-- Services Ends -->
            </section>
             <!-- begin bomba mutcho -->            
            <section class="contactform" id="inovadoras" style="margin-top: -55px;">
                <div class="section-overlay">
                    <div class="container">
                        <!-- Main Heading Starts -->
                        <div class="text-center top-text">
                            <h1><span>Características </span> Innovadoras</h1>                            
                        </div>
                         <!-- Divider Starts -->
                        <div class="divider text-center">
                            <span class="outer-line"></span>
                            <span class="fa fa-microchip" aria-hidden="true"></span>
                            <span class="outer-line"></span>
                        </div>
                <!-- Divider Ends -->
                        <div class="row" style="margin-top: 45px;">
                                <div class="col-lg-6 col-md-12 col-sm-12 bombas_abastecimentos">                                    
									<img class="img-fluid" id="altera_img" />
                                </div>
								<div class="col-sm-12 col-md-12 col-lg-6" >
                                    <div class="panel-group" id="accordion">
										 <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="eletronica" data-parent="#accordion" href="#collapse13" aria-expanded="false">
                                                        <i class="fa fa-plus-circle eletronica_i" ></i>&nbsp;&nbsp;Electrónica Companytec</a>
													
                                                </h4>
												
                                            </div>
                                            <div id="collapse13" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Simple, moderna y de fácil integración y conectividad fue proyectada y desarrollada utilizando como referencia la ordenanza Inmetro nº 559.
Electrónica compuesta por 7 partes: <br/> <br/>
• Controladora (separada del resto para minimizar posibles sustituciones); <br/>
• Interfaz / Fuente / Drive; <br/>
• Indicador - LCD (Display); <br/>
• Teclas Touch + Lector RFID; <br/>
• Transductor (pulser) / Sensor de boquilla; <br/>
• Loop de corriente compatible con la mayoría de los sistemas de automatización del mercado; <br/>
• Módulo de Medios (Opcional). <br/> <br/>
Otros diferenciales de la electrónica: <br/> <br/>

• Electrónica estandarizada - todos los modelos de bombas tienen los mismos componentes. Menor costo de banca de piezas y agilidad en la reposición; <br/> <br/>
• Operación con hasta 3 niveles de precios - a la vista (dinero), tarjeta de débito, tarjeta de crédito; <br/> <br/>
• Identificación de hardware automático - sin puentes o configuraciones de las partes durante un cambio de piezas. El controlador identifica los componentes "plug and play"; <br/> <br/>
• Inteligencia distribuida - red de datos interna independiente por lado, reduciendo las conexiones; facilidad de mantenimiento y reducción de situaciones de pane general (un lado no interfiere en el otro); posibilidad de ampliar nuevas funciones en el sistema sólo añadiendo módulos; <br/> <br/>
• Sistema permite configuración y actualización por pendrive - agilidad en la carga de configuraciones a través de archivos modelos y reducción del tiempo de mantenimiento; <br/> <br/>
• Copia de seguridad de configuración del controlador en la interfaz - en caso extremo de cambio de la controladora no existe pérdida de tiempo del mecánico.<br/><br/>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="segura" data-parent="#accordion" href="#collapse30" aria-expanded="false"><i class="fa fa-plus-circle segura_i" ></i>&nbsp;&nbsp;Configuración segura </a>
                                                </h4>
                                            </div>
                                            <div id="collapse30" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> Configuración y administración a través de tarjetas sin contacto: <br/>
                                                         - Tarjeta Gerencial - permite configurar las opciones de operación de la bomba; <br/>
                                                         - Tarjeta técnica - acceso de nivel técnico que permite alterar los perfiles de funcionamiento; </br>
                                                         - Tarjeta Sistema - gestión de parámetros relativos a la comunicación con el sistema gerencial del puesto.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="identfid" data-parent="#accordion" href="#collapse31" aria-expanded="false"><i class="fa fa-plus-circle identfid_i" ></i>&nbsp;&nbsp;Identificação de Frentistas e Clientes </a>
                                                </h4>
                                            </div>
                                            <div id="collapse31" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> 
                                                        Sistema de identificação Identfid®* da Companytec.<br/>
                                                        Permite  o desbloqueio da bomba mediante identificação, possibilita o controle de produtividade para frentista e a implementação de programas de fidelidade.
                                                        Compatível com outras automações.<br/>
                                                        *O sistema requer licença para o uso.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="armazenamento" data-parent="#accordion" href="#collapse32" aria-expanded="false"><i class="fa fa-plus-circle armazenamento_i" ></i>&nbsp;&nbsp;Almacenamiento de datos </a>
                                                </h4>
                                            </div>
                                            <div id="collapse32" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> 
                                                        Memoria de grabación de los últimos 50 suministros completos.

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="portico" data-parent="#accordion" href="#collapse12" aria-expanded="false"><i class="fa fa-plus-circle portico_i" ></i>&nbsp;&nbsp;Pórtico </a>
                                                </h4>
                                            </div>
                                            <div id="collapse12" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> Pórtico es una solución de diseño en las bombas para la uniformidad del ambiente decór del puesto cuando existe la necesidad de utilizar bombas de manguera alta y baja en la misma cobertura de abastecimiento o para la sustitución de los indicadores productos, de Isla o posición de abastecimiento.</p>
                                                </div>
                                            </div>
                                        </div>
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="bicos" data-parent="#accordion" href="#collapseTwo" aria-expanded="false"><i class="fa fa-plus-circle bicos_i" ></i>&nbsp;&nbsp;Soporte de boquillas</a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Soporte de boquillas con indicador de producto en policarbonato que garantiza durabilidad y facilita la limpieza y el mantenimiento.
                                                    </p>
                                                </div>
                                            </div>
                                        </div> 
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="base" data-parent="#accordion" href="#collapseFour" aria-expanded="false"><i class="fa fa-plus-circle base_i" ></i>&nbsp;&nbsp;Base Robusta</a>
                                                </h4>
                                            </div>
                                            <div id="collapseFour" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Base robusta construida con materiales que no sufren la corrosión.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
										
										
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="mostrador" data-parent="#accordion" href="#collapse6" aria-expanded="false"><i class="fa fa-plus-circle mostrador_i" ></i>&nbsp;&nbsp;Pantalla</a>
                                                </h4>
                                            </div>
                                            <div id="collapse6" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Pantalla en vidrio templado y serigrafado internamente con dígitos blancos en fondo negro para facilitar la lectura de día o de noche.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="display" data-parent="#accordion" href="#collapse5" aria-expanded="false"><i class="fa fa-plus-circle display_i" ></i>&nbsp;&nbsp;Indicadores de producto (Display)</a>
                                                </h4>
                                            </div>
                                            <div id="collapse5" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Indicador (Display) con dígito de 1.5 "con iluminación en LED y fondo negro. Ahorro de energía y mayor vida útil de iluminación.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="totalizador" data-parent="#accordion" href="#collapse7" aria-expanded="false"><i class="fa fa-plus-circle totalizador_i" ></i>&nbsp;&nbsp;Totalizador</a>
                                                </h4>
                                            </div>
                                            <div id="collapse7" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Totalizador perpetuo con tecnología e-ink (la imagen no desaparece incluso con la bomba apagada). Agilidad en la conferencia de los totalizadores electrónicos, informando número del pico y totalizador completo.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="teclado" data-parent="#accordion" href="#collapse8" aria-expanded="false"><i class="fa fa-plus-circle teclado_i" ></i>&nbsp;&nbsp;Teclado Touch</a>
                                                </h4>
                                            </div>
                                            <div id="collapse8" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Teclado táctil en el cristal de la pantalla, con números iluminados en LED. Durabilidad y estanqueidad (no entra agua).
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="indicadores" data-parent="#accordion" href="#collapse9" aria-expanded="false"><i class="fa fa-plus-circle indicadores_i" ></i>&nbsp;&nbsp;Indicadores de producto</a>
                                                </h4>
                                            </div>
                                            <div id="collapse9" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>Indicadores de producto adhesivos detrás del cristal; mejor acabado y durabilidad.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="multimidia" data-parent="#accordion" href="#collapse10" aria-expanded="false"><i class="fa fa-plus-circle multimidia_i" ></i>&nbsp;&nbsp;multimedia</a>
                                                </h4>
                                            </div>
                                            <div id="collapse10" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Multimedia (opcional) desarrollada y gestionada por el puesto, con monitor en LCD de 10.4 "y conexiones por cable o pendrive. La multimedia de las bombas WERTCO, pueden servir para: <br/>
                                                        - Promoción de productos y servicios; <br/> - Canal exclusivo de comunicación con los clientes; <br/> - Generación de ingresos adicionales;                                                 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <!--<div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="antifraude" data-parent="#accordion" href="#collapse14" aria-expanded="false"><i class="fa fa-plus-circle" ></i>&nbsp;&nbsp;Antifraude</a>
                                                </h4>
                                            </div>
                                            <div id="collapse14" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                         Transdutor sem partes móveis, com encoder magnético absoluto que detecta a posição instantânea do medidor sem possibilidade de perder pulsos como em tecnologias de bombas atuais. Esta tecnologia, com alta resolução, inibe e dificulta qualquer tipo de fraude.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>-->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="conectividade" data-parent="#accordion" href="#collapse15" aria-expanded="false"><i class="fa fa-plus-circle conectividade_i" ></i>&nbsp;&nbsp;conectividad</a>
                                                </h4>
                                            </div>
                                            <div id="collapse15" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        - Conexión Ethernet; <br/> - Bluetooth opcional; <br/> - Loop de corriente (compatible con la mayoría de los sistemas de automatización del mercado).
														
                                                    </p>
                                                </div>
                                            </div>
										</div>                                                                              
                                       
										<div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="smc" data-parent="#accordion" href="#collapseOne" aria-expanded="false"><i class="fa fa-plus-circle smc_i"  ></i>&nbsp;&nbsp;Bases, Columnas Laterales, Tapas, Soporte de Boquillas y Acabados fabricados en SMC</a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <p>
                                                        El SMC es un compuesto que consiste en una resina termofijo, fibras de refuerzo, cargas minerales y aditivos termoplásticos.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="revestimentos" data-parent="#accordion" href="#collapseFive" aria-expanded="false"><i class="fa fa-plus-circle revestimentos_i" ></i>&nbsp;&nbsp;Revestimentos dos painéis frontais, laterais, pórticos e fechamentos</a>
                                                </h4>
                                            </div>
                                            <div id="collapseFive" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Revestimientos de los paneles frontales, laterales, pórticos y cerramientos.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>     
                                         <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="teclado" data-parent="#accordion" href="#collapsesFive" aria-expanded="false"><i class="fa fa-plus-circle teclado_i" ></i>&nbsp;&nbsp;Teclado de predetermina</a>
                                                </h4>
                                            </div>
                                            <div id="collapsesFive" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Hasta tres niveles de precios configurables en la bomba (a la vista, tarjeta de crédito y tarjeta de débito).
                                                    </p>
                                                </div>
                                            </div>
                                        </div>  
                                         <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="certificacoes" data-parent="#accordion" href="#collapseses" aria-expanded="false"><i class="fa fa-plus-circle certificacoes_i" ></i>&nbsp;&nbsp;Certificaciones</a>
                                                </h4>
                                            </div>
                                            <div id="collapseses" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Certificadas por UL de Brasil y homologadas por el Inmetro, las bombas cumplen con las más modernas normas técnicas brasileñas de protección y seguridad.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="manutencao" data-parent="#accordion" href="#facilmanut" aria-expanded="false"><i class="fa fa-plus-circle manutencao_i"  ></i>&nbsp;&nbsp;Fácil Mantenimiento</a>
                                                </h4>
                                            </div>
                                            <div id="facilmanut" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>
                                                        Diseñada para facilitar y disminuir el tiempo de servicio y mantenimiento.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>                                        
                                        
                                        </div>
                                      </div>      
                                    </div>
                            </div>                                          
                </div>            
        </section>
        <!-- end Accordion Section -->       
        <!-- Blog Section Starts -->
        <section id="certificacoes" class="blog">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Certificaciones</span></h1>
                    <h4></h4>
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-certificate" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
                <!-- Latest Blog Posts Starts -->
                <div class="row" style="margin-top: 50px; ">
                    <div class="col-sm-4">
                        <div class="card">
                            <div style="height: 100px;position: absolute;top: 45%;left: 50%;margin-top: -44px;margin-left: -35px;text-align: center;">
                                <a href="<?php echo base_url('bootstrap/certificados/certificadoUL.pdf');?>" target="blank"><img src="<?php echo base_url('bootstrap/img/ubl.png');?>" height="100"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div style="margin: auto;">
                                <a href="<?php echo base_url('Home/qualidade');?>" target="blank"><img src="<?php echo base_url('bootstrap/certificados/iso.png');?>" height="100%" class="img-fluid"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div style="width: 228px;height: 100px;position: absolute;top: 50%;left: 50%;margin-top: -44px;margin-left: -116px;text-align: center;">
                                <a href="<?php echo base_url('bootstrap/certificados/inmetro.pdf');?>" target="blank"><p>Ordenanza Inmetro/Dimel n.º 11, de 26 de enero de 2018.</p></a>
                            </div>    
                        </div>
                    </div>
                </div>
                <!-- Latest Blog Posts Ends -->
            </div>
        </section>
        
     
        <!-- Newsletter Section Starts -->
        <section class="newsletter">
            <div class="section-overlay">
                <!-- Container Starts -->
                <div class="container">
                    <!-- Main Heading Starts -->
                    <div class="text-center top-text">
                        <h1><span> newsletter </span></h1>
                        <h4></h4>
                    </div>
                    <!-- Main Heading Ends -->
                    <div class="newsletter-content">
                        <p class="text-center">Suscríbase al boletín y reciba información sobre el mercado de los puestos de combustible, <br/> lanzamientos y demás novedades de Wertco</p>
                        <!-- Newsletter Form Starts -->
                        <form class="form-inputs">
                            <!-- Newsletter Form Input Field Starts -->
                            <div class="col-md-12 form-group custom-form-group p-0">
                                <span class="input custom-input">
                                    <input placeholder="Insira seu E-mail" class="input-field custom-input-field" required id="email_newsletter" type="text" />
                                    <label class="input-label custom-input-label" >
                                        <i class="fa fa-envelope-open-o icon icon-field"></i>
                                    </label>
                                </span>
								 <!-- Form Submit Message Starts -->
                                <div class="col-sm-12 text-center output_message_holder">
                                    <p class="output_message" id="output_message_newsletter"></p>
                                </div>
                                <!-- Form Submit Message Ends -->
                            </div>
                            <!-- Newsletter Form Input Field Ends -->
                            <!-- Newsletter Form Submit Button Starts -->
                            <button id="enviar_newsletter" name="submit" type="button" class="custom-button" title="Send">Enviar</button>
                            <!-- Newsletter Form Submit Button Ends -->
                        </form>
                        <!-- Newsletter Form Ends -->
                    </div>
                </div>
                <!-- Container Ends -->
            </div>
        </section>
        <!-- Newsletter Section Ends -->
        <!-- Contact Section Starts -->
        <section id="contact" class="contact">
            <!-- Container Starts -->
            <div class="container">
                <!-- Main Heading Starts -->
                <div class="text-center top-text">
                    <h1><span>Contacto</span></h1>                   
                </div>
                <!-- Main Heading Starts -->
                <!-- Divider Starts -->
                <div class="divider text-center">
                    <span class="outer-line"></span>
                    <span class="fa fa-envelope" aria-hidden="true"></span>
                    <span class="outer-line"></span>
                </div>
                <!-- Divider Ends -->
            </div>
            <!-- Container Ends -->
            <!-- Map Section Starts -->
            <div class="info-map">
                <div class="google-map">                    
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3661.687696628723!2d-46.3295730844523!3d-23.399512661521648!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce8781654bd9bb%3A0xf0545ab63ac06be0!2sAv.+Get%C3%BAlio+Vargas%2C+280+-+Jardim+Vitoria%2C+Aruj%C3%A1+-+SP%2C+07432-575!5e0!3m2!1spt-BR!2sbr!4v1516099257753" width="2000" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <!-- Info Map Boxes Starts -->
            <div class="container">
                <div class="row info-map-boxes">
					<!-- left Info Map Box Starts -->
                    <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-map-marker">
                            <h1>Dirección</h1>
								<p>Avenida Getúlio Vargas, 280 
								<br/>Quadra C - Lote 4 - Jardim Ângelo 
								<br/>07400-230 - Arujá - SP - BR																	
								</p>
                        </div>
                    </div>
					
                    <!-- left Info Map Box Ends -->
                    <!-- center Info Map Box Starts -->
                    <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-phone">
                            <h1>Teléfono &amp; E-mail</h1>
								<p>Teléfono : +55 11 3900-2565									
									<br><a href="#">vendas@wertco.com.br</a><br/>
								</p>
                        </div>
                    </div>
                    <!-- center Info Map Box Ends -->
                    <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-wrench">
                            <h1>Asistencia Técnica</h1>
                            <p>Teléfono: +55 11 3900 - 2565 <br/>
                            <a href="#">servicos@wertco.com.br</a><br/>    
                            </p>
                        </div>
                    </div>    
					 <!-- Right Info Map Box Starts -->
					 <div class="col-md-3 col-sm-12">
                        <div class="info-map-boxes-item fa fa-clock-o">
                            <h1>Tratamiento</h1>
                            <p>De lunes a viernes : 08:00–17:00                                
                                </p>
                        </div>
                    </div>
                    
                    <!-- Right Info Map Box Ends -->

                </div>
            </div>
            <!-- Info Map Boxes Ends -->
        </section>
        <!-- Contact Section Ends -->
        <!-- Contact Form Section Starts -->
        <section class="contactform" style="-webkit-transform: skewY(0deg) !important;transform: none !important;">
            <div class="section-overlay">
                <div class="container" style="-webkit-transform: skewY(0deg) !important;transform: skewY(0deg) !important;">
                    <!-- Main Heading Starts -->
                    <div class="text-center top-text">
                        <h1><span>Hable con nosotros</span></h1>                        
                    </div>
                    <!-- Main Heading Ends -->
                    <div class="form-container">
                        <!-- Contact Form Starts -->
                        <form class="formcontact" method="post" >
                            <div class="row form-inputs">
                                <!-- First Name Field Starts -->
                                <div class="col-md-6 form-group custom-form-group">
                                    <span class="input custom-input">
										<input placeholder="Nome" class="input-field custom-input-field" id="firstname" name="firstname" type="text" required data-error="NEW ERROR MESSAGE">
										<label class="input-label custom-input-label" >
											<i class="fa fa-user icon icon-field"></i>
										</label>
									</span>
                                </div>
                                <!-- First Name Field Ends -->
                                <!-- Last Name Field Starts -->
                                <div class="col-md-6 form-group custom-form-group">
                                    <span class="input custom-input">
										<input placeholder="Sobrenome" class="input-field custom-input-field" id="lastname" name="lastname" type="text" required>
										<label class="input-label custom-input-label" >
											<i class="fa fa-user-o icon icon-field"></i>
										</label>
									</span>
                                </div>
                                <!-- Last Name Field Ends -->
                                <!-- Phone Field Starts -->
                                <div class="col-md-12 form-group custom-form-group">
                                    <span class="input custom-input">
                                        <input placeholder="Telefone" class="input-field custom-input-field" id="telefone" name="phone" type="text" required>
                                        <label class="input-label custom-input-label" >
                                            <i class="fa fa-phone icon icon-field"></i>
                                        </label>
                                    </span>
                                </div>
                                <!-- Phone Field Ends -->
                                <!-- Message Field Starts -->
                                <div class="form-group custom-form-group col-md-12">
                                    <textarea placeholder="Mensagem" id="message" name="message" cols="45" rows="7" required></textarea>
                                </div>
                                <!-- Message Field Ends -->
                                <!-- Email Name Field Starts -->
                                <div class="col-md-6 form-group custom-form-group">
                                    <span class="input custom-input">
										<input placeholder="E-mail" class="input-field custom-input-field" id="email_contact" name="email_contact" type="text" required>
										<label class="input-label custom-input-label" >
											<i class="fa fa-envelope icon icon-field"></i>
										</label>
									</span>
                                </div>
                                <!-- Email Name Field Ends -->
                                <!-- Submit Button Starts -->
                                <div class="col-md-6 submit-form">
                                    <button id="form-submit" name="submit" type="button" class="custom-button" title="Send">Enviar</button>
                                </div>
                                <!-- Submit Button Ends -->
                                <!-- Form Submit Message Starts -->
                                <div class="col-sm-12 text-center output_message_holder"> 
                                    <p class="output_message" id="contato"></p>
                                </div>
                                <!-- Form Submit Message Ends -->
                            </div>
                        </form>
                        <!-- Contact Form Ends -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact Form Section Ends -->
        <!-- Logos Section Starts -->
        <section class="logos">
          <!-- Footer Section Starts -->
        <footer class="footer text-center">
            <!-- Container Starts -->
            <div class="container">                
                <p>Wertco Ind. Com. y servicios de mantenimiento em Bombas LTDA.<br/>CNPJ.: 27.314.980/0001-53 </p>
                <p>
                    &copy; Copyright <?php echo date('Y'); ?> Wertco 
                </p>                
            </div>
            <!-- Container Ends -->
            <div class="social-icons" style="margin-bottom: 40px;">
                <ul class="social">
                    <!--<li>
                        <a class="twitter" href="#" title="twitter"></a>
                    </li>-->
                    <li>
                        <a class="facebook" href="https://www.facebook.com/Wertco-146223742724238/" title="facebook"></a>
                    </li>
                    <!--<li>
                        <a class="google" href="#" title="google"></a>
                    </li>
                    <li>
                        <a class="skype" href="#" title="skype"></a>
                    </li>-->
                    <li>
                        <a class="instagram" href="https://www.instagram.com/wertco_bombas/" title="instagram"></a>
                    </li>
                    <!--<li>
                        <a class="linkedin" href="#" title="linkedin"></a>
                    </li>
                    <li>
                        <a class="youtube" href="#" title="youtube"></a>
                    </li>-->
                </ul>
            </div>
            <div style="margin-right: 13px; ">
                <a href="http://www.companytec.com.br" target="blank">
                    <img src="<?php echo base_url('bootstrap/img/logoCompanytec.png')?>" /> 
                </a>
            </div>
        </footer>
        <!-- Footer Section Starts -->
        </section>
        <!-- Logos Section Ends -->
        
        <!-- Back To Top Starts -->
        <div id="back-top-wrapper" class="d-none d-sm-block">
            <p id="back-top">
                <a href="index.html#top"><span></span></a>
            </p>
        </div>
        <!-- Back To Top Ends -->
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyBpN0oc0NeZv9JolJSIzQRNW9IkUOfKrxw"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <!--<script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>-->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js')?>"></script>

    <!-- Revolution Slider Main JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/jquery.themepunch.tools.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/jquery.themepunch.revolution.min.js')?>"></script>

    <!-- Revolution Slider Extensions -->

    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.actions.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.carousel.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.migration.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.navigation.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.parallax.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/revolution/js/extensions/revolution.extension.video.min.js')?>"></script>
    
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>
    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>

    <!-- Revolution Slider Initialization Starts -->
    <script type="text/javascript">
	(function(){
		"use strict";
        var tpj = jQuery;
        var revapi4;
        tpj(document).ready(function() {
            if (tpj("#rev_slider").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider");
            } else {
                revapi4 = tpj("#rev_slider").show().revolution({
                    sliderType: "standard",
                    jsFileLocation: "js/plugins/revolution/js/",
                    dottedOverlay: "none",
					sliderLayout:"fullscreen",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "zeus",
                            enable: true,
                            hide_onmobile: true,
                            hide_under: 600,
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            tmp: '<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 90,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 90,
                                v_offset: 0
                            }
                        },
                        bullets: {
                            enable: false,
                            hide_onmobile: true,
                            hide_under: 600,
                            style: "metis",
                            hide_onleave: true,
                            hide_delay: 200,
                            hide_delay_mobile: 1200,
                            direction: "horizontal",
                            h_align: "center",
                            v_align: "bottom",
                            h_offset: 0,
                            v_offset: 30,
                            space: 5,
                            tmp: '<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span>'
                        }
                    },
                    viewPort: {
                        enable: true,
                        outof: "pause",
                        visible_area: "80%"
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [600, 600, 500, 400],
                    lazyType: "none",
                    parallax: {
                        type: "mouse",
                        origo: "slidercenter",
                        speed: 2000,
                        levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
                    },
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    hideThumbsOnMobile: "off",
                    autoHeight: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        });
		
		// GOOGLE MAP
		function init_map() {
			
			var myOptions = {
				scrollwheel: false,
				zoom: 12,
				center: new google.maps.LatLng(-23.3994819,-46.3279319,19),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
			var marker = new google.maps.Marker({
				map: map,
				icon: "img/markers/yellow.png",
				position: new google.maps.LatLng(-23.3994819,-46.3279319,19)
			});
			var infowindow = new google.maps.InfoWindow({
				content: "<strong>WERTCO</strong><br>São Paulo<br>"
			});
			google.maps.event.addListener(marker, "click", function() {
				infowindow.open(map, marker);
			});
		}
		google.maps.event.addDomListener(window, "load", init_map);
		
       

	})(jQuery);
    </script>
    <!-- Revolution Slider Initialization Ends -->
</body>

</html>