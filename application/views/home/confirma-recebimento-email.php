    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/modernizr.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/jquery-2.2.4.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.easing.1.3.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/bootstrap.bundle.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.bxslider.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.filterizr.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js'); ?>"></script>    
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>"></script>     
    <script type="text/javascript" src="<?=base_url('bootstrap/js/sweetalert.js')?>"></script>

<?php echo '<span style ="color: #fff;">';print_r($this->session->flashdata());echo '</span>'; if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Recebemos sua confirmação! Qualquer dúvida entre em contato com o setor de Assistência Técnica da WERTCO!',
                type: 'success'
            }).then(function() {
                window.location.href = 'http://www.wertco.com.br/';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>