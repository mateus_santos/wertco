<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="x-apple-disable-message-reformatting" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><meta name="format-detection" content="telephone=no" /><title></title><style type="text/css">
    /* Resets */
    .ReadMsgBody { width: 100%; background-color: #ebebeb;}
    .ExternalClass {width: 100%; background-color: #ebebeb;}
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
    a[x-apple-data-detectors]{
        color:inherit !important;
        text-decoration:none !important;
        font-size:inherit !important;
        font-family:inherit !important;
        font-weight:inherit !important;
        line-height:inherit !important;
    }        
    body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
    body {margin:0; padding:0;}
    .yshortcuts a {border-bottom: none !important;}
    .rnb-del-min-width{ min-width: 0 !important; }

    /* Add new outlook css start */
    .templateContainer{
        max-width:590px !important;
        width:auto !important;
    }
    /* Add new outlook css end */

    /* Image width by default for 3 columns */
    img[class="rnb-col-3-img"] {
        max-width:170px;
    }

    /* Image width by default for 2 columns */
    img[class="rnb-col-2-img"] {
        max-width:264px;
    }

    /* Image width by default for 2 columns aside small size */
    img[class="rnb-col-2-img-side-xs"] {
        max-width:180px;
    }

    /* Image width by default for 2 columns aside big size */
    img[class="rnb-col-2-img-side-xl"] {
        max-width:350px;
    }

    /* Image width by default for 1 column */
    img[class="rnb-col-1-img"] {
        max-width:550px;
    }

    /* Image width by default for header */
    img[class="rnb-header-img"] {
        max-width:590px;
    }

    /* Ckeditor line-height spacing */
    .rnb-force-col p, ul, ol{margin:0px!important;}
    .rnb-del-min-width p, ul, ol{margin:0px!important;}

    /* tmpl-2 preview */
    .rnb-tmpl-width{ width:100%!important;}

    /* tmpl-11 preview */
    .rnb-social-width{padding-right:15px!important;}

    /* tmpl-11 preview */
    .rnb-social-align{float:right!important;}

    /* Ul Li outlook extra spacing fix */
    li{mso-margin-top-alt: 0; mso-margin-bottom-alt: 0;}        

    /* Outlook fix */
    table {mso-table-lspace:0pt; mso-table-rspace:0pt;}
    
    /* Outlook fix */
    table, tr, td {border-collapse: collapse;}

    /* Outlook fix */
    p,a,li,blockquote {mso-line-height-rule:exactly;} 

    /* Outlook fix */
    .msib-right-img { mso-padding-alt: 0 !important;}

    /* Fix text line height on preview */
    .content-spacing div {display: list-item; list-style-type: none;}

    @media only screen and (min-width:590px){
        /* mac fix width */
        .templateContainer{width:590px !important;}
    }

    @media screen and (max-width: 360px){
        /* yahoo app fix width "tmpl-2 tmpl-10 tmpl-13" in android devices */
        .rnb-yahoo-width{ width:360px !important;}
    }

    @media screen and (max-width: 380px){
        /* fix width and font size "tmpl-4 tmpl-6" in mobile preview */
        .element-img-text{ font-size:24px !important;}
        .element-img-text2{ width:230px !important;}
        .content-img-text-tmpl-6{ font-size:24px !important;}
        .content-img-text2-tmpl-6{ width:220px !important;}
    }

    @media screen and (max-width: 480px) {
        td[class="rnb-container-padding"] {
            padding-left: 10px !important;
            padding-right: 10px !important;
        }

        /* force container nav to (horizontal) blocks */
        td.rnb-force-nav {
            display: inherit;
        }

        /* fix text alignment "tmpl-11" in mobile preview */
        .rnb-social-text-left {
            width: 100%;
            text-align: center;
            margin-bottom: 15px;
        }
        .rnb-social-text-right {
            width: 100%;
            text-align: center;
        }
    }

    @media only screen and (max-width: 600px) {

        /* center the address &amp; social icons */
        .rnb-text-center {text-align:center !important;}

        /* force container columns to (horizontal) blocks */
        td.rnb-force-col {
            display: block;
            padding-right: 0 !important;
            padding-left: 0 !important;
            width:100%;
        }

        table.rnb-container {
           width: 100% !important;
       }

       table.rnb-btn-col-content {
        width: 100% !important;
    }
    table.rnb-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
    }

    table.rnb-last-col-3 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
    }

    table.rnb-col-2 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
        /*border-bottom: 1px solid #eee;*/
    }

    table.rnb-col-2-noborder-onright {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-bottom: 10px;
        padding-bottom: 10px;
    }

    table.rnb-col-2-noborder-onleft {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;

        /* change left/right padding and margins to top/bottom ones */
        margin-top: 10px;
        padding-top: 10px;
    }

    table.rnb-last-col-2 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
    }

    table.rnb-col-1 {
        /* unset table align="left/right" */
        float: none !important;
        width: 100% !important;
    }

    img.rnb-col-3-img {
        /**max-width:none !important;**/
        width:100% !important;
    }

    img.rnb-col-2-img {
        /**max-width:none !important;**/
        width:100% !important;
    }

    img.rnb-col-2-img-side-xs {
        /**max-width:none !important;**/
        width:100% !important;
    }

    img.rnb-col-2-img-side-xl {
        /**max-width:none !important;**/
        width:100% !important;
    }

    img.rnb-col-1-img {
        /**max-width:none !important;**/
        width:100% !important;
    }

    img.rnb-header-img {
        /**max-width:none !important;**/
        width:100% !important;
        margin:0 auto;
    }

    img.rnb-logo-img {
        /**max-width:none !important;**/
        width:100% !important;
    }

    td.rnb-mbl-float-none {
        float:inherit !important;
    }

    .img-block-center{text-align:center !important;}

    .logo-img-center
    {
        float:inherit !important;
    }

    /* tmpl-11 preview */
    .rnb-social-align{margin:0 auto !important; float:inherit !important;}

    /* tmpl-11 preview */
    .rnb-social-center{display:inline-block;}

    /* tmpl-11 preview */
    .social-text-spacing{margin-bottom:0px !important; padding-bottom:0px !important;}

    /* tmpl-11 preview */
    .social-text-spacing2{padding-top:15px !important;}

}</style><!--[if gte mso 11]><style type="text/css">table{border-spacing: 0; }table td {border-collapse: separate;}</style><![endif]--><!--[if !mso]><!--><style type="text/css">table{border-spacing: 0;} table td {border-collapse: collapse;}</style> <!--<![endif]--><!--[if gte mso 15]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--><!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head><body>

    <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" class="main-template" bgcolor="#fff" style="background-color: rgb(255, 255, 255);">

        <tbody><tr style="display:none !important; font-size:1px; mso-hide: all;"><td> </td></tr><tr>
            <td align="center" valign="top">
        <!--[if gte mso 9]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="590" style="width:590px;">
                        <tr>
                        <td align="center" valign="top" width="590" style="width:590px;">
                        <![endif]-->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="max-width:590px!important; width: 590px;">
                            <tbody><tr>

                                <td align="center" valign="top">

                                    <div style="background-color: rgb(255, 255, 255);">
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%; -webkit-backface-visibility: hidden; line-height: 10px;" name="Layout_19" id="Layout_19">
                    <tbody><tr>
                        <td class="rnb-del-min-width" valign="top" align="center" style="min-width: 590px;">
                            <table width="100%" class="rnb-container" cellpadding="0" border="0" bgcolor="#ffffff" align="center" cellspacing="0" style="background-color: rgb(255, 255, 255);">
                                <tbody><tr>
                                    <td valign="top" align="center">
                                        <table cellspacing="0" cellpadding="0" border="0">
                                            <tbody><tr>
                                                <td>
                                                    <div style="display:block; border-radius:0px; width:590;;max-width:700px !important;border-top:0px None #000;border-right:0px None #000;border-bottom:0px None #000;border-left:0px None #000;border-collapse: separate;border-radius: 0px;">
                                                        <div>
                                                            <img ng-if="col.img.source != 'url'" border="0" hspace="0" vspace="0" width="590" class="rnb-header-img" alt="" style="display:block; float:left; border-radius: 0px; " src="https://img.mailinblue.com/2056864/images/rnb/original/5d9f77e4545a9824d228277d.png"></div><div style="clear:both;"></div>
                                                        </div></td>
                                                    </tr>
                                                </tbody></table>

                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr></tbody></table>
            <!--[if mso]>
                </td>
            <![endif]-->

                <!--[if mso]>
                </tr>
                </table>
            <![endif]-->
        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_22">
                    <tbody><tr>
                        <td class="rnb-del-min-width" align="center" valign="top">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">
                                <tbody>
                                <tr>
                                    <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="top" class="rnb-container-padding" align="left">

                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                            <tbody><tr>
                                                <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                    <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                        <tbody><tr>
                                                            <td class="content-spacing" style="font-size:14px; font-family:'Lucida Sans Unicode',Lucida Grande,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"><table cellpadding="0" cellspacing="0" style="width: 580px !important;" width="580">
                                                               <thead>
                                                                  <tr>
                                                                     <th bgcolor="#ffcc00" height="40" style="text-align: center;" width="50"><span style="font-size:12px;"><span style="color:#000000;">ID</span></span></th>
                                                                     <th bgcolor="#ffcc00" height="40" style="text-align: center;" width="280"><span style="font-size:12px;"><span style="color:#000000;">CLIENTE</span></span></th>
                                                                     <th bgcolor="#ffcc00" height="40" style="text-align: center;" width="200"><span style="font-size:12px;"><span style="color:#000000;">RESPONSÁVEL</span></span></th>
                                                                     <th bgcolor="#ffcc00" height="40" style="text-align: center;" width="50"><span style="font-size:12px;"><span style="color:#000000;">DIAS</span></span></th>
                                                                 </tr>
                                                             </thead>
                                                             <tbody>
                                                              <tr>
                                                                 <td bgcolor="#CCCCCC" height="35" style="text-align: center;"><span style="font-size:12px;">#1</span></td>
                                                                 <td bgcolor="#E6E6E6" height="35" style="text-align: center;"><span style="font-size:12px;">112.312.131/0001-58 <br/> BIBIANA LOUCA LTDA</span></td>
                                                                 <td bgcolor="#CCCCCC" height="35" style="text-align: center;"><span style="font-size:12px;">SR. BIBIANA - REPRESENTANTE</span></td>
                                                                 <td bgcolor="#E6E6E6" height="35" style="text-align: center;"><span style="font-size:12px;">15</span></td>
                                                             </tr>
                                                             <tr>
                                                                <td colspan="4" bgcolor="#ffffff" height="2"></td>
                                                             </tr>
                                                             <tr>
                                                                 <td bgcolor="#CCCCCC" height="35" style="text-align: center;"><span style="font-size:12px;">#1</span></td>
                                                                 <td bgcolor="#E6E6E6" height="35" style="text-align: center;"><span style="font-size:12px;">112.312.131/0001-58 <br/> BIBIANA LOUCA LTDA</span></td>
                                                                 <td bgcolor="#CCCCCC" height="35" style="text-align: center;"><span style="font-size:12px;">SR. BIBIANA - REPRESENTANTE</span></td>
                                                                 <td bgcolor="#E6E6E6" height="35" style="text-align: center;"><span style="font-size:12px;">15</span></td>
                                                             </tr>
                                                         </tbody>
                                                     </table>
                                                 </td>
                                             </tr>
                                         </tbody></table>

                                     </td></tr>
                                 </tbody></table></td>
                             </tr>
                             <tr>
                                <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table><!--[if mso]>
                </td>
            <![endif]-->

                <!--[if mso]>
                </tr>
                </table>
            <![endif]-->

        </div></td>
    </tr><tr>

        <td align="center" valign="top">

            <div style="background-color: rgb(255, 255, 255); border-radius: 0px;">
                <!--[if mso]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                <tr>
                <![endif]-->
                
                <!--[if mso]>
                <td valign="top" width="590" style="width:590px;">
                <![endif]-->
                <table class="rnb-del-min-width" width="100%" cellpadding="0" border="0" cellspacing="0" style="min-width:100%;" name="Layout_20">
                    <tbody><tr>
                        <td class="rnb-del-min-width" align="center" valign="top">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-container" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255); padding-left: 20px; padding-right: 20px; border-collapse: separate; border-radius: 0px; border-bottom: 0px none rgb(200, 200, 200);">

                                <tbody><tr>
                                    <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td valign="top" class="rnb-container-padding" align="left">

                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="rnb-columns-container">
                                            <tbody><tr>
                                                <td class="rnb-force-col" valign="top" style="padding-right: 0px;">

                                                    <table border="0" valign="top" cellspacing="0" cellpadding="0" width="100%" align="left" class="rnb-col-1">

                                                        <tbody><tr>
                                                            <td class="content-spacing" style="font-size:14px; font-family:Arial,Helvetica,sans-serif, sans-serif; color:#3c4858; line-height: 21px;"></td>
                                                        </tr>
                                                    </tbody></table>

                                                </td></tr>
                                            </tbody></table></td>
                                        </tr>
                                        <tr>
                                            <td height="20" style="font-size:1px; line-height:0px; mso-hide: all;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
            </tbody></table><!--[if mso]>
                </td>
            <![endif]-->

                <!--[if mso]>
                </tr>
                </table>
            <![endif]-->

        </div></td>
    </tr></tbody></table>
            <!--[if gte mso 9]>
                        </td>
                        </tr>
                        </table>
                    <![endif]-->
                </td>
            </tr>
        </tbody></table>

    </body></html>