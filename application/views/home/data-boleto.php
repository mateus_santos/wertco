  <head>
    <meta charset="utf-8" />
    <title>WERTCO - Data Boleto</title>
    <meta name="description" content="Default form examples">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="max-age=0" />
    
    <!--begin::Web font -->
    <script src="<?=base_url('bootstrap/assets/webfont/1.6.16/webfont.js')?>"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <script type="text/javascript">var base_url = '<?php echo base_url(); ?>';</script>
    <script src="<?=base_url('bootstrap/assets/demo/default/custom/components/utils/jquery.js')?>" type="text/javascript"></script> 
    <script src="<?=base_url('bootstrap/js/plugins/bootstrap-datepicker.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/assets/vendors/base/vendors.bundle.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/assets/demo/default/base/scripts.bundle.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/assets/demo/default/custom/components/datatables/base/html-table.js')?>"    type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/assets/demo/default/custom/components/datatables/datatables.min.js')?>"     type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/assets/demo/default/custom/components/base/sweetalert2.js')?>" type="text/javascript"></script> 
    <script src="<?=base_url('bootstrap/js/jquery.mask.min.js')?>" type="text/javascript"></script> 
    <script src="<?=base_url('bootstrap/js/plugins/ion.rangeSlider.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/js/plugins/jquery.blockui.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/js/plugins/tether.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/js/plugins/shepherd.min.js')?>" type="text/javascript"></script>

    <script src="<?=base_url('bootstrap/assets/vendors/base/jszip.min.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/assets/vendors/base/FileSaver.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/assets/vendors/base/script.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/assets/vendors/base/googled3193f02.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/assets/demo/default/custom/components/forms/validation/form-widgets.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('bootstrap/assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js')?>" type="text/javascript"></script>

    <script src="<?=base_url('bootstrap/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')?>" type="text/javascript"></script>
    <!--end::Page Vendors -->  
    <!--begin::Page Snippets -->
    <script src="<?=base_url('bootstrap/assets/app/js/dashboard.js')?>" type="text/javascript"></script>

    <!--begin::Base Styles -->  
    <link href="<?=base_url('bootstrap/assets/vendors/base/vendors.bundle.css')?>" rel="stylesheet" type="text/css" />  
    <link href="<?=base_url('bootstrap/assets/demo/default/base/style.bundle.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('bootstrap/assets/vendors/base/datatable.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('bootstrap/assets/demo/default/base/jquery-ui.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('bootstrap/css/shepherd-theme-arrows.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('bootstrap/css/shepherd-theme-square-dark.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('bootstrap/css/bootstrap-slider.css')?>" rel="stylesheet" type="text/css" />

    <script src="<?=base_url('bootstrap/js/jquery-editable-select.min.js')?>"></script>
    <script src="<?=base_url('bootstrap/assets/demo/default/custom/components/utils/jquery-ui.min.js')?>" type="text/javascript"></script>

    <link href="<?=base_url('bootstrap/css/jquery-editable-select.min.css')?>" rel="stylesheet">
    
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/assets/demo/default/media/img/logo/favicon1.png')?>" />  
</head>
<body>
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12" style="margin-top: 50px;">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="flaticon-statistics"></i>
                                </span>
                                
                                <h2 class="m-portlet__head-label m-portlet__head-label--danger">
                                    <span>
                                        Data Boleto
                                    </span>
                                </h2>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                          
                        </div>
                    </div>
                    <div class="m-portlet__body container">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-3">                                
                                
                            </div>
                            <div class="col-lg-3">
                                <label>Data Inicial:</label>
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text"   name="dt_ini"  id="dt_ini" class="form-control m-input datepicker" placeholder="data inicial" style="width: 49%;float: left;" value="<?php echo date('d/m/Y');?>"  /> 
                                </div>                          
                            </div>
                            <div class="col-lg-3">
                                <label>Dias a somar:</label>
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" name="dias"  id="dias" class="form-control m-input datepicker" placeholder="data inicial" style="width: 152px;float: left;" value="28"  />
                                    
                                </div>                          
                            </div>
                            <div class="col-lg-3">
                                 <button style="margin-top: 30px; " name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
                            </div>

                        </div>                       
                        <div class="row">
                            <div class="col-lg-12" style="text-align: center;">
                                <h2 id="dt_final"></h2>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</body>
<script type="text/javascript">
    $('#dt_ini').datepicker({format: 'dd/mm/yyyy'});
    $('#enviar').bind('click', function(){
        dt_ini = $('#dt_ini').val().split('/');
        
        $.ajax({
            method: "POST",
            url:  base_url+"Home/somaDiasData", 
            async: false,
            data: { dt_ini  :     dt_ini[2]+'-'+dt_ini[1]+'-'+dt_ini[0],
                    dias    :     $('#dias').val()   }
        }).done(function(data) {
            var dados = $.parseJSON(data);
            dt_fim = dados.dt_fim.split('-');
            $('#dt_final').text(dt_fim[0]+'/'+dt_fim[1]+'/'+dt_fim[2]);
        });

    });
</script>