<!DOCTYPE html>
<html lang="pt_br">
<head>
    <!--Start of Tawk.to Script-->
>
<!--End of Tawk.to Script-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="Wertco-Companytec, bombas de combustiveis, pump machine,ABASTECIMIENTOS  " />
    <meta name="description" content="Wertco - abastecimientos" />
    <meta name="author" content="Mateus Santos - P&D Companytec" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <title>WERTCO - ABASTECIMIENTOS</title>
    
    <!-- Favicon -->
    <!--<link rel="shortcut icon" href="images/favicon.ico" />-->
	<link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon1.png')?>">
    
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;Raleway:300,400,500,600,700,800,900" rel="stylesheet">
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?=base_url('bootstrap/mkt_ext/css/bootstrap.min.css')?>">
    
    <!-- owl-carousel -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/mkt_ext/css/owl-carousel/owl.carousel.css')?>" />
    
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/mkt_ext/css/font-awesome.css')?>" />

    <!-- Magnific Popup -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/mkt_ext/css/magnific-popup/magnific-popup.css')?>" />

    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/mkt_ext/css/animate.css')?>" />
    
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=base_url('bootstrap/mkt_ext/css/ionicons.min.css')?>" />
    
    <!-- Style -->
    <link rel="stylesheet" href="<?=base_url('bootstrap/mkt_ext/css/style.css')?>" />

    <!-- Responsive -->
    <link rel="stylesheet" href="<?=base_url('bootstrap/mkt_ext/css/responsive.css')?>">
    
    <!-- custom style -->
    <link rel="stylesheet" href="<?=base_url('bootstrap/mkt_ext/css/custom.css')?>" />

</head>

<body data-spy="scroll" data-offset="80">

    <!-- loading -->

    <div id="loading">
        <div id="loading-center">
            <div class="loader">
              <div id="largeBox"></div>
              <div id="smallBox"></div>
            </div>
        </div>
    </div>

    <!-- loading End -->

    <!-- Header -->
    <header id="header-wrap">
        <div class="navbar navbar-default navbar-fixed-top menu-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="index.html" class="navbar-brand" style="margin: 6px 0px;">
                        <img class="img-responsive" id="logo_img" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png')?>" alt="logo">
                    </a> 
                </div>
                <div class="navbar-collapse collapse">

                    <!--<a href="<?=base_url('Home/bombasExt')?>" class=" pull-right" >
                        <img src="<?=base_url('bootstrap/mkt_ext/images/espanha.png')?>" class="" style="width: 55px;"/>
                    </a>
                    <a href="<?=base_url('Home/bombasExtIng')?>" class=" pull-right " style="margin-right: 10px;">
                        <img src="<?=base_url('bootstrap/mkt_ext/images/reino-unido.png')?>" class="" style="width: 55px;"/>
                    </a>-->
                   
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->
    <!-- Banner -->
    <!-- Banner End -->
    <!-- Main Content -->
    <div class="main-content">

        <!-- How it Works -->

        <!-- How it Works END -->


        <!-- Who is Sofbox ? -->

        
        <!-- Who is Sofbox ? END -->


        <!-- Software Features -->

        <section id="software-features" class="overview-block-ptb software">
            <div class="iq-software-demo">
                <img class="img-responsive" src="<?=base_url('bootstrap/mkt_ext/images/ImagemBombas.jpg')?>" alt="wertco abastecimientos">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6">                        
                        <p class="iq-font-15">Usted a comprado la mejor máquina que el mercado puede ofrecer, y eso no solo lo hace parte de la red de clientes que más crece en Brasil y América Latina, también lo hace parte nuestra familia <b style="color: #ffcc00">Wertco-Companytec</b>.
Usted tendrá canal abierto a nuestro equipo técnico-comercial, así como línea directa para hablar con nuestros socios fundadores, director y equipo de ingenieros que estarán atentos a escuchar sus problemas y recomendaciones.<br/><br/>

Buscamos al mismo tiempo diferenciación y bajo costo para nuestros clientes. Para eso desarrollaremos productos y servicios disruptivos que desafían la estructura de la industria actual.
En esta pagina usted encontrara todo lo que necesita para su jornada junto a nosotros. Manuales técnicos y de servicio, videos explicativos, brochure de productos e informaciones de utilidad que servirán para conocer mejor todos nuestros modelos.<br/>
Venga a dar un salto junto a nosotros, y hagamos de la competencia algo irrelevante.<br/><br/>

Buenos abastecimientos,<br/>
<b style="color: #ffcc00">Grupo Companytec -Wertco</b><br/><br/><br/>

<a id="baixar" href="<?=base_url('/downloads/ManualdeServicioWertco.pdf')?>">DESCARGAR MANUAL DE SERVICIO</a><br/><br/><br/>

Siéntase en confianza para contactarnos en los siguientes números en caso de más dudas.<br/><br/>

<b style="color: #ffcc00">Sebastian Andres S.</b><br/>
Ejecutivo de Desarrollo de Negocios Internacionales<br/>
Whats: +55 53 99949-4945<br/>
e-mail: global@companytec.com.br<br/><br/>


<b style="color: #ffcc00">Leandro Tabeleão</b><br/>
Gerente Industrial<br/>
Whats: +55 53 99964-9518<br/>
e-mail: industrial@wertco.com.br<br/><br/>


<b style="color: #ffcc00">Matheus Alves</b><br/>
Soporte Técnico<br/>
Whats: +55 11 95340-0031<br/>
e-mail: suporte@wertco.com.br<br/>
</p>
                        <!--<ul class="iq-mt-40 iq-list">
                            <li class="iq-tw-6"><i class="ion-android-download iq-mr-10 iq-font-blue iq-font-30"></i><span class="iq-font-black">Funciona independente do PC.</span></li>
                            <li class="iq-tw-6"><i class="ion-android-done-all iq-mr-10 iq-font-blue iq-font-30"></i><span class="iq-font-black">Controla e gerencia dados das bombas de combustíveis e dispensers GNV.</span></li>
                            <li class="iq-tw-6"><i class="ion-android-done-all iq-mr-10 iq-font-blue iq-font-30"></i><span class="iq-font-black">Design moderno com suporte para parede, suporte para cabos e suporte para Rack (opcional).</span></li>
                            <li class="iq-tw-6"><i class="ion-android-done-all iq-mr-10 iq-font-blue iq-font-30"></i><span class="iq-font-black">Armazena até 10.000 abastecimentos, 8.192 eventos e 65.536 cartões Identfid.</span></li>
                            <li class="iq-tw-6"><i class="ion-android-done-all iq-mr-10 iq-font-blue iq-font-30"></i><span class="iq-font-black">Capacidade para até 192 bicos com até 48 abastecimentos simultâneos.</span></li>
                            <li class="iq-tw-6"><i class="ion-android-done-all iq-mr-10 iq-font-blue iq-font-30"></i><span class="iq-font-black">A interface de comunicação com as bombas possui isolação elétrica, aumentando a proteção contra descargas atmosféricas.</span></li>
                        </ul>						-->
                    </div>
                </div>
            </div>            
			
        </section>

        <!-- Software Features END -->


        
    
    <!-- Footer END -->

    <!-- back-to-top -->        
    <div id="back-to-top">
        <a class="top" id="top" href="#top"> <i class="ion-ios-upload-outline"></i></a>
    </div>

    <!-- back-to-top End -->

    <!-- style-customizer -->
   
    <!-- style-customizer END -->


    <!-- jQuery -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/jquery.min.js')?>"></script>

    <!-- owl-carousel -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/owl-carousel/owl.carousel.min.js')?>"></script>

    <!-- Counter -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/counter/jquery.countTo.js')?>"></script>

    <!-- Jquery Appear -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/jquery.appear.js')?>"></script>

    <!-- Magnific Popup -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/magnific-popup/jquery.magnific-popup.min.js')?>"></script>

    <!-- Retina -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/retina.min.js')?>"></script>

    <!-- wow -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/wow.min.js')?>"></script>

    <!-- Countdown -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/jquery.countdown.min.js')?>"></script>

    <!-- video background -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/jquery.vide.js')?>"></script>

    <!-- Skrollr -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/skrollr.min.js')?>"></script>

    <!-- bootstrap -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/bootstrap.min.js')?>"></script>

    <!-- Style Customizer -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/style-customizer.js')?>"></script>

    <!-- Custom -->
    <script type="text/javascript" src="<?=base_url('bootstrap/mkt_ext/js/custom.js')?>"></script>

</body>

</html>