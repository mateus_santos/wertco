﻿<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 9px;
 	color: #333;
 	width: 100%;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px;
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	
	
}
table tr{	
	
}
table td{
	padding: 2px !important;
	
	font-size: 10px;	
	text-transform: uppercase;
}

p.info{
	font-size: 8px;
}

.borda{
	border: 1px solid #ffcc00;
	padding: 2px;
}

</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<table border="0">
				<tr>
					<td style="margin: 5px auto;">
						<img STYLE="width: 150px;" src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" >
					</td>
					<td style="text-align: right; font-size: 8px;margin-top: -10px;">
						<p > WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
										07400-230 - Arujá - SP<br>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br>
						</p>
					</td>			
				</tr>
			</table>
		</div>
		<div class="corpo" style="margin-top: 350px !important;">	
			<table  style="margin-top: 55px;" >
				<tr>
					<td style="border: 0 !important;"><h3 style="text-align: left;margin-top: 15px;	">Confirmação de Pedido: <?php echo $empresa[0]['id'].'/'.date('y'); ?></h3></td>			

					<td style="border: 0 !important;"><h3 style="text-align: right;margin-top: 15px;	">Referência Orçamento: <?php echo $empresa[0]['orcamento_id'].'/'.date('y'); ?></h3></td>		
				</tr>
			</table>
			<table cellspacing="0" style="margin-top: 5px; margin-bottom: 15px;" class="borda">
				<tbody>
				<tr>
					<td style=" "><b>Data Emissão:</b> <?php echo date('d/m/Y'); ?></td>
					<td></td>
					<td style=" "></td>
				</tr>
				<tr>	
					<td><b><?php echo ($empresa[0]['tp_cadastro'] == 'PJ' || $empresa[0]['tp_cadastro'] == '') ? 'Razão Social' : 'Nome'; ?>: </b><?php echo $empresa[0]['razao_social']; ?></td>
					<td></td>
					<td><b>Contato: </b><?php echo $empresa[0]['nome_cliente']; ?></td>
				</tr>
				<tr>
					<td><b><?php echo ($empresa[0]['tp_cadastro'] == 'PJ' || $empresa[0]['tp_cadastro'] == '') ? 'CNPJ' : 'CPF'; ?>: </b><?php echo $empresa[0]['cnpj']; ?></td>
					<td><b><?php echo ($empresa[0]['tp_cadastro'] == 'PJ' || $empresa[0]['tp_cadastro'] == '') ? 'I.E:' : ''; ?></b><?php echo $empresa[0]['insc_estadual']; ?></td>					
					<td><b>Suframa:</b></td>
				</tr>				
				<tr>
					<td><b>Telefone: </b><?php echo $empresa[0]['telefone']; ?> </td>
					<td><b>Celular: </b><?php echo ($empresa[0]['celular_contato'] != '') ? $empresa[0]['celular_contato'] : $empresa[0]['telefone']; ?> </td>	
					<td><b>E-mail: </b><?php echo strtolower($empresa[0]['email']); ?></td>		
				</tr>
				<tr>
					<td><b>Endereço Principal:</b> <?php echo $empresa[0]['endereco']; ?></td>
					<td><b>Bairro:</b> <?php echo $empresa[0]['bairro']; ?></td>					
					<td><b>CEP:</b><?php echo $empresa[0]['cep']; ?></td>			
				</tr>
				<tr>
					<td><b>Cidade:</b><?php echo $empresa[0]['cidade']; ?></td>
					<td><b>UF:</b><?php echo $empresa[0]['estado']; ?></td>					
					<td></td>					
				</tr>
				<tr>
					<td colspan="3"><b>Endereço de Entrega:</b> <?php echo $empresa[0]['endereco_entrega']; ?></td>					
				</tr>
			</tbody>
		</table>
				
		<table class="table borda" cellspacing="0" style="margin-top: 5px; margin-bottom: 15px; padding: 5px;" >
			<thead bgcolor="#ccc" style="background-color: #ccc;font-size: 10px;" class="borda">
			
				<tr >
					<th style="text-align: center;">#</th>					
					<th style="text-align: center;">QTD</th>
					<th style="text-align: center;">MODELO</th>
					<th style="text-align: center;">DESCRIÇÃO</th>
					<th style="text-align: center;">PRODUTOS</th>
					<th style="text-align: right;">PREÇO LIQ.</th>
					<th style="text-align: center;">IPI</th>
					<th style="text-align: right;">PREÇO ITEM</th>
					<th style="text-align: right;">TOTAL P/ ITEM</th>
				</tr>
			</thead>
			<tbody class="borda">
				<?php $numb=1;$total=0; foreach($produtos as $produto){ ?>
				<tr>
					<td cellspacing="0" style="text-align: center;"><?php echo $numb; ?></td>
					<td cellspacing="0" style="text-align: center;"><?php echo $produto['qtd']; ?></td>					
					<td cellspacing="0" style="text-align: center;"><?php echo $produto['modelo']; ?></td>
					<td cellspacing="0" style="text-align: center;"><?php echo $produto['descricao']; ?></td>
					<td cellspacing="0" style="text-align: center;"><?php echo $produto['produtos']; ?></td>
					<td cellspacing="0" style="text-align: right;">R$ <span class="valor_unitario"><?php echo number_format($produto['valor'] / 1.05, 2, ',', '.');?></span></td>
					<td cellspacing="0" style="text-align: center;">5%</td>
					<td cellspacing="0" style="text-align: right;">R$ <span class="valor_unitario"><?php echo number_format($produto['valor'], 2, ',', '.');?></span></td>
					<td cellspacing="0" style="text-align: right;">R$ <span class="valor_unitario"><?php echo number_format($produto['valor'] * $produto['qtd'], 2, ',', '.');?></span></td>
				</tr>			
				<?php $numb++; $total = ($produto['valor'] * $produto['qtd']) + $total;} ?>	
				
				<tr>										
					<td colspan="8" style="text-align: right;"><b>Total</b></td>					
					<td style="text-align: right;"><b class="valor_unitario">R$ <?php echo number_format($total, 2, ',', '.');?></b></td>

				</tr>
				<tr>					
					<td colspan="9" style="text-align: left;"><b>OS PREÇOS OFERTADOS INCLUEM ICMS E IPI.</b></td>					
					
				</tr>
				<tr>
					<td colspan="9" style="text-align: left;"><b>ATENÇÃO: A DIFERENÇA DE ALÍQUOTA DE ICMS DEVIDA AO ESTADO DE DESTINO É DE RESPONSABILIDADE DO CLIENTE. </b></td>										

				</tr>				

			</tbody>
		</table>
<?php 
	$identidade_visual = "CONFORME ARQUIVO ANEXADO AO PEDIDO."; 
	if($empresa[0]['fl_identidade_visual'] == 'A' ){
		$identidade_visual = "AGUARDANDO DEFINIÇÃO.";	
	}else{
		$identidade_visual = " NÃO POSSUI IDENTIDADE VISUAL.";	
	} 

?>
			<p><b>MOTOR:</b>  <?php echo mb_strtoupper($empresa[0]['motor']); ?> </p>
			<p><b>TÉCNICO RESPONSÁVEL PELA STARTUP E GARANTIA:</b> <?php echo mb_strtoupper($empresa[0]['tecnico']); ?> </p>
			<p><b>CONDIÇÃO DE PAGAMENTO:</b> 
				<?php foreach($forma_pagto as $pagto){
					echo ($pagto['porcentagem'] != '0.00') ? '<br/>'.$pagto['porcentagem']."% (R$".number_format(($pagto['porcentagem']/100)*$total, 2, ',', '.').") - ".mb_strtoupper($pagto['descricao']).". " : '<br/>'.mb_strtoupper($pagto['descricao']);
				} ?>
			</p>
			<p style="background: #f0f0f0;"><b>ATENÇÃO:</b> O PAGAMENTO DEVE SER EFETUADO APENAS E DIRETAMENTE NA CONTA CORRENTE DA WERTCO LTDA.</p>
			<p><b>DADOS PARA DEPÓSITO:</b> BANCO ITAÚ: BCO: 341, AG: 8454, C/C: 30100-6 <BR/>BANCO DO BRASIL: BCO 001, AG: 0029 (TRANSF. ENTRE BANCOS UTILIZAR 29-9), C/C: 166351-8<BR/> CNPJ: 27.314.980/0001-53 - FAVORECIDO: WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA. 
			<div style="PAGE-BREAK-AFTER: always"><p style="text-align: right;border-radius: 10px 10px 10px 10px solid #000;">1</p></div> 
			<div class="cabecalho">			
				<table border="0">
					<tr>
						<td style="margin: 5px auto;">
							<img STYLE="width: 150px;" src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" >
						</td>
						<td style="text-align: right; font-size: 8px;margin-top: -10px;">
							<p > WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
											CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
											Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
											07400-230 - Arujá - SP<br>
											Tel: +55 11 3900-2565 | www.wertco.com.br<br>
							</p>
						</td>			
					</tr>
				</table>
			</div>
			<div style="font-size: 10px; margin-top: 250px;">
				<br><br><br><br><br><br>
			<p style="background: #f0f0f0;"><b>ATENÇÃO:</b> TODO E QUALQUER DEPÓSITO DEVERÁ SER IDENTIFICADO. ENVIAR COMPROVANTE POR E-MAIL OU FAX AO VENDEDOR RESPONSÁVEL.</p>	

			<p><b>PRAZO DE ENTREGA:</b> <?php echo mb_strtoupper($entrega['descricao']);?>
			</p>			
			<p><b>VALIDADE DO PEDIDO:</b> 5 DIAS A PARTIR DA DATA DE EMISSÃO.</p>
			<p><b>FRETE:</b> <?php echo strtoupper($empresa[0]['frete']); ?></p>
			<p><b>PADRÃO DE PINTURA:</b> <?php echo ($empresa[0]['pintura'] == 'BANDEIRA_BRANCA' || $empresa[0]['pintura'] == 'bandeira_branca') ? 'BANDEIRA BRANCA '. mb_strtoupper($empresa[0]['pintura_descricao']) : mb_strtoupper($empresa[0]['pintura']); ?> </p>
			<p><b>IDENTIDADE VISUAL: </b> <?=$identidade_visual?>
			<p><b>CADASTRO DO CLIENTE:</b> ENVIAR COM ESTE PEDIDO: CÓPIA DO CONTRATO SOCIAL COM AS ÚLTIMAS ALTERAÇÕES; FICHA CADASTRAL COMPLETA; REFERÊNCIAS COMERCIAIS E BANCÁRIAS COM TELEFONES E CONTATOS.
			</p>							
			<p><b>PAGAMENTO VIA FINAME:</b> SE O CLIENTE UTILIZAR RECURSOS PROVENIENTES DO BANCO NACIONAL DE DESEVOLVIMENTO ECONÔMICO E SOCIAL - BNDES PARA A AQUISIÇÃO DE BOMBA(S), A LIBERAÇÃO DA VERBA PELO BANCO
INTERMEDIADOR DA OPERAÇÃO SERÁ POR CONTA E RISCO DO CLIENTE. CASO O BANCO INTERMEDIADOR, POR QUALQUER RAZÃO, NÃO LIBERE OS REFERIDOS RECURSOS DENTRO DO PRAZO DE 30 DIAS 
CONTADOS DA DATA DE ENTREGA DA(S) BOMBA(S), FICA A WERTCO LTDA.,DESDE JÁ, AUTORIZADA A EMITIR NOTA FISCAL FATURA CONTRA O CLIENTE, COM VENCIMENTO À VISTA, PODENDO PROCEDER
DE IMEDIATO A SUA COBRANÇA. OUTROSSIM, EM CASO DE ATRASO, SERÁ ACRESCIDO AO VALOR DO DÉBITO JUROS DE MORA DE 1% AO MÊS, ALÉM DE MULTA CONTRATUAL ESTIPULADA ENTRE AS PARTES 
EM 10%, DANDO DIREITO À WERTCO LTDA. DE RESCINDIR O PRESENTE INSTRUMENTO, SEM PREJUÍZO DE COBRANÇA, NA FORMA DA LEI, DAS OBRIGAÇÕES VENCIDAS E NÃO PAGAS.
			</p>	
			<p><b>VENDA À PRAZO E RESRVA DE DOMÍNIO:</b> CASO A PROPOSTA DE COMPRA SEJA NA MODALIDADE DE FINANCIAMENTO DIRETO, COM PARCELAMENTO, O CLIENTE DECLARA ESTAR CIENTE DE QUE A COMPRA E VENDA SERÁ ACRESCIDA DE CLÁUSULA DE RESERVA DE DOMÍNIO, DE FORMA QUE A PROPRIEDADE DOS BENS SERÃO DA VENDEDORA ATÉ O PAGAMENTO INTEGRAL DO DÉBITO,  SENDO-LHE CONFERIDA APENAS A POSSE PROVISÓRIA SOBRE OS BENS, CABENDO-LHE A GUARDA E CONSERVAÇÃO DOS MESMOS. O CLIENTE TAMBÉM RESTA CIENTE DE QUE O NÃO PAGAMENTO DAS PARCELAS IMPORTARÁ NA BUSCA E APREENSÃO DOS BENS, SEM PREJUÍZO DA COBRANÇA DE PERDAS E DANOS EVENTUALMENTE INCORRIDAS, CASO NÃO PURGUE A MORA EM 15 DIAS APÓS NOTIFICADO.
			</p>	
			<p><b>CONTRATO DE ADESÃO:</b> COM A ASSINATURA DO CLIENTE NO PRESENTE PEDIDO E A RÚBRICA NO CONTRATO DE ADESÃO DE VENDA À PRAZO COM RESERVA DE DOMÍNIO, O CLIENTE DECLARA TER PLENO CONHECIMENTO ÀS SUAS CLÁUSULAS, TENDO ADERIDO ÀS DISPOSIÇÕES CONSTANTES DO DOCUMENTO.
			</p>
			<p> <b>CANCELAMENTO DO PEDIDO:</b> O CLIENTE, EM CONFORMIDADE COM A LEI VIGENTE, PODERÁ CANCELAR ESTE PEDIDO, SEM QUALQUER ÔNUS, EM ATÉ 7 DIAS A CONTAR DA DATA DA CONFIRMAÇÃO. APÓS ESTE PRAZO, O CLIENTE
DEVERÁ PAGAR À WERTCO LTDA. O EQUIVALENTE A 10% DO VALOR DESTE PEDIDO, A TÍTULO DE RESSARCIMENTO PELOS CUSTOS INCORRIDOS COM O INÍCIO DA PRODUÇÃO DA(S) BOMBA(S). 
			</p>
			<p><b>DISPONIBILIDADE DA(S) BOMBA(S):</b> EM CASO DE FRETE POR CONTA DO CLIENTE, O EQUIPAMENTO DEVERÁ SER RETIRADO EM ATÉ 05 DIAS DO COMUNICADO DE DISPONIBILIDADE. DECORRIDO ESTE PRAZO, O CLIENTE ARCARÁ COM
DESPESAS DE GUARDA E SEGURANÇA DO EQUIPAMENTO, NO VALOR DE R$ 350,00/DIA POR BOMBA ALTA E R$ 200,00/DIA POR BOMBA BAIXA, SOB PENA DE RETENÇÃO DA(S) BOMBAS(S).</p>
			<p><b>INSTALAÇÃO DA(S) BOMBA(S):</b> ATENÇÃO: ANTES DE REALIZAR PROCEDIMENTOS DE INSTALAÇÃO LEIA ATENTAMENTE AS INFORMAÇÕES MENCIONADAS NO MANUAL DO PROPRIETÁRIO, QUE ESTÁ DISPONÍVEL PARA DOWNLOAD EM WWW.WERTCO.COM.BR. <b>A WERTCO NÃO SE RESPONSABILIZA POR BARULHOS CAUSADOS POR PROBLEMAS NA LINHA DE COMBUSTÍVEL, COMO VAZAMENTOS, DISTÂNCIA E ALTURA MANOMÉTRICA DO TANQUE FORA DE PADRÃO, OU QUALQUER OUTRA DEFINIÇÃO ESPECIFICADA NO MANUAL DE INSTALAÇÃO.</b>
			</p>						
			<p>
				<b>GARANTIA DE FABRICAÇÃO:</b> 24 MESES A CONTAR DA DATA DE EMISSÃO DA NOTA FISCAL.<br/>
O EQUIPAMENTO DEVERÁ SER INSTALADA DE ACORDO COM O MANUAL DO PROPRIETÁRIO E/OU NORMAS DA ABNT, E DENTRO DO PRAZO MÁXIMO DE 6 MESES, 
CONTADOS A PARTIR DA DATA DE EMISSÃO DA NOTA FISCAL, SOB PENA DE PERDA DA GARANTIA. DEMAIS INFORMAÇÕES, VIDE "TERMO DE GARANTIA DO PRODUTO".
			
			</p>
		</div>
			<div style="PAGE-BREAK-AFTER: always">
				<p style="text-align: right;border-radius: 10px 10px 10px 10px solid #000;">2</p>
			</div> 
			<div class="cabecalho">			
				<table border="0">
					<tr>
						<td style="margin: 5px auto;">
							<img STYLE="width: 150px;" src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" >
						</td>
						<td style="text-align: right; font-size: 8px;margin-top: -10px;">
							<p > WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
											CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
											Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
											07400-230 - Arujá - SP<br>
											Tel: +55 11 3900-2565 | www.wertco.com.br<br>
							</p>
						</td>			
					</tr>
				</table>
			</div>		
		
		<div style="font-size: 10px; margin-top: 150px !important;">	
			<br><br><br><br><br><br>
		<h3 style="background: #f0f0f0; text-align: center; margin-top: 20px !important;"><b>CONDIÇÕES GERAIS</b></h3>	
		<p>
			<b>1.</b> As condições gerais de venda e fornecimento dos produtos fabricados pela Wertco Ltda. a seus clientes são reguladas pelas disposições abaixo especificadas. Qualquer inclusão ou alteração destes termos e condições deverá ser previamente acordada entre as partes, por escrito, sob pena de ineficácia e invalidade.<br>
		<b>2.</b> Para efeito deste instrumento: FABRICANTE: Wertco Ind. Com. e Serviços de Manut. em Bombas Ltda.; PRODUTO: bomba de abastecimento de combustível fabricada e comercializada pela Wertco Ltda.;<br>
		<b>CLIENTE:</b> pessoa física ou jurídica,  que efetua a compra do produto, qualificada na Nota Fiscal de Faturamento emitida pela Wertco Ltda.; SEW: Serviço Especializado Wertco Ltda.<br>

		<b>3.RESPONSABILIDADES:</b>
		<b>A.</b> A Fabricante é responsável pela adequada produção e embalagem do produto, que será colocado à disposição do Cliente nas instalações daquela, para carregamento e transporte,exceto se as partes acordarem de forma comercializada pela Wertco Ltda.;  diversa, por escrito, conforme disposto no item "1" acima. <br>
		<b>B.</b> Após a entrega do produto ao Cliente, a Fabricante não se responsabiliza pela perda, furto ou roubo do mesmo, ou por qualquer dano decorrente de seu transporte, armazenamento ou eventos não cobertos pela garantia.<br>		
		<b>C.</b> A responsabilidade da Fabricante com a guarda do produto estará encerrada assim que este for entregue ao Cliente, conforme disposto no item "A" acima.<br>
		<b>D.</b> Após o aceite deste Pedido de Compra, nenhuma modificação em seus termos poderá ser realizada sem a concordância formal da Fabricante. A aprovação da modificação solicitada, entretanto, estará sujeita a reajuste de preço, alteração do prazo de entrega e adequação de outras condições afetadas.<br>
		<b>E.</b> A Fabricante em hipótese alguma aceitará devolução do produto, exceto quando, a seu exclusivo critério, julgar conveniente.<br>
		<b>F.</b> Na hipótese de cancelamento deste Pedido de Compra, fica a exclusivo critério da Fabricante avaliar a viabilidade e as condições para sua eventual aceitação.<br>
		<b>G.</b> No caso de inadimplemento, incidirá multa de 10% sobre as parcelas vincendas e juros de mora de 1% ao mês, além de honorários advocatícios de 20% em caso de cobrança judicial e dos encargos previstos no contrato de compra e venda com reserva de domínio a ser assinado entre as partes. Caberá, também, a busca e apreensão dos mesmos caso não seja purgada a mora e encargos no prazo de 15 dias após notificação extrajudicial, além dos demais encargos pela conservação e uso do bem, conforme contrato.<br>				
		<b>4.</b> QUANTO AO "START UP" (PARTIDA INICIAL) DO PRODUTO:<br>
		<b>A.</b> A contratação dos serviços de instalação do produto é responsabilidade do Cliente, que arcará com o respectivo custo.<br>
		<b>B.</b> Após a instalação completa do produto, o Cliente entrará em contato com a Fabricante, que enviará ao local um de seus técnicos ou uma empresa autorizada dentro do prazo máximo de 48 horas, a contar da efetiva comunicação. O técnico da Fabricante ou o SEW inspecionará as condições da instalação elétrica e hidráulica e, estando em conformidade com as especificações e recomendações da Fabricante, efetuará o "start up" do produto e o treinamento do usuário.<br>
		<b>C.</b> O "start up" do produto deverá ser realizado pela Fabricante ou o SEW, que preencherá o "formulário de start up" que acompanha o produto. O referido formulário será vistado e carimbado pelo Cliente ou por seu representante legal como forma de aceitação do funcionamento inicial do produto. A não realização do "start up" pela Fabricante ou SEW acarretará perda automática da garantia do produto.<br>		
		<b>D.</b> Caso a Fabricante ou SEW verifique que as instalações não estão em conformidade com as especificações técnicas exigidas pela Fabricante, não será recomendado o "start up" do produto, sob pena de perda da garantia. Neste caso, o Cliente será orientado a corrigir as falhas encontradas, e só então deverá acionar novamente a Fabricante.<br>
		<b>E.</b> Caso o Cliente acione a Fabricante para efetuar o "start up" e este não possa ser efetuado em virtude de instalações elétricas/hidráulicas inadequadas, ou pela falta de combustível no(s) tanque(s), o Cliente arcará com os custos de deslocamento da Fabricante ou SEW.<br>
		<b>F.</b> O cliente resta cientificado que o “start up”  pelo técnico autorizado somente será autorizado caso esteja adimplente com suas obrigações até aquele momento, e que, caso não seja autorizado, deve entrar em contato com o setor comercial da Wertco. <br>

		</p>
		<h3 style="background: #f0f0f0; text-align: center;"><b>TERMO DE GARANTIA</b></h3>
		<p>
			<b>1. </b>As condições gerais de garantia dos produtos fabricados pela Wertco Ltda. são reguladas pelas disposições abaixo especificadas. Qualquer inclusão ou alteração destes termos e condições deverá ser previamente acordada entre as partes, por escrito, sob pena de ineficácia e invalidade.<br/><br/>
			<b>2. </b>Para efeito deste instrumento: vide o item 2 de "Condições Gerais de Fornecimento".<br/>
			<b>3. </b>O produto tem garantia contra defeitos de projeto e fabricação pelo período negociado entre as partes, constante na página 2 deste Pedido de Compra.<br/>
			<b>4. </b>O produto deverá ser instalado dentro do prazo máximo de 6 meses, contados a partir da data de emissão da respectiva Nota Fiscal de Faturamento, sob pena de perda da garantia. <br/>
			<b>5. </b>A garantia compreende (I) a substituição de peças para reparo de defeitos, por meio do fornecimento de peças sobressalentes; (II) a substituição do produto quando seu reparo não for possível. <br/>
			 Em ambos os casos, as falhas ou defeitos deverão ser constatados pela Fabricante ou SEW.<br/>
			<b>6. </b>As garantias aqui estabelecidas ficam automaticamente invalidadas nas seguintes situações: <br/>
			<b>•</b> Se o "start up" (partida inicial) do produto não for realizado pela Fabricante ou pelo SEW.<br/>
			<b>•</b> Se os danos sofridos pelo produto e seus acessórios ocorrerem em função de sua utilização inadequada, incorreta ou não autorizada, em desacordo com as especificações fixadas pela Fabricante;<br/>
			<div style="PAGE-BREAK-AFTER: always">
				<p style="text-align: right;border-radius: 10px 10px 10px 10px solid #000;">3</p>
			</div>			 
			<div class="cabecalho">			
				<table border="0">
					<tr>
						<td style="margin: 5px auto;">
							<img STYLE="width: 150px;" src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" >
						</td>
						<td style="text-align: right; font-size: 8px;margin-top: -10px;">
							<p > WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
											CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
											Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
											07400-230 - Arujá - SP<br>
											Tel: +55 11 3900-2565 | www.wertco.com.br<br>
							</p>
						</td>			
					</tr>
				</table>
			</div>		
		
		<div style="font-size: 10px; margin-top: 150px !important;">	
			<br><br><br><br>
		<p style="margin-top: 40px !important;">			
			<b>•</b> Se as avarias sofridas pelo produto e seus acessórios ocorrerem em consequência de sua utilização para finalidades diversas contrárias às especificadas pela Fabricante ou forem causadas por resíduos sólidos (sujeira)  bombeados dos tanques ou tubulações;<br/>			
			<b>•</b> Se o produto tiver sofrido danos em função de descarga elétrica, curto-circuito e/ou variação na tensão elétrica, bem como qualquer problema elétrico proveniente de falta de proteção ou inadequação da rede elétrica do cliente.<br/> 
			<b>•</b> Se o produto tiver sofrido danos, em função de armazenamento, transporte e/ou manuseio incorretos ou inadequados, após a entrega do mesmo pela Fabricante ao Cliente, na forma especificada no documento "Condições Gerais de Fornecimento", item 3, "A";<br/>
			<b>•</b> Se o produto tiver sofrido, sem autorização prévia e por escrito da Wertco Ltda., qualquer tipo de modificação estética ou funcional;<br/>
			<b>•</b> Se existirem sinais de violação no produto, bem como se tiver sido realizada qualquer tipo de intervenção por pessoal não autorizado pela Fabricante;<br/>
			<b>•</b> Se o produto tiver sofrido danos decorrentes de atos dolosos ou culposos, praticados por terceiros ou pelo Cliente, como, por exemplo, abalroamento, quebra de vidraria (densímetros, visores, etc.) e atos de vandalismo;<br/>
			<b>•</b> Se o produto tiver sofrido avarias causadas por fenômenos naturais como, mas não limitados a, inundações, raios, vendavais, incêndios, explosões, etc.<br/>
			<b>7.</b> A garantia não cobre despesas com instalações do produto, nem de peças e acessórios sujeitos a desgaste natural, descartáveis e removíveis, como mangueira, bico, adesivos e qualquer 
			acessório opcional aplicável ao produto, como kit densímetro, breakaway e swivel (conexão giratória). A garantia também não inclui serviços de aferição, limpeza de filtros e ajuste da correia do motor.<br/>
		
			<b>8. </b>Acessórios opcionais (bico, mangueira, kit densímetro, swivel, breakaway, suporte de mangueira flexível, etc.) do produto têm garantia de 6 meses desde que o Cliente envie o 
			acessório para a Fabricante e esta constate, mediante análise, que há defeito de fabricação. Caso o Cliente opte por receber um técnico em seu estabelecimento, a visita e mão de obra da 
			Fabricante ou SEW será cobrada de acordo com as taxas de serviços vigentes.<br/>
			<b>9. </b>Quaisquer custos incorridos pela Fabricante, oriundos das situações descritas nos itens 6 e 7, serão reembolsados pelo Cliente, de acordo com as taxas de serviços aplicadas pela Fabricante na ocasião do atendimento.<br/>
			<b>10.</b> Para casos excepcionais, como, por exemplo, quando o produto destinar-se a um local não abrangido por qualquer SEW, as partes poderão negociar a exclusão total ou parcial das condições de garantia descritas  acima.<br/>
			Os termos da exclusão serão previamente discutidos e negociados entre as partes, devendo, necessariamente, constar neste Pedido de Compra e na respectiva Nota Fiscal.<br/>
			<b>11.</b> Tendo a Fabricante cumprido com todos os deveres descritos neste instrumento, esta não poderá ser responsabilizada por quaisquer danos indiretos ou lucros cessantes sofridos pelo Cliente, em virtude de falhas no produto sob garantia. <br/>

		</p>
		<p><b>ELEIÇÃO DE FORO:</b> Fica eleito o Foro da cidade de Arujá/SP para dirimir quaisquer divergências decorrentes deste instrumento, com a expressa renúncia a qualquer outro, por mais privilegiado que seja.
		</p>
		<?php if( $empresa[0]['observacao'] != '' ) {	?>
		
			<p><b>OBSERVAÇÃO:</b> <?php echo mb_strtoupper($empresa[0]['observacao']); ?></p>
		
		<?php } ?>
		</div>	
			<div style="position: fixed; bottom: 150;">
				<center>
					<table border="0" >					
						<tr>
							<td style="text-align: right; width: 40%;"><b>Aprovado:</b></td>
							<td style="text-align: left; width: 60%;">__________________________________</td>						
						</tr>
						<tr>							
							<td >&nbsp;</td>
							<td style="text-align: left;"><b style="margin-left: 40px;">Itiel L. Nascimento</b></td>						
						</tr>
						<tr>
							<td >&nbsp;</td>
							<td style="text-align: left;"><b style="margin-left: 26px;">Gestor Adm. Comercial</b></td>						
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
					</table>
				</center>

				<table border="0">					
					<tr>
						<td><b>Empresa: Wertco Ind. Com. e Serv de Manut. em Bombas Ltda.</b></td>
						<td><b>APROVAÇÃO DO PEDIDO PELO CLIENTE: <?php echo mb_strtoupper($empresa[0]['razao_social']); ?></b></td>
					</tr>
					<tr>
						<td><b>Contato: <?php echo mb_strtoupper($usuario_wertco['nome']);?></b></td>
						<td><b> "Declaro ter compreendido todas as informações mencionados neste pedido, estando de acordo."</b></td>						
					</tr>	
					<tr>
						<td><b>Telefone: <?php echo $usuario_wertco['telefone'];?></b></td>
						<td><b>Nome: <?php echo mb_strtoupper($empresa[0]['nome_cliente']);?></b></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><b>E-mail: <?php echo $usuario_wertco['email'];?></b></td>
						<td><b>CPF/RG: <?php echo $empresa[0]['cpf_cliente'];?><?php echo ($empresa[0]['cpf_cliente'] != '' && $empresa[0]['rg_cliente'] != '') ? ' / '.$empresa[0]['rg_cliente'] : '';?></b></td>	
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>					
					<tr>
						<td><b>Assinatura:</b>________________________________________</td>
						<td><b>Assinatura:</b>________________________________________</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="2"><b><?php echo ($empresa[0]['indicador'] != '' ) ? 'INDICADOR: '.mb_strtoupper($empresa[0]['indicador']) : ''; ?></b></td>
						
					</tr>
				</table>	
					<p style="text-align: right;border-radius: 10px 10px 10px 10px solid #000;">4</p>
			</div>		
	</div>

</body>
</html>