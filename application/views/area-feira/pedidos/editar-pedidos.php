<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">						
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaAdministrador/pedidos'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaAdministrador/pedidos'); ?>">Voltar</a>						
						</h3>
						<h3 class="m-portlet__head-text">&nbsp;&nbsp;  Pedido #<?php echo $dados->id;?></h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/editarPedidos');?>" method="post" enctype="multipart/form-data">
				<div class="m-portlet__body">	
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Orçamento</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="orcamento" id="orcamento" class="form-control m-input" placeholder="" required value="<?php echo '#'.$dados->orcamento_id.' | '.$dados->cliente; ?>">
								<input type="hidden" required name="orcamento_id" id="orcamento_id" class="form-control m-input" placeholder="" required value="<?php echo $dados->orcamento_id; ?>">
								<input type="hidden" required name="id" id="id" class="form-control m-input" placeholder="" required value="<?php echo $dados->id; ?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
							</div>
						</div>
						
					</div>									
					<div class="form-group m-form__group row">
						<div class="col-lg-4">
							<label>Padrão Pintura:</label>
							<div class="m-input-icon m-input-icon--right">
								<select class="form-control m-input m-input--square" name="pintura" class="pintura" id="exampleSelect1">
									<option value="" <?php echo ($dados->pintura == '') ? 'selected="selected"' : ''; ?>>
										Selecione uma bandeira
									</option>
									<option value="BR" <?php echo ($dados->pintura == 'BR') ? 'selected="selected"' : ''; ?>>
										BR
									</option>
									<option value="IPIRANGA" <?php echo ($dados->pintura =='IPIRANGA') ? 'selected="selected"' : ''; ?>>
										Ipiranga
									</option>
									<option value="SHELL" <?php echo ($dados->pintura =='SHELL') ? 'selected="selected"' : ''; ?>>
										Shell
									</option>
									<option value="ALE" <?php echo ($dados->pintura =='ALE') ? 'selected="selected"' : ''; ?>>
										Ale
									</option>
									<option value="BANDEIRA_BRANCA" <?php echo ($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA') ? 'selected="selected"' : ''; ?>>
										Bandeira Branca
									</option>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-paint"></i></span></span>
							</div>
							<img src="<?php echo base_url('pedidos/'.$dados->arquivo);?>" class="arquivo img-responsive" width="100" style="border-radius: 50%;<?php echo ($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA') ? 'display: block;' : 'display: none;'; ?> margin-top: 10px;" />

							<input type="file" name="arquivo_bb" class="form-control m-input" placeholder="insira o arquivo" id="arquivo_bb" style="<?php echo ($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA') ? 'display: block;' : 'display: none;'; ?> margin-top: 10px;" value="" />									
							<textarea name="pintura_descricao" value="" id="pintura_descricao" class="form-control m-input" placeholder="Descrição da pintura" style="<?php echo ($dados->pintura == 'bandeira_branca' || $dados->pintura == 'BANDEIRA_BRANCA') ? 'display: block;' : 'display: none;'; ?>margin-top: 10px;"><?php echo $dados->pintura_descricao; ?></textarea>
						</div>
						<div clas s="col-lg-4">														
							<label>Técnico para startup cadastrado:</label>	
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" class="tecnico" name="tecnico" id="" value="1" <?php echo ($dados->tecnico_id != 0) ? 'checked="checked"' : '';?>  />
									Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" class="tecnico" name="tecnico" id="" value="0" <?php echo ($dados->tecnico_id == 0) ? 'checked="checked"' : '';?>>
									Não
									<span></span>
								</label>								
							</div>
							<input type="text" name="tecnico_startup" class="form-control m-input" placeholder="Pesquise o técnico" id="tecnico_startup" style="<?php echo ($dados->tecnico_id == 0) ? 'display:none;' : 'display:block;';?>margin-top: 20px;" value="<?php echo $dados->tecnico; ?>" />		
							<input type="hidden" name="tecnico_id" id="tecnico_id" value="<?php echo $dados->tecnico_id?>" />						
						</div>											
						<div class="col-lg-4">														
							<label>Cliente deseja receber informações da produção do pedido:</label>	
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" name="fl_receber_info" id="" value="1" <?php echo ($dados->fl_receber_info ==1 ) ? 'checked="checked"' : ''; ?> />
									Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" name="fl_receber_info" id="" value="0"  <?php echo ($dados->fl_receber_info ==0 ) ? 'checked="checked"' : ''; ?>>
									Não
									<span></span>
								</label>								
							</div>						
						</div>
					</div>	
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>Endereço de entrega</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="endereco_entrega" id="endereco_entrega" class="form-control m-input" placeholder="" value="<?php echo $dados->endereco_entrega;?>">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>Nome do Cliente:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="nome_cliente" id="nome_cliente" value="<?php echo $dados->nome_cliente; ?>" class="form-control m-input" placeholder="" required>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>RG:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text"  name="rg_cliente" id="rg_cliente" value="<?php echo $dados->rg_cliente; ?>"  class="form-control m-input" placeholder="" >
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>CPF</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text"  name="cpf_cliente" id="cpf_cliente" value="<?php echo $dados->cpf_cliente; ?>" class="form-control m-input" placeholder=""  />
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
					</div>	
				<div class="form-group m-form__group row" >
	                <div class="m-portlet col-lg-12">
	                    <div class="m-portlet__head">
	                        <div class="m-portlet__head-caption">
	                            <div class="m-portlet__head-title">
	                                <span class="m-portlet__head-icon">
	                                    <i class="la la-thumb-tack m--font-warning"></i>
	                                </span>
	                                <h3 class="m-portlet__head-text m--font-warning">
	                                    Forma de Pagamento (informações, parcelamentos, etc...)
	                                </h3>
	                            </div>
	                        </div> 
	                    </div>
						<div class="m-portlet__body ">
							<table class="table m-table m-table--head-separator-warning">
							  	<thead>
							    	<tr>			      		
							      		<th style="width: 10%;">Porcentagem</th>
							      		<th style="width: 80%;">Descrição</th>							      		
							      		<th style="width: 10%;">
							      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add" data-toggle="m-tooltip" data-placement="top" title="Adicionar Forma de Pagamento">
												<i class="la la-plus"></i>
											</a>
										</th>				      		
							    	</tr>
							  	</thead>
							  	<tbody>	
							  		<?php $i=0; foreach($formaPagto as $pagto){ ?>
							  				<tr id='excluir-<?php echo $i;?>'>
							  					<td>
							  						<div class="input-group">
														<?php echo $pagto['porcentagem'];?> %
															
														
													</div>		
												</td>	
												<td>
													<?php echo $pagto['descricao'];?>
												</td>
												<td>													
													<i class="fa fa-minus excluir" pedido_id='<?php echo $dados->id; ?>' forma_pagto_id='<?php echo $pagto['id']; ?>' tr='excluir-<?php echo $i; ?>' style="color: red;cursor: pointer;"></i>													
												</td>
							  		<?php $i++;}	?>	
							  		 <tr id="modelo" indice='0' total_indice='0'>
							  		 	<td>
							  		 		<div class="input-group">
												<input type="text" name="porcentagem[]" min="0" max="100" maxlength="3" class="form-control m-input porcentagem" placeholder="%"  >
												<div class="input-group-append">
													<span class="input-group-text" id="basic-addon2">
														%
													</span>
												</div>
											</div>
							  		 	</td>	
							  		 	<td><input type="text"  name="descricao[]"  class="form-control m-input descricao" placeholder="Descrição"></td></td>
    						  		 	<td>
			                            	<i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
			                            </td>
							  		 </tr>
							  	</tbody>
							  </table>
						</div>	
	                </div>                                               
                </div> 
		        <div class="form-group m-form__group row produtos" >
	                <div class="m-portlet col-lg-12">
	                    <div class="m-portlet__head">
	                        <div class="m-portlet__head-caption">
	                            <div class="m-portlet__head-title">
	                                <span class="m-portlet__head-icon">
	                                    <i class="la la-thumb-tack m--font-success"></i>
	                                </span>
	                                <h3 class="m-portlet__head-text m--font-success">
	                                    Produtos
	                                </h3>
	                            </div>
	                        </div> 
	                    </div>
						<div class="m-portlet__body " id="produtos_cadastrar">
							<?php foreach ($produtos as $produto) {
								if( $produto['tipo_produto_id'] != 4 ){
									$tipo = 'text';
									$value=$produto['produtos'];									
								}else{
									$tipo  = 'hidden';									
									$value = 'OPCIONAL';
								}
								
								$html = '<div class="form-group m-form__group row novos_produtos">';
								$html .= '	<div class="input-group m-input-group m-input-group--solid">';
								$html .= '			<div class="input-group-prepend">';
								$html .= '				<span class="input-group-text" id="basic-addon1">';
								$html .= $produto['codigo'].' - '.$produto['modelo'].' - '.$produto['descricao'];
								$html .= '				</span>';						
								$html .= '			</div>';
								$html .= '			<input type="'.$tipo.'" class="form-control m-input" name="produtos[]" placeholder="Produtos" value="'.$value.'" style="background: #fff;" required>';
								$html .= '			<input type="hidden" class="form-control m-input" name="produto_id[]" value="'.$produto['produto_id'].'" placeholder="Produtos" style="background: #fff;">';
								$html .= '			<input type="hidden" class="form-control m-input" name="qtd['.$produto['produto_id'].']" value="'.$produto['qtd'].'" placeholder="Produtos" style="background: #fff;">';
								$html .= '			<input type="hidden" class="form-control m-input" name="tipo_produto[]" value="'.$produto['tipo_produto_id'].'" placeholder="Produtos" style="background: #fff;">';
								$html .= '			<input type="hidden" class="form-control m-input" name="valor[]" value="'.$produto['valor'].'" placeholder="valor_produto" style="background: #fff;">';
								$html .= '		</div>';
								$html .= '	</div>';
							
							for($j=0;$j<$produto['qtd'];$j++)
							{
								echo $html;
							}
						} ?>
						</div>	
	                </div>                                               
                </div>						
	            <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>
			</div>
			</form>
			<!--end::Form-->
		</div>
</div>	