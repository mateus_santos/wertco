<div class="m-content">
	<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('areaFeira/pedidos'); ?>"><i class=""><-</i></a>
						</span>
						<h3 class="m-portlet__head-text">	
							<a href="<?php echo base_url('areaFeira/pedidos'); ?>">Voltar</a>						
						</h3>
					</div>
				</div> 
			</div> 
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('areaAdministrador/cadastraPedidos');?>" method="post" enctype="multipart/form-data">
				<div class="m-portlet__body" style="padding: 10px;">	
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Orçamento</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="orcamento" 			id="orcamento" class="form-control m-input" placeholder="" >
								<input type="hidden" required name="orcamento_id" 		id="orcamento_id" class="form-control m-input" placeholder="" >
								<input type="hidden" required name="empresa_id" 		id="empresa_id" class="form-control m-input" placeholder="" >
								<input type="hidden" required name="frete_id" 			id="frete_id" class="form-control m-input" placeholder="" >
								<input type="hidden" required name="valor_total" 		id="valor_total" class="form-control m-input" placeholder="" >
								<input type="hidden" required name="valor_total_stx" 	id="valor_total_stx" class="form-control m-input" placeholder="" >
								<input type="hidden" name="testeira" id="v_testeira" />
								<input type="hidden" name="painel_lateral" id="v_painel_lateral" />
								<input type="hidden" name="painel_frontal" id="v_painel_frontal" />
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-cart"></i></span></span>
							</div>
						</div>						
					</div>
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>Padrão Pintura:</label>
							<div class="m-input-icon m-input-icon--right">
								<select class="form-control m-input m-input--square" name="pintura" class="pintura" id="pintura">
									<option value="">
										Selecione uma bandeira
									</option>
									<option value="BR">
										BR
									</option>
									<option value="IPIRANGA">
										Ipiranga
									</option>
									<option value="SHELL">
										Shell
									</option>
									<option value="ALE">
										Ale
									</option>
									<option value="TOTAL">
										Total
									</option>
									<option value="WERTCO">
										Wertco 
									</option>
									<option value="BANDEIRA_BRANCA">
										Bandeira Branca
									</option>
									<option value="TABOCAO">
										Tabocão
									</option>
								</select>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-paint"></i></span></span>
							</div>													
						</div>
						<div class="col-lg-3">
							<label>Motor:</label>
							<div class="m-input-icon m-input-icon--right">
								<select class="form-control m-input m-input--square" name="motor" class="motor" id="exampleSelect2">
									<option value="220V">
										220V
									</option>
									<option value="380V">
										380V
									</option>									
								</select>
							</div>
						</div>
						<div class="col-lg-3">														
							<label>Técnico para startup cadastrado:</label>	
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" class="tecnico" name="tecnico" id="" value="1"  />
									Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" class="tecnico" name="tecnico" id="" value="0" checked="checked">
									Não
									<span></span>
								</label>								
							</div>
							<input type="text" name="tecnico_startup" class="form-control m-input" placeholder="Pesquise o técnico" id="tecnico_startup" style="display: none;margin-top: 20px;" />		
							<input type="hidden" name="tecnico_id" id="tecnico_id"  />						
						</div>											
						<div class="col-lg-3">														
							<label>Cliente deseja receber informações da produção do pedido:</label>	
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" name="fl_receber_info" id="" value="1"  />
									Sim
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" name="fl_receber_info" id="" value="0" checked="checked">
									Não
									<span></span>
								</label>								
							</div>						
						</div>
					</div>	
					<div class="form-group m-form__group row" id="arquivo_bb" style="display: none;">
						<div class="col-lg-8"  style="border-top: 1px solid #ffcc00;border-left: 1px solid #ffcc00; border-bottom: 1px solid #ffcc00;">
							<div class="m-radio-inline">
								<label class="m-radio">
									<input type="radio" class="fl_identidade_visual" name="fl_identidade_visual" id="" value="A" checked="checked">
									Identidade visual será inserida posteriormente
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" class="fl_identidade_visual" name="fl_identidade_visual" id="" value="S"  />
									Possui identidade visual
									<span></span>
								</label>
								<label class="m-radio">
									<input type="radio" class="fl_identidade_visual" name="fl_identidade_visual" id="" value="N"  />
									Não possui identidade visual
									<span></span>
								</label>																
							</div>
						</div>
						<div class="col-lg-4" style="border-top: 1px solid #ffcc00;border-right: 1px solid #ffcc00; border-bottom: 1px solid #ffcc00;">
							<div class="m-input-icon m-input-icon--right">
								<input type="file" name="arquivo_bb" class="form-control m-input arquivo_bb" placeholder="insira o arquivo"  />		
								<textarea name="pintura_descricao" value="" id="pintura_descricao" class="form-control m-input" placeholder="Descrição da pintura" ></textarea>	
							</div>
						</div>
					</div>	
					<div class="form-group m-form__group row">
						<div class="col-lg-3">
							<label>Endereço de entrega:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="endereco_entrega" id="endereco_entrega" class="form-control m-input" placeholder="" required>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>Nome do Cliente:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" required name="nome_cliente" value="" id="nome_cliente" class="form-control m-input" placeholder="" required>
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>RG:</label>
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="rg_cliente" id="rg_cliente" class="form-control m-input" placeholder="">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
						<div class="col-lg-3">
							<label>CPF</label> 
							<div class="m-input-icon m-input-icon--right">
								<input type="text" name="cpf_cliente" id="cpf_cliente" class="form-control m-input" placeholder="">
								<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-user"></i></span></span>
							</div>
						</div>
					</div>	
					<div class="form-group m-form__group row">
						<div class="col-lg-12">
							<label>Observação</label>
							<textarea class="form-control" name="observacao" id="observacao"></textarea>
						</div>
					</div>					

							
	           	 <div class="form-group m-form__group row">
	                <div class="m-portlet col-lg-12">
	                    <div class="m-portlet__head">
	                        <div class="m-portlet__head-caption">
	                            <div class="m-portlet__head-title">
	                                <span class="m-portlet__head-icon">
	                                    <i class="la la-thumb-tack m--font-warning"></i>
	                                </span>
	                                <h3 class="m-portlet__head-text m--font-warning">
	                                    Forma de Pagamento (informações, parcelamentos, etc...)
	                                </h3>
	                            </div>
	                        </div> 
	                    </div>
						<div class="m-portlet__body ">
							<input type="hidden" name="forma_pagto_orc" id="forma_pagto_orc" />
							<input type="hidden" name="edicao_forma_pgto" id="edicao_forma_pgto" value="0" />
							<table class="table m-table m-table--head-separator-warning">
							  	<thead>
							    	<tr>			      		
							      		<th style="width: 10%;">Porcentagem</th>
							      		<th style="width: 80%;">Descrição</th>							      		
							      		<th style="width: 10%;">
							      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add" data-toggle="m-tooltip" data-placement="top" title="Adicionar Forma de Pagamento">
												<i class="la la-plus"></i>
											</a>
										</th>				      		
							    	</tr>
							  	</thead>
							  	<tbody>
							  		 <tr id="modelo" indice='0' total_indice='0'>
							  		 	<td>
							  		 		<div class="input-group">
												<input type="text" name="porcentagem[]" min="0" max="100" maxlength="3" class="form-control m-input porcentagem" placeholder="%" >
											</div>
							  		 	</td>	
							  		 	<td>
							  		 		<input type="text" required name="descricao[]"  class="form-control m-input descricao forma_pagto" placeholder="Descrição" required>
							  		 	</td>
    						  		 	<td>
			                            	<i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
			                            </td>
							  		 </tr>
							  	</tbody>
							  </table>
						</div>	
	                </div>                                               
                </div>
		        <div class="form-group m-form__group row produtos" style="display: none;">
	                <div class="m-portlet col-lg-12">
	                    <div class="m-portlet__head">
	                        <div class="m-portlet__head-caption">
	                            <div class="m-portlet__head-title">
	                                <span class="m-portlet__head-icon">
	                                    <i class="la la-thumb-tack m--font-success"></i>
	                                </span>
	                                <h3 class="m-portlet__head-text m--font-success">
	                                    Produtos
	                                </h3>
	                            </div>
	                        </div> 
	                    </div>
						<div class="m-portlet__body " id="produtos_cadastrar">
						</div>	
	                </div>                                               
                </div> 
                <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-6">
								<button type="submit" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
								<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
							</div>							
						</div>
					</div>
				</div>      	
			</div>
			</form>
			<!--end::Form-->
		</div>
</div>
<div class="modal fade" id="m_pedido_pintura" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">		
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title fotos_pedido_titulo" >Padrão de pintura</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
				<div class="row" style="width: 422px;height: 591px;background: url(<?=base_url('bootstrap/img/silhuetaBomba.png')?>) no-repeat;margin: 0 auto;">
					<div id="p_testeira" style="margin: 0 27px;">
						<select id="testeira" class="form-control testeira_frontal" placeholder="Cor Testeira">
						</select>
						
					</div>
					<div id="p_lateral" style="margin-left: 295px;margin-top: 324px;">
						<select id="painel_lateral" class="form-control" >
						</select>							

					</div>
					<div id="p_frontal" style="margin: 0 27px;">
						<select id="painel_frontal" class="form-control testeira_frontal">
						</select>
						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Confirmar</button>						
			</div>
		</div>		
	</div>		
</div>
