<style type="text/css">
	#html_table tbody > tr > td{
		text-align: center;
	}
	#html_table_processing{
		position: absolute;
		top: 0;
		font-size: 21px;
		color: #000;
		font-weight: 400;
		text-align: center;
		width: 98%;
		background: #ffcc0075;
		height: 100%;
		padding-top: 18%;
	}
	
</style>
<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Orçamentos
					</h3>
					<a  class="novo_orcamento" style="color: #ffcc00; font-weight: bold;" >
						<i class="" style="font-size: 38px;margin-top: 14px;margin-left: 10px;">+</i>
					</a>
				</div>				
			</div>
			<div class="m-portlet__head-caption cadastrar_orcamento">													
				
			</div>
			<div class="m-list__content">
                <div class="m-list-badge" style="margin-top: 25px;">                				
				</div>
			</div>					
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">			
			<div class="form-group m-form__group row">	
				<div class="col-lg-6" id="campo_ie">
					<label class="">Pesquisar Cliente:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" name="pesquisa" id="pesquisa" value="" class="form-control"/>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class=""></i></span></span>
						<button id="enviar">Pesquisar</button>
					</div>	
				</div>
			</div>
			<!--begin: Datatable -->
			<div class="form-group m-form__group row" id="mostra-orcamentos">	

			</div>
			<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group status_orcamento_" >
						<label for="exampleSelect1">Situação do Orçamento</label>
						<select class="form-control m-input m-input--air" id="status_orcamento">
							<option value="">Selecione o status do orçamento</option>						
							<option value="1">Aberto</option>
							<option value="5">Orçamento entregue</option>
							<option value="6">Em negociação</option>
							<option value="2">Fechado</option>							
							<option value="10">Perdido para Wayne</option>
							<option value="11">Perdido para Gilbarco</option>
							<option value="3">Perdido Outros</option>
							<option value="4">Cancelado</option>
							<option value="9">Em Testes</option>

						</select>
					</div>
					<div class="form-group m-form__group aprova_indicacao" style="display: none;" >
						<label for="exampleSelect1">Aprovar esta Indicação?</label>
						<div class="m-radio-inline">
							<label class="m-radio">
								<input type="radio" name="aprovar_indicacao" class="aprova_inidicacao" value="1" checked="checked" />
								Sim
								<span></span>
							</label>
							<label class="m-radio">
								<input type="radio" name="aprovar_indicacao" class="aprova_inidicacao" value="0" />
								Não
								<span></span>
							</label>							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" class="btn btn-primary" orcamento_id="" indicacao="0">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaFeira/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>
		<?php if ($this->session->flashdata('exclusao') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Orçamento excluído com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaAdministrador/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['exclusao']); } ?>