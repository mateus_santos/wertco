<div class="m-content">	
	<!-- begin Portlet -->
	<div class="m-portlet m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_5">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-diagram" style="color: #464e3f;" ></i>
					</span>
					<h3 class="m-portlet__head-text" style="color: #000;">
						Relatório de Orçamentos
					</h3>
					
				</div>

			</div>
			<div class="m-portlet__head-tools"></div>
		</div>				
		<div class="m-portlet__body" >		
			<form class="" action="<?php echo base_url('areaAdministrador/gerarRelatorioOrcamentos');?>" method="post" target="_blank"> 		
			<div class="form-group m-form__group row">				
				<div class="col-lg-4">
					<label class="">Status:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="status_id" placeholder="Status Chamado" id="status_id" >
                        	<option value="">SELECIONE UM STATUS</option>
                        <?php 	foreach($status as $sts ){	?>       
                        	<option value="<?php echo $sts['id']; ?>"><?php echo mb_strtoupper($sts['descricao']); ?></option>
                        <?php } ?>
                       	</select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map-signs"></i></span></span>
					</div>							
				</div>					
				<div class="col-lg-4">
					<label>Período:</label>
					<div class="m-input-icon m-input-icon--right">
						<input type="text" 	 name="dt_ini" 	id="dt_ini"	class="form-control m-input datepicker" placeholder="data inicial" style="width: 49%;float: left;"  /> 
						<input type="text" 	 name="dt_fim" 	id="dt_fim"	class="form-control m-input datepicker" placeholder="data final" style="width: 49%;float: right;" />
					</div>							
				</div>
				<div class="col-lg-4">
					<label>Região:</label>
					<div class="m-input-icon m-input-icon--right">
						<select  class="form-control"  maxlength="250" name="regiao" placeholder="" id="regiao" >
							<option value="">SELECIONE UMA REGIÃO</option>
							<option value="NORTE">NORTE</option>
							<option value="NORDESTE">NORDESTE</option>
							<option value="CENTRO-OESTE">CENTRO-OESTE</option>
							<option value="SUDESTE">SUDESTE</option>
							<option value="SUL">SUL</option>
						</select>
						<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-map"></i></span></span>
					</div>
				</div>
			</div>			
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="submit" name="salvar" id="enviar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase">Enviar</button>
							<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom">Limpar</button>
						</div>							
					</div>
				</div>
			</div>
			</form>	
		</div>						
	</div>
	<!--end::Portlet-->
	<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi" id="rel_chamados" style="display: none;">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="flaticon-statistics"></i>
					</span>
					<h3 class="m-portlet__head-text">						
					</h3>
					<h2 class="m-portlet__head-label m-portlet__head-label--danger">
						<span>
							Chamados
						</span>
					</h2>
				</div>
			</div>
			<div class="m-portlet__head-tools">				
			</div>
		</div>
		<div id="relatorio" class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			<!--begin: Datatable -->			
		</div>
	</div>

	
	<!--end::Portlet-->	
</div>	
