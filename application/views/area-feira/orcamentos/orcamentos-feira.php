<style type="text/css">
	#html_table tbody > tr > td{
		text-align: center;
	}
	#html_table_processing{
		position: absolute;
		top: 0;
		font-size: 21px;
		color: #000;
		font-weight: 400;
		text-align: center;
		width: 98%;
		background: #ffcc0075;
		height: 100%;
		padding-top: 18%;
	}
	
</style>
<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Orçamentos
					</h3>
					<a  class="novo_orcamento" style="color: #ffcc00; font-weight: bold;" >
						<i class="" style="font-size: 38px;margin-top: 14px;margin-left: 10px;">+</i>
					</a>
				</div>				
			</div>
			<div class="m-portlet__head-caption cadastrar_orcamento">													
				
			</div>
			<div class="m-list__content">
                <div class="m-list-badge" style="margin-top: 25px;">                				
				</div>
			</div>					
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">			
			<input type="hidden" name="pesquisa_ativa" id="pesquisa_ativa" value="<?php echo urldecode($pesquisa);?>" />
			<!--begin: Datatable -->
			<table class="table table-striped table-responsive" id="" width="100%">
				<thead class="thead-dark">
					<tr>
						<th style="text-align: center;">Número</th>
						<th style="text-align: center;">Solicitante</th>
						<th style="text-align: center;">Emissão</th>											
						<th style="text-align: center;">Status</th>																		
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				<tbody>
					
					<?php foreach($dados as $dado){
						if( $dado->status == "aberto" ){
							$status = "btn m-btn--pill m-btn--air btn-secondary";
						}elseif( $dado->status == "fechado" ){
							$status = "btn m-btn--pill m-btn--air btn-success";
						}elseif( $dado->status == "perdido outros" || $dado->status == "perdido para wayne" || $dado->status == "perdido para gilbarco" ){
							$status = "btn m-btn--pill m-btn--air btn-danger";							
						}elseif( $dado->status == "cancelado" ){
							$status = "btn m-btn--pill m-btn--air btn-warning";
						}elseif( $dado->status == "orçamento entregue" ){
							$status = "btn m-btn--pill m-btn--air btn-info";
						}else{
							$status = "btn m-btn--pill m-btn--air btn-primary";
						}

						?>
						<tr>
							<td style="width: 5%;text-align: center;"><?php echo $dado->orcamento_id; ?></td>
							<td style="width: 30%;text-align: center;"> <?php echo $dado->cnpj." | ".$dado->razao_social; ?></td>							
							<td style="width: 10%;text-align: center;"><?php echo date('d/m/Y H:i:s', strtotime($dado->emissao)).'- '.$dado->criado_por; ?></td>	
							<td style="width: 15%;text-align: center; text-transform: capitalize;">
								<button class="status <?php echo $status; ?>" title="Status do orçamento" onclick="status(<?php echo $dado->orcamento_id; ?>);">
									<?php echo ucfirst($dado->status); ?>
								</button>
							</td>
							<td data-field="Actions" class="m-datatable__cell " style="width:5%;">
								<span style="overflow: visible; width: 110px;" class="">								
									 <a class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air visualizar_orcamento" onclick="visualizar_orcamento('<?php echo $dado->orcamento_id; ?>')" title="Visualizar" orcamento_id="'<?php echo $dado->orcamento_id; ?>'" target="_blank" >
                                     	<i class="visualizar" style="font-size: 30px;font-style: normal;">✍</i>
                                     </a> 
                                     <a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air geraPdf" orcamento_id="<?php echo $dado->orcamento_id; ?>" status_id='<?php echo $dado->status_orcamento_id; ?>' data-toggle="modal" data-target="#m_modal_10"  title="Gerar PDF do Orçamento" contato="<?php echo $dado->contato_posto;?>">
										<i class="" style="font-size: 20px;font-style: normal;">📠</i>
									</a>										
								</span>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group status_orcamento_" >
						<label for="exampleSelect1">Situação do Orçamento</label>
						<select class="form-control m-input m-input--air" id="status_orcamento">
							<option value="">Selecione o status do orçamento</option>						
							<option value="1">Aberto</option>
							<option value="5">Orçamento entregue</option>
							<option value="6">Em negociação</option>
							<option value="2">Fechado</option>							
							<option value="10">Perdido para Wayne</option>
							<option value="11">Perdido para Gilbarco</option>
							<option value="3">Perdido Outros</option>
							<option value="4">Cancelado</option>
							<option value="9">Em Testes</option>

						</select>
					</div>
					<div class="form-group m-form__group aprova_indicacao" style="display: none;" >
						<label for="exampleSelect1">Aprovar esta Indicação?</label>
						<div class="m-radio-inline">
							<label class="m-radio">
								<input type="radio" name="aprovar_indicacao" class="aprova_inidicacao" value="1" checked="checked" />
								Sim
								<span></span>
							</label>
							<label class="m-radio">
								<input type="radio" name="aprovar_indicacao" class="aprova_inidicacao" value="0" />
								Não
								<span></span>
							</label>							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" class="btn btn-primary" orcamento_id="" indicacao="0">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaFeira/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>
		<?php if ($this->session->flashdata('exclusao') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Orçamento excluído com sucesso!',
				type: "success"
			}).then(function() {
		      	//window.location = base_url+'AreaAdministrador/orcamentos';
		   	}); 
		</script>	
		<?php unset($_SESSION['exclusao']); } ?>