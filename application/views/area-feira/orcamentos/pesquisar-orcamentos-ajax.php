<table class="table table-striped table-responsive" id="" width="100%" >
	<thead class="thead-dark">
		<tr>
			<th style="text-align: center;">Número</th>
			<th style="text-align: center;">Solicitante</th>
			<th style="text-align: center;">Emissão</th>											
			<th style="text-align: center;">Status</th>																		
			<th style="text-align: center;">Ações</th>
		</tr>
	</thead>
	<tbody id="mostra-orcamentos">		
		<tr>
			<td style="width: 5%;text-align: center;"><?php echo $dados->id; ?></td>
			<td style="width: 30%;text-align: center;"> <?php echo $dados->cliente; ?></td>						
			<td style="width: 10%;text-align: center;"><?php echo date('d/m/Y H:i:s', strtotime($dados->emissao)).'- '.$dados->criado_por; ?></td>	
			<td style="width: 15%;text-align: center; text-transform: capitalize;">
				<button class="status " title="Status do orçamento" >
					<?php echo ucfirst($dados->status); ?>
				</button>
			</td>
			<td data-field="Actions" class="m-datatable__cell " style="width:5%;">
				<span style="overflow: visible; width: 110px;" class="">								
					 <a class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air visualizar_orcamento" onclick="visualizar_orcamento('<?php echo $dados->id; ?>')" title="Visualizar" id="'<?php echo $dados->id; ?>'" target="_blank" >
                     	<i class="visualizar" style="font-size: 30px;font-style: normal;">✍</i>
                     </a> 
                     <a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air geraPdf" id="<?php echo $dados->id; ?>" status_id='<?php echo $dados->status_id; ?>' data-toggle="modal" data-target="#m_modal_10"  title="Gerar PDF do Orçamento" contato="<?php echo $dados->contato_posto;?>">
						<i class="" style="font-size: 20px;font-style: normal;">📠</i>
					</a>										
				</span>
			</td>
		</tr>
		
		</tbody>
	</table>