<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head" >
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Chamados&nbsp;&nbsp;
					</h3>
					<div class="m-portlet__head-caption cadastrar_orcamento">													
						<a href="<?php echo base_url('AreaClientes/cadastraChamado')?>" class="" style="color: #ffcc00; font-weight: bold;" >
							<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo chamado"></i>
						</a>
					</div>
				</div>
				
			</div>
			
			<div class="m-list__content">
                <div class="m-list-badge" style="margin-top: 25px;">					
				</div>
			</div>		
			<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: -25px;" data-toggle="m-tooltip" data-placement="top" title="Tour Virtual" id="tour_virtual">
				<i class="la la-info"></i>
			</a>
		
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >
			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;width: 5%;"># Chamado</th>						
						<th style="text-align: center;width: 15%;">Situação</th>						
						<th style="text-align: center;width: 15%;">Tipo</th>	
						<th style="text-align: center;width: 15%;">Criação</th>
						<th style="text-align: center;width: 45%;">Descrição</th>
						<th style="width: 5%;">Ações</th>
					</tr>
				</thead>
				<tbody>					
					<?php 	foreach($dados as $dado){ 	?>
						<tr>
							<td style="text-align: center;"><?php echo $dado['id']; ?></td>																	
							<td style="text-align: center;text-transform: capitalize;">
								<?php if( $dado['status_id'] == 1 ){
									$status = "btn m-btn--pill m-btn--air btn-success";
								}elseif( $dado['status_id'] == 2 ){
									$status = "btn m-btn--pill m-btn--air btn-info";
								}elseif( $dado['status_id'] == 3 ){
									$status = "btn m-btn--pill m-btn--air btn-warning";
								}elseif( $dado['status_id'] == 4 ){
									$status = "btn m-btn--pill m-btn--air btn-danger";
								}elseif( $dado['status_id'] == 5 ){
									$status = "btn m-btn--pill m-btn--air btn-primary";
								}elseif( $dado['status_id'] == 10 ){
									$status = "btn m-btn--pill m-btn--air btn-metal";
								
								}elseif( $dado['status_id'] == 6 ){
									$status = "btn m-btn--pill m-btn--air btn-brand";
								}
								?>
								<button class="status <?php echo $status; ?>" status="<?php echo $dado['status'];?>" title="Status do chamado" onclick="status(<?php echo $dado['id']; ?>);">
									<?php echo ucfirst($dado['status']); ?>								
								</button>
							</td>
							<td style="text-align: center;"><?php echo $dado['tipo']; ?></td>								
							<td style="text-align: center;">
								<?php echo date('d/m/Y H:i:s',strtotime($dado['inicio'])); ?>							
							</td>							
							<td style=""><?php echo $dado['descricao']; ?></td>
							<td data-field="Actions" class="m-datatable__cell " style="">
								<a href="<?php echo base_url('AreaClientes/geraChamadoPdf/'.$dado['id'].'/post')?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" >
									<i class="la la-print	 editar"></i>
								</a>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	
	
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('retorno') == 'erro'){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['retorno']);} ?>
		<?php if ($this->session->flashdata('retorno') == 'sucesso'){ ?>
		<script type="text/javascript">			
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaClientes/chamados';
		   	}); 
		</script>	
		<?php unset($_SESSION['retorno']); } ?>