<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px;
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{
	padding: 2px !important;
	border-bottom: 1px solid #f0f0f0;
	border-right: 1px solid #f0f0f0;
	font-size: 9px;	
}


</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;" "="">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
										07400-230 - Arujá - SP<br>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br>
			</p>
		</div>
		<div class="corpo">	
			<h3 style="text-align: center;margin-top: 15px;	">PROPOSTA PARA O FORNECIMENTO DE BOMBA COMERCIAL</h3>		
			<table cellspacing="0" style="margin-top: 15px;">
				<tbody ><tr>
					<td style=" width: 10% !important;">ORÇAMENTO Nº: </td>
					<td><?php echo $empresa['orcamento_id']; ?></td>
					<td style=" width: 10% !important;">EMISSÃO: </td>
					<td><?php echo date('d/m/Y H:i:s', strtotime($empresa['emissao']));?> </td>
					<td style="text-align: right;">VALIDADE: </td>
					<td>30 DIAS </td>

				</tr>
				<tr>
					<td>RAZÃO SOCIAL: </td>
					<td colspan="5"><?php echo $empresa['razao_social']; ?> </td>					
				</tr>
				<tr>
					<td>CNPJ: </td>
					<td><?php echo $empresa['cnpj']; ?> </td>					
					<td>I.E.: </td>					
					<td colspan="3"><?php echo $empresa['inscricao_estadual']; ?> </td>					
				</tr>
				<tr>
					<td>TELEFONE: </td>
					<td><?php echo $empresa['telefone']; ?> </td>					
					<td>ENDEREÇO: </td>					
					<td colspan="3"><?php echo $empresa['endereco'];?> </td>					
				</tr>
				<tr>
					<td>CELULAR: </td>
					<td><?php //echo $empresa['celular']; ?></td>					
					<td>BAIRRO: </td>					
					<td colspan="3"><?php //echo $empresa['bairro'];?></td>					
				</tr>
				<tr>
					<td>FAX:</td>
					<td></td>					
					<td>CIDADE:</td>					
					<td><?php echo $empresa['cidade'];?></td>					
					<td style="text-align: right;">CEP </td>					
					<td></td>					
				</tr>
				<tr>
					<td>E-MAIL: </td>
					<td><?php echo $empresa['email'];?></td>					
					<td>UF: </td>					
					<td><?php echo $empresa['estado'];?></td>					
					<td style="text-align: right;">REPRESENTANTE LEGAL </td>					
					<td><?php echo $empresa['representante_legal'];?></td>					
				</tr>
			</tbody></table>	
			<img src="./bootstrap/img/imgOrcamento.jpg" width="100%" style="margin-top: 15px;">
		</div>
		<div class="cabecalho" style="PAGE-BREAK-BEFORE: always">
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVIÇOS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
										Av. Getúlio Vargas, 280 - Jd. Ângelo<br>
										07400-230 - Arujá - SP<br>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br>
			</p>
		</div>
		<table class="table" cellspacing="0" style="margin-top: 25px;">
			<thead bgcolor="#ccc" style="background-color: #ccc;font-size: 10px;">
				<tr  >
					<th>#</th>					
					<th>MODELO</th>
					<th>DESCRIÇÃO</th>
					<th>QTD</th>					
				</tr>
			</thead>
			<tbody>
				<?php $i=1; $total = 0; foreach( $produtos as $produto ){?>
				<tr>
					<td cellspacing="0" cellpadding="0" style="text-align: center;"><?php echo $i; ?></td>					
					<td cellspacing="0" style="text-align: center;"><?php echo $produto['modelo']; ?></td>
					<td cellspacing="0"><?php echo $produto['descricao']; ?></td>
					<td cellspacing="0" style="text-align: center;"><?php echo $produto['qtd']; ?></td>
				</tr>	
				<?php 	} 	 ?>		
					
			</tbody>
		</table>
		<div class="info">
			<p>* Preços com ICMS de <?php echo $empresa['valor_tributo']; ?>%, IPI de 5% inclusos</p>
			<p>
				<br><br><b>CONDIÇÃO DE PAGAMENTO (sujeito à análise de crédito):</b><br/>
					25% Na confirmação do pedido;<br/>
					25% Na disponibilidade dos equipamentos;<br/>
					50% 4 PARCELAS IGUAIS APÓS EMISSÃO DA NOTA FISCAL<br>

			</p>
			<p>
				<b>OUTRAS OPÇÕES DE CRÉDITO:</b><br/>
				Cartão BNDES e Proger;<br>
				Finame, Leasing (sob consulta).<br>

			</p>
			<p><b>PRAZO DE ENTREGA:</b><br/>
				30 dias úteis - A contar da data do pedido de venda assinado, pagamento e definição do produto final
					(pintura, acessórios e configurações).<br>
			</p>
			
			<p>
				<b>GARANTIA:</b><br>
				24 meses - À contar da data de emissão da nota fiscal.<br>
			</p>
			<p>
				<b>FRETE:</b><br>
FOB - POR CONTA DO CLIENTE: o Cliente retira a(s) bomba(s) em nossa fábrica, localizada na Av. Getúlio
Vargas, 280 - Jd. Ângelo - Arujá - SP - 07400-230	<br> 
			</p>
			<p>
				<b>START-UP:</b><br>
				O start-up somente poderá ser realizado por um técnico autorizado pela Wertco.<br>
				Esse serviço é gratuito e deverá ser solicitado somente após a instalação completa do posto e da bomba,
com combustível no tanque.<br>
A realização deste procedimento por pessoas não autorizadas, acarretará na perda da garantia dos equipamentos.<br>
			</p>
			<p>
				<b>ASSISTÊNCIA TÉCNICA:</b><br>
				A Wertco disponibiliza uma ampla rede de técnicos autorizados em todo o território nacional.<br>
				Central de Atendimento e Suporte Técnico: (11) 3900-2565.
			</p>
		</div>	
		 
			<p style="position: fixed; bottom: 150;">
				<br/><br/>Estamos a sua disposição para esclarecer quaisquer dúvidas.<br><br>
				Atenciosamente,<br>
				<?php echo $info['nome']; ?><br>
				Departamento de Vendas<br>
				Email: <?php echo $info['email']; ?><br>
				Tel: (11) 3900-2565<br>
				Responsável / Representante / Indicador: <?php echo $representante['responsavel']; ?> - <?php echo $representante['empresa']; ?>
			</p>		
	</div>

</body>
</html>