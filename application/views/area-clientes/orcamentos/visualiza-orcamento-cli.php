<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-list-2"></i>
					</span>
					
					<h3 class="m-portlet__head-text">
						Orçamento #<?php echo $dados[0]->orcamento_id; ?> &nbsp;&nbsp; <?php if( $dados[0]->status == "aberto" ){
							echo '<button class="btn m-btn--pill m-btn--air btn-secondary" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Aberto </button>';
						}elseif( $dados[0]->status == "fechado" ){
							$status = "";
							echo '<button class="btn m-btn--pill m-btn--air btn-success" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Fechado </button>';
						}elseif( $dados[0]->status == "perdido" ){
							echo '<button class="btn m-btn--pill m-btn--air btn-danger" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Perdido </button>';
						}elseif( $dados[0]->status == "cancelado" ){
							
							echo '<button class="btn m-btn--pill m-btn--air btn-warning" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Cancelado </button>';
						}elseif( $dados[0]->status == "orçamento entregue" ){							
							echo '<button class="btn m-btn--pill m-btn--air btn-info" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Orçamento Entregue </button>';
						}else{
							echo '<button class="btn m-btn--pill m-btn--air btn-primary" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Em Negociação </button>';
							
						} ?>
					</h3>
				</div>			
			</div>			
		</div>
		<div class="m-portlet__body">			
			<div class="row">
				<div class="col-lg-4 col-xs-12"> 
					<h5><span class="m--font-warning m--font-boldest">Empresa: </span>							
						<?php echo $dados[0]->cnpj.' | '.$dados[0]->razao_social.' | '.$dados[0]->fantasia;?>						
					</h5>
				</div>							
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">Telefone: </span>
						<?php echo $dados[0]->telefone;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">E-mail: </span>
						<?php echo $dados[0]->email;?>
					</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Enderço:</span>
						 <?php echo $dados[0]->endereco;?>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Cidade:</span>
						 <?php echo $dados[0]->cidade;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Estado:</span>
						 <?php echo $dados[0]->estado;?>
					</h5>
				</div>							
			</div>
		</div>
	</div>
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>Produtos</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<table class="table m-table m-table--head-separator-warning">
			  	<thead>
			    	<tr>
			      		<th>#</th>
			      		<th>Código | Modelo | Descrição</th>			      		
			      		<th>Quantidade</th>			      		
			    	</tr>
			  	</thead>
			  	<tbody total_indice='<?php echo count($dados); ?>'>
			  		<?php $i=1;$total=0; foreach($dados as $dado){ ?>
			    	<tr indice='<?php echo $i; ?>' class="novo_produto_orc">
				      	<th scope="row"><?php echo $i;?></th>
				      	<td>
				      		<?php 
				      		echo $dado->codigo.' | '.$dado->modelo.' | '.$dado->descricao;
                            ?>
                            

				      	</td>
				      	<td>
				      		<input type="text" name="qtd[]" indice='<?php echo $i;?>' class='form-control qtd' orcamento_produto_id="<?php echo $dado->orcamento_produto_id;?>" value='<?php echo $dado->qtd; ?>' style="width: 50px;    float: left;" maxlength="3" id="qtd_<?php echo $i;?>" disabled='disabled' />
				      	</td>				      					      	
			    	</tr>	
			    	<?php $i++;} ?>		    			    	
			  	</tbody>
			</table>
		</div>
	</div>
</div>	

<!-- Modal Emitir orçamento -->

<!-- Modal Adiciona produto -->

