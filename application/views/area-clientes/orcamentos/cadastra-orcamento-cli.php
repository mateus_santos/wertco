
<div class="m-content">	
	<form accept-charset="utf-8" action="<?php echo base_url('AreaClientes/orcamentosCli/'); ?>" method="post" style="padding-left: 25px;padding-right: 25px;">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-list-2"></i>
					</span>
					<h3 class="m-portlet__head-text"> 
						Solicitação de Orçamento 
					</h3>
				</div>			
			</div> 
			<div class="m-portlet__head-tools">
				
			</div>
		</div>
				
		<div class="m-portlet__body">
			
	
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>Produtos</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<input type="hidden"  id="salvar" maxlength="15" name="salvar" value="1"    required />
            <input type="hidden"  id="empresa_id" maxlength="15" name="empresa_id" value="<?php echo $usuario['empresa_id'];?>"  />
			<table class="table m-table m-table--head-separator-warning">
			  	<thead>
			    	<tr>			      		
			      		<th>Modelo | Descrição</th>
			      		<th>Quantidade</th>			      		
			      		<th>
			      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add" data-toggle="m-tooltip" data-placement="top" title="Adicionar produto">
								<i class="la la-plus"></i>
							</a>
						</th>				      		
			    	</tr>
			  	</thead>
			  	<tbody>
			  		 <tr id="modelo" indice='0' total_indice='0'>
                            <td>
                                <?php 
                                        $bombas_op     =   "";
                                        $opcionais_op  =   "";
                                        $bombas_op_slim  =   "";
                                        $bombas_op_baixa  =   "";

                                        foreach( $bombas as $bomba )
                                        {
                                            if( $bomba->tipo_produto_id == 4 ){
                                                $opcionais_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'> ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }elseif($bomba->tipo_produto_id == 1){
                                                $bombas_op_baixa.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'> ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }elseif($bomba->tipo_produto_id == 2){
                                                $bombas_op_slim.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'> ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }elseif ($bomba->tipo_produto_id == 3){
                                                $bombas_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'> ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                            }    
                                        } 
                                ?>
                                <select  class="form-control bombas" style="font-size: 15px;" name="produto[]" indice='0'>
                                	<option value="">Selecione um produto</option>
                                    <optgroup label="Bombas Mangueira Alta" data-subtext="optgroup subtext">
                                        <?php echo $bombas_op; ?>
                                    </optgroup>    
                                    <optgroup label="Bombas Mangueira Slim" data-subtext="optgroup subtext">
                                        <?php echo $bombas_op_slim; ?>
                                    </optgroup>    
                                    <optgroup label="Bombas Mangueira Baixa" data-subtext="optgroup subtext">
                                        <?php echo $bombas_op_baixa; ?>
                                    </optgroup>
                                    <optgroup label="Opcionais" data-subtext="optgroup subtext">
                                        <?php echo $opcionais_op; ?>
                                    </optgroup>
                                </select>
                            </td>
                            <td>
                                <input type="text" maxlength="3" class="form-control qtd" style="width: 70px;" name="quantidade[]" required  />
                            </td>                            
                            <td>
                                <i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
                            </td>

                        </tr>
                    </tbody>
                </table> 
                <div style="text-align: center;">
                    <button type="submit" class="btn btn-warning" >Solicitar orçamento</button>    
                </div>             			
		</div>
	</div>
	</div>
	</div>	
	</form>
</div>	

<?php if ($this->session->flashdata('erro') == TRUE){ ?>
    <script type="text/javascript">     
        swal(
            'Ops!',
            'Aconteceu algum problema, reveja seus dados e tente novamente!',
            'error'
        );
    </script>
<?php unset($_SESSION['erro']);} ?>
<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
    <script type="text/javascript">     
        swal({
                title: "OK!",
                text: 'Orçamento solicitado com sucessoo, em breve entraremos em contato!',
                type: 'success'
            }).then(function() {
                window.location = base_url+'AreaAdministrador/cadastraOrcamento';
        }); 
    </script>   
<?php unset($_SESSION['sucesso']); } ?>