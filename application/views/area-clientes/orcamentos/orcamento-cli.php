<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Solicitação de Orçamentos

					</h3>
				</div>				
			</div>
			
			<div class="m-portlet__head-caption cadastrar_orcamento">													
				<a href="<?php echo base_url('AreaClientes/cadastraOrcamentos')?>" class="" style="color: #ffcc00; font-weight: bold;">
					<i class="la la-plus-circle" style="font-size: 38px;"></i>
				</a>
			</div>
			<div class="">
					<div style="text-align: center;margin-top: 25px;">
					Aberto&nbsp;&nbsp;<span class="m-badge m-badge--secondary"></span>				    	
					&nbsp;Orçamento Entregue&nbsp;&nbsp;<span class="m-badge m-badge--info"></span>
					&nbsp;Em negociação&nbsp;&nbsp;<span class="m-badge m-badge--primary"></span>
					&nbsp;Fechado&nbsp;&nbsp;<span class="m-badge m-badge--success"></span>				    		
					&nbsp;Cancelado&nbsp;&nbsp;<span class="m-badge m-badge--warning"></span>				    	
				    &nbsp;Perdido&nbsp;&nbsp;<span class="m-badge m-badge--danger"></span>	
				    <a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: -10px;" data-toggle="m-tooltip" data-placement="top" title="Tour Virtual" id="tour_virtual">
						<i class="la la-info"></i>
					</a> 		    
					</div>

			</div>					

		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded">
			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;">Número</th>
						<th style="text-align: center;">Solicitante</th>
						<th style="text-align: center;">Emissão</th>											
						<th style="text-align: center;">Status</th>
						<th style="text-align: center;">Andamento</th>
						<th style="text-align: center;">Responsável</th>						
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				<tbody>
					
					<?php foreach($dados as $dado){
						if( $dado->status == "aberto" ){
							$status = "m-badge m-badge--secondary";
						}elseif( $dado->status == "fechado" ){
							$status = "m-badge m-badge--success";
						}elseif( $dado->status == "perdido" ){
							$status = "m-badge m-badge--danger";
						}elseif( $dado->status == "cancelado" ){
							$status = "m-badge m-badge--danger--warning";
						}elseif( $dado->status == "orçamento entregue" ){
							$status = "m-badge m-badge--info";
						}else{
							$status = "m-badge m-badge--primary";
						}
						?>
						<tr>
							<td style="width: 5%;text-align: center;"><?php echo $dado->orcamento_id; ?></td>
							<td style="width: 30%;text-align: center;"> <?php echo $dado->cnpj." | ".$dado->razao_social; ?></td>
							<td style="width: 10%;text-align: center;"><?php echo date('d/m/Y H:i:s', strtotime($dado->emissao)); ?></td>		
							<td style="width: 20%;text-align: center; text-transform: capitalize;"  class="status" >
									<?php echo ucfirst($dado->status); ?> &nbsp;<span class="<?php echo $status;?>"></span>
							</td>														
							<td style="width: 5%;text-align: center;">
								<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour"><i title="Andamento do Orçamento" data-toggle="m-tooltip" data-placement="top" onclick="andamento(<?php echo $dado->orcamento_id; ?>,<?php echo $dado->status_orcamento_id; ?>);" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i></button>
							</td>
							<td style="width: 25%;text-align: center;">
								<?php echo $dado->responsavel; ?>								
							</td>											
							<td data-field="Actions" class="m-datatable__cell " style="width: 5%;">
								<span style="overflow: visible; width: 110px;" class="">								
									<a href="<?php echo base_url('AreaClientes/visualizarOrcamento/'.$dado->orcamento_id); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="m-tooltip" data-placement="top" title="Visualizar" orcamento_id="<?php echo $dado->orcamento_id; ?>" >
										<i class="la la-eye visualizar"></i>
									</a> 									
								</span>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->			
			</div>
		</div>		        
	</div>	
	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group" >
						<label for="exampleSelect1">Situação do Orçamento</label>
						<select class="form-control m-input m-input--air" id="status_orcamento">
													
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" class="btn btn-primary" orcamento_id="">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>	

	<!-- Modal Andamento -->
	<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="m-list-timeline">
						<div class="m-list-timeline__items">                
						</div>
					</div> 
					<hr/>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				</div>
			</div>
		</div>
	</div>	

	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() {
		      	window.location = base_url+'AreaClientes/orcamentosCli';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>
		<?php if ($this->session->flashdata('Atenção') == TRUE){ ?>
		<script type="text/javascript"> 	
			swal({
				title: "Atenção!",
				text: 'Já encontra-se um orçamento aberto para o seu cnpj, entre em contato com o representante da sua região, ou com o setor de vendas da WERTCO.!',
				type: "warning"
			}).then(function() {
		      	window.location = base_url+'AreaClientes/orcamentosCli';
		   	}); 
		</script>	
		<?php unset($_SESSION['Atenção']); } ?>
		