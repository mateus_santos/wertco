<div class="m-content">
	<div class="row">		
		<div class="col-xl-12" >
			<!--begin:: Widgets/Top Products-->
			<div class="m-portlet m-portlet--full-height m-portlet--fit ">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon">
								<i class="flaticon-list" style="color: #215617;" ></i>
							</span>
							<h3 class="m-portlet__head-text">
								Últimas solicitações de peças enviadas
							</h3>
						</div>
					</div>										
				</div>
				<div class="m-portlet__body">
					<!--begin::Widget5-->
					<div class="m-widget4 m-widget4--chart-bottom" style="min-height: 480px">	

						<?php 	
							if(count($ultimas_solicitacoes) > 0 ){
								foreach($ultimas_solicitacoes as $solicitacoes){	
						?>
						<div class="m-widget4__item">												
							<div class="m-widget4__info">
								<span class="m-widget4__title">
								<?php echo $solicitacoes['id'].' - '.$solicitacoes['descricao'] ;?>	
								</span>
								<br>
								<span class="m-widget4__sub">
								<?php echo $solicitacoes['cliente']; ?>
								</span>
							</div>
							<span class="m-widget4__ext">
								<span class="m-widget4__number m--font-brand">
									<a href="<?php echo base_url('AreaAssistencia/visualizarSolicitacaoPecas/'.$solicitacoes['id'])?>" class="btn btn-outline-primary btn-sm 	m-btn m-btn--icon">
										<span>											
											<span>
												Abrir <?php echo $solicitacoes['status']; ?>
											</span>
										</span>
									</a>
								</span>
							</span>
						</div>
						<?php } 
								}else{ ?>
						<h2>Nenhuma solicitação realizada!</h2>			
						<?php 	} ?>
						<div class="m-widget4__chart m-portlet-fit--sides m--margin-top-20" style="height:260px;">
							<canvas id="top_produtos_orcados"></canvas>
						</div>
					</div>
					<!--end::Widget 5-->
				</div>
			</div>
			<!--end:: Widgets/Top Products-->
		</div>		
	</div>	
</div>	
<div class="modal fade" id="m_lista_anexo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title m_lista_anexo_title" id="exampleModalLongTitle"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
				<div class="row anexos">
					
				</div>				
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>	
<script type="text/javascript">
	
	var config = {
		type: 'pie',
		data: {
			datasets: [{
				data:  <?php echo $total_status['valor']; ?>,
				backgroundColor: <?php echo $total_status['cor'];?>,
				label: 'Status'
			}],
			labels: <?php echo $total_status['descricao'];?>,
		},
		options: {
			responsive: true,			 
			 legend: {
           		 labels: {
               		 // This more specific font property overrides the global property
               		 fontColor: '#000'
          	 	 }
       		 }
		}
	};

	var config2 = {
		type: 'bar',
		data: {
			datasets: [{
				data:  				<?php echo $total_defeitos['valor']; ?>,
				backgroundColor: 	<?php echo $total_defeitos['cor'];?>,				
				borderColor: '#000'
			}],
			labels: <?php echo $total_defeitos['descricao'];?>,
		},
		options: {
			responsive: true,
			legend: {
            	display: false
        	}
		}
	};
	
	var config1 = {
            type: 'line',
            data: {
                labels: <?php echo $total_mes['mes'];?>,
                datasets: [{                    
                    backgroundColor: 'rgba(255,204,0,0.24)',
                    borderColor: mUtil.getColor('warning'),
                    labels: <?php echo $total_mes['mes'];?>,
                    pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
                    pointHoverBackgroundColor: mUtil.getColor('danger'),
                    pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),                  
                    data: <?php echo $total_mes['valor']; ?>
                }]
            },
            options: {
                title: {
                    display: false,
                },
                tooltips: {
                   intersect: true,                    
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 0   
                                    
                },
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: true,               
                scales: {
                    xAxes: [{
                        display: false,
                        gridLines: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Mês'
                        }
                    }],
                    yAxes: [{
                        display: false,
                        gridLines: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0.000001
                    },
                    point: {
                        radius: 6,
                        borderWidth: 12
                    }
                },
                layout: {
                    padding: {
                        left: 6,
                        right: 2,
                        top: 0,
                        bottom: 0
                    }
                }
            }
        };

   	var config3 = {
		type: 'bar',
		data: {
			datasets: [{
				data:  				<?php echo $total_estado['valor']; ?>,
				backgroundColor: 	<?php echo $total_estado['cor'];?>,				
				borderColor: '#000'
			}],
			labels: <?php echo $total_estado['descricao'];?>,
		},
		options: {
			responsive: true,
			legend: {
            	display: false
        	}
		}
	};	

	$(document).ready(function(){
		
		var total_chamados 	= new Chart($('#total_chamados'), config);		
		var total_defeitos 	= new Chart($('#total_defeitos'), config2);
		var total_mes 		= new Chart($('#total_mes'), config1);
		var totat_estado 	= new Chart($('#total_estado'), config3);

	});
		
</script>