<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="la la-building-o"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Solicitação  de Peças #<?php  echo $dados[0]['id']; ?>&nbsp;&nbsp;
					</h3>
					<span class="m-portlet__head-icon">
							<a href="<?php echo base_url('AreaAlmoxarifado/solicitacaoPecas'); ?>"><i class="la la-arrow-left"></i></a>
						</span>
					<h3 class="m-portlet__head-text">	
						<a href="<?php echo base_url('AreaAlmoxarifado/solicitacaoPecas'); ?>">Voltar</a>
					</h3>
				</div>			
			</div>
			
		</div>
		<div class="m-portlet__body">
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>Descrição/Observação: 
						<small class="text-muted"> <?php echo $dados[0]['descricao'];?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>Cliente: 
						<small class="text-muted"> <?php echo $dados[0]['cliente'];?></small>
					</h5>
				</div>
				<input type="hidden" value="<?php echo $dados[0]['id'];?>" id="empresa_id">
				
				<div class="col-lg-4 col-xs-12">
					<h5>Destinatário: 
						<small class="text-muted"> <?php echo $dados[0]['destinatario'];?></small>
					</h5>
				</div>				
			</div>
			<hr/>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>Endereço de Entrega: 
						<small class="text-muted"> <?php echo $dados[0]['endereco_entrega'];?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5># Chamado: 
						<small class="text-muted"> <a href="<?php echo base_url('AreaAssistencia/geraChamadoPdf/'.$dados[0]['chamado_id'].'/post')?>" target="_blank"><?php echo $dados[0]['chamado_id'];?></a></small>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						Status da Solicitação: <small class="text-muted"> <?php echo $dados[0]['status'];?></small>
						<input type="hidden" id="status_solicitacao_id" value="<?php echo $dados[0]['status_id'];?>" >
						<input type="hidden" id="solicitacao_id" value="<?php echo $dados[0]['id'];?>" >
					</h5>
				</div>				
			</div>
			<hr/>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Data/Hora Solicitação: <small class="text-muted"> <?php echo date('d/m/Y H:i:s', strtotime($dados[0]['dthr_solicitacao']));?></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Nota Fiscal Vinculada: <small class="text-muted"> <a target="_blank" href="<?php echo base_url('nfe-assistencia/'.$dados[0]['arquivo_nfe']); ?>"><?php echo $dados[0]['nr_nf'];?></a></small>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						Data Envio: <small class="text-muted"> 	<?php echo ($dados[0]['dthr_envio'] == 'null' || $dados[0]['dthr_envio'] == '') ? '' : date('d/m/Y', strtotime($dados[0]['dthr_envio']));?> </small>
					</h5>
				</div>							
			</div>
			<hr/>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						Modo Envio: <small class="text-muted"> <?php echo $dados[0]['modo_envio'];?></small>
					</h5>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-lg-12 col-xs-12">
					<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('AreaAlmoxarifado/atualizaInfoEnvio');?>" method="post">
						<input type="hidden" name="solicitacao_peca_id" value="<?=$dados[0]['id']?>">
						<textarea class="form-control" placeholder="Informações das caixas para envio" id="info_caixas" name="info_caixas" required="required"><?=$dados[0]['info_caixas']?></textarea>
						<button type="submit" name="salvar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase" style="float: left;margin-top: 10px;" id="enviar_info_caixa">Enviar</button>
						<button type="reset" class="btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom"  style="float: left;margin-top: 10px;">Limpar</button>
					</form>					
				</div>
			</div>
			
		</div>
	</div>
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>Peças</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">			
			<table class="table m-table m-table--head-separator-warning">
			  	<thead>
			    	<tr>
			    		<th>#</th>
			      		<th>Peça</th>
			      		<th>Qtd</th>			      		
			      		<th>Custo Unidade (R$)</th>
			    	</tr>
			  	</thead>
			  	<tbody>

			  		<?php $i=1; $aux = 0;  foreach($dados as $dado){ 	?>
			    	<tr>
			    		<td><?php echo $i; ?></td>
				      	<td scope="row"><?php echo $dado['pecas'];?></td>
				      	<td><?php echo $dado['qtd']; ?></td>
				      	<td>
				      		<input type="text" class="form-control m-input custo" name="custo" id="custo-<?php echo $i;?>" maxlength="10" solicitacao_pecas_id="<?php echo $dado['solicitacao_itens_id']; ?>" value="<?php echo $dado['custo']; ?>" solicitacao_id="<?php  echo $dados[0]['id']; ?>" status_solicitacao_id="<?php  echo $dados[0]['status_id']; ?>" descricao='<?php echo $dado['pecas'];?>' />
				      	</td>
				    </tr>
				    <?php  if( $aux != $dado['solicitacao_itens_id'] ){
				    			$n = 1;		
				    			foreach ($itens as $item) {

				    				if ($item['solicitacao_peca_item_id'] == $dado['solicitacao_itens_id']) {
				    					
				    		?>
				    	<tr>
				    		<td colspan="4" style="text-align: center;background: #f0f0f0;">
				    			<div style="width: 35%; margin: 0 auto;">
					    			<span style="float: left;margin-top: 10px;"><?=$n?> -</span> 
					    			<input class="form-control nr_serie" type="text" name="nr_serie" id="<?=$item['id']?>" placeholder="Nr. Série"  solicitacao_peca_item_id="<?php echo $item['solicitacao_peca_item_id']; ?>" style="width: 51%;margin-left: 18px;float: left;" value="<?=$item['nr_serie']?>" />
					    			<i class="fa fa-remove excluir-item" style="float: left;    margin-left: 15px;    margin-top: 10px; color: #ffcc00; display: none;" onclick='excluirItem(<?php echo $item['solicitacao_peca_item_id']; ?>,<?php  echo $dados[0]['id']; ?>,<?php  echo $dados[0]['status_id']; ?>,<?=$item['id']?>,"<?php echo str_replace("'", "", $dado['pecas']);?>","<?=$item['nr_serie']?>",<?php echo $dado['qtd']; ?>);'>					    				
					    			</i>
				    			</div>
				    		</td>
				    	</tr>
				    <?php 
				    				$n++;
				    				}  
				    			} 	
				    		} 
				    ?>
				    
				    <?php 
				    	$aux = $dado['solicitacao_itens_id'];  
				    	$i++; 

					} ?>  	
			  	</tbody>
			</table>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-6">
							<button type="button" id="atualizar" value="1" class="btn m-btn--pill m-btn--air btn-accent m-btn m-btn--custom m-btn--bolder m-btn--uppercase" solicitacao_id="<?php  echo $dados[0]['id']; ?>" chamado_id="<?=$dados[0]['chamado_id']?>" cliente="<?=$dados[0]['cliente'];?>">Peças preparadas para envio</button>					
						</div>							
					</div>
				</div>
			</div>			
		</div>
		
	</div>

</div>
<!-- Modal Excluir -->
<div class="modal fade" id="m_excluir_item" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">	
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-excluir-title" ></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">					
				<div class="form-group m-form__group">
					<label>Motivo da Exclusão</label>
					<div class="m-input-icon m-input-icon--left">
						<textarea class="form-control m-input--air" name="motivo_exclusao" id="motivo_exclusao" required="required"></textarea>
						<span class="m-input-icon__icon m-input-icon__icon--left">
							<span>
								<i class="la la-warning"></i>
							</span>
						</span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				<button type="submit" id ="excluir_item" class="btn btn-success" item_id="" solicitacao_id="" status_solicitacao_id="" item_nr_id="" descricao="" qtd="">Excluir</button>
			</div>
		</div>
	</div>	
</div>	
<?php if ($this->session->flashdata('retorno') == 'erro'){ ?>
	<script type="text/javascript">
		swal('Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error' );
	</script>
<?php unset($_SESSION['retorno']);} ?>
<?php if ($this->session->flashdata('retorno') == 'sucesso'){ ?>
	<script type="text/javascript"> 	
		swal({
            title: "OK!",
            text: 'Atualização realizada com sucesso!',
            type: "success"
        }).then(function() {}); 
	</script>	
<?php unset($_SESSION['retorno']); } ?>