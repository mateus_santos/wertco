<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>XX Concurso 2019 O POSTO MAIS BONITO DO BRASIL</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon1.png')?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />    
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen" href="<?=base_url('bootstrap/css/skins/yellowgreen.css')?>" />
    
	
	 <!-- Revolution Slider CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/settings.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/layers.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/navigation.css')?>" />
	
	
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

</head>

<body class="double-diagonal blog-page blog-post">
    <!-- Preloader Starts -->
    <div class="preloader" id="preloader">
        <div class="logopreloader">
            <img src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-black">
        </div>
        <div class="loader" id="loader"></div>
    </div>
    <!-- Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                     <div class="logo">
                        <a data-toggle="collapse" data-target=".navbar-collapse.show" class="navbar-brand link-menu scroll-to-target" href="#mainslider">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            <!-- Logo Black Starts -->
                            <img id="logo-dark" class="logo-dark" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-dark" />
                            <!-- Logo Black Ends -->
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external" href="<?=base_url()?>"><i class="fa fa-home"></i> Voltar para o site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		
		<!-- Blog Section Starts -->
        <section id="blog" class="blog">
            <!-- Container Starts -->
            <div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-8">
						<!-- Blog Posts Starts -->
						<div class="blog-content">
							<!-- Article Starts -->
							<article>
								<h3>Companytec e Wertco apoiam o XX Concurso 2019 O POSTO MAIS BONITO DO BRASIL</h3>
								<!-- Figure Starts -->
								<figure class="blog-figure">
										<img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/noticiaPostoBonitoWertco.png')?>" alt="">
								</figure>
								<!-- Figure Ends -->
								<!-- Excerpt Starts -->
								<div class="blog-excerpt">
									<p> Promovido pela Revista Posto de Observação e Brascombustíveis (Associação Brasileira do Comércio Varejista de Combustíveis Automotivos e de Lubrificantes), com o apoio do Sincopetro (Sindicato do Comércio Varejista de Derivados de Petróleo do Estado de São Paulo).</p>
                                    <p> O concurso é voltado aos postos revendedores de combustíveis e lojas de conveniência localizados em todo o território nacional que representem a modernidade, a inovação, o respeito aos recursos humanos e meio ambiente, além de proporcionar ao consumidor a melhor experiência possível.</p>
                                    <p> Durante a seleção, foram escolhidos os 5 (cinco) estabelecimentos semifinalistas para Posto Urbano, os 3 (três) semifinalistas para Posto de Rodovia e os 3 (três) semifinalistas para Loja de Conveniência.</p>
                                    <p> O resultado será divulgado na cerimônia de premiação no dia 05 de novembro no Buffet Colonial em Moema - SP, às 19h30.</p>
                                    <p> O resultado foi determinado pela avaliação realizada pela equipe da Revista Posto de Observação em visitas aos finalistas, tomando por base os checklists preparados para cada categoria.</p>
                                    <p> A lista de postos participantes está divulgada no site: 
                                        <a href="http://www.postonet.com.br/pmbb2019/finalistas2019.php">http://www.postonet.com.br/pmbb2019/finalistas2019.php</a> 
                                    </p>	
								</div>
								<!-- Excerpt Ends --> 
							</article> 
							<!-- Article Ends -->
						</div>
						<!-- Latest Blog Posts Ends -->
					</div>
				</div>	
            </div>
        </section>
        <!-- Blog Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center">
            <!-- Container Starts -->
            <div class="container">
                <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/00001-53 </p>
                <p>
                    &copy; Copyright <?=date('Y')?> Wertco 
                </p>
                <!-- Copyright Text Ends -->
                <!-- Social Media Links Starts
                <div class="social-icons">
                    <ul class="social">
                        <li>
                            <a class="twitter" href="#" title="twitter"></a>
                        </li>
                        <li>
                            <a class="facebook" href="#" title="facebook"></a>
                        </li>
                        <li>
                            <a class="google" href="#" title="google"></a>
                        </li>
                        <li>
                            <a class="skype" href="#" title="skype"></a>
                        </li>
                        <li>
                            <a class="instagram" href="#" title="instagram"></a>
                        </li>
                        <li>
                            <a class="linkedin" href="#" title="linkedin"></a>
                        </li>
                        <li>
                            <a class="youtube" href="#" title="youtube"></a>
                        </li>
                    </ul>
                </div>
                <!-- Social Media Links Ends -->
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
        <!-- Back To Top Starts -->
        <div id="back-top-wrapper" class="hidden-xs">
            <p id="back-top">
                <a href="#top"><span></span></a>
            </p>
        </div>
        <!-- Back To Top Ends -->
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js')?>"></script>

    <!-- Live Style Switcher JS File - only demo -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/styleswitcher.js')?>"></script>

    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>

    <!-- Revolution Slider Initialization Ends -->
</body>


<!-- Mirrored from celtano.top/salimo/demos/blog-post.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
</html>