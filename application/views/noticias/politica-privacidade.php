<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>POLÍTICA DE PRIVACIDADE - LGPD</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon1.png')?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />    
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen" href="<?=base_url('bootstrap/css/skins/yellowgreen.css')?>" />
    
	
	 <!-- Revolution Slider CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/settings.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/layers.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/navigation.css')?>" />
	
	
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

</head>

<body class="double-diagonal blog-page blog-post">
    <!-- Preloader Starts -->
    <div class="preloader" id="preloader">
        <div class="logopreloader">
            <img src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-black">
        </div>
        <div class="loader" id="loader"></div>
    </div>
    <!-- Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                     <div class="logo">
                        <a data-toggle="collapse" data-target=".navbar-collapse.show" class="navbar-brand link-menu scroll-to-target" href="#mainslider">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            <!-- Logo Black Starts -->
                            <img id="logo-dark" class="logo-dark" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-dark" />
                            <!-- Logo Black Ends -->
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external" href="<?=base_url()?>"><i class="fa fa-home"></i> Voltar para o site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		
		<!-- Blog Section Starts -->
        <section id="blog" class="blog">
            <!-- Container Starts -->
            <div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-8">
						<!-- Blog Posts Starts -->
						<div class="blog-content">
							<!-- Article Starts -->
							<article>
								<h3>POLÍTICA DE PRIVACIDADE - LGPD</h3>
								<!-- Figure Starts -->
								<figure class="blog-figure">
									<!--<img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/NoticiasWertcoExponline.png')?>" alt="">-->
								</figure>
								<!-- Figure Ends -->
								<!-- Excerpt Starts -->
								<div class="blog-excerpt">
									<p>A WERTCO IND. COM. E SERVIÇOS DE MANUTENÇÃO EM BOMBAS LTDA. não quer assediar ninguém, muito menos usar seus dados para fins indevidos, sejam eles quais forem.<br><br>

No entanto, é necessário deixar claro que coletaremos alguns de seus dados em nosso site, sejam eles COMPULSÓRIOS ou ESPONTÂNEA, o que explicaremos a seguir.<br><br>

Se você tem menos de 18 anos, infelizmente, não será possível utilizar nossas plataformas, pois todas elas capturam, de alguma forma, seus dados. Portanto, se seguir, você atesta sua maioridade.<br><br>

Então, estando ciente de nossa POLÍTICA DE PRIVACIDADE, você declara que concorda com todas as implicações — que verá, não são nada demais.<br><br>

O recolhimento de dados de forma COMPULSÓRIA é feito através de cookies de quaisquer tipos e outras ferramentas tecnológicas. Mas ele serve para sabermos que páginas foram visitadas por você, quantas vezes, horários e durações. Se algum dia, você se cadastrou em nossa plataforma, a gente tem a possibilidade de saber que foi você que nos visitou novamente e como o fez. Porém, se você nunca se cadastrou conosco, a gente não faz ideia de quem você seja, e só temos acesso a dados anônimos de uso.<br><br>

O recolhimento de dados de forma ESPONTÂNEA é aquele quando você preenche um formulário. Neste momento, você nos informa que concordou com nossa Política de Privacidade também. Quando isso acontece, você o faz por interesse próprio em nossos produtos, serviços ou informações. Nesse caso, para conseguirmos lhe oferecer o que deseja, adicionamos você a nossa base. Chamamos as pessoas em nossa base de “leads”. Assim, quando alguém cadastrado visita nossas páginas novamente, estaremos registrando seus passos conosco, da forma COMPULSÓRIA expressa acima, para compreendermos que tipo de conteúdo nossos leads mais visitam e podermos criar conteúdos mais eficientes. É só isso.<br><br>

Quando a gente percebe que um lead da base, que consuma nosso conteúdo ou não, é um potencial cliente, talvez a gente envie (caso ele consinta no formulário) alguns e-mails esporádicos sobre novidades da empresa. Nesses emails sempre há um convite ao descadastramento da base. No caso mais extremo, é possível também que a gente entre em contato para oferecer um serviço ou convidá-lo para ser parceiro em algum projeto.<br><br>

Claro que também iremos usar o contato fornecido em questões contratuais de clientes por legítimo interesse, em casos que a qualidade da entrega de nosso trabalho exija contato ou mesmo repassar os dados para um fornecedor realizar uma entrega, por exemplo.<br><br>

É bom citar também, que todas as plataformas (sejam as contratadas por nós, sejam as redes sociais) podem coletar também dados como localização e até alguns que provavelmente não teremos interesse, como IP etc. Fazem isso por uma questão de segurança deles e, até mesmo sua, caso alguém esteja tentando se cadastrar com o seu nome.<br><br>

Você pode ter total certeza que a gente não utiliza, nem NUNCA utilizará, seus dados para quaisquer outros fins, muito menos cederemos a terceiros que nada tenham a ver com eles.<br><br>

É bom lembrar também que em nossas mídias sociais (salvo quando publicarmos algum formulário de captação, no qual você mesmo insere os dados) a gente não transfere nenhum tipo de informação sua para nossas bases de dados. Por exemplo, se você comentou em um post do Facebook, curtiu uma foto no Instagram ou retuitou algo pelo Twitter, esses dados ficam apenas lá nessas redes, seguindo a política de privacidade de cada uma. Agora, se a gente colocar algum formulário de captação por lá, aí sim, você concorda com a Política de Privacidade específica da rede em questão e com a nossa, funcionando conforme falamos acima sobre coleta ESPONTÂNEA.<br><br>

Esses dados ficam armazenados nessas plataformas com acesso online, e pelo tempo necessário para o interesse do cadastro, seguindo todos os protocolos de segurança que elas oferecem.<br><br>

Como ninguém fica parado no tempo, a gente se vê no direito de alterar esta POLÍTICA DE PRIVACIDADE toda vez que se fizer necessário. Por exemplo, quando adotarmos alguma ferramenta nova com novas funções que obriguem a ajustes por aqui. Mas tenha sempre a certeza de que faremos dentro dos limites de bom senso e zelando ao extremo pela sua privacidade e segurança.<br><br>

Por último, resta reforçar que você tem o direito a qualquer momento de solicitar todo e qualquer dado que armazenemos a seu respeito em nossas plataformas, bem como retificar, requerer exclusão, transferir, limitar, se opor ao tratamento ou retirar o consentimento. Esse procedimento pode ser feito através do email marketing[arroba]companytec.com.br (escrevemos assim para o endereço não ser capturado por robôs e ser usado sem o nosso consentimento. Você entendeu, né? Caso não tenha entendido, nos procure por qualquer outro meio disponível em nossos pontos de contato.<br><br>

Sendo assim, gostaríamos de finalizar dizendo que prezamos fortemente pela ética, segurança e privacidade dos usuários de nossas plataformas.<br><br>

Obrigado<br><br>

</p>
									
								</div>
								<!-- Excerpt Ends --> 
							</article> 
							<!-- Article Ends -->
						</div>
						<!-- Latest Blog Posts Ends -->
					</div>
				</div>	
            </div>
        </section>
        <!-- Blog Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center">
            <!-- Container Starts -->
            <div class="container">
                <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/00001-53 </p>
                <p>
                    &copy; Copyright <?php echo date('Y');?> Wertco 
                </p>
                <!-- Copyright Text Ends -->
                <!-- Social Media Links Starts
                <div class="social-icons">
                    <ul class="social">
                        <li>
                            <a class="twitter" href="#" title="twitter"></a>
                        </li>
                        <li>
                            <a class="facebook" href="#" title="facebook"></a>
                        </li>
                        <li>
                            <a class="google" href="#" title="google"></a>
                        </li>
                        <li>
                            <a class="skype" href="#" title="skype"></a>
                        </li>
                        <li>
                            <a class="instagram" href="#" title="instagram"></a>
                        </li>
                        <li>
                            <a class="linkedin" href="#" title="linkedin"></a>
                        </li>
                        <li>
                            <a class="youtube" href="#" title="youtube"></a>
                        </li>
                    </ul>
                </div>
                <!-- Social Media Links Ends -->
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
        <!-- Back To Top Starts -->
        <div id="back-top-wrapper" class="hidden-xs">
            <p id="back-top">
                <a href="#top"><span></span></a>
            </p>
        </div>
        <!-- Back To Top Ends -->
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js')?>"></script>

    <!-- Live Style Switcher JS File - only demo -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/styleswitcher.js')?>"></script>

    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>

    <!-- Revolution Slider Initialization Ends -->
</body>


<!-- Mirrored from celtano.top/salimo/demos/blog-post.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
</html>