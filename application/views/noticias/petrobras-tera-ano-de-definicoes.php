<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Petrobras terá ano de definições</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon1.png')?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />    
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen" href="<?=base_url('bootstrap/css/skins/yellowgreen.css')?>" />
    
	
	 <!-- Revolution Slider CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/settings.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/layers.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/navigation.css')?>" />
	
	
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

</head>

<body class="double-diagonal blog-page blog-post">
    <!-- Preloader Starts -->
    <div class="preloader" id="preloader">
        <div class="logopreloader">
            <img src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-black">
        </div>
        <div class="loader" id="loader"></div>
    </div>
    <!-- Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                     <div class="logo">
                        <a data-toggle="collapse" data-target=".navbar-collapse.show" class="navbar-brand link-menu scroll-to-target" href="#mainslider">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            <!-- Logo Black Starts -->
                            <img id="logo-dark" class="logo-dark" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-dark" />
                            <!-- Logo Black Ends -->
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external" href="<?=base_url()?>"><i class="fa fa-home"></i> Voltar para o site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		
		<!-- Blog Section Starts -->
        <section id="blog" class="blog">
            <!-- Container Starts -->
            <div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-8">
						<!-- Blog Posts Starts -->
						<div class="blog-content">
							<!-- Article Starts -->
							<article>
								<h3>Petrobras terá ano de definições</h3>
								<!-- Figure Starts -->
								<figure class="blog-figure">
										<img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/petrobras.jpg')?>" alt="">
								</figure>
								<!-- Figure Ends -->
								<!-- Excerpt Starts -->
								<div class="blog-excerpt">
									<p>O ano de 2019 promete ser de muitas definições para a Petrobras. O novo presidente da companhia, Roberto Castello Branco, terá de lidar nos próximos meses com pautas importantes para o futuro da estatal. Cessão onerosa, abertura do refino e a decisão sobre continuar ou não no setor de petroquímica são alguns dos assuntos da agenda da empresa no ano.<br><br>



Um dos temas mais aguardados do ano envolve a conclusão do acordo com a União para revisão do contrato da cessão onerosa – que cedeu à estatal o direito de produzir 5 bilhões de barris no pré-sal, como parte da operação que elevou a participação da União no capital da empresa em 2010. O Santander vê o acordo como um elemento “catalisador” para a Petrobras em 2019 e acredita ser possível chegar a uma relação “ganha-ganha” entre as partes.<br><br>



Isso porque o acordo poderá representar a entrada de bilhões de reais no caixa da petroleira, ao mesmo tempo em que destravaria o leilão dos excedentes da cessão onerosa – volumes de óleo descobertos pela estatal e que excedem os 5 bilhões de barris as quais ela tem direito de produzir.<br><br>



“Um dos objetivos do recém-eleito governo brasileiro [de Jair Bolsonaro] é reduzir o déficit fiscal e uma iniciativa que ajudaria nessa frente seria o leilão dos excedentes da cessão onerosa”, cita o relatório assinado pelos analistas Christian Audi, Gustavo Allevato e Rodrigo Almeida.<br><br>



O Santander estima que a Petrobras tenha a receber entre US$ 8 bilhões e US$ 12 bilhões da União. O Valor apurou que, num dos cenários discutidos com o governo, a estatal tem a receber cerca de US$ 14 bilhões, mais o ressarcimento de até US$ 10 bilhões pelos investimentos já feitos nas áreas que eventualmente sejam licitadas no leilão dos excedentes. Os números, contudo, ainda são alvo de embates dentro do governo. Diante do esforço para ajustar as contas públicas, a equipe econômica do ministro Paulo Guedes busca reduzir ao máximo o valor a pagar.<br><br>



“A cessão onerosa é o assunto de 2019. Com esse assunto pendente, o ano ainda não começou para a Petrobras. Trata-se de um valor relevante que pode permitir uma revisão do atual plano de negócios da empresa, para que ele incorpore mais [investimentos no] pré-sal”, comenta a coordenadora de pesquisa da FGV Energia, Fernanda Delgado.<br><br>



O plano de negócios da empresa prevê investimentos de US$ 84,1 bilhões entre 2019 e 2023 e metas de vendas de ativos de US$ 26,9 bilhões no período. A expectativa é que o ano seja marcado por importantes decisões de desinvestimentos, dentre as quais vender ou não sua fatia na Braskem (onde a estatal é sócia da Odebrecht).<br><br>



A Petrobras ainda aguarda os avanços das negociações entre a Odebrecht e a holandesa LyondellBasell para avaliar se acompanhará ou não o movimento de sua sócia de vender o ativo. Em 2017, a estatal anunciou que sairia integralmente do negócio de petroquímica, mas recentemente voltou atrás, ao incorporar a área como atividade integrada ao refino dentro de seu novo plano de negócios.<br><br>



Caberá à nova gestão da Petrobras definir os próximos passos. Ao tomar posse, Castello Branco anunciou que espera rever o atual plano de negócios “o mais rapidamente possível” e que pode vir a fazer “ajustes marginais”, mas ainda não deu detalhes sobre o segmento petroquímico.<br><br>



O ano começa também com a expectativa de retomada de desinvestimentos importantes, como o das refinarias e da Transportadora Associada de Gás (TAG). Essas negociações estavam suspensas desde meados de 2018, mas a liminar do Supremo Tribunal Federal (STF) que proibia a venda de estatais e suas subsidiárias sem o aval do Congresso foi derrubada. Foi em parte por causa dessa liminar que a petroleira não conseguiu atingir a sua meta de desinvestimentos de 2017/2018, de US$ 21 bilhões.<br><br>



O ano de 2019 será um ano crítico também para a negociação dos campos terrestres e em águas rasas. A Agência Nacional de Petróleo (ANP) decidiu pressionar para que a estatal acelere as negociações, já que os desinvestimentos estão abertos desde 2016. O órgão regulador definiu um prazo até junho para que a estatal conclua as negociações em curso. A petroleira pediu a postergação do prazo e a agência ainda analisa o pleito.<br><br>



“Os desinvestimentos estavam sendo conduzidos ao bel prazer da Petrobras, seguindo os interesses da Petrobras, no tempo da Petrobras. Isso é legitimo, mas agora não é mais a Petrobras que define prazo para a venda”, disse recentemente o diretor-geral da ANP, Décio Oddone.<br><br>



O ano deverá ser marcado também por um salto de 13% na produção de petróleo, para níveis recordes de 2,3 milhões de barris/dia no Brasil. Esse crescimento, puxado pela entrada de quatro novas plataformas, no entanto, deve se dar num ambiente mais desafiador do ponto de vista dos preços internacionais. Segundo as projeções da agência de Administração de Informações de Energia dos Estados Unidos (EIA, na sigla em inglês), o preço médio do Brent deve fechar 2019 a US$ 60,5 o barril, abaixo dos US$ 71 de 2018.<br>.</p>									
<p><b>Fonte: Valor Econômico</b></p>
									
								</div>
								<!-- Excerpt Ends --> 
							</article> 
							<!-- Article Ends -->
						</div>
						<!-- Latest Blog Posts Ends -->
					</div>
				</div>	
            </div>
        </section>
        <!-- Blog Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center">
            <!-- Container Starts -->
            <div class="container">
                <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/00001-53 </p>
                <p>
                    &copy; Copyright 2017 Wertco 
                </p>
                <!-- Copyright Text Ends -->
                <!-- Social Media Links Starts
                <div class="social-icons">
                    <ul class="social">
                        <li>
                            <a class="twitter" href="#" title="twitter"></a>
                        </li>
                        <li>
                            <a class="facebook" href="#" title="facebook"></a>
                        </li>
                        <li>
                            <a class="google" href="#" title="google"></a>
                        </li>
                        <li>
                            <a class="skype" href="#" title="skype"></a>
                        </li>
                        <li>
                            <a class="instagram" href="#" title="instagram"></a>
                        </li>
                        <li>
                            <a class="linkedin" href="#" title="linkedin"></a>
                        </li>
                        <li>
                            <a class="youtube" href="#" title="youtube"></a>
                        </li>
                    </ul>
                </div>
                <!-- Social Media Links Ends -->
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
        <!-- Back To Top Starts -->
        <div id="back-top-wrapper" class="hidden-xs">
            <p id="back-top">
                <a href="#top"><span></span></a>
            </p>
        </div>
        <!-- Back To Top Ends -->
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js')?>"></script>

    <!-- Live Style Switcher JS File - only demo -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/styleswitcher.js')?>"></script>

    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>

    <!-- Revolution Slider Initialization Ends -->
</body>


<!-- Mirrored from celtano.top/salimo/demos/blog-post.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
</html>