<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Etanol recua em 15 Estados, diz ANP; preço médio cai 0,04% no País</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon1.png')?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />    
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen" href="<?=base_url('bootstrap/css/skins/yellowgreen.css')?>" />
    
	
	 <!-- Revolution Slider CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/settings.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/layers.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/navigation.css')?>" />
	
	
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

</head>

<body class="double-diagonal blog-page blog-post">
    <!-- Preloader Starts -->
    <div class="preloader" id="preloader">
        <div class="logopreloader">
            <img src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-black">
        </div>
        <div class="loader" id="loader"></div>
    </div>
    <!-- Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                     <div class="logo">
                        <a data-toggle="collapse" data-target=".navbar-collapse.show" class="navbar-brand link-menu scroll-to-target" href="#mainslider">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            <!-- Logo Black Starts -->
                            <img id="logo-dark" class="logo-dark" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-dark" />
                            <!-- Logo Black Ends -->
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external" href="<?=base_url()?>"><i class="fa fa-home"></i> Voltar para o site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		
		<!-- Blog Section Starts -->
        <section id="blog" class="blog">
            <!-- Container Starts -->
            <div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-8">
						<!-- Blog Posts Starts -->
						<div class="blog-content">
							<!-- Article Starts -->
							<article>
								<h3>Etanol recua em 15 Estados, diz ANP; preço médio cai 0,04% no País</h3>
								<!-- Figure Starts -->
								<figure class="blog-figure">
									<img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/10.jpg')?>" alt="">
								</figure>
								<!-- Figure Ends -->
								<!-- Excerpt Starts -->
								<div class="blog-excerpt">
								<p>Os preços do etanol hidratado recuaram nos postos de 15 Estados brasileiros na semana passada, segundo levantamento da Agência Nacional do Petróleo, Gás Natural e Biocombustíveis (ANP) compilado pelo AE-Taxas. Em dez Estados e no Distrito Federal houve alta e no Amapá não foi feita avaliação.<br/>   

                                Na média dos postos brasileiros pesquisados pela ANP houve leve queda de 0,04% no preço do etanol na semana passada, para R$ 2,830.<br/>
                                Em São Paulo, principal Estado produtor e consumidor, a cotação média do hidratado também recuou 0,04% sobre a semana anterior, de R$ 2,644 para R$ 2,643 o litro.<br/>
                                A Paraíba registrou maior recuo no preço do biocombustível na semana passada, de 1,21%, e a maior alta, de 1,66%, foi em Alagoas.<br/>Na comparação mensal os preços do etanol subiram apenas no Acre, Alagoas, Bahia e em Goiás, com recuos em todas as outras Unidades da Federação. No Amapá, não houve avaliação.<br/>Na média brasileira o preço do etanol pesquisado pela ANP acumulou baixa de 1,36% na comparação mensal. A maior queda nos preços do biocombustível no período, de 4,59%, foi nos postos do Rio Grande do Norte.<br/>  O preço mínimo registrado na semana passada para o etanol em um posto foi de R$ 2,259 o litro, em São Paulo, e o máximo individual ficou de R$ 4,949 o litro, no Rio Grande do Sul. São Paulo mantém o menor preço médio estadual, de R$ 2,643 o litro, e o maior preço médio ocorreu nos postos do Acre, de R$ 4,041 o litro.<br/><br/>Competitividade<br/><br/>
                                Pela segunda semana consecutiva, os preços médios do etanol permanecem vantajosos ante os da gasolina em apenas cinco Estados brasileiros – Goiás, Mato Grosso, Minas Gerais, Paraná e São Paulo.<br/>
                                O levantamento da ANP compilado pelo AE-Taxas considera que o etanol de cana ou de milho, por ter menor poder calorífico, tenha um preço limite de 70% do derivado de petróleo nos postos para ser considerado vantajoso.<br/>Em Mato Grosso, o hidratado é vendido em média por 59,68% do preço da gasolina, em São Paulo por 64,26%, em Minas Gerais a 64,73% e em Goiás a 68,56%. No Paraná, a paridade está em 69,53%. Na média brasileira, a paridade é de 65,36% entre os preços médios do etanol e da gasolina, também favorável ao biocombustível.<br/>A gasolina segue mais vantajosa em Roraima, com a paridade de 96,43% para o preço do etanol.<br/><br/>
                                *Extraída do site UDOP.
                                </p>
								<p><b>Fonte: <a href="http://www.udop.com.br/index.php?item=noticias&cod=1174573" target="_blank">site UDOP</a></b></p>									
								</div>
								<!-- Excerpt Ends --> 
							</article> 
							<!-- Article Ends -->
						</div>
						<!-- Latest Blog Posts Ends -->
					</div>
				</div>	
            </div>
        </section>
        <!-- Blog Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center">
            <!-- Container Starts -->
            <div class="container">
                <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/00001-53 </p>
                <p>
                    &copy; Copyright <?php echo date('Y'); ?> Wertco 
                </p>
                <!-- Copyright Text Ends -->
                <!-- Social Media Links Starts
                <div class="social-icons">
                    <ul class="social">
                        <li>
                            <a class="twitter" href="#" title="twitter"></a>
                        </li>
                        <li>
                            <a class="facebook" href="#" title="facebook"></a>
                        </li>
                        <li>
                            <a class="google" href="#" title="google"></a>
                        </li>
                        <li>
                            <a class="skype" href="#" title="skype"></a>
                        </li>
                        <li>
                            <a class="instagram" href="#" title="instagram"></a>
                        </li>
                        <li>
                            <a class="linkedin" href="#" title="linkedin"></a>
                        </li>
                        <li>
                            <a class="youtube" href="#" title="youtube"></a>
                        </li>
                    </ul>
                </div>
                <!-- Social Media Links Ends -->
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
        <!-- Back To Top Starts -->
        <div id="back-top-wrapper" class="hidden-xs">
            <p id="back-top">
                <a href="#top"><span></span></a>
            </p>
        </div>
        <!-- Back To Top Ends -->
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js')?>"></script>

    <!-- Live Style Switcher JS File - only demo -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/styleswitcher.js')?>"></script>

    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>

    <!-- Revolution Slider Initialization Ends -->
</body>


<!-- Mirrored from celtano.top/salimo/demos/blog-post.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
</html>