<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>ANP faz audiência pública sobre primeira resolução de transparência nos preços</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon1.png')?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />    
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen" href="<?=base_url('bootstrap/css/skins/yellowgreen.css')?>" />
    
	
	 <!-- Revolution Slider CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/settings.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/layers.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/navigation.css')?>" />
	
	
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

</head>

<body class="double-diagonal blog-page blog-post">
    <!-- Preloader Starts -->
    <div class="preloader" id="preloader">
        <div class="logopreloader">
            <img src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-black">
        </div>
        <div class="loader" id="loader"></div>
    </div>
    <!-- Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                     <div class="logo">
                        <a data-toggle="collapse" data-target=".navbar-collapse.show" class="navbar-brand link-menu scroll-to-target" href="#mainslider">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            <!-- Logo Black Starts -->
                            <img id="logo-dark" class="logo-dark" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-dark" />
                            <!-- Logo Black Ends -->
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external" href="<?=base_url()?>"><i class="fa fa-home"></i> Voltar para o site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		
		<!-- Blog Section Starts -->
        <section id="blog" class="blog">
            <!-- Container Starts -->
            <div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-8">
						<!-- Blog Posts Starts -->
						<div class="blog-content">
							<!-- Article Starts -->
							<article>
								<h3>ANP faz audiência pública sobre primeira resolução de transparência nos preços</h3>
								<!-- Figure Starts -->
								<figure class="blog-figure">
										<img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/audiencia.jpg')?>" alt="">
								</figure>
								<!-- Figure Ends -->
								<!-- Excerpt Starts -->
								<div class="blog-excerpt">
									<p>A ANP realizou no dia 20/3 audiência pública sobre a minuta de resolução que dispõe sobre a transparência na formação de preços relativos à comercialização de derivados de petróleo e biocombustíveis por produtores, importadores e distribuidores.


                                    <br>Na abertura do evento, o diretor José Cesário Cecchi reforçou que transparência não pode ser confundida com controle. “Hoje, estamos fazendo a continuidade do debate sobre esse tema. O objetivo é garantir a transparência, esse é o princípio da agência reguladora. Passa longe da intenção da ANP exercer qualquer tipo de controle sobre os preços”, afirmou.<br>
                                    Cecchi ressaltou ainda que os princípios norteadores da minuta são: liberdade de negociação entre as partes; manutenção da liberdade de preços no mercado de combustíveis; ausência de vinculação a qualquer parâmetro pré-definido pela ANP; e cláusula de preços acrescida aos contratos.
                                   <br>
                                    Com a proposta, os objetivos da ANP são aumentar a transparência e reduzir a assimetria de informação no processo de formação dos preços de derivados de petróleo e biocombustíveis, considerando os fundamentos legais e regulatórios, os benefícios e riscos potenciais, e as características estruturais e de comportamento de cada segmento.<br>Entre os pontos principais da minuta, estão:

<br>
– Obrigação aos produtores e importadores de derivados de petróleo definidos como agentes dominantes (participação de mercado superior a 20% na macrorregião de atuação) de publicarem, no sítio eletrônico da empresa, o preço de lista (preço para pagamento à vista, discriminado por produto, modalidade de venda e ponto de entrega), bem como o histórico dos últimos meses;<br>
– Inclusão, como pré-requisito à homologação pela ANP dos contratos celebrados entre agente dominante (no fornecimento primário) e distribuidor, do preço parametrizado pactuado entre as partes, discriminado por produto e por ponto de entrega, formado por parâmetros fixos ou variáveis exógenas, que seja claro, objetivo e passível de cálculo prévio pelos agentes econômicos partícipes do contrato e pela ANP.
<br>
A proposta resulta do desmembramento de uma minuta de resolução anterior, que dispunha sobre a transparência na formação de preços de derivados de petróleo, gás natural e biocombustíveis em todos os elos da cadeia.

<br>Após análise das 39 contribuições recebidas na consulta pública, a minuta original foi dividia em três: uma para os segmentos de produção, importação e distribuição de derivados de petróleo e biocombustíveis, objeto da audiência de hoje; uma específica para o gás natural; e outra direcionada à revenda de combustíveis líquidos automotivos e de GLP. As duas últimas estão em discussão na ANP e passarão posteriormente por consultas e audiências públicas.<br>

A decisão da ANP de realizar novo processo público visou dar aos agentes de mercado e à sociedade em geral a oportunidade de se manifestarem mais uma vez, tendo em vista as alterações promovidas em relação à proposta original.<br>

As novas resoluções são resultado dos estudos iniciados no segundo semestre de 2018, sobre opções regulatórias para ampliar a transparência na formação de preços de derivados de petróleo, gás natural e biocombustíveis.<br>



</p>									
									<p><b>Fonte: Fonte: Assessoria de Comunicação da ANP e <a href="http://www.revistapetrus.com.br/anp-faz-audiencia-publica-sobre-primeira-resolucao-de-transparencia-nos-precos/">Revista Petrus</a>
</b></p>
									
								</div>
								<!-- Excerpt Ends --> 
							</article> 
							<!-- Article Ends -->
						</div>
						<!-- Latest Blog Posts Ends -->
					</div>
				</div>	
            </div>
        </section>
        <!-- Blog Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center">
            <!-- Container Starts -->
            <div class="container">
                <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/00001-53 </p>
                <p>
                    &copy; Copyright <?php echo date('Y');?> Wertco 
                </p>
                <!-- Copyright Text Ends -->
                <!-- Social Media Links Starts
                <div class="social-icons">
                    <ul class="social">
                        <li>
                            <a class="twitter" href="#" title="twitter"></a>
                        </li>
                        <li>
                            <a class="facebook" href="#" title="facebook"></a>
                        </li>
                        <li>
                            <a class="google" href="#" title="google"></a>
                        </li>
                        <li>
                            <a class="skype" href="#" title="skype"></a>
                        </li>
                        <li>
                            <a class="instagram" href="#" title="instagram"></a>
                        </li>
                        <li>
                            <a class="linkedin" href="#" title="linkedin"></a>
                        </li>
                        <li>
                            <a class="youtube" href="#" title="youtube"></a>
                        </li>
                    </ul>
                </div>
                <!-- Social Media Links Ends -->
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
        <!-- Back To Top Starts -->
        <div id="back-top-wrapper" class="hidden-xs">
            <p id="back-top">
                <a href="#top"><span></span></a>
            </p>
        </div>
        <!-- Back To Top Ends -->
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js')?>"></script>

    <!-- Live Style Switcher JS File - only demo -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/styleswitcher.js')?>"></script>

    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>

    <!-- Revolution Slider Initialization Ends -->
</body>


<!-- Mirrored from celtano.top/salimo/demos/blog-post.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
</html>