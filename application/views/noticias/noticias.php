<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from celtano.top/salimo/demos/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
<head>
    <meta charset="utf-8" />
    <title>Blog de Notícias - WERTCO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    -->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url('bootstrap/img/favicon.png'); ?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/font-awesome.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/magnific-popup.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/style.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/skins/yellow.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/bootstrap-select.min'); ?>" />
    <script type="text/javascript">
        var base_url = "<?php echo base_url();?>";
    </script>

    <!-- Template JS Files -->
    <script type="text/javascript" src="<?php echo base_url('bootstrap/js/modernizr.js'); ?>"></script>

</head>

<body class="double-diagonal blog-page dark">    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
                    <!-- Logo Starts -->
                    <div class="logo">
                        <a class="navbar-brand link-menu nav-external" href="index-slideshow.html">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?php echo base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png'); ?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            <!-- Logo Black Starts -->
                            <img id="logo-dark" class="logo-dark" src="<?php echo base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png'); ?>" alt="logo-dark" />
                            <!-- Logo Black Ends -->
                        </a>
                    </div>
                    <!-- Logo Ends -->
                    <!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span id="icon-toggler">
                          <span></span>
                          <span></span>
                          <span></span>
                          <span></span>
                        </span>
                    </button>
                    <!-- Hamburger Icon Ends -->
                    <!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external" href="<?php echo base_url('home/index'); ?>"><i class="fa fa-arrow-left"></i> Voltar ao Site</a></li>
                        </ul>
                    </div>
                    <!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>    
        <!-- Banner Starts -->
        <section class="banner">
            <div class="content text-center">
                <div class="text-center top-text">
                    <h1>BLOG DA WERTCO</h1>
                    <hr>
                    <h4></h4>
                </div>
            </div>
        </section>
        <!-- Banner Ends -->    
       <section id="blog" class="blog">
            <!-- Container Starts -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-8">
                        <!-- Blog Posts Starts -->
                        <div class="blog-content">
                             <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/exponline')?>">
                                    <h3>Exponline</h3>
                                </a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/exponline')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/NoticiasWertcoExponline.png')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>A Exponline Companytec está sendo um sucesso! 💥<br><br>
Muitos postos estão aproveitando a oportunidade para automatizar o posto, fazer um upgrade e para comprar as bombas mais modernas do mercado com preços inacreditáveis</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/exponline')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">
                                    <span class="date"><i class="fa fa-calendar"></i> 08 Setembro 2020</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                             <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/brasilTemMaiorNrlacradas')?>">
                                    <h3>Brasil tem maior número de bombas de combustível lacradas desde 2009</h3>
                                </a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/brasilTemMaiorNrlacradas')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/brasilTemMaiorNrlacradas.png')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>A quantidade de bombas de combustíveis lacradas nos postos brasileiros saltou para 1.153 no ano passado. O número de interdições, 79% superior ao contabilizado em 2018, é o maior...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/brasilTemMaiorNrlacradas')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">
                                    <span class="date"><i class="fa fa-calendar"></i> 04 Fevereiro 2020</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Starts -->
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/concursoPostoBonito')?>">
                                    <h3>Companytec e Wertco apoiam o XX concurso 2019 o posto mais bonito do brasil</h3>
                                </a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/concursoPostoBonito')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/noticiaPostoBonitoWertco.png')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>Promovido pela Revista Posto de Observação e Brascombustíveis (Associação Brasileira do Comércio Varejista de Combustíveis Automotivos e de Lubrificantes), com o apoio do Sincopetro...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/concursoPostoBonito')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">
                                    <span class="date"><i class="fa fa-calendar"></i> 02 Maio 2019</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Starts -->
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/WertcoeCompanytecExpopostos')?>">
                                    <h3>Companytec e Wertco marcam presença no Expopetro</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/WertcoeCompanytecExpopostos')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/WertcoeCompanytecExpopostos.png')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>A Companytec e a Wertco participarão do 20° Congresso de Revendedores de Combustiveis da Região Sul e Expopetro, programado para os dias 9 e 10 de outubro no Centro de Eventos do BarraShoppingSul...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/WertcoeCompanytecExpopostos')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">
                                    <span class="date"><i class="fa fa-calendar"></i> 02 Maio 2019</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Starts -->
                             <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/WertcoeAbiepsIniciam')?>">
                                    <h3>ANP faz audiência pública sobre primeira resolução de transparência nos preços</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/WertcoeAbiepsIniciam')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/AbiepsWertco.png')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>Com o objetivo de oferecer capacitação profissional na instalação e manutenção de suas bombas, a Wertco e a ABIEPS criaram um projeto direcionado aos técnicos e mecânicos de bombas de todo o país, atendendo a todos os requisitos das normas...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/WertcoeAbiepsIniciam')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">
                                    <span class="date"><i class="fa fa-calendar"></i> 02 Maio 2019</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Starts -->
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/anpFazAudienciaPublica')?>">
                                    <h3>ANP faz audiência pública sobre primeira resolução de transparência nos preços</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/anpFazAudienciaPublica')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/audiencia.jpg')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>A ANP realizou no dia 20/3 audiência pública sobre a minuta de resolução que dispõe sobre a transparência na formação de preços relativos à comercialização de derivados de petróleo e biocombustíveis por produtores, importadores e distribuidores...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/anpFazAudienciaPublica')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">
                                    <span class="date"><i class="fa fa-calendar"></i> 22 Março 2019</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Starts -->
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/petrobrasQuerLancarCartao')?>">
                                    <h3>Petrobras quer lançar cartão pré-pago para gasolina</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/petrobrasQuerLancarCartao')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/cartao-pre-pago.jpg')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>O presidente da Petrobras, Roberto Castello Branco, que assumiu a estatal em janeiro, sugeriu ao governo a adoção de um cartão pré-pago para os consumidores de combustíveis como forma de proteção contra a volatilidade dos preços...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/petrobrasQuerLancarCartao')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">
                                    <span class="date"><i class="fa fa-calendar"></i> 11 Janeiro 2019</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/petrobrasTeraAnoDeDefinicoes')?>">
                                    <h3>Petrobras terá ano de definições</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/petrobrasTeraAnoDeDefinicoes')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/petrobras.jpg')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>O ano de 2019 promete ser de muitas definições para a Petrobras. O novo presidente da companhia, Roberto Castello Branco, terá de lidar nos próximos meses com pautas importantes para o futuro da estatal. Cessão onerosa, abertura do refino...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/petrobrasTeraAnoDeDefinicoes')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">
                                    <span class="date"><i class="fa fa-calendar"></i> 11 Janeiro 2019</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Ends -->
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/etanolRecuaEm15Estados')?>">
                                    <h3>Etanol recua em 15 Estados, diz ANP; preço médio cai 0,04% no País</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/etanolRecuaEm15Estados')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/10.jpg')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>Os preços do etanol hidratado recuaram nos postos de 15 Estados brasileiros na semana passada, segundo levantamento da Agência Nacional do Petróleo, Gás Natural e Biocombustíveis (ANP) compilado pelo AE-Taxas. Em dez Estados e no Distrito Federal houve alta e no Amapá não foi feita avaliação...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/etanolRecuaEm15Estados')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">
                                    <span class="date"><i class="fa fa-calendar"></i> 11 Janeiro 2019</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Ends -->
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/saibaQuaisSaoAsAlteracoesNoRegulamentoTecnico')?>">
                                    <h3>Saiba quais são as alterações no regulamento técnico referente às bombas de combustíveis</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/saibaQuaisSaoAsAlteracoesNoRegulamentoTecnico')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/blogInmetro.jpg')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>Em 29 de junho de 2018, o Inmetro editou a Portaria 248/2018, publicada no Diário Oficial da União de 2 de julho, alterando alguns itens do Regulamento Técnico Metrológico (RTM) – aprovado pela Portaria Inmetro nº 559/2016 – que estabelece condições mínimas de segurança em bombas medidoras de combustíveis líquidos...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/saibaQuaisSaoAsAlteracoesNoRegulamentoTecnico')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">
                                    <span class="date"><i class="fa fa-calendar"></i> 18 Dezembro 2018</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Ends -->
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/anpPedeExplicacoesDeDistribuidoras')?>">
                                    <h3>ANP PEDE EXPLICAÇÕES DE DISTRIBUIDORAS SOBRE REPASSE DE CORTES DA GASOLINA AO CONSUMIDOR</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/anpPedeExplicacoesDeDistribuidoras')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/blogAnp.jpg')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>A Agência Nacional do Petróleo, Gás Natural e Biocombustíveis (ANP) pediu nesta terça-feira (27) que as principais distribuidoras de combustíveis esclareçam por que a redução do preço da gasolina nas refinarias não tem chegado para o consumidor final. As empresas terão um prazo de 15 dias para atender ao pedido.O preço médio da gasolina praticado...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/anpPedeExplicacoesDeDistribuidoras')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">
                                    <span class="date"><i class="fa fa-calendar"></i> 29 Novembro 2018</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Ends -->
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/nacs')?>"><h3>Companytec participa pela terceira vez da NACSSHOW e apresenta a Wertco</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/nacs')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/blogNacShow.jpg')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>Empresa líder em soluções integradas para o gerenciamento de postos apresenta sua nova empresa de bombas de combustíveis, a Wertco.
                                    Considerada a maior feira mundial especializada em postos de combustíveis e lojas de conveniência, a NACSSHOW 2018 - promovida pela The Association for Convenience & Fuel Retailing - acontece em Las Vegas de 07 a 10 de outubro, com mais de 1.600 empresas expositoras...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/nacs')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">                                    
                                    <span class="date"><i class="fa fa-calendar"></i>  06 Setembro 2018</span>
                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Ends -->
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/ascoval')?>"><h3>Wertco é case da Ascoval na Emerson Global Users Exchange Conference 2018</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/ascoval')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/blogAscoval.jpg')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>A Wertco e a Ascoval apresentarão durante a Emerson Global Users Exchange Conference, o projeto desenvolvido especialmente para atender a necessidade da Wertco de uma válvula solenoide confiável e de alta performance. A válvula foi desenvolvida para atender às necessidades de fluxo proporcional, taxas de pressão operacional, compatibilidade de fluido e características de montagem....</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/ascoval')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">                                    
                                    <span class="date"><i class="fa fa-calendar"></i>  06 Setembro 2018</span>
                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Ends -->
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/wertcoEaNovaEmpresaDeCombustiveisCompanytec')?>"><h3>Wertco é a nova empresa de bombas de combustíveis da Companytec</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/wertcoEaNovaEmpresaDeCombustiveisCompanytec')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/blogWertcoCompanytec.jpg')?>" alt="" />
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>Atendendo a uma necessidade do mercado, a Companytec marca líder no segmento de automação e controle para postos, investiu no segmento de bombas de combustíveis constituindo a empresa Wertco. A nova empresa já nasce com o know how, alta tecnologia, inovação, projeto e fabricação próprios e a qualidade inquestionável da marca Companytec...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/wertcoEaNovaEmpresaDeCombustiveisCompanytec')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">                                    
                                    <span class="date"><i class="fa fa-calendar"></i>15 Junho 2018</span>                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Ends -->                            
                            <!-- Article Starts -->
                            <article>
                                <a href="<?=base_url('noticias/wertcoIngressaNoMercadoBrasileiro')?>"><h3>WERTCO INGRESSA NO MERCADO BRASILEIRO</h3></a>
                                <!-- Figure Starts -->
                                <figure class="blog-figure">
                                    <a href="<?=base_url('noticias/wertcoIngressaNoMercadoBrasileiro')?>">
                                        <img class="img-fluid" src="<?php echo base_url('bootstrap/img/noticias/blogWertco.jpg')?>" alt="">
                                    </a>
                                </figure>
                                <!-- Figure Ends -->
                                <!-- Excerpt Starts -->
                                <div class="blog-excerpt">
                                    <p>A WERTCO é a nova empresa de capital Brasileiro que entra no mercado oferecendo uma linha moderna e completa de bombas para postos de combustíveis.
                                    A imagem altamente tecnológica e inovadora dos equipamentos é complementada pelos acessórios ELAFLEX, incluindo Bicos, Mangueiras e BreakAways...</p>
                                    <a class="custom-button readmore" href="<?=base_url('noticias/wertcoIngressaNoMercadoBrasileiro')?>">Leia Mais</a>
                                </div>
                                <!-- Excerpt Ends -->
                                <!-- Meta Starts -->
                                <div class="meta">                                    
                                    <span class="date"><i class="fa fa-calendar"></i>  06 Setembro 2018</span>
                                    
                                </div>
                                <!-- Meta Ends -->
                            </article>
                            <!-- Article Ends -->
                        </div>
                        <!-- Latest Blog Posts Ends -->                       
                    </div>                    
                </div>
            </div>
        </section>        
        <!-- Footer Section Starts -->
        <footer class="footer text-center">
            <!-- Container Starts -->
            <div class="container">
                <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/00001-53 </p>
                <p>
                    &copy; Copyright <?php echo date('Y'); ?> Wertco 
                </p>
                <!-- Copyright Text Ends -->
                <!-- Social Media Links Starts
                <div class="social-icons">
                    <ul class="social">
                        <li>
                            <a class="twitter" href="#" title="twitter"></a>
                        </li>
                        <li>
                            <a class="facebook" href="#" title="facebook"></a>
                        </li>
                        <li>
                            <a class="google" href="#" title="google"></a>
                        </li>
                        <li>
                            <a class="skype" href="#" title="skype"></a>
                        </li>
                        <li>
                            <a class="instagram" href="#" title="instagram"></a>
                        </li>
                        <li>
                            <a class="linkedin" href="#" title="linkedin"></a>
                        </li>
                        <li>
                            <a class="youtube" href="#" title="youtube"></a>
                        </li>
                    </ul>
                </div>
                <!-- Social Media Links Ends -->
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
        <!-- Back To Top Starts -->
        <div id="back-top-wrapper" class="hidden-xs">
            <p id="back-top">
                <a href="#top"><span></span></a>
            </p>
        </div>
        <!-- Back To Top Ends -->
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js')?>"></script>

    <!-- Live Style Switcher JS File - only demo -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/styleswitcher.js')?>"></script>

    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>

    <!-- Revolution Slider Initialization Ends -->
    </body>


<!-- Mirrored from celtano.top/salimo/demos/blog-post.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
</html>