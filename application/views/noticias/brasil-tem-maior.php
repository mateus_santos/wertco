<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Brasil tem maior número de bombas de combustível lacradas desde 2009</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <!-- Google Fonts 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	-->
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?=base_url('bootstrap/img/favicon1.png')?>">

    <!-- Template CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/font-awesome.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/magnific-popup.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/style.css')?>" />
	<link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/css/skins/yellow.css')?>" />    
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen" href="<?=base_url('bootstrap/css/skins/yellowgreen.css')?>" />
    
	
	 <!-- Revolution Slider CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/settings.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/layers.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('bootstrap/js/plugins/revolution/css/navigation.css')?>" />
	
	
    <!-- Template JS Files -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/modernizr.js')?>"></script>

</head>

<body class="double-diagonal blog-page blog-post">
    <!-- Preloader Starts -->
    <div class="preloader" id="preloader">
        <div class="logopreloader">
            <img src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-black">
        </div>
        <div class="loader" id="loader"></div>
    </div>
    <!-- Preloader Ends -->
    
    <!-- Page Wrapper Starts -->
    <div class="wrapper">
        <!-- Header Starts -->
        <header id="header" class="header">
            <div class="header-inner">
                <!-- Navbar Starts -->
                <nav class="navbar navbar-expand-lg p-0" id="singlepage-nav">
					<!-- Logo Starts -->
                     <div class="logo">
                        <a data-toggle="collapse" data-target=".navbar-collapse.show" class="navbar-brand link-menu scroll-to-target" href="#mainslider">
                            <!-- Logo White Starts -->
                            <img id="logo-light" class="logo-light" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-light" />
                            <!-- Logo White Ends -->
                            <!-- Logo Black Starts -->
                            <img id="logo-dark" class="logo-dark" src="<?=base_url('bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png')?>" alt="logo-dark" />
                            <!-- Logo Black Ends -->
                        </a>
                    </div>
					<!-- Logo Ends -->
					<!-- Hamburger Icon Starts -->
                    <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span id="icon-toggler">
						  <span></span>
						  <span></span>
						  <span></span>
						  <span></span>
						</span>
					</button>
					<!-- Hamburger Icon Ends -->
					<!-- Navigation Menu Starts -->
                    <div class="collapse navbar-collapse nav-menu" id="navbarSupportedContent">
                        <ul class="nav-menu-inner ml-auto">
                            <li><a class="link-menu nav-external" href="<?=base_url()?>"><i class="fa fa-home"></i> Voltar para o site</a></li>
                        </ul>
                    </div>
					<!-- Navigation Menu Ends -->
                </nav>
                <!-- Navbar Ends -->
            </div>
        </header>
		
		<!-- Blog Section Starts -->
        <section id="blog" class="blog">
            <!-- Container Starts -->
            <div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-8">
						<!-- Blog Posts Starts -->
						<div class="blog-content">
							<!-- Article Starts -->
							<article>
								<h3>Brasil tem maior número de bombas de combustível lacradas desde 2009</h3>
								<!-- Figure Starts -->
								<figure class="blog-figure">
										<img class="img-fluid" src="<?=base_url('bootstrap/img/noticias/brasilTemMaiorNrlacradas.png')?>" alt="">
								</figure>
								<!-- Figure Ends -->
								<!-- Excerpt Starts -->
								<div class="blog-excerpt">
									<p> A quantidade de bombas de combustíveis lacradas nos postos brasileiros saltou para 1.153 no ano passado. O número de interdições, 79% superior ao contabilizado em 2018, é o maior registrado desde 2009, quando o saldo de equipamentos lacrados foi 1.278. As informações foram obtidas junto à ANP (Agência Nacional do Petróleo, Gás Natural e Biocombustíveis) via Lei de Acesso à Informação.</p>

                                    <p>No período, o número de fiscalizações foi apenas 4,5% superior ao registrado em 2018. Diante dos dados, a ANP avalia que o aumento significativo no número de interdições de bombas de combustíveis ocorreu, principalmente, pelo novo Regulamento Técnico Metrológico, que passou a valer no dia 1º de janeiro do ano passado. “A portaria reduziu o limite de tolerância máxima no caso de erro contra o consumidor para 60 mililitros, na realização do teste no aferidor de 20 litros. Quando a favor do consumidor, o limite de tolerância se manteve em 100 mililitros. É importante ressaltar que devido à complexidade dos equipamentos medidores pequenas variações podem ocorrer, caso haja descuido na manutenção”, explica o órgão regulador.</p>

                                    <p>Entre as interdições do ano passado, quatro de cada 10 (40,42%) foram motivadas pela “afeição irregular” das bombas medidoras, cuja fiscalização é de responsabilidade do Inmetro (Instituto Nacional de Metrologia, Qualidade e Tecnologia) e dos Ipems (Institutos de Pesos e Medidas). A falta de segurança nas instalações (21,16%), a ausência de autorização para exercer atividade (17,09%) e gasolinas fora das especificações (6,94%) aparecem na sequência na lista de principais razões para as interdições. “Os fiscais da Superintendência de Fiscalização do Abastecimento testam, por meio de balde aferidor, se o equipamento medidor de combustível líquido está fornecendo o volume registrado”, destaca a ANP.</p>

                                    <p>Casos seja constatada alguma irregularidade na fiscalização, os postos de combustíveis podem ser punidos com multa, apreensão de bens e produtos, cancelamento do registro do produto, suspensão de fornecimento de produtos, suspensão de funcionamento, cancelamento de registro e revogação de autorização para o exercício de atividade.</p>

                                    <h3>Estados</h3>
                                    <p>Na análise por Unidade da Federação, São Paulo aparece no topo da lista, com 14,5% (168) das interdições de bombas realizadas no ano passado. O Estado do Sudeste, que concentra o maior número de estabelecimentos do país, é seguido por Rio Grande do Sul e Bahia.</p>

                                    <p>A ANP avalia que o elevado número de casos registrados no Rio Grande do Sul  pode ter sido originado pela demora na adaptação dos postos locais às novas normais de fiscalização. “Uma explicação plausível é que os revendedores de combustível da região tenham demorado a atentar para a alteração das normas, que gerou a necessidade de rigoroso acompanhamento e manutenção de seus equipamentos medidores.” “A partir do segundo semestre, o número de autuações caiu vertiginosamente, retornando aos percentuais históricos para essa irregularidade. Isso comprova, inclusive, a eficiência e a eficácia do trabalho da fiscalização”, afirma a ANP.</p>

                                    <p>Fonte: <a href="https://www.correiodopovo.com.br/not%C3%ADcias/economia/brasil-tem-maior-n%C3%BAmero-de-bombas-de-combust%C3%ADvel-lacradas-desde-2009-1.396963">Correio do Povo</a></p>                                    
								</div>
								<!-- Excerpt Ends --> 
							</article> 
							<!-- Article Ends -->
						</div>
						<!-- Latest Blog Posts Ends -->
					</div>
				</div>	
            </div>
        </section>
        <!-- Blog Section Ends -->
        <!-- Footer Section Starts -->
        <footer class="footer text-center">
            <!-- Container Starts -->
            <div class="container">
                <!-- Copyright Text Starts -->
                <p>Wertco Ind. Com. e Serviços de Manutenção em Bombas LTDA.<br/>CNPJ.: 27.314.980/00001-53 </p>
                <p>
                    &copy; Copyright <?=date('Y')?> Wertco 
                </p>
                <!-- Copyright Text Ends -->
                <!-- Social Media Links Starts
                <div class="social-icons">
                    <ul class="social">
                        <li>
                            <a class="twitter" href="#" title="twitter"></a>
                        </li>
                        <li>
                            <a class="facebook" href="#" title="facebook"></a>
                        </li>
                        <li>
                            <a class="google" href="#" title="google"></a>
                        </li>
                        <li>
                            <a class="skype" href="#" title="skype"></a>
                        </li>
                        <li>
                            <a class="instagram" href="#" title="instagram"></a>
                        </li>
                        <li>
                            <a class="linkedin" href="#" title="linkedin"></a>
                        </li>
                        <li>
                            <a class="youtube" href="#" title="youtube"></a>
                        </li>
                    </ul>
                </div>
                <!-- Social Media Links Ends -->
            </div>
            <!-- Container Ends -->
        </footer>
        <!-- Footer Section Starts -->
        <!-- Back To Top Starts -->
        <div id="back-top-wrapper" class="hidden-xs">
            <p id="back-top">
                <a href="#top"><span></span></a>
            </p>
        </div>
        <!-- Back To Top Ends -->
    </div>
    <!-- Wrapper Ends -->

    <!-- Template JS Files -->
	<script type="text/javascript" src="<?=base_url('bootstrap/js/jquery-2.2.4.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/bootstrap.bundle.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.bxslider.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.filterizr.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.magnific-popup.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('bootstrap/js/plugins/jquery.singlePageNav.min.js')?>"></script>

    <!-- Live Style Switcher JS File - only demo -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/styleswitcher.js')?>"></script>

    <!-- Main JS Initialization File -->
    <script type="text/javascript" src="<?=base_url('bootstrap/js/custom.js')?>"></script>

    <!-- Revolution Slider Initialization Ends -->
</body>


<!-- Mirrored from celtano.top/salimo/demos/blog-post.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Dec 2017 15:20:06 GMT -->
</html>