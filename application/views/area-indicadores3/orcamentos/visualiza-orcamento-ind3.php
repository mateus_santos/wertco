<div class="m-content">	
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon">
						<i class="flaticon-list-2"></i>
					</span>
					<h3 class="m-portlet__head-text">
						Orçamento #<?php echo $dados[0]->orcamento_id; ?> &nbsp;&nbsp; <?php if( $dados[0]->status == "aberto" ){
							echo '<button class="btn m-btn--pill m-btn--air btn-secondary" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Aberto </button>';
						}elseif( $dados[0]->status == "fechado" ){
							$status = "";
							echo '<button class="btn m-btn--pill m-btn--air btn-success" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Fechado </button>';
						}elseif( $dados[0]->status == "perdido" ){
							echo '<button class="btn m-btn--pill m-btn--air btn-danger" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Perdido </button>';
						}elseif( $dados[0]->status == "cancelado" ){
							
							echo '<button class="btn m-btn--pill m-btn--air btn-warning" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Cancelado </button>';
						}elseif( $dados[0]->status == "orçamento entregue" ){							
							echo '<button class="btn m-btn--pill m-btn--air btn-info" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Orçamento Entregue </button>';
						}else{
							echo '<button class="btn m-btn--pill m-btn--air btn-primary" id="status" status="'.$dados[0]->status.'" title="" data-toggle="m-tooltip" data-placement="top" data-original-title="Status do orçamento"> Em Negociação </button>';
							
						} ?>
					</h3>
				</div>			
			</div>
			<div class="m-portlet__head-tools">
				<ul class="m-portlet__nav">
					<!-- GERAR DESCONTO 
					<li class="m-portlet__nav-item">
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="geraDesconto" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>" data-toggle="modal" data-target="#m_modal_8" data-placement="top" title="Alterar Comissão">
							<i class="la la-dollar"></i>
						</a>	
					</li>-->
					<li class="m-portlet__nav-item">
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="" orcamento_id="<?php echo $dados[0]->id_orcamento; ?>" data-toggle="modal" data-target="#m_modal_10"  title="Gerar PDF do Orçamento">
							<i class="la la-print"></i>
						</a>	
					</li>
					<li class="m-portlet__nav-item">						
						<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" id="emitirOrcamento" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>" data-toggle="modal" data-target="#m_modal_7" title="Emitir orçamento ao cliente">
							<i class="la la-send"></i>
						</a>							
					</li>
				</ul>
			</div>
		</div>
		<div class="m-portlet__body">
			<?php if($dados[0]->origem != "Site" && $dados[0]->origem != ""){ ?>
			<div class="row">
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest"> Solicitante: </span>
						 <?php echo $solicitante[0]->nome.' | '.$solicitante[0]->razao_social.' | '.$solicitante[0]->cnpj;?>
					</h5>
				</div>							
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest"> Telefone: </span>
						<?php echo $solicitante[0]->telefone;?>
					</h5>
				</div>
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">E-mail: </span>
						<?php echo $solicitante[0]->email;?>
					</h5>
				</div>
				<div class="col-lg-3 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">Local: </span>
						<?php echo $solicitante[0]->local;?>
					</h5>
				</div>
			</div>
			<?php }else{ ?>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">Solicitante: </span>
						Orçamento realizado no site 
					</h5>
				</div>							
				
			</div>
			<?php } ?>
			<hr/>
			<div class="row">
				<div class="col-lg-4 col-xs-12"> 
					<h5><span class="m--font-warning m--font-boldest">Empresa/Cliente: </span>							
						<?php echo $dados[0]->cnpj.' | '.$dados[0]->razao_social.' | '.$dados[0]->fantasia;?>						
					</h5>
				</div>							
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">Telefone: </span>
						<?php echo $dados[0]->telefone;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5><span class="m--font-warning m--font-boldest">E-mail: </span>
						<?php echo $dados[0]->email;?>
					</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Enderço:</span>
						 <?php echo $dados[0]->endereco;?>
					</h5>
				</div>	
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Cidade:</span>
						 <?php echo $dados[0]->cidade;?>
					</h5>
				</div>
				<div class="col-lg-4 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Estado:</span>
						 <?php echo $dados[0]->estado;?>
					</h5>
				</div>							
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-12 col-xs-12">
					<h5>
						<span class="m--font-warning m--font-boldest">Contato do Posto:</span>
						 <?php echo $dados[0]->contato_posto;?>
					</h5>
				</div>	
			</div>
			
			
			<!--<div class="row">
				<div class="col-lg-12 col-xs-12">
					<h4>
						<span class="m--font-warning m--font-boldest">Indicador: </span>
						<?php if( count($indicador) > 0 ) {?>

							<?php echo $indicador['indicador'];?>

						<?php }else{ ?> 

							<?php echo "Não houve indicação para este orçamento"; ?>

						<?php } ?>	
					</h4>
				</div>											
			</div>-->
		</div>
	</div>
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	***************************************** Forma de entrega, fretes e forma de pagto *******************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">					
					<h2 class="m-portlet__head-label m-portlet__head-label--warning" style="width: 320px;">
						<span>
							Forma de Pagto, Entrega e Frete
						</span>
					</h2>
				</div>
			</div>			
		</div>
		<div class="m-portlet__body">
			<div class="form-group m-form__group">
				<label for="Frete">
					Forma de Pagamento
				</label>
				<select class="form-control m-input m-input--square entregaFretePagto" id="forma_pagto" name="forma_pagto" orcamento_id='<?php echo $dados[0]->orcamento_id; ?>' status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>
					<option value="0">Selecione a forma de pagamento</option>					
					<?php 	foreach ($formaPagto as $pagto) { ?>
						<option value="<?php echo $pagto->id;?>" <?php echo ( $dados[0]->forma_pagto_id == $pagto->id  ) ? 'selected="selected"' : '';?>><?php echo $pagto->descricao; ?></option>								
					<?php } ?>
				</select>
			</div>
			<div class="form-group m-form__group">
				<label for="Frete">
					Entrega
				</label>
				<select class="form-control m-input m-input--square entregaFretePagto" id="entregas" name="entrega" orcamento_id='<?php echo $dados[0]->orcamento_id; ?>' status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>
					<option value="0">Selecione como vai ser realizado a entrega do(s) produto(s)</option>					
					<?php foreach ($entregas as $entrega) { ?>
						<option value="<?php echo $entrega->id;?>"<?php echo ( $dados[0]->entrega_id == $entrega->id  ) ? 'selected="selected"' : '';?>><?php echo $entrega->descricao; ?></option>								
					<?php } ?>
				</select>
			</div>
			<div class="form-group m-form__group">
				<label for="Frete">
					Frete
				</label>
				<select class="form-control m-input m-input--square entregaFretePagto" id="frete" name="frete" orcamento_id='<?php echo $dados[0]->orcamento_id; ?>' status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>
					<option value="0">Selecione o tipo de frete utilizado</option>		
					<?php foreach ($fretes as $frete) { ?>
						<option value="<?php echo $frete->id;?>" <?php echo ( $dados[0]->frete_id == $frete->id  ) ? 'selected="selected"' : '';?>><?php echo $frete->descricao; ?></option>								
					<?php } ?>			
				</select>
			</div>
		</div>
	</div>	
	<!-- 
	*******************************************************************************************************************************
	*******************************************************************************************************************************	
	********************************************************* Produtos ************************************************************
	*******************************************************************************************************************************
	*******************************************************************************************************************************
	-->
	<div class="m-portlet m-portlet--creative m-portlet--bordered-semi" id="produtos_anchor">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">										
					<h2 class="m-portlet__head-label m-portlet__head-label--warning">
						<span>Produtos</span>
					</h2>
				</div>			
			</div>				
		</div>
		<div class="m-portlet__body">
			<table class="table m-table m-table--head-separator-warning">
			  	<thead>
			    	<tr>
			      		<th>#</th>
			      		<th>Código | Modelo | Descrição</th>			      		
			      		<th>Quantidade</th>			      		
			      		<th style="text-align: right;">Valor Unitário R$</th>
			      		<th style="text-align: right;">
			      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" data-toggle="modal" data-target="#m_modal_9"  title="Adicionar produto">
								<i class="la la-plus"></i>
							</a>
						</th>	
			    	</tr>
			  	</thead>
			  	<tbody total_indice='<?php echo count($dados); ?>'>
			  		<?php $i=1;$total=0; foreach($dados as $dado){ ?>
			    	<tr indice='<?php echo $i; ?>' class="novo_produto_orc">
				      	<th scope="row"><?php echo $i;?></th>
				      	<td>
				      		<?php 
                                    $bombas_op     =   "";
                                    $opcionais_op  =   "";
                                    $bombas_op_slim  =   "";
                                    $bombas_op_baixa  =   "";

                                    foreach( $bombas as $bomba )
                                    {
                                        if( $bomba->tipo_produto_id == 4 ){
                                        	if( $dado->produto_id == $bomba->id ){
                                        		$selected = 'selected="selected"';
                                        		$valor_base = $bomba->valor_unitario;
                                        		$tipo = 'opcionais';

                                        	}else{
                                        		$selected = '';
                                        	}

                                            $opcionais_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'".$selected." tipo='opcionais'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif($bomba->tipo_produto_id == 1){
                                        	if( $dado->produto_id == $bomba->id ){
                                        		$selected = 'selected="selected"';
                                        		$valor_base = $bomba->valor_unitario;
                                        		$tipo = 'bomba_baixa';
                                        	}else{
                                        		$selected = '';
                                        	}
                                            $bombas_op_baixa.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'".$selected." tipo='bomba_baixa'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif($bomba->tipo_produto_id == 2){

                                        	if( $dado->produto_id == $bomba->id ){
                                        		$selected = 'selected="selected"';
                                        		$valor_base = $bomba->valor_unitario;
                                        		$tipo = 'bomba_baixa_slim';
                                        	}else{
                                        		$selected = '';
                                        	}
                                            $bombas_op_slim.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'".$selected." tipo='bomba_baixa_slim'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif ($bomba->tipo_produto_id == 3){
                                        	if( $dado->produto_id == $bomba->id ){
                                        		$selected = 'selected="selected"';
                                        		$valor_base = $bomba->valor_unitario;
                                        		$tipo = 'bomba_alta';
                                        	}else{
                                        		$selected = '';
                                        	}
                                            $bombas_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."'".$selected." tipo='bomba_alta'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }    
                                    } 
                            ?>
                            <select  class="form-control bombas" style="font-size: 15px;" name="produto[]" indice='<?php echo $i;?>' orcamento_produto_id="<?php echo $dado->orcamento_produto_id;?>" id="bombas_<?php echo $i;?>">
                            	<option value="">Selecione um produto</option>
                                <optgroup label="Bombas Mangueira Alta" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op; ?>
                                </optgroup>    
                                <optgroup label="Bombas Mangueira Slim" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op_slim; ?>
                                </optgroup>    
                                <optgroup label="Bombas Mangueira Baixa" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op_baixa; ?>
                                </optgroup>
                                <optgroup label="Opcionais" data-subtext="optgroup subtext">
                                    <?php echo $opcionais_op; ?>
                                </optgroup>
                            </select>

				      	</td>
				      	<td>
				      		<input type="text" name="qtd[]" indice='<?php echo $i;?>' class='form-control qtd' orcamento_produto_id="<?php echo $dado->orcamento_produto_id;?>" value='<?php echo $dado->qtd; ?>' style="width: 50px;    float: left;" maxlength="3" id="qtd_<?php echo $i;?>" disabled='disabled' />
				      		<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill editar_qtd" data-toggle="m-tooltip"  data-original-title="Editar Quantidade" style="float: right;margin-top: -5px;"  indice='<?php echo $i;?>'>
								<i class="la la-edit"></i>
							</button>
				      	</td>
				      	<td style="text-align: right;">
				      		<b class="valor_unitario valor_unitario_<?php echo $i;?>" valor_base="<?php echo $valor_base; ?>" orcamento_produto_id='<?php echo $dado->orcamento_produto_id; ?>' orcamento_id = "<?php echo $dados[0]->orcamento_id;?>" tipo='<?php echo $tipo; ?>'><?php echo $dado->valor; ?></b>

							<input type="hidden" name="valor[]" id="valor_produto_<?php echo $i;?>" class="valor_produto" value="<?php echo $dado->valor; ?>" required="required" maxlength="14" disabled="disabled" style="width: 71%;text-align: right;padding: 8px;float: right;" qtd="<?php echo $dado->qtd; ?>">	
				      	</td>
				      	<td colspan="2" style="text-align: right;">
				      		<i class="fa fa-minus excluir_produto" data-toggle="m-tooltip"  data-original-title="Excluir Produto" onclick="excluirProduto(<?php echo $dado->orcamento_produto_id; ?>,<?php echo $i; ?>,<?php echo $dados[0]->orcamento_id; ?>);" style="color: red;font-size: 16px; cursor: pointer; margin-top: 10px;" indice='0'></i>
				      	</td>
			    	</tr>
			    	<?php $i++; $total = ($dado->qtd * $dado->valor) + $total; } ?>
			    	<tr>			    		
			    		<td></td>
						<td></td>
						<td></td>
						<!-- <?php echo $dados[0]->valor_tributo."%";?> de ICMS + 5% de IPI + <?php echo $desconto[0]['valor_desconto'] ."%";?> de Comissão -->
			    		<td><b style="text-align: right;">Total: </b></td>			    		
			    		<td style="text-align: right;">R$ <b class="valor_subtotal" style="margin-right: 10px;"><?php echo number_format($total,2,",",".");?> </b>
			    			<!--<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill atualiza_valor" data-toggle="m-tooltip"  data-original-title="Atualizar Valores" style="float: right;margin-top: -5px;"  indice='<?php echo $i;?>'>
								<i class="la la-refresh"></i>
							</button>-->
							<input type="hidden" name="valor_total" id="valor_total"	 	value="<?php echo $total;?>" />
			    			<input type="hidden" name="valor_tributo" id="valor_tributo" 	value="<?php echo $dados[0]->valor_tributo; ?>">
			    			<input type="hidden" name="orcamento_id" id="orcamento_id" 		value="<?php echo $dados[0]->orcamento_id; ?>">
			    			<input type="hidden" name="status_id" id="status_id" 			value="<?php echo $dados[0]->status_orcamento_id; ?>">
			    			<input type="hidden" name="comissao" id="comissao" 				value="<?php echo (count($desconto) > 0) ? $desconto[0]['valor_desconto'] : '' ?>">
			    			<input type="hidden" name="fator" id="fator" value="<?php echo $dados[0]->fator; ?>" />
			    			  
			    		</td>
			    	</tr>
			  	</tbody>
			</table>
			 <div style="text-align: center;">
                    <div class="m-radio-inline">
                    	<label>Selecione a comissão: &nbsp;</label>
						<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="3% de comissão" class="comissao">
							<input type="radio" name="valor_desconto_orc" value="3" class="comissao" <?php echo ($desconto[0]['valor_desconto'] == '3.00') ? 'checked=checked' : '';  ?>/>
							3%  
							<span></span>
						</label>
						<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="4% de comissão" class="comissao">
							<input type="radio" name="valor_desconto_orc" value="4" class="comissao" <?php echo ($desconto[0]['valor_desconto'] == '4.00') ? 'checked=checked' : ''; ?>/>
							4%
							<span></span>
						</label>
						<label class="m-radio" data-toggle="m-tooltip" data-placement="top" title="5% de comissão" >
							<input type="radio" name="valor_desconto_orc" class="comissao" value="5" <?php echo ($desconto[0]['valor_desconto'] == '5.00') ? 'checked=checked' : ''; ?> />
							5%
							<span></span>
						</label>
					</div>
					
                    
                </div>       
		</div>
	</div>
</div>	
<!-- Modal inserir representante e contato -->
<div class="modal fade" id="m_modal_10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Dados Contato do posto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">																
					<div class="col-lg-12">
						<label>Contato do posto:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="text" required="" name="contato_posto" id="contato_posto" value="<?php echo $dados[0]->contato_posto;?>" class="form-control m-input" placeholder="" style="" maxlength="" >
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-user"></i></span></span>
						</div>
					</div>	
				</div>										
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="geraPdf" orcamento_id="<?php echo $dados[0]->id_orcamento; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>Enviar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Emitir orçamento -->
<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Emitir orçamento para cliente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">						
					<div class="col-lg-12">
						<label>Enviar para o e-mail:</label>
						<div class="m-input-icon m-input-icon--right">
							<input type="email" required="" name="email_destino" id="email_destino" class="form-control m-input" placeholder="" style="text-align: right;"  value="<?php echo $dados[0]->email;?>">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-envelope"></i></span></span>
						</div>							
					</div>
					
				</div>
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="emitir_orcamento" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>Enviar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Desconto -->
<div class="modal fade" id="m_modal_8" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Selecionar Comissão</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 750px;">
				<div class="form-group m-form__group row">											
					<div class="col-lg-6">
						<label>Comissão:</label>
						<div class="m-input-icon m-input-icon--right">
							<input id="valor_promo_orc" name="valor_promo_orc" data-slider-tooltip="show" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="20" data-slider-step="1" data-slider-value="<?php if( count($desconto) > 0) echo $desconto[0]['valor_desconto']; else echo 0; ?>"/>
							<input type="text" required="" name="valor_promo_orc" id="valor_promo_orc" value="<?php if( count($desconto) > 0) echo $desconto[0]['valor_desconto']; ?>" class="form-control m-input" placeholder="" style="text-align: right;" maxlength="5">
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="fa fa-percent"></i></span></span>
							
						</div>							
					</div>
					<div class="col-lg-6">
						<label>Motivo do desconto:</label>
						<div class="m-input-icon m-input-icon--right">							
							<textarea name="motivo_desconto_orc" id="motivo_desconto_orc" class="form-control m-input" disabled="disabled"><?php if( count($desconto) > 0) echo $desconto[0]['motivo_desconto']; ?></textarea>
							<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-comment"></i></span></span>
						</div>							
					</div>
				</div>							
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="adicionar_desconto_orc" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>">Enviar</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal Adiciona produto -->
<div class="modal fade" id="m_modal_9" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title modal-andamento-title">Adicionar Produto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="width: 850px;">
				<div class="m-portlet__body">
					
					<form id="produtos_novos">
					<table class="table m-table m-table--head-separator-warning novo">
						
				  	<thead>
				    	<tr>				      		
				      		<th>Código | Modelo | Descrição</th>			      		
				      		<th>Quantidade</th>			      		
				      		<th style="text-align: right;">Valor Unitário R$</th>
				      		<th>
				      			<a class="btn btn-outline-warning m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air add" data-toggle="m-tooltip" data-placement="top" title="Adicionar produto">
									<i class="la la-plus"></i>
								</a>
							</th>	
				    	</tr>
				  	</thead>
				  	<tbody >
			  		<?php $i=0;?>
			    	<tr id="modelo" indice='0' total_indice='0'>	
				      	<td>
				      		<?php 
                                    $bombas_op     =   "";
                                    $opcionais_op  =   "";
                                    $bombas_op_slim  =   "";
                                    $bombas_op_baixa  =   "";

                                    foreach( $bombas as $bomba )
                                    {
                                        if( $bomba->tipo_produto_id == 4 ){
                                        	$opcionais_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='opcionais'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif($bomba->tipo_produto_id == 1){
                                        	
                                            $bombas_op_baixa.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='bomba'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif($bomba->tipo_produto_id == 2){                                        	
                                            $bombas_op_slim.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='bomba'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }elseif ($bomba->tipo_produto_id == 3){
                                        	$bombas_op.="<option value='".$bomba->id."' valor_unitario='".$bomba->valor_unitario."' tipo='bomba'>".$bomba->codigo." | ".$bomba->modelo." | ".$bomba->descricao."</option>";

                                        }    
                                    } 
                            ?>
                            <select  class="form-control bombas_new" style="font-size: 12px;" name="produto_id" indice='0' id="bombas_0">
                            	<option value="">Selecione um produto</option>
                                <optgroup label="Bombas Mangueira Alta" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op; ?>
                                </optgroup>    
                                <optgroup label="Bombas Mangueira Slim" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op_slim; ?>
                                </optgroup>    
                                <optgroup label="Bombas Mangueira Baixa" data-subtext="optgroup subtext">
                                    <?php echo $bombas_op_baixa; ?>
                                </optgroup>
                                <optgroup label="Opcionais" data-subtext="optgroup subtext">
                                    <?php echo $opcionais_op; ?>
                                </optgroup>
                            </select>

				      	</td>
				      	<td>
				      		<input type="text" name="qtd" indice='0' class='form-control'  maxlength="3" id="qtd_new_0" style="width: 50px;"  />
				      		
				      	</td>
				      	<td>				      		
							<b class="valor_unitarios_text valor_unitarios_text_0" ></b>
							<input type="hidden" name="valor_unitarios" id="valor_unitarios_0" class="valor_unitarios form-control" required="required" maxlength="14" orcamento_id = "<?php echo $dados[0]->orcamento_id;?>"/>
				      	</td>
				      	<td>
                        	<i class="fa fa-minus remover" style="color: red;font-size: 24px; cursor: pointer;display: none;" indice='0'></i>
                        </td>
			    	</tr>		    	
			    	
			  		</tbody>
					</table>
					 
				</div>										
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>					
				<button type="button" class="btn btn-info" id="adicionar_produto" orcamento_id="<?php echo $dados[0]->orcamento_id; ?>" status_id='<?php echo $dados[0]->status_orcamento_id; ?>'>Enviar</button>
			</div>
			</form>	
		</div>
	</div>
</div>