<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px;
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}

table td{		
	font-size: 9px;	
}



</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVI�OS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
										Av. Get�lio Vargas, 280 - Jd. �ngelo<br>
										07400-230 - Aruj� - SP<br>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br>
			</p>
		</div>
		<div class="corpo">				
		<h3 style="text-align: center;margin-top: 15px;	">CHAMADOS DE ASSISTENCIA T�CNICA</h3>	

		<table class="table" cellspacing="0" style="margin-top: 15px;">
			<thead bgcolor="#ccc" style="background-color: #ccc;font-size: 10px;">
				<tr>
					<th>#</th>					
					<th>CLIENTE</th>
					<th>CONTATO DO CLIENTE</th>
					<th>TIPO</th>					
					<th>INICIO - FIM</th>
					<th>USU�RIO</th>
					<th>STATUS</th>
				</tr>
			</thead>
			<tbody>

				<?php $i=0; foreach( $chamados as $chamado ){ 
					if( $chamado['fim'] != '' ){
						$fim = date('d/m/Y',strtotime($chamado['fim']));
					}else{
						$fim = "";
					}

					?>
				<tr bgcolor="<?php echo (($i%2) == 0) ? '#f0f0f0' : '#ffffff'; ?>" >
					<td cellspacing="0" cellpadding="0" style="text-align: center;"><?php echo $chamado['id']; 	?>	</td>					
					<td cellspacing="0" style="text-align: left;"><?php echo utf8_decode(strtoupper($chamado['cliente']));	?>	</td>
					<td cellspacing="0"  style="text-align: center;"><?php echo utf8_decode(strtoupper($chamado['contato']));?> 				</td>
					<td cellspacing="0" style="text-align: center;"><?php echo utf8_decode($chamado['tipo']);?>					</td>
					<td style="text-align: center;"><?php echo date('d/m/Y', strtotime($chamado['inicio'])).' - '.$fim; ?>					</td>
					<td cellspacing="0" style="text-align: center;"><?php echo utf8_decode(strtoupper($chamado['usuario'])); ?>				</td>
					<td cellspacing="0" style="text-align: center;"><?php echo utf8_decode($chamado['status']); ?>				</td>				
				</tr>
				<?php $i++; } ?>				
			</tbody>
		</table>
		
	</div>	
</div>
</body>
</html>