<html lang="pt-br"><head>
	<meta charset="utf-8">
	<title></title>

<style type="text/css">

body {
 	background-color: #fff;
 	margin: 0 auto;
 	position: relative;
 	font-family: Lucida Grande, Verdana, Sans-serif;
 	font-size: 12px;
 	color: #333;
 	width: 100%;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 	color: #444;
 	background-color: transparent;
 	border-bottom: 1px solid #D0D0D0;
 	font-size: 14px; 
 	font-weight: bold;
 	margin: 24px 0 2px 0; 	
}
.cabecalho{
	margin: 0 auto;
	position: relative;
}
table{	
	padding: 0;
	width: 100%;
	border: 1px solid #ffcc00;
	
}
table tr{	
	
}
table td{
	padding: 2px !important;
	border-bottom: 1px solid #f0f0f0;
	border-right: 1px solid #f0f0f0;
	font-size: 9px;	
}


</style>
</head>
<body>
	<div class="conteudo" style="padding: 0px;">
		<div class="cabecalho">			
			<img src="./bootstrap/img/styleswitcher/logos/logos-dark/wertcofundoescuro1.png" style="width: 150px;margin: 5px auto;">
			<p style="text-align: right; font-size: 8px;margin-top: -10px;"> WERTCO IND. COM. E SERVI�OS DE MANUT. EM BOMBAS LTDA.<br>
										CNPJ: 27.314.980/0001-53 | IE: 188.103.734.119<br>
										Av. Get�lio Vargas, 280 - Jd. �ngelo<br>
										07400-230 - Aruj� - SP<br>
										Tel: +55 11 3900-2565 | www.wertco.com.br<br>
			</p>
		</div>
		<div class="corpo">	
			<h3 style="text-align: center;margin-top: 15px;	">CHAMADO DE ASSIST�NCIA T�CNICA </h3>		
			<table cellspacing="0" style="margin-top: 15px; margin-bottom: 0;">
				<tbody >
					<tr>
						<?php $fim = ( $chamados[0]['fim'] != '' ) ? date('d/m/Y',strtotime($chamados[0]['fim'])) : '';?>
						<td style=" width: 15% !important;">CHAMADO N�: </td>
						<td><?php echo $chamados[0]['id']; ?></td>
						<td style=" text-align: right;width: 20% !important;">CRIA��O | FINALIZA��O: </td>
						<td style=" width: 20% !important;"><?php echo date('d/m/Y',strtotime($chamados[0]['inicio'])).' | '.$fim; ?> </td>
						<td style="text-align: right;width: 15% !important;">USU�RIO CRIA��O: </td>
						<td style="width: 20% !important;"><?php echo utf8_decode(strtoupper($chamados[0]['nome'])); ?></td>

					</tr>
				</tbody>
			</table>
			<table cellspacing="0">	
				<tbody>
					<tr>
						<td style=" width: 15% !important;">CLIENTE: </td>
						<td colspan="5"><?php echo utf8_decode(strtoupper($chamados[0]['cliente'])); ?> </td>					
					</tr>
				</tbody>
			</table>
			<table>
				<tbody>
					<tr>
						<td style="width: 15% !important;">CONTATO:</td>
						<td style="width: 30%;"><?php echo utf8_decode(strtoupper($chamados[0]['contato'])); ?></td>					
						<td style="width: 10%; text-align: right;">CIDADE | UF:</td>					
						<td style="width: 30%; text-align: left;"><?php echo utf8_decode(strtoupper($chamados[0]['cidade']).' | '.$chamados[0]['estado']); ?></td>					
						<td style="width: 5%; text-align: right;">CEP:</td>					
						<td style="width: 10%; text-align: left;"><?php echo $chamados[0]['cep']; ?></td>
					</tr>
				</tbody>
			</table>
			<table>
				<tbody>
					<tr>
						<td>SITUA��O: </td>
						<td style=" width: 30% !important;text-align: left;"><?php echo utf8_decode(strtoupper($chamados[0]['status'])); ?></td>
						<td style="  width: 10% !important;text-align: right;">TIPO: </td>					
						<td colspan="3" style=" width: 45% !important; text-align: left;"><?php echo utf8_decode(strtoupper($chamados[0]['tipo'])); ?></td>				
					</tr>
				
				</tbody>
			</table>	

			<hr style="border: 1px solid #cccccc; background-color: #cccccc;" />							
		<h3 style="text-align: center;margin-top: 15px;	">ATIVIDADES REALIZADAS</h3>
		<table class="table" cellspacing="0" style="margin-top: 15px;">
			<thead bgcolor="#ccc" style="background-color: #ccc;font-size: 10px;">
				<tr>
					<th>#</th>					
					<th>DEFEITO</th>
					<th>MODELO BOMBA</th>
					<th>N�MERO DE SERIE</th>
					<th>STATUS</th>
					<th>INICIO - FIM</th>
				</tr>
			</thead>
			<tbody>

				<?php $i=1; foreach( $chamados as $atividade ){ 
					if( $atividade['fim_atividade'] != '' ){
						$fim = date('d/m/Y',strtotime($atividade['fim_atividade']));
					}else{
						$fim = "";
					}

					?>
				<tr>
					<td cellspacing="0" cellpadding="0" style="text-align: center;"><?php echo $i;?></td>					
					<td cellspacing="0" style="text-align: center;"><?php echo utf8_decode(strtoupper($atividade['defeito']));?></td>
					<td cellspacing="0"  style="text-align: center;"><?php echo $atividade['modelo'];?> </td>
					<td cellspacing="0" style="text-align: center;"><?php echo $atividade['numero_serie'];?></td>
					<td style="text-align: center;"><?php echo utf8_decode($atividade['status_atividade']); ?></td>
					<td cellspacing="0" style="text-align: center;"><?php echo date('d/m/Y', strtotime($atividade['inicio_atividade'])).' - '.$fim;?></td>				
				</tr>
				<?php $i++; } ?>				
			</tbody>
		</table>

		<hr style="border: 1px solid #cccccc; background-color: #cccccc;" />	
		<?php if(isset($tecnicos[0])) { ?>
		<h3 style="text-align: center;margin-top: 15px;	">T�CNICOS</h3>	

		<table class="table" cellspacing="0" style="margin-top: 15px;">
			<thead bgcolor="#ccc" style="background-color: #ccc;font-size: 10px;">
				<tr>
					<th>#</th>					
					<th>RAZ�O SOCIAL</th>
				</tr>
			</thead>
			<tbody>

				<?php $i=1; foreach( $tecnicos as $tecnico ){ 	?>
				<tr>
					<td cellspacing="0" cellpadding="0" style="text-align: center;"><?php echo $i;?></td>					
					<td cellspacing="0" style="text-align: center;"><?php echo utf8_decode(strtoupper($tecnico['razao_social'].' - '. $tecnico['cnpj']));?></td>
					
				</tr>
				<?php $i++; } ?>				
			</tbody>
		</table>
		<hr style="border: 1px solid #cccccc; background-color: #cccccc;" />
		<?php } if( isset($ops[0]) ) {	?>
		<h3 style="text-align: center;margin-top: 15px;	">ORDEM DE PAGAMENTO VINCULADA AO CHAMADO #<?php echo $chamados[0]['id']; ?></h3>	
		<table class="table" cellspacing="0" style="margin-top: 15px;">
			<tr>
				<td>Favorecido: 	<?php echo utf8_decode(strtoupper($ops[0]['razao_social'])); ?> </td>		
				<td>Emiss�o:	 	<?php echo date('d/m/Y H:i:s', strtotime($ops[0]['dt_emissao'])); ?> </td>
			</tr>
		</table>

		<table class="table" cellspacing="0" style="margin-top: 15px;">
			<thead bgcolor="#ccc" style="background-color: #ccc;font-size: 10px;">
				
				<tr>
					<th>#</th>					
					<th>DESPESA</th>
					<th>QTD</th>
					<th>VALOR</th>
					<th>TOTAL</th>					
				</tr>
			</thead>
			<tbody>

				<?php 
					$i=1;
					$total = 0; 
					foreach( $ops as $op ){
			 	?>
						<tr>
							<td cellspacing="0" cellpadding="0" style="text-align: center;"><?php echo $i;?></td>		
							<td cellspacing="0" style="text-align: center;"><?php echo utf8_decode(strtoupper($op['despesa']));?></td>
							<td cellspacing="0"  style="text-align: center;"><?php echo $op['qtd'];?> </td>
							<td cellspacing="0" style="text-align: center;"><?php echo number_format($op['valor'], 2, ',', '.');?></td>
							<td style="text-align: center;"><?php echo number_format($op['valor']*$op['qtd'], 2, ',', '.'); ?></td>					
						</tr>
				<?php 	
						$i++; 
						$total = $total + ( $op['valor'] * $op['qtd'] );
					} 
				?>				
				<tr>
					<td colspan="5" style="text-align: right;">
						<b>TOTAL: R$ <?php echo number_format($total, 2, ',', '.'); ?></b>
					</td>
				</tr>
			</tbody>
		</table>
		<?php } ?>
	</div>	
</div>
</body>
</html>