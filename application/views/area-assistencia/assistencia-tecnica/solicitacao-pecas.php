<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head" >
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						<?=$titulo?> &nbsp;&nbsp;

					</h3>
					<!--<div class="m-portlet__head-caption cadastrar_orcamento">													
						<a href="<?php echo base_url('AreaAdministrador/cadastraPedidos')?>" class="" style="color: #ffcc00; font-weight: bold;" >
							<i class="la la-plus-circle" style="font-size: 38px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar novo pedido"></i>
						</a>
					</div>-->
				</div>
				
			</div>
			
			<div class="m-list__content">
                <div class="m-list-badge" style="margin-top: 25px;width: 80%;margin-left: auto;margin-right: auto;">
                	<span class="m-list-badge__item"><span class="m-badge m-badge--secondary"></span></span>&nbsp;Solicitação Realizada&nbsp;&nbsp;					
					<span class="m-list-badge__item"><span class="m-badge m-badge--success"></span></span>&nbsp;Peças Enviadas&nbsp;&nbsp;
					<span class="m-list-badge__item"><span class="m-badge m-badge--danger"></span></span>&nbsp;Solicitação Cancelada&nbsp;&nbsp;
					<span class="m-list-badge__item"><span class="m-badge m-badge--warning"></span></span>&nbsp;Peças Entregue&nbsp;&nbsp;
					<span class="m-list-badge__item"><span class="m-badge m-badge--info"></span></span>&nbsp;Solicitação Finalizada&nbsp;&nbsp;
					<span class="m-list-badge__item"><span class="m-badge m-badge--info" style="background: #f6ff6e;"></span></span>&nbsp;Peças disponíveis para envio&nbsp;&nbsp;
				</div>
			</div>		
			<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: -25px;" data-toggle="m-tooltip" data-placement="top" title="Tour Virtual" id="tour_virtual">
				<i class="la la-info"></i>
			</a>
		
		</div>
		<div class="m-portlet__body m-datatable m-datatable--default m-datatable--brand m-datatable--loaded" >			
			<!--begin: Datatable -->
			<table class="" id="html_table" width="100%">
				<thead>
					<tr>
						<th style="text-align: center;"># Solicitação</th>
						<th style="text-align: center;"># Chamado</th>
						<th style="text-align: center;">Cliente</th>
						<th style="text-align: center;">Descrição/Obs</th>										
						<th style="text-align: center;">Situação</th>
						<th style="text-align: center;">Andamento</th>
						<th style="text-align: center;">Dthr. Solicitação</th>
						<th style="text-align: center;">Ações</th>
					</tr>
				</thead>
				<tbody>		

					<?php 	foreach($dados as $dado){
						$status = "";
						$background = "";
						if( $dado['status_id'] == 1 ){
							$status = "btn m-btn--pill m-btn--air btn-secondary";
							$background = '';
						}elseif( $dado['status_id'] == 2 ){
							$status = "btn m-btn--pill m-btn--air btn-info";
							$background = '';
						}elseif( $dado['status_id'] == 3 ){
							$status = "btn m-btn--pill m-btn--air btn-primary";
							$background = '';
						}elseif( $dado['status_id'] == 4 ){
							$status = "btn m-btn--pill m-btn--air btn-success";
							$background = "";
						}elseif( $dado['status_id'] == 5 ){
							$status = "btn m-btn--pill m-btn--air btn-danger";
							$background = "";
						}elseif( $dado['status_id'] == 6 ){
							$status = "btn m-btn--pill m-btn--air btn-warning";
							$background = "";
						}elseif( $dado['status_id'] == 7 ){
							$status = "btn m-btn--pill m-btn--air btn-info";
							$background = "";
						}elseif( $dado['status_id'] == 8 ){
							$status = "btn m-btn--pill m-btn--air btn-info";
							$background = "";
						}elseif( $dado['status_id'] == 9 ){
							$status = "btn m-btn--pill m-btn--air btn-warning";
							$background = "";
						}elseif( $dado['status_id'] == 10 ){
							$status = "btn m-btn--pill m-btn--air";
							$background = "style='background: #f6ff6e;'";
						}else{
							$status = "btn m-btn--pill m-btn--air btn-success";
							$background = "";
						}

					?>
						<tr>
							<td style="text-align: center;"><?php echo $dado['id']; ?></td>
							<td style="text-align: center;"> <?php echo $dado['chamado_id']; ?></td>
							<td style="text-align: center;"><?php echo $dado['cliente']; ?></td>		
							<td style="text-align: center;"><?php echo $dado['descricao']; ?></td>
							<td style="text-align: center; text-transform: capitalize;">
								<button class="status <?php echo $status; ?>" status="<?php echo $dado['status_id'];?>" <?php echo $background;?> title="Status da solicitação" onclick="status(<?php echo $dado['id']; ?>,'<?php echo str_replace("'"," ",$dado['cliente']); ?>', <?php echo $dado['chamado_id']; ?>);">
									<?php echo ucfirst($dado['status']); ?>								
								</button>
							</td>
							<td style="text-align: center;">
								<button class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air andamento_tour">
									<i title="Andamento da Solicitação" onclick="andamento(<?php echo $dado['id']; ?>,<?php echo $dado['status_id']; ?>);" class="flaticon-list-3 andamento" style="cursor: pointer;" ></i>
								</button>
							</td>	
							<td style="text-align: center;">
								<?php echo date('d/m/Y', strtotime($dado['dthr_solicitacao'])); ?>							
							</td>																				
							<td data-field="Actions" class="m-datatable__cell " style="text-align: center !important;">
								<span style="overflow: visible;" class="">
									<a href="<?php echo base_url('AreaAssistencia/visualizarSolicitacaoPecas/'.$dado['id']);?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" solicitacao_id="<?php echo $dado['id']; ?>" >
										<i class="la la-eye visualizar"></i>
									</a>
									<a href="#" onclick="anexarNfe(<?php echo $dado['id']; ?>,'<?php echo str_replace("'","*",$dado['cliente']); ?>','<?php echo $dado['nr_nf']; ?>','<?php echo $dado['arquivo_nfe']; ?>','<?php echo date('d/m/Y', strtotime($dado['dthr_envio'])); ?>');" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill finalizar_pedido" solicitacao_id="<?php echo $dado['id']; ?>" cliente="<?php echo $dado['cliente']; ?>" >
										<i class="flaticon-tool-1"></i>
									</a>
									<a href="#" onclick="anexarRastreio(<?php echo $dado['id']; ?>,'<?php echo $dado['rastreio']; ?>','<?php echo str_replace("'","*",$dado['cliente']); ?>','<?php echo $dado['rastreio_entrada']; ?>','<?php echo $dado['comprovante_rastreio']; ?>', '<?php echo $dado['comprovante_rastreio_entrada']; ?>');" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill finalizar_pedido" solicitacao_id="<?php echo $dado['id']; ?>"  >
										<i class="la la-truck"></i>
									</a>
									<?php if($dado['status_id']==10) { ?>
										<a href="#" onclick="retornoSolicitacao(<?php echo $dado['id']; ?>);" class="m-portlet__nav-link btn m-btn m-btn--hover-primary m-btn--icon m-btn--icon-only m-btn--pill " solicitacao_id="<?php echo $dado['id']; ?>"  >
											<i class="la la-undo"></i>
										</a>
									<?php } ?>
									<a href="#" onclick="anexarFotos(<?php echo $dado['id']; ?>,'<?php echo str_replace("'","*",$dado['cliente']); ?>');" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill finalizar_pedido" solicitacao_id="<?php echo $dado['id']; ?>" cliente="<?php echo $dado['cliente']; ?>" title="Conferência">
										<i class="la la-picture-o"></i>
									</a>									
								</span>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<!--end: Datatable -->
			</div>
		</div>		        
	</div>	
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ Início Modais ***********************************
			**************************************************************************************
			**************************************************************************************
	-->
	<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group status_orcamento_" >
						<label for="exampleSelect1">Situação da Solicitãção</label>
						<select class="form-control m-input m-input--air" id="status_solicitacao">
							<option value="">Selecione o Status da Solicitação</option>	
							<!--<?php 	foreach($situacao as $status){ ?>
								<option value="<?=$status['id']?>"><?=$status['descricao']?></option>
							<?php } ?>-->
							<?php if( $setor_id == 4 ){ ?>
								<option value="9">Serviço Realizado</option>
								<option value="5">Solicitação Cancelada</option>
							<?php }else{ ?>		
							<option value="6">Peça(s) entregue(s)</option>
							<option value="11">Aguardando NF e Peças</option>
							<option value="12">Aguardando NF</option>
							<option value="13">Aguardando Peças</option>
							<option value="14">Disponível Para Retirada (Aviso ao suporte)</option>
							<option value="7">Solicitação Finalizada</option>
							<option value="5">Solicitação Cancelada</option>
						<?php } ?>
						</select>
					</div>					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status_andamento" class="btn btn-primary" solicitacao_id="" nome_cliente="" chamado_id="" indicacao="0">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>	

	<!-- Modal inserir andamento referente aos status 11,12 e 13 -->
	<div class="modal fade" id="andamento_status" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group status_orcamento_" >
						<label for="exampleSelect1">Insira as informações da cobrança</label>
						<textarea class="form-control" id="andamento_cobranca"></textarea>
					</div>					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status" class="btn btn-primary" solicitacao_id="" nome_cliente="" chamado_id="" indicacao="0">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal tipo de solicitação quando o status selecionado for o 7 -->
	<div class="modal fade" id="andamento_status_tipo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle">Tipo de Solicitação</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group " >
						<label for="exampleSelect1">Selecione o tipo de solicitação</label>
						<div class="m-radio-inline">
							<label class="m-radio">
								<input type="radio" name="tipo" value="DOAÇÃO" class="tipo" checked="checked" />
								DOAÇÃO
								<span></span>
							</label>
							<label class="m-radio">
								<input type="radio" name="tipo" value="SUBSTITUIÇÃO" class="tipo" />
								SUBSTITUIÇÃO
								<span></span>
							</label>							
						</div>	
						<div class="form-group m-form__group nf_retorno" style="display: none;margin-top: 50px;">
							<label for="exampleSelect1">Nota Fiscal de Retorno</label>
							<input type="text" class="form-control" id="nr_nf_retorno" style="width: 30%" maxlength="50" />
						</div>
					</div>					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="alterar_status_finalizado" class="btn btn-primary" solicitacao_id="" nome_cliente="" chamado_id="" indicacao="0">Alterar Situação</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Finalizar pedido -->
	<div class="modal fade" id="m_modal_7" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-finalizar-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" >
							<label for="exampleSelect1">E-mail do cliente</label>
							<input type="email" style="text-transform: lowercase;" class="form-control m-input--air" value="" id="email" placeholder="E-mail" />
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="finalizar_pedido" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Anexar nfe ao pedido -->
	<div class="modal fade" id="anexarNfe" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('AreaAssistencia/anexarNfeAssistencia');?>" method="post"  enctype="multipart/form-data">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content" style="margin-top: 88px !important;">
				<div class="modal-header">
					<h5 class="modal-title modal-anexar-nfe-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="row">
						<div class="form-group m-form__group col-lg-6 col-xs-12">						
							<label for="exampleSelect1">Nota fiscal</label>
							<input type="text" class="form-control m-input--air" name="nr_nf" id="nr_nf" placeholder="Número" required="required" />
							<input type="file" class="form-control m-input--air" name="nfe" id="nfe" placeholder="Anexar Nfe" required="required" />
						</div>	
						<div class="form-group m-form__group col-lg-6 col-xs-12">						
							<label for="exampleSelect1">Data/Hora de Envio</label>
							<input type="text" class="form-control m-input--air data" name="dthr_envio" id="dthr_envio" placeholder="Data/Hora Envio" required="required" />
							<input type="hidden" id ="solicitacao_id"  name="solicitacao_id" value="" />
							<input type="hidden" id ="cliente_id" name="cliente"  />
						</div>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
			</div>			
		</div>
		</form>
	</div>
	<!-- Modal Anexar código de rastreio -->
	<div class="modal fade" id="anexarRastreio" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('AreaAssistencia/anexarRastreio');?>" method="post"  enctype="multipart/form-data">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content" style="margin-top: 88px !important;">
				<div class="modal-header">
					<h5 class="modal-title modal-anexar-rastreio" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="row">
						<div class="form-group m-form__group col-lg-6 col-xs-6">						
							<label for="exampleSelect1">Código de Rastreio</label>
							<input type="text" class="form-control m-input--air" name="rastreio" id="rastreio" placeholder="Código" required="required" />					
							<input type="hidden" id ="solicitacao_id_rastreio"  name="solicitacao_id" value="" />
							<input type="hidden" id ="cliente_id_rastreio" name="cliente"  />
						</div>						
						<div class="form-group m-form__group col-lg-6 col-xs-6 comprovante_rastreio">						
							<label for="comprovante_rastreio">Comprovante de Rastreio</label>
							<input type="file" name="comprovante_rastreio" id="comprovante_rastreio" class="form-control m-input--air">
						</div>

					</div>
					<div class="row" id="div_rastreio_entrada" style="display: none;">
						<div class="form-group m-form__group col-lg-6 col-xs-6">						
							<label for="exampleSelect1">Código de Rastreio de Entrada</label>
							<input type="text" class="form-control m-input--air" name="rastreio_entrada" id="rastreio_entrada" placeholder="Código" />				
						</div>						
						<div class="form-group m-form__group col-lg-6 col-xs-6 comprovante_rastreio_entrada">						
							<label for="exampleSelect1">Comprovante do Rastreio de Entrada</label>
							<input type="file" name="comprovante_rastreio_entrada" id="comprovante_rastreio_entrada" class="form-control m-input--air">
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" pedido_id="" orcamento_id="">Enviar</button>
				</div>
			</div>			
		</div>
		</form>
	</div>
	<!-- Modal Andamento -->
	<div class="modal fade" id="modal-andamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">		
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-andamento-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="m-list-timeline">
						<div class="m-list-timeline__items">                
						</div>
					</div> 
					<hr/>
					<div class="form-group m-form__group">
						<div class="form-group m-form__group" >
							<label for="exampleSelect1">Adicionar Andamento da Solicitação</label>
							<textarea type="text" class="form-control m-input m-input--air" id="andamento_texto"></textarea>					
						</div>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="adicionar_andamento" class="btn btn-primary" status_id="" solicitacao_id="">Adicionar Andamento</button>
				</div>
			</div>
		</div>		
	</div>	

	<!-- Modal retorno solicitação de peças -->
	<div class="modal fade" id="retorno_pecas" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title modal-status-title" id="exampleModalLongTitle">Retorno da Solicitação de Peças</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">
					<div class="form-group m-form__group status_orcamento_" >
						<label for="exampleSelect1">Motivo retorno da solicitação peças</label>
						<textarea class="form-control" id="motivo_retorno"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="button" id ="retornar_solicitacao" class="btn btn-primary" solicitacao_id="" nome_cliente="" chamado_id="" indicacao="0">Retornar Solicitação</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Anexar Fotos Conferência -->
	<div class="modal fade" id="anexarFotos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="<?php echo base_url('AreaAssistencia/anexarFotosConferencia');?>" method="post"  enctype="multipart/form-data">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content" style="margin-top: 88px !important;">
				<div class="modal-header">
					<h5 class="modal-title modal-anexar-fotos-title" ></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" style="width: 750px;">					
					<div class="row">
						<div class="form-group m-form__group col-lg-12 col-xs-12"  id="fotosConferencia">	
						</div>
					</div>
					<div class="row">
						<div class="form-group m-form__group col-lg-6 col-xs-12">						
							<label for="exampleSelect1">Upload Fotos</label>							
							<input type="file" class="form-control m-input--air" name="foto[]" id="fotos" placeholder="Anexar Fotos" required="required" multiple />
							<input type="hidden" name="solicitacao_id" id="solicitacao_id_foto">
						</div>								
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary" >Enviar</button>
				</div>
			</div>			
		</div>
		</form>
	</div>
	<!-- 
			************************************************************************************** 
			**************************************************************************************
			************************************ FIM Modais **************************************
			**************************************************************************************
			**************************************************************************************
	-->
	<!-- end:: Body -->
	<?php if ($this->session->flashdata('erro') == TRUE){ ?>
	<script type="text/javascript"> 	
		swal(
			'Ops!',
			'Aconteceu algum problema, reveja seus dados e tente novamente!',
			'error'
			);
		</script>
		<?php unset($_SESSION['erro']);} ?>
		<?php if ($this->session->flashdata('sucesso') == TRUE){ ?>
						 
		<script type="text/javascript">
			
			swal({
				title: "OK!",
				text: 'Cadastro realizado com sucesso!',
				type: "success"
			}).then(function() { 
		      	//window.location = base_url+'AreaAssistencia/solicitacaoPecas';
		   	}); 
		</script>	
		<?php unset($_SESSION['sucesso']); } ?>