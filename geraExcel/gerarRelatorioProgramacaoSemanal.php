<?php

	include("excelwriter.inc.php");
	include("conexao.php");
	
	$excel=new ExcelWriter("relatorioProgramacaoSemanal.xls");
	if($excel===false){
		echo $excel->error;
	}

	$myArr=array("Nr. Pedido", "Cliente",utf8_decode("Situação"),"Semana","Tipo","Modelo","Qtd", "Nr.Bicos",utf8_decode("Combustível"), "Pintura", "Desc. Bombas",utf8_decode("Informações"),"Status Prod.");
		
	$excel->writeLine($myArr);

	$dt_ini = '';
    $dt_fim = '';    
    $condicao = '1=1';

    if($_GET['dt_ini'] != '' && $_GET['dt_fim'] != ''){
    	$explode 	= 	explode('/',$_GET['dt_ini']);
    	$dt_ini 	= 	$explode[2].'-'.$explode[1].'-'.$explode[0];
    	$explodeF 	= 	explode('/',$_GET['dt_fim']);
    	$dt_fim 	= 	$explodeF[2].'-'.$explodeF[1].'-'.$explodeF[0];
    	//$dt_fim 	=	date('Y-m-d', strtotime('+4 days',strtotime($dt_ini)));
    	//$fim 		= 	date('d/m/Y', strtotime('+4 days',strtotime($dt_ini)));
    	$condicao = " and ps.dt_semana_prog BETWEEN '".$dt_ini."' and '".$dt_fim."'";
    }	        

	$select =	"SELECT      ps.pedido_id, date_format(ps.dt_semana_prog,'%d/%m/%Y') as dt_semana_prog , SPLIT_STRING(op.modelo, '-',1) as tipo, if(ps.modelos_quebra != '', concat( ps.modelos_quebra,' - ', ps.nr_bicos,' - ', ps.nr_bombas ), op.modelo) as modelo, op.qtd, op.combustivel, pe.pintura, op.descricao, op.informacoes, if(ps.modelos_quebra != '', ps.nr_bombas,op.qtd)  as nr_quebrados, s.descricao as status, ps.nr_bicos,
                (select sp.descricao from programacao_semanal pse 
                    inner join producao_andamento pa on pa.pedido_id = pse.pedido_id
                    inner join status_producao sp on sp.id = pa.status_producao_id
                    where pse.id = ps.id group by pse.pedido_id ) as status_producao 
                FROM        programacao_semanal ps
                INNER JOIN  pedidos pe on pe.id = ps.pedido_id
                INNER JOIN  ordem_producao op on op.pedido_id = pe.id                
                INNER JOIN  status_pedidos s on pe.status_pedido_id = s.id
                where       1=1 ".$condicao."                
                UNION 
                SELECT      ps.pedido_id, date_format(ps.dt_semana_prog,'%d/%m/%Y') as dt_semana_prog , SPLIT_STRING(pro.modelo, '-',1) as tipo, if(ps.modelos_quebra != '', concat( ps.modelos_quebra,' - ', ps.nr_bicos,' - ', ps.nr_bombas ), pro.modelo) as modelo, pi.qtd,pi.produtos, pe.pintura, pro.modelo, '', if(ps.modelos_quebra != '', ps.nr_bombas,pi.qtd)  as nr_quebrados, s.descricao as status, ps.nr_bicos,
                (select sp.descricao from programacao_semanal pse 
                    left join producao_andamento pa on pa.pedido_id = pse.pedido_id
                    left join status_producao sp on sp.id = pa.status_producao_id
                    where pse.id = ps.id group by pse.pedido_id ) as status_producao 
                FROM        programacao_semanal ps
                INNER JOIN  pedidos pe on pe.id = ps.pedido_id
                INNER JOIN  pedido_itens pi on pi.pedido_id = pe.id
                inner join  produtos pro on pro.id = pi.produto_id
                INNER JOIN  status_pedidos s on pe.status_pedido_id = s.id
                where       s.ordem < 6 and pro.tipo_produto_id in (1,2,3,6) ".$condicao."  
                GROUP BY pe.id, pro.id
                ORDER BY 2,1";
	$result =	$mysqli->query($select);
	while($dados[] = $result->fetch_array(MYSQLI_ASSOC)){}
	if( count($dados) > 0 ){ 
	foreach( $dados as $dado ){

		$excel->writeRow();
		$excel->writeCol(urldecode($dado['pedido_id']));
		$excel->writeCol(urldecode(utf8_decode($dado['cliente'])));
		$excel->writeCol(urldecode(utf8_decode($dado['status'])));
		$excel->writeCol(urldecode(date('w',strtotime($dado['dt_semana_prog'])).'-'.$dado['dt_semana_prog']));
		$excel->writeCol(urldecode(utf8_decode($dado['tipo'])));
		$excel->writeCol(urldecode($dado['modelo']));
		$excel->writeCol(urldecode($dado['qtd']));
		$excel->writeCol(urldecode($dado['nr_bicos']));
		$excel->writeCol(urldecode($dado['combustivel']));
		$excel->writeCol(urldecode(utf8_decode($dado['pintura'])));
		$excel->writeCol(urldecode(utf8_decode($dado['descricao'])));
		$excel->writeCol(urldecode(utf8_decode($dado['informacoes'])));
		$excel->writeCol(urldecode(utf8_decode($dado['status_producao'])));
	}

	$excel->close();
	
?>
	<script type="text/javascript">
		window.open('relatorioProgramacaoSemanal.xls');
	</script>

<?php }else{ ?>
	<script type="text/javascript">
		window.alert('Nenhum resultado encontrado');
		window.history.go(-2);
	</script>
<?php } ?>