<?php
		
	include("excelwriter.inc.php");
	include("conexao.php");
	
	$excel=new ExcelWriter("relatorioChamadosOps.xls");
	if($excel===false){
		echo $excel->error;
	}

	$myArr=array("Estado", "Pedidos", "Bombas","Bicos","Chamados", "OP's","Deslocamento");		
	$excel->writeLine($myArr);
	$where = '';
		        
        $estado = "";
        if($_GET['estado'] != ''){
            $estado = "e.estado in ".$_GET['estado']." and ";
        }

        if($_GET['regiao'] != ''){
            $estado = "e.estado in ".$_GET['regiao']." and ";
        }
        $sql = "SELECT bombas.*, chamados.total as chamados, format(total_op,2,'de_DE') as total_op, format(vlr_deslocamento,2,'de_DE') as vlr_deslocamento FROM 
            (select e.estado, count(distinct pe.id) as pedidos, sum(pi.qtd) as bombas, sum(pi.qtd*pr.nr_bicos) as bicos from pedidos pe 
            inner join pedido_itens pi  on  pe.id = pi.pedido_id 
            inner join produtos pr      on pr.id = pi.produto_id
            inner join orcamentos o     on  o.id = pe.orcamento_id
            inner join empresas e       on  e.id = o.empresa_id 
            where ".$estado." pe.dthr_geracao BETWEEN '".$_GET['dt_ini']."' and '".$_GET['dt_fim']."' and pr.tipo_produto_id in (1,2,3,6) and pe.status_pedido_id <> 7
            group by e.estado) AS bombas 
            /* aqui vão os chamados */
            left join (select e.estado, count(DISTINCT c.id) as total, sum(op.vlr_toral) as total_op, sum(op.vl_deslocamento) as vlr_deslocamento 
            from chamado c 
            inner join empresas e on e.id = c.cliente_id
            left join (SELECT distinct  op.id, op.chamado_id,  sum(opi.qtd*opi.valor) as vlr_toral, op.valor_op as total, sum( if( opi.despesa_id in (12,16,22,20,14,18,11,15,27,31,33,34,26,30), opi.qtd*opi.valor,0 ) ) as vl_deslocamento from ordem_pagamento op 
            inner join ordem_pagamento_itens opi on opi.ordem_pagamento_id = op.id
            group by op.id ) as op on op.chamado_id = c.id 
            where ".$estado." c.inicio BETWEEN  '".$_GET['dt_ini']."' and '".$_GET['dt_fim']."' and c.status_id <> 4 group by e.estado) as chamados 
            ON bombas.estado = chamados.estado"; 
	
	$result =	$mysqli->query($sql);

	while($dados[] = $result->fetch_array(MYSQLI_ASSOC)){}
	
	foreach( $dados as $dado ){
		
		if(isset($dado)){
			$excel->writeRow();					
			$excel->writeCol((str_replace("%2F","/",$dado['estado'])));
			$excel->writeCol((str_replace("%2F","/",$dado['pedidos'])));
			$excel->writeCol((str_replace("%2F","/",$dado['bombas'])));
			$excel->writeCol((str_replace("%2F","/",$dado['bicos'])));
			$excel->writeCol((str_replace("%2F","/",$dado['chamados'])));		
			$excel->writeCol((str_replace("%2F","/",$dado['total_op'])));
			$excel->writeCol((str_replace("%2F","/",$dado['vlr_deslocamento'])));			
		}

	} 

	$excel->close();	

	
?>
	<script type="text/javascript">
		window.open('relatorioChamadosOps.xls');
	</script>