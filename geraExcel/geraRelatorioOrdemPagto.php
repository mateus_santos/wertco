<?php
		
	include("excelwriter.inc.php");
	include("conexao.php");
	
	$excel=new ExcelWriter("relatorioOrdensPagamento.xls");
	if($excel===false){
		echo $excel->error;
	}

	$myArr=array("ID", "Chamado #", "Descrição","Tipo","Favorecido","Cliente","Estado","Despesas", "valor");
		
	$excel->writeLine($myArr);
	$where = '';
	if($_GET['dt_fim'] != ''){
            $dt_ini = explode('-',$_GET['dt_ini']);            
            $dt_ini = $dt_ini[2].'-'.$dt_ini[1].'-'.$dt_ini[0];
            $dt_fim = explode('-',$_GET['dt_fim']);
            $dt_fim = $dt_fim[2].'-'.$dt_fim[1].'-'.$dt_fim[0];
            $where.=" and op.dt_emissao between '".$dt_ini."' and '".$dt_fim."' ";
        }

        if($_GET['tipo_id'] != ''){
            $where.=" and c.tipo_id =".$_GET['tipo_id'];               
        }

        if($_GET['cliente_id'] != ''){
            $where.=" and cli.id =".$_GET['cliente_id']; 
        }

        if($_GET['favorecido_id'] != ''){
            $where.=" and tec.id =".$_GET['favorecido_id']; 
        }       

        if($_GET['estado'] != ''){
            $where.=" and cli.estado = '".$_GET['estado']."'"; 
        }

        if($_GET['regiao'] != ''){
            $where.=" and cli.estado in ".$_GET['regiao']; 
        }

        $sql = "SELECT  op.id, op.descricao, ct.descricao as tipo_chamado, concat(upper(tec.razao_social),'-',tec.cnpj) as favorecido, 
                        concat(upper(cli.razao_social),'-',cli.cnpj) as cliente, tec.estado as estado_tec, cli.estado as estado_cli, GROUP_CONCAT(d.descricao) as despesas, 
                        format(sum(opi.valor*opi.qtd ),2,'DE_de') as valor, sum(opi.valor*opi.qtd ) as valor_n, op.chamado_id    
                FROM    ordem_pagamento op
                INNER JOIN ordem_pagamento_itens opi on opi.ordem_pagamento_id = op.id
                INNER JOIN despesas d on d.id = opi.despesa_id
                INNER JOIN empresas tec on tec.id = op.favorecido_id
                INNER JOIN chamado c on c.id = op.chamado_id 
                INNER JOIN empresas cli on cli.id = c.cliente_id
                LEFT JOIN chamado_tipo ct on ct.id = c.tipo_id
                WHERE 1=1 ".$where." 
                GROUP by op.id  
                ORDER BY op.id  DESC";
    
	$result =	$mysqli->query($sql);
	while($dados[] = $result->fetch_array(MYSQLI_ASSOC)){}
	
	foreach( $dados as $dado ){
		
		if(isset($dado)){
			$excel->writeRow();
			$excel->writeCol(urldecode($dado['id']));		
			$excel->writeCol((str_replace("%2F","/",$dado['descricao'])));
			$excel->writeCol((str_replace("%2F","/",$dado['descricao'])));
			$excel->writeCol((str_replace("%2F","/",$dado['tipo_chamado'])));
			$excel->writeCol((str_replace("%2F","/",$dado['favorecido'])));
			$excel->writeCol((str_replace("%2F","/",$dado['cliente'])));		
			$excel->writeCol((str_replace("%2F","/",$dado['estado_cli'])));
			$excel->writeCol((str_replace("%2F","/",$dado['despesas'])));
			$excel->writeCol((str_replace("%2F","/",$dado['valor'])));
		}
	} 

	$excel->close();	

	
?>
	<script type="text/javascript">
		window.open('relatorioOrdensPagamento.xls');
	</script>