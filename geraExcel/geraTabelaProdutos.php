<?php

	include("conexao.php");

	$select =	"SELECT * FROM produtos WHERE tipo_produto_id not in (4,5)";

	$result =	$mysqli->query($select);	

	while($dados[] = $result->fetch_array(MYSQLI_ASSOC)){}

	echo '<pre>';

	$i = 0;	

	//icms 7%
	$html = "<p>ICMS 7% </p>
			<table>
				<thead>
					<tr>
						<td>Modelo</td>
						<td>0%</td>
						<td>2%</td>
						<td>3%</td>
						<td>4%</td>
						<td>5%</td>
					</tr>
				</thead>
				<tbody>			
			";

	foreach( $dados as $dado ){
		if(is_array($dado)){
			$html.= "<tr>
						<td>".$dado['codigo']." - ".$dado['modelo']."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.7200) * (1.2589928057554 * 1.11)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.6945) * (1.2589928057554 * 1.11)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.6805) * (1.2589928057554 * 1.11)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.637)  * (1.2589928057554 * 1.11)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.583)  * (1.2589928057554 * 1.11)), 2,',','.')."</td>	
					</tr>";
		}		

	}
	$html.="	</tbody>
			</table>";
	//icms 12%
	$html.= "<hr/><p>ICMS 12% </p>
			<table>
				<thead>
					<tr>
						<td>Modelo</td>
						<td>0%</td>
						<td>2%</td>
						<td>3%</td>
						<td>4%</td>
						<td>5%</td>
					</tr>
				</thead>			
			";

	foreach( $dados as $dado ){
		if(is_array($dado)){
			$html.= "<tr>
						<td>".$dado['codigo']." - ".strtoupper($dado['modelo'])."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.7200) * (1.34357005758157 * 1.09)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.6945) * (1.34357005758157 * 1.09)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.6805) * (1.34357005758157 * 1.09)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.637)  * (1.34357005758157 * 1.09)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.583)  * (1.34357005758157 * 1.09)), 2,',','.')."</td>	
					</tr>";
		}		

	}
	$html.="	</tbody>
			</table>";

	//icms 12%
	$html.= "<hr/><p>ICMS 18% </p>
			<table>
				<thead>
					<tr>
						<td>Modelo</td>
						<td>0%</td>
						<td>2%</td>
						<td>3%</td>
						<td>4%</td>
						<td>5%</td>
					</tr>
				</thead>			
			";

	foreach( $dados as $dado ){
		if(is_array($dado)){
			$html.= "<tr>
						<td>".$dado['codigo']." - ".$dado['modelo']."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.7200) * (1.46137787056367 * 1.07)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.6945) * (1.46137787056367 * 1.07)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.6805) * (1.46137787056367 * 1.07)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.637)  * (1.46137787056367 * 1.07)), 2,',','.')."</td>
						<td>R$ ".number_format(round(($dado['valor_unitario'] / 0.583)  * (1.46137787056367 * 1.07)), 2,',','.')."</td>	
					</tr>";
		}
	}
	
	$html.="	</tbody>
			</table>";
	echo $html;
?>