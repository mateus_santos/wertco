$(document).ready(function() {
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('.datepicker').mask('99/99/9999');	
	$('.datepicker').datepicker({
	    format: 'dd/mm/yyyy'	    
	});
});