$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    //$('#m_modal_7').modal('show');
    swal({
        title: 	"Atenção!",
        text: 	"Para atualizar seu cartão, entre em contato com o suporte técnico da WERTCO - (11) 3900-2565!",
        type: 	'warning' 
    }).then(function() {
  		window.location = base_url+'AreaTecnicos/';  	    
    }); 
}); 

function geraSenhaValidade(){

	if( $('#dt_vencimento').val() === 'MAIOR'){
		
		var tag = document.getElementById('tagMecanico').value;
		var data = (document.getElementById('m_datepicker').value).replace("/", "").replace("/", "");
		
		if(tag.length == 16 && data.length > 0){

			var senha 	= 	aesResult(data,tag,2);
			var id 		=	$('#id').val();
			
			if( senha.toString().length < 7 ){
				senha = '0' + senha.toString();
			}
			
			document.getElementById("aesSenha").value = senha;
			$.ajax({
		        method: "POST",
		        url: base_url+'AreaTecnicos/atualizaCartaoTecnico',
		        async: true,
		        data: { tag    				:   tag,
		        		ultima_atualizacao 	: 	$('#m_datepicker').val(),
		        		senha 				: 	senha,
		        		id 					: 	id 	}
	        }).done(function( data ) {
	            var dados = $.parseJSON(data);
	            if( dados.retorno == 'sucesso')
	            {
	            	swal({
	                    title: "Atenção!",
	                    text: "Cartão atualizado com sucesso! A nova data é " + $('#m_datepicker').val() +" e a senha gerada é " + senha +"!",
	                    type: 'success'
                    }).then(function() {
                  		window.location = base_url+'AreaTecnicos/';  	    
                    });  
	            }
	        });
		}else{
			document.getElementById("aesSenha").value = "Campos inválidos";
		}	
	}else if( $('#ativo').val() == 1 ){
		swal({
   			title: "Atenção!",
   			text:  "O seu cartão ainda está válido.",
   			type:  'warning'
    	});

    }else{

		swal({
   			title: "Atenção!",
   			text:  "O seu cartão está inativado, entre em contato com o suporte técnico para mais informações.",
   			type:  'warning'
    	});
	}

	
	
	

}