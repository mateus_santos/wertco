$(document).ready(function(){   
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('.esconde').hide();
    $('#cadastro').hide();
    $('.date').datepicker({
        orientation: 'bottom',
        format: 'dd/mm/yyyy'
    });
    $('.comissao').bind('click', function(){
        $('.salvar').removeAttr('disabled');
    });

    $( "#nome_indicador" ).autocomplete({
        source: base_url+"AreaAdministradorComercial/retornaIndicadores",
        minLength: 2,
        select: function( event, ui ) {

            $('#indicador_id').val(ui.item.id);
            var indicador   =   ui.item.id;
            
        }
    });

    $('.indicacao').bind('click', function(){
        if($(this).val() == 1){
            $('.nome_indicador').fadeIn('slow');
            $('#nome_indicador').attr('required','required');

        }else{

            $('.nome_indicador').fadeOut('slow');
            $('#nome_indicador').removeAttr('required');
            $('#nome_indicador').val('');           
        }
    });

    $('.bombas').bind('change', function(){
        indice = $(this).attr('indice'); 
        console.log($(this).find('option:selected').attr('reajuste'));
        $('.valor_unitario_'+indice).attr('valor_base',$(this).find('option:selected').attr('valor_unitario'));  
        $('.valor_unitario_'+indice).attr('tipo',$(this).find('option:selected').attr('tipo'));
        $('.valor_unitario_'+indice).attr('reajuste',$(this).find('option:selected').attr('reajuste'));
        $('#valor_unitario_'+indice).val($(this).find('option:selected').attr('valor_unitario'));          
        var comissao = $("input[name='valor_desconto_orc']:checked").val();
        $('#reajuste').val($(this).find('option:selected').attr('reajuste'));
        if(  comissao != 'outro' && comissao != '' && comissao != undefined && $('#fator').val() != ''){
            
            atualiza_valores($('#estado').val());
        }

        $('.salvar').attr('disabled', false);
    }); 


    $('.add').bind('click', function(){

        var html = $('#modelo').clone();                
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
        $(html).find('.bombas').attr('indice', indice);
        $(html).find('.valor_unitario').attr('indice', indice);
        $(html).find('.valor_unitario').text('');
        $(html).find('b.valor_unitario').addClass('valor_unitario_'+indice);
        $(html).find('b.valor_unitario').attr('valor_base','');
        $(html).find('b.valor_unitario').removeClass('valor_unitario_0');
        $(html).find('input.valor_unitario').attr('id', 'valor_unitario_'+indice);

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();                
        body+="</tr>";                
        $('.table').append(body);                
        $('#modelo').attr('total_indice',indice);
        excluir();
        mostra_valor();    
        var comissao = $("input[name='valor_desconto_orc']:checked").val();
        if(comissao == 'outro'){
            $('#valor_unitario_'+indice).unmask();
            $('#valor_unitario_'+indice).mask('##.##0,00');
        }
    });
    
    $('.tp_cadastro').bind('click', function(){
            
            $('#cadastro').slideDown('slow');

            if( $(this).val() == 'PJ' ){
                
                $('#cadastro input').attr('disabled', false);
                $('#cnpj').mask('00.000.000/0000-00');
                $('#cnpj').attr('placeholder','INSIRA UM CNPJ');
                $('#razao_social').attr('placeholder','INSIRA UMA RAZÃO SOCIAL');
                $('#label_documento').text('CNPJ:');    
                $('#label_razao').text('RAZÃO SOCIAL:');
                $('#campo_fantasia').fadeIn('slow');
                $('#campo_ie').fadeIn('slow');
                $('#inscricao_estadual').attr('required',true);
                $('#fantasia').attr('required',true);
                
                $('#estadoE').removeAttr('name');
                $('#estado').attr('name','estado');
                $('#estadoE').fadeOut('slow');
                $('#estado').fadeIn('slow');

            }else if(   $(this).val() == 'PF'   ){

                $('#cadastro input').attr('disabled', false);
                $('#cnpj').mask('000.000.000-00');
                $('#cnpj').attr('placeholder','INSIRA UM CPF');
                $('#razao_social').attr('placeholder','INSIRA O NOME');
                $('#label_documento').text('CPF:');
                $('#label_razao').text('NOME:');
                $('#campo_fantasia').fadeOut('slow');
                $('#campo_ie').fadeOut('slow');
                $('#inscricao_estadual').removeAttr('required');
                $('#fantasia').removeAttr('required');

                $('#estadoE').removeAttr('name');
                $('#estado').attr('name','estado');
                $('#estadoE').fadeOut('slow');
                $('#estado').fadeIn('slow');
               
            }else{

                $('#cadastro input').attr('disabled', false);
                $('#cnpj').unmask();
                $('#cnpj').attr('placeholder','INSIRA ALGO PARA IDENTIFICAR');
                $('#razao_social').attr('placeholder','INSIRA UMA RAZÃO SOCIAL');
                $('#label_documento').text('ID. DOC:');    
                $('#label_razao').text('RAZÃO SOCIAL');
                $('#campo_fantasia').fadeIn('slow');
                $('#campo_ie').fadeOut('slow');
                $('#inscricao_estadual').attr('required',false);
                $('#fantasia').attr('required',false);  
                $('#estado').attr('required',false);
                $('#estadoE').attr('type','text');
                $('#estadoE').attr('readonly',true);
                $('#estadoE').fadeIn('slow');
                $('#estadoE').attr('name','estado');
                $('#estadoE').val('EX');

                $('#estado').attr('required',false);
                $('#estado').removeAttr('name');
                $('#estado').fadeOut('slow');   
                //Simula icms de 7%
                atualiza_valores('AC');
                $('#estado').val('AC');
                $('.esconde').fadeIn('slow');
                $('#msg_produtos').fadeOut('fast');
            }

    });
    
    //$('#cnpj').mask('00.000.000/0000-00');
    //$('#cpf').mask('000.000.000-00');   
   
    //$('#cnpj').mask('00.000.000/0000-00');
    //$('#cpf').mask('000.000.000-00');
    $('#bombas').mask('00000'); 
    $('#bicos').mask('00000');  
    $('.qtd').mask('000');
    $('#cep').mask('00000-000');  
    
    mostra_valor(); 
    
   $('#cnpj').bind('focusout', function(){
        var cnpj = $(this).val(); 
        var tp_cadastro = $('.tp_cadastro:checked').val();
        var erro = 0;

        if(cnpj != ""){ 

            if(  tp_cadastro == 'PJ' ) {
                if(!isCNPJValid(cnpj)){
                    swal({
                        title: "Atenção!",
                        text: "CNPJ inválido!",
                        type: 'warning'
                    }).then(function() {
                        $('#cnpj').val('');
                    });
                    erro = 1;
                }else{
                    erro = 0;
                }
            }else if(tp_cadastro == 'PF'){
            
                $.ajax({
                    method: "POST",
                    url: base_url+'clientes/verifica_cpf_apenas',
                    async: true,
                    data: { cpf    :   cnpj }
                    }).done(function( data ) {
                        var dados = $.parseJSON(data);
                        if(dados.status == 'erro'){
                             swal({
                                title: "Atenção!",
                                text: "CPF inválido!",
                                type: 'warning'
                            }).then(function() {
                                $('#cnpj').val('');
                            });
                            erro = 1;
                        }else{
                            erro = 0;
                        }

                });                
            }

            if(  erro == 0 ){

            $.ajax({
                method: "POST",
                url: base_url+'clientes/verificaOrcamentoId',
                async: true,
                data: { cnpj    :   cnpj }
                }).done(function( data ) {
                    var dados = $.parseJSON(data);
                    if( dados.total == 0 || dados.status_id == 13){

                        if(dados.status_id == 13){
                            swal({
                                title: 'Atenção',
                                text: 'Existe um orçamento (#'+dados.id+') para esse cliente, deseja acessá-lo?',
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonClass: "swal2-confirm btn btn-success m-btn m-btn--custom",
                                confirmButtonText: "Acessar Orçamento",
                                cancelButtonText: "Cancelar"
                            }).then(function(isConfirm) {
                                if (isConfirm.value) { 
                                    window.open(base_url+'AreaAdministradorComercial/visualizarOrcamento/'+dados.id);      
                                    
                                }else{
                                    
                                    $('#cnpj').val('');
                                    
                                }
                            });    
                        }

                        $.ajax({
                            method: "POST",
                            url: base_url+'clientes/verifica_cnpj',
                            async: false,
                            data: { cnpj    :   cnpj }
                            }).done(function( data ) {
                                var dados = $.parseJSON(data);                          
                                if(dados.length > 0){ 

                                    $('#razao_social').val(dados[0].razao_social);      
                                    $('#razao_social').attr('disabled', true);      
                                    $('#fantasia').val(dados[0].fantasia);      
                                    $('#fantasia').attr('disabled', true);
                                    $('#telefone').val(dados[0].telefone);
                                    $('#telefone').attr('disabled', true);
                                    $('#endereco').val(dados[0].endereco);      
                                    $('#endereco').attr('disabled', true);                                  
                                    $('#email').attr('disabled', true);
                                    $('#email').val(dados[0].email);
                                    $('#cidade').attr('disabled', true);
                                    $('#cidade').val(dados[0].cidade);  
                                    $('#estado').attr('disabled', true);
                                    $('#estado').val(dados[0].estado);                        
                                    $('#pais').attr('disabled', true);
                                    $('#pais').val(dados[0].pais);                        
                                    $('#salvar').val('0');
                                    $('#empresa_id').val(dados[0].id);
                                    $('#inscricao_estadual').attr('disabled', true);
                                    $('#inscricao_estadual').val(dados[0].insc_estadual);
                                    $('#cep').attr('disabled', true);
                                    $('#cep').val(dados[0].cep);
                                    $('#bairro').attr('disabled', true);
                                    $('#bairro').val(dados[0].bairro);
                                     //verifica Observações cliente
                                    $.ajax({
                                        method: "POST",
                                        url:  base_url+"AreaAdministrador/buscaObservacoes", 
                                        async: true,
                                        data: { empresa_id : dados[0].id }
                                    }).done(function(data) {

                                        var dados = $.parseJSON(data);  
                                        //$('.m-list-timeline__item').remove();             
                                        var html="";                                        
                                        if( dados.length > 0 ){
                                            for( var i=0;i<dados.length;i++ ){
                                                var data_hora = dados[i]['dthr_obs'].split(' ');
                                                var data = data_hora[0].split('-');
                                                var hora = data_hora[1];
                                                var data_andamento = data[2]+'/'+data[1]+'/'+data[0];
                                                html+= '<div class="m-list-timeline__item">';
                                                html+='<span class="m-list-timeline__badge m-list-timeline__badge--success"></span>';
                                                html+=' <span class="m-list-timeline__text">'+dados[i]['descricao']+'</span>';
                                                html+=' <span class="m-list-timeline__time">'+data_andamento+' '+hora+'</span>';
                                                html+='</div>';
                                        
                                                $('.m-list-timeline__items').empty();
                                                $('.m-list-timeline__items').append(html);
                                                console.log(html);
                                                $("#m_obs").modal({
                                                    show: true
                                                });
                                            }
                                        }else{
                                            //$('.m-list-timeline__item').remove();
                                        }     

                                    });    

                                    atualiza_valores(dados[0].estado );
                                }else{

                                    if(tp_cadastro == 'PJ'){
                                        var cnpj_sem_mascara = cnpj.replace(/[^\d]+/g,'');

                                        $.ajax({
                                            method: "POST",
                                            url: base_url+'AreaAdministrador/ValidaCnpj',
                                            async: false,
                                            data: { cnpj    :   cnpj_sem_mascara }
                                        }).done(function( data ) {
                                            var dados = $.parseJSON(data);   
                                            
                                            if (dados.receita.erro == 0 && dados.receita.situacao == "ATIVA" ){
                                                $('#razao_social').attr('disabled', false);     
                                                $('#razao_social').val(dados.receita.retorno.razao_social);
                                                $('#razao_social').attr('style','border-color: #5fda17;');
                                                $('#fantasia').attr('disabled', false);
                                                $('#fantasia').val(dados.receita.retorno.nome_fantasia );
                                                $('#fantasia').attr('style','border-color: #5fda17;');
                                                $('#telefone').attr('disabled', false);
                                                $('#telefone').val(dados.receita.retorno.telefone);
                                                $('#telefone').attr('style','border-color: #5fda17;');
                                                $('#endereco').attr('disabled', false); 
                                                $('#endereco').val(dados.receita.retorno.logradouro+', '+dados.receita.retorno.numero+' '+dados.receita.retorno.complemento);                                
                                                $('#endereco').attr('style','border-color: #5fda17;');
                                                $('#email').attr('disabled', false); 
                                                $('#email').val('');                                                                                                
                                                $('#cidade').attr('disabled', false); 
                                                $('#cidade').val(dados.receita.retorno.municipio_ibge);  
                                                $('#cidade').attr('style','border-color: #5fda17;');
                                                $('#estado').attr('disabled', false); 
                                                $('#estado').val(dados.receita.retorno.uf);
                                                $('#estado').attr('style','border-color: #5fda17;');
                                                $('#bairro').attr('disabled', false); 
                                                $('#bairro').val(dados.receita.retorno.bairro);
                                                $('#bairro').attr('style','border-color: #5fda17;');
                                                $('#cep').attr('disabled', false); 
                                                $('#cep').val(dados.receita.retorno.cep);                          
                                                $('#cep').attr('style','border-color: #5fda17;');
                                                $('#pais').attr('disabled', false);
                                                $('#pais').attr('style','border-color: #5fda17;');                                                
                                                $('#inscricao_estadual').attr('disabled', false);
                                                $('#inscricao_estadual').val('');                        
                                                $('#cartao_cnpj').val(dados.receita.save);   
                                                $('#codigo_ibge_cidade').val(dados.receita.retorno.codigo_ibge);                  
                                                
                                                $('#pais').val('Brasil');         
                                                $('#salvar').val('1');

                                                atualiza_valores( dados.receita.retorno.uf );

                                            }else{
                                               /*swal({
                                                    title: "Atenção!",
                                                    text: dados.msg,
                                                    type: 'warning'
                                                }).then(function() {
                                                    $('#cnpj').val('');
                                                });*/

                                            }
                                        });
                                    }
                                   

                                }                           
                        });  
                    }else{

                       swal({
                            title: 'Atenção',
                            text: 'Existe um orçamento (#'+dados.id+') para esse cliente, deseja acessá-lo?',
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonClass: "swal2-confirm btn btn-success m-btn m-btn--custom",
                            confirmButtonText: "Acessar Orçamento",
                            cancelButtonText: "Cancelar"
                        }).then(function(isConfirm) {
                            if (isConfirm.value) { 
                                window.open(base_url+'AreaAdministradorComercial/visualizarOrcamento/'+dados.id);      
                                if( dados.status_id != 13 ){
                                    $('#cnpj').val('');
                                }
                            }else{
                                if( dados.status_id != 13 ){
                                    $('#cnpj').val('');
                                }
                            }
                        });
                   }
                });

            }
        
        }
    });
    
    $('#estado').bind('change', function(){
        
        atualiza_valores( $(this).val() );


    });

    $('.comissao').bind('click', function(){
        var comissao = $(this).val();
        var comissaoObj = $(this);
        

        if( $('#estado').val() == '' )
        {
            swal({
                title: "Atenção!",
                text: "Selecione o estado do cliente para calcular o(s) valor(es) do(s) produto(s)!",
                type: 'warning'
            }).then(function() {
                $(this).attr('checked', false);
                $('.salvar').attr('disabled', true);
            }); 
        }else{
            var estado = $('#estado').val();
            if(comissao == 'outro'){
                
                $('b.valor_unitario').hide();
                $('input.valor_unitario').attr('type','text');
                $('input.valor_unitario').fadeIn('slow');
                $('input.valor_unitario').unmask();
                $('input.valor_unitario').mask('##.##0,00');
            }else{
                $('b.valor_unitario').show();
                $('input.valor_unitario').attr('type','hidden');
                $('input.valor_unitario').fadeOut('slow');
                $('input.valor_unitario').unmask();
            
            $.ajax({
                method: "POST",
                url: base_url+'AreaAdministradorComercial/verificaIcms',
                async: true,
                data: { estado    :   estado }
                }).done(function( data ) {

                    var dados = $.parseJSON(data);
                    $('#fator').val(dados.fator);
                    var indice = 0;
                    console.log(comissao);
                    $('b.valor_unitario').each(function(){
                        
                        if($(this).attr('valor_base') != '' && $(this).attr('valor_base') != undefined && $(this).attr('tipo') != 'opcionais')  {
                            if( comissao == '3' && dados.valor_tributo == '7.00' ) {
                                $('.bombas option').show();
                                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(dados.fator) * 1.11).toFixed(2);
                            }else if( comissao == '3' && dados.valor_tributo == '10.00' ) {
                                $('.bombas option').show();
                                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(dados.fator) * 1.11) * 1.12).toFixed(2);
                            }else if( comissao == '3' && dados.valor_tributo == '12.00' ) {
                                $('.bombas option').show();
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(dados.fator) * 1.09).toFixed(2);
                            }else if( comissao == '3' && dados.valor_tributo == '18.00' ) {
                                $('.bombas option').show();
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(dados.fator) * 1.07).toFixed(2);
                            // ************************************
                            // ********* 4% de comissão ***********
                            // ************************************
                            }else if( comissao == '4' && dados.valor_tributo == '7.00' ) {
                                $('.bombas option').show();    
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(dados.fator) * 1.11).toFixed(2);
                            }else if( comissao == '4' && dados.valor_tributo == '10.00' ) {
                                $('.bombas option').show();    
                                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(dados.fator) * 1.11) * 1.12).toFixed(2);
                            }else if( comissao == '4' && dados.valor_tributo == '12.00') {
                                $('.bombas option').show();
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(dados.fator) * 1.09).toFixed(2);
                            }else if( comissao == '4' && dados.valor_tributo == '18.00') {
                                $('.bombas option').show();
                               var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) *  parseFloat(dados.fator) * 1.07).toFixed(2);
                            // ************************************
                            // ********* 5% de comissão ***********
                            // ************************************
                            }else if(comissao == '5' && dados.valor_tributo == '7.00'  ) {
                                $('.bombas option').show();
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(dados.fator) * 1.11).toFixed(2);                                
                                if( estado == 'AC' || estado == 'AP' || estado == 'AM' || estado == 'PA' || estado == 'RO' || estado == 'RR' || estado == 'TO'){
                                    valor_produto = valor_produto / 0.9798;
                                }
                            }else if(comissao == '5' && dados.valor_tributo == '10.00'  ) {
                                $('.bombas option').show();
                                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(dados.fator) * 1.11) *1.12).toFixed(2);
                                
                            }else if(comissao == '5' && dados.valor_tributo == '12.00' ) {
                                $('.bombas option').show();
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(dados.fator) * 1.09).toFixed(2);
                            }else if(comissao == '5' && dados.valor_tributo == '18.00' ) {
                                $('.bombas option').show();
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(dados.fator) * 1.07).toFixed(2);
                            // ************************************
                            // ********* 2% de comissão ***********
                            // ************************************
                            }else if(comissao == '2' && dados.valor_tributo == '7.00') {

                                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(dados.fator) * 1.11).toFixed(2);
                            }else if(comissao == '2' && dados.valor_tributo == '10.00') {

                                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(dados.fator) * 1.11) * 1.12).toFixed(2);
                            }else if(comissao == '2' && dados.valor_tributo == '12.00') {

                               
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(dados.fator) * 1.09).toFixed(2);
                            }else if(comissao == '2' && dados.valor_tributo == '18.00') {

                               
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(dados.fator) * 1.07).toFixed(2);
                            // ************************************
                            // ********* 0% de comissão ***********
                            // ************************************
                            }else if(comissao == '0' && dados.valor_tributo == '7.00') {

                               //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)).toFixed(2);
                               var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(dados.fator) * 1.11).toFixed(2);
                            }else if(comissao == '0' && dados.valor_tributo == '10.00') {

                               //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)).toFixed(2);
                               var valor_produto = Math.round(parseFloat(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(dados.fator) * 1.11) *1.12).toFixed(2);
                            }else if(comissao == '0' && dados.valor_tributo == '12.00') {
                               
                               var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(dados.fator) * 1.09 ).toFixed(2);
                            }else if(comissao == '0' && dados.valor_tributo == '18.00') {

                               
                               var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(dados.fator) * 1.07).toFixed(2);
                            }else if(comissao == 'TB2') {
                                
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(dados.fator)).toFixed(2);
                            }else if(comissao == 'TN' ) {
                                $('.bombas option').show();
                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(dados.fator) * 1.11).toFixed(2);
                                valor_produto = valor_produto / 0.9798;
                            }        

                            // Acréscimo de 20% sobre o valor final quando for para o nordeste
                            /*
                            if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
                                $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
                                $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
                                $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
                            {
                                valor_produto = valor_produto * 1.2;
                            }*/

                            /* Acréscimo de 12% para todos os estados */
                            //valor_produto = Math.round(valor_produto * 1.12);
                            valor_produto = Math.round(parseFloat(valor_produto) * 1.12);
                            /* Inserção do reajuste 30/11/2022 */                            
                            console.log($(this).attr('reajuste'));
                            if( $(this).attr('reajuste') > 0){
                                console.log($(this).attr('reajuste'));
                                valor_produto = Math.round(parseFloat(valor_produto) * (1 + parseFloat($(this).attr('reajuste') / 100))).toFixed(2);    
                            }else{
                                valor_produto = valor_produto.toFixed(2);
                            }

                            $(this).text(valor_produto);
                            $(this).unmask();
                            $(this).mask('##.##0,00');
                            $('#valor_unitario_'+indice).val(valor_produto);
                        }
                        
                        if($(this).attr('valor_base') != '' && $(this).attr('valor_base') != undefined && $(this).attr('tipo') == 'opcionais'){
                            valor_produto = Math.round(parseFloat($(this).attr('valor_base'))).toFixed(2);
                            $(this).text(valor_produto);
                            $(this).unmask();                            
                            $('#valor_unitario_'+indice).val(valor_produto);   

                        }   
                        indice++; 
                    });
                });
            }
        }

    });
    

}); 
    
function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');                
        $('tr[indice="'+indice+'"]').remove(); 
    });    
}

function mostra_valor()
{
    $('.bombas').bind(  'change', function(){
        indice = $(this).attr('indice'); 

        $('.valor_unitario_'+indice).attr('valor_base',$(this).find('option:selected').attr('valor_unitario'));  
        $('.valor_unitario_'+indice).attr('tipo',$(this).find('option:selected').attr('tipo'));
        $('.valor_unitario_'+indice).attr('reajuste',$(this).find('option:selected').attr('reajuste'));
        $('#valor_unitario_'+indice).val($(this).find('option:selected').attr('valor_unitario'));       
        var comissao = $("input[name='valor_desconto_orc']:checked").val();   
        if( comissao != 'outro' && comissao  != '' && comissao != undefined && $('#fator').val() != ''){
            
            atualiza_valores($('#estado').val());            
        }

        $('.salvar').attr('disabled', false);
    }); 

}

function atualiza_valores(estado){

    var comissao = $("input[name='valor_desconto_orc']:checked").val();
    $('#msg_produtos').fadeOut('fast');
    $('.esconde').fadeIn('slow');
    
    if(comissao != 'outro'){
    $.ajax({
        method: "POST",
        url: base_url+'AreaAdministradorComercial/verificaIcms',
        async: false,
        data: {     estado    :     estado      }
    }).done(function( data ) {
        var retorno = $.parseJSON(data);
        if(retorno.valor_tributo == '7.00'){
            $('#tn').fadeIn('slow');
        }else{
            $('#tn').fadeOut('slow');
            
        }

        
        $('#fator').val(retorno.fator);        
        //outro valor
        $('b.valor_unitario').each(function(){
            
            if($(this).attr('valor_base') != '' && $(this).attr('valor_base') != undefined && $(this).attr('tipo') != 'opcionais')  {                
                var indice = $(this).attr('indice');                
                if( comissao == '3' && retorno.valor_tributo == '7.00' ) {
                    $('.bombas option').show();            
                    //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)* 1.11).toFixed(2);
                }else if( comissao == '3' && retorno.valor_tributo == '10.00' ) {
                    $('.bombas option').show();            
                    //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                    var valor_produto = Math.round(parseFloat(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)* 1.11) * 1.12).toFixed(2);
                }else if( comissao == '3' && retorno.valor_tributo == '12.00' ) {
                    $('.bombas option').show();
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)* 1.09).toFixed(2);
                }else if( comissao == '3' && retorno.valor_tributo == '18.00' ) {
                    $('.bombas option').show();
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)* 1.07).toFixed(2);
                // ************************************
                // ********* 4% de comissão ***********
                // ************************************
                }else if( comissao == '4' && retorno.valor_tributo == '7.00' ) {
                    $('.bombas option').show();
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(retorno.fator)* 1.11).toFixed(2);
                }else if( comissao == '4' && retorno.valor_tributo == '10.00' ) {
                    $('.bombas option').show();
                    var valor_produto = Math.round(parseFloat(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(retorno.fator)* 1.11) *1.12).toFixed(2);
                }else if( comissao == '4' && retorno.valor_tributo == '12.00') {
                    $('.bombas option').show();
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(retorno.fator)* 1.09).toFixed(2);
                }else if( comissao == '4' && retorno.valor_tributo == '18.00') {
                    $('.bombas option').show();
                   var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(retorno.fator)* 1.07).toFixed(2);
                // ************************************
                // ********* 5% de comissão ***********
                // ************************************
                }else if(comissao == '5' && retorno.valor_tributo == '7.00'  ) {
                    $('.bombas option').show();
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(retorno.fator)* 1.11).toFixed(2);
                    if( estado == 'AC' || estado == 'AP' || estado == 'AM' || estado == 'PA' || estado == 'RO' || estado == 'RR' || estado == 'TO'){
                        valor_produto = valor_produto / 0.9798;
                    }
                }else if(comissao == '5' && retorno.valor_tributo == '10.00'  ) {
                    $('.bombas option').show();
                    var valor_produto = Math.round(parseFloat(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(retorno.fator)* 1.11) * 1.12).toFixed(2);
                    
                }else if(comissao == '5' && retorno.valor_tributo == '12.00' ) {
                    $('.bombas option').show();
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(retorno.fator)* 1.09).toFixed(2);
                }else if(comissao == '5' && retorno.valor_tributo == '18.00' ) {
                    $('.bombas option').show();
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(retorno.fator)* 1.07).toFixed(2);
                // ************************************
                // ********* 2% de comissão ***********
                // ************************************
                }else if(comissao == '2' && retorno.valor_tributo == '7.00') {
                    
                    //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)* 1.11).toFixed(2);
                }else if(comissao == '2' && retorno.valor_tributo == '10.00') {
                   
                    //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                    var valor_produto = Math.round(parseFloat(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)* 1.11) * 1.12).toFixed(2);
                }else if(comissao == '2' && retorno.valor_tributo == '12.00') {
                    
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)* 1.09).toFixed(2);
                }else if(comissao == '2' && retorno.valor_tributo == '18.00') {
                   
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)* 1.07).toFixed(2);
                // ************************************
                // ********* 0% de comissão ***********
                // ************************************
                }else if(comissao == '0' && retorno.valor_tributo == '7.00') {

                   //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)).toFixed(2);
                   var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)* 1.11).toFixed(2);
                }else if(comissao == '0' && retorno.valor_tributo == '10.00') {

                   //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)).toFixed(2);
                   var valor_produto = Math.round(parseFloat(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)* 1.11) * 1.12).toFixed(2);
                }else if(comissao == '0' && retorno.valor_tributo == '12.00') {

                   
                   var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)* 1.09).toFixed(2);
                }else if(comissao == '0' && retorno.valor_tributo == '18.00') {

                   
                   var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)* 1.07).toFixed(2);
                }else if(comissao == 'TB2') {
                     $('.bombas option').show();
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                }else if(comissao == 'TN'  ) {                    
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(retorno.fator)* 1.11).toFixed(2);
                    valor_produto = valor_produto / 0.9798;
                }               

                // Acréscimo de 20% sobre o valor final quando for para o nordeste
                /*if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
                    $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
                    $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
                    $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
                {
                    valor_produto = valor_produto * 1.2;
                }
                */
                /* Acréscimo de 12% no valor do produto geral 01/08/2022 */
                //valor_produto = Math.round(parseFloat(valor_produto * 1.12).toFixed(2));
                valor_produto = Math.round(parseFloat(valor_produto) * 1.12);
                /* Inserção do reajuste 30/11/2022 */
                
                if( $(this).attr('reajuste') > 0){
                
                    valor_produto = Math.round(valor_produto * (1 + parseFloat($(this).attr('reajuste') / 100))).toFixed(2);    
                }else{
                    valor_produto = valor_produto.toFixed(2);
                }
                
                $(this).text(valor_produto);
                $(this).unmask();
                $(this).mask('##.##0,00');
                $('#valor_unitario_'+indice).val(valor_produto);
            }

            if($(this).attr('valor_base') != '' && $(this).attr('valor_base') != undefined && $(this).attr('tipo') == 'opcionais'){
                valor_produto = Math.round(parseFloat($(this).attr('valor_base'))).toFixed(2);
                $(this).text(valor_produto);
                $(this).unmask();
                //$(this).mask('000.000.000,00');                
                $('#valor_unitario_'+indice).val($(this).attr('valor_base'));   

            }
            
            
        });
        
    });
    }
}

function isCNPJValid(cnpj) {  
            
    var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
    if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
        return false;
    for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
    if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
    if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    return true; 
}
