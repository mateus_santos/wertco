$('.datepicker').mask('99/99/9999');
$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
$('.datepicker').datepicker({
    format: 'dd/mm/yyyy'
    
});

$('#dt_fim').bind('focusout', function(){
	var dataFim = $('#dt_fim').val();
	if(dataFim != '' && dataFim != undefined){
		var partesData = dataFim.split("/");
		var dataFim = new Date(partesData[2], partesData[1] - 1, partesData[0]);

		var dataIni 	= 	$('#dt_ini').val();
		var partesData	= 	dataIni.split("/");
		var dataIni 	= 	new Date(partesData[2], partesData[1] - 1, partesData[0]);
		console.log(dataFim);
		console.log(dataIni);
		if( dataFim < dataIni ){
			swal({
				title: "Ops!",
				text: "DATA FINAL menor que a DATA INICIAL",
				type: 'warning'
			}).then(function() {
				$('#dt_fim').val('');
			}); 
		}
	}
});