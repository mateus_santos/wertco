$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
function excluirZonaAtuacao(id){

		var id = id;
		swal({
		  title: 'Excluir?',
		  text: "Deseja realmente excluir as áreas de atuação desse usuário?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Sim!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					method: "POST",
					url:  base_url+"AreaAdministrador/excluirZonaAtuacao", 
					async: false,
					data: { usuario_id 	: id }
				}).done(function(data) {
					var dados = $.parseJSON(data);		
					
					if(dados.retorno == 'sucesso'){				
						swal({
				   			title: "OK!",
				   			text: "Zonas de Atuação Excluído com sucesso",
				   			type: 'success'
				    	}).then(function() {
				    	   	window.location = base_url+'AreaAdministrador/zonaAtuacao';
				    	}); 
					}
				});	
			}
		})
	
}