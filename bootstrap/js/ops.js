$(document).ready(function() {
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#html_table').DataTable({
        "scrollX": true,
        "order": [
            [0, "desc"]
        ],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_    Resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": '<i class="la la-angle-double-right"></i>',
                "sPrevious": '<i class="la la-angle-double-left"></i>',
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });

    $('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
    $('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
    $('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
    $('.dataTables_filter').css('float', 'right');
    $('table thead').css('background-color', '#f4f3f8');
    $('table tbody tr:odd').addClass('zebraUm');
    $('table tbody tr:even').addClass('zebraDois');
    $("#alterar_status").bind('click', function() {
        op_id       =   $(this).attr('op_id');
        status_op   =   $("#status_op").val();
        $.ajax({
            method: "POST",
            url: base_url + "AreaAdministrador/alteraStatusOp",
            async: true,
            data: {
                op_id: op_id,
                status_op: status_op
            }
        }).done(function(data) {
            var dados = $.parseJSON(data);
            if (dados.retorno == 'sucesso') {
                swal({
                    title: "OK!",
                    text: "Status da op atualizado com sucesso",
                    type: 'success'
                }).then(function() {
                    window.location = base_url + 'AreaAdministrador/ops';
                });
            }
        });
    });

    $('#excluir_op_enviar').bind('click', function(){
        var id      = $(this).attr('op');
        var motivo  = $('#motivo').val();
        
        $.ajax({
            method: "POST",
            url: base_url + "AreaAdministrador/excluirOps",
            async: true,
            data: {
                id      :   id ,
                motivo  :   motivo
            }
        }).done(function(data) {

            var dados = $.parseJSON(data);
                                        
            swal({
                title:  dados.titulo,
                text:   dados.mensagem,
                type:   dados.status
            }).then(function() {
                window.location = base_url + 'AreaAdministrador/ops';
            });
            
        });
    });
});

function status(op_id) {
    $('.modal-status-title').text('Alterar Pedido #' + op_id);
    $('#alterar_status').attr('op_id', op_id);
    $("#m_modal_6").modal({
        show: true
    });
}

function finalizarPedido(pedido_id, email) {
    $('.modal-finalizar-title').text('Finalizar e Enviar E-mail do Pedido #' + pedido_id);
    $('#finalizar_pedido').attr('pedido_id', pedido_id);
    $('#email').val(email);
    $("#m_modal_7").modal({
        show: true
    });
}

function isEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function confirma_exclusao(id) {
    swal({
        title: 'Exclusão de OP',
        text: 'Tem certeza que deseja excluir esta OP?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: "swal2-confirm btn btn-danger m-btn m-btn--custom",
        confirmButtonText: "Excluir",
        cancelButtonText: "Cancelar"
    }).then(function(isConfirm) {
        if (isConfirm.value) {            
            $('#excluir_op_enviar').attr('op',id);            
            
            $("#excluir_op").modal({
                show: true
            });

        }
    });

}