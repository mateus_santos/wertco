$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('#cpf_cliente').mask('000.000.000-00');
	$('.porcentagem').mask('000');

	$('.add').bind('click', function(){

        var html = $('#modelo').clone();                
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
       
        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();                
        body+="</tr>";                
        $('.table').append(body);
        $('#modelo').attr('total_indice',indice);
		excluir();
		$('.porcentagem').unmask();
		$('.porcentagem').mask('000');    

    });

	$('.tecnico').bind('click', function(){
		
		if($(this).val() == 1){
			$('#tecnico_startup').fadeIn('slow');
			$('#tecnico_startup').attr('required',true);
			$('#tecnico_id').attr('required',true);

		}else{
			$('#tecnico_startup').fadeOut('slow');
			$('#tecnico_startup').attr('required',false);
			$('#tecnico_id').attr('required',false);

		}

	});

	$('#cpf_cliente').bind('focusout', function(){
		var cpf = $(this).val();
		if(cpf != ""){
			$.ajax({
				method: "POST",
				url: base_url+"clientes/verifica_cpf_apenas",
				async: true,
				data: { cpf 	: 	cpf }
			}).done(function( data ) {
				var dados = $.parseJSON(data);
				if(dados.status == 'erro'){
					swal(
				  		'Ops!',
				  		'Cpf inválido',
				  		'error'
					)
					$('#cpf_cliente').val('');	
				}				
			});
		}
	});

	$('#exampleSelect1').bind('change', function(){
		
		if($(this).val() == 'bandeira_branca')
		{	
			$('#arquivo_bb').fadeIn('slow');
			$('#pintura_descricao').fadeIn('slow');
			
		}else{
			$('#arquivo_bb').fadeOut('slow');
			$('#pintura_descricao').fadeOut('slow');
			
		}

	});

	$( "#orcamento" ).autocomplete({
		source: base_url+"AreaRepresentantes2/retornaOrcamentos",
		minLength: 3,
		select: function( event, ui ) {
			$('#orcamento_id').val(ui.item.id);			
			var orcamento_id = ui.item.id;
			$('#frete_id').val(ui.item.frete_id);			
			$('#observacao').val(ui.item.observacao);

			$.ajax({
				method: "POST",
				url:  base_url+"AreaRepresentantes2/retornaFormaPagto",
				async: true,
				data: { orcamento_id 	: 	orcamento_id }
			}).done(function(data) { 

				var dados = $.parseJSON(data);
				if(dados == null){
					swal({
						title: 	"Atenção!",
						text: 	'Altere a forma de pagamento no orçamento.',
						type: 	'warning'
					}).then(function() {					
						$('#descricao').val('');
						$('#orcamento_id').val('');
						$('#orcamento').val('');
					});	
				}else{
					
					$('#descricao').val(dados.descricao);
					$.ajax({
						method: "POST",
						url:  base_url+"AreaRepresentantes2/retornaProdutosOrcamentos",
						async: true,
						data: { orcamento_id 	: 	orcamento_id }
					}).done(function(data) {

						var dados = $.parseJSON(data);
						$('.produtos').fadeIn('slow');
						$('.novos_produtos').remove();
						$('#nome_cliente').val(dados[0]['contato_posto']);
						$('#empresa_id').val(dados[0]['empresa_id']);
						var valor_total 		= 	0.00;
						var valor_total_stx 	= 	0.00;
						for(var i=0;i<dados.length;i++)
						{					
							if( dados[i]['tipo_produto_id'] != 4 ){
								tipo 	= 	'text';
								value	=	'';
								valor_stx = parseFloat(dados[i]['valor']) / 1.05;
							}else{
								tipo  = 'hidden';
								value = 'opcional';
								valor_stx = dados[i]['valor'];
							}

							valor_total = (	dados[i]['valor'] * dados[i]['qtd']	) + parseFloat(valor_total);

							valor_total_stx = ( valor_stx * dados[i]['qtd']	) + parseFloat(valor_total_stx);

								var html = '<div class="form-group m-form__group row novos_produtos">';
									html += '	<div class="input-group m-input-group m-input-group--solid">';
									html += '			<div class="input-group-prepend">';
									html += '				<span class="input-group-text" id="basic-addon1">';
									html += dados[i]['codigo']+' - '+dados[i]['modelo']+' - '+dados[i]['descricao'];
									html += '				</span>';						
									html += '			</div>';
									html += '			<input type="'+tipo+'" class="form-control m-input" name="produtos[]" placeholder="Produtos" value="'+value+'" style="background: #fff;">';
									html += '			<input type="hidden" class="form-control m-input" name="produto_id[]" value="'+dados[i]['produto_id']+'" placeholder="Produtos" style="background: #fff;">';
									html += '			<input type="hidden" class="form-control m-input" name="qtd['+dados[i]['produto_id']+']" value="'+dados[i]['qtd']+'" placeholder="Produtos" style="background: #fff;">';
									html += '			<input type="hidden" class="form-control m-input" name="tipo_produto[]" value="'+dados[i]['tipo_produto_id']+'" placeholder="Produtos" style="background: #fff;">';
									html += '			<input type="hidden" class="form-control m-input" name="valor[]" value="'+dados[i]['valor']+'" placeholder="valor_produto" style="background: #fff;">';
									html += '			<input type="hidden" class="form-control m-input" name="valor_stx[]" value="'+valor_stx+'" placeholder="valor_produto" style="background: #fff;">';
									html += '		</div>';
									html += '	</div>';

								for(var j=0;j<dados[i]['qtd'];j++)
								{
									$('#produtos_cadastrar').append(html);
								}

						}

						$('#valor_total').val(valor_total);
						$('#valor_total_stx').val(valor_total_stx);
						
					});
				}
				
			});

			


		},
		response: function (event, ui) {
	        if (!ui.content.length) {
	            swal({
					title: "Atenção!",
					text: 'Selecione um orçamento para dar continuidade.',
					type: 'warning'
				}).then(function() {					
					$('#orcamento').val('');
				});

	        }

	    },
	    change: function(event,ui) {
          	if(ui.item == null){
          		$('#orcamento_id').val('');
          	}
      	}
    });

    $( "#tecnico_startup" ).autocomplete({
		source: base_url+"AreaRepresentantes2/retornaTecnicos",
		minLength: 3,
		select: function( event, ui ) {
			$('#tecnico_id').val(ui.item.id);
			
		}
    });


});

function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');                
        $('tr[indice="'+indice+'"]').remove(); 
    });    
}
