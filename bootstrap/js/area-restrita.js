$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	/*****************************************************************************************
	******************************************************************************************
	*********************************** Menu area-restrita ***********************************
	******************************************************************************************
	******************************************************************************************/
	
	$('.clientes_menu:not(li)').bind('click', function(){
		showMenu($(this),'#menu_clientes', '#img_clientes');
	});

	$('.rep_menu:not(li)').bind('click', function(){
		showMenu($(this),'#menu_rep', '#img_rep');
	});

	$('.tecnicos_menu:not(li)').bind('click', function(){
		showMenu($(this),'#menu_tecnicos', '#img_tecnicos');
	});

	$('.indicador_menu:not(li)').bind('click', function(){
		showMenu($(this),'#menu_indicador', '#img_indicador');
	});

});

function showMenu(obj,id_click, id_img){

		if($(obj).hasClass('inativo'))
		{
			$(id_click).slideDown('slow');
			$(id_img).attr('src',base_url+'bootstrap/img/up.png');
			$(obj).removeClass('inativo');
			$(obj).addClass('ativo');

		}else{
				
			$(id_click).slideUp('slow');
			$(id_img).attr('src',base_url+'bootstrap/img/down.png');
			$(obj).removeClass('ativo');
			$(obj).addClass('inativo');		
		}

	
}

function abrir(link){
	window.open(link);	
}