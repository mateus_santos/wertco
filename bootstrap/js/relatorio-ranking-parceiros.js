$(document).ready(function() {
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('.datepicker').mask('99/99/9999');	
	$('.datepicker').datepicker({
	    format: 'dd/mm/yyyy'	    
	});

	$('#produto').bind('change', function(){
		$('#modelo').val($('option:selected',this).text());
	});

	$(	"#parceiro" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaParceiros",
		minLength: 3,
		select: function( event, ui ) {
	
			$("#parceiro_id").val(ui.item.id);
	
		}
    });
});