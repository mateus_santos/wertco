$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);	
	
	$('#salvar').bind('click', function(){		

		var nr_serie_origem = 	$('#pedido_origem').find('option:selected').attr('nr_serie');
		var pedido_id 		= 	$('#pedido_origem').find('option:selected').attr('pedido_id');
		var status_id 		= 	$('#pedido_origem').find('option:selected').attr('status_pedido_id');
		
		if(nr_serie_origem == undefined && pedido_id == undefined){
			swal({
	   			title: "Atenção!",
	   			text: 'Selecione um modelo para alteração do nº de série',
	   			type: 'warning'
	    	}).then(function(){});	
		}else{
			var path = base_url+"AreaAdministrador/alterarNrSeriePorProduto";
			var id_disponivel = $('#nr_serie_final').val();

			if(id_disponivel == ''){
				swal({
		   			title: "Atenção!",
		   			text: 'Selecione um nr. de série disponível para alteração',
		   			type: 'warning'
	    		}).then(function(){});	
			}else{
				var id_origem = pedido_id;
				console.log(nr_serie_origem);
				if(nr_serie_origem != ''){
					var path = base_url+"AreaAdministrador/alterarNrSeriePorNrSerie";
					var id_origem = nr_serie_origem;
				}
				$.ajax({
					method: "POST",
					url: path,
					async: true,
					data: { id_origem 	: 	id_origem,
							id_destino 	: 	id_disponivel,
							pedido 		: 	$('#pedido_id').val(),
							status_id 	: 	status_id }
				}).done(function( data ) {
					var dados = $.parseJSON(data);
					if(dados.retorno = 'sucesso'){
						swal({
				   			title: "Ok!",
				   			text: 'Nr. de série alterado com sucesso',
				   			type: 'success'
			    		}).then(function(){
			    			window.location = base_url+'AreaAdministrador/editarNrSerie/'+$('#pedido_id').val();
			    		});	
					}else{
						swal({
				   			title: "Atenção!",
				   			text: 'Aconteceu um erro inesperado',
				   			type: 'warning'
			    		}).then(function(){
			    			window.location = base_url+'AreaAdministrador/editarNrSerie/'+$('#pedido_id').val();
			    		});	
					}
				});	
			}
		}		

	});
})

