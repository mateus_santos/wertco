function atualizarStatus(ativo,id)
{	
	if(ativo == 0){
		msg = "Usuário inativado com suceso.";
		type= "success";
	}else{
		msg = "Usuário ativado com suceso.";
		type= "success";
	}
	$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/alteraStatus", 
			async: false,
			data: { ativo	:	ativo,
					id		: 	id }
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso'){				
					swal({
               		title: "OK!",
               		text: msg,
               		type: type
		        	}).then(function() {
		        	   	window.location = base_url+'AreaAdministrador/gestaoUsuarios';
		        	}); 
				}
		});	
}