$(document).ready(function(){	
	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);		
	
	$('#estado').bind('change', function(){
		var estado_id = $('option:selected', this).attr('codigo');
		$.ajax({
			method: "GET",
			url:  "https://servicodados.ibge.gov.br/api/v1/localidades/estados/"+estado_id+"/municipios", 
			async: false			
		}).done(function(data) {			
			
			$('#cidades').empty();
			var option = "";
			for(var i=0;data.length > i; i++){
			
				option += "<option value='"+data[i].id+"' cidade='"+data[i].nome+"'> "+data[i].nome+"</option>";
			}

			$('#cidades').append(option);

		});	
	});

	$('.la-info').bind('click', function(){

		$("#m_modal_6").modal({
			show: true
		});

	});
	
	$("#btnLeft").click(function () {
    	var selectedItem = $("#selecionados option:selected");
    	selectedItem.each(function(){
    		$('input[value="'+$(this).attr('cidade')+'"]').remove();
    		$('input[value="'+$(this).val()+'"]').remove();
    	});
    	$("#cidades").append(selectedItem);
	});

	$("#btnRight").click(function () {
	    var selectedItem = $("#cidades option:selected");	         
	    var cidade = '';
	    var codigo = '';
	    var i = parseInt($('#div_selecionados').attr('indice')) + 1;

	    selectedItem.each(function(){
	    	cidade+='<input type="hidden" class="cidade_nova" name="cidade['+i+']" value="'+$(this).attr('cidade')+'">'; 
	    	codigo+='<input type="hidden" class="codigo_nova" name="codigo['+i+']" value="'+$(this).val()+'">'; 	    	
	    	$('#div_selecionados').attr('indice',i++);
	    });
	    $("#selecionados").append(selectedItem);
	    $("#div_selecionados").append(cidade);
	    $("#div_selecionados").append(codigo);
	    
	});

	$('#file').bind('change', function(){
		var arquivo = $(this).val().split('.');
		if( arquivo[1] != 'xls' ){
			swal({
	   			title:"Atenção!",
	   			text: "Formato do arquivo errado, por favor insira um arquivo do tipo XLS.",
	   			type: 'success'
	    	}).then(function() {
	    	   	$('#file').val('');
	    	}); 	
		}
	});

	$('#salvar').bind('click', function(){
		$('#selecionados option').prop('selected', true);
		var cidades = [];
		var codigos	= [];

		var arquivo = $('#file').val();

		$('#selecionados option').each(function(){
			cidades.push($(this).attr('cidade'));
			codigos.push($(this).val());
		});

		if(	(cidades.length > 0 &&  arquivo != '') || (cidades.length == 0 &&  arquivo != '')	){
			$.blockUI();
			$('#form_area').submit();
		}

		if( cidades.length > 0 && arquivo == '' ){
			$.ajax({
				method: "POST",
				url:  base_url+"AreaRepresentantes/salvarAreasAjax", 
				async: true,
				data: { cidades 	: 	cidades,
						codigos 	: 	codigos,
						estado 		: 	$('#estado').val() 	}		
			}).done(function(data) {
				var dados = $.parseJSON(data);

				if(	dados.retorno=='sucesso'	){
					swal({
			   			title:"Sucesso!",
			   			text: "Áreas salvas com sucesso!",
			   			type: 'success'
			    	}).then(function() {
			    	  window.location.href=base_url+"AreaRepresentantes/areaAtuacao";
			    	}); 
				}else{
					swal({
			   			title:"Ops!",
			   			text: "Algo deu errado!",
			   			type: 'warning'
			    	}).then(function() {
			    		window.location.href=base_url+"AreaRepresentantes/cadastraArea";  
			    	}); 
				}
			});	
		}	
		
	});
});