$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('#cpf').mask('000.000.000-00');
	$('#telefone').mask('(00) 0000 - 00000');
		
	$('#cpf').bind('focusout', function(){
		var cpf = $(this).val();
		if(cpf != ""){
			$.ajax({
				method: "POST",
				url: base_url+'clientes/verifica_cpf',
				data: { cpf 	: 	cpf },
				success: function( data ) {
					var dados = $.parseJSON(data);
					if(dados.status == 'erro'){
						swal(
							'Ops!',
							dados.mensagem,
							'error'
						).then(function() {
							$('#cpf').focus();
						});
					}
				}
			});
		}
	});
	
	$('#email').bind('focusout', function(){
		var email = $(this).val();
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test(email)){
			swal(
				'Ops!',
				'E-mail inválido',
				'error'
			).then(function() {
				$('#email').focus();
			});
		}
	});
	

	$( "#cliente" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaClienteWS",
		minLength: 3,
		select: function( event, ui ) {
			$('#cliente_id').val(ui.item.id);
			$('#endereco').val(ui.item.endereco);
			$('#cidade').val(ui.item.cidade);
			$('#tipo_cadastro_id').val(ui.item.tipo_cadastro_id);
			$('#fl_inadimplencia').val(ui.item.fl_inadimplencia);

			if( ui.item.fl_inadimplencia == 1 ){
				swal({
		   			title: "Atenção!",
		   			text:  "Cliente consta como inadimplente no sistema.",
		   			type:  'warning'
		    	}).then(function() {
		    	   	$('#tipo_id option[value=1]').remove();
		    	});
			}

			$.ajax({
				method: "POST",
				url: base_url+"AreaAdministrador/retornaUsuariosEmpresaWS",
				async: true,
				data: { empresa_id 	: 	ui.item.id },
				beforeSend: function(){
					$('#contato_id').empty();
				},
				success: function( data ) {
					var dados = $.parseJSON(data);
					if(dados.length > 0){
						var html='';
						for (var i = 0; i < dados.length; i++) {
							html+= '<option value="'+dados[i].id+'">'+dados[i].label.toUpperCase()+'</option>';
						}				
					} else {
						var html='<option value="0">Nenhum contato cadastrado</option>';
					}
					$('#contato_id').append(html);					
				}
			});
			
		}
    });


    $('#novo_usuario').bind('click', function(){
    	var cliente_id = $('#cliente_id').val();
    	if(cliente_id != '' || cliente_id != undefined)
    	{

    	}else{
    		swal(
		  		'Ops!',
		  		'Selecione um cliente',
		  		'warning'
			);
    	}
    });

});

function valida_form(){
	if($("#cliente").val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o cliente",
			type: "warning"
		}).then(function() {
			$("#cliente").focus();
		});
		return false;
	}
	if($("#cliente_id").val() == '') {
		swal({
			title: "Campo em branco",
			text: "Preencha o cliente",
			type: "warning"
		}).then(function() {
			$("#cliente").focus();
		});
		return false;
	}
	if($("#contato_id").val() == 0) {
		swal({
			title: "Campo em branco",
			text: "Informe o contato do cliente",
			type: "warning"
		}).then(function() {
			$("#contato_id").focus();
		});
		return false;
	}	
	//Cliente inadimplente e tipo de chamado igual a startup
	if( $("#fl_inadimplencia").val() == 1 && $('#tipo_id').val() == 1  ){
		swal({
			title: "Atenção",
			text: "Cliente inadimplente, impossível cadastrar um chamado de startup.",
			type: "warning"
		}).then(function() {
			$("#tipo_id").focus();
		});
		return false;
	}
	
}

function adicionar_funcionario(){
	if($('#cliente_id').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Selecione um cliente",
			type: "warning"
		});
		return;
	}
	if($('#nome').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o nome do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	
	if($('#email').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o email do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	if($('#telefone').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o telefone do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	$.ajax({
		method: "POST",
		url: base_url+'usuarios/cadastrar',
		async: true,
		data: {
			nome: $('#nome').val(),
			cpf: $('#cpf').val(),
			email: $('#email').val(),
			telefone: $('#telefone').val(),
			empresa_id: $('#cliente_id').val(),
			tipo_cadastro_id: $('#tipo_cadastro_id').val()
		},
		success: function( data ) {
			var dados = $.parseJSON(data);
			swal({
				title: dados.titulo,
				text: dados.texto,
				type: dados.tipo
			}).then(function() {
				console.log(dados);
				if(dados.tipo == 'success') {
					$("#contato_id option[value='0']").remove();
					$('#contato_id').append('<option selected value="'+dados.id+'">'+$('#nome').val()+'</option>');
				}
			});
		}
	});
}

function redireciona_configuracao(){

	if($("#cliente_id").val() == ''){
		swal({
			title: "Cliente não informe",
			text: "Informe o cliente para editar sua configuração",
			type: "warning"
		});
	} else {
		window.open(base_url+'AreaAdministrador/configuracaoEmpresa/'+$("#cliente_id").val(), '_blank').focus();
	}

}
    