$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('.date').datepicker({
        orientation: 'bottom',
        format: 'dd/mm/yyyy'
    });
    $('#cadastro').hide();    
    $('.add').bind('click', function(){

        var html = $('#modelo').clone();
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
        $(html).find('.bombas').attr('indice', indice);
        $(html).find('.valor_unitario').attr('indice', indice);
        $(html).find('.valor_unitario').attr('id', 'valor_unitario_'+indice);

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();
        body+="</tr>";
        $('.table').append(body);
        $('#modelo').attr('total_indice',indice);

        excluir();
        mostra_valor(indice);   

    });

    $('#formulario').submit(function(){
        $('#enviar').attr('disabled', 'disabled');
             
    });

    $('#cnpj').bind('focusout', function(){
        var cnpj = $(this).val(); 
        var tp_cadastro = $('.tp_cadastro:checked').val();
        var erro = 0;

        if(cnpj != ""){ 

            if(  tp_cadastro == 'PJ' ) {
                if(!isCNPJValid(cnpj)){
                    swal({
                        title: "Atenção!",
                        text: "CNPJ inválido!",
                        type: 'warning'
                    }).then(function() {
                        $('#cnpj').val('');
                    });
                    erro = 1;
                }else{
                    erro = 0;
                }
            }else if(tp_cadastro == 'PF'){
            
                $.ajax({
                    method: "POST",
                    url: base_url+'clientes/verifica_cpf_apenas',
                    async: true,
                    data: { cpf    :   cnpj }
                    }).done(function( data ) {
                        var dados = $.parseJSON(data);
                        if(dados.status == 'erro'){
                             swal({
                                title: "Atenção!",
                                text: "CPF inválido!",
                                type: 'warning'
                            }).then(function() {
                                $('#cnpj').val('');
                            });
                            erro = 1;
                        }else{
                            erro = 0;
                        }

                });                
            }

            if(  erro == 0 ){

            $.ajax({
                method: "POST",
                url: base_url+'clientes/verificaOrcamentoId',
                async: true,
                data: { cnpj    :   cnpj }
                }).done(function( data ) {
                    var dados = $.parseJSON(data);
                    if( dados.total == 0){

                        $.ajax({
                            method: "POST",
                            url: base_url+'clientes/verifica_cnpj',
                            async: false,
                            data: { cnpj    :   cnpj }
                            }).done(function( data ) {
                                var dados = $.parseJSON(data);                          
                                if(dados.length > 0){
                                    
                                    $('#razao_social').val(dados[0].razao_social);      
                                    $('#razao_social').attr('disabled', true);      
                                    $('#fantasia').val(dados[0].fantasia);      
                                    $('#fantasia').attr('disabled', true);
                                    $('#telefone').val(dados[0].telefone);
                                    $('#telefone').attr('disabled', true);
                                    $('#endereco').val(dados[0].endereco);      
                                    $('#endereco').attr('disabled', true);                                  
                                    $('#email').attr('disabled', true);
                                    $('#email').val(dados[0].email);
                                    $('#cidade').attr('disabled', true);
                                    $('#cidade').val(dados[0].cidade);  
                                    $('#estado').attr('disabled', true);
                                    $('#estado').val(dados[0].estado);                        
                                    $('#pais').attr('disabled', true);
                                    $('#pais').val(dados[0].pais);                        
                                    $('#salvar').val('0');
                                    $('#empresa_id').val(dados[0].id);
                                    $('#inscricao_estadual').attr('disabled', true);
                                    $('#inscricao_estadual').val(dados[0].insc_estadual);
                                    $('#cep').attr('disabled', true);
                                    $('#cep').val(dados[0].cep);
                                    $('#bairro').attr('disabled', true);
                                    $('#bairro').val(dados[0].bairro);
                                    
                                    
                                }else{

                                    if(tp_cadastro == 'PJ'){
                                        var cnpj_sem_mascara = cnpj.replace(/[^\d]+/g,'');

                                        $.ajax({
                                            method: "POST",
                                            url: base_url+'AreaAdministrador/ValidaCnpj',
                                            async: false,
                                            data: { cnpj    :   cnpj_sem_mascara }
                                        }).done(function( data ) {
                                            var dados = $.parseJSON(data);   
                                            
                                            if (dados.receita.erro == 0 && dados.receita.situacao == "ATIVA" ){
                                                $('#razao_social').attr('disabled', false);     
                                                $('#razao_social').val(dados.receita.retorno.razao_social);
                                                $('#razao_social').attr('style','border-color: #5fda17;');
                                                $('#fantasia').attr('disabled', false);
                                                $('#fantasia').val(dados.receita.retorno.nome_fantasia );
                                                $('#fantasia').attr('style','border-color: #5fda17;');
                                                $('#telefone').attr('disabled', false);
                                                $('#telefone').val(dados.receita.retorno.telefone);
                                                $('#telefone').attr('style','border-color: #5fda17;');
                                                $('#endereco').attr('disabled', false); 
                                                $('#endereco').val(dados.receita.retorno.logradouro+', '+dados.receita.retorno.numero+' '+dados.receita.retorno.complemento);                                
                                                $('#endereco').attr('style','border-color: #5fda17;');
                                                $('#email').attr('disabled', false); 
                                                $('#email').val('');                                                                                                
                                                $('#cidade').attr('disabled', false); 
                                                $('#cidade').val(dados.receita.retorno.municipio_ibge);  
                                                $('#cidade').attr('style','border-color: #5fda17;');
                                                $('#estado').attr('disabled', false); 
                                                $('#estado').val(dados.receita.retorno.uf);
                                                $('#estado').attr('style','border-color: #5fda17;');
                                                $('#bairro').attr('disabled', false); 
                                                $('#bairro').val(dados.receita.retorno.bairro);
                                                $('#bairro').attr('style','border-color: #5fda17;');
                                                $('#cep').attr('disabled', false); 
                                                $('#cep').val(dados.receita.retorno.cep);                          
                                                $('#cep').attr('style','border-color: #5fda17;');
                                                $('#pais').attr('disabled', false);
                                                $('#pais').attr('style','border-color: #5fda17;');                                                
                                                $('#inscricao_estadual').attr('disabled', false);
                                                $('#inscricao_estadual').val('');                        
                                                $('#cartao_cnpj').val(dados.receita.save);   
                                                $('#codigo_ibge_cidade').val(dados.receita.retorno.codigo_ibge);                  
                                                
                                                $('#pais').val('Brasil');         
                                                $('#salvar').val('1');
                                               
                                            }else{
                                               swal({
                                                    title: "Atenção!",
                                                    text: dados.msg,
                                                    type: 'warning'
                                                }).then(function() {
                                                    $('#cnpj').val('');
                                                });

                                            }
                                        });
                                    }
                                   

                                }                           
                        });  
                    }else{

                        /*swal({
                            title: "Atenção!",
                            text: "Já Existe um orçamento em andamento para este cliente, entre em contato com a WERTCO!",
                            type: 'warning'
                        }).then(function() {
                            $('#cnpj').val('');
                        }); */
                        swal({
                            title: 'Atenção',
                            text: 'Existe um orçamento (#'+dados.id+') em andamento para esse cliente, deseja acessá-lo?',
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonClass: "swal2-confirm btn btn-success m-btn m-btn--custom",
                            confirmButtonText: "Acessar Orçamento",
                            cancelButtonText: "Cancelar"
                        }).then(function(isConfirm) {
                            if (isConfirm.value) { 
                                window.open(base_url+'AreaAdministrador/visualizarOrcamento/'+dados.id);      
                                $('#cnpj').val('');
                            }else{
                                $('#cnpj').val('');
                            }
                        });
                   }
                });

            }
        
        }

       

    });

    $('.tp_cadastro').bind('click', function(){
        $('#cadastro').slideDown('slow');
        
        if( $(this).val() == 'PJ' ){
            $('#cadastro input').attr('disabled', false);
            $('#cnpj').mask('00.000.000/0000-00');
            $('#cnpj').attr('placeholder','INSIRA UM CNPJ');
            $('#razao_social').attr('placeholder','INSIRA UMA RAZÃO SOCIAL');
            $('#label_documento').text('CNPJ:');    
            $('#label_razao').text('RAZÃO SOCIAL:');
            $('#campo_fantasia').fadeIn('slow');
            $('#campo_ie').fadeIn('slow');
            $('#inscricao_estadual').attr('required',true);
            $('#fantasia').attr('required',true);

            $('#estadoE').removeAttr('name');
            $('#estado').attr('name','estado');
            $('#estadoE').fadeOut('slow');
            $('#estado').fadeIn('slow');

        }else if(   $(this).val() == 'PF'   ){
            $('#cadastro input').attr('disabled', false);
            $('#cnpj').mask('000.000.000-00');
            $('#cnpj').attr('placeholder','INSIRA UM CPF');
            $('#razao_social').attr('placeholder','INSIRA O NOME');
            $('#label_documento').text('CPF:');
            $('#label_razao').text('NOME:');
            $('#campo_fantasia').fadeOut('slow');
            $('#campo_ie').fadeOut('slow');
            $('#inscricao_estadual').removeAttr('required');
            $('#fantasia').removeAttr('required');

            $('#estadoE').removeAttr('name');
            $('#estado').attr('name','estado');
            $('#estadoE').fadeOut('slow');
            $('#estado').fadeIn('slow');

        }else{
            $('#cadastro input').attr('disabled', false);
            $('#cnpj').unmask();
            $('#cnpj').attr('placeholder','INSIRA ALGO PARA IDENTIFICAR');
            $('#razao_social').attr('placeholder','INSIRA UMA RAZÃO SOCIAL');
            $('#label_documento').text('ID. DOC:');    
            $('#label_razao').text('RAZÃO SOCIAL');
            $('#campo_fantasia').fadeIn('slow');
            $('#campo_ie').fadeOut('slow');
            $('#inscricao_estadual').attr('required',false);
            $('#fantasia').attr('required',false);            
            $('#estado').attr('required',false);

            $('#estadoE').attr('type','text');
            $('#estadoE').fadeIn('slow');
            $('#estadoE').attr('name','estado');
            $('#estado').attr('required',false);
            $('#estado').removeAttr('name');
            $('#estado').fadeOut('slow');    
            $('#estadoE').attr('readonly',true);
            $('#estadoE').val('EX');        
        }

    });
    
    //$('#cnpj').mask('00.000.000/0000-00');
    //$('#cpf').mask('000.000.000-00');
    $('#bombas').mask('00000'); 
    $('#bicos').mask('00000');  
    $('.qtd').mask('000');
    $('#cep').mask('00000-000');  

    $('.bombas').bind('change', function(){
        
        //$('#valor_unitario_0').val($(this).find(':selected').attr('valor_unitario')); 
        $('#valor_unitario_0').val(''); 
        $('#valor_unitario_0').mask('#.##0,00', {reverse: true});         
    }); 
    
    
    
    $( "#nome_indicador" ).autocomplete({
        source: base_url+"AreaAdministrador/retornaIndicadores",
        minLength: 2,
        select: function( event, ui ) {

            $('#indicador_id').val(ui.item.id);
            var indicador   =   ui.item.id;
            
        }
    });

    $('.indicacao').bind('click', function(){
        if($(this).val() == 1){
            $('.nome_indicador').fadeIn('slow');
            $('#nome_indicador').attr('required','required');

        }else{

            $('.nome_indicador').fadeOut('slow');
            $('#nome_indicador').removeAttr('required');
            $('#nome_indicador').val('');           
        }
    });

});
	
	
function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');                
        $('tr[indice="'+indice+'"]').remove(); 
    });    
}

function mostra_valor(indice)
{
    $('.bombas').bind('change', function(){
        
        //$('#valor_unitario_'+indice).val($(this).find(':selected').attr('valor_unitario')); 
        $('#valor_unitario_'+indice).val(''); 
        $('#valor_unitario_'+indice).mask('#.##0,00', {reverse: true});
    }); 

}

function isCNPJValid(cnpj) {  
    var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
    if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
        return false;
    for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
    if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
    if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    return true; 
};