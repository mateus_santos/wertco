$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	$("#alterar_status").bind('click', function(){

		chamado_id 		= 	$(this).attr('chamado_id');
		status_chamado	= 	$("#status_chamado").val();

		$.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/alteraStatusChamado", 
			async: true,
			data: {	id 			: 	chamado_id,
					status_id 	: 	status_chamado	}
		}).done(function(data) {
			var dados = $.parseJSON(data);		

			if(dados.retorno == 'sucesso')
			{
				swal({
		   			title: "OK!",
		   			text: "Status do chamado atualizado com sucesso",
		   			type: 'success'
		    	}).then(function() {
		    	   	window.location = base_url+'AreaAdministrador/chamados';
		    	});
			}
		});		
	});

	$(".geraPdf").bind('click', function(){

		var chamado_id = $(this).attr('chamado_id');

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/geraPedidosPdf/"+chamado_id+"/ajax/", 
			async: true,
			data: { chamado_id : chamado_id}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno != 'erro')
			{				
				window.open(base_url+'pedidos/'+dados.retorno);
			}

		});		

		
	});  

});

function status(chamado_id){
	
	$('.modal-status-title').text('Alterar chamado #'+ chamado_id);
	$('#alterar_status').attr('chamado_id', chamado_id);
	$("#m_modal_6").modal({
	    show: true
	});
	
}

function finalizarPedido(chamado_id,email){
	
	$('.modal-finalizar-title').text('Finalizar e Enviar E-mail do Pedido #'+ chamado_id);
	$('#finalizar_pedido').attr('chamado_id', chamado_id);
	$('#email').val(email);
	$("#m_modal_7").modal({
	    show: true
	});
	
}

function isEmail(email){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  	return re.test(email);

}

