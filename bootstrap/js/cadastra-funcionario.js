$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('#cpf').mask('000.000.000-00');	
	$('#celular').mask('(00) 00000 - 0000');
	$('#telefone').mask('(00) 0000 - 0000');

	$('#formulario').submit(function(){
			 
		$('#enviar').attr('disabled', 'disabled');
			 
	});	
	
	function isCNPJValid(cnpj) {  
	
		var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
		if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
			return false;
		for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
		if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
			return false; 
		for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
		if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
			return false; 
		return true; 
	};

	$('#cpf').bind('focusout', function(){
		var cpf = $(this).val();
		if(cpf != ""){
			$.ajax({
				method: "POST",
				url: base_url+'clientes/verifica_cpf',
				async: true,
				data: { cpf 	: 	cpf }
			}).done(function( data ) {
				var dados = $.parseJSON(data);
					if(dados.status == 'erro'){
						swal({
				   			title: "Ops!",
				   			text: dados.mensagem,
				   			type: 'warning'
				    	}).then(function() {
				    	   	$('#cpf').val('');
				    	});						
					} 
				
			});
		}
	});

	$('#email').bind('focusout', function(){
		var email = $(this).val();
		if(email != ""){
			$.ajax({
				method: "POST",
				url: base_url+'clientes/verifica_email',
				async: true,
				data: { email 	: 	email }
			}).done(function( data ) {
				var dados = $.parseJSON(data);
					if(dados.status == 'erro'){
						swal({
				   			title: "Ops!",
				   			text: dados.mensagem,
				   			type: 'warning'
				    	}).then(function() {
				    	   	$('#email').val('');	
				    	});

						
					}
				
			});
		}
	});

	$('#senha2').bind('focusout', function(){		
		var senha 	= 	$('#senha').val();
		var senha2 	= 	$('#senha2').val();
		if( senha != senha2 ){
			swal(
		  		'Ops!',
		  		'Senha não confere, tente novamente',
		  		'error'
			)
			$('#senha').val('');
			$('#senha2').val('');
		}	
	});
});