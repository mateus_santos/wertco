$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('#cpf_cliente').mask('000.000.000-00');
	$('.porcentagem').mask('000');
	$('.qtd_opcional').mask('000')
	$('.subitem').bind('click', function(){

		var indice 			= $(this).attr('indice');
        var opcional_indice = $(this).attr('opcional_indice');
        
        if(opcional_indice == 1){
        	$('#subitem-'+indice+'1').attr('required', true);
        }

        var sub_select 	= 	$('#subitem-'+indice+'1').clone();
        var id_novo 	= 	'subitem-'+indice;
        var id_subitem	= 	id_novo+(parseInt(opcional_indice)+1);
        $(sub_select).attr('id',id_novo+(parseInt(opcional_indice)+1));
        $(sub_select).attr('opcional_indice', parseInt(opcional_indice)+1);
        var sub_qtd 	= 	$('#subitem_qtd-'+indice+'1').clone();     
        $(sub_qtd).val('');
        var id_novo 	= 	'subitem_qtd-'+indice;   
        var id_qtd 		= 	id_novo+(parseInt(opcional_indice)+1);
        $(sub_qtd).attr('id',id_novo+(parseInt(opcional_indice)+1));
        
        $(this).before(sub_select);
        $(this).before(sub_qtd);

        $(this).attr('opcional_indice',parseInt(opcional_indice)+1);       

        var minus = '<i id="excluir-'+indice+'" class="fa fa-minus excluir-opcional" indice="'+indice+'" subitem="'+id_subitem+'" subitem_qtd="'+id_qtd+'" style="color: #ffcc00;margin: 10px; cursor: pointer;"></i>';
        $(this).before(minus);

        excluirOpcional();
        
        validaQtd();

        $(this).fadeOut('slow');
    });

	validaQtd();

	$( "#enviar" ).bind('click', function(){
		var erro = 0;
		$('.opicionais').each(function(){
			var opcional_id 	= 	$(this).attr('produto_id');
			var qtd 			= 	$(this).attr('qtd');
			var descricao		= 	$(this).attr('descricao');
			total_subitens 		= 	0;

			$('.subitens').each(function(){
				var valor = $(this).val().split('+');
				if(opcional_id == valor[0])
				{
					total_subitens++;
					
				}
			});

			
		});
		total_qtd_opcional = 0;
		
		$('.qtd_opcional').each(function(){
			var value = $(this).val();

			if(value == ''){
				value = 0;
			}
			total_qtd_opcional = total_qtd_opcional + parseInt(value) ;
			console.log(total_qtd_opcional);
		});
		console.log(total_qtd_opcional +'!='+ $('#total_opcional').val());
		if(total_qtd_opcional != $('#total_opcional').val() ){
			erro = 1;
		}	
		
		submeteForm(erro);

	});


});


function submeteForm(erro){
	if(erro == 0){
		
		$('#enviar-form').trigger('click');
	}
	//Nº de opcionais diferente do que está vinculado no pedido
	if(erro == 1){
		swal({
            title: "Atenção!",
            text:  "Nº de opcionais selecionados diferente do total contido no pedido!",
            type:  'warning'
        }).then(function() {            
            
        }); 	
	}
}

function validaQtd(){
	$('select').bind('change', function(){

		var indice = $(this).attr('indice');
		var opcional_indice = $(this).attr('opcional_indice');
		var id 		= $(this).attr('id');
		if( $(this).val() != ''){
			
			$('#adicionar-'+indice).fadeIn('slow');			
			valor = $(this).val();
			
			if(opcional_indice != 1){

				$('.subitens').each(function(){					
					var indice_atual = $(this).attr('indice');			
					var opcional_indice_atual = $(this).attr('opcional_indice');
					var opcional_indice_anterior = parseInt(opcional_indice) - 1;
					
					if( indice_atual == indice && opcional_indice_atual != opcional_indice){
						
						if( valor == $(this).val() ){							
			               
							swal({
			                    title: "Atenção!",
			                    text:  "Já foi selecionado este opcional para esta bomba!",
			                    type:  'warning'
			                }).then(function() {
			                	
			                    $('#'+id+' option:first').attr('selected',false);
			                    $('#'+id+' option:first').attr('selected',true);
			                    
			                }); 
						}
					}
				});			
			}
			$('#subitem_qtd-'+indice+opcional_indice).attr('required', true);
			$(this).attr('required', true);
			
		}else{
			
			$('#adicionar-'+indice).fadeOut('slow');			
			$('#subitem_qtd-'+indice+opcional_indice).attr('required', false);
			$(this).attr('required', false);
		}
	});
}

function excluirOpcional(){

	$('.excluir-opcional').bind('click', function(){
		subitem 	= $(this).attr('subitem');
		subitem_qtd = $(this).attr('subitem_qtd');
		indice 		= $(this).attr('indice');	

		$('#'+subitem).remove();
		$('#'+subitem_qtd).remove();
		$(this).remove();
		
		if($('select[indice='+indice+']').length == 1){
			
			if( $('select[indice='+indice+']').val() == ""){
				$('select[indice='+indice+']').attr('required',false);
				$('input[indice='+indice+']').attr('required',false);
			}
		}


	});
}

function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');                
        $('tr[indice="'+indice+'"]').remove(); 
    });    
}