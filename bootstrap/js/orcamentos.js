$(document).ready(function(){
	
	$('.receber_alerta').bind('click', function(){
		if( $(this).is(':checked') ){
			
			$('.esconde_alerta').fadeIn('slow');
		}else{
			$('.esconde_alerta').fadeOut('slow');
		}
	});

	$('.data').click(function(){
		console.log('here');
		$('.datepicker-dropdown').css({
			'z-index' : '999'
		});

	});


	var table = $('#html_table').DataTable({
		"scrollX": true,
		"order": [[ 0, "desc" ]],
		"iDisplayLength": 10,
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"processing": true,
		"serverSide": true,
		aoColumnDefs: [
		{ "width": "5%", "targets": 0 },
		{ "width": "30%", "targets": 1 },
		{ "width": "10%", "targets": 2 },
		{ "width": "20%", "targets": 3 },
		{ "width": "5%", "targets": 4 },
		{ "width": "20%", "targets": 5 },
		{ "width": "5%", "targets": 6 },
		{ "width": "5%", "targets": 7 },
		{ "width": "5%", "targets": 8 },
		{ "targets": [ 4,8 ],  "searchable": false}
		], 
		columns:[ {
			data: "orcamento_id"
		},{
			data: "solicitante"
		},{
			data: "emissao"
		},{
			data: "status"
		},{
			data: "andamento"
		},{
			data: "responsavel"
		},{
			data: "estado"
		},{
			data: "origem"
		},{
			data: "acoes"
		}],
		"ajax": {
			url: base_url+"AreaAdministrador/buscaOrcamentosSS",
			type : "POST",			
			data: function ( d ) {
                    d.zona 			= 	$('#zona').val();
                    d.representante = 	$('#representante').val();
                    d.indicadores 	= 	$('#indicadores').val();
                    d.estado 		= 	$('#estado').val();
                    d.periodo_ini	= 	$('#periodo_ini').val();
                    d.periodo_fim	= 	$('#periodo_fim').val();
                    d.status 		= 	$('#status').val();
                },
			error: function(retorno){
				console.log(retorno.responseText);
			},
		},    	
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_    Resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": '<i class="la la-angle-double-right"></i>',
				"sPrevious": '<i class="la la-angle-double-left"></i>',
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			}
		},
		
		"initComplete": function() {
			var $searchInput = $('#html_table_filter input');
			$searchInput.unbind();
			$searchInput.on({
			keyup : function(e) {
			    if(e.keyCode == 13) {
			      table.search(this.value).draw();
			      return;
			    }
			}
			});
        }
	}).search($('#pesquisa_ativa').val()).draw();
	//.search($('#pesquisa_ativa').val()).draw();

	/* **************** FILTROS ****************** */
	$('.filtro').bind('change', function(){
		table.ajax.reload();
	});

	$('#pesquiar_periodo').bind('click', function(){
		table.ajax.reload();		
	});

	$('#pesquiar_status').bind('click', function(){
		table.ajax.reload();
	});

	var seta = "<i class='fa fa-search' id='pesquiar' style='float: right;width: 15%;margin-top: 11px;'></i>";
	$('#html_table_filter label').append(seta);
	$('#html_table_filter input').attr('style','float: left;width: 80%;');
	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');

	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	$('.fa-search').bind('click', function(){
		table.search($('#html_table_filter input').val()).draw();
		//orcamentosFiltros(table, '1');
	});

	$('.novo_orcamento').bind('click', function(){
		if($('#usuario_id').val() == 7167){
			window.location.href = base_url+"AreaAdministradorComercial/cadastraOrcamentos/"+$('input[type="search"]').val();
		}else{	
			window.location.href = base_url+"AreaAdministrador/cadastraOrcamentos/"+$('input[type="search"]').val();
		}
	});

	$('.status').bind('click', function(){
		
		if( $(this).attr('status') == 'Solicitação de Orçamentos - Indicador' ){
			$('.aprova_indicacao').show();
			$('.status_orcamento_').hide();
			$('#alterar_status').attr('indicacao','1');
		}else{
			$('.aprova_indicacao').hide();
			$('.status_orcamento_').show();
			$('#alterar_status').attr('indicacao','0');
		}
	});

	
	$('.valor_unitario').mask('#.##0,00', {reverse: true});

	$("#alterar_status").bind('click', function(){

		orcamento_id 		= 	$(this).attr('orcamento_id');
		status_orcamento	= 	$("#status_orcamento").val();
		indicacao 			= 	$(this).attr('indicacao');
		aprovar_indicacao 	= 	$('input[name="aprovar_indicacao"]:checked').val();
		dt_previsao 		= 	$('#dt_previsao').val();
		erro = 0;
		if( status_orcamento == "" && aprovar_indicacao == 1 ){
			status_orcamento = 1;
		}else if( status_orcamento == "" && aprovar_indicacao == 0 ){
			status_orcamento = 8;
		}
		
		if(status_orcamento == 6){
			if(dt_previsao == ''){
				swal({
					title: "ATENÇÃO!",
					text: "Preencha a previsão de conclusão deste orçamento",
					type: 'warning'
				}).then(function() {
					//window.location = base_url+'AreaAdministrador/orcamentos';
				});
				erro = 1;
			}
		}
		if(erro == 0){
			$.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/alteraStatusOrcamento", 
				async: false,
				data: { orcamento_id 	: 	orcamento_id,
						status_orcamento 	: 	status_orcamento,
						indicacao 			: 	indicacao,
						aprovar_indicacao 	: 	aprovar_indicacao,
						dt_previsao			: 	dt_previsao }
			}).done(function(data) {
				var dados = $.parseJSON(data);		

				if(dados.retorno == 'sucesso')
				{
					swal({
						title: "OK!",
						text: "Status do orçamento atualizado com sucesso",
						type: 'success'
					}).then(function() {
						window.location = base_url+'AreaAdministrador/orcamentos';
					});
				}
			});		
			
		}
	});
	$("#adicionar_andamento").bind('click', function(){

		orcamento_id 		= 	$(this).attr('orcamento_id');
		status_orcamento_id	= 	$(this).attr('status_orcamento_id'); 
		andamento			= 	$("#andamento_texto").val();
		if( andamento == '')
		{
			swal({
				title: "Atenção!",
				text: "Insira alguma observação para ser adicionada ao processo desta venda!",
				type: 'warning'
			}); 

		}else{
			var dthr_alerta = data_alerta = '';
			if( $('.receber_alerta').is(':checked') ){
				if( $('.dthr_alerta').val() == '' || $('.dthr_alerta').val() == undefined ){
					swal({
						title: "Atenção!",
						text: "Insria uma data válida.",
						type: 'warning'
					}); 					
				}else{
					dthr_alerta = $('.dthr_alerta').val();
					var data_explode = dthr_alerta.split('/');					
					var data_alerta = data_explode[2]+"*"+data_explode[1]+"*"+data_explode[0];
				}
			}
			
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/insereAndamentoOrcamento", 
				async: true,
				data: { orcamento_id 		: 	orcamento_id,
						status_orcamento_id	: 	status_orcamento_id,
						andamento 			: 	andamento,
						dthr_alerta 		: 	data_alerta	}
				}).done(function(data) {
					var dados = $.parseJSON(data);		

					if(dados.retorno == 'sucesso')
					{
						swal(	{
							title: "OK!",
							text: "Observação adicionada ao andamento do orçamento #"+orcamento_id,
							type: 'success'
						}).then(function() {
							window.location = base_url+'AreaAdministrador/orcamentos';
						}	); 
					}
				});
			}
		});

	/* autocomplete responsável */
	$( "#responsavel" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaResponsavelOrcamentos",
		minLength: 1,
		select: function( event, ui ) {
			$('#responsavel_id').val(ui.item.id);
		}
	});

	$("#alterar_responsavel").bind('click', function(){
		orcamento_id 				=	$(this).attr('orcamento_id');
		responsavel_id 				= 	$("#responsavel_id").val();
		orcamento_responsavel_id = $(this).attr('responsavel_id');	
		status_orcamento_id = $(this).attr('status_orcamento_id');	

		if(responsavel_id == "")
		{
			swal({
				title: "Atenção!",
				text: "Selecione um representante!",
				type: 'warning'
			}); 

		}else{
			
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/alteraResponsavelOrcamento", 
				async: true,
				data: { orcamento_id 				: 	orcamento_id,
					responsavel_id				: 	responsavel_id,
					orcamento_responsavel_id 	: 	orcamento_responsavel_id,
					status_orcamento_id			: 	status_orcamento_id						}
				}).done(function(data) {
					var dados = $.parseJSON(data);		

					if(dados.retorno == 'sucesso')
					{				
						swal({
							title: "OK!",
							text: "Responsável por dar continuidade ao orçamento foi adicionado com sucesso.",
							type: 'success'
						}).then(function() {
							window.location = base_url+'AreaAdministrador/orcamentos';
						}); 
					}
				});		
			}	
		});

	$("#tour_virtual").bind('click', function(){

		if($('.status').length > 0){

			var tour;

			tour = new Shepherd.Tour({
				defaults: {
					classes: 'shepherd-element shepherd-open shepherd-theme-arrows',
					scrollTo: true
				}
			});

			tour.addStep('primeiro-step', {
				text: 'Área onde se encontram todos os orçamentos realizados no sistema WERTCO.',
				attachTo: '.m-datatable top',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.status click',
				showCancelLink: true,
				buttons: [
				{
					text: 'Próximo', 
					action: tour.next
				}
				]
			});

			tour.addStep('segundo-step', {
				text: 'Aqui você pode alterar o status do orçamento. Basta clicar no botão e selecionar o próximo passo da negociação.',
				attachTo: '.status top',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.andamento click',
				showCancelLink: true,
				buttons: [		    
				{
					text: 'Anterior',
					action: tour.back
				},
				{
					text: 'Próximo',
					action: tour.next
				}
				]
			});

			tour.addStep('terceiro-step', {
				text: 'Neste botão é possível visualizar o andamento da negociação e adicionar algum comentário/observação a esta negociação. <br/>Lembrando que apenas você e o responsável pelo orçamento, terão acesso ao andamento do mesmo.',
				attachTo: '.andamento_tour top',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.responsavel_tour click',
				showCancelLink: true,
				buttons: [		    
				{
					text: 'Anterior',
					action: tour.back
				},
				{
					text: 'Próximo',
					action: tour.next
				}
				]
			});

			tour.addStep('quarto-step', {
				text: 'Neste botão é possível Alterar o representante responsável por conduzir a venda.',
				attachTo: '.responsavel_tour top',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.visualizar click',
				showCancelLink: true,
				buttons: [		    
				{
					text: 'Anterior',
					action: tour.back
				},
				{
					text: 'Próximo',
					action: tour.next
				}
				]
			});

			tour.addStep('quinto-step', {
				text: 'Aqui pode ser visualizado o orçamento completo.<br/> Além disso, pode-se imprimir o orçamento, gerar desconto<br/> e emitir o orçamento para o cliente final.',
				attachTo: '.visualizar left',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.cadastrar_orcamento click',
				showCancelLink: true,
				buttons: [		    
				{
					text: 'Anterior',
					action: tour.back
				},
				{
					text: 'Próximo',
					action: tour.next
				}
				]
			});

			tour.addStep('sexto-step', {
				text: 'Clique neste botão para cadastrar um novo orçamento no sistema.',
				attachTo: '.cadastrar_orcamento top',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.cadastrar_orcamento click',
				showCancelLink: true,
				buttons: [		    
				{
					text: 'Anterior',
					action: tour.back
				},
				{
					text: 'Finalizar',
					action: tour.next
				}
				]
			});
			
			tour.start();
		}
	});
	
	$('.datepicker').datepicker({
        orientation: 'bottom',
        format: 'dd/mm/yyyy'
    });

	$("#status_orcamento").bind('change', function(){
		if($(this).val() == 6){
			$('.dt_previsao').slideDown('slow');			
		}else{
			$('.dt_previsao').slideUp('slow');
		}
	});
});

function pesquisar(){
	table.search($('#pesquisa_ativa').val()).draw();
	//orcamentosFiltros(table,'1');
}

function visualizar_orcamento(orcamento_id,usuario_id){
	
	var pesquisa 		= 	$('.dataTables_filter input').val(); 	
	console.log(usuario_id);
	if(usuario_id == 7167){

		window.open(base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id+'/'+pesquisa,'_blank');
	}else{
		window.open(base_url+'AreaAdministrador/visualizarOrcamento/'+orcamento_id+'/'+pesquisa,'_blank');
	}

}

function status(orcamento_id,dt_previsao){
	
	$('.modal-status-title').text('Alterar Status Orçamento #'+ orcamento_id);
	$('#alterar_status').attr('orcamento_id', orcamento_id);

	dt_previsao = dt_previsao.split('-');
	console.log(dt_previsao);
	$('#dt_previsao').val(dt_previsao[2]+'/'+dt_previsao[1]+'/'+dt_previsao[0]);
	$("#m_modal_6").modal({
		show: true
	});
	
}

function excluirOrcamento(orcamento_id){
	
	$('.modal-excluir-title').text('Excluir Orçamento #'+ orcamento_id);
	$('#orcamento_id').val(orcamento_id);
	$("#m_excluir_orcamento").modal({
		show: true
	});
	
}

function andamento(orcamento_id,status_orcamento_id){

	$('.modal-andamento-title').text('Andamento orçamento #'+ orcamento_id);
	$('#adicionar_andamento').attr('orcamento_id', orcamento_id);
	$('#adicionar_andamento').attr('status_orcamento_id', status_orcamento_id);

	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/buscaAndamentos", 
		async: true,
		data: { orcamento_id 	: 	orcamento_id}
	}).done(function(data) {

		var dados = $.parseJSON(data);	
		//$('.m-list-timeline__item').remove();				
		var html="";

		if( dados.length > 0 ){

			for( var i=0;i<dados.length;i++ ){ 
				var tipo = '';
				if( dados[i]['status_orcamento_id'] == 1)
				{
					tipo = 'secondary';
				}else if( dados[i]['status_orcamento_id'] == 2 ){
					tipo = 'success';
				}else if( dados[i]['status_orcamento_id'] == 3 ){
					tipo = 'danger';

				}else if( dados[i]['status_orcamento_id'] == 4 ){
					tipo = 'warning';

				}else if( dados[i]['status_orcamento_id'] == 5 ){
					tipo = 'info';

				}else if( dados[i]['status_orcamento_id'] == 6 ){
					tipo = 'primary';

				}

				var data_hora = dados[i]['dthr_andamento'].split(' ');
				var data = data_hora[0].split('-');
				var hora = data_hora[1];
				var data_andamento = data[2]+'/'+data[1]+'/'+data[0];
				html+= '<div class="m-list-timeline__item">';
				html+='<span class="m-list-timeline__badge m-list-timeline__badge--'+tipo+'"></span>';
				html+=' <span class="m-list-timeline__text">'+dados[i]['andamento']+'</span>';
				html+=' <span class="m-list-timeline__time">'+dados[i]['usuario']+' '+data_andamento+' '+hora+'</span>';
				html+='</div>';
			}
			$('.andamento_list > .m-list-timeline__items').empty();
			$('.andamento_list > .m-list-timeline__items').append(html);
		}else{
			$('.m-list-timeline__item').remove();
		}
	});

	$("#m_modal_7").modal({
		show: true
	});

}	

function responsavel(orcamento_id,responsavel_id, status_orcamento_id){
	
	$('.modal-responsavel-title').text('Alterar/Adicionar Responsável pelo Orçamento #'+ orcamento_id);
	$('#alterar_responsavel').attr('orcamento_id', orcamento_id);
	$('#alterar_responsavel').attr('responsavel_id', responsavel_id);
	$('#alterar_responsavel').attr('status_orcamento_id', status_orcamento_id);
	$("#m_modal_8").modal({
		show: true
	});

}

function orcamentosFiltros(table, where){
	//Destroy the old Datatable
	/*table.clear().destroy();

	table = $('#html_table').DataTable({
		"scrollX": true,
		"order": [[ 0, "desc" ]],
		"iDisplayLength": 10,
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"processing": true,
		"serverSide": true,
		aoColumnDefs: [
		{ "width": "5%", "targets": 0 },
		{ "width": "30%", "targets": 1 },
		{ "width": "10%", "targets": 2 },
		{ "width": "20%", "targets": 3 },
		{ "width": "5%", "targets": 4 },
		{ "width": "20%", "targets": 5 },
		{ "width": "5%", "targets": 6 },
		{ "width": "5%", "targets": 7 },
		{ "width": "5%", "targets": 8 },
		{ "targets": [ 4,8 ],  "searchable": false}
		], 
		columns:[ {
			data: "orcamento_id"
		},{
			data: "solicitante"
		},{
			data: "emissao"
		},{
			data: "status"
		},{
			data: "andamento"
		},{
			data: "responsavel"
		},{
			data: "estado"
		},{
			data: "origem"
		},{
			data: "acoes"
		}],
		"ajax": {
			url: base_url+"AreaAdministrador/buscaOrcamentosSS/TESTE",
			type : "POST",
			data : { "where" : where },						
			error: function(retorno){
				console.log(retorno.responseText);
			},
		},    	
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_    Resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": '<i class="la la-angle-double-right"></i>',
				"sPrevious": '<i class="la la-angle-double-left"></i>',
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			}
		},
		
		"initComplete": function() {
			var $searchInput = $('#html_table_filter input');
			$searchInput.unbind();
			$searchInput.on({
			keyup : function(e) {
			    if(e.keyCode == 13) {
			      table.search(this.value).draw();
			      return;
			    }
			}
			});
        }
	}).search($('#pesquisa_ativa').val()).draw();

	var seta = "<i class='fa fa-search' id='pesquiar' style='float: right;width: 15%;margin-top: 11px;'></i>";
	$('#html_table_filter label').append(seta);
	$('#html_table_filter input').attr('style','float: left;width: 80%;');
	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');

	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');
	*/

}



