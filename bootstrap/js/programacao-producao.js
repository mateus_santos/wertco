$(document).ready(function(){

	$('#dt_programacao, #dt_semana_nova1, #dt_semana_nova2').datepicker({
		format: 'dd/mm/yyyy',
    	daysOfWeekDisabled: [0,2,3,4,5,6]
	});
	
	// Esconde anos anteriores
	$('.linha_semana[ano="2020"]').slideUp('slow');
	$('.fa[ano="2020"]').removeClass('fa-arrow-down');
	$('.fa[ano="2020"]').addClass('fa-arrow-up');

	$('.linha_semana[ano="2021"]').slideUp('slow');
	$('.fa[ano="2021"]').removeClass('fa-arrow-down');
	$('.fa[ano="2021"]').addClass('fa-arrow-up');

	$('.linha_semana[ano="2022"]').slideUp('slow');
	$('.fa[ano="2022"]').removeClass('fa-arrow-down');
	$('.fa[ano="2022"]').addClass('fa-arrow-up');
	
	$('.linha_ano').bind('click', function(){
		var ano = $(this).attr('ano');
		if( $('.fa[ano="'+ano+'"]' ).hasClass('fa-arrow-up')){
			$('.fa[ano="'+ano+'"]').removeClass('fa-arrow-up');
			$('.fa[ano="'+ano+'"]').addClass('fa-arrow-down');
		}else if( $('.fa[ano="'+ano+'"]' ).hasClass('fa-arrow-down')){
			$('.fa[ano="'+ano+'"]').removeClass('fa-arrow-down');
			$('.fa[ano="'+ano+'"]').addClass('fa-arrow-up');
		}
		$('.linha_semana[ano="'+ano+'"]').toggle('slow');
	});

	$('#excluir_pedido').bind('click', function(){

		var pedido_id = $(this).attr('pedido_id');
		
		swal({
	        title: 'Tem certeza?',
	        text: 'Deseja realmente excluir esse pedido da programação?',
	        type: 'warning',
	        showCancelButton: true,
	        confirmButtonClass: "swal2-confirm btn btn-success m-btn m-btn--custom",
	        confirmButtonText: "Sim",
	        cancelButtonText: "Cancelar"
	    }).then(function(isConfirm) {

		  if (isConfirm.value) {
		  	$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/excluirPedidoPS", 
				async: false,
				data: { pedido_id : pedido_id }		
			}).done(function(data) {
				var dados = $.parseJSON(data);

				swal({
		   			title:"Ok!",
		   			text: "Pedido retirado da programação da produção.",
		   			type: 'success'
		    	}).then(function() {
		    		location.reload(); 
		    	}); 

			});	
		  }else{

		  
		  }

		});
	});

	if( $('#usuario_id').val() == 2465 	|| 
		$('#usuario_id').val() == 12 	|| 
		$('#usuario_id').val() == 134 	|| 
		$('#usuario_id').val() == 2 	|| 
		$('#usuario_id').val() == 645 	|| 
		$('#usuario_id').val() == 1951 ){
		
		$( ".area_pedidos" ).droppable({
	      revert: true
	    });
		
		$( ".pedidos" ).draggable({
			start: function( event, ui ) {
				$(this).removeClass('parado');
				$(this).addClass('movido');
				$(this).find('i.primeiro').attr('class','la la-arrows-alt');
			},
			
			revert: function(is_valid_drop,ui){

				if(!is_valid_drop){
					$(this).removeClass('movido');
					$(this).addClass('parado');	
					$(this).find('i.primeiro').attr('class','la la-edit');
				   	return true;

				} else {

				   	var bombas_semana 			= 	$(is_valid_drop).attr('bombas');
					var bicos_semana 			= 	$(is_valid_drop).attr('bicos');
					var bombas_regra 			= 	$(is_valid_drop).attr('total_bombas');	
					var bicos_regra 			= 	$(is_valid_drop).attr('total_bicos');
					var dt_semana_prog 			= 	$(is_valid_drop).attr('semana');
					var bicos_pedido 			= 	$(this).attr('bicos');	
					var bombas_pedido 			= 	$(this).attr('bombas');										
					var pedido_id 				= 	$(this).attr('pedido_id');				
					var programacao_semanal_id 	= 	$(this).attr('programacao_semanal_id');
					var	semana_atual			= 	$('#semana_atual').val();
					
					total_bombas 		= 	parseInt(bombas_semana)	+ 	parseInt(bombas_pedido);
					total_bicos  		= 	parseInt(bicos_semana) 	+ 	parseInt(bicos_pedido);	
						
					if( total_bombas > bombas_regra || total_bicos > bicos_regra  ){

						$(this).removeClass('movido');
						$(this).addClass('parado');			    				
						$(this).find('i.primeiro').attr('class','la la-edit');
						swal({
				   			title:"Atenção!",
				   			text: "Total de bicos ou de bombas maior que o limite permitido.",
				   			type: 'warning'
				    	}).then(function() {}); 	
						return true;	

					}else{
						var validacao = 0;

						if( $('#usuario_id').val() == 2465 	||
							 $('#usuario_id').val() == 2 	|| 
							 $('#usuario_id').val() == 645 	|| 
							 $('#usuario_id').val() == 1951  ){
							var semana_atual_date = new Date(semana_atual);
							var dt_semana_prog_date = new Date(dt_semana_prog);
							console.log(semana_atual_date.getTime() > dt_semana_prog_date.getTime())
							if( semana_atual_date.getTime() > dt_semana_prog_date.getTime()	 ){
								validacao = 1;

								swal({
						   			title:"Atenção!",
						   			text: "Semana Fechada para inclusão de pedidos",
						   			type: 'warning'
						    	}).then(function() {}); 

						    	return true;
							}
						}

						$(this).removeClass('movido');
						$(this).addClass('parado');			    				
						$(this).find('i.primeiro').attr('class','la la-check');
						var obj = $(this);
						if( validacao == 0 ){
							swal({
						        title: 'Tem certeza?',
						        text: 'Deseja realmente alterar a data de produção desse pedido?',
						        type: 'warning',
						        showCancelButton: true,
						        confirmButtonClass: "swal2-confirm btn btn-success m-btn m-btn--custom",
						        confirmButtonText: "Sim",
						        cancelButtonText: "Cancelar"
						    }).then(function(isConfirm) {

							  if (isConfirm.value) {
							  	$.ajax({
									method: "POST",
									url:  base_url+"AreaAdministrador/atualizarProgramacaoSemanal", 
									async: false,
									data: { programacao_semanal_id 	: 	programacao_semanal_id,
											nr_bombas 				: 	bombas_pedido,
											nr_bicos				: 	bicos_pedido,
											dt_semana_prog			: 	dt_semana_prog	}		
								}).done(function(data) {
									var dados = $.parseJSON(data);

									swal({
							   			title:"Ok!",
							   			text: "Data de produção alterada com sucesso.",
							   			type: 'success'
							    	}).then(function() {
							    		location.reload(); 
							    	}); 
			
								});	
							  }else{

							  	obj.removeClass('movido');
								obj.addClass('parado');			    				
								obj.addClass('borda-erro');
								obj.find('i.primeiro').attr('class','la la-warning');
							  	return true;
							  }

							});
						}				
					}

				}
			},
			stop: function(event, ui){}
		});	
	}
	//informação
	$('.la-info-circle').bind('click', function(){
		
		var pedido_id 	= 	$(this).parents('span.pedidos').attr('pedido_id');
		var bicos 		= 	$(this).parents('span.pedidos').attr('bicos');
		var bombas 		= 	$(this).parents('span.pedidos').attr('bombas');
		var programacao_semanal_id = $(this).parents('span.pedidos').attr('programacao_semanal_id');

		$('#alterar_dt').attr('programacao_semanal_id',programacao_semanal_id);
		console.log(programacao_semanal_id);
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/buscaDadosPedido",
			async: true,
			data: { pedido_id 	:  	pedido_id,
					prog_id 	: 	programacao_semanal_id}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			console.log(dados);
			$('#titulo').text('Pedido #'+pedido_id+' - '+dados[0].status);
			$('#cliente').text(dados[0].cliente);
			$('#cidade').text(dados[0].cidade);
			$('#bombas').text(bombas);
			$('#bicos').text(bicos);
			$('#modelos').empty();
			$('#dt_programacao').empty();
			$('#observacao_pedido').val('');
			$('#observacao_pedido').val(dados[0].observacao);

			for (var i = 0; i < dados.length; i++) {
				$('#modelos').append('<h5>'+dados[i].modelo+': <small class="text-muted">'+dados[i].qtd+'</small></h5>');

			}			
			$('#visualizar_status').attr('pedido_id', pedido_id);
			$('#visualizar_status').attr('programacao_id', dados[0].programacao_id);
			if( dados[0].status_id == 7 ){
				$('#excluir_pedido').fadeIn('slow');
				$('#excluir_pedido').attr('pedido_id',pedido_id);
			}else{
				$('#excluir_pedido').fadeOut('slow');
			}
			$("#info-pedido").modal({
				show: true
			});

		});
	});

	$('#observacao_pedido').bind('focusout', function(){
		var pedido_id 	= 	$('#visualizar_status').attr('pedido_id');
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/alteraObservacaoPedidoPP",
			async: false,
			data: { pedido_id	:  pedido_id,
					observacao 	: $(this).val()	}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			console.log(dados);
			if(dados.retorno == 'sucesso'){
				swal({
		   			title:"Ok!",
		   			text: "Observação alterada com sucesso.",
		   			type: 'success'
		    	}).then(function() {
		    		
		    		$("#info-pedido").modal('hide');
					$('#observacao_pedido').val('');
		    	}); 
			}

		});	
	});

	if( $('#usuario_id').val() == 2465 	|| 
		  $('#usuario_id').val() == 12 	|| 
		  $('#usuario_id').val() == 134 || 
		  $('#usuario_id').val() == 645 || 
		  $('#usuario_id').val() == 1951){
		$('#alterar_dt').bind('click', function(){
			var dt_semana_prog = $('#dt_programacao').val();
			if( dt_semana_prog == '' ){
				swal({
		   			title:"Atenção!",
		   			text: "Preencha a nova data de alteração.",
		   			type: 'warning'
		    	}).then(function() {
		    		$('#dt_programacao').focus();
		    	}); 				
			}else{

				var programacao_semanal_id 	= 	$(this).attr('programacao_semanal_id');
				dt_programacao 				= 	dt_semana_prog.replace("/", "*");
				dt_programacao 				= 	dt_programacao.replace("/","*");

				$.ajax({
					method: "POST",
					url:  base_url+"AreaAdministrador/alteraDataInexistePedido",
					async: true,
					data: {	programacao_semanal_id 	:  	programacao_semanal_id,
							dt_semana_prog 			: 	dt_programacao 	}
				}).done(function(data) {
					var dados = $.parseJSON(data);

					if(dados.retorno == 'sucesso'){
						swal({
				   			title:"OK!",
				   			text: "Data de programação alterada.",
				   			type: 'success'
				    	}).then(function() {
				    		location.reload(); 
				    	}); 	
					}else if(dados.retorno == 'data existente'){					
						swal({
				   			title:"Atenção!",
				   			text: "Semana já existente, arraste o pedido para a semana desejada.",
				   			type: 'warning'
				    	}).then(function() {
				    		location.reload(); 
				    	}); 
					}
				
				});
			}

		});

		$('.la-cut').bind('click', function(){
			var pedido_id 	= 	$(this).parents('span.pedidos').attr('pedido_id');
			var bicos 		= 	$(this).parents('span.pedidos').attr('bicos');
			var bombas 		= 	$(this).parents('span.pedidos').attr('bombas');
			var cliente 	= 	$(this).parents('span.pedidos').attr('title');
			var dt_semana_pedido 		= $(this).parents('span.pedidos').attr('semana');
			var programacao_semanal_id 	= $(this).parents('span.pedidos').attr('programacao_semanal_id');

			$('#titulo_divisao').empty();
			$('#titulo_divisao').append('Dividir Pedido <b>'+pedido_id+' - '+cliente+'</b>');
			$('#bicos_divisao').text(bicos);
			$('#bombas_divisao').text(bombas);		
			$('#dividir_pedido').attr('programacao_semanal_id',programacao_semanal_id);
			$('#dividir_pedido').attr('pedido_id',pedido_id);
			$('#dividir_pedido').attr('bicos',bicos);
			$('#dividir_pedido').attr('bombas',bombas);
			$('#dividir_pedido').attr('dt_semana_pedido',dt_semana_pedido);
			$("#dividir-pedido").modal({
				show: true
			});	

			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/buscaModelosPedido", 
				async: true,
				data: { pedido_id 	: 	pedido_id	}		
			}).done(function(data) {
				var dados = $.parseJSON(data);
				var option = '';
				
				$('.modelo_1, .modelo_2').empty();

				for (var i = dados.length - 1; i >= 0; i--) {
					option+='<option value="'+dados[i].nr_bicos+'">'+dados[i].modelo+'</option>';
					
				}

				$('.modelo_1, .modelo_2').append(option);
				

			});	

		});
		// copia e cola o modelo e a qtd de bombas
		copiaModeloQtd(1);
		copiaModeloQtd(2);
	}

	if( $('#usuario_id').val() == 2465 	|| 
		$('#usuario_id').val() == 12 	|| 
		$('#usuario_id').val() == 134 	|| 
		$('#usuario_id').val() == 2 	|| 
		$('#usuario_id').val() == 1951	){

		$('#dividir_pedido').bind('click', function(){

			var bicos 					= 	parseInt($(this).attr('bicos'));
			var bombas 					= 	parseInt($(this).attr('bombas'));
			var pedido_id 				= 	$(this).attr('pedido_id');
			var programacao_semanal_id 	= 	$(this).attr('programacao_semanal_id');
			var dt_semana_pedido 		= 	$(this).attr('dt_semana_pedido');

			var bicos1 = 0;
			var bombas1 = 0;
			var modelo1 = '';
			var modelo2 = '';
			$( ".modelo_1" ).each(function() {
				var indice = $(this).attr('indice');		
				bicos1+= parseInt($(this).val()) * parseInt($('.qtd_1[indice='+indice+']').val());
				bombas1+= parseInt($('.qtd_1[indice='+indice+']').val());
				modelo1+= $('.modelo_1 option:selected').text()+' qtd: '+parseInt($('.qtd_1[indice='+indice+']').val())+'<br/>';
			});

			var bicos2 	= 0;
			var bombas2 = 0;
			var modelo2 ='';
			
			$( ".modelo_2" ).each(function() {
				var indice = $(this).attr('indice');		
				bicos2+= parseInt($(this).val()) * parseInt($('.qtd_2[indice='+indice+']').val());
				bombas2+= parseInt($('.qtd_2[indice='+indice+']').val());
				modelo2+= $('.modelo_2 option:selected').text()+' qtd: '+parseInt($('.qtd_2[indice='+indice+']').val())+'<br/>';
			});
			
			/*var bicos1 					= 	parseInt($('#bicos_quebra1').val());
			var bombas1 				= 	parseInt($('#bombas_quebra1').val());
			var bicos2 					= 	parseInt($('#bicos_quebra2').val());
			var bombas2 				= 	parseInt($('#bombas_quebra2').val())
			*/

			var dt_semana_nova1 		= 	$('#dt_semana_nova1').val();	
			var dt_semana_nova2 		= 	$('#dt_semana_nova2').val();
			
			var dt_programacao1 		= 	dt_semana_nova1.replace("/", "*");
			dt_programacao1				= 	dt_programacao1.replace("/","*");

			var dt_programacao2 		= 	dt_semana_nova2.replace("/", "*");
			dt_programacao2				= 	dt_programacao2.replace("/","*");

			var total_bicos_div = parseInt(bicos1) + parseInt(bicos2);
			var total_bomba_div = parseInt(bombas1) + parseInt(bombas2);
			
			if( total_bicos_div > bicos || total_bomba_div > bombas ){
				swal({
		   			title:"Atenção!",
		   			text: "Número de bicos ou de bombas maior que o contido no pedido.",
		   			type: 'warning'
		    	}).then(function() {
		    		$('#bicos_divisao').attr('style','font-size: 16px; color: #ffcc00 !important;font-weight: 600;');
		    		$('#bombas_divisao').attr('style','font-size: 16px; color: #ffcc00 !important;font-weight: 600;');
		    	}); 	
			}else{			
				//Verifica o total de bicos para a semana1 desejada
				if( verificaBicosBombasSemana(dt_programacao1, bicos1, bombas1, dt_semana_pedido) === 0){
					if(verificaBicosBombasSemana(dt_programacao2, bicos2, bombas2, dt_semana_pedido) === 0 ){
						
						//Quebra em dois o pedido e exclui o pedido principal
						$.ajax({
							method: "POST",
							url:  base_url+"AreaAdministrador/quebraPedidoProgramacao",
							async: true,
							data: {	programacao_semanal_id	: 	programacao_semanal_id,
									pedido_id				: 	pedido_id,
									bicos1 					: 	bicos1,
									bombas1 				: 	bombas1,
									dt_semana_nova1 		: 	dt_programacao1,
									modelo1 				: 	modelo1,
									bicos2 					: 	bicos2,
									bombas2 				: 	bombas2,
									dt_semana_nova2 		: 	dt_programacao2,
									modelo2 				: 	modelo2	}
						}).done(function(data) {
							var dados = $.parseJSON(data);
							if( dados.retorno ){
								swal({
						   			title:"OK!",
						   			text: "Pedido dividido em dois, verifique se a data foi inserida corretamente.",
						   			type: 'success'
						    	}).then(function() {
						    		location.reload();
						    	}); 
							}else{			
						    	swal({
						   			title:"Atenção!",
						   			text: "Houve algum problema na execução, verifique com o Webmaster.",
						   			type: 'warning'
						    	}).then(function() {
						    		
						    	});
							}
						});
					}else{
						var dt_semana_format 	= 	dt_programacao2.replace("*", "/");
						dt_semana_format		= 	dt_semana_format.replace("*","/");
						swal({
				   			title:"Atenção!",
				   			text: "Número de bicos ou bombas maior que o disponível na semana "+dt_semana_format,
				   			type: 'warning'
				    	}).then(function() {
				    		$('#dt_semana_nova2').val('');
				    	}); 
					}
				}else{
					var dt_semana_format 	= 	dt_programacao1.replace("*", "/");
					dt_semana_format		= 	dt_semana_format.replace("*","/");
					swal({
			   			title:"Atenção!",
			   			text: "Número de bicos ou bombas maior que o disponível na semana "+dt_semana_format,
			   			type: 'warning'
			    	}).then(function() {
			    		$('#dt_semana_nova1').val('');
			    	}); 
				}
				
			}
			
		});
	}

	$('#visualizar_status').bind('click', function(){

		var pedido_id = $(this).attr('pedido_id');
		$('#pedido_id').val(pedido_id);
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/buscaNrSeriePorPedido",
			async: false,
			data: {	pedido_id :	pedido_id 	}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			$('#nr_serie_id').empty();
			$('#nr_serie_id').append(dados.combo);

		});

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/buscaStatusProducao",
			async: false,
			data: {	pedido_id :	pedido_id 	}			
		}).done(function(data) {
			var dados = $.parseJSON(data);
			$('#status_producao_id').empty();
			$('#status_producao_id').append(dados.combo);

		});

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/buscaStatusPedidosProducao",
			async: false,
			data: {	pedido_id :	pedido_id 	}			
		}).done(function(data) {
			var dados = $.parseJSON(data);
			$('#conteudo_status').empty();			
			$('#conteudo_status').append(dados.html);

		});

		$('#status-pedido').modal('show');
		
	});

	$('#enviar_status').bind('click', function(){
		var pedido_id 	= $('#pedido_id').val();
		var nr_serie_id = $('#nr_serie_id').val();
		var status_producao_id = $('#status_producao_id').val();
		var observacao 			= 	$('#observacao').val();
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/insereAndamentoProducao",
			async: true,
			data: {	pedido_id 			: 	pedido_id,
					nr_serie_id 		: 	nr_serie_id,
					status_producao_id 	: 	status_producao_id,
					observacao 			: 	observacao 	}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			if(dados.retorno == 'sucesso'){
				swal({
		   			title:"Ok!",
		   			text: "Status da Bomba Modificada com sucesso",
		   			type: 'success'
		    	}).then(function() {
		    		window.location.href= base_url+"AreaAdministrador/programacaoProducao";
		    	}); 
			}

		});
	})
});

function verificaBicosBombasSemana( dt_semana, bicos, bombas, dt_semana_pedido ){
	var retorno = '';
	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/verificaBicosBombasDt",
		async: false,
		data: {	dt_semana_prog		: 	dt_semana,
				bicos 				: 	bicos,
				bombas 				: 	bombas,
				dt_semana_pedido 	:  	dt_semana_pedido}
	}).done(function(data) {
		var dados = $.parseJSON(data);
		if( dados.retorno === true){
			retorno = 0;
		}else{			
	    	retorno = 1;	
		}
	});
	console.log(retorno);
	return retorno;
}

function copiaModeloQtd(indice){
	$('#add_div_'+indice).bind('click', function(){
		var indice_atu 	= 	parseInt($(this).attr('indice')) + 1;
		$(this).attr('indice', indice_atu);
		var html = $('#copiar_'+indice).clone();
		$(html).find('select').attr('indice', indice_atu);
		$(html).find('input').attr('indice', indice_atu);
		$(html).find('.la-minus').attr('style','display: block;float: right;margin-right: 20px;margin-top: 10px;color: #ffcc00;font-weight: 600;');	
		$(html).find('.la-minus').attr('onclick','excluir('+indice_atu+')');
		$(html).find('.la-minus').attr('indice',indice_atu);
		var html_copia = '<div class="col-lg-12 col-xs-6" style="display: inline;">'+$(html).html()+'</div>';
		$('#colar_'+indice).append(html_copia);

	});
}

function excluir(indice){
	$('select[indice="'+indice+'"]').fadeOut('slow', function() {
		$(this).remove();
	});

	$('input[indice="'+indice+'"]').fadeOut('slow', function() {
		$(this).remove();
	});

	$('i.la-minus[indice="'+indice+'"]').fadeOut('slow', function() {
		$(this).remove();
	});
}