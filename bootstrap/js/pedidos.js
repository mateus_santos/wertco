$(document).ready(function(){	
	
	/*if( $('#nr_nf').val() != '' ){
		$('#enviar_nf').hide();
	}*/

	$('.date').datepicker({
		orientation: 'bottom',
		format: 'dd/mm/yyyy'
	});

	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');

	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	$('#enviar_reprovacao').bind('click',function(){
		var pedido_id  	= 	$(this).attr('pedido_id');
		var motivo 		= 	$('#motivo_reprovacao_insp').val();
		$.ajax({
			method: "POST", 
			url:  base_url+"AreaQualidade/avisoProducaoReprovado", 
			async: true,
			data: { pedido_id 		: 	pedido_id,
					motivo 			: 	motivo}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			if(dados.retorno == 'sucesso'){
				swal({
		   			title: "OK!",
		   			text: "Foi enviado um e-mail para o setor de produção informando a reprovação na inspeção",
		   			type: 'success'
		    	}).then(function() {
		    	   	window.location = base_url+'AreaAdministrador/pedidos';
		    	});	
			}
		});
	});

	$("#alterar_status").bind('click', function(){

		pedido_id 				= 	$(this).attr('pedido_id');
		status_pedido			= 	$("#status_pedido").val();
		cliente 				= 	$(this).attr('cliente');
		status_atual 			= 	$(this).attr('status_atual');
		tipo_acesso 			= 	$(this).attr('tipo_acesso');
		ordem 					= 	$(this).attr('ordem');
		fl_identidade_visual 	=  	$(this).attr('fl_identidade_visual');
		bandeira 				=  	$(this).attr('bandeira');

		if( tipo_acesso == 'qualidade' && status_pedido == 4 ){
			$('#enviar_reprovacao').attr('pedido_id',pedido_id);

			$('#m_reprovacao_inspecao').modal({
	    		show: true
			});			
		
		}else if( tipo_acesso == 'qualidade' && status_pedido == 14){
			$('#pedido_id_embalagem').val(pedido_id);

			$('#m_upload_foto_pedido').modal({
	    		show: true
			});				
				
		}else if(status_atual == 2 && status_pedido == 16){
			
			if(bandeira == 'BANDEIRA_BRANCA' || bandeira == 'bandeira_branca' || bandeira == 'WERTCO'){
				var texto_msg = "As informações referentes a Identidade Visual desse pedido são as seguintes: <br><b>";
				if(fl_identidade_visual == 'A'){
					texto_msg+=" A DEFINIR!<br>";
				}
				if( fl_identidade_visual == 'S' ){
					texto_msg+=" POSSUI IDENTIDADE VISUAL E O ARQUIVO ESTÁ ANEXADO AO PEDIDO!<br>";	
				}
				if( fl_identidade_visual == 'N' ){
					texto_msg+=" NÃO POSSUI IDENTIDADE VISUAL!<br/>";	
				}
				texto_msg+=" </b>Confirma essas informações?";

				swal({
			        title: 'Identidade Visual',
			        html: texto_msg,
			        type: 'warning',
			        showCancelButton: true,
			        confirmButtonClass: "swal2-confirm btn btn-success m-btn m-btn--custom",
			        confirmButtonText: "Sim",
			        cancelButtonClass: "swal2-confirm btn btn-warning m-btn m-btn--custom",
			        cancelButtonText: "Não, alterar a identidade visual",
			        width: '35rem'

				}).then(function(isConfirm) {
				    if (isConfirm.value) {
				       verificaOps(status_pedido,pedido_id, base_url+'AreaAdministrador/confirmaPedidoOps/'+pedido_id, cliente, 'confirmar');

				    }else{
				    	listarPintura(pedido_id, cliente, status_pedido, tipo_acesso, bandeira, fl_identidade_visual);
				    }
				});
			}else{
				verificaOps(status_pedido,pedido_id, base_url+'AreaAdministrador/confirmaPedidoOps/'+pedido_id, cliente, 'confirmar');
			}

		}else if(status_pedido == "anterior"){
			
			$('#enviar_retorno').attr('pedido_id',pedido_id);
			$('#enviar_retorno').attr('ordem',ordem);
			$('#m_retorno_status').modal({
				show: true
			});	

		}else if(status_pedido == 11){
			$.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/verificaOpsPedido", 
				async: false,
				data: { 	pedido_id 		: 	pedido_id 	}
			}).done(function(data) {
				var dados = $.parseJSON(data);	
				
				if(dados.length == 0){
					swal({
			   			title: "ATENÇÃO!",
			   			text: "Para avançar esse pedido é necessário gerar as op's!",
			   			type: 'warning'
			    	}).then(function() {
			    		window.location = base_url+'areaAdministrador/confirmaPedidoOps/'+pedido_id;
			    	});				
		    	}else{
		    		$.ajax({
						method: "POST", 
						url:  base_url+"AreaAdministrador/alteraStatusPedido", 
						async: true,
						data: { pedido_id 		: 	pedido_id,
								status_pedido 	: 	status_pedido}
					}).done(function(data) {
						var dados = $.parseJSON(data);		

						if(dados.retorno == 'sucesso')
						{
							swal({
					   			title: "OK!",
					   			text: "Status do pedido atualizado com sucesso",
					   			type: 'success'
					    	}).then(function() {
					    	   	window.location = base_url+'AreaAdministrador/pedidos';
					    	});
						}
					});		
		    	}	

		    });
		/*}else if(status_pedido == 5){
			$.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/buscaNfePedido", 
				async: false,
				data: { 	pedido_id 		: 	pedido_id 	}
			}).done(function(data) {
				var dados = $.parseJSON(data);	

				anexarNfe(pedido_id,dados.empresa_id, dados.nr_nf, dados.arquivo_nfe, dados.prazo_entrega, dados.dt_emissao_nf );

			});*/

		}else{
			$.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/alteraStatusPedido", 
				async: true,
				data: { pedido_id 		: 	pedido_id,
						status_pedido 	: 	status_pedido}
			}).done(function(data) {
				var dados = $.parseJSON(data);		

				if(dados.retorno == 'sucesso')
				{
					swal({
			   			title: "OK!",
			   			text: "Status do pedido atualizado com sucesso",
			   			type: 'success'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaAdministrador/pedidos';
			    	});
				}
			});		
		}
	});

	$('#enviar_retorno').bind('click', function(){
		if($('#motivo_retorno').val() != ''){
			var motivo 		=	$('#motivo_retorno').val();	 	
			var ordem 		= 	$(this).attr('ordem');
			var pedido_id 	= 	$(this).attr('pedido_id');
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/retornaStatusAnterior",
				async: true,
				data: { pedido_id 	: 	pedido_id,
						ordem 		: 	ordem,
						motivo 		: 	motivo }
			}).done(function(data) {
				var dados = $.parseJSON(data);

				if(dados.retorno == 'sucesso'){
					swal({
			   			title: "OK!",
			   			text: "Status do pedido retornado. E-mails foram enviados para o setor reponsavel.",
			   			type: 'success'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaAdministrador/pedidos';
			    	});
				}else{
					swal({
			   			title: "ATENÇÃO!",
			   			text: "Status do pedido retornado. E-mails foram enviados para o setor reponsavel.",
			   			type: 'warning'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaAdministrador/pedidos';
			    	});
				}

			});
		}else{
			swal({
	   			title: "ATENÇÃO!",
	   			text: "Insira um motivo para retornar o status do pedido",
	   			type: 'warning'
	    	}).then(function() {
	    	   	
	    	});

		}
	})
	
	/* autocomplete responsável */
	$( "#responsavel" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaRepresentantes",
		minLength: 1,
		select: function( event, ui ) {
			$('#responsavel_id').val(ui.item.id);
		}
    });

    $('#finalizar_pedido').bind('click', function(){
    	var pedido_id = $(this).attr('pedido_id');
    	var email = $('#email').val();

    	if(isEmail(email)){

	    	$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/confirmaEnviaEmailPedido/", 
				async: true,
				data: { pedido_id : pedido_id,
						email 	  : email	}
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{				
					swal({
			   			title: "OK!",
			   			text: "Status do pedido atualizado com sucesso",
			   			type: 'success'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaAdministrador/pedidos';
			    	});
				}else{
					swal({
			   			title: "Atenção!",
			   			text: dados.retorno+". Entre em contato com o webmaster!",
			   			type: 'success'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaAdministrador/pedidos';
			    	});
				}

			});	

		}else{
			swal({
	   			title: "Atenção!",
	   			text: "E-mail Inválido",
	   			type: 'warning'
	    	}).then(function() {
	    	   	//window.location = base_url+'AreaAdministrador/pedidos';
	    	});
		}
    });

    $("#tour_virtual").bind('click', function(){
    	
    	if($('.status').length > 0){

	    	var tour;
	  
			tour = new Shepherd.Tour({
			  defaults: {
			    classes: 'shepherd-element shepherd-open shepherd-theme-arrows',
			    scrollTo: true
			  }
			});

			tour.addStep('primeiro-step', {
			  text: 'Área onde se encontram todos os pedidos realizados no sistema WERTCO.',
			  attachTo: '.m-datatable top',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.status click',
			  showCancelLink: true,
			  buttons: [
			    {
			      text: 'Próximo', 
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('segundo-step', {
			  text: 'Aqui você pode alterar o status dos pedidos. Basta clicar no botão e selecionar a situação atual do pedido.',
			  attachTo: '.status top',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.visualizar click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Próximo',
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('terceiro-step', {
			  text: 'Clicando nesse botão o sistema irá gerar um arquivo pdf, contendo todas as informações do pedido.',
			  attachTo: '.visualizar left',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.editar click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Próximo',
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('quarto-step', {
			  text: 'Aqui você poderá alterar as informações contidas no pedido.',
			  attachTo: '.editar left',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.gerar_ops click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Próximo',
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('quinto-step', {
			  text: 'Clicando neste botão é possível confirmar o pedido e gerar as ordens de produção do mesmo.',
			  attachTo: '.gerar_ops left',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.cadastrar_orcamento click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Próximo',
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('sexto-step', {
			  text: 'Clique neste botão para cadastrar um novo pedido no sistema.',
			  attachTo: '.cadastrar_orcamento top',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.cadastrar_orcamento click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Finalizar',
			      action: tour.next
			    }
			  ]
			});
			
			tour.start();
		}
    });
       
    $(".fl_identidade_visual").bind('click', function(){
		if($(this).val() == 'S'){
			$('#arquivo_pintura').fadeIn('slow');
			$('#arquivo_pintura').attr('required', true);
		}else {
			$('#arquivo_pintura').attr('required', false);
			$('#arquivo_pintura').fadeOut('slow');
		}
	});

});

function listarFotos(pedido_id, cliente){

	$.ajax({
		method: "POST", 
		url:  base_url+"AreaAdministrador/listarFotosPedido", 
		async: true,
		data: { pedido_id 		: 	pedido_id }
	}).done(function(data) {
		var dados = $.parseJSON(data);
		$('.fotos_pedido_titulo').empty();
		$('.fotos_pedido_titulo').text('Fotos Pedido #'+pedido_id+' Cliente:'+cliente);		
		$('#fotos_pedido').empty();
		$('#fotos_pedido').append(dados.html);
	});

	$('#m_foto_pedido').modal({
		show: true
	});	
	
}


function listarPintura(pedido_id, cliente, status, acesso, bandeira, fl_identidade_visual){
	if(fl_identidade_visual == 'A'){
		$('#fl_idv_a').attr('checked',true);
		$('#fl_idv_s').attr('checked', false);
		$('#fl_idv_n').attr('checked', false);
	}else if(fl_identidade_visual == 'S'){
		$('#fl_idv_s').attr('checked',true);
		$('#fl_idv_a').attr('checked', false);
		$('#fl_idv_n').attr('checked', false);
	}else if(fl_identidade_visual == 'N'){
		$('#fl_idv_n').attr('checked',true);
		$('#fl_idv_a').attr('checked', false);
		$('#fl_idv_s').attr('checked', false);
	}

	var coresTesteiraFrontal = [
    {
        text: "BRANCO TOTAL",
        value: "BRANCO TOTAL cod.: 10007312",
        selected: false,
        description: "cod.: 10007312",
        imageSrc: base_url+"bootstrap/img/branco.jpg"
    },
    {
        text: "BRANCO NEVE",
        value: "BRANCO NEVE cod.: JA211B",
        selected: false,
        description: "cod.: JA211B",
        imageSrc: base_url+"bootstrap/img/branco_neve.jpg"
    },
    {
        text: "BRANCO GELO",
        value: "BRANCO GELO cod.: 10005081",
        selected: false,
        description: "cod.: 10005081",
        imageSrc: base_url+"bootstrap/img/branco_gelo.jpg"
    },
    {
        text: "CINZA N8",
        value: "CINZA N8 cod.: PC748B",
        selected: false,
        description: "cod.: PC748B",
        imageSrc: base_url+"bootstrap/img/cinza_n8.jpg"
    },
    {
        text: "CINZA N6,5",
        value: "CINZA N6,5 cod.: JL016B",
        selected: false,
        description: "cod.: JL016B",
        imageSrc: base_url+"bootstrap/img/cinza_n6.jpg"
    },
    {
        text: "CINZA 7037",
        value: "CINZA 7037 cod.: 7037",
        selected: false,
        description: "cod.: 7037",
        imageSrc: base_url+"bootstrap/img/cinza_7037.jpg"
    },
    {
        text: "PRATA + VERNIZ",
        value: "PRATA + VERNIZ cod.: 100044871",
        selected: false,
        description: "cod.: 100044871",
        imageSrc: base_url+"bootstrap/img/prata+verniz.jpg"
    },
    {
        text: "RAL 9001",
        value: "RAL 9001 cod.: 10005061",
        selected: false,
        description: "cod.: 10005061",
        imageSrc: base_url+"bootstrap/img/ral.jpg"
    },
    {
        text: "Bege",
        value: "Bege cod.: JD012B",
        selected: false,
        description: "cod.: JD012B",
        imageSrc: base_url+"bootstrap/img/bege.jpg"
    },
    {
        text: "AMARELO SKOL",
        value: "AMARELO SKOL cod.: HY005B",
        selected: false,
        description: "cod.: HY005B",
        imageSrc: base_url+"bootstrap/img/amarelo_skol.jpg"
    },
    {
        text: "AMARELO OURO",
        value: "AMARELO OURO cod.: 10005738",
        selected: false,
        description: "cod.: 10005738",
        imageSrc: base_url+"bootstrap/img/amarelo_ouro.jpg"
    },
    {
        text: "LARANJA LISO BR",
        value: "LARANJA LISO BR cod.: PBF006",
        selected: false,
        description: "cod.: PBF006",
        imageSrc: base_url+"bootstrap/img/laranja.jpg"
    },
    {
        text: "VERMELHO",
        value: "VERMELHO cod.: PBG002",
        selected: false,
        description: "cod.: PBG002",
        imageSrc: base_url+"bootstrap/img/vermelho.jpg"
    },
    {
        text: "MARROM FOSCO",
        value: "MARROM FOSCO cod.:  M-3600",
        selected: false,
        description: "cod.: M-3600",
        imageSrc: base_url+"bootstrap/img/marrom.jpg"
    },
    {
        text: "VERDE ESCURO",
        value: "VERDE ESCURO cod.: PBK041",
        selected: false,
        description: "cod.: PBK041",
        imageSrc: base_url+"bootstrap/img/verde_escuro.jpg"
    },
 	{
        text: "AZUL LISO BR",
        value: "AZUL LISO BR cod.: EJ002B",
        selected: false,
        description: "cod.: EJ002B",
        imageSrc: base_url+"bootstrap/img/azul_liso.jpg"
    },   
	{
        text: "AZUL MARINHO",
        value: "AZUL MARINHO cod.: PBJ125",
        selected: false,
        description: "cod.: PBJ125",
        imageSrc: base_url+"bootstrap/img/azul_marinho.jpg"
    },  
    {
        text: "AZUL 5015",
        value: "AZUL 5015 cod.: 10005009",
        selected: false,
        description: "cod.: 10005009",
        imageSrc: base_url+"bootstrap/img/azul_5015.jpg"
    },  
    {
        text: "BRANCO AZULADO",
        value: "BRANCO AZULADO cod.: IC004B",
        selected: false,
        description: "cod.: IC004B",
        imageSrc: base_url+"bootstrap/img/branco_azulado.jpg"
    }];

    var coresLateral = [
    {
        text: "BRANCO TOTAL",
        value: "BRANCO TOTAL cod.: 10007312",
        selected: false,
        description: "cod.: 10007312",
        imageSrc: base_url+"bootstrap/img/branco.jpg"
    },
    {
        text: "CINZA 7037",
        value: "CINZA 7037 cod.: 7037",
        selected: false,
        description: "cod.: 7037",
        imageSrc: base_url+"bootstrap/img/cinza_7037.jpg"
    }];
    
	$.ajax({
		method: "POST", 
		url:  base_url+"AreaAdministrador/listarPinturaPedido",
		async: true,
		data: { pedido_id 		: 	pedido_id }
	}).done(function(data) {
		var dados = $.parseJSON(data);
		
		$('.modal-listar-pintura-title').empty();
		$('.modal-listar-pintura-title').text('Pintura Pedido #'+pedido_id+' Cliente:'+cliente);
		$('#listar-pintura').empty();
		$('#listar-pintura').append(dados.html);
		$('#testeira').empty();
		$('#painel_lateral').empty();
		$('#painel_frontal').empty();
		$('#p_testeira').html('<select id="testeira" class="form-control testeira_frontal"></select>');
		$('#p_lateral').html('<select id="painel_lateral" class="form-control" ></select>');
		$('#p_frontal').html('<select id="painel_frontal" class="form-control testeira_frontal"></select>');

		if(dados.existe == 1){
			$('#arquivo_pintura').attr('required', false);
			$('#descricao_pintura').attr('required', false);
		}else{
			$('#arquivo_pintura').attr('required', true);
			$('#descricao_pintura').attr('required', true);
		}

		var selectedTesteira = 0;
	    $.each(coresTesteiraFrontal, function (key, item) {
	       
	        if( item.value == dados.pedido.testeira ){
	        	selectedTesteira = key;
	        }
	    });

	    var selectedPainelFrontal = 0;
	    $.each(coresTesteiraFrontal, function (key, item) {
	        //console.log(item);        
	        if( item.value == dados.pedido.painel_frontal ){
	        	selectedPainelFrontal = key;
	        }
	    });

	    var selectedPainelLateral = 0;
	    $.each(coresTesteiraFrontal, function (key, item) {
	        //console.log(item);        
	        if( item.value == dados.pedido.painel_lateral ){
	        	selectedPainelLateral = key;
	        }
	    });
		
		$('#testeira').ddslick({
		    data: coresTesteiraFrontal,
		    width: 220,		    
		    height: 350,
		    imagePosition: "left",
		    defaultSelectedIndex: selectedTesteira,
		    onSelected: function (data) {	        
		        $('#v_testeira').val(data.selectedData.value);
		    }
		});

		$('#painel_frontal').ddslick({
		    data: coresTesteiraFrontal,
		    width: 220,		    
		    height: 350,
		    imagePosition: "left",
		    defaultSelectedIndex: selectedPainelFrontal,
		    onSelected: function (data) { 
		        $('#v_painel_frontal').val(data.selectedData.value);
		    }
		});

		$('#painel_lateral').ddslick({
		    data: coresTesteiraFrontal,
		    width: 220,	
		    height: 350,    
		    imagePosition: "left",
		    defaultSelectedIndex: selectedPainelLateral,
		    onSelected: function (data) {
				$('#v_painel_lateral').val(data.selectedData.value);
		    }
		});
	});

	if( acesso == 'administrador geral' 
		|| acesso == 'administrador tecnico' 
		|| (acesso == 'administrador comercial' 
			&& (status == 1 || status == 2 || status == 9 || status == 10 || status == 16)) ){
		$('#alterar_pintura').fadeIn('slow');
	}else{
		$('#alterar_pintura').fadeOut('slow');
	}

	$('#pedido_id_pintura').val(pedido_id);

	if( bandeira == 'bandeira_branca' ||  bandeira == 'BANDEIRA_BRANCA' ){
		$('.bomba_pintura').fadeIn('slow');
	}else{
		$('.bomba_pintura').fadeOut('slow');
	}

	if(fl_identidade_visual == 'A'){
		$('#fl_idv_a').attr('checked',true);
		$('#fl_idv_s').attr('checked', false);
		$('#fl_idv_n').attr('checked', false);
	}else if(fl_identidade_visual == 'S'){
		$('#fl_idv_s').attr('checked',true);
		$('#fl_idv_a').attr('checked', false);
		$('#fl_idv_n').attr('checked', false);
	}else if(fl_identidade_visual == 'N'){
		$('#fl_idv_n').attr('checked',true);
		$('#fl_idv_a').attr('checked', false);
		$('#fl_idv_s').attr('checked', false);
	}

	$('#m_listar_pintura').modal({
		show: true
	});	
	
}

function verificaOps(status,pedido_id, caminho, cliente, tipo){
	
	if(	status == 10 && tipo == 'editar'){
		swal({
   			title: "Atenção!",
   			text: "Pedido confirmado pelo gestor, não pode mais ser alterado!",
   			type: 'warning'
    	}).then(function() { });

	}else if(status == 9 && tipo == 'confirmar'){
		swal({
   			title: "Atenção!",
   			text: "Gestor já foi alertado sobre a confirmação desse pedido!",
   			type: 'warning'
    	}).then(function() {
    	   	
    	});
    }else if((status == 1 || status == 9) && tipo == 'editar'){
    	window.location = caminho;

	}else{
		//valida se existe op's geradas dessa op
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/verificaOpsPedido/", 
			async: true,
			data: { pedido_id : pedido_id }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			console.log(dados); 
			if(dados.length > 0)
			{
				swal({
		   			title: "Atenção!",
		   			text: "Pedido disponível para importação, não pode ser alterado!",
		   			type: 'warning'
		    	}).then(function() {
		    	   	//window.location = base_url+'AreaAdministrador/pedidos';
		    	});
		    }else{
		    	if( status == 10 || tipo == 'confirmar'){
		    		//window.location = caminho;
		    		$.ajax({
						method: "POST",
						url:  base_url+"AreaAdministrador/verificaCombustiveis/", 
						async: true,
						data: { pedido_id : pedido_id }
					}).done(function(data) {
						var dados = $.parseJSON(data);	
						if(dados.total > 0){
							swal({
								title: "Atenção!",
								text:  "Existem combustíveis em branco ou a definir, corrija os combustíveis antes de enviar para produção!",
								type: 'warning'
							}).then(function() {
								
							});	
						}else{

							$.ajax({
								method: "POST",
								url:  base_url+"AreaAdministrador/liberaPedidoProducao/", 
								async: true,
								data: { pedido_id : pedido_id }
							}).done(function(data) {
								var dados = $.parseJSON(data);	

								if(dados.retorno == 'sucesso')
								{
									swal({
										title: "Ok!",
										text:  "A produção foi avisada que o pedido "+pedido_id+" foi liberado para geração de op's.",
										type: 'success'
									}).then(function() {
										window.location = base_url+'AreaAdministrador/pedidos';
									});	
								}else if(dados.retorno == 'erro pintura'){
									swal({
										title: "Atenção!",
										text:  "Preencha corretamente os dados de pintura antes de enviar o pedido para produção.",
										type: 'warning'
									}).then(function() {
										//window.location = base_url+'AreaAdministrador/pedidos';
										/*$('#m_listar_pintura').modal({
											show: true
										});	*/
										listarPintura(pedido_id, cliente, status, $('#tipo_acesso').val());
									});
								}else if(dados.retorno != 'sucesso' && dados.retorno != 'erro inicial'){
									swal({
										title: "Atenção!",
										text:  "Houve um problema no envio automático do aviso da produção, entre em contato com o setor pessoalmente para dar prosseguimento no processo.",
										type: 'warning'
									}).then(function() {
										window.location = base_url+'AreaAdministrador/pedidos';
									});	
								}
							});

						}
					});


		    	}else{
		    		//solicitaAprovacao(pedido_id, cliente);
		    	}
		    }
		});		
	}
}

function solicitaAprovacao(pedido_id, cliente){

	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/solicitaAprovacao/", 
		async: true,
		data: { pedido_id 	: 	pedido_id,
				cliente 	: 	cliente }
	}).done(function(data) {
		var dados = $.parseJSON(data);		
		
		if(dados.retorno == 'sucesso')
		{
			swal({
	   			title: "OK!",
	   			text:  "Solicitação de aprovação enviada para o gestor. Aguarde o retorno do mesmo.",
	   			type:  'success'
	    	}).then(function() {
	    		window.location = base_url+'AreaAdministrador/pedidos';   	
	    	});				
		}else{
			swal({
	   			title: "Ops!",
	   			text: "Aconteceu algum problema, contacte o webmaster",
	   			type: 'warning'
	    	}).then(function() {
	    	   	
	    	});
		}
	});		
}

function solicitaLiberacao(pedido_id, cliente){

	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/solicitaLiberacao/", 
		async: true,
		data: { pedido_id 	: 	pedido_id,
				cliente 	: 	cliente }
	}).done(function(data) {
		var dados = $.parseJSON(data);		
		
		if(dados.retorno == 'sucesso')
		{
			swal({
	   			title: "OK!",
	   			text:  "Solicitação de liberação enviada para o setor financeiro. Aguarde o retorno.",
	   			type:  'success'
	    	}).then(function() {
	    		window.location = base_url+'AreaAdministrador/pedidos';   	
	    	});				
		}else{
			swal({
	   			title: "Ops!",
	   			text: "Aconteceu algum problema, contacte o webmaster",
	   			type: 'warning'
	    	}).then(function() {
	    	   	
	    	});
		}
	});		
}

function status(pedido_id, cliente, status, tipo_acesso, setor_usuario, setor_status, ordem, usuario_id, fl_identidade_visual, bandeira){
	
	$('.modal-status-title').text('Alterar Pedido #'+ pedido_id);
	$('#alterar_status').attr('pedido_id', pedido_id);
	$('#alterar_status').attr('cliente', cliente);
	$('#alterar_status').attr('status_atual', status);
	$('#alterar_status').attr('ordem', ordem);
	$('#alterar_status').attr('fl_identidade_visual', fl_identidade_visual);
	$('#alterar_status').attr('bandeira', bandeira);

	if( setor_usuario != setor_status ){
		swal({
   			title: "Atenção!", 
   			text: "Pedido encontra-se em responsabilidade de outro setor.",
   			type: 'warning'
    	}).then(function() {
    	   	
    	});		
	}else if( status == 9 && ( usuario_id != 2465 ) ){
	
		swal({
   			title: "Atenção!", 
   			text:  "Aguarde aprovação do seu gestor.",
   			type:  'warning'
    	}).then(function() {
    		   	
    	});				
	
	}else{
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/buscaProximoStatusPedido", 
			async: true,
			data: { ordem 	: 	ordem,
					status 	: 	status }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno)
			{
				$('#status_pedido').empty();							
				$('#status_pedido').append(dados.retorno);

			}else{
				swal({
		   			title: "Atenção!",
		   			text: "Erro ao gerar pdf do pedido, entre em contato com o webmaster",
		   			type: 'warning'
		    	}).then(function() {
		    	   	
		    	});
			}

		});	

		$("#m_modal_6").modal({
	    	show: true
		});
	}
	
	
}

function finalizarPedido(pedido_id,email){
	
	$('.modal-finalizar-title').text('Finalizar e Enviar E-mail do Pedido #'+ pedido_id);
	$('#finalizar_pedido').attr('pedido_id', pedido_id);
	$('#email').val(email);
	$("#m_modal_7").modal({
	    show: true
	});
	
}

function anexarNfe(pedido_id,cliente, nr_nf, arquivo_nfe,prazo_entrega, dt_emissao_nf, dt_previsao_entrega, transp_id ){

	$('.modal-anexar-nfe-title').text('Anexar nota fiscal ao pedido #'+ pedido_id);	
	$('#pedido_id').val( pedido_id);		
	$('#cliente_id').val( cliente);
	$('#link_arquivo').remove();
	$('#dt_emissao_nf').val('');
	$("#transp_id").val(transp_id).change();

	if(arquivo_nfe != '' ){
		$('#nr_nf').attr('style','width: 75%;float: left;');
		$('#nfe').before('<a href="'+base_url+'nfe/'+arquivo_nfe+'" target="_blank" id="link_arquivo" class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air" style="float: right;"><i class="la la-file-pdf-o"></i></a>');
	}
	$('#nr_nf').val(nr_nf);	
	$('#prazo_entrega').val(prazo_entrega);	
	$("#anexarNfe").modal({
	    show: true
	});

	if(dt_emissao_nf != ''){
		dt_emissao = dt_emissao_nf.split('-');
		$('#dt_emissao_nf').val(dt_emissao[2]+'/'+dt_emissao[1]+'/'+dt_emissao[0]);
	}

	if(dt_previsao_entrega != ''){
		dt_previsao_entrega = dt_previsao_entrega.split('-');
		$('#dt_previsao_entrega').val(dt_previsao_entrega[2]+'/'+dt_previsao_entrega[1]+'/'+dt_previsao_entrega[0]);
	}

	/*if( dt_emissao_nf != '' ){
		
		$('#enviar_nf').attr('style','display: none');
	}else{

		$('#enviar_nf').attr('style','display: block');
	}*/

}

function isEmail(email){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  	return re.test(email);

}

function gerarPdf(pedido_id){

	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/geraPedidosPdf/"+pedido_id+"/ajax/", 
		async: true,
		data: { pedido_id : pedido_id}
	}).done(function(data) {
		var dados = $.parseJSON(data);		
		
		if(dados.retorno != 'erro')
		{				
			window.open(base_url+'pedidos/'+dados.retorno);

		}else{
			swal({
	   			title: "Atenção!",
	   			text: "Erro ao gerar pdf do pedido, entre em contato com o webmaster",
	   			type: 'warning'
	    	}).then(function() {
	    	   	
	    	});
		}

	});		

}

function gerarPdfStartup(pedido_id, orcamento_id, cliente_id){

	$.ajax({
		method: "POST", 
		url:  base_url+"AreaAdministrador/retornaNrSeriePedido",
		async: true,
		data: { pedido_id 		: 	pedido_id }
	}).done(function(data) {
		var dados = $.parseJSON(data);

		$('#modelos_nr_series').empty();
		$('.modal-anexar-nfe-title').text('Gerar formulário de startup');
		$('.cliente_id').val(cliente_id);

		for(var i=0;i<dados.length;i++)
		{

		var html = '<div class="form-group m-form__group row novos_produtos">';
			html += '	<div class="input-group m-input-group m-input-group--solid">';
			html += '			<div class="input-group-prepend">';
			html += '				<span class="input-group-text" id="basic-addon1">';
			html += dados[i]['modelo'];
			html += '				</span>';						
			html += '			</div>';
			html += '			<input type="text" class="form-control m-input"	name="nr_serie[]" placeholder="Nr. de Série" value="'+dados[i]['id']+'/'+dados[i]['ano']+'" style="background: #fff;" readonly>';
			html += '			<a onclick="geraQrCode('+dados[i]['id']+')" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" >';
			html += '				<i class="la la-qrcode"></i>';
			html += '			</a>';
			html += '			<input type="hidden" class="form-control m-input" name="modelo[]" placeholder="Produtos" 	 value="'+ dados[i]['modelo']+'" style="background: #fff;" >';
			html += '		</div>';
			html += '	</div>';	
	
			$('#modelos_nr_series').append(html);		
			
		}
		
		$("#gerarPdfStartup").modal({
	    	show: true
		});
	});
}

function geraQrCode(id){
	window.open(base_url+'AreaAdministrador/geraQrCode/'+id);
}

function gerarAvisoDisp(pedido_id, orcamento_id, cliente_id){

	$.ajax({
		method: "POST", 
		url:  base_url+"AreaAdministrador/buscaPedidosByid",
		async: true,
		data: { pedido_id 		: 	pedido_id }
	}).done(function(data) {
		var dados = $.parseJSON(data);

		$('#modelos_aviso').empty();
		$('.modal-aviso-title').text('Gerar Aviso de Disponibilidade - Pedido '+pedido_id);
		$('.cliente_id').val(cliente_id);

		var html = '<div class="form-group m-form__group row">';
			html += '	<div class="col-lg-4">';
			html += '			<label>Contato</label>';			
			html += '			<input type="text" class="form-control m-input"	name="contato" placeholder="Contato" value="'+dados[0]['contato']+'">';						
			html += '			<input type="hidden" name="pedido_id" value="'+pedido_id+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Razão Social</label>';			
			html += '			<input type="text" class="form-control m-input"	name="razao_social" placeholder="Contato" value="'+dados[0]['razao_social']+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>CNPJ</label>';			
			html += '			<input type="text" class="form-control m-input"	name="cnpj" placeholder="CNPJ" value="'+dados[0]['cnpj']+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Telefone</label>';			
			html += '			<input type="text" class="form-control m-input"	name="telefone" placeholder="Telefone" value="'+dados[0]['telefone']+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Celular</label>';			
			html += '			<input type="text" class="form-control m-input"	name="celular" placeholder="Celular" value="">';
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Inscrição Estadual</label>';			
			html += '			<input type="text" class="form-control m-input"	name="insc_estadual" placeholder="Inscrição Estadual" value="'+dados[0]['insc_estadual']+'">';
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Endereço</label>';			
			html += '			<input type="text" class="form-control m-input"	name="endereco" placeholder="Endereço" value="'+dados[0]['endereco']+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Bairro</label>';			
			html += '			<input type="text" class="form-control m-input"	name="bairro" placeholder="Bairro" value="'+dados[0]['bairro']+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Cidade</label>';			
			html += '			<input type="text" class="form-control m-input"	name="cidade" placeholder="Cidade" value="'+dados[0]['cidade']+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Estado</label>';			
			html += '			<input type="text" class="form-control m-input"	name="estado" placeholder="Cidade" value="'+dados[0]['estado']+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>E-mail</label>';			
			html += '			<input type="text" class="form-control m-input"	name="email" placeholder="E-mail" value="'+dados[0]['email']+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Cep</label>';			
			html += '			<input type="text" class="form-control m-input"	name="cep" placeholder="Cep" value="'+dados[0]['cep']+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>FAX</label>';			
			html += '			<input type="text" class="form-control m-input"	name="fax" placeholder="Fax" value="">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Frete</label>';			
			html += '			<input type="text" class="form-control m-input"	name="frete" placeholder="Frete" value="">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Peso Bruto</label>';			
			html += '			<input type="text" class="form-control m-input"	name="peso_bruto" placeholder="Peso Bruto" value="">';						
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '			<label>Peso Líquido</label>';			
			html += '			<input type="text" class="form-control m-input"	name="peso_liquido" placeholder="Peso Líquido" value="">';						
			html += '	</div>';
			html += '</div><hr/>';	
	
		$('#modelos_aviso').append(html);	

		for(var i=0;i<dados.length;i++)
		{

		var html = '<div class="form-group m-form__group row">';
			html += '	<div class="col-lg-3">';
			html += '			<label>QTD</label>';			
			html += '			<input type="text" class="form-control m-input"	name="qtd[]" placeholder="Qtd" value="'+dados[0]['qtd']+'">';						
			html += '	</div>';	
			html += '	<div class="col-lg-3">';
			html += '			<label>Modelo</label>';			
			html += '			<input type="text" class="form-control m-input"	name="modelo[]" placeholder="Qtd" value="'+dados[0]['modelo']+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-3" style="display:none;">';
			html += '			<label>Descrição</label>';			
			html += '			<input type="text" class="form-control m-input"	name="descricao[]" placeholder="Qtd" value="'+dados[0]['descricao']+'">';						
			html += '	</div>';
			html += '	<div class="col-lg-3">';
			html += '			<label>Valor</label>';			
			html += '			<input type="text" class="form-control m-input"	name="valor[]" placeholder="Valor" value="'+dados[0]['valor']+'" readonly>';						
			html += '	</div>';
			html += '	<div class="col-lg-3">';
			html += '			<label>Dimensões (AxLxC)</label>';			
			html += '			<input type="text" class="form-control m-input"	name="dimensoes[]" placeholder="Dimensões" value="">';						
			html += '	</div>';
			html += '</div>';
	
			$('#modelos_aviso').append(html);		
			
		}
		
		$("#gerarAvisoDisp").modal({
	    	show: true
		});
	});
}
