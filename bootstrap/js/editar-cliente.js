$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('#cpf').mask('999.999.999-99');
	$('#telefone').mask('(99) 9999-99999');

	$('#senha').bind('focusout', function(){
		if(	$(this).val() != ""	){
			$('#senha2').attr('required','required');
		}else{
			$('#senha2').removeattr('required');
		}
	});

	$('#senha2').bind('focusout', function(){
		if( $(this).val() != $('#senha').val() )
		{
			swal(
		  		'Ops!',
		  		'As senhas não correspondem, tente novamente!',
		  		'warning'
			);

			$(this).val('');
			$('#senha').val('');
		}
	});
});