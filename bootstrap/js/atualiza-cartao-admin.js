$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#m_modal_7').modal('show');
    
    $( "#tecnico" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaParceirosCartao",
		minLength: 3,
		select: function( event, ui ) {
			$('#ultima_atualizacao').val(ui.item.ultima_atualizacao);
			$('#tagMecanico').val(ui.item.tag);
			$("#ativo").val(ui.item.ativo);
			$('#id').val(ui.item.id);
			$('#periodo_renovacao').val(ui.item.periodo_renovacao);
			$('#dt_vencimento').val(ui.item.data_renovacao_vencimento);
			$('#ultima_senha').val(ui.item.ultima_senha);
			
			$.ajax({
				method: "POST",
				url: base_url+"AreaAdministrador/retornaDadosCartao",
				async: true,
				data: { ultima_atualizacao 	: 	ui.item.ultima_atualizacao,
						periodo_renovacao 	: 	ui.item.periodo_renovacao,
						dt_vencimento 		: 	ui.item.dt_vencimento},				
				success: function( data ) {
					var dados = $.parseJSON(data);
					$('#vencimento').val(dados.vencimento);
					$('#diferenca_dias').val(dados.diferenca_dias);
					$('#diferenca_mes').val(dados.diferenca_mes);
					$('#diferenca_ano').val(dados.diferenca_ano);
					$('#dt_vencimento').val(dados.data_renovacao_vencimento);
					$('#m_datepicker').val(dados.vencimento);
					$('#maior_vencimento').val(dados.maior_vencimento);
					$('#ultima_atualizacao').val(dados.ultima_atualizacao);
					
				}
			});
			
		}
    });
});

function geraSenhaValidade(){

	if( $('#dt_vencimento').val() === 'MAIOR'){

		//if( ( $('#diferenca_mes').val() == parseInt($('#periodo_renovacao').val()) - 1 || $('#maior_vencimento').val() == 1 || $('#diferenca_mes').val() > 0 || $('#diferenca_ano').val() > 0) && $('#ativo').val() == 1){
			var tag 	= 	document.getElementById('tagMecanico').value;
			var data 	= 	(document.getElementById('m_datepicker').value).replace("/", "").replace("/", "");
			
			if(tag.length == 16 && data.length > 0){

				var senha 	= 	aesResult(data,tag,2);
				var id 		=	$('#id').val();
				
				if( senha.toString().length < 7 ){
					senha = '0' + senha.toString();
				}
				
				document.getElementById("aesSenha").value = senha;
				$.ajax({
			        method: "POST",
			        url: base_url+'AreaTecnicos/atualizaCartaoTecnico',
			        async: true,
			        data: { tag    				:   tag,
			        		ultima_atualizacao 	: 	$('#m_datepicker').val(),
			        		senha 				: 	senha,
			        		id 					: 	id 	}
		        }).done(function( data ) {
		            var dados = $.parseJSON(data);
		            if( dados.retorno == 'sucesso')
		            {
		            	swal({
		                    title: "Atenção!",
		                    text: "Cartão atualizado com sucesso! A nova data é " + $('#m_datepicker').val() +" e a senha gerada é " + senha +"!",
		                    type: 'success'
	                    }).then(function() {
	                  		window.location = base_url+'AreaAssistencia/';  	    
	                    });  
		            }
		        });
			}else{
				document.getElementById("aesSenha").value = "Campos inválidos";
			}	
	}else if( $('#ativo').val() == 1 ){
		swal({
   			title: "Atenção!",
   			text:  "Esse cartão ainda está válido.",
   			type:  'warning'
    	});
	
    }else{

		swal({
   			title: "Atenção!",
   			text:  "Esse está inativado, entre no cadastro de cartões técnicos e ative o cartão.",
   			type:  'warning'
    	});
	}

	
	
	

}