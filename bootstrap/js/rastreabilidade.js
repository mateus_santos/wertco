$(document).ready(function(){

	var table = $('#html_table').DataTable({
		"scrollX": true,
		"order": [[ 0, "desc" ]],
		"iDisplayLength": 10,
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"processing": true,
		"serverSide": true,
		aoColumnDefs: [
		{ "width": "10%", "targets": 0 },
		{ "width": "10%", "targets": 1 },
		{ "width": "10%", "targets": 2 },
		{ "width": "10%", "targets": 3 },
		{ "width": "10%", "targets": 4 },
		{ "width": "30%", "targets": 5 },
		{ "width": "20", "targets": 6 }, 
		{ "targets": [ 6 ],  "searchable": false ,  "sortable": false}
		], 
		columns:[ {
			data: "id"
		},{
			data: "modelo"
		},{
			data: "combustivel"
		},{
			data: "pedido_id"
		},{
			data: "ordem_producao_id"
		},{
			data: "cliente"
		},{
			data: "acoes"
		}],
		"ajax": {
			url: base_url+"AreaQualidade/buscaNrSerieSS",
			type : "POST",			
			data: function ( d ) {                    
                },
			error: function(retorno){
				console.log(retorno.responseText);
			},
		},    	
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"sLengthMenu": "_MENU_    Resultados por página",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sZeroRecords": "Nenhum registro encontrado",
			"sSearch": "Pesquisar",
			"oPaginate": {
				"sNext": '<i class="la la-angle-double-right"></i>',
				"sPrevious": '<i class="la la-angle-double-left"></i>',
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			}
		},
		
		"initComplete": function() {
			var $searchInput = $('#html_table_filter input');
			$searchInput.unbind();
			$searchInput.on({
			keyup : function(e) {
			    if(e.keyCode == 13) {
			      table.search(this.value).draw();
			      return;
			    }
			}
			});
        }
	});
	//.search($('#pesquisa_ativa').val()).draw();	
	$('#html_table_filter input').attr('style','float: left;width: 80%;');
	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');

	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	$('#alterar_afericao').bind('click', function(){
		var nr_serie = $(this).attr('nr_serie_id');
		var dt_afericao = $('#dt_afericao').val();

		if(dt_afericao == ''){
			swal({
	   			title: "OPS!",
	   			text: "Insira uma data",
	   			type: 'warning'
		    }).then(function() {});
		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaQualidade/atualizaNrSerieAfericao", 
				async: true,
				data: { id 			: 	nr_serie,
						dt_afericao : 	dt_afericao	}
			}).done(function(data) {
				var dados = $.parseJSON(data);	
				if(dados.retorno == 'sucesso'){
					swal({
			   			title: "OK!",
			   			text: "Data de aferição da bomba "+nr_serie+" alterada com sucesso!",
			   			type: 'success'
			    	}).then(function() {
			    	   	$("#modal_afericao").modal('hide');
			    	});
				}
			});
		}
	});

	$('#alterar_qtd_lacres').bind('click', function(){
		var nr_serie = $(this).attr('nr_serie_id');
		var qtd_lacres = $('#qtd_lacres').val();

		if(qtd_lacres == ''){
			swal({
	   			title: "OPS!",
	   			text: "Insira uma quantidade",
	   			type: 'warning'
		    }).then(function() {

		    });
		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaQualidade/atualizaNrSerieQtdLacres", 
				async: true,
				data: { id 			: 	nr_serie,
						qtd_lacres : 	qtd_lacres	}
			}).done(function(data) {
				var dados = $.parseJSON(data);	
				if(dados.retorno == 'sucesso'){
					swal({
			   			title: "OK!",
			   			text: "Qtd de lacres para a bomba "+nr_serie+" alterada com sucesso!",
			   			type: 'success'
			    	}).then(function() {
			    	   	$("#modal_qtd_lacres").modal('hide');
			    	});
				} 
			});
		}
	});

	$('#alterar_obs').bind('click', function(){
		var nr_serie = $(this).attr('nr_serie_id');
		var observacao = $('#obs').val();

		if(observacao == ''){
			swal({
	   			title: "OPS!",
	   			text: "Insira uma observação",
	   			type: 'warning'
		    }).then(function() {

		    });
		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaQualidade/atualizaNrSerieObs", 
				async: true,
				data: { id 			: 	nr_serie,
						observacao : 	observacao	}
			}).done(function(data) {
				var dados = $.parseJSON(data);	
				if(dados.retorno == 'sucesso'){
					swal({
			   			title: "OK!",
			   			text: "Observações para a bomba "+nr_serie+" alterada com sucesso!",
			   			type: 'success'
			    	}).then(function() {
			    	   	$("#modal_obs").modal('hide');
			    	});
				} 
			});
		}
	});

});



function afericao(nr_serie_id){

	$.ajax({
		method: "POST",
		url:  base_url+"AreaQualidade/buscaNrSerieAfericao", 
		async: true,
		data: { id 			: 	nr_serie_id }
	}).done(function(data) {
		var dados = $.parseJSON(data);	
		if(dados.retorno == 'sucesso'){
			console.log(dados);
			$('#dt_afericao').val(dados.dt_afericao);
		} 
	});

	$('.modal-afericao-title').empty();
	$('.modal-afericao-title').text('Aferição - Nº Série #'+ nr_serie_id);
	$('#alterar_afericao').attr('nr_serie_id', nr_serie_id);
	$("#modal_afericao").modal({
		show: true
	});
	
}

function qtdLacres(nr_serie_id){

	$.ajax({
		method: "POST",
		url:  base_url+"AreaQualidade/buscaNrSerieQtdLacres", 
		async: true,
		data: { id 			: 	nr_serie_id }
	}).done(function(data) {
		var dados = $.parseJSON(data);	
		if(dados.retorno == 'sucesso'){
			console.log(dados);
			$('#qtd_lacres').val(dados.qtd_lacres);
		} 
	});

	$('.modal-qtd-lacres-title').empty();
	$('.modal-qtd-lacres-title').text('Qtd. Lacres - Nº Série #'+ nr_serie_id);
	$('#alterar_qtd_lacres').attr('nr_serie_id', nr_serie_id);
	$("#modal_qtd_lacres").modal({
		show: true
	});
	
} 

function observacao(nr_serie_id){

	$.ajax({
		method: "POST",
		url:  base_url+"AreaQualidade/buscaNrSerieObs", 
		async: true,
		data: { id 			: 	nr_serie_id }
	}).done(function(data) {
		var dados = $.parseJSON(data);	
		if(dados.retorno == 'sucesso'){
			console.log(dados);
			$('#obs').val(dados.observacao);
		} 
	});

	$('.modal-obs-title').empty();
	$('.modal-obs-title').text('Observação - Nº Série #'+ nr_serie_id);
	$('#alterar_obs').attr('nr_serie_id', nr_serie_id);
	$("#modal_obs").modal({
		show: true
	});
	
}

function excluirOrcamento(orcamento_id){
	
	$('.modal-excluir-title').text('Excluir Orçamento #'+ orcamento_id);
	$('#orcamento_id').val(orcamento_id);
	$("#m_excluir_orcamento").modal({
		show: true
	});
	
}



