$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('.add').bind('click', function(){

        var html = $('#modelo').clone();                
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
        $(html).find('.bombas').attr('indice', indice);
        $(html).find('.valor_unitario').attr('indice', indice);
        $(html).find('.valor_unitario').text('');
        $(html).find('b.valor_unitario').addClass('valor_unitario_'+indice);
        $(html).find('input.valor_unitario').attr('id', 'valor_unitario_'+indice);

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();                
        body+="</tr>";                
        $('.table').append(body);                
        $('#modelo').attr('total_indice',indice);

        excluir();
        mostra_valor();        
    });
    
    $('#cnpj').mask('00.000.000/0000-00');
    $('#cpf').mask('000.000.000-00');
    $('#bombas').mask('00000'); 
    $('#bicos').mask('00000');  
    $('.qtd').mask('000');
    $('#cep').mask('00000-000');  
    
    $('.bombas').bind('change', function(){
        
        $('b.valor_unitario').text($(this).find(':selected').attr('valor_unitario'));
        $('#valor_unitario_0').val($(this).find(':selected').attr('valor_unitario'));
        $('.valor_unitario_0').mask('#.##0,00', {reverse: true});

        if($(this).find('option:selected').val() != ''){

            $('.add').fadeIn('slow');
        }else{

            $('.add').fadeOut('slow');
        }
    }); 

    $('#estado').bind('change', function(){
        $('#msg_produtos').fadeOut('fast');
        $('.esconde').fadeIn('slow');
        
        insereConsultor( $(this).val()  );
        if( $(this).val() == 'AC' || $(this).val() == 'AL' || $(this).val() == 'AP' ||
            $(this).val() == 'AM' || $(this).val() == 'BA' || $(this).val() == 'CE' || 
            $(this).val() == 'MA' || $(this).val() == 'MT' || $(this).val() == 'MS' ||
            $(this).val() == 'PA' || $(this).val() == 'PB' || $(this).val() == 'PE' ||
            $(this).val() == 'PI' || $(this).val() == 'RN' || $(this).val() == 'RO' ||
            $(this).val() == 'RR' || $(this).val() == 'SE')
        {
            swal({
                title: "Atenção!",
                text: "Por favor, entre em contato com o setor comercial da Wertco para emitir orçamento para esse estado ("+$(this).val()+")",
                type: 'warning'
            }).then(function() {
                window.location.reload();
            });     
        }
    });
    
    $('#cnpj').bind('focusout', function(){
        var cnpj = $(this).val(); 
        if(cnpj != ""){
            if( isCNPJValid(cnpj) ){
            $.ajax({
                method: "POST",
                url: base_url+'clientes/verificaOrcamento',
                async: true,
                data: { cnpj    :   cnpj }
                }).done(function( data ) {
                    var dados = $.parseJSON(data);
                    if( dados.total == 0){

                        $.ajax({
                            method: "POST",
                            url: base_url+'clientes/verifica_cnpj',
                            async: false,
                            data: { cnpj    :   cnpj }
                            }).done(function( data ) {
                                var dados = $.parseJSON(data);                          
                                if(dados.length > 0){
                                    
                                    $('#razao_social').val(dados[0].razao_social);      
                                    $('#razao_social').attr('disabled', true);      
                                    $('#fantasia').val(dados[0].fantasia);      
                                    $('#fantasia').attr('disabled', true);
                                    $('#telefone').val(dados[0].telefone);
                                    $('#telefone').attr('disabled', true);
                                    $('#endereco').val(dados[0].endereco);      
                                    $('#endereco').attr('disabled', true);                                  
                                    $('#email').attr('disabled', true);
                                    $('#email').val(dados[0].email);
                                    $('#cidade').attr('disabled', true);
                                    $('#cidade').val(dados[0].cidade);  
                                    $('#estado').attr('disabled', true);
                                    $('#estado').val(dados[0].estado);                        
                                    $('#pais').attr('disabled', true);
                                    $('#pais').val(dados[0].pais);
                                    $('#cep').attr('disabled', true);
                                    $('#cep').val(dados[0].cep);                        
                                    $('#bairro').attr('disabled', true);
                                    $('#bairro').val(dados[0].bairro);                        
                                    $('#salvar').val('0');
                                    $('#empresa_id').val(dados[0].id);
                                    insereConsultor( dados[0].estado );

                                }else{

                                    var cnpj_sem_mascara = cnpj.replace(/[^\d]+/g,'');

                                    $.ajax({
                                        method: "POST",
                                        url: base_url+'AreaTecnicos/ValidaCnpj',
                                        async: true,
                                        data: { cnpj    :   cnpj_sem_mascara }
                                    }).done(function( data ) {
                                        var dados = $.parseJSON(data);   
                                        
                                        if (dados.receita.erro == 0 && dados.receita.situacao == "ATIVA" ){
                                            $('#razao_social').attr('disabled', false);     
                                            $('#razao_social').val(dados.receita.retorno.razao_social);
                                            if(dados.receita.retorno.razao_social != ''){    
                                                $('#razao_social').attr('style','border-color: #5fda17;');
                                            }
                                            $('#fantasia').attr('disabled', false);
                                            $('#fantasia').val(dados.receita.retorno.nome_fantasia );
                                            if(dados.receita.retorno.nome_fantasia != ''){
                                                $('#fantasia').attr('style','border-color: #5fda17;');
                                            }
                                            $('#telefone').attr('disabled', false);
                                            $('#telefone').val(dados.receita.retorno.telefone);
                                            if(dados.receita.retorno.telefone != ''){
                                                $('#telefone').attr('style','border-color: #5fda17;');
                                            }
                                            $('#endereco').attr('disabled', false); 
                                            $('#endereco').val(dados.receita.retorno.logradouro+', '+dados.receita.retorno.numero+' '+dados.receita.retorno.complemento);                                
                                            $('#endereco').attr('style','border-color: #5fda17;');
                                            
                                            $('#email').attr('disabled', false); 
                                            $('#email').val('');                                                                                                
                                            $('#cidade').attr('disabled', false); 
                                            $('#cidade').val(dados.receita.retorno.municipio_ibge);  
                                            if(dados.receita.retorno.municipio_ibge != ''){
                                                $('#cidade').attr('style','border-color: #5fda17;');
                                            }
                                            $('#estado').attr('disabled', false); 
                                            $('#estado').val(dados.receita.retorno.uf);
                                            if(dados.receita.retorno.uf != ''){
                                                $('#estado').attr('style','border-color: #5fda17;');
                                            }
                                            $('#bairro').attr('disabled', false); 
                                            $('#bairro').val(dados.receita.retorno.bairro);
                                            if(dados.receita.retorno.bairro != ''){
                                                $('#bairro').attr('style','border-color: #5fda17;');
                                            }    
                                            $('#cep').attr('disabled', false); 
                                            $('#cep').val(dados.receita.retorno.cep);                          
                                            if(dados.receita.retorno.bairro != ''){
                                                $('#cep').attr('style','border-color: #5fda17;');
                                            }
                                            $('#pais').attr('disabled', false);
                                            $('#pais').attr('style','border-color: #5fda17;');                                                
                                            $('#inscricao_estadual').attr('disabled', false);
                                            $('#inscricao_estadual').val('');               
                                            $('#cartao_cnpj').val(dados.receita.save);
                                            
                                            $('#pais').val('Brasil');         
                                            $('#salvar').val('1');
                                            insereConsultor( dados.receita.retorno.uf );
                                        }else{
                                           swal({
                                                title: "Atenção!",
                                                text: dados.receita.msg,
                                                type: 'warning'
                                            }).then(function() {
                                                $('#cnpj').val('');
                                            });

                                        }
                                    });

                                }
                        });  
                    }else{

                        swal({
                            title: "Atenção!",
                            text: "Já Existe um orçamento em andamento para este cliente, entre em contato com a WERTCO!",
                            type: 'warning'
                        }).then(function() {
                            $('#cnpj').val('');
                        }); 
                   }
                });

            }else{
                swal({
                    title: 'Atenção!',
                    text: 'CNPJ inválido!',
                    type:'warning'
                }).then(function(isConfirm) {
                    $('#cnpj').val('');
                    $('#cnpj').focus();
                });
            }
        
        }
    });

});
	
	
function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');                
        $('tr[indice="'+indice+'"]').remove(); 
    });    
}

function mostra_valor()
{
    $('.bombas').bind('change', function(){
        indice = $(this).attr('indice');        
        $('.valor_unitario_'+indice).text($(this).find(':selected').attr('valor_unitario'));  
        $('#valor_unitario_0'+indice).val($(this).find(':selected').attr('valor_unitario'));  
        $('.valor_unitario_0'+indice).mask('#.##0,00', {reverse: true});         
        
    }); 

}


function isCNPJValid(cnpj) {  
            
    var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
    if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
        return false;
    for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
    if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
    if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    return true; 
}

function insereConsultor(estado){

    $.ajax({
        method: "POST",
        url: base_url+'AreaTecnicos/buscaConsultor',
        async: true,
        data: { estado  : estado }
    }).done(function( data ) {
        var retorno = $.parseJSON(data);
        console.log(retorno);
        $('#responsavel').empty();
        html = '<option value=""> -- SELECIONE UM CONSULTOR WERTCO -- </option>';
        html+= '<option value="7167">CINDIEL</option>';
        for (var i = 0; i < retorno.length ; i++) {
            html+= '<option value="'+retorno[0]['usuario_id']+'">'+retorno[0]['nome']+'</option>';
            
        }
        $('#responsavel').append(html);
        
    });

}