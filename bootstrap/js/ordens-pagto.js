$(document).ready(function(){	
	
	/*if( $('#nr_nf').val() != '' ){
		$('#enviar_nf').hide();
	}*/

	$('.date').datepicker({
		orientation: 'bottom',
		format: 'dd/mm/yyyy'
	});

	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');

	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	$('#enviar_reprovacao').bind('click',function(){
		var pedido_id  	= 	$(this).attr('pedido_id');
		var motivo 		= 	$('#motivo_reprovacao_insp').val();
		$.ajax({
			method: "POST", 
			url:  base_url+"AreaQualidade/avisoProducaoReprovado", 
			async: true,
			data: { pedido_id 		: 	pedido_id,
					motivo 			: 	motivo}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			if(dados.retorno == 'sucesso'){
				swal({
		   			title: "OK!",
		   			text: "Foi enviado um e-mail para o setor de produção informando a reprovação na inspeção",
		   			type: 'success'
		    	}).then(function() {
		    	   	window.location = base_url+'AreaAdministrador/pedidos';
		    	});	
			}
		});
	});
       

});

function visualizar(op_id){
	
	$.ajax({
		method: "POST", 
		url:  base_url+"AreaAdministrador/listarFotosPedido", 
		async: true,
		data: { pedido_id 		: 	pedido_id }
	}).done(function(data) {
		var dados = $.parseJSON(data);
		$('.fotos_pedido_titulo').empty();
		$('.fotos_pedido_titulo').text('Fotos Pedido #'+pedido_id+' Cliente:'+cliente);		
		$('#fotos_pedido').empty();
		$('#fotos_pedido').append(dados.html);
	});

	$('#m_foto_pedido').modal({
		show: true
	});	
	
}

function anexar(op_id,comprovante, nota_fiscal,nr_nf,cliente,favorecido,valor,descricao,chamado_id){

	$('.modal-anexar-nfe-title').text('Anexar arquivos à ordem de pagamento #'+ op_id);	
	$('#op_id').val(op_id);			
	$('.link_arquivo').remove();
	if(nota_fiscal != '' ){
		$('#nr_nf').attr('style','width: 75%;float: left;');
		$('#nota_fiscal').before('<a href="'+base_url+'nfe/'+nota_fiscal+'" target="_blank"  class="link_arquivo btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air" style="float: right;"><i class="la la-file-pdf-o"></i></a>');
	}
	if(comprovante != '' ){		
		$('#comprovante').before('<a href="'+base_url+'nfe/'+comprovante+'" target="_blank"  class="link_arquivo btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air" style="float: right;"><i class="la la-file-pdf-o"></i></a>');
	}

	$('#nr_nf').val(nr_nf);
	$('#cliente').val(cliente);
	$('#favorecido').val(favorecido);
	$('#valor').val(valor);
	$('#descricao').val(descricao);
	$('#chamado_id').val(chamado_id);

	$("#anexarNfe").modal({
	    show: true
	});
	
	/*if( dt_emissao_nf != '' ){
		
		$('#enviar_nf').attr('style','display: none');
	}else{

		$('#enviar_nf').attr('style','display: block');
	}*/

}

function isEmail(email){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  	return re.test(email);

}

function visualizar(op_id){

	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/buscaItensOp", 
		async: true,
		data: { op_id : op_id}
	}).done(function(data) {
		var dados = $.parseJSON(data);		
		
		if(dados.retorno != 'erro')
		{				
			$('#conteudo_itens').append(dados.retorno);
		}

	});		

	$("#visualizarOp").modal({
	    show: true
	});

}

function gerarPdfStartup(pedido_id, orcamento_id, cliente_id){


	$.ajax({
		method: "POST", 
		url:  base_url+"AreaAdministrador/retornaProdutosOrcamentos", 
		async: true,
		data: { orcamento_id 		: 	orcamento_id}
	}).done(function(data) {
		var dados = $.parseJSON(data);		

		$('#modelos_nr_series').empty();
		$('.modal-anexar-nfe-title').text('Gerar formulário de startup');
		$('.cliente_id').val(cliente_id);

		for(var i=0;i<dados.length;i++)
		{
			if(dados[i]['tipo_produto_id'] != 4){

				var html = '<div class="form-group m-form__group row novos_produtos">';
					html += '	<div class="input-group m-input-group m-input-group--solid">';
					html += '			<div class="input-group-prepend">';
					html += '				<span class="input-group-text" id="basic-addon1">';
					html += dados[i]['modelo'];
					html += '				</span>';						
					html += '			</div>';
					html += '			<input type="text" class="form-control m-input" name="nr_serie[]" placeholder="Nr. de Série" value="" style="background: #fff;">';
					html += '			<input type="hidden" class="form-control m-input" name="modelo[]" placeholder="Produtos" value="'+ dados[i]['modelo']+'" style="background: #fff;">';
					html += '		</div>';
					html += '	</div>';
						
				for(var j=0;j<dados[i]['qtd'];j++)
				{
					$('#modelos_nr_series').append(html);
				}
			}
		}
		
		$("#gerarPdfStartup").modal({
	    	show: true
		});
	});
}

