$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	
	if($('tbody > tr').length == 0){
		$('#modal_add_atividade').modal({show: true, backdrop: 'static', keyboard: false});
		$('#destinatario').fadeOut('slow');
	}else{
		$('#destinatario').fadeIn('slow');
	}

	$('#email').bind('focusout', function(){
		var email = $(this).val();
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test(email)){
			swal(
				'Ops!',
				'E-mail inválido',
				'error'
			).then(function() {
				$('#email').focus();
			});
		}
	});

	$('#telefone').mask('(00) 0000-0000');
	$('#celular').mask('(00) 00000-0000');

	$('.cadastrar_atividade').bind('click', function(){

		$('#form_atividade input').val('');
		$('#form_atividade select').val('');
		$('#form_atividade textarea').val('');
		$('#atividade_id').removeAttr('name');

		$('input[name="prospeccao_id"]').val($('#prospeccao_id').val());

		$('.modal-andamento-title').text('Cadastrar Atividade');
		$('#form_atividade').attr('action',base_url+'AreaAdministradorComercial/inserirAtividade/');
		$('#modal_add_atividade').modal({show: true, backdrop: 'static', keyboard: false});
	});
	
/************************************************************/

	$('#status_id').bind('change',function(){
		console.log($(this).val());
		if( $(this).val() == '2' )
		{
			$('.destinatario').fadeIn('slow');
		}else{
			$('.destinatario').fadeOut('slow');
		}
	});

	$('#encaminhar').bind('click', function(){

		if( $('#setor_destinacao').val() == '' ||  $('#msg_destinacao').val() == ''){
			
			swal({
				title: "Atenção!",
				text: "Preencha todos o setor e a mensagem para ser encaminhada ao destinatário.",
				type: "warning"
			});

		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministradorComercial/encaminharAtendimento", 
				async: false,
				data: { id 					: 	$('#prospeccao_id').val(),
						setor_destinacao 	: 	$('#setor_destinacao').val(),
						msg_destinacao 		: 	$('#msg_destinacao').val(),
						cliente 			: 	$('#cliente').val() }

			}).done(function(data) {
				var dados = $.parseJSON(data);

				if( dados.retorno == 'sucesso'){
					swal({
						title: "Ok!",
						text: "Mensagem encaminhada para o setor de destino.",
						type: "success"
					}).then(function() {
			    	   	$('#setor_destinacao').attr('disabled',true);
			    	   	$('#msg_destinacao').attr('disabled',true);
			    	   	$('#encaminhar').fadeOut('slow');
			    	});

				}else{
					swal({
						title: "Atenção!",
						text: "Erro.",
						type: "warning"
					});	
				}
			});	
		}
	});

	$('#encaminhar_1').bind('click', function(){

		if( $('#setor_destinacao_1').val() == '' ||  $('#msg_destinacao_1').val() == ''){
			
			swal({
				title: "Atenção!",
				text: "Preencha todos o setor e a mensagem para ser encaminhada ao destinatário.",
				type: "warning"
			});

		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministradorComercial/encaminharAtendimento", 
				async: false,
				data: { id 					: 	$('#prospeccao_id').val(),
						setor_destinacao 	: 	$('#setor_destinacao_1').val(),
						msg_destinacao 		: 	$('#msg_destinacao_1').val(),
						cliente 			: 	$('#cliente').val() }

			}).done(function(data) {
				var dados = $.parseJSON(data);

				if( dados.retorno == 'sucesso'){
					swal({
						title: "Ok!",
						text: "Mensagem encaminhada para o setor de destino.",
						type: "success"
					}).then(function() {
			    	   	$('#setor_destinacao').attr('disabled',true);
			    	   	$('#msg_destinacao').attr('disabled',true);
			    	   	$('#encaminhar').fadeOut('slow');

			    	   	$('#setor_destinacao_1').attr('disabled',true);
			    	   	$('#msg_destinacao_1').attr('disabled',true);
			    	   	$('#encaminhar_1').fadeOut('slow');
			    	});

				}else{
					swal({
						title: "Atenção!",
						text: "Erro.",
						type: "warning"
					});	
				}
			});	
		}
	});

});

function valida_form_edita_atividade(){
	if($('#edit_defeito_id').val() == 0){
		swal({
			title: "Campo em branco",
			text: "Selecione um defeito",
			type: "warning"
		});
		return false;
	}
}


function editar_atividade(atividade_id){
	$('#atividade_id').val(atividade_id);
	$('.modal-andamento-title').text('Editar Atividade');
	$('#form_atividade').attr('action',base_url+'AreaAdministradorComercial/editarAtividade/');
	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministradorComercial/buscaAtividade", 
		async: false,
		data: { atividade_id 	: 	atividade_id }
	}).done(function(data) {
		var dados = $.parseJSON(data);

		$('#nome').val(dados.nome);
		$('#telefone').val(dados.telefone);
		$('#celular').val(dados.celular);
		$('#email').val(dados.email);
		$('#cargo').val(dados.cargo);
		$('#descricao').val(dados.descricao);
		$('#atividade_id').attr('name','id');
	});	

	$('#modal_add_atividade').modal({show: true, backdrop: 'static', keyboard: false});
}














