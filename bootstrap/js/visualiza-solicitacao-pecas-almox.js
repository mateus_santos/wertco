$(document).ready(function(){
	$('.custo').mask('#.##0,00', {reverse: true});	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

	if($('#status_solicitacao_id').val() != 1 && $('#status_solicitacao_id').val() != 3 ){
		$('input').attr('disabled','disabled');
		$('textarea').attr('disabled','disabled');
		$('#atualizar').remove();
		$('#enviar_info_caixa').attr('style','display: none;');
		$('.excluir-item').fadeIn('slow');
	}else{
		if( $('#status_solicitacao_id').val() == 1 ){
			swal({
		        title: 'ATENÇÃO',
		        text: 'Essas peças já estão em separação/montagem/teste?',
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonClass: "swal2-confirm btn btn-danger m-btn m-btn--custom",
		        confirmButtonText: "Sim",
		        cancelButtonText: "Não"
		    }).then(function(isConfirm) {
		        if(isConfirm.value) {

		        	var solicitacao_id = $('#solicitacao_id').val();
		        	$.ajax({
						method: "POST",
						url:  base_url+"AreaAlmoxarifado/alterarStatusSolicitacaoAlmox", 
						async: true,
						data: { solicitacao_id 			: 	solicitacao_id }
								
					}).done(function(data) {
						var dados = $.parseJSON(data);

						if(dados.retorno == 'sucesso')
						{
							swal({
								title: "OK!",
								text: 'Status da solicitação Alterada para Peças em Separação/Montagem/Teste!',
								type: "success"
							}).then(function() {});
						}
					});

		        }
		    });
		}

	}



	$('.custo').bind('focusout', function(){
		var custo 					= $(this).val();
		var solicitacao_pecas_id 	= $(this).attr('solicitacao_pecas_id');
		var solicitacao_id 			= $(this).attr('solicitacao_id');
		var status_solicitacao_id	= $(this).attr('status_solicitacao_id');
		var descricao				= $(this).attr('descricao');	
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAssistencia/alterarCustoSolicitacao", 
			async: true,
			data: { custo 					: 	custo,
					solicitacao_pecas_id 	: 	solicitacao_pecas_id,
					solicitacao_id 			: 	solicitacao_id,
					status_solicitacao_id	: 	status_solicitacao_id,
					descricao				: 	descricao			}
					
		}).done(function(data) {
			var dados = $.parseJSON(data);

			if(dados.retorno == 'sucesso')
			{
				swal({
					title: "OK!",
					text: 'Custo da peça alterado com sucesso!',
					type: "success"
				}).then(function() {
					
				});
			}
		});

	});

	$('.nr_serie').bind('focusout', function(){
		var id = $(this).attr('id');
		var nr_serie = $(this).val();
		if( nr_serie != '' ){
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAlmoxarifado/alterarNrSerie", 
				async: true,
				data: { id 			: 	id,
						nr_serie 	: 	nr_serie }					
			}).done(function(data) {
				var dados = $.parseJSON(data);

				if(dados.retorno == 'sucesso')
				{
					swal({
						title: "OK!",
						text: 'Nr de série vinculado a solicitação!',
						type: "success"
					}).then(function() {});
				}
			});
		}
	});

	$('#atualizar').bind('click', function(){		
		var solicitacao_id 	= 	$(this).attr('solicitacao_id');
		var cliente 		= 	$(this).attr('cliente');
		var chamado_id 		= 	$(this).attr('chamado_id');
		var erro 			= 	0;
		// verifica se existe algum numero de serie em branco
		$('.nr_serie').each(function(){
			if( $(this).val() == '' ){
				erro = 1;
			}
		});

		if( erro == 1 ){
			swal({
				title: "ATENÇÃO!",
				text: 'Preencha todos os número de série das peças a serem enviadas!',
				type: "warning"
			}).then(function() {});
		
		}else{
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAlmoxarifado/confirmarPecasEnviar", 
				async: true,
				data: { solicitacao_id	: 	solicitacao_id,
						cliente 		: 	cliente,
						chamado_id 		: 	chamado_id	 }
			}).done(function(data) {
				var dados = $.parseJSON(data);

				if(dados.retorno == 'sucesso')
				{
					swal({
						title: "OK!",
						text: 'Peças preparadas para envio!',
						type: 'success'
					}).then(function(){
						window.location.href=base_url+"AreaAlmoxarifado/solicitacaoPecas";
					});
				}
			});
		}
	});

	$('#excluir_item').bind('click', function(){
		item_id 		= 	$(this).attr('item_id');
		status_id 		=	$(this).attr('status_id');
		solicitacao_id 	= 	$(this).attr('solicitacao_id');
		item_nr_id 		= 	$(this).attr('item_nr_id');
		descricao 		= 	$(this).attr('descricao');
		qtd		 		= 	$(this).attr('qtd');
		motivo_exclusao = 	$('#motivo_exclusao').val(); 
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAlmoxarifado/excluirItem",
			async: true,
			data: { solicitacao_id	: 	solicitacao_id,
					status_id 		: 	status_id,
					item_id 		: 	item_id,
					item_nr_id 		: 	item_nr_id,
					descricao 		: 	descricao,
					qtd	 			: 	qtd,
 					motivo_exclusao : 	motivo_exclusao 	}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			if(dados.retorno == 'sucesso')
			{ 
				swal({
					title: "OK!",
					text: 'Item da peça excluída da solicitação!',
					type: 'success'
				}).then(function(){
					window.location.reload();					
				});
			}
		});	
	});
}); 

function excluirItem(item_id,solicitacao_id, status_id, item_nr_id, descricao, nr_serie,qtd){

	swal({
        title: 'ATENÇÃO',
        text: 'Deseja excluir esse item?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: "swal2-confirm btn btn-danger m-btn m-btn--custom",
        confirmButtonText: "Sim",
        cancelButtonText: "Não"
    }).then(function(isConfirm) {
        if(isConfirm.value) {

    		$('#excluir_item').attr('item_id',item_id);
    		$('#excluir_item').attr('item_nr_id',item_nr_id);
    		$('#excluir_item').attr('solicitacao_id',solicitacao_id);
    		$('#excluir_item').attr('status_id',status_id);
    		$('#excluir_item').attr('descricao',descricao+' - Nº Série: '+nr_serie);
    		$('#excluir_item').attr('qtd',qtd);
    		$('#motivo_exclusao').attr('disabled', false);
        	$('#m_excluir_item').modal({
        		show: true
			});
        }
    });
}