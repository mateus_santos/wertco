$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('#empresa').mask('99.999.999/9999-99');

	$('#salvar').bind('click', function(){
		if( $('#empresa_id').val() != '' && $('#empresa_id').val() != undefined){
			$('#form').submit();
		}else{

			swal({
				title: "Atenção!",
				text: 'Selecione um posto cadastrado!',
				type: "warning"
			}).then(function() {
		      	
		   	});
			return false;
		}
	});

	/* autocomplete responsável */
	$( "#empresa" ).autocomplete({
		source: base_url+"AreaTecnicos/retornaEmpresas",
		minLength: 9,
		select: function( event, ui ) {
			$('#empresa').attr('maxlength','550');
			$('#empresa_id').val(ui.item.id);
		}
    });

    
});
