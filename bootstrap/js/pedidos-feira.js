$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);	

	$("#alterar_status").bind('click', function(){

		pedido_id 			= 	$(this).attr('pedido_id');
		status_pedido		= 	$("#status_pedido").val();
		

		$.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/alteraStatusPedido", 
			async: true,
			data: { pedido_id 		: 	pedido_id,
					status_pedido 	: 	status_pedido}
		}).done(function(data) {
			var dados = $.parseJSON(data);		

			if(dados.retorno == 'sucesso')
			{
				swal({
		   			title: "OK!",
		   			text: "Status do pedido atualizado com sucesso",
		   			type: 'success'
		    	}).then(function() {
		    	   	window.location = base_url+'AreaFeira/pedidos';
		    	});
			}
		});		
	});
	
	/* autocomplete responsável */
	$( "#responsavel" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaRepresentantes",
		minLength: 1,
		select: function( event, ui ) {
			$('#responsavel_id').val(ui.item.id);
		}
    });   

    $('#finalizar_pedido').bind('click', function(){
    	var pedido_id = $(this).attr('pedido_id');
    	var email = $('#email').val();

    	if(isEmail(email)){

	    	$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/confirmaEnviaEmailPedido/", 
				async: true,
				data: { pedido_id : pedido_id,
						email 	  : email	}
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{				
					swal({
			   			title: "OK!",
			   			text: "Status do pedido atualizado com sucesso",
			   			type: 'success'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaFeira/pedidos';
			    	});
				}else{
					swal({
			   			title: "Atenção!",
			   			text: dados.retorno+". Entre em contato com o webmaster!",
			   			type: 'success'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaFeira/pedidos';
			    	});
				}

			});	

		}else{
			swal({
	   			title: "Atenção!",
	   			text: "E-mail Inválido",
	   			type: 'warning'
	    	}).then(function() {
	    	   	//window.location = base_url+'AreaAdministrador/pedidos';
	    	});
		}
    });

    $("#tour_virtual").bind('click', function(){
    	
    	if($('.status').length > 0){

	    	var tour;
	  
			tour = new Shepherd.Tour({
			  defaults: {
			    classes: 'shepherd-element shepherd-open shepherd-theme-arrows',
			    scrollTo: true
			  }
			});

			tour.addStep('primeiro-step', {
			  text: 'Área onde se encontram todos os pedidos realizados no sistema WERTCO.',
			  attachTo: '.m-datatable top',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.status click',
			  showCancelLink: true,
			  buttons: [
			    {
			      text: 'Próximo', 
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('segundo-step', {
			  text: 'Aqui você pode alterar o status dos pedidos. Basta clicar no botão e selecionar a situação atual do pedido.',
			  attachTo: '.status top',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.visualizar click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Próximo',
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('terceiro-step', {
			  text: 'Clicando nesse botão o sistema irá gerar um arquivo pdf, contendo todas as informações do pedido.',
			  attachTo: '.visualizar left',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.editar click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Próximo',
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('quarto-step', {
			  text: 'Aqui você poderá alterar as informações contidas no pedido.',
			  attachTo: '.editar left',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.gerar_ops click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Próximo',
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('quinto-step', {
			  text: 'Clicando neste botão é possível confirmar o pedido e gerar as ordens de produção do mesmo.',
			  attachTo: '.gerar_ops left',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.cadastrar_orcamento click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Próximo',
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('sexto-step', {
			  text: 'Clique neste botão para cadastrar um novo pedido no sistema.',
			  attachTo: '.cadastrar_orcamento top',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.cadastrar_orcamento click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Finalizar',
			      action: tour.next
			    }
			  ]
			});
			
			tour.start();
		}
    });

   

});

function verificaOps(status,pedido_id, caminho){
	
		//valida se existe op's geradas dessa op
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/verificaOpsPedido/", 
			async: true,
			data: { pedido_id : pedido_id }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.length > 0)
			{
				swal({
		   			title: "Atenção!",
		   			text: "Já existem op's para esse pedido",
		   			type: 'warning'
		    	}).then(function() {
		    	   	//window.location = base_url+'AreaAdministrador/pedidos';
		    	});
		    }else{
		    	window.location = caminho;
		    }
		});
		
}

function status(pedido_id){
	
	$('.modal-status-title').text('Alterar Pedido #'+ pedido_id);
	$('#alterar_status').attr('pedido_id', pedido_id);
	$("#m_modal_6").modal({
	    show: true
	});
	
}

function finalizarPedido(pedido_id,email){
	
	$('.modal-finalizar-title').text('Finalizar e Enviar E-mail do Pedido #'+ pedido_id);
	$('#finalizar_pedido').attr('pedido_id', pedido_id);
	$('#email').val(email);
	$("#m_modal_7").modal({
	    show: true
	});
	
}

function anexarNfe(pedido_id,cliente, nr_nf, arquivo_nfe,prazo_entrega ){

	$('.modal-anexar-nfe-title').text('Anexar nota fiscal ao pedido #'+ pedido_id);	
	$('#pedido_id').val( pedido_id);		
	$('#cliente_id').val( cliente);
	$('#link_arquivo').remove();
	if(arquivo_nfe != '' ){
		$('#nfe').before('<a href="'+base_url+'nfe/'+arquivo_nfe+'" target="_blank" id="link_arquivo" class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air"><i class="la la-file-pdf-o"></i></a>');
	}
	$('#nr_nf').val(nr_nf);	
	$('#prazo_entrega').val(prazo_entrega);	
	$("#anexarNfe").modal({
	    show: true
	});
	
}

function isEmail(email){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  	return re.test(email);

}

function gerarPdf(pedido_id){



	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/geraPedidosPdf/"+pedido_id+"/ajax/", 
		async: true,
		data: { pedido_id : pedido_id}
	}).done(function(data) {
		var dados = $.parseJSON(data);		
		
		if(dados.retorno != 'erro')
		{				
			window.open(base_url+'pedidos/'+dados.retorno);
		}else{
			swal({
	   			title: "Atenção!",
	   			text: "Erro ao gerar pdf do pedido, entre em contato com o webmaster",
	   			type: 'warning'
	    	}).then(function() {
	    	   	//window.location = base_url+'AreaAdministrador/pedidos';
	    	});
		}

	});		

}

function gerarPdfStartup(pedido_id, orcamento_id, cliente_id){


	$.ajax({
		method: "POST", 
		url:  base_url+"AreaAdministrador/retornaProdutosOrcamentos", 
		async: true,
		data: { orcamento_id 		: 	orcamento_id}
	}).done(function(data) {
		var dados = $.parseJSON(data);		

		$('#modelos_nr_series').empty();
		$('.modal-anexar-nfe-title').text('Gerar formulário de startup');
		$('.cliente_id').val(cliente_id);

		for(var i=0;i<dados.length;i++)
		{
			if(dados[i]['tipo_produto_id'] != 4){

				var html = '<div class="form-group m-form__group row novos_produtos">';
					html += '	<div class="input-group m-input-group m-input-group--solid">';
					html += '			<div class="input-group-prepend">';
					html += '				<span class="input-group-text" id="basic-addon1">';
					html += dados[i]['modelo'];
					html += '				</span>';						
					html += '			</div>';
					html += '			<input type="text" class="form-control m-input" name="nr_serie[]" placeholder="Nr. de Série" value="" style="background: #fff;">';
					html += '			<input type="hidden" class="form-control m-input" name="modelo[]" placeholder="Produtos" value="'+ dados[i]['modelo']+'" style="background: #fff;">';
					html += '		</div>';
					html += '	</div>';
						
				for(var j=0;j<dados[i]['qtd'];j++)
				{
					$('#modelos_nr_series').append(html);
				}
			}
		}
		
		$("#gerarPdfStartup").modal({
	    	show: true
		});
	});
}

