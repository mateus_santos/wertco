$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('.date').datepicker({
		orientation: 'bottom',
		format: 'dd/mm/yyyy'
	});
	//Tabela de chamados
	$('#table-chamados').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#table-chamados_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#table-chamados_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	$('#chamados').bind('click', function(){
		$("#modal_chamados").modal({
	    	show: true
		});
	});

	$('#obs').bind('click', function(){
		$("#m_obs").modal({
	    	show: true
		});
	});


	//Tabela de pedidos
	$('#table-pedidos').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#table-pedidos_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#table-pedidos_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	$('#pedidos').bind('click', function(){
		$("#modal_pedidos").modal({
	    	show: true
		});
	});

	$('#cpf').mask('000.000.000-00');
	$('#telefone').mask('(00) 0000 - 00000');
	$('#celular').mask('(00) 0000 - 00000');

	$('#cpf').bind('focusout', function(){
		var cpf = $(this).val();
		if(cpf != ""){
			$.ajax({
				method: "POST",
				url: base_url+'clientes/verifica_cpf',
				async: true,
				data: { cpf 	: 	cpf },
				success: function( data ) {
					var dados = $.parseJSON(data);
					if(dados.status == 'erro'){
						swal({
							title: 'Atenção!',
							text: dados.mensagem,
							type: 'warning'
						}).then(function() {
							$('#cpf').val('');
							$('#cpf').focus();
						});
					} 
				}
			});
		}
	});
	
	$('#email').bind('focusout', function(){
		var email = $(this).val();
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test(email)){
			swal(
				'Ops!',
				'E-mail inválido',
				'error'
			).then(function() {
				$('#email').val('');				
			});
		}else{
			$.ajax({
				method: "POST",
				url: base_url+'clientes/verifica_email',
				async: true,
				data: {
					email 	: 	$('#email').val()
				},
				success: function( data ) {
					var dados = $.parseJSON(data);
					if(dados.status == 'erro'){
						swal({
							title: 'Atenção!',
							text: dados.mensagem,
							type: 'warning'
						}).then(function() {
							$('#email').val('');
							
						});
					}
				}
			});
		}
	});

	//Tabela de ANEXOS
	$('#table-anexos').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "asc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#table-anexos_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#table-anexos_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	$('#anexos').bind('click', function(){
		$("#modal_anexos").modal({
	    	show: true
		});
	});
	
});

function gerarPdf(pedido_id){

	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/geraPedidosPdf/"+pedido_id+"/ajax/", 
		async: true,
		data: { pedido_id : pedido_id}
	}).done(function(data) {
		var dados = $.parseJSON(data);		
		
		if(dados.retorno != 'erro')
		{				
			window.open(base_url+'pedidos/'+dados.retorno);
		}else{
			swal({
	   			title: "Atenção!",
	   			text: "Erro ao gerar pdf do pedido, entre em contato com o webmaster",
	   			type: 'warning'
	    	}).then(function() {
	    	   	//window.location = base_url+'AreaAdministrador/pedidos';
	    	});
		}

	});		

}

function adicionar_funcionario(){
	if($('#nome').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o nome do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	
	if($('#email').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o email do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	if($('#telefone').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o telefone do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	var subtipo_cadastro_id = '';
	if( $('#tipo_cadastro_id').val() == 4 && $('#subtipo_cadastro_id').val() == "" ){
		swal({
			title: "Campo em branco",
			text: "Preencha o telefone do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});

		subtipo_cadastro_id = $('#subtipo_cadastro_id').val();

		return;	
	}

	$.ajax({
		method: "POST",
		url: base_url+'usuarios/cadastrar',
		async: true,
		data: {
			nome: 		$('#nome').val(),
			cpf: 		$('#cpf').val(),
			email: 		$('#email').val(),
			telefone: 	$('#telefone').val(),
			celular: 	$('#celular').val(),
			empresa_id: $('#empresa_id').val(),
			tipo_cadastro_id: $('#tipo_cadastro_id').val(),
			subtipo_cadastro_id : subtipo_cadastro_id
		},
		success: function( data ) {
			var dados = $.parseJSON(data);
			swal({
				title: dados.titulo,
				text: dados.texto,
				type: dados.tipo
			}).then(function() {
				if(dados.tipo == 'success') location.reload();
			});
		}
	});
}

function anexarNfe(pedido_id,cliente, nr_nf, arquivo_nfe,prazo_entrega ){

	$('.modal-anexar-nfe-title').text('Anexar nota fiscal ao pedido #'+ pedido_id);	
	$('#pedido_id').val( pedido_id	);		
	$('#cliente_id').val( cliente 	);
	$('#link_arquivo').remove();
	if(arquivo_nfe != '' ){
		$('#nfe').before('<a href="'+base_url+'nfe/'+arquivo_nfe+'" target="_blank" id="link_arquivo" class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air"><i class="la la-file-pdf-o"></i></a>');
	}
	$('#nr_nf').val(nr_nf);	
	$('#prazo_entrega').val(prazo_entrega);	
	$("#anexarNfe").modal({
	    show: true
	});

	if(dt_emissao != ''){
		dt_emissao = dt_emissao_nf.split('-');
		$('#dt_emissao_nf').val(dt_emissao[2]+'/'+dt_emissao[1]+'/'+dt_emissao[0]);
	}

	if( dt_emissao_nf != '' ){
		$('#enviar_nf').attr('style','display: none');
	}else{
		$('#enviar_nf').attr('style','display: block');
	}
	
}

function excluirAnexo(id, empresa_id) {
    swal({
        title: 'Exclusão de Anexo',
        text: 'Tem certeza que deseja excluir este Anexo?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: "swal2-confirm btn btn-danger m-btn m-btn--custom",
        confirmButtonText: "Excluir",
        cancelButtonText: "Cancelar"
    }).then(function(isConfirm) {
        if (isConfirm.value) {            
           $.ajax({
				method: "POST",
				url: base_url+'AreaAdministrador/excluirAnexoEmpresa',
				async: true,
				data: { id : id },
				success: function( data ) {
					var dados = $.parseJSON(data);
					swal({
						title: 'Ok',
						text: 'Anexo excluído.',
						type: 'success'
					}).then(function() {
						location.href=base_url+'AreaAdministrador/visualizarEmpresa/'+empresa_id;
					});
				}
			}); 
        }
    });

}
