$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
function excluirProspeccaoStatus(id){

		var id = id;
		swal({
		  title: 'Excluir?',
		  text: "Deseja realmente excluir esse status?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Sim!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					method: "POST",
					url:  base_url+"AreaAdministradorComercial/excluirStatusProspeccao", 
					async: false,
					data: { id 	: id }
				}).done(function(data) {
					var dados = $.parseJSON(data);		
					console.log(dados);
					if(dados.retorno == 'sucesso'){
						swal({
				   			title: "OK!",
				   			text: "Status Excluído com sucesso",
				   			type: 'success'
				    	}).then(function() {
				    	   	window.location = base_url+'AreaAdministradorComercial/prospeccoesStatus';
				    	}); 
					}
				});	
			}
		})
	
}