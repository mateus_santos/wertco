$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
function excluirfretes(id){

		var id = id;
		swal({
		  title: 'Excluir?',
		  text: "Deseja realmente excluir esse frete?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Sim!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					method: "POST",
					url:  base_url+"AreaAdministrador/excluirfrete", 
					async: false,
					data: { id 	: id }
				}).done(function(data) {
					var dados = $.parseJSON(data);		
					
					if(dados.retorno == 'sucesso'){				
						swal({
				   			title: "OK!",
				   			text: "Fretes Excluído com sucesso",
				   			type: 'success'
				    	}).then(function() {
				    	   	window.location = base_url+'AreaAdministrador/fretes';
				    	}); 
					}
				});	
			}
		})
	
}