$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

});

function listaAnexos(chamado_id){
	
	$('.m_lista_anexo_title').text('Anexos relacionados ao chamado #'+ chamado_id);	
	
	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/buscaAnexosChamados", 
		async: true,
		data: { chamado_id 	: 	chamado_id}
	}).done(function(data) {

		var dados = $.parseJSON(data);	
		$('.anexos').empty();

		if( dados.length == 0){
			$('.anexos').append('<h5 style="color: #ffcc00;">Não foram anexado documentos a este chamado.</h5>');

		}else{
		
			for(var i = 0; i< dados.length;i++){
				
				$('.anexos').append('<a target="_blank" class="col-md-3 col-lg-3 col-xs-1" href='+base_url+'chamados_atividades/'+dados[i].arquivo+' style="text-align: center;" ><i class="flaticon-file-1" style="font-size: 40px;color: #ffcc00;"></i><br/>'+(parseInt(i)+1)+'</a>');
			}

		}	
		
	});
	$("#m_lista_anexo").modal({
	    show: true
	});
	
}