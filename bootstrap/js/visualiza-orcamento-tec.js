$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('.valor_unitario').mask('#.##0,00', {reverse: true}); 
	if( $('#status_orcamento').val() == 8 || $('#status_orcamento').val() == 8 )
	$("#geraPdf").bind('click', function(){

		orcamento_id 	= 	$(this).attr('orcamento_id');	
			
		$.ajax({
			method: "POST",
			url:  base_url+"AreaTecnicos/geraOrcamento", 
			async: false,
			data: { orcamento_id 		: 	orcamento_id}
		}).done(function(data) {

			var dados = $.parseJSON(data);		
			
			if(dados.retorno != 'erro')
			{				
				window.open(base_url+'pdf/'+dados.retorno);
			}

		});		
	});

});