$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#pesquisa').mask('00.000.000/0000-00');
    $('#enviar').bind('click', function(){
    	
    	$.ajax({
			method: "POST",
			url: base_url+"AreaFeira/pesquisarOrcamentosAjax",
			async: true,
			data: { cnpj 	: 	$('#pesquisa').val()	 }
		}).done(function( data ) {
			var dados = $.parseJSON(data);			
			$('#mostra-orcamentos').empty();
			$('#mostra-orcamentos').append(dados.retorno);			
				
		});
	});

	$(".geraPdf").bind('click', function(){

		orcamento_id 	= 	$(this).attr('orcamento_id');	
		contato 		= 	$(this).attr('contato');
		
		
		$.ajax({
			method: "POST",
			url:  base_url+"AreaFeira/geraOrcamento/"+orcamento_id+"/ajax/"+contato, 
			async: true,
			data: { orcamento_id 	: 	orcamento_id,
					contato 		: 	contato	}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno != 'erro')
			{
				window.open(base_url+'pdf/'+dados.retorno);
			}

		});		
		
	});

});

function visualizar_orcamento(orcamento_id){	
	 	
	window.open(base_url+'AreaFeira/visualizarOrcamento/'+orcamento_id);

}