$(document).ready(function(){
	$.fn.datetimepicker.defaults.language = 'pt-BR'; 
	$ ( ".date" ). datetimepicker ({
        format: 'dd/mm/yyyy hh:ii:ss',
        autoclose : true , 
        todayBtn : true , 
        locale : 'pt-BR',
        language : 'pt-BR',
        minuteStep : 10
    });

	
	$('#inicio_atendimento').bind('focusout', function(){
		var chamado_id = $('#id_chamado').val();
		$.ajax({
			method: "POST",
			url: base_url+"AreaAdministrador/buscaSolicitacaoAtendimento",
			async: true,
			data: { chamado_id : chamado_id },			
			success: function( data ) {
				var dados = $.parseJSON(data);
				if(dados.retorno == 0){
					swal({
			   			title: "Atenção!",
			   			html: "<b>Nenhuma soicitação de atendimento foi emitida, emita uma solicitação e edite o início de atendimento.<b>",
			   			type: 'warning'
			    	}).then(function() {
			    		$('#inicio_atendimento').val('');
			    	});
		    	}
			}
		});
	});

	$('.copiar').bind('click', function(){

		$('#modal-copiar').modal('show');
		$('#indice_atividade').text($(this).attr('indice'));
		$('#copiar_atividade').attr('copiar_atividade',$(this).attr('atividade_id'));

	});

	$('#copiar_atividade').bind('click', function(){
		var chamado_id 		= 	$('#chamado_id').val();
		var atividade_id 	= 	$(this).attr('copiar_atividade');
		var modelo 			= 	$('#modelo_id_c').val();
		var numero_serie 	= 	$('#nr_serie_c').val();	
		var observacao 		= 	$('#obs_c').val();

		swal({
			title: 'Copiar atendimentos?',
			text: "Deseja copiar os atendimentos dessa atividade?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim!',
			cancelButtonText: 'Não!'
		}).then((result) => {
			if (result.value) {

				$.ajax({
					method: "POST",
					url:  base_url+"AreaAdministrador/copiarAtividadeAtendimento", 
					async: false,
					data: { atividade_id 	: 	atividade_id,
							atendimento 	: 	's',
							modelo_id 		: 	modelo,
							numero_serie 	: 	numero_serie,
							observacao 		: 	observacao }
				}).done(function(data) {
					var dados = $.parseJSON(data);		
					
					if(dados.retorno == 'sucesso'){				
						swal({
				   			title: "OK!",
				   			text: "Atividade copiada com sucesso.",
				   			type: 'success'
				    	}).then(function() {
				    		window.location.href = base_url+"AreaAdministrador/editaChamado/"+chamado_id;   	
				    	}); 
					}
				});	
			
			}else{

				$.ajax({
					method: "POST",
					url:  base_url+"AreaAdministrador/copiarAtividadeAtendimento", 
					async: false,
					data: { atividade_id 	: 	atividade_id,
							atendimento 	: 	'n',
							modelo_id 		: 	modelo,
							numero_serie 	: 	numero_serie,
							observacao 		: 	observacao }
				}).done(function(data) {
					var dados = $.parseJSON(data);		
					
					if(dados.retorno == 'sucesso'){				
						swal({
				   			title: "OK!",
				   			text: "Atividade copiada com sucesso.",
				   			type: 'success'
				    	}).then(function() {
				    		window.location.href = base_url+"/areaAdministrador/editaChamado/"+chamado_id;   	
				    	}); 
					}
				});	
			}
		});	
	});

	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	// Adicionar despesa ordem de pagamento
	$('.add').bind('click', function(){

        var html = $('#modelo').clone();                
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
        $(html).find('.bombas').attr('indice', indice);
        $(html).find('.valor_unitario').attr('indice', indice);
        $(html).find('.valor_unitario').attr('id', 'valor_unitario_'+indice);
        $(html).find('.qtd').attr('indice', indice);
        $(html).find('.qtd').attr('id', 'qtd_'+indice);

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();
        body+="</tr>";
        $('.table-itens').append(body);
        $('#modelo').attr('total_indice',indice);
        $('.valor_unitario').unmask();
		$('.valor_unitario').mask('#.##0,00', {reverse: true});
        excluirLinhaDespesa();
        mostraValor(indice);
        $('table tbody tr:odd').addClass('zebraUm');
		$('table tbody tr:even').addClass('zebraDois');

		$(".data").datepicker({
            todayHighlight:!0, orientation:"top left", templates: {
				leftArrow: 	'<i class="la la-angle-left"></i>',
				rightArrow: '<i class="la la-angle-right"></i>'
			}, format: 'dd/mm/yyyy'
        });
    });


	if($('#solicitacao_emitida').val() == 1 ){
		$('div.form_autorizacao input').attr('disabled', true);
		$('div.form_autorizacao select').attr('disabled', true);
		$('div.form_autorizacao textarea').attr('disabled', true);
		$('div.form_autorizacao button').attr('disabled', true);		
	}

	$('#libera_emissao_aut').bind('click', function(){
		$('div.form_autorizacao input').attr('disabled', false);
		$('div.form_autorizacao select').attr('disabled', false);
		$('div.form_autorizacao textarea').attr('disabled', false);
		$('div.form_autorizacao button').attr('disabled', false);		
	});

    $('.excluirItem').bind('click', function(){
    	var id  = $(this).attr('item_id');
    	var obj = $(this); 
		
		$.ajax({
			method: "POST",
			url: base_url+'areaAdministrador/excluirItemOp',
			async: false,
			data: {
				id: id
			},
			success: function( data ) {
				var dados = $.parseJSON(data);
				if( dados.retorno == 'sucesso' ){

					swal(
						'OK!',
						'Item Excluído com sucesso',
						'warning'
					).then(function() {
						$('#'+id).fadeOut('slow').remove();
						atualizaTotal();
					});	
				}
			}

		});		

		
    });
    
    $('.atualizaTotal').bind('click', function(){
    	
    	atualizaTotal();
    });

    mostraValor('0');

    /* Verifica se deve liberar a edição de apenas o titulo da ordem de pagamento */
    if( $('.excluirItem').length > 0 ){
		$('.despesas').attr('required', false);
		$('.qtd').attr('required', false);
		$('.data').attr('required', false);
		$('.valor_unitario').attr('required', false);
	}else{
		
		$('.despesas').attr('required', true);
		$('.qtd').attr('required', true);
		$('.data').attr('required', true);
		$('.valor_unitario').attr('required', false);
	}

    $('#emitirOp').bind('click', function(){
    	if( $(this).attr('acao') == 'reemitida' ){
    		$('#acao_emissao').val('reemitida');
    	}

    	$("#modal_emitir_op").modal();
    });

    $('.valor_unitario').mask('#.##0,00', {reverse: true}); 
    
    $('footer.m-footer').attr('style','position: fixed; bottom: 0;')
    
    $('#cnpj').mask('00.000.000/0000-00');
    $('#cpf').mask('000.000.000-00');
    $('#bombas').mask('00000'); 
    $('#bicos').mask('00000');  
    $('.qtd').mask('0000');
	// Máscaras	
	$('#cpf').mask('000.000.000-00');
	$('#telefone').mask('(00) 0000 - 00000');

	//hack para o select do dofeeito encontrado
	setTimeout(function(){$('.bootstrap-select').attr('style','width: 90% !important; float: left;'); }, 3000); 
	setTimeout(function(){$('.dropdown-menu').css('overflow','show'); }, 3000); 
	var status_chamado = $('#status_id').val();
	
	$('#status_id').bind('change', function(){
		var chamado_id = $('#chamado_id').val();
		
		if( $(this).val() == '10' ){
			swal(
				'Atenção!',
				'Esse status não pode ser selecionado!',
				'warning'
			).then(function() {
				
				$("#status_id").find('option[value="10"]').removeAttr('selected');
				$("#status_id").find('option[value=""]').attr('selected','selected');
			});
		}

		if( $(this).val() == '3' ){

			$.ajax({
				method: "POST",
				url: base_url+'areaAdministrador/verificaStatusAtividade',
				async: false,
				data: {
					chamado_id: chamado_id
				},
				success: function( data ) {
					var dados = $.parseJSON(data);
					if( dados.total > 0 ){
						swal(
							'Atenção!',
							'Existe(m) Atividade(s) que não está(ão) fechada(s)',
							'warning'
						).then(function() {
							$("#status_id").find('option').removeAttr('selected');							
							$("#status_id").find('option[value="'+status_chamado+'"]').attr('selected','selected');
						});	
					}
				}

			});				
		}

	});

	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/listaAnexosChamado',
		async: true,
		data: {
			chamado_id: $('#id_chamado').val()
		},
		success: function( data ) {
			var dados = $.parseJSON(data);
			$('#anexo_chamado').empty();			
			for( var i=0;i<dados.length;i++ ){
				var tipo_arquivo = dados[i].arquivo.split('.');
				if(tipo_arquivo[1] == 'png' || tipo_arquivo[1] == 'jpg' || tipo_arquivo[1] == 'jpeg' || tipo_arquivo[1] == 'gif' || tipo_arquivo[1] == 'bmp' ){
					img = '<img src="'+base_url+'chamados_atividades/'+ dados[i].arquivo +'" class="img-fluid" style="max-height: 250px !important;" />';
				}else{
					img = '<i class="la la-file-o" style="font-size: 60px; color: #ffcc00;"></i>';
				}
				var html = 	'<div class="col-lg-3" id="'+dados[i].id+'">';
					html+=	'	<label><a href="http://www.wertco.com.br/chamados_atividades/'+dados[i].arquivo+'" target="_blank">'+dados[i].descricao+'</a></label>';
					html+=	'<a href="http://www.wertco.com.br/chamados_atividades/'+dados[i].arquivo+'" target="_blank">'+img+'</a>';
					html+=	'<a href="#" onclick="excluirAnexo('+dados[i].id+', this)" tipo="chamado" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: 10px;"><i class="la la-times" style="color: #f0f0f0;"></i>';
					html+=	'</div>';

				$('#anexo_chamado').append(html);	
			}
		}

	});

	/*$('#emitirEmailOp').bind('click',function(){
    	var ordem_pagamento_id 	= 	$(this).attr('ordem_pagamento_id');
    	var email 				= 	$('#email_tecnico').val();
    	var chamado_id 			=  	$("#id_chamado").val()

    	$.ajax({
			method: "POST",
			url: base_url+"AreaAdministrador/emitirOp",
			async: true,
			data: {	ordem_pagamento_id 	: 	ordem_pagamento_id,
					email	 			: 	email	},			
			success: function( data ) {
				var dados = $.parseJSON(data);
				if(dados){
					swal(
						'OK!',
						'Ordem de Pagamento emitida com sucesso',
						'success'
					).then(function() {
						window.location.href = "http://www.wertco.com.br/areaAdministrador/editaChamado/"+chamado_id;
					});		
				}
			}
		});
    });*/

	$('.adicionar_defeito').bind('click', function(){
		$('#div_novo_defeito_constatado').toggle();
		if( $(this).hasClass('la-plus-circle') ){
			$(this).removeClass('la-plus-circle');
			$(this).addClass('la-minus-circle');
		}else if($(this).hasClass('la-minus-circle') ){
			$(this).removeClass('la-minus-circle');
			$(this).addClass('la-plus-circle');
		}
	});

	$('.edit_adicionar_defeito').bind('click', function(){
		$('#div_edit_novo_defeito_constatado').toggle();
		if( $(this).hasClass('la-plus-circle') ){
			$(this).removeClass('la-plus-circle');
			$(this).addClass('la-minus-circle');
		}else if($(this).hasClass('la-minus-circle') ){
			$(this).removeClass('la-minus-circle');
			$(this).addClass('la-plus-circle');
		}
	});


	$('#cpf').bind('focusout', function(){
		var cpf = $(this).val();
		if(cpf != ""){
			$.ajax({
				method: "POST",
				url: base_url+'clientes/verifica_cpf',
				data: { cpf 	: 	cpf },
				success: function( data ) {
					var dados = $.parseJSON(data);
					if(dados.status == 'erro'){
						swal(
							'Ops!',
							dados.mensagem,
							'error'
						).then(function() {
							$('#cpf').focus();
						});
					}
				}
			});
		}
	});
	
	// Binds
	
	$('#email').bind('focusout', function(){
		var email = $(this).val();
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test(email)){
			swal(
				'Ops!',
				'E-mail inválido',
				'error'
			).then(function() {
				$('#email').focus();
			});
		}
	});
	
	$('#contato_id').bind('change', function(){
		var validade_cartao = $("#contato_id option:selected").attr('validade');
        if (validade_cartao != 'null') $("#cartao_validade").val(validade_cartao);
		else $("#cartao_validade").val('');
	});
	
	$('#edit_contato_id').bind('change', function(){
		var validade_cartao = $("#edit_contato_id option:selected").attr('validade');
        if (validade_cartao != 'null') $("#edit_cartao_validade").val(validade_cartao);
		else $("#edit_cartao_validade").val('');
	});
	
	$('#novo_usuario').bind('click', function(){
    	var parceiro_id = $('#parceiro_id').val();
    	if(parceiro_id != '' || parceiro_id != undefined)
    	{

    	}else{
    		swal(
		  		'Ops!',
		  		'Selecione um cliente',
		  		'warning'
			);
    	}
    });
	
	// Autocompletes	
	$( "#tecnico" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaParceiros",
		minLength: 3,
		select: function( event, ui ) {
			$('#subatividade_parceiro_id').val(ui.item.id);
			$('#endereco_tecnico').val(ui.item.endereco);
			$('#cidade_tecnico').val(ui.item.cidade);
			$("#parceiro_id").val(ui.item.id);
			$.ajax({
				method: "POST",
				url: base_url+"AreaAdministrador/retornaUsuariosEmpresaWS",
				async: true,
				data: { empresa_id 	: 	ui.item.id },
				beforeSend: function(){
					$('#contato_id').empty();
				},
				success: function( data ) {
					var dados = $.parseJSON(data);
					if(dados.length > 0){
						var html='';
						for (var i = 0; i < dados.length; i++) {
							html+= '<option validade="'+dados[i].validade+'" value="'+dados[i].id+'">'+dados[i].label+'</option>';
						}
						$("#cartao_validade").val(dados[0].validade);
					} else {
						var html='<option value="">Nenhum contato cadastrado</option>';
					}
					$('#contato_id').append(html);
				}
			});
			
		}
    });
	
	$( "#edit_tecnico" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaParceiros",
		minLength: 3,
		select: function( event, ui ) {
			$('#edit_parceiro_id').val(ui.item.id);
			$('#edit_endereco_tecnico').val(ui.item.endereco);
			$('#edit_cidade_tecnico').val(ui.item.cidade);
			$("#parceiro_id").val(ui.item.id);
			$.ajax({
				method: "POST",
				url: base_url+"AreaAdministrador/retornaUsuariosEmpresaWS",
				async: true,
				data: { empresa_id 	: 	ui.item.id },
				beforeSend: function(){
					$('#edit_contato_id').empty();
				},
				success: function( data ) {
					var dados = $.parseJSON(data);
					if(dados.length > 0){
						var html='';
						for (var i = 0; i < dados.length; i++) {
							html+= '<option validade="'+dados[i].validade+'" value="'+dados[i].id+'">'+dados[i].label+'</option>';
						}
						$("#edit_cartao_validade").val(dados[0].validade);
					} else {
						var html='<option value="">Nenhum contato cadastrado</option>';
					}
					$('#edit_contato_id').append(html);
				}
			});
			
		}
    });


	$( "#tecnico_autoriz" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaTecnicos",
		minLength: 3,
		select: function( event, ui ) {
			$('#tecnico_id').val(ui.item.id);		
			$('#email_autoriz').val(ui.item.email);
		}
    });

   
	
	//Datatable 	
	$('#tabela_subatividades').DataTable({
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		},
		"fnDrawCallback": function( oSettings ) {
      		$('div#tabela_subatividades_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
			$('div#tabela_subatividades_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
			$('a[aria-controls="tabela_subatividades"]').addClass('m-datatable__pager-link m-datatable__pager-link');
			$('.dataTables_filter').css('float','right');	
			$('table thead').css('background-color', '#f4f3f8');
			//$('table thead').css('height', '150px');
			$('table tbody tr:odd').addClass('zebraUm');
			$('table tbody tr:even').addClass('zebraDois');
    	}
	});

	$("#enviar1").bind('click', function(){
		var id = $("#atividade_id").val();
		$('#id_atividade').val(id);
		if( $('#anexos_atividade').val() == '' ||  $('#anexos_atividade').val() == undefined){
			swal(
				'ATENÇÃO!',
				'Selecione algum arquivo para enviar.',
				'warning'
			).then(function() {});	
			
		}else{
			$("#form2").submit();
		}
		
	});


	/***************** SOLICITAÇÃO DE PEÇAS ********************/

	$('#empresa_entrega').bind('change', function(){
		var empresa_id 	= $(this).val();
		var destinatario 	= $(this).find('option:selected').text();
		$('#destinatario').val(destinatario);
		$.ajax({
			method: "POST",
			url: base_url+"AreaAdministrador/retornaEnderecoEmpresa",
			async: true,
			data: { empresa_id 	: 	empresa_id },			
			success: function( data ) {
				var dados = $.parseJSON(data);
				if(dados.length > 0){
					
					$('#endereco_entrega').empty().val('Rua: '+dados[0].endereco+' - Bairro: '+dados[0].bairro+' - CEP: '+dados[0].cep+' - Cidade: '+dados[0].cidade+' - Estado: '+dados[0].estado);


				} else {
					var html='<option value="">Nenhum contato cadastrado</option>';
				}
				$('#edit_contato_id').append(html);
			}
		});			
	});

	autocompletePecas($('#peca_0'));

	$('.add_pecas').bind('click', function(){

        var html = $('#modelo_peca').clone();                
        var indice = parseInt($('#modelo_peca').attr('total_indice')) + 1;
        $(html).find('.remover_pecas').css('display', 'block');
        $(html).find('.remover_pecas').attr('indice', indice);        
        $(html).find('.qtd_pecas').attr('indice', indice);
        $(html).find('.qtd_pecas').attr('id', 'qtd_'+indice);
        $(html).find('.pecas').attr('id', 'peca_'+indice);
        $(html).find('.pecas').attr('indice', indice);
        $(html).find('.peca_id').attr('id', 'peca_id_'+indice);
        $(html).find('.peca_id').val('0');

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();                
        body+="</tr>";                
        $('.table-itens-pecas').append(body);                
        $('#modelo_peca').attr('total_indice',indice);        
        $('#modelo_peca').attr('indice',indice);        
        excluirLinhaPecas();
        autocompletePecas($('#peca_'+indice));
        $('table tbody tr:odd').addClass('zebraUm');
		$('table tbody tr:even').addClass('zebraDois');

    });

	$('.excluirItemPeca').bind('click', function(){
    	var id  = $(this).attr('item_id');
    	var obj = $(this); 
		
		$.ajax({
			method: "POST",
			url: base_url+'areaAdministrador/excluirItemSolicitacaoPecas',
			async: false,
			data: {
				id: id
			},
			success: function( data ) {
				var dados = $.parseJSON(data);
				if( dados.retorno == 'sucesso' ){

					swal(
						'OK!',
						'Item Excluído com sucesso',
						'warning'
					).then(function() {
						$('#'+id).fadeOut('slow').remove();
						
					});	
				}
			}

		});	

	});	

	$("#adicionar_andamento").bind('click', function(){

		solicitacao_id 		= 	$(this).attr('solicitacao_id');
		status_id			= 	$(this).attr('status_id'); 
		andamento			= 	$("#andamento_texto").val();
		chamado_id 			= 	$('#chamado_id').val();
		if( andamento == '')
		{
			swal({
	   			title: "Atenção!",
	   			text: "Insira alguma observação para ser adicionada ao processo desta venda!",
	   			type: 'warning'
		    }); 

		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaAssistencia/insereAndamentoSolicitacao", 
				async: true,
				data: { solicitacao_id 			: 	solicitacao_id,
						status_solicitacao_id	: 	status_id,
						andamento 				: 	andamento}
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{				
					swal({
			   			title: "OK!",
			   			text: "Andamento adicionado a solicitação #"+solicitacao_id,
			   			type: 'success'
			    	}).then(function() {
			    	   window.location.href = base_url+"areaAdministrador/editaChamado/"+chamado_id;

			    	});
				}
			});		

		}
	});

	/************************************************************/


});

   

function excluirLinhaPecas()
{
    $('.remover_pecas').bind('click', function(){
        var indice = $(this).attr('indice');
        $('tr[indice="'+indice+'"]').each(function(){
        	
        	if( $(this).attr('id') !== 'modelo_peca' ){
        		$(this).remove();
        	}
        });

    });
}

function autocompletePecas(obj){

	$( obj ).autocomplete({
		source: base_url+"AreaAdministrador/retornaPecas",
		minLength: 3,
		select: function( event, ui ) {

			indice = $(this).attr('indice');
			$('#peca_id_'+indice).val(ui.item.id);

		},		
		response: function (event, ui) {
	        if (!ui.content.length) {        
	        	indice = $(this).attr('indice');
				$('#peca_id_'+indice).val('0');	        	
	        }

	    }
    });
}

function atualizaTotal(){

	var total = 0;

	$('.valor_unitario').each(function(){

		if($(this).val() != undefined && $(this).val() != ''){
			
			if( $(this).hasClass('item_inserido') ){
				valor = $(this).val().replace('.','').replace(',','.');
				total = ((parseFloat(valor).toFixed(2) * parseInt($(this).attr('qtd'))) + parseFloat(total)).toFixed(2);
			}else{
				
				indice = $(this).attr('indice');
				if( $('#qtd_'+indice).val() != undefined && $('#qtd_'+indice).val() != '' ){
					valor = $(this).val().replace('.','').replace(',','.');	
					total = ((parseFloat(valor).toFixed(2) * parseInt($('#qtd_'+indice).val())) + parseFloat(total)).toFixed(2);
				}	
			}
		}
	});
	
	$('.total_op').text(total);
	$('.moeda').unmask();
	$('.moeda').mask('#.##0,00', {reverse: true});

}


function mostraValor(indice){

	$('.despesas').bind('change', function(){
        
        $('#valor_unitario_'+indice).val($(this).find(':selected').attr('valor')); 
        $('.valor_unitario').unmask();
		$('.valor_unitario').mask('#.##0,00', {reverse: true});      
    }); 
}

// Defeito / Causa / Solução
function adiciona_defeito()
{

	var defeito = $("#novo_defeito_constatado").val();	   	
	
	if ($.trim(defeito) != '') {
		$.ajax({
			method: "POST",
			url: base_url+'areaAdministrador/inserirDefeitoWS',
			async: true,
			data: {
				descricao: defeito
			},
			success: function( id ) {
				
				if(id){
					$("#novo_defeito_constatado").val('');
					$("#defeito_id").append('<option value='+id+' selected>'+defeito+'</option></select></div></div>');
					$("#defeito_id").selectpicker('refresh');
					$('.adicionar_defeito').removeClass('la-minus-circle').addClass('la-plus-circle');
					$('#div_novo_defeito_constatado').toggle('slow');
				}else{
					
				}
			}
		});
	}else{
		swal({
			title: "Campo em branco",
			text: "Insira um defeito",
			type: "warning"
		});
	}
}

function adiciona_defeito_edicao()
{
	var defeito = $("#edit_novo_defeito_constatado").val();
	if ($.trim(defeito)=='') return;
   
	// Changed from previous version to cater to new
	// layout used by bootstrap-select.
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/inserirDefeitoWS',
		async: true,
		data: {
			descricao: defeito
		},
		success: function( id ) {
			if(id){
				$("#edit_novo_defeito_constatado").val('');
				$("#edit_causa_id").empty();
				$("#edit_solucao_id").empty();
				$("#edit_defeito_id").append('<option value='+id+' selected>'+defeito+'</option></select></div></div>');
				$("#edit_defeito_id").selectpicker('refresh');
				$("#edit_causa_id").selectpicker('refresh');
				$("#edit_solucao_id").selectpicker('refresh');
			}
		}
	});
}

function adiciona_causa()
{
	var causa = $("#edit_nova_causa").val();
	if ($.trim(causa) ==''){
		swal({
			title: "Campo em branco",
			text: "Insira uma causa",
			type: "warning"
		});
	} ;
	
	var defeito_id = $("#edit_defeito_id").val();
    
	// Changed from previous version to cater to new
	// layout used by bootstrap-select.
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/inserirCausaWS',
		async: true,
		data: {
			descricao: causa,
			defeito_id: defeito_id
		},
		success: function( id ) {
			if(id){
				$("#edit_nova_causa").val('');
				$("#edit_solucao_id").empty();
				$("#edit_causa_id").append('<option value='+id+' selected>'+causa+'</option></select></div></div>');
				$("#edit_causa_id").selectpicker('refresh');
				listar_solucoes(id);
			}
		}
	});
};

function adiciona_solucao()
{
	var solucao = $("#edit_nova_solucao").val();
	var causa_id = $("#edit_causa_id").val();
	
	if(causa_id == null) {
		swal({
			title: "Campo em branco",
			text: "Insira uma causa",
			type: "warning"
		});
	}   

   	if($.trim(solucao) != ''){

		// Changed from previous version to cater to new
		// layout used by bootstrap-select.
		$.ajax({
			method: "POST",
			url: base_url+'areaAdministrador/inserirSolucaoWS',
			async: true,
			data: {
				descricao: solucao,
				causa_id: causa_id
			},
			success: function( id ) {
				if(id){
					$("#edit_nova_solucao").val('');
					$("#edit_solucao_id").empty();
					$("#edit_solucao_id").append('<option value='+id+' selected>'+solucao+'</option></select></div></div>');
					$("#edit_solucao_id").selectpicker('refresh');
				}
			}
		});
	
	}else{

		swal({
			title: "Campo em branco",
			text: "Insira uma nova solução",
			type: "warning"
		});
	}
};

function listar_causas(defeito_id){
	//var id = elem.id;
	//var solucao_id = id.replace('causas','solucoes');
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/listarCausasWS',
		async: true,
		data: {
			defeito_id: defeito_id
		},
		success: function( data ) {
			var causas = JSON.parse(data);
			$("#edit_causa_id").empty();
			$("#edit_solucao_id").empty();
			for(var index in causas){
				$("#edit_causa_id").append('<option value="'+causas[index].id+'">'+causas[index].descricao+'</option>');
			}
			if(causas.length > 0){
				listar_solucoes(causas[0].id);
			}
			$("#edit_causa_id").selectpicker('refresh');
			$("#edit_solucao_id").selectpicker('refresh');
		}
	});
}

function listar_solucoes(causa_id){
	//var id = elem.id;
	//var solucao_id = id.replace('causas','solucoes');
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/listarSolucoesWS',
		async: true,
		data: {
			causa_id: causa_id
		},
		success: function( data ) {
			var solucoes = JSON.parse(data);
			$("#edit_solucao_id").empty();
			for(var index in solucoes){
				$("#edit_solucao_id").append('<option value="'+solucoes[index].id+'">'+solucoes[index].descricao+'</option>');
			}	
			$("#edit_solucao_id").selectpicker('refresh');
		}
	});
}

// Funções cadastro atividade
function change_modelo(modelo){
	console.log(modelo.nr_serie);
	if(modelo.value == 0){
		$("#div_modelo").css('display', 'block');
		$("#modelo_manual").attr('required', 'required');
	}
	else {
		$("#div_modelo").css('display', 'none');
		$("#modelo_manual").removeAttr('required');
	}
}

function valida_form_adiciona_atividade(){
	if($('#defeito_id').val() == 0){
		swal({
			title: "Campo em branco",
			text: "Selecione um defeito",
			type: "warning"
		});
		return false;
	}
}

// Funções edição atividade
function change_edit_modelo(modelo){
	if(modelo.value == 0){

		$("#div_edit_modelo").css('display', 'block');
		$("#edit_modelo_manual").attr('required', 'required');
	}else{

		$("#div_edit_modelo").css('display', 'none');
		$("#edit_modelo_manual").removeAttr('required');
	}
}

function valida_form_edita_atividade(){
	if($('#edit_defeito_id').val() == 0){
		swal({
			title: "Campo em branco",
			text: "Selecione um defeito",
			type: "warning"
		});
		return false;
	}
}

function adiciona_causa_solucao(){
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/inserirCausaSolucaoWS',
		async: true,
		data: {
			defeito_id: 	$('#edit_defeito_id').val(),
			causa_id: 		$('#edit_causa_id').val(),
			solucao_id: 	$('#edit_solucao_id').val(),
			atividade_id: 	$('#atividade_id').val()
		},
		success: function( id ) {
			console.log(id);
			if(id) editar_atividade($("#atividade_id").val());
		}
	});
	
}

function remove_causa_solucao(id){
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/removerCausaSolucaoWS',
		async: true,
		data: {
			id: id
		},
		success: function( id ) {
			if(id) editar_atividade($("#atividade_id").val());

			
		}
	});
	
}

function editar_atividade(atividade_id){
	console.log(atividade_id);
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/editaAtividadeWS',
		async: true,
		data: {
			atividade_id: atividade_id
		},
		success: function( data ) {
			var dados = $.parseJSON(data);
			var atividade = dados;
			if(atividade.id){
				$("#atividade_id").val(atividade.id);
				$("#subatividade_atividade_id").val(atividade.id);
				if(atividade.modelo_id == 0){
					$("#div_edit_modelo").css('display', 'block');
					$("#edit_modelo_manual").val(atividade.modelo_manual);
				} else {
					$("#div_edit_modelo").css('display', 'none');
				}
				$("#edit_numero_serie").val(atividade.numero_serie);
				$("#edit_status_id").val(atividade.status_id);
				$("#edit_observacao").val(atividade.observacao);
				$("#edit_modelo_id").val(atividade.modelo_id);
				
				var causas_solucoes = atividade.causas_solucoes;
				$("#div_causas").empty();
				var causa = '<div class="col-lg-6" id="causa"><label>Causa</label><div class="m-input-icon m-input-icon--right">';
				causa += '<select onchange="listar_solucoes(this.value)" id="edit_causa_id" class="form-control selectpicker" data-live-search="true">';
				causa += '</select><a href="#" style="color: #ffcc00 !important;font-weight: 100;float: right;">';
				causa += '<i class="la edit_adicionar_causa la-plus-circle" onclick="esconde_adiciona(nova_causa,this);" style="font-size: 35px;" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Adicionar novo defeito">';
				causa += '</i></a></div></div>';
				causa += '<div class="col-lg-6" style="float: right;display: none;" id="nova_causa">';
				causa += '		<label>Nova causa</label>';
				causa += '		<div class="m-input-icon m-input-icon--right">';
				causa += '		<input type="text"  id="edit_nova_causa" class="form-control m-input" placeholder="" style="float: left;width: 90%">';
				causa += '		<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-build"></i></span></span>';
				causa += '		<a href="#" onclick="adiciona_causa()" style="color: #008000ad !important;font-weight: 100;float: right;">';
				causa += '			<i class="la la-check-circle" style="font-size: 38px;" title="Adicionar nova causa"></i>';
				causa += '		</a>';
				causa += '</div>';								
				causa += '</div>';
				$("#div_causas").append(causa);
				$("#div_solucoes").empty();
				var solucao = '<div class="col-lg-6" id="solucao"><label>Solução</label><div class="m-input-icon m-input-icon--right">';
				solucao += '<select id="edit_solucao_id" class="form-control selectpicker" data-live-search="true">'
				solucao += '</select><a href="#" style="color: #ffcc00 !important;font-weight: 100;float: right;">';
				solucao += '<i class="la edit_adicionar_causa la-plus-circle" onclick="esconde_adiciona(nova_solucao,this)" style="font-size: 35px;" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Adicionar novo defeito">';
				solucao += '</i></a></div></div>';
				solucao += '<div class="col-lg-6" style="float: right;display: none;" id="nova_solucao">'
				solucao += '	<label>Nova solução</label>'
				solucao += '	<div class="m-input-icon m-input-icon--right">'
				solucao += '		<input type="text"  id="edit_nova_solucao" class="form-control m-input" placeholder="" style="float: left;width: 90%">'
				solucao += '		<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="la la-build"></i></span></span>'
				solucao += '		<a href="#" onclick="adiciona_solucao()" style="color: #008000ad !important;font-weight: 100;float: right;">'
				solucao += '			<i class="la la-check-circle" style="font-size: 38px;" title="Adicionar nova solução"></i>'
				solucao += '		</a>'
				solucao += '	</div>'
				solucao += '</div>'
				$("#div_solucoes").append(solucao);				
				var i = 0;	
				
				$('#div_causas_solucoes').empty();
				
				

				for(var index in causas_solucoes){
					
					var causa_solucao = '<div class="col-lg-4 defeito_cs"><label>Defeito</label>';
					causa_solucao += '<div class="m-input-icon m-input-icon--right"><select class="form-control" disabled>';
					causa_solucao += '<option>'+atividade.defeito.toUpperCase()+'</option></select></div></div>';
					causa_solucao += '<div class="col-lg-4 causa_cs"><label>Causa</label>';
					causa_solucao += '<div class="m-input-icon m-input-icon--right"><select class="form-control" disabled>';
					causa_solucao += '<option>'+causas_solucoes[index].causa+'</option></select></div></div>';
					causa_solucao += '<div class="col-lg-4 solucao_cs"><label>Solução</label>'; 
					causa_solucao += '<div class="m-input-icon m-input-icon--right" ><select class="form-control" disabled style="width: 90% !important; float: left;">'; 
					causa_solucao += '<option>'+causas_solucoes[index].solucao+'</option></select>';
					causa_solucao += '<a href="#" id="div_remove_causa_solucao_'+index+'" onclick="remove_causa_solucao('+causas_solucoes[index].id+')" style="color: #b920209c !important; float: right; font-weight: bold;">';
					causa_solucao += '<i class="la la-times-circle" style="font-size: 38px;" title="Remover causa/solucao"></i></a></div></div>';
				
				

					$("#div_causas_solucoes").prepend(causa_solucao);
					// se atividade estiver fechada ou cancelada não é possível remover causa/solução	
					if(atividade.status_id == 3 || atividade.status_id == 4){
						$("#div_remove_causa_solucao_"+index).css('display','none');
					} else {
						$("#div_remove_causa_solucao_"+index).css('display','inline-block');
					}

					i = parseInt(i) + 1;
				}
				
				var causa_solucao 	= 	'<div class="col-lg-12">';
				causa_solucao 		+= 	'<a href="#" onclick="adiciona_causa_solucao()" class="btn btn-success m-btn m-btn--icon"  id="div_add_causa_solucao"><span>';
				causa_solucao 	    += 	'<i class="la la-check-circle" style="font-size: 35px;" data-toggle="m-tooltip" data-placement="top" title="Adicionar nova causa/solucao"></i>';
				causa_solucao 		+=	'<span>Salvar Defeito, causa, solução</span></span></a></div>';
				$("#div_confirma_causas_solucoes").empty();
				$("#div_confirma_causas_solucoes").append(causa_solucao);
				
				
				

				var defeitos = atividade.defeitos;
				$("#edit_defeito_id").empty();
				for(var index in defeitos){
					$("#edit_defeito_id").append('<option value="'+defeitos[index].id+'">'+defeitos[index].descricao+'</option>');
				}
				$("#edit_defeito_id").val(atividade.defeito_id);
				
				var causas = atividade.causas;
				$("#edit_causa_id").empty();
				for(var index in causas){
					$("#edit_causa_id").append('<option value="'+causas[index].id+'">'+causas[index].descricao+'</option>');
				}
				
				var solucoes = atividade.solucoes;
				$("#edit_solucao_id").empty();
				for(var index in solucoes){
					$("#edit_solucao_id").append('<option value="'+solucoes[index].id+'">'+solucoes[index].descricao+'</option>');
				}
				
				// colocar input causa e solução
				if(atividade.status_id == 3 || atividade.status_id == 4){
					$("#edit_defeito_id").attr('disabled','disabled');
					$("#edit_causa_id").attr('disabled','disabled');
					$("#edit_solucao_id").attr('disabled','disabled');
					$("#edit_modelo_id").attr('disabled','disabled');
					$("#edit_numero_serie").attr('disabled','disabled');
					$("#edit_observacao").attr('disabled','disabled');
					$("#div_add_atendimento").css('display','none');
					$("#div_add_causa_solucao").css('display','none');
					$("#div_edit_novo_defeito_constatado").css('display','none');
					$("#div_causa_solucao").css('display','none');
					if(atividade.modelo_id == 0){
						$("#edit_modelo_manual").attr('disabled','disabled');
					}
				} else {
					$("#edit_defeito_id").removeAttr('disabled');
					$("#edit_modelo_id").removeAttr('disabled');
					$("#edit_numero_serie").removeAttr('disabled');
					$("#edit_observacao").removeAttr('disabled');
					$("#div_add_atendimento").css('display','inline-block');
					$("#div_add_causa_solucao").css('display','inline-block');

					if(atividade.modelo_id == 0){
						$("#edit_modelo_manual").removeAttr('disabled');
					}
				}
				$(".selectpicker").selectpicker('refresh');

				if(causas_solucoes.length > 0 ){
					//$("#edit_defeito_id").attr('readonly',true);
					$('#edit_defeito_id').attr('disabled', true);					
  					$('#edit_defeito_id').selectpicker('refresh');															
  					
				}else{
					$('#edit_defeito_id').attr('disabled', false);					
  					$('#edit_defeito_id').selectpicker('refresh');	

				}	

				var table = $('#tabela_subatividades').DataTable();
				table.clear();
				var subatividades = atividade.subatividades;
				for (var index in subatividades){
					var acoes = '<td data-field="Actions" class="m-datatable__cell " style="width: 5%;text-align: center !important;">';
					acoes += '<a href="#" onclick="editar_subatividade('+subatividades[index].id+','+atividade_id+')" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" >';
					if(atividade.status_id == 3 || atividade.status_id == 4){
						$("#edit_tecnico").attr('disabled', 'disabled');
						$("#edit_contato_id").attr('disabled', 'disabled');
						$("#edit_subatividade_observacao").attr('disabled', 'disabled');
					} else {
						$("#edit_tecnico").removeAttr('disabled');
						$("#edit_contato_id").removeAttr('disabled');
						$("#edit_subatividade_observacao").removeAttr('disabled');
					}
					acoes += '<i class="la la-edit editar"></i></a></td>';
					table.row.add([Number(index)+1, subatividades[index].observacao, subatividades[index].inicio, subatividades[index].fim, acoes]).draw(true);
				}
				$("#modal_edita_atividade").modal('show');
				table.search(' ').draw(true);
			} else {
				swal(
					'Ops!',
					'Aconteceu algum problema, Tente novamente!',
					'error'
				);
			}
		}
	});

	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/listaAnexosAtividades',
		async: true,
		data: {
			atividade_id: atividade_id
		},
		success: function( data ) {
			var dados = $.parseJSON(data);
			$('#anexos').empty();
			
			for( var i=0;i<dados.length;i++ ){
				var html = 	'<div class="col-lg-3" id="'+dados[i].id+'">';
					html+=	'	<label>'+dados[i].descricao+'</label>';
					html+=	'<a href="http://www.wertco.com.br/chamados_atividades/'+dados[i].arquivo+'" target="_blank"><img src="'+base_url+'chamados_atividades/'+dados[i].arquivo+'" class="img-fluid" style="max-height: 250px !important;"  /></a>';
					html+=	'<a href="#" onclick="excluirAnexo('+dados[i].id+', this)" tipo="atividade" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" style="float: right;margin-top: 10px;"><i class="la la-times" style="color: #f0f0f0;"></i>';
					html+=	'</div>';
				$('#anexos').append(html);	
			}
		}

	});

	
}

function excluirAnexo(anexo_id, obj){

	if($(obj).attr('tipo') == 'chamado'){
		controller = 'excluirAnexosChamado';
	}else{
		controller = 'excluirAnexosAtividade';
	}

	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/'+controller,
		async: true,
		data: {
			id: anexo_id
		},
		success: function( data ) {
			var dados = $.parseJSON(data);
			
			if(dados.retorno == 'sucesso'){

				swal({
					title: 	"Sucesso",
					text: 	"Anexo Excluído com sucesso",
					type: 	"success"
				});

				$('#'+anexo_id).remove();

			}else{

				swal({
					title: 	"Atenção!",
					text: 	"Não foi possível excluir o anexo",
					type: 	"warning"
				});
			}
		}

	});


}

// Funções cadastro funcionário

function esconde_adiciona(id,obj){
	
	$(id).toggle('slow');

	if( $(obj).hasClass('la-plus-circle') ){
		$(obj).removeClass('la-plus-circle').addClass('la-minus-circle');
	}else{
		$(obj).removeClass('la-minus-circle').addClass('la-plus-circle');
	}
}

function adicionar_funcionario(){
	if($('#parceiro_id').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Selecione um parceiro",
			type: "warning"
		});
		return;
	}
	if($('#nome').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o nome do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	if($('#status_id').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Selecione um status para o chamado!",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	if($('#cpf').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o CPF do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	if($('#email').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o email do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	if($('#telefone').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o telefone do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	$.ajax({
		method: "POST",
		url: base_url+'usuarios/cadastrar',
		async: true,
		data: {
			nome: $('#nome').val(),
			cpf: $('#cpf').val(),
			email: $('#email').val(),
			telefone: $('#telefone').val(),
			empresa_id: $('#parceiro_id').val()
		},
		success: function( data ) {
			var dados = $.parseJSON(data);
			swal({
				title: dados.titulo,
				text: dados.texto,
				type: dados.tipo
			}).then(function() {
				if(dados.tipo == 'success') {
					$.ajax({
						method: "POST",
						url: base_url+"AreaAdministrador/retornaUsuariosEmpresaWS",
						async: true,
						data: { empresa_id 	: 	$('#parceiro_id').val() },
						beforeSend: function(){
							$('#contato_id').empty();
							$('#edit_contato_id').empty();
						},
						success: function( data ) {
							var tecnicos = $.parseJSON(data);
							if(tecnicos.length > 0){
								var html='';
								for (var i = 0; i < tecnicos.length; i++) {
									var select = '';
									if(tecnicos[i].id == dados.id){
										select = 'selected';
										$("#edit_cartao_validade").val(tecnicos[i].validade);
									}
									html+= '<option validade="'+tecnicos[i].validade+'" '+select+' value="'+tecnicos[i].id+'">'+tecnicos[i].label+'</option>';
								}
							} else {
								var html='<option value="">Nenhum contato cadastrado</option>';
							}
							$('#contato_id').append(html);
							$('#edit_contato_id').append(html);
						}
					});
				}
			});
		}
	});
}

// Funções cadastro subatividade

function valida_form_subatividade(){
	if($('#subatividade_parceiro_id').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Selecione um parceiro",
			type: "warning"
		});
		return false;
	}
	if($('#contato_id').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Selecione um contato",
			type: "warning"
		});
		return false;
	}
	if($('#subatividade_observacao').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Informe a observação do atendimento",
			type: "warning"
		});
		return false;
	}
}

// Funções edição subatividade

function valida_form_edit_subatividade(){
	if($('#edit_parceiro_id').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Selecione um parceiro",
			type: "warning"
		});
		return false;
	}
	if($('#edit_contato_id').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Selecione um contato",
			type: "warning"
		});
		return false;
	}
	if($('#edit_subatividade_observacao').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Informe a observação do atendimento",
			type: "warning"
		});
		return false;
	}
}

function editar_subatividade(subatividade_id,atividade_id){
	
	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/editaSubAtividadeWS',
		async: true,
		data: {
			subatividade_id: subatividade_id
		},
		success: function( data ) {
			var dados = $.parseJSON(data);
			$("#subatividade_id").val(dados.id);
			$("#parceiro_id").val(dados.parceiro_id);
			$("#edit_parceiro_id").val(dados.parceiro_id);
			$("#edit_tecnico").val(dados.tecnico);
			$("#edit_endereco_tecnico").val(dados.endereco);
			$("#edit_cidade_tecnico").val(dados.cidade);
			$("#edit_subatividade_observacao").val(dados.observacao);

			$.ajax({
				method: "POST",
				url: base_url+"AreaAdministrador/retornaUsuariosEmpresaWS",
				async: true,
				data: { empresa_id 	: 	dados.parceiro_id },
				beforeSend: function(){
					$('#edit_contato_id').empty();
				},
				success: function( data ) {
					var tecnicos = $.parseJSON(data);
					if(tecnicos.length > 0){
						var html='';
						for (var i = 0; i < tecnicos.length; i++) {
							var select = '';
							if(tecnicos[i].id == dados.contato_id){
								select = 'selected';
								$("#edit_cartao_validade").val(tecnicos[i].validade);
							}
							html+= '<option validade="'+tecnicos[i].validade+'" '+select+' value="'+tecnicos[i].id+'">'+tecnicos[i].label+'</option>';
						}
					} else {
						var html='<option value="">Nenhum contato cadastrado</option>';
					}
					$('#edit_contato_id').append(html);
				}
			});
			
			$('#atendimento_atividade_id').val(atividade_id);
			$('#modal_edita_subatividade').modal('show');
			
		}
	});
}

function redireciona_configuracao(cliente_id){
	window.open(base_url+'AreaAdministrador/configuracaoEmpresa/'+cliente_id, '_blank').focus();
}

function excluirLinhaDespesa()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');                
        $('tr[indice="'+indice+'"]').remove(); 
    });    
}

function visualizarPecas(pecas_id)
{
	$('#modal_listar_pecas').modal('show');

	$.ajax({
		method: "POST",
		url: base_url+'areaAdministrador/listaPecasSolicitacao',
		async: true,
		data: {
			pecas_id: pecas_id
		},
		success: function( data ) {

			var dados = $.parseJSON(data);

			$('#listaPecasSolicitacao').empty();
			$('.id_solicitacao_pecas').text(dados[0].id);
			$('#solicitacaoPecasDest').text(dados[0].destinatario);
			$('#solicitacaoPecasEnd').text(dados[0].endereco_entrega);
			$('#solicitacaoPecasRastreio').text(dados[0].rastreio);
			$('#txt_modo_envio').text(dados[0].modo_envio);
			$('#solicitacaoPecasNfe').empty();
			$('#solicitacaoPecasNfe').append('<a href="'+base_url+'nfe-assistencia/'+dados[0].arquivo_nfe+'">'+dados[0].nr_nf+'</a>');

			for( var i=0;i<dados.length;i++ ){
				var html = 	'<tr>';
					html+=	'	<td>'+dados[i].pecas+'</td>';
					html+=	'	<td style="text-align: center;">'+dados[i].qtd+'</td>';
					html+=	'</tr>';				
				
				$('#listaPecasSolicitacao').append(html);
			}
		
		}

	});
}

function andamento(solicitacao_id,status_id){

	$('.modal-andamento-title').text('Andamento solicitação #'+ solicitacao_id);
	$('#adicionar_andamento').attr('solicitacao_id', solicitacao_id);
	$('#adicionar_andamento').attr('status_id', status_id);

	$.ajax({
		method: "POST",
		url:  base_url+"AreaAssistencia/buscaAndamentos", 
		async: true,
		data: { solicitacao_id 	: 	solicitacao_id}
	}).done(function(data) {

		var dados = $.parseJSON(data);	
		$('.m-list-timeline__item').remove();				
		var html="";

		if( dados.length > 0 ){

			for( var i=0;i<dados.length;i++ ){
				var tipo = '';
				
				if( dados[i]['status_id'] == 1)
				{
					tipo = 'secondary';
				}else if( dados[i]['status_id'] == 2 ){
					tipo = 'info';
				}else if( dados[i]['status_id'] == 3 ){
					tipo = 'primary';

				}else if( dados[i]['status_id'] == 4 ){
					tipo = 'success';

				}else if( dados[i]['status_id'] == 5 ){
					tipo = 'danger';

				}else if( dados[i]['status_id'] == 6 ){
					tipo = 'warning';

				}else if( dados[i]['status_id'] == 7 ){
					tipo = 'info';

				}else if( dados[i]['status_id'] == 8 ){
					tipo = 'success';

				}
				
				var data_hora = dados[i]['dthr_andamento'].split(' ');
				var data = data_hora[0].split('-');
				var hora = data_hora[1];
				var data_andamento = data[2]+'/'+data[1]+'/'+data[0];
				html+= '<div class="m-list-timeline__item">';
                html+='<span class="m-list-timeline__badge m-list-timeline__badge--'+tipo+'"></span>';
                html+=' <span class="m-list-timeline__text">'+dados[i]['andamento']+'</span>';
                html+=' <span class="m-list-timeline__time">'+dados[i]['usuario']+'  '+data_andamento+' '+hora+'</span>';
                html+='</div>';
            }

            $('.m-list-timeline__items').append(html);
		}else{
			$('.m-list-timeline__item').remove();
		}
	});

	$("#modal-andamento").modal({
	    show: true
	});
    
}
;(function($){ 
	$.fn.datetimepicker.dates['pt-BR'] = {
        format: 'dd/mm/yyyy',
		days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo"],
		daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
		daysMin: ["Do", "Se", "Te", "Qu", "Qu", "Se", "Sa", "Do"],
		months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
		monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
		today: "Hoje",
		suffix: [],
		meridiem: []
	};
}(jQuery));