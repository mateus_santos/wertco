$(document).ready(function(){
	$('.custo').mask('#.##0,00', {reverse: true});	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('.custo').bind('focusout', function(){
		var custo 					= $(this).val();
		var solicitacao_pecas_id 	= $(this).attr('solicitacao_pecas_id');
		var solicitacao_id 			= $(this).attr('solicitacao_id');
		var status_solicitacao_id	= $(this).attr('status_solicitacao_id');
		var descricao				= $(this).attr('descricao');	
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAssistencia/alterarCustoSolicitacao", 
			async: true,
			data: { custo 					: 	custo,
					solicitacao_pecas_id 	: 	solicitacao_pecas_id,
					solicitacao_id 			: 	solicitacao_id,
					status_solicitacao_id	: 	status_solicitacao_id,
					descricao				: 	descricao			}
					
		}).done(function(data) {
			var dados = $.parseJSON(data);

			if(dados.retorno == 'sucesso')
			{
				swal({
					title: "OK!",
					text: 'Custo da peça alterado com sucesso!',
					type: "success"
				}).then(function() {
					
				});
			}
		});

	});
}); 