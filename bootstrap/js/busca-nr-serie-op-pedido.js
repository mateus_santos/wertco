$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');
	

    $('#enviar').bind('click', function(){
    	
    	$('#rel_chamados').slideDown('slow');
    	
	    $.ajax({
			method: "POST",
			url: base_url+"AreaAdministrador/buscaNrSerieOpPedidoAjax",
			async: true,
			data: { nr_serie 	: 	$('#nr_serie').val(),
					nr_op 		: 	$('#nr_op').val(),
					nr_pedido 	: 	$('#nr_pedido').val()	}
		}).done(function( data ) {
			var dados = $.parseJSON(data);			
			$('#relatorio').empty();
			$('#relatorio').append(dados.retorno);
			datatable();
				
		});
	});

   
});

function abrirModalPedido(pedido_id){

	$.ajax({
		method: "POST",
		url: base_url+"AreaAdministrador/buscaPedidosAndamento",
		async: true,
		data: { pedido_id 	: 	pedido_id	}
	}).done(function( data ) {
		var dados = $.parseJSON(data);	
		$('.modal-pedido-title').empty();		
		$('.pedido-body').empty();
		titulo = 'Pedido #'+pedido_id;
		titulo+= ' <i class="fa fa-print" style="color: #ffcc00;/* float: right; */position: absolute;right: 10%;font-size: 24px;" onclick="geraPedidosPdf('+pedido_id+')"></i>'; 
		$('.modal-pedido-title').append(titulo);		
		$('.pedido-body').append(dados.retorno);
	});
	
	$('#modalPedido').modal('show');
	
}

function abrirModalOp(op_id, nr_serie){

	$.ajax({
		method: "POST",
		url: base_url+"AreaAdministrador/buscaOrdemProducao",
		async: true,
		data: { op_id 		: 	op_id,
				nr_serie 	: 	nr_serie}
	}).done(function( data ) {
		var dados = $.parseJSON(data);	
		$('.modal-op-title').empty();		
		$('.op-body').empty();
		titulo = 'Ordem de Produção #'+op_id;
		
		$('.modal-op-title').append(titulo);		
		$('.op-body').append(dados.retorno);
	});
	
	$('#modalOp').modal('show');
	
}

function geraPedidosPdf(pedido_id){

	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/geraPedidosPdf/"+pedido_id+"/ajax/", 
		async: true,
		data: { pedido_id : pedido_id}
	}).done(function(data) {
		var dados = $.parseJSON(data);		
		
		if(dados.retorno != 'erro')
		{
			window.open(base_url+'pedidos/'+dados.retorno);

		}else{
			swal({
	   			title: "Atenção!",
	   			text: "Erro ao gerar pdf do pedido, entre em contato com o webmaster",
	   			type: 'warning'
	    	}).then(function() {
	    	   	
	    	});
		}

	});	
	
}

function datatable()
{

	$('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');
}