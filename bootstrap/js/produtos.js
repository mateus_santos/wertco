$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

	$('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});   

	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.no-footer').attr('style','margin-left: 0px;width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	$('#valor_tributo').mask('#####.00', );

	$('#simular_valor').bind('click', function(){
		atualiza_tabela($('#valor_tributo').val());
	});

	$(document).on('keypress',function(e) {
	    if(e.which == 13) {
	       atualiza_tabela($('#valor_tributo').val());
	    }
	});

});

function simulador(produto_id, modelo, valor){
	id = produto_id;	
	$('.titulo_simulador').empty();
	$('.titulo_simulador').append('Simular Preço para <span style="color: #ffcc00;">'+ modelo+'</span>');
	var valor_base = valor;
	$('#valor_tributo').val(valor_base);
	$('#valor_base_set').text(valor_base);	
	atualiza_tabela(valor_base);	
	$("#modal_simulador").modal({
    	show: true
	});
}

function atualiza_tabela(valor_base){
	$('.tabela_precos').unmask();
	// ICMS 7
	$('#tabela_7_0').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#icms_7').val() )) * 1.11 ) * 1.12).toFixed(2));
	$('#tabela_7_2').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#icms_7').val() )) * 1.11 )* 1.12).toFixed(2));
	$('#tabela_7_3').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#icms_7').val() )) * 1.11 )* 1.12).toFixed(2));
	$('#tabela_7_4').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.637 ) * parseFloat($('#icms_7').val() )) * 1.11 )* 1.12).toFixed(2));
	$('#tabela_7_5').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.583 ) * parseFloat($('#icms_7').val() )) * 1.11 )* 1.12).toFixed(2));
	// ICMS 12
	$('#tabela_12_0').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#icms_12').val() )) * 1.09 )* 1.12).toFixed(2));
	$('#tabela_12_2').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#icms_12').val() )) * 1.09 )* 1.12).toFixed(2));
	$('#tabela_12_3').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#icms_12').val() )) * 1.09 )* 1.12).toFixed(2));
	$('#tabela_12_4').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.637 ) * parseFloat($('#icms_12').val() )) * 1.09 )* 1.12).toFixed(2));
	$('#tabela_12_5').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.583 ) * parseFloat($('#icms_12').val() )) * 1.09 )* 1.12).toFixed(2));
	// ICMS 18
	$('#tabela_18_0').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#icms_18').val() )) * 1.07 )* 1.12).toFixed(2));
	$('#tabela_18_2').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#icms_18').val() )) * 1.07 )* 1.12).toFixed(2));
	$('#tabela_18_3').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#icms_18').val() )) * 1.07 )* 1.12).toFixed(2));
	$('#tabela_18_4').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.637 ) * parseFloat($('#icms_18').val() )) * 1.07 )* 1.12).toFixed(2));
	$('#tabela_18_5').text(Math.round(((parseFloat(parseFloat(valor_base) / 0.583 ) * parseFloat($('#icms_18').val() )) * 1.07 )* 1.12).toFixed(2));
	$('.tabela_precos').mask('#.##0,00', {reverse: true});
}

function excluirProduto(id){

		var id = id;
		swal({
		  title: 'Excluir?',
		  text: "Deseja realmente excluir esse produto!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Sim!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					method: "POST",
					url:  base_url+"AreaAdministrador/excluirProduto", 
					async: false,
					data: { id 	: id }
				}).done(function(data) {
					var dados = $.parseJSON(data);		
					
					if(dados.retorno == 'sucesso'){				
						swal({
				   			title: "OK!",
				   			text: "Produto Excluído com sucesso",
				   			type: 'success'
				    	}).then(function() {
				    	   	window.location = base_url+'AreaAdministrador/produtos';
				    	}); 
					}
				});	
			}
		})
	
}