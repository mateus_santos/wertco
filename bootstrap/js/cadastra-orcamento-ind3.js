$(document).ready(function(){	
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('.esconde').hide();

    $('.comissao').bind('click', function(){
        $('.salvar').removeAttr('disabled');
    });
    $( "#nome_indicador" ).autocomplete({
        source: base_url+"AreaIndicadores3/retornaIndicadores",
        minLength: 2,
        select: function( event, ui ) {

            $('#indicador_id').val(ui.item.id);
            var indicador   =   ui.item.id;
            
        }
    });

    $('.indicacao').bind('click', function(){
        if($(this).val() == 1){
            $('.nome_indicador').fadeIn('slow');
            $('#nome_indicador').attr('required','required');

        }else{

            $('.nome_indicador').fadeOut('slow');
            $('#nome_indicador').removeAttr('required');
            $('#nome_indicador').val('');           
        }
    });

    $('.add').bind('click', function(){

        var html = $('#modelo').clone();                
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
        $(html).find('.bombas').attr('indice', indice);
        $(html).find('.valor_unitario').attr('indice', indice);
        $(html).find('.valor_unitario').text('');
        $(html).find('b.valor_unitario').addClass('valor_unitario_'+indice);
        $(html).find('b.valor_unitario').attr('valor_base','');
        $(html).find('b.valor_unitario').removeClass('valor_unitario_0');
        $(html).find('input.valor_unitario').attr('id', 'valor_unitario_'+indice);

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();                
        body+="</tr>";                
        $('.table').append(body);                
        $('#modelo').attr('total_indice',indice);
        excluir();
        mostra_valor();        
    });
   
   
    $('#cnpj').mask('00.000.000/0000-00');
    $('#cpf').mask('000.000.000-00');
    $('#bombas').mask('00000'); 
    $('#bicos').mask('00000');  
    $('.qtd').mask('000');
    $('#cep').mask('00000-000');  
    
    mostra_valor(); 
    
    $('#cnpj').bind('focusout', function(){
        var cnpj = $(this).val(); 
        if(cnpj != ""){
            if(isCNPJValid(cnpj) ){
            $.ajax({
                method: "POST",
                url: base_url+'clientes/verificaOrcamento',
                async: true,
                data: { cnpj    :   cnpj }
                }).done(function( data ) {
                    var dados = $.parseJSON(data);
                    if( dados.total == 0){

                        $.ajax({
                            method: "POST",
                            url: base_url+'clientes/verifica_cnpj',
                            async: false,
                            data: { cnpj    :   cnpj }
                            }).done(function( data ) {
                                var dados = $.parseJSON(data);                          
                                if(dados.length > 0){
                                    
                                    $('#razao_social').val(dados[0].razao_social);      
                                    $('#razao_social').attr('disabled', true);      
                                    $('#fantasia').val(dados[0].fantasia);      
                                    $('#fantasia').attr('disabled', true);
                                    $('#telefone').val(dados[0].telefone);
                                    $('#telefone').attr('disabled', true);
                                    $('#endereco').val(dados[0].endereco);      
                                    $('#endereco').attr('disabled', true);                                  
                                    $('#email').attr('disabled', true);
                                    $('#email').val(dados[0].email);
                                    $('#cidade').attr('disabled', true);
                                    $('#cidade').val(dados[0].cidade);  
                                    $('#bairro').attr('disabled', true);
                                    $('#bairro').val(dados[0].cidade);
                                    $('#cep').attr('disabled', true);
                                    $('#cep').val(dados[0].cidade);  
                                    $('#estado').attr('disabled', true);
                                    $('#estado').val(dados[0].estado);                        
                                    $('#pais').attr('disabled', true);
                                    $('#pais').val(dados[0].pais);                        
                                    $('#salvar').val('0');
                                    $('#empresa_id').val(dados[0].id);
                                    $('.esconde').fadeIn('slow');
                                    $('#msg_produtos').fadeOut('fast');
                                    atualiza_valores( dados[0].estado );
                                }else{

                                    var cnpj_sem_mascara = cnpj.replace(/[^\d]+/g,'');

                                    $.ajax({
                                        method: "POST",
                                        url: base_url+'AreaIndicadores3/ValidaCnpj',
                                        async: true,
                                        data: { cnpj    :   cnpj_sem_mascara }
                                    }).done(function( data ) {
                                        var dados = $.parseJSON(data);   
                                        
                                        if (dados.receita.erro == 0 && dados.receita.situacao == "ATIVA" ){
                                            $('#razao_social').attr('disabled', false);     
                                            $('#razao_social').val(dados.receita.retorno.razao_social);
                                            if(dados.receita.retorno.razao_social != ''){    
                                                $('#razao_social').attr('style','border-color: #5fda17;');
                                            }
                                            $('#fantasia').attr('disabled', false);
                                            $('#fantasia').val(dados.receita.retorno.nome_fantasia );
                                            if(dados.receita.retorno.nome_fantasia != ''){
                                                $('#fantasia').attr('style','border-color: #5fda17;');
                                            }
                                            $('#telefone').attr('disabled', false);
                                            $('#telefone').val(dados.receita.retorno.telefone);
                                            if(dados.receita.retorno.telefone != ''){
                                                $('#telefone').attr('style','border-color: #5fda17;');
                                            }
                                            $('#endereco').attr('disabled', false); 
                                            $('#endereco').val(dados.receita.retorno.logradouro+', '+dados.receita.retorno.numero+' '+dados.receita.retorno.complemento);                                
                                            $('#endereco').attr('style','border-color: #5fda17;');
                                            
                                            $('#email').attr('disabled', false); 
                                            $('#email').val('');                                                                                                
                                            $('#cidade').attr('disabled', false); 
                                            $('#cidade').val(dados.receita.retorno.municipio_ibge);  
                                            if(dados.receita.retorno.municipio_ibge != ''){
                                                $('#cidade').attr('style','border-color: #5fda17;');
                                            }
                                            $('#estado').attr('disabled', false); 
                                            $('#estado').val(dados.receita.retorno.uf);
                                            if(dados.receita.retorno.uf != ''){
                                                $('#estado').attr('style','border-color: #5fda17;');
                                                $('#msg_produtos').fadeOut('fast');
                                                $('.esconde').fadeIn('slow');
                                                atualiza_valores( $('#estado').val()     );
                                                
                                            }
                                            $('#bairro').attr('disabled', false); 
                                            $('#bairro').val(dados.receita.retorno.bairro);
                                            if(dados.receita.retorno.bairro != ''){
                                                $('#bairro').attr('style','border-color: #5fda17;');
                                            }    
                                            $('#cep').attr('disabled', false); 
                                            $('#cep').val(dados.receita.retorno.cep);                          
                                            if(dados.receita.retorno.bairro != ''){
                                                $('#cep').attr('style','border-color: #5fda17;');
                                            }
                                            $('#pais').attr('disabled', false);
                                            $('#pais').attr('style','border-color: #5fda17;');                                                
                                            $('#inscricao_estadual').attr('disabled', false);
                                            $('#inscricao_estadual').val('');                        
                                            $('#cartao_cnpj').val(dados.receita.save);                    
                                            
                                            $('#pais').val('Brasil');         
                                            $('#salvar').val('1');


                                        }else{
                                           swal({
                                                title: "Atenção!",
                                                text: dados.receita.msg,
                                                type: 'warning'
                                            }).then(function() {
                                                $('#cnpj').val('');
                                            });

                                        }
                                    });    

                                }                           
                        });  
                    }else{

                        swal({
                            title: "Atenção!",
                            text: "Já Existe um orçamento em andamento para este cliente, entre em contato com a WERTCO!",
                            type: 'warning'
                        }).then(function() {
                            $('#cnpj').val('');
                        }); 
                   }
                });

            }else{
                swal({
                    title: "Atenção!",
                    text: "CNPJ Inválido!",
                    type: 'warning'
                }).then(function() {
                    $('#cnpj').val('');

                }); 
            }
        
        }
    });
    
    $('#estado').bind('change', function(){
        $('#msg_produtos').fadeOut('fast');
        $('.esconde').fadeIn('slow');
        atualiza_valores( $(this).val() );

    });

    $('.comissao').bind('click', function(){
        var comissao = $(this).val();
        var comissaoObj = $(this);
        if( $('#estado').val() == '')
        {
            swal({
                title: "Atenção!",
                text: "Selecione o estado do cliente para calcular o(s) valor(es) do(s) produto(s)!",
                type: 'warning'
            }).then(function() {
                $(this).attr('checked', false);
                $('.salvar').attr('disabled', true);
            }); 
        }else{
            var estado = $('#estado').val();
            $.ajax({
                method: "POST",
                url: base_url+'AreaIndicadores3/verificaIcms',
                async: true,
                data: { estado    :   estado }
                }).done(function( data ) {
                    var dados = $.parseJSON(data);
                    $('#fator').val(dados.fator);
                    var indice = 0;
                    $('b.valor_unitario').each(function(){
                        
                        if($(this).attr('valor_base') != '' && $(this).attr('valor_base') != undefined && $(this).attr('tipo') != 'opcionais')  {
                            if( comissao == '3' ){

                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(dados.fator)).toFixed(2);
                            }else if( comissao == '4' ){

                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(dados.fator)).toFixed(2);
                            }else{

                                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(dados.fator)).toFixed(2);
                            }        
                            $(this).text(valor_produto);
                            $(this).unmask();
                            $(this).mask('##.##0,00');
                            $('#valor_unitario_'+indice).val(valor_produto);
                        }
                       
                        
                        if($(this).attr('valor_base') != '' && $(this).attr('valor_base') != undefined && $(this).attr('tipo') == 'opcionais'){
                            $(this).text($(this).attr('valor_base'));
                            $(this).unmask();                            
                            $('#valor_unitario_'+indice).val($(this).attr('valor_base'));   

                        }   
                        indice++; 
                    });
                });

        }

    });

});	
	
function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');                
        $('tr[indice="'+indice+'"]').remove(); 
    });    
}

function mostra_valor()
{
    $('.bombas').bind('change', function(){
        indice = $(this).attr('indice');          
        $('.valor_unitario_'+indice).attr('valor_base',$(this).find('option:selected').attr('valor_unitario'));  
        $('.valor_unitario_'+indice).attr('tipo',$(this).find('option:selected').attr('tipo'));
        $('#valor_unitario_'+indice).val($(this).find('option:selected').attr('valor_unitario'));          
        if( $('.comissao').val() != '' && $('.comissao').val() != undefined && $('#fator').val() != ''){
            
            atualiza_valores($('#estado').val());            
        }

        $('.salvar').attr('disabled', false);
    }); 

}

function atualiza_valores(estado){

    var comissao = $("input[name='valor_desconto_orc']:checked").val();
        
    $.ajax({
        method: "POST",
        url: base_url+'AreaIndicadores3/verificaIcms',
        async: false,
        data: { estado    : estado   }
    }).done(function( data ) {
        var retorno = $.parseJSON(data);
        $('#fator').val(retorno.fator);
        

        $('b.valor_unitario').each(function(){ 
            
            if($(this).attr('valor_base') != '' && $(this).attr('valor_base') != undefined && $(this).attr('tipo') != 'opcionais')  {                
                var indice = $(this).attr('indice');                
                if( comissao == '3' ){

                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                }else if( comissao == '4' ){

                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(retorno.fator)).toFixed(2);
                }else{

                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(retorno.fator)).toFixed(2);
                }        

                $(this).text(valor_produto);
                $(this).unmask();
                $(this).mask('##.##0,00');
                $('#valor_unitario_'+indice).val(valor_produto);
            }

            if($(this).attr('valor_base') != '' && $(this).attr('valor_base') != undefined && $(this).attr('tipo') == 'opcionais'){
                
                $(this).text($(this).attr('valor_base'));
                $(this).unmask();
                //$(this).mask('000.000.000,00');                
                $('#valor_unitario_'+indice).val($(this).attr('valor_base'));   

            }
            
            
        });
    });
}

function isCNPJValid(cnpj) {  
            
    var b = [6,5,4,3,2,9,8,7,6,5,4,3,2], c = cnpj;
    if((c = c.replace(/[^\d]/g,"").split("")).length != 14)
        return false;
    for (var i = 0, n = 0; i < 12; n += c[i] * b[++i]); 
    if(c[12] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    for (var i = 0, n = 0; i <= 12; n += c[i] * b[i++]); 
    if(c[13] != (((n %= 11) < 2) ? 0 : 11 - n))
        return false; 
    return true; 
}