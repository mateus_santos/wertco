$(document).ready(function(){
	$('#cpf_cliente').mask('000.000.000-00');
	$('.porcentagem').mask('000');
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);	

	if( $('#status_id').val()=='17' || $('#status_id').val()=='16' ||  $('#status_id').val()=='2' ){
		$('.excluir').hide();
		$('.add').hide();
		$('.descricao').hide();
		$('.porcentagem').hide();
	}

	$('.add').bind('click', function(){

        var html = $('#modelo').clone();                
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
       
        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();                
        body+="</tr>";                
        $('.table').append(body);
        $('#modelo').attr('total_indice',indice);
		excluir();
		$('.porcentagem').unmask();
		$('.porcentagem').mask('000');            
    });

	$('.tecnico').bind('click', function(){
		
		if($(this).val() == 1){
			$('#tecnico_startup').fadeIn('slow');
			$('#tecnico_startup').attr('required',true);
			$('#tecnico_id').attr('required',true);
		}else{
			$('#tecnico_startup').fadeOut('slow');
			$('#tecnico_startup').attr('required',false);
			$('#tecnico_id').attr('required',false);
			
		}

	});

	$('#cpf_cliente').bind('focusout', function(){
				var cpf = $(this).val();
				if(cpf != ""){
					$.ajax({
						method: "POST",
						url: base_url+"clientes/verifica_cpf_apenas",
						async: true,
						data: { cpf 	: 	cpf }
					}).done(function( data ) {
						var dados = $.parseJSON(data);
							if(dados.status == 'erro'){
								swal(
							  		'Ops!',
							  		'Cpf inválido',
							  		'error'
								)
								$('#cpf_cliente').val('');	
							}
						
					});
				}
			});

	$('#pintura').bind('change', function(){
		
		swal({
   			title: "Atenção!",
   			text: 'Atualize os produtos/combustíveis de acordo com essa bandeira ('+$(this).val()+')',
   			type: 'warning'
    	}).then(function() {
    	   	$('html,body').animate({
            scrollTop: $('.produtos').offset().top},
            'slow');

    	   	$('.old').val('');
    	});		

		if($(this).val() == 'bandeira_branca' || $(this).val() == 'BANDEIRA_BRANCA' )
		{
			$('#arquivo_bb').fadeIn('slow');
			$('.arquivo').fadeIn('slow');
			$('#pintura_descricao').fadeIn('slow');			
			$('#m_pedido_pintura').modal({
	    		show: true
			});
			$('#paineis').fadeIn('slow');
			$('.div_pintura').find('h5').fadeIn('slow');
		}else{
			$('#arquivo_bb').fadeOut('slow');
			$('.arquivo').fadeOut('slow');
			$('#pintura_descricao').fadeOut('slow');
			$('#paineis').fadeOut('slow');
			$('.div_pintura').find('h5').fadeOut('slow');
		}
		
	});

	$(".fl_identidade_visual").bind('click', function(){
		if($(this).val() == 'S'){
			$('.arquivo_bb').fadeIn('slow');
			$('.arquivo_bb').attr('required', true);
		}else {
			$('.arquivo_bb').attr('required', false);
			$('.arquivo_bb').fadeOut('slow');
		}
	});

	$('.editar').bind('click', function(){
		var selected = ($(this).attr('arla') == 1) ? 'selected=selected' : '';  
		console.log()
		
		var combo = "<option value='QUEROSENE'>QUEROSENE</option><option value='ARLA' "+selected+">ARLA</option>";
		if($('#pintura').val() == 'bandeira_branca' || $('#pintura').val() == 'BANDEIRA_BRANCA' || $('#pintura').val() == 'WERTCO' )
		{
			combo+='<option value="GC">Gasolina Comum</option>';
			combo+='<option value="GA">Gasolina Aditivada</option>';
			combo+='<option value="ET">Etanol Comum</option>';
			combo+='<option value="ETA">Etanol Aditivado</option>';
			combo+='<option value="DC">Diesel Comum</option>';
			combo+='<option value="DA">Diesel Aditivado</option>';
			combo+='<option value="DS10">Diesel S10</option>';
			combo+='<option value="DS500">Diesel S500</option>';
			combo+='<option value="DIESELMARITIMO">DIESEL MARÍTIMO</option>';
			$('#arquivo_bb').fadeIn('slow');
			$('#pintura_descricao').fadeIn('slow');
			if($(this).val() != 'WERTCO'){
				$('#m_pedido_pintura').modal({
		    		show: true
				});
			}

		}else if($('#pintura').val() == 'BR' || $('#pintura').val() == 'BR NOVA'  ||  $('#pintura').val()== 'PETROBRAS NOVA' ||  $('#pintura').val() == 'PETROBRAS ANTIGA'){
			$('#arquivo_bb').fadeOut('slow');
			$('#pintura_descricao').fadeOut('slow');
			
			combo+='<option value="GC">Gasolina Comum</option>';
			combo+='<option value="GAGRID">Gasolina Aditivada GRID</option>';
			combo+='<option value="GAPODIUM">Gasolina Premium PODIUM</option>';
			combo+='<option value="ET">Etanol</option>';
			combo+='<option value="ETAGRID">Etanol Aditivado GRID</option>';
			combo+='<option value="DC">Diesel Comum</option>';
			combo+='<option value="DAGRID">Diesel Aditivado GRID</option>';
			combo+='<option value="DAPODIUM">Diesel Premium PODIUM</option>';
			combo+='<option value="DS10">Diesel S10</option>';
			combo+='<option value="DS500">Diesel 500</option>';
			combo+='<option value="DS10ADITIVADO">Diesel S10 ADITIVADO</option>';
			combo+='<option value="DIESEL MARÍTIMO VERANA">Diesel Marítimo Verana</option>';
			combo+='<option value="DIESEL MARÍTIMO">Diesel Marítimo</option>';
		}else if($('#pintura').val() == 'IPIRANGA' || $('#pintura').val() == 'IPIRANGA NOVA'  ||  $('#pintura').val() == 'IPIRANGA ANTIGA'){
			$('#arquivo_bb').fadeOut('slow');
			$('#pintura_descricao').fadeOut('slow');
			
			combo+='<option value="IPIMAXPRO">IpimaxPro Gasolina Premium Aditivada</option>';
			combo+='<option value="IPIMAXGA">Ipimax Gasolina Aditivada</option>';
			combo+='<option value="GCORIGINAL">Gasolina Comum Original</option>';
			combo+='<option value="ETORIGINAL">Etanol Comum Original </option>';
			combo+='<option value="ETAIPIMAX">Etanol Aditivado Ipimax</option>';
			combo+='<option value="DS10ORIGINAL">Diesel S10 Original</option>';
			combo+='<option value="DSMARINAIPIMAX">Diesel Marina IPIMAX</option>';
			combo+='<option value="DAS10IPIMAX">Diesel S10 Aditivado Ipimax</option>';
			combo+='<option value="DS500ORIGINAL">Diesel S500 Original</option>';	

		}else if($('#pintura').val() == 'SHELL'){
			$('#arquivo_bb').fadeOut('slow');
			$('#pintura_descricao').fadeOut('slow');
			
			combo+='<option value="GC">Gasolina Comum</option>';
			combo+='<option value="GAVPOWER">Gasolina Aditivada VPOWER</option>';
			combo+='<option value="GARACINGVPOWER">Gasolina Aditivada RACING VPOWER</option>';
			combo+='<option value="ET">Etanol Comum</option>';
			combo+='<option value="ETAVPOWER">Etanol Aditivado VPOWER</option>';
			combo+='<option value="DC">Diesel Comum</option>';
			combo+='<option value="DS10">Diesel S10</option>';
			combo+='<option value="DS500">Diesel S500</option>';
			combo+='<option value="DS10EVOLUX">Diesel S10 EVOLUX</option>';
			combo+='<option value="DS10EVOLUX">Diesel  S500 EVOLUX</option>';
		}else if($('#pintura').val() == 'ALE'){
			$('#arquivo_bb').fadeOut('slow');
			$('#pintura_descricao').fadeOut('slow');
			
			combo+='<option value="GC">Gasolina Comum</option>'
			combo+='<option value="GAALEPREMIUM">Gasolina  Aditivada ALEPLUS</option>';
			combo+='<option value="GAALEPREMIUM">Gasolina  Aditivada ALEPREMIUM</option>';
			combo+='<option value="ET">Etanol Comum</option>';
			combo+='<option value="DC">Diesel Comum</option>';
			combo+='<option value="DAPLUS">Diesel Aditivado DIESELPLUS</option>';
			combo+='<option value="DS10E">Diesel S10 </option>';
		}else if($('#pintura').val() == 'TOTAL'){
			$('#arquivo_bb').fadeOut('slow');
			$('#pintura_descricao').fadeOut('slow');
			
			combo+='<option value="GC">Gasolina Comum</option>'
			combo+='<option value="GA">Gasolina  Aditivada </option>';			
			combo+='<option value="ET">Etanol Comum</option>';
			combo+='<option value="ETA">Etanol Aditivado</option>';
			combo+='<option value="DC">Diesel Comum</option>';
			combo+='<option value="DA">Diesel Aditivado</option>';
			combo+='<option value="DS10">Diesel Comum S10</option>';
			combo+='<option value="DS10ADITIVADO">Diesel S10 Aditivado</option>';
			combo+='<option value="DSEXCELLIUM">Diesel Excellium</option>';
			combo+='<option value="S500">S500</option>';
		}else if($('#pintura').val() == 'PERU' || $('#pintura').val() == 'RAYGAS'){
			$('#arquivo_bb').fadeOut('slow');
			$('#pintura_descricao').fadeOut('slow');
			
			combo+='<option value="G-95 GASOHOL 95 PLUS"> G-95 gasohol 95 plus</option>'
			combo+='<option value="G-90 GASOHOL PLUS">G-90 gasohol plus </option>';			
			combo+='<option value="G-95 GASOHOL 85 PLUS">G-95 gasohol 85 plus</option>';
			combo+='<option value="D-B5 DIESEL 85 S-50">D-B5 diesel 85 S-50</option>';	
		}else{
			$('#arquivo_bb').fadeOut('slow');
			$('#pintura_descricao').fadeOut('slow');
			
			combo+='<option value="GC">Gasolina Comum</option>';
			combo+='<option value="GA">Gasolina Aditivada</option>';
			combo+='<option value="ET">Etanol Comum</option>';
			combo+='<option value="ETA">Etanol Aditivado</option>';
			combo+='<option value="DC">Diesel Comum</option>';
			combo+='<option value="DA">Diesel Aditivado</option>';
			combo+='<option value="DS10">Diesel S10</option>';
			combo+='<option value="DS500">Diesel S500</option>';
		}


		nr_produtos = $(this).attr('nr_produtos');
		html = "";
		indice = $(this).attr('indice');
		for(var i=0;i<nr_produtos;i++){
			
			html += '			<select class="form-control m-input '+$(this).attr('combustivel')+indice+' new" indice="'+indice+'"  placeholder="Produtos" >';
			html += '				<option value="A DEFINIR"> A definir </option>';
			html += combo;
			html += ' 			</select>';
		}							
		$('.old_'+indice).remove();
		
		html+='<input type="hidden" indice="'+indice+'" class="form-control m-input new_'+indice+'" name="produtos[]" placeholder="Produtos" style="background: #fff;" >';
		$(this).after(html);
		$(this).hide();

	});

	$( "#orcamento" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaOrcamentos",
		minLength: 3,
		select: function( event, ui ) {
			$('#orcamento_id').val(ui.item.id);
			var orcamento_id = ui.item.id; 
			if( ui.item.estado == 'EX' ){
				$('.proforma').fadeIn('slow');
			}else{
				$('.proforma').fadeOut('slow');
				$('#proforma').val('');
			}
			var combo = "<option value='QUEROSENE'>QUEROSENE</option><option value='ARLA'>ARLA</option>";
			if($('#pintura').val() == 'bandeira_branca')
			{
				$('#arquivo_bb').fadeIn('slow');
				$('#pintura_descricao').fadeIn('slow');
				combo+='<option value="GC">Gasolina Comum</option>';
				combo+='<option value="GA">Gasolina Aditivada</option>';
				combo+='<option value="ET">Etanol Comum</option>';
				combo+='<option value="ETA">Etanol Aditivado</option>';
				combo+='<option value="DC">Diesel Comum</option>';
				combo+='<option value="DA">Diesel Aditivado</option>';
				combo+='<option value="DS10">Diesel S10</option>';
				combo+='<option value="DS500">Diesel S500</option>';
				combo+='<option value="DIESELMARITIMO">DIESEL MARÍTIMO</option>';

			}else if($('#pintura').val() == 'BR' || $('#pintura').val() == 'BR NOVA' ||  $('#pintura').val() == 'PETROBRAS NOVA' ||  $('#pintura').val() == 'PETROBRAS ANTIGA'){
				$('#arquivo_bb').fadeOut('slow');
				$('#pintura_descricao').fadeOut('slow');
				combo+='<option value="GC">Gasolina Comum</option>';
				combo+='<option value="GAGRID">Gasolina Aditivada GRID</option>';
				combo+='<option value="GAPODIUM">Gasolina Premium PODIUM</option>';
				combo+='<option value="ET">Etanol</option>';
				combo+='<option value="ETAGRID">Etanol Aditivado GRID</option>';
				combo+='<option value="DC">Diesel Comum</option>';
				combo+='<option value="DAGRID">Diesel Aditivado GRID</option>';
				combo+='<option value="DAPODIUM">Diesel Premium PODIUM</option>';
				combo+='<option value="DS10">Diesel S10</option>';
				combo+='<option value="DS500">Diesel 500</option>';
				combo+='<option value="DS10ADITIVADO">Diesel S10 ADITIVADO</option>';
				combo+='<option value="DIESEL MARÍTIMO VERANA">Diesel Marítimo Verana</option>';
				combo+='<option value="DIESEL MARÍTIMO">Diesel Marítimo</option>';
			}else if($('#pintura').val() == 'IPIRANGA' || $('#pintura').val() == 'IPIRANGA NOVA'  ||  $('#pintura').val() == 'IPIRANGA ANTIGA'){
				$('#arquivo_bb').fadeOut('slow');
				$('#pintura_descricao').fadeOut('slow');
				combo+='<option value="IPIMAXPRO">IpimaxPro Gasolina Premium Aditivada</option>';
				combo+='<option value="IPIMAXGA">Ipimax Gasolina Aditivada</option>';
				combo+='<option value="GCORIGINAL">Gasolina Comum Original</option>';
				combo+='<option value="ETORIGINAL">Etanol Comum Original </option>';
				combo+='<option value="ETAIPIMAX">Etanol Aditivado Ipimax</option>';
				combo+='<option value="DS10ORIGINAL">Diesel S10 Original</option>';
				combo+='<option value="DSMARINAIPIMAX">Diesel Marina IPIMAX</option>';
				combo+='<option value="DAS10IPIMAX">Diesel S10 Aditivado Ipimax</option>';
				combo+='<option value="DS500ORIGINAL">Diesel S500 Original</option>';	

			}else if($('#pintura').val() == 'SHELL'){
				$('#arquivo_bb').fadeOut('slow');
				$('#pintura_descricao').fadeOut('slow');
				combo+='<option value="GC">Gasolina Comum</option>';
				combo+='<option value="GAVPOWER">Gasolina Aditivada VPOWER</option>';
				combo+='<option value="GARACINGVPOWER">Gasolina Aditivada RACING VPOWER</option>';
				combo+='<option value="ET">Etanol Comum</option>';
				combo+='<option value="ETAVPOWER">Etanol Aditivado VPOWER</option>';
				combo+='<option value="DC">Diesel Comum</option>';
				combo+='<option value="DS10">Diesel S10</option>';
				combo+='<option value="DS500">Diesel S500</option>';
				combo+='<option value="DS10EVOLUX">Diesel S10 EVOLUX</option>';
				combo+='<option value="DS10EVOLUX">Diesel  S500 EVOLUX</option>';
			}else if($('#pintura').val() == 'ALE'){
				$('#arquivo_bb').fadeOut('slow');
				$('#pintura_descricao').fadeOut('slow');
				combo+='<option value="GC">Gasolina Comum</option>'
				combo+='<option value="GAALEPREMIUM">Gasolina  Aditivada ALEPLUS</option>';
				combo+='<option value="GAALEPREMIUM">Gasolina  Aditivada ALEPREMIUM</option>';
				combo+='<option value="ET">Etanol Comum</option>';
				combo+='<option value="DC">Diesel Comum</option>';
				combo+='<option value="DAPLUS">Diesel Aditivado DIESELPLUS</option>';
				combo+='<option value="DS10E">Diesel S10 </option>';
			}else if($('#pintura').val() == 'TOTAL'){
			var combo = "";
				combo+='<option value="GC">Gasolina Comum</option>'
				combo+='<option value="GA">Gasolina  Aditivada </option>';			
				combo+='<option value="ET">Etanol Comum</option>';
				combo+='<option value="ETA">Etanol Aditivado</option>';
				combo+='<option value="DC">Diesel Comum</option>';
				combo+='<option value="DA">Diesel Aditivado</option>';
				combo+='<option value="DS10">Diesel Comum S10</option>';
				combo+='<option value="DS10ADITIVADO">Diesel S10 Aditivado</option>';
				combo+='<option value="DSEXCELLIUM">Diesel Excellium</option>';
				combo+='<option value="S500">S500</option>';
			}else if($('#pintura').val() == 'PERU' || $('#pintura').val() == 'RAYGAS'){
				var combo = "";
				combo+='<option value="G-95 GASOHOL 95 PLUS"> G-95 gasohol 95 plus</option>'
				combo+='<option value="G-90 GASOHOL PLUS">G-90 gasohol plus </option>';			
				combo+='<option value="G-95 GASOHOL 85 PLUS">G-95 gasohol 85 plus</option>';
				combo+='<option value="D-B5 DIESEL 85 S-50">D-B5 diesel 85 S-50</option>';						
			}else{
				$('#arquivo_bb').fadeOut('slow');
				$('#pintura_descricao').fadeOut('slow');
				combo+='<option value="GC">Gasolina Comum</option>';
				combo+='<option value="GA">Gasolina Aditivada</option>';
				combo+='<option value="ET">Etanol Comum</option>';
				combo+='<option value="ETA">Etanol Aditivado</option>';
				combo+='<option value="DC">Diesel Comum</option>';
				combo+='<option value="DA">Diesel Aditivado</option>';
				combo+='<option value="DS10">Diesel S10</option>';
				combo+='<option value="DS500">Diesel S500</option>';
			}

			$.ajax({
				method: "POST", 
				url:  base_url+"AreaAdministrador/retornaProdutosOrcamentos", 
				async: true,
				data: { orcamento_id 	: 	orcamento_id}
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				$('.produtos').fadeIn('slow');
				$('.novos_produtos').remove();
				/*$('#forma_pagto').val(dados[0]['forma_pagto']);
				$('#forma_pagto').attr('style','border: 3px solid #ffcc00;');
				$('#icone_alerta').empty();
				$('#icone_alerta').append('<i class="fa fa-warning" style="color: #ffcc00;"></i>');*/
				var valor_total 	= 0.00;
				var valor_total_stx = 0.00;
				var n = 0;
				for(var i=0;i<dados.length;i++)
				{

					if( dados[i]['tipo_produto_id'] != 4 ){
						tipo 	= 	'text';
						value	=	'';
						valor_stx = parseFloat(dados[i]['valor']) / 1.05;
					}else{
						tipo  = 'hidden';
						value = 'opcional';
						valor_stx = dados[i]['valor'];
					}
					valor_total = (	dados[i]['valor'] * dados[i]['qtd']	) + parseFloat(valor_total);
					valor_total_stx = ( valor_stx * dados[i]['qtd']	) + parseFloat(valor_total_stx);
					for(var j=0;j<dados[i]['qtd'];j++)
					{
						n++;
						var html = '<div class="form-group m-form__group row novos_produtos">';
							html += '	<div class="input-group m-input-group m-input-group--solid">';
							html += '			<div class="input-group-prepend">';
							html += '				<span class="input-group-text" id="basic-addon1">';
							html += dados[i]['codigo']+' - '+dados[i]['modelo']+' - '+dados[i]['descricao'];
							html += '				</span>';						
							html += '			</div>';
							if(dados[i]['nr_produtos'] > 0){	
						
								for(l=0;l<dados[i]['nr_produtos'];l++){
									
									
									html += '			<select class="form-control m-input '+dados[i]['produto_id']+n+' new"  indice="'+n+'"  placeholder="Produtos" >';
									html += '				<option value="A DEFINIR"> A definir </option>'; 
									html += combo;
									html += ' 			</select>';
								}
							}
							html+='				<input type="hidden" indice="'+n+'" class="form-control m-input new_'+n+'" name="produtos[]" placeholder="Produtos" style="background: #fff;" >';
							html += '			<input type="hidden" class="form-control m-input produto_id" name="produto_id[]" indice="'+n+'" value="'+dados[i]['produto_id']+'" placeholder="Produtos" style="background: #fff;">';
							html += '			<input type="hidden" class="form-control m-input" name="qtd['+dados[i]['produto_id']+']" value="'+dados[i]['qtd']+'" placeholder="Produtos" style="background: #fff;">';
							html += '			<input type="hidden" class="form-control m-input" name="tipo_produto[]" value="'+dados[i]['tipo_produto_id']+'" placeholder="Produtos" style="background: #fff;">';
							html += '			<input type="hidden" class="form-control m-input" name="valor[]" value="'+dados[i]['valor']+'" placeholder="valor_produto" style="background: #fff;">';
							html += '			<input type="hidden" class="form-control m-input" name="valor_stx[]" value="'+valor_stx+'" placeholder="valor_produto" style="background: #fff;">';
							html += '		</div>';
							html += '	</div>';
					
						$('#produtos_cadastrar').append(html);

					}

				}
				$('#valor_total').val(valor_total);
				$('#valor_total_stx').val(valor_total_stx);
			});
		}
    });

    $( "#tecnico_startup" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaTecnicos",
		minLength: 3,
		select: function( event, ui ) {
			$('#tecnico_id').val(ui.item.id);
			
		}
    });

    $('.excluir').bind('click', function(){
		var forma_pagto_id = $(this).attr('forma_pagto_id');
		var tr = $(this).attr('tr');	
		$.ajax({
			method: "POST", 
			url:  base_url+"AreaAdministrador/excluirFormaPagtoPedidos", 
			async: true,
			data: { id 		: 	forma_pagto_id}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			if(dados.retorno == 'sucesso'){
				swal(
			  		'Ok!',
			  		'Forma de pagamento excluída com sucesso',
			  		'success'
				);
				$('#'+tr).remove();
				$('#edicao_forma_pgto').val('1');
			}
							
		});
    });

    $('#enviar').bind('click', function(){
    	if($("form")[0].checkValidity()) {
			//your form execution code
			event.preventDefault();
			var i = 0;
			var total = $(".produto_id").length;
			var novos_produtos = $('.new').length;
			if(novos_produtos > 0){
		    	$(".produto_id").each(function( index ) {

		    		var indice = $(this).attr('indice');
		    		console.log(indice);
		    		produtos = "";
		    		$('select[indice="'+indice+'"]').each(function(){
		    			produtos+=$(this).val()+'/';
		    			
		    		});
		    		console.log(produtos);
		    		$('.new_'+indice).val(produtos);
		    		i++;
		    	});

  				$("form").submit();			    	
	    	}else{

	    		$("form").submit();	
	    	}
	    		    	
    	}
    });

    $('#paineis').bind('click', function(){
		$('#m_pedido_pintura').modal({
    		show: true
		});
	}); 

	var coresTesteiraFrontal = [
    {
        text: "BRANCO TOTAL",
        value: "BRANCO TOTAL cod.: 10007312",
        selected: false,
        description: "cod.: 10007312",
        imageSrc: base_url+"bootstrap/img/branco.jpg"
    },
    {
        text: "BRANCO NEVE",
        value: "BRANCO NEVE cod.: JA211B",
        selected: false,
        description: "cod.: JA211B",
        imageSrc: base_url+"bootstrap/img/branco_neve.jpg"
    },
    {
        text: "BRANCO GELO",
        value: "BRANCO GELO cod.: 10005081",
        selected: false,
        description: "cod.: 10005081",
        imageSrc: base_url+"bootstrap/img/branco_gelo.jpg"
    },
    {
        text: "CINZA N8",
        value: "CINZA N8 cod.: PC748B",
        selected: false,
        description: "cod.: PC748B",
        imageSrc: base_url+"bootstrap/img/cinza_n8.jpg"
    },
    {
        text: "CINZA N6,5",
        value: "CINZA N6,5 cod.: JL016B",
        selected: false,
        description: "cod.: JL016B",
        imageSrc: base_url+"bootstrap/img/cinza_n6.jpg"
    },
    {
        text: "CINZA 7037",
        value: "CINZA 7037 cod.: 7037",
        selected: false,
        description: "cod.: 7037",
        imageSrc: base_url+"bootstrap/img/cinza_7037.jpg"
    },
    {
        text: "PRATA + VERNIZ",
        value: "PRATA + VERNIZ cod.: 100044871",
        selected: false,
        description: "cod.: 100044871",
        imageSrc: base_url+"bootstrap/img/prata+verniz.jpg"
    },
    {
        text: "RAL 9001",
        value: "RAL 9001 cod.: 10005061",
        selected: false,
        description: "cod.: 10005061",
        imageSrc: base_url+"bootstrap/img/ral.jpg"
    },
    {
        text: "Bege",
        value: "Bege cod.: JD012B",
        selected: false,
        description: "cod.: JD012B",
        imageSrc: base_url+"bootstrap/img/bege.jpg"
    },
    {
        text: "AMARELO SKOL",
        value: "AMARELO SKOL cod.: HY005B",
        selected: false,
        description: "cod.: HY005B",
        imageSrc: base_url+"bootstrap/img/amarelo_skol.jpg"
    },
    {
        text: "AMARELO OURO",
        value: "AMARELO OURO cod.: 10005738",
        selected: false,
        description: "cod.: 10005738",
        imageSrc: base_url+"bootstrap/img/amarelo_ouro.jpg"
    },
    {
        text: "LARANJA LISO BR",
        value: "LARANJA LISO BR cod.: PBF006",
        selected: false,
        description: "cod.: PBF006",
        imageSrc: base_url+"bootstrap/img/laranja.jpg"
    },
    {
        text: "VERMELHO",
        value: "VERMELHO cod.: PBG002",
        selected: false,
        description: "cod.: PBG002",
        imageSrc: base_url+"bootstrap/img/vermelho.jpg"
    },
    {
        text: "MARROM FOSCO",
        value: "MARROM FOSCO cod.:  M-3600",
        selected: false,
        description: "cod.: M-3600",
        imageSrc: base_url+"bootstrap/img/marrom.jpg"
    },
    {
        text: "VERDE ESCURO",
        value: "VERDE ESCURO cod.: PBK041",
        selected: false,
        description: "cod.: PBK041",
        imageSrc: base_url+"bootstrap/img/verde_escuro.jpg"
    },
 	{
        text: "AZUL LISO BR",
        value: "AZUL LISO BR cod.: EJ002B",
        selected: false,
        description: "cod.: EJ002B",
        imageSrc: base_url+"bootstrap/img/azul_liso.jpg"
    },   
	{
        text: "AZUL MARINHO",
        value: "AZUL MARINHO cod.: PBJ125",
        selected: false,
        description: "cod.: PBJ125",
        imageSrc: base_url+"bootstrap/img/azul_marinho.jpg"
    },  
    {
        text: "AZUL 5015",
        value: "AZUL 5015 cod.: 10005009",
        selected: false,
        description: "cod.: 10005009",
        imageSrc: base_url+"bootstrap/img/azul_5015.jpg"
    },  
    {
        text: "BRANCO AZULADO",
        value: "BRANCO AZULADO cod.: IC004B",
        selected: false,
        description: "cod.: IC004B",
        imageSrc: base_url+"bootstrap/img/branco_azulado.jpg"
    }];

    var coresLateral = [
    {
        text: "BRANCO TOTAL",
        value: "BRANCO TOTAL cod.: 10007312",
        selected: false,
        description: "cod.: 10007312",
        imageSrc: base_url+"bootstrap/img/branco.jpg"
    },
    {
        text: "CINZA 7037",
        value: "CINZA 7037 cod.: 7037",
        selected: false,
        description: "cod.: 7037",
        imageSrc: base_url+"bootstrap/img/cinza_7037.jpg"
    }];

    var selectedTesteira = 0;
    $.each(coresTesteiraFrontal, function (key, item) {
        //console.log(item);        
        if( item.value == $('#v_testeira').val() ){
        	selectedTesteira = key;
        }
    });

    var selectedPainelFrontal = 0;
    $.each(coresTesteiraFrontal, function (key, item) {
        //console.log(item);        
        if( item.value == $('#v_painel_frontal').val() ){
        	selectedPainelFrontal = key;
        }
    });

    var selectedPainelLateral = 0;
    $.each(coresTesteiraFrontal, function (key, item) {
        //console.log(item);        
        if( item.value == $('#v_painel_lateral').val() ){
        	selectedPainelLateral = key;
        }
    });

	$('#testeira').ddslick({
	    data: coresTesteiraFrontal,
	    width: 250,
	    height: 300, 
	    imagePosition: "left",
	    defaultSelectedIndex: selectedTesteira,
	    onSelected: function (data) {	        
	        $('#v_testeira').val(data.selectedData.value);
	    }
	});

	$('#painel_frontal').ddslick({
	    data: coresTesteiraFrontal,
	    width: 250,
	    height: 300, 
	    imagePosition: "left",
	    defaultSelectedIndex: selectedPainelFrontal,
	    onSelected: function (data) { 
	        $('#v_painel_frontal').val(data.selectedData.value);
	    }
	});

	$('#painel_lateral').ddslick({
	    data: coresTesteiraFrontal,
	    width: 200,	
	    height: 300,    
	    imagePosition: "left",
	    defaultSelectedIndex: selectedPainelLateral,
	    onSelected: function (data) {	        
	        $('#v_painel_lateral').val(data.selectedData.value);
	    }
	});
    
});

function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');                
        $('tr[indice="'+indice+'"]').remove(); 
    });    
}

function editar_produtos(combustivel){
	var produtos = "";
	$('.'+combustivel).each(function(){
		produtos+=$(this).val()+'/';
	});

	$.ajax({
		method: "POST", 
		url:  base_url+"AreaAdministrador/editarCombustiveisPedidos", 
		async: true,
		data: { produto_pedido_id 	: 	combustivel,
				produtos 			: 	produtos }
	}).done(function(data) {
		var dados = $.parseJSON(data);		
		
		if( dados.retorno == 'sucesso' ){
			swal({
	   			title: "OK!",
	   			text: "Combustíveis modificados com sucesso",
	   			type: 'success'
	    	}).then(function() {
	    	   	window.location.reload();
	    	}); 


		}
	});

}
