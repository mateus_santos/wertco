$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('.valor_unitario').mask('#.##0,00', {reverse: true}); 

	
	$("#geraPdf").bind('click', function(){

		orcamento_id 	= 	$(this).attr('orcamento_id');	
		contato 		= 	$('#contato_posto').val();			
		fl_especial 	= 	$('#fl_especial').val();

		$.ajax({
			method: "POST",
			url:  base_url+"AreaIndicadores/verificaEmissao",
			async: true,
			data: { orcamento_id 	: 	orcamento_id 	}
		}).done(function(data) {
			var dados = $.parseJSON(data);		

			if(fl_especial == 1 && dados.dias >= 5 ){
				$('#modal_reemissao').modal('show');

			}else if(fl_especial == 0 && dados.dias >= 15 ){
				$('#modal_reemissao').modal('show');

			}else{
				geraPdfOrcamento(orcamento_id, contato);				

			}
			
		});	
	});

	$('#adicionar_motivo').bind('click', function(){
		orcamento_id 		= 	$(this).attr('orcamento_id');
		status_orcamento_id = 	$(this).attr('status_orcamento');
		motivo_emissao 		= 	$('#motivo_emissao').val();
		contato 			= 	$('#contato_posto').val();

		$.ajax({
			method: "POST",
			url:  base_url+"AreaIndicadores/insereMotivoReemissao",
			async: true,
			data: { orcamento_id 		: 	orcamento_id,					
					motivo_emissao 		: 	motivo_emissao,
					status_orcamento_id : 	status_orcamento_id 	}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso'){
				swal({
					title: "OK!",
					text: 'Motivo inserido com sucesso!',
					type: "success"
				}).then(function() {
					cadastraEmissao(orcamento_id);
			      	window.location = base_url+'AreaIndicadores/visualizarOrcamentoInd/'+orcamento_id;
			   	});
				geraPdfOrcamento(orcamento_id, contato);

			}else{
				swal(
			  		'Ops!',
			  		'Algum erro aconteceu!',
			  		'warning'
				);
			}

		});

	});


});

function geraPdfOrcamento(orcamento_id,contato){

	if( contato != '' ) {

		$.ajax({
			method: "POST",
			url:  base_url+"AreaIndicadores/geraOrcamento/"+orcamento_id+"/ajax/"+contato, 
			async: true,
			data: { orcamento_id 	: 	orcamento_id,
					contato 		: 	contato			}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno != 'erro')
			{				
				$('#m_modal_10').modal('hide');
				window.open(base_url+'pdf/'+dados.retorno);
			}

		});		

	}else{
		swal({
			title: "OPS!",
			text: 'Preencha todos os dados para continuar!',
			type: "warning"
		});
	}
}	

function cadastraEmissao(orcamento_id){
	
	var status_orcamento_id = $('#status_id').val();

	$.ajax({
		method: "POST",
		url:  base_url+"AreaIndicadores/insereMotivoReemissao",
		async: true,
		data: { orcamento_id 		: 	orcamento_id,					
				motivo_emissao 		: 	'Alteração no orçamento.',
				status_orcamento_id : 	status_orcamento_id 	}
	}).done(function(data) {
		var dados = $.parseJSON(data);
	});

}


