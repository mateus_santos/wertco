$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#html_table').DataTable({
    	"scrollX": true,
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});


	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');

	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');
	$('div.dataTables_scrollHeadInner').attr('style','box-sizing: content-box; width: 100% !important; padding-right: 0px;');
	$('table.dataTable').attr('style','margin-left: 0px;');

	$('.valor_unitario').mask('#.##0,00', {reverse: true}); 

	$('.confirmar').bind('click', function(){
		var usuario_id 		= $(this).attr('usuario_id');
		var solicitacao_id 	= $(this).attr('solicitacao_id');		
		swal({
		  title: "Atenção?",
		  text: "Deseja realmente confirmar o envio deste cartão?",
		  type: "warning",
		  showCancelButton: !0,
		  confirmButtonClass: "confirm btn btn-lg btn-success",
		  cancelButtonClass: "cancel btn btn-lg btn-default",
		  confirmButtonText: "Sim",
		  cancelButtonText: "Não"
		  
		}).then(function(){						
			confirmarEnvio(solicitacao_id,usuario_id);
			
		});
		
	});

	$('.excluir').bind('click', function(){
		var usuario_id 		= $(this).attr('usuario_id');
		var solicitacao_id 	= $(this).attr('solicitacao_id');		
		swal({
		  title: "Atenção?",
		  text: "Deseja realmente excluir a solicitação deste cartão?",
		  type: "warning",
		  showCancelButton: !0,
		  confirmButtonClass: "confirm btn btn-lg btn-success",
		  cancelButtonClass: "cancel btn btn-lg btn-default",
		  confirmButtonText: "Sim",
		  cancelButtonText: "Não"
		  
		}).then(function(){						
			confirmarExclusao(solicitacao_id,usuario_id);
			
		});
		
	});

});

function confirmarEnvio(id,usuario_id)
{ 
	$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/confirmarEnvio", 
			async: true,
			data: { usuario_id	:	usuario_id,
					id			: 	id}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso'){
			swal({
           		title: "OK!",
           		text: 'Envio confirmado!',
           		type: 'success'
	        	}).then(function() {
	        	   	window.location = base_url+'AreaAdministrador/solicitacoesCartaoTecnico';
	        	}); 
			}
		});		
}

function confirmarExclusao(id,usuario_id)
{ 
	$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/confirmarExclusaoSolicitacaoCartao", 
			async: true,
			data: { usuario_id	:	usuario_id,
					id			: 	id}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso'){
			swal({
           		title: "OK!",
           		text: 'Envio excluido!',
           		type: 'success'
	        	}).then(function() {
	        	   	window.location = base_url+'AreaAdministrador/solicitacoesCartaoTecnico';
	        	}); 
			}
		});		
}