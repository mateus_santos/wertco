$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    $('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 21px;');

    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('table.no-footer').attr('style','');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');

	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	$("#alterar_status").bind('click', function(){

		var solicitacao_id		= 	$(this).attr('solicitacao_id');
		var status_solicitacao	= 	$("#status_solicitacao").val();
		var nome_cliente 		= 	$(this).attr('nome_cliente');
		var chamado_id	 		= 	$(this).attr('chamado_id');

		$.ajax({
			method: "POST", 
			url:  base_url+"AreaAssistencia/alteraStatusSolicitacaoPecas", 
			async: true,
			data: { solicitacao_id 		: 	solicitacao_id,
					status_solicitacao 	: 	status_solicitacao,
					nome_cliente 		: 	nome_cliente,
					chamado_id			: 	chamado_id,
					andamento_cobranca 	: 	'',
					tipo 				: 	'',
					nf_retorno 			: 	''}
		}).done(function(data) {
			var dados = $.parseJSON(data);		

			if(dados.retorno == 'sucesso')
			{
				swal({
		   			title: "OK!",
		   			text: "Status da solicitacao atualizada com sucesso",
		   			type: 'success'
		    	}).then(function() {
		    	   	window.location = base_url+'AreaAssistencia/solicitacaoPecas';
		    	});
			}
		});		
	});
	
	/* autocomplete responsável */
	$( "#responsavel" ).autocomplete({
		source: base_url+"AreaAssistencia/retornaRepresentantes",
		minLength: 1,
		select: function( event, ui ) {
			$('#responsavel_id').val(ui.item.id);
		}
    });   

	$("#adicionar_andamento").bind('click', function(){

		solicitacao_id 		= 	$(this).attr('solicitacao_id');
		status_id			= 	$(this).attr('status_id'); 
		andamento			= 	$("#andamento_texto").val();
		if( andamento == '')
		{
			swal({
	   			title: "Atenção!",
	   			text: "Insira alguma observação para ser adicionada ao processo desta venda!",
	   			type: 'warning'
		    }); 

		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaAssistencia/insereAndamentoSolicitacao", 
				async: true,
				data: { solicitacao_id 			: 	solicitacao_id,
						status_solicitacao_id	: 	status_id,
						andamento 				: 	andamento}
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{				
					swal({
			   			title: "OK!",
			   			text: "Andamento adicionado a solicitação #"+solicitacao_id,
			   			type: 'success'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaAssistencia/solicitacaoPecas';
			    	}); 
				}
			});		

		}
	});

});


function status(solicitacao_id,nome_cliente,chamado_id){
	
	$('.modal-status-title').text('Alterar Solicitação #'+ solicitacao_id);
	$('#alterar_status').attr('solicitacao_id', solicitacao_id);
	$('#alterar_status').attr('nome_cliente', nome_cliente);
	$('#alterar_status').attr('chamado_id', chamado_id);
	$("#m_modal_6").modal({
	    show: true
	});
	
}

function finalizarSolicitação(chamado_id,email){
	
	$('.modal-finalizar-title').text('Finalizar e Enviar E-mail da Solicitação #'+ chamado_id);
	$('#finalizar_Solicitação').attr('chamado_id', chamado_id);
	$('#email').val(email);
	$("#m_modal_7").modal({
	    show: true
	});
	
}

function anexarNfe(solicitacao_id,cliente, nr_nf, arquivo_nfe,dthr_envio ){

	$('.modal-anexar-nfe-title').text('Anexar nota fiscal de emissao a solicitação #'+ solicitacao_id);		
	$('#solicitacao_id').val(solicitacao_id);		
	$('#cliente_id').val(cliente);
	$('#link_arquivo').remove();
	if(arquivo_nfe != '' ){
		$('#nfe').before('<a href="'+base_url+'nfe-assistencia/'+arquivo_nfe+'" target="_blank" id="link_arquivo" class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air"><i class="la la-file-pdf-o"></i></a>');
	}
	$('#nr_nf').val(nr_nf);	
	$('#dthr_envio').val(dthr_envio);
	
	$("#anexarNfe").modal({
	    show: true
	});
	
}

function anexarRastreio(solicitacao_id,rastreio,cliente, rastreio_entrada, comprovante_rastreio, comprovante_rastreio_entrada ){

	$('.modal-anexar-rastreio').text('Anexar número de rastreio para solicitação #'+ solicitacao_id);
	$('#solicitacao_id_rastreio').val(solicitacao_id);	
	$('#rastreio').val(rastreio);	
	$('#rastreio_entrada').val(rastreio_entrada);	
	$('#cliente_id_rastreio').val(cliente);
	$('.comprovante_rastreio a').remove();
	$('.comprovante_rastreio_entrada a').remove();
	if(comprovante_rastreio != '' && comprovante_rastreio != undefined){
		$('#div_rastreio_entrada').attr('style','');
		$('.comprovante_rastreio').append('<a href="'+base_url+'comprovantes_rastreio/'+comprovante_rastreio+'" class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air"><i class="la la-file"></i></a>');
	}else{
		$('#div_rastreio_entrada').attr('style','display: none');
	}
	if(comprovante_rastreio_entrada != '' && comprovante_rastreio_entrada != undefined){
		$('.comprovante_rastreio_entrada').append('<a href="'+base_url+'comprovantes_rastreio/'+comprovante_rastreio_entrada+'" class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air"><i class="la la-file"></i></a>');
	}
	$("#anexarRastreio").modal({
	    show: true
	});
	
}

function andamento(solicitacao_id,status_id){

	$('.modal-andamento-title').text('Andamento solicitação #'+ solicitacao_id);
	$('#adicionar_andamento').attr('solicitacao_id', solicitacao_id);
	$('#adicionar_andamento').attr('status_id', status_id);

	$.ajax({
		method: "POST",
		url:  base_url+"AreaAssistencia/buscaAndamentos", 
		async: true,
		data: { solicitacao_id 	: 	solicitacao_id}
	}).done(function(data) {

		var dados = $.parseJSON(data);	
		$('.m-list-timeline__item').remove();				
		var html="";

		if( dados.length > 0 ){

			for( var i=0;i<dados.length;i++ ){
				var tipo = '';
				
				if( dados[i]['status_id'] == 1)
				{
					tipo = 'secondary';
				}else if( dados[i]['status_id'] == 2 ){
					tipo = 'info';
				}else if( dados[i]['status_id'] == 3 ){
					tipo = 'primary';

				}else if( dados[i]['status_id'] == 4 ){
					tipo = 'success';

				}else if( dados[i]['status_id'] == 5 ){
					tipo = 'danger';

				}else if( dados[i]['status_id'] == 6 ){
					tipo = 'warning';

				}else if( dados[i]['status_id'] == 7 ){
					tipo = 'info';

				}else if( dados[i]['status_id'] == 8 ){
					tipo = 'success';

				}else if( dados[i]['status_id'] == 9 ){
					tipo = 'warning';

				}else{
					tipo = 'success';					
				}
				
				var data_hora = dados[i]['dthr_andamento'].split(' ');
				var data = data_hora[0].split('-');
				var hora = data_hora[1];
				var data_andamento = data[2]+'/'+data[1]+'/'+data[0];
				html+= '<div class="m-list-timeline__item">';
                html+='<span class="m-list-timeline__badge m-list-timeline__badge--'+tipo+'"></span>';
                html+=' <span class="m-list-timeline__text">'+dados[i]['andamento']+'</span>';
                html+=' <span class="m-list-timeline__time">'+dados[i]['usuario']+'  '+data_andamento+' '+hora+'</span>';
                html+='</div>';
            }

            $('.m-list-timeline__items').append(html);
		}else{
			$('.m-list-timeline__item').remove();
		}
	});

	$("#modal-andamento").modal({
	    show: true
	});
    
}

function anexarFotos(solicitacao_id,cliente ){

	$('.modal-anexar-fotos-title').text('Anexar Fotos P/ Conferência Solicitação #'+ solicitacao_id);		
	$('#solicitacao_id_foto').val(solicitacao_id);		
	$('#cliente_id').val(cliente);	
	
	$.ajax({
		method: "POST",
		url:  base_url+"AreaAssistencia/buscarFotosConferencia", 
		async: true,
		data: { solicitacao_id 	: 	solicitacao_id}
	}).done(function(data) {

		var dados = $.parseJSON(data);	
		console.log(dados.length);
		$('#fotosConferencia').empty();
		for (var i = 0; i < dados.length; i++) {
			html="<a href='"+base_url+"nfe-assistencia/"+dados[i].img+"' target='_blank' style='padding-right: 2px'>";
			html+="<img src='"+base_url+"nfe-assistencia/"+dados[i].img+"' width='50' height='50'></a>";
			console.log(html)
			$('#fotosConferencia').append(html);	
		}			
		
	});		
		
	$("#anexarFotos").modal({
	    show: true
	});
	
}




