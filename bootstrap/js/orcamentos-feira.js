$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
   

	$('.novo_orcamento').bind('click', function(){
		window.location.href = base_url+"AreaFeira/cadastraOrcamentos/"+$('input[type="search"]').val();
	});

	$('.status').bind('click', function(){
		if( $(this).attr('status') == 'Solicitação de Orçamentos - Indicador' ){
			$('.aprova_indicacao').show();
			$('.status_orcamento_').hide();
			$('#alterar_status').attr('indicacao','1');
		}else{
			$('.aprova_indicacao').hide();
			$('.status_orcamento_').show();
			$('#alterar_status').attr('indicacao','0');
		}
	});
	
	$('.valor_unitario').mask('#.##0,00', {reverse: true}); 

	$("#alterar_status").bind('click', function(){

		orcamento_id 		= 	$(this).attr('orcamento_id');
		status_orcamento	= 	$("#status_orcamento").val();
		indicacao 			= 	$(this).attr('indicacao');
		aprovar_indicacao 	= 	$('input[name="aprovar_indicacao"]:checked').val();

		if( status_orcamento == "" && aprovar_indicacao == 1 ){
			status_orcamento = 1;
		}else if( status_orcamento == "" && aprovar_indicacao == 0 ){
			status_orcamento = 8;
		}
		
		$.ajax({			
			method: "POST", 
			url:  	base_url+"AreaFeira/alteraStatusOrcamento", 
			async: 	true,
			data: { orcamento_id 		: 	orcamento_id,
					status_orcamento 	: 	status_orcamento,
					indicacao 			: 	indicacao,
					aprovar_indicacao 	: 	aprovar_indicacao	}
			}).done(function(data) {
				var dados = $.parseJSON(data);		

				if(dados.retorno == 'sucesso')
				{
					swal({
						title: "OK!",
						text: "Status do orçamento atualizado com sucesso",
						type: 'success'
					}).then(function() {
						window.location = base_url+'AreaFeira/orcamentos';
					});
				}
			});		
		});

	$("#adicionar_andamento").bind('click', function(){

		orcamento_id 		= 	$(this).attr('orcamento_id');
		status_orcamento_id	= 	$(this).attr('status_orcamento_id'); 
		andamento			= 	$("#andamento_texto").val();
		if( andamento == '')
		{
			swal({
				title: "Atenção!",
				text: "Insira alguma observação para ser adicionada ao processo desta venda!",
				type: 'warning'
			}); 

		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaFeira/insereAndamentoOrcamento", 
				async: true,
				data: { orcamento_id 		: 	orcamento_id,
					status_orcamento_id	: 	status_orcamento_id,
					andamento 			: 	andamento}
				}).done(function(data) {
					var dados = $.parseJSON(data);		

					if(dados.retorno == 'sucesso')
					{				
						swal({
							title: "OK!",
							text: "Observação adicionada ao andamento do orçamento #"+orcamento_id,
							type: 'success'
						}).then(function() {
							window.location = base_url+'AreaFeira/orcamentos';
						}); 
					}
				});		

			}
		});

	/* autocomplete responsável */
	$( "#responsavel" ).autocomplete({
		source: base_url+"AreaFeira/retornaResponsavelOrcamentos",
		minLength: 1,
		select: function( event, ui ) {
			$('#responsavel_id').val(ui.item.id);
		}
	});

	$("#alterar_responsavel").bind('click', function(){
		orcamento_id 				=	$(this).attr('orcamento_id');
		responsavel_id 				= 	$("#responsavel_id").val();
		orcamento_responsavel_id = $(this).attr('responsavel_id');	
		status_orcamento_id = $(this).attr('status_orcamento_id');	

		if(responsavel_id == "")
		{
			swal({
				title: "Atenção!",
				text: "Selecione um representante!",
				type: 'warning'
			}); 

		}else{
			
			$.ajax({
				method: "POST",
				url:  base_url+"AreaFeira/alteraResponsavelOrcamento", 
				async: true,
				data: { orcamento_id 				: 	orcamento_id,
					responsavel_id				: 	responsavel_id,
					orcamento_responsavel_id 	: 	orcamento_responsavel_id,
					status_orcamento_id			: 	status_orcamento_id						}
				}).done(function(data) {
					var dados = $.parseJSON(data);		

					if(dados.retorno == 'sucesso')
					{				
						swal({
							title: "OK!",
							text: "Responsável por dar continuidade ao orçamento foi adicionado com sucesso.",
							type: 'success'
						}).then(function() {
							window.location = base_url+'AreaFeira/orcamentos';
						}); 
					}
				});		
			}	
		});

	$("#tour_virtual").bind('click', function(){

		if($('.status').length > 0){

			var tour;

			tour = new Shepherd.Tour({
				defaults: {
					classes: 'shepherd-element shepherd-open shepherd-theme-arrows',
					scrollTo: true
				}
			});

			tour.addStep('primeiro-step', {
				text: 'Área onde se encontram todos os orçamentos realizados no sistema WERTCO.',
				attachTo: '.m-datatable top',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.status click',
				showCancelLink: true,
				buttons: [
				{
					text: 'Próximo', 
					action: tour.next
				}
				]
			});

			tour.addStep('segundo-step', {
				text: 'Aqui você pode alterar o status do orçamento. Basta clicar no botão e selecionar o próximo passo da negociação.',
				attachTo: '.status top',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.andamento click',
				showCancelLink: true,
				buttons: [		    
				{
					text: 'Anterior',
					action: tour.back
				},
				{
					text: 'Próximo',
					action: tour.next
				}
				]
			});

			tour.addStep('terceiro-step', {
				text: 'Neste botão é possível visualizar o andamento da negociação e adicionar algum comentário/observação a esta negociação. <br/>Lembrando que apenas você e o responsável pelo orçamento, terão acesso ao andamento do mesmo.',
				attachTo: '.andamento_tour top',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.responsavel_tour click',
				showCancelLink: true,
				buttons: [		    
				{
					text: 'Anterior',
					action: tour.back
				},
				{
					text: 'Próximo',
					action: tour.next
				}
				]
			});

			tour.addStep('quarto-step', {
				text: 'Neste botão é possível Alterar o representante responsável por conduzir a venda.',
				attachTo: '.responsavel_tour top',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.visualizar click',
				showCancelLink: true,
				buttons: [		    
				{
					text: 'Anterior',
					action: tour.back
				},
				{
					text: 'Próximo',
					action: tour.next
				}
				]
			});

			tour.addStep('quinto-step', {
				text: 'Aqui pode ser visualizado o orçamento completo.<br/> Além disso, pode-se imprimir o orçamento, gerar desconto<br/> e emitir o orçamento para o cliente final.',
				attachTo: '.visualizar left',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.cadastrar_orcamento click',
				showCancelLink: true,
				buttons: [		    
				{
					text: 'Anterior',
					action: tour.back
				},
				{
					text: 'Próximo',
					action: tour.next
				}
				]
			});

			tour.addStep('sexto-step', {
				text: 'Clique neste botão para cadastrar um novo orçamento no sistema.',
				attachTo: '.cadastrar_orcamento top',
				classes: 'shepherd-theme-arrows',
				advanceOn: '.cadastrar_orcamento click',
				showCancelLink: true,
				buttons: [		    
				{
					text: 'Anterior',
					action: tour.back
				},
				{
					text: 'Finalizar',
					action: tour.next
				}
				]
			});
			
			tour.start();
		}
	});

	$(".geraPdf").bind('click', function(){

		orcamento_id 	= 	$(this).attr('orcamento_id');	
		contato 		= 	$(this).attr('contato');
		
		
		$.ajax({
			method: "POST",
			url:  base_url+"AreaFeira/geraOrcamento/"+orcamento_id+"/ajax/"+contato, 
			async: true,
			data: { orcamento_id 	: 	orcamento_id,
					contato 		: 	contato	}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno != 'erro')
			{
				window.open(base_url+'pdf/'+dados.retorno);
			}

		});		
		
	});
});

function pesquisar(table){
	table.search($('#pesquisa_ativa').val()).draw();
}

function visualizar_orcamento(orcamento_id){
	
	var pesquisa 		= 	$('.dataTables_filter input').val(); 	
	window.open(base_url+'AreaFeira/visualizarOrcamento/'+orcamento_id+'/'+pesquisa,'_blank');

}

function status(orcamento_id){
	
	$('.modal-status-title').text('Alterar Status Orçamento #'+ orcamento_id);
	$('#alterar_status').attr('orcamento_id', orcamento_id);
	$("#m_modal_6").modal({
		show: true
	});
	
}

function excluirOrcamento(orcamento_id){
	
	$('.modal-excluir-title').text('Excluir Orçamento #'+ orcamento_id);
	$('#orcamento_id').val(orcamento_id);
	$("#m_excluir_orcamento").modal({
		show: true
	});
	
}

function andamento(orcamento_id,status_orcamento_id){

	$('.modal-andamento-title').text('Andamento orçamento #'+ orcamento_id);
	$('#adicionar_andamento').attr('orcamento_id', orcamento_id);
	$('#adicionar_andamento').attr('status_orcamento_id', status_orcamento_id);

	$.ajax({
		method: "POST",
		url:  base_url+"AreaFeira/buscaAndamentos", 
		async: true,
		data: { orcamento_id 	: 	orcamento_id}
	}).done(function(data) {

		var dados = $.parseJSON(data);	
		$('.m-list-timeline__item').remove();				
		var html="";

		if( dados.length > 0 ){

			for( var i=0;i<dados.length;i++ ){ 
				var tipo = '';
				if( dados[i]['status_orcamento_id'] == 1)
				{
					tipo = 'secondary';
				}else if( dados[i]['status_orcamento_id'] == 2 ){
					tipo = 'success';
				}else if( dados[i]['status_orcamento_id'] == 3 ){
					tipo = 'danger';

				}else if( dados[i]['status_orcamento_id'] == 4 ){
					tipo = 'warning';

				}else if( dados[i]['status_orcamento_id'] == 5 ){
					tipo = 'info';

				}else if( dados[i]['status_orcamento_id'] == 6 ){
					tipo = 'primary';

				}

				var data_hora = dados[i]['dthr_andamento'].split(' ');
				var data = data_hora[0].split('-');
				var hora = data_hora[1];
				var data_andamento = data[2]+'/'+data[1]+'/'+data[0];
				html+= '<div class="m-list-timeline__item">';
				html+='<span class="m-list-timeline__badge m-list-timeline__badge--'+tipo+'"></span>';
				html+=' <span class="m-list-timeline__text">'+dados[i]['andamento']+'</span>';
				html+=' <span class="m-list-timeline__time">'+dados[i]['usuario']+' '+data_andamento+' '+hora+'</span>';
				html+='</div>';
			}

			$('.m-list-timeline__items').append(html);
		}else{
			$('.m-list-timeline__item').remove();
		}
	});

	$("#m_modal_7").modal({
		show: true
	});

}	

function responsavel(orcamento_id,responsavel_id, status_orcamento_id){
	
	$('.modal-responsavel-title').text('Alterar/Adicionar Responsável pelo Orçamento #'+ orcamento_id);
	$('#alterar_responsavel').attr('orcamento_id', orcamento_id);
	$('#alterar_responsavel').attr('responsavel_id', responsavel_id);
	$('#alterar_responsavel').attr('status_orcamento_id', status_orcamento_id);
	$("#m_modal_8").modal({
		show: true
	});

}

