$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);	
	$('#valor_tributo').mask('00,00', {reverse: true});

	$('select[name="estado_id"]').bind('change', function(){
		var estado_id = $(this).val();
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/verificaEstadoIcms", 
			async: false,
			data: { estado_id 	: estado_id }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'erro'){				
				swal({
		   			title: "Atenção!",
		   			text:  "Icms para esse Estado já está cadastrado.",
		   			type:  'warning'
		    	}).then(function() {
		    	   	window.location = base_url+'AreaAdministrador/cadastraIcms';
		    	}); 
			}
		});	
	});
	
	
});