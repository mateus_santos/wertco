$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('.date').datepicker({
        orientation: 'bottom',
        format: 'dd/mm/yyyy'
    });
	var atualizacao_dados = 0; 
	
	$('.valor_unitario').mask('#.##0,00', {reverse: true}); 
	$('#valor_desconto').mask('00,00', {reverse: true}); 
	$('#valor_desconto_orc').mask('00,00', {reverse: true}); 
	
	if( $('#valor_desconto').val() != "" ){
		$('#valor_desconto').attr('disabled',true);
	}
	$('.editar_produto').bind('click', function(){
		var indice = $(this).attr('indice');
		$('#valor_produto_'+indice).removeAttr('disabled');
	});

	$('.bombas').bind('change', function(){

        indice 					= 	$(this).attr('indice');
        produto_id 				= 	$(this).val();
        orcamento_produto_id 	= 	$(this).attr('orcamento_produto_id');
        orcamento_id 			= 	$('#orcamento_id').val();
        status_id				= 	$('#status_id').val();
        produto 				= 	$(this).find('option:selected').text();
        produto_nr 				= 	indice;
        
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/alterarProdutoOrcamento", 
			async: true,
			data: { produto_id 				: 	produto_id,
					orcamento_produto_id 	: 	orcamento_produto_id,
					status_orcamento_id 	: 	status_id,
					orcamento_id 			: 	orcamento_id,
					produto_nr 				: 	produto_nr,
					produto 				:  	produto
					 }
		}).done(function(data) {
			var dados = $.parseJSON(data);

			if(dados.retorno == 'sucesso')
			{
				atualizacao_dados = 1;	
				swal({
					title: "OK!",
					text: 'Produto alterado com sucesso! Não esqueça de alterar o valor deste produto.',
					type: "success"
				}).then(function() {

					$('#valor_produto_'+indice).css('border','2px solid green');
					$('#valor_produto_'+indice).removeAttr('disabled');
					$('#valor_produto_'+indice).focus();					
					window.setTimeout(function(){ $('#valor_produto_'+indice).css('border','none'); }, 6000);
				});
			}
		});
    }); 

	$('.entregaFretePagto').bind('change', function(){
		var orcamento_id 		= 	$(this).attr('orcamento_id');
		var valor 				= 	$(this).val();
		var tipo 				= 	$(this).attr('id');	
		var status_orcamento_id	= 	$(this).attr('status_id');
		
		var url = '';
		if( tipo == 'forma_pagto' ){
			url = 'alterarFormaPagto';
			desc_tipo = 'Forma de Pagamento';
		}else if( tipo == 'entregas' ) {
			url = 'alterarEntrega';
			desc_tipo = 'Forma de Entrega';
		}else if( tipo == 'frete'){
			url = 'alterarFrete';
			desc_tipo = 'Frete';
		}else{
			url = 'alterarGarantia';
			desc_tipo = 'Garantia';
		}

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/"+url, 
			async: true,
			data: { valor 				: 	valor,
					orcamento_id 		: 	orcamento_id,
					status_orcamento_id	: 	status_orcamento_id}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			atualizacao_dados = 1;	
			if(dados.retorno == 'sucesso')
			{
				swal({
					title: "OK!",
					text: desc_tipo+' alterado com sucesso!',
					type: "success"
				});
			}
		});
	});

	$('.editar_qtd').bind('click', function(){
		var indice = $(this).attr('indice');
		$('#qtd_'+indice).removeAttr('disabled');
	});

	$('.qtd').bind('focusout', function(){
		
		var indice 					= 	$(this).attr('indice');
		var qtd 					= 	$(this).val();
		var orcamento_produto_id 	= 	$(this).attr('orcamento_produto_id');
		var orcamento_id 			= 	$('#orcamento_id').val();
        var status_id				= 	$('#status_id').val();
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/alterarQtdProduto", 
			async: true,
			data: { qtd 					: 	qtd,
					orcamento_produto_id 	: 	orcamento_produto_id,
					status_orcamento_id 	: 	status_id,
					orcamento_id 			: 	orcamento_id  }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			atualizacao_dados = 1;	
			if(dados.retorno == 'sucesso')
			{				
				$('#valor_produto_'+indice).attr('qtd', qtd);
			}
		});

	});	

	$('#observacao').bind('focusout', function(){

		var observacao	= 	$(this).val();
		var id			= 	$('#orcamento_id').val();
		
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/alterarObservacao",
			async: true,
			data: { observacao	: observacao,
					id 			: id }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			atualizacao_dados = 1;	
			if(dados.retorno == 'sucesso')
			{
				swal({
					title: "OK!",
					text: 'Observação alterada com sucesso!',
					type: "success"
				});
			}
		});

	});	

	$('#dt_previsao').bind('change', function(){

		var dt_previsao	= 	$(this).val();
		var id			= 	$('#orcamento_id').val();
		if(dt_previsao != ''){
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/alterarDtPrevisao",
				async: true,
				data: { dt_previsao	: dt_previsao,
						id 			: id }
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				atualizacao_dados = 1;	
				if(dados.retorno == 'sucesso')
				{
					swal({
						title: "OK!",
						text: 'Previsão de fechamento alterada com sucesso!',
						type: "success"
					});
				}
			});
		}

	});

	$('#dt_previsao_prod').bind('change', function(){

		var dt_previsao	= 	$(this).val();
		var id			= 	$('#orcamento_id').val();
		if(dt_previsao != ''){
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/alterarDtPrevisaoProd",
				async: true,
				data: { dt_previsao_prod	: dt_previsao,
						id 			: id }
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				atualizacao_dados = 1;	
				if(dados.retorno == 'sucesso')
				{
					swal({
						title: "OK!",
						text: 'Previsão de produção alterada com sucesso!',
						type: "success"
					});
				}
			});
		}

	});

	$('.valor_produto').bind('focusout', function(){
		var valor_produto 			= 	$(this).val();
		var orcamento_id 			= 	$(this).attr('orcamento_id');
		var orcamento_produto_id 	= 	$(this).attr('orcamento_produto_id');
		var orcamento_id 			= 	$('#orcamento_id').val();
        var status_id				= 	$('#status_id').val();

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/alterarValorProduto", 
			async: true,
			data: { valor_produto 			: 	valor_produto,
					orcamento_produto_id 	: 	orcamento_produto_id,
					status_orcamento_id 	: 	status_id,
					orcamento_id 			: 	orcamento_id }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			atualizacao_dados = 1;	
			if(dados.retorno == 'sucesso')
			{				
				
			}
		});

	});	

	$('.atualiza_valor').bind('click', function(){
		var valor = 0;
		$('.valor_produto').each(function(){
			novo_valor = replaceAll($(this).val(), '.', '');
			novo_valor = replaceAll(novo_valor, ',', '.');
			
			valor = valor + (parseFloat(novo_valor) * parseInt($(this).attr('qtd')));
			
		});
		
		subtotal 	= 	valor.toFixed(2);
		valor_total = 	(valor + (parseFloat(valor.toFixed(2))/100 * parseFloat($('#valor_tributo').val())) + (parseFloat(valor.toFixed(2))/100  * 5)).toFixed(2);
		$('.valor_total').text(valor_total);
		$('.valor_subtotal').text(subtotal);
		$('.valor_total').mask('#.##0,00', {reverse: true}); 
		$('.valor_subtotal').mask('#.##0,00', {reverse: true});
	});


	$('#alterar_valor').bind('click', function(){
		orcamento_id 	= 	$(this).attr('orcamento_id');	
		status_id 		= 	$(this).attr('status_id');	
		valor_orcamento = 	$('#valor_orcamento').val();
			
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/alterarValorOrcamento", 
			async: true,
			data: { orcamento_id 	: 	orcamento_id,
					valor_orcamento : 	valor_orcamento,
					status_id 		: 	status_id}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso')
			{				
				atualizacao_dados = 1;	
				swal({
						title: "OK!",
						text: 'Valor do Orçamento alterado com sucesso!',
						type: "success"
					}).then(function() {
				      	window.location = base_url+'AreaAdministrador/visualizarOrcamento/'+orcamento_id;
				   	});

			}else{		
				swal({
					title: "OPS!",
					text: 'Alguma inconsistência ocorreu, contate o webmaster!',
					type: "warning"
				}).then(function() {
			      	window.location = base_url+'AreaAdministrador/orcamentos';
			   	});
			}

		});		

	});

	$("#geraPdf").bind('click', function(){

		orcamento_id 	    	= 	$(this).attr('orcamento_id');	
		contato 		    	= 	$('#contato_posto').val();			
		fl_especial 	    	= 	$('#fl_especial').val();
		status_orcamento_id 	= 	$(this).attr('status_id');

		if( status_orcamento_id == 2 ){

			geraPdfOrcamento(orcamento_id, contato);
		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministradorComercial/verificaEmissao",
				async: true,
				data: { orcamento_id 	: 	orcamento_id }
			}).done(function(data) {
				var dados = $.parseJSON(data);	
				
				if(fl_especial == 1 && dados.dias >= 5 && atualizacao_dados == 0){
					$('#modal_reemissao').modal('show');

				}else if(fl_especial == 0 && dados.dias >= 15 && atualizacao_dados == 0	){
					$('#modal_reemissao').modal('show');

				}else{
					geraPdfOrcamento(orcamento_id, contato);				

				}
				
			});		
		}

		
	});

	$('#adicionar_motivo').bind('click', function(){
		orcamento_id 		= 	$(this).attr('orcamento_id');
		status_orcamento_id = 	$(this).attr('status_orcamento');
		motivo_emissao 		= 	$('#motivo_emissao').val();
		contato 			= 	$('#contato_posto').val();

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministradorComercial/insereMotivoReemissao",
			async: true,
			data: { orcamento_id 		: 	orcamento_id,					
					motivo_emissao 		: 	motivo_emissao,
					status_orcamento_id : 	status_orcamento_id 	}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			if(dados.retorno == 'sucesso'){
				swal({
					title: "OK!",
					text: 'Motivo inserido com sucesso!',
					type: "success"
				}).then(function() {
			      	window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;
			   	});
				geraPdfOrcamento(orcamento_id, contato);		
				
			}else{
				swal(
			  		'Ops!',
			  		'Algum erro aconteceu!',
			  		'warning'
				);
			}

		});

	});

	$("#valor_desconto").bind('focusout', function(){
		
		if( $(this).val() != "" )
		{
			$("#motivo_desconto").removeAttr('disabled');
		}else{
			$("#motivo_desconto").attr('disabled',true);
		}
	});

	$("#valor_desconto_orc").bind('focusout', function(){
		
		if( $(this).val() != "" )
		{
			$("#motivo_desconto_orc").removeAttr('disabled');
		}else{
			$("#motivo_desconto_orc").attr('disabled',true);
		}
	});

	$("#emitir_orcamento").bind('click', function(){

		orcamento_id 		= 	$(this).attr('orcamento_id');
		email_destino		= 	$('#email_destino').val();
		//desconto			= 	$('#valor_desconto').val();
		//motivo_desconto	= 	$('#motivo_desconto').val();
		valor_tributo		= 	$('#valor_tributo').val();
		valor_desconto		= 	$('#valor_desconto').val();
		valor_total			= 	$('#valor_total').val();
		status_orcamento_id	= 	$(this).attr('status_id');		
		contato_posto		= 	$('#contato_posto_env').val(); 	

		salvar_desconto = 0;		
		var erro = 0;
		if( erro == 0 ){
			
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/emitirOrcamento", 
				async: true,
				data: { orcamento_id 		: 	orcamento_id,
						email_destino		: 	email_destino,						
						valor_tributo 		: 	valor_tributo,
						valor_total 		: 	valor_total,
						salvar_desconto 	: 	salvar_desconto,
						status_orcamento_id : 	status_orcamento_id,						
						contato_posto 		: 	contato_posto }

			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{
					
					swal({
						title: "OK!",
						text: 'Orçamento Emitido com sucesso!',
						type: "success"
					}).then(function() {
				      	window.location = base_url+'AreaAdministrador/orcamentos';
				   	});

				}else{
			
					swal({
						title: "OPS!",
						text: 'Alguma inconsistência ocorreu, contate o webmaster!',
						type: "warning"
					}).then(function() {
				      	window.location = base_url+'AreaAdministrador/orcamentos';
				   	});
				}

			});		
		}
	});

	$("#adicionar_desconto_orc").bind('click', function(){
		var valor_desconto 		=  	$('#valor_desconto_orc').val();	
		var motivo_desconto 	=  	$('#motivo_desconto_orc').val();
		var orcamento_id 		= 	$(this).attr('orcamento_id');
		var erro = 0;
		if(valor_desconto == ""){
			swal(
		  		'Ops!',
		  		'Insira um desconto para o orçamento!',
		  		'warning'
			);
			erro++;	
		}

		if( valor_desconto != "" && motivo_desconto == ""){
			swal(
		  		'Ops!',
		  		'Insira um motivo para o desconto dado!',
		  		'warning'
			);
			erro++;	
		}		

		if( erro == 0 ){

			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/insereDescontoOrcamento", 
				async: true,
				data: { orcamento_id 		: 	orcamento_id,
						valor_desconto 		: 	valor_desconto,
						motivo_desconto 	: 	motivo_desconto }

			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{				
					swal({
						title: "OK!",
						text: 'Desconto adicionado ao orçamento com sucesso!',
						type: "success"
					}).then(function() {
				      	window.location = base_url+'AreaAdministrador/visualizarOrcamento/'+orcamento_id;
				   	});
				}else{
					swal({
						title: "OPS!",
						text: 'Alguma inconsistência ocorreu, contate o webmaster!',
						type: "warning"
					}).then(function() {
				      	window.location = base_url+'AreaAdministrador/visualizarOrcamento/'+orcamento_id;
				   	});
				}

			});		
		}


	});

	$('.add').bind('click', function(){

        var html = $('#modelo').clone();                
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
        $(html).find('.bombas_new').attr('indice', indice);
        $(html).find('.bombas_new').attr('id', 'bombas_new_'+indice);
        $(html).find('.valor_unitario').attr('indice', indice);
        $(html).find('.valor_unitario').attr('id', 'valor_unitario_'+indice);

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();                
        body+="</tr>";                
        $('table.novo').append(body);                
        $('#modelo').attr('total_indice',indice);
		
        excluir();
        mostra_valor();        
    });
    
    mostra_valor();


    $('#adicionar_produto').bind('click', function(){
    	var dados 			= 	$('#produtos_novos').serializeArray();
    	orcamento_id 		= 	$(this).attr('orcamento_id');
    	status_orcamento_id = 	$(this).attr('status_id');
		var inputVazios = $('.novo').find('input').filter(function () {
        	return this.value.split(' ').join('') == '';
    	});

    	var selectVazios = $('.novo').find('select').filter(function () {
        	return this.value.split(' ').join('') == '';
    	});

		if( selectVazios.length > 0 || inputVazios.length > 0){
			swal({
				title: "Atenção!",
				text: 'Preencha corretamente os campos dos novos produtos!',
				type: "warning"
			});	
		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/insereProdutosNovosOrcamento", 
				async: true,
				data: { dados				: 	dados,
						orcamento_id 		: 	orcamento_id,
						status_orcamento_id : 	status_orcamento_id }

			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{				
					swal({
						title: "OK!",
						text: 'Produto(s) adicionado(s) ao orçamento com sucesso!',
						type: "success"
					}).then(function() {
				      	window.location = base_url+'AreaAdministrador/visualizarOrcamento/'+orcamento_id;
				   	});
				}else{
					swal({
						title: "OPS!",
						text: 'Alguma inconsistência ocorreu, contate o webmaster!',
						type: "warning"
					}).then(function() {
				      	window.location = base_url+'AreaAdministrador/visualizarOrcamento/'+orcamento_id;
				   	});
				}
			});	
		}
	});


	$( "#nome_indicador" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaIndicadores",
		minLength: 2,
		select: function( event, ui ) {

			$('#nome_indicador').val(ui.item.id);
			var indicador 		= 	ui.item.id;
	    	var orcamento_id 	= 	$('#nome_indicador').attr('orcamento_id');
	    	var status_orcamento_id	= 	$('#status_id').val();

	    	$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/alteraIndicadorOrcamento", 
				async: true,
				data: { indicador			: 	indicador,
						orcamento_id 		: 	orcamento_id,
						status_orcamento_id : 	status_orcamento_id }

			}).done(function(data) {

				var dados = $.parseJSON(data);
				if(dados.retorno == 'sucesso'){
					swal({
						title: "OK!",
						text: 'Indicador atualizado com sucesso!',
						type: "success"
					});

					$('#alterar_indicador').text($('#nome_indicador').val());

				}else{
					swal({
						title: "OPS!",
						text: 'Acontenceu um erro inesperado, entre em contato com o webmaster!',
						type: "warning"
					});
				}
	    	});
		}
    });

	$('.excluir_indicador').bind('click', function(){
		indicador = '';
        orcamento_id = $(this).attr('orcamento_id');
        
        $.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/alteraIndicadorOrcamento", 
			async: true,
			data: { indicador		: 	indicador,
					orcamento_id 	: 	orcamento_id }

		}).done(function(data) {

			var dados = $.parseJSON(data);
			if(dados.retorno == 'sucesso'){
				swal({
					title: "OK!",
					text: 'Indicador excluído com sucesso!',
					type: "success"
				});

				$("#alterar_indicador").text('NÃO HOUVE INDICAÇÃO PARA ESTE ORÇAMENTO');
			}else{
				swal({
					title: "OPS!",
					text: 'Acontenceu um erro inesperado, entre em contato com o webmaster!',
					type: "warning"
				});
			}
    	});	
	});

	$("#adicionar_andamento").bind('click', function(){

		orcamento_id 		= 	$(this).attr('orcamento_id');
		status_orcamento_id	= 	$(this).attr('status_orcamento_id'); 
		andamento			= 	$("#andamento_texto").val();
		if( andamento == '')
		{
			swal({
				title: "Atenção!",
				text: "Insira alguma observação para ser adicionada ao processo desta venda!",
				type: 'warning'
			}); 

		}else{
			var dthr_alerta = data_alerta = '';
			if( $('.receber_alerta').is(':checked') ){
				if( $('.dthr_alerta').val() == '' || $('.dthr_alerta').val() == undefined ){
					swal({
						title: "Atenção!",
						text: "Insria uma data válida.",
						type: 'warning'
					}); 					
				}else{
					dthr_alerta = $('.dthr_alerta').val();
					var data_explode = dthr_alerta.split('/');					
					var data_alerta = data_explode[2]+"*"+data_explode[1]+"*"+data_explode[0];
				}
			}
			
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/insereAndamentoOrcamento", 
				async: true,
				data: { orcamento_id 		: 	orcamento_id,
						status_orcamento_id	: 	status_orcamento_id,
						andamento 			: 	andamento,
						dthr_alerta 		: 	data_alerta	}
			}).done(function(data) {
				var dados = $.parseJSON(data);		

				if(dados.retorno == 'sucesso')
				{
					swal(	{
						title: "OK!",
						text: "Observação adicionada ao andamento do orçamento #"+orcamento_id,
						type: 'success'
					}).then(function() {
						window.location = base_url+'AreaAdministrador/visualizarOrcamento/'+orcamento_id;
					}	); 
				}
			});
		}
	});

	$('.indicacao').bind('click', function(){
        if($(this).is(':checked')){
            $('.nome_indicador').fadeIn('slow');
            $('#nome_indicador').attr('required','required');

        }else{

            $('.nome_indicador').fadeOut('slow');
            $('#nome_indicador').removeAttr('required');
            $('#nome_indicador').val('');

          /* indicador = '';
            orcamento_id = $('#nome_indicador').attr('orcamento_id');
            
            $.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/alteraIndicadorOrcamento", 
				async: true,
				data: { indicador		: 	indicador,
						orcamento_id 	: 	orcamento_id }

			}).done(function(data) {

				var dados = $.parseJSON(data);
				if(dados.retorno == 'sucesso'){
					swal({
						title: "OK!",
						text: 'Indicador atualizado com sucesso!',
						type: "success"
					});
				}else{
					swal({
						title: "OPS!",
						text: 'Acontenceu um erro inesperado, entre em contato com o webmaster!',
						type: "warning"
					});
				}
	    	});*/
        }
    });

    
});

function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');                
        $('tr[indice="'+indice+'"]').remove(); 
    });    
}

function mostra_valor()
{
    $('.bombas_new').bind('change', function(){
        indice = $(this).attr('indice');
        $('#valor_unitario_'+indice).val($(this).find(':selected').attr('valor_unitario')); 
        $('#valor_unitario_'+indice).mask('#.##0,00', {reverse: true});
    }); 

}

function excluirProduto(orcamento_produto_id,indice){
		var total_linhas = $('.novo_produto_orc').length;
		if( total_linhas == 1 ){
			swal({
				title: "OPS!",
				text: 'Impossível remover este produto. Altere o status do orçamento para "CANCELADO"!',
				type: "warning"
			});
		}else{

			swal({
			  title: 'Excluir?',
			  text: "Deseja realmente excluir esse produto?",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Sim!'
			}).then((result) => {
				if (result.value) {
					$.ajax({
						method: "POST",
						url:  base_url+"AreaAdministrador/excluirProdutoOrcamento", 
						async: true,
						data: { orcamento_produto_id	: 	orcamento_produto_id }

					}).done(function(data) {
						var dados = $.parseJSON(data);		
					
						if(dados.retorno == 'sucesso')
						{				
							swal({
								title: "OK!",
								text: 'Produto(s) excluído(s) do orçamento com sucesso!',
								type: "success"
							});

							$('tr[indice="'+indice+'"]').remove();

						}else{
							swal({
								title: "OPS!",
								text: 'Alguma inconsistência ocorreu, contate o webmaster!',
								type: "warning"
							}).then(function() {
					      		window.location = base_url+'AreaAdministrador/visualizarOrcamento/'+orcamento_id;
					   		});
						}

					});	
				}
			});
		}


}

function replaceAll(str, de, para){
    var pos = str.indexOf(de);
    while (pos > -1){
		str = str.replace(de, para);
		pos = str.indexOf(de);
	}
    return (str);
}
 
function adicionar_funcionario(){

	if($('#nome').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o nome do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	
	if($('#email').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o email do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}
	if($('#telefone').val() == ''){
		swal({
			title: "Campo em branco",
			text: "Preencha o telefone do funcionário",
			type: "warning"
		}).then(function() {
			$("#modal_add_usuario").modal('show');
		});
		return;
	}

	var orcamento_id = $('#orcamento_id').val();

	$.ajax({
		method: "POST",
		url: base_url+'usuarios/cadastrar',
		async: true,
		data: {
			nome: 		$('#nome').val(),
			cpf: 		$('#cpf').val(),
			email: 			$('#email').val(),
			telefone: 		$('#telefone').val(),
			empresa_id: 	$('#empresa_id').val(),
			tipo_cadastro_id: '1'
		},
		success: function( data ) {
			var dados = $.parseJSON(data);			
			var usuario_id = dados.id;
			$.ajax({
				method: "POST",				
				url: base_url+'AreaAdministrador/insereContatoOrcamento',				
				async: true,
				data: {
					orcamento_id 	: 	orcamento_id,
					contato_id 		: 	usuario_id
				},
				success: function( data ) {
					var dados = $.parseJSON(data);
					if(dados.retorno == 'sucesso'){
						swal({
							title: 'OK',
							text: 'Contato do posto vinculado ao orçamento com sucesso',
							type: 'success'
						}).then(function() {
							location.reload();
						});
					}else{
						swal({
							title: 'OPS!',
							text: 'Entre em contato com a webmaster',
							type: 'warning'
						}).then(function() {
							//location.reload();
						});
					}
				}
			});
			
		}
	});
}

function aprovarOrcamento(orcamento_id){
	$.ajax({
		method: "POST", 
		url:  base_url+"AreaAdministrador/confirmaOrcamentoGestor", 
		async: true,
		data: { orcamento_id 	: 	orcamento_id 	}

	}).done(function(data) {
		var dados = $.parseJSON(data);	
		if( dados.retorno == 'sucesso'){
			swal({
				title: 	"OK!",
				text: 	'Orçamento aprovado com sucesso!',
				type: 	"success"
			}).then(function() {
		      	window.location = base_url+'AreaAdministrador/visualizarOrcamento/'+orcamento_id;
		   	}); 
		}

	});
}

function geraPdfOrcamento(orcamento_id, contato){

	if( contato != '' ) {

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/geraOrcamento/"+orcamento_id+"/ajax/"+contato, 
			async: true,
			data: { orcamento_id 	: 	orcamento_id,
					contato 		: 	contato			}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno != 'erro')
			{				
				$('#m_modal_10').modal('hide');
				window.open(base_url+'pdf/'+dados.retorno);
			}

		});		

	}else{
		swal({
			title: "OPS!",
			text: 'Preencha todos os dados para continuar!',
			type: "warning"
		});
	}	
	
 }

function cadastraEmissao(orcamento_id){
	var status_orcamento_id = $('#status_id').val();
	
	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministradorComercial/insereMotivoReemissao",
		async: true,
		data: { orcamento_id 		: 	orcamento_id,					
				motivo_emissao 		: 	'Alteração no orçamento.',
				status_orcamento_id : 	status_orcamento_id 	}
	}).done(function(data) {
		var dados = $.parseJSON(data);
	});

}

function andamento(orcamento_id,status_orcamento_id){

	$('.modal-andamento-title').text('Andamento orçamento #'+ orcamento_id);
	$('#adicionar_andamento').attr('orcamento_id', orcamento_id);
	$('#adicionar_andamento').attr('status_orcamento_id', status_orcamento_id);

	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/buscaAndamentos", 
		async: true,
		data: { orcamento_id 	: 	orcamento_id}
	}).done(function(data) {

		var dados = $.parseJSON(data);	
		//$('.m-list-timeline__item').remove();				
		var html="";

		if( dados.length > 0 ){

			for( var i=0;i<dados.length;i++ ){ 
				var tipo = '';
				if( dados[i]['status_orcamento_id'] == 1)
				{
					tipo = 'secondary';
				}else if( dados[i]['status_orcamento_id'] == 2 ){
					tipo = 'success';
				}else if( dados[i]['status_orcamento_id'] == 3 ){
					tipo = 'danger';

				}else if( dados[i]['status_orcamento_id'] == 4 ){
					tipo = 'warning';

				}else if( dados[i]['status_orcamento_id'] == 5 ){
					tipo = 'info';

				}else if( dados[i]['status_orcamento_id'] == 6 ){
					tipo = 'primary';

				}

				var data_hora = dados[i]['dthr_andamento'].split(' ');
				var data = data_hora[0].split('-');
				var hora = data_hora[1];
				var data_andamento = data[2]+'/'+data[1]+'/'+data[0];
				html+= '<div class="m-list-timeline__item">';
				html+='<span class="m-list-timeline__badge m-list-timeline__badge--'+tipo+'"></span>';
				html+=' <span class="m-list-timeline__text">'+dados[i]['andamento']+'</span>';
				html+=' <span class="m-list-timeline__time">'+dados[i]['usuario']+' '+data_andamento+' '+hora+'</span>';
				html+='</div>';
			}
			$('.andamento_list > .m-list-timeline__items').empty();
			$('.andamento_list > .m-list-timeline__items').append(html);
		}else{
			$('.m-list-timeline__item').remove();
		}
	});

	$("#andamento").modal({
		show: true
	});

}	