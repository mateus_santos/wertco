$(document).ready(function(){

	$('#dt_programacao, #dt_semana_nova1, #dt_semana_nova2').datepicker({
		format: 'dd/mm/yyyy',
    	daysOfWeekDisabled: [0,2,3,4,5,6]
	});
	
	// Esconde os anos anteriores
	$('.linha_semana[ano="2018"]').slideUp('slow');
	$('.fa[ano="2018"]').removeClass('fa-arrow-down');
	$('.fa[ano="2018"]').addClass('fa-arrow-up');
	$('.linha_semana[ano="2019"]').slideUp('slow');
	$('.fa[ano="2019"]').removeClass('fa-arrow-down');
	$('.fa[ano="2019"]').addClass('fa-arrow-up');
	$('.linha_semana[ano="2020"]').slideUp('slow');
	$('.fa[ano="2020"]').removeClass('fa-arrow-down');
	$('.fa[ano="2020"]').addClass('fa-arrow-up');
	$('.linha_semana[ano="2021"]').slideUp('slow');
	$('.fa[ano="2021"]').removeClass('fa-arrow-down');
	$('.fa[ano="2021"]').addClass('fa-arrow-up');
	
	$('.linha_ano').bind('click', function(){
		var ano = $(this).attr('ano');
		if( $('.fa[ano="'+ano+'"]' ).hasClass('fa-arrow-up')){
			$('.fa[ano="'+ano+'"]').removeClass('fa-arrow-up');
			$('.fa[ano="'+ano+'"]').addClass('fa-arrow-down');
		}else if( $('.fa[ano="'+ano+'"]' ).hasClass('fa-arrow-down')){
			$('.fa[ano="'+ano+'"]').removeClass('fa-arrow-down');
			$('.fa[ano="'+ano+'"]').addClass('fa-arrow-up');
		}
		$('.linha_semana[ano="'+ano+'"]').toggle('slow');
	});

	$('#excluir_pedido').bind('click', function(){

		var pedido_id = $(this).attr('pedido_id');
		
		swal({
	        title: 'Tem certeza?',
	        text: 'Deseja realmente excluir esse pedido da programação?',
	        type: 'warning',
	        showCancelButton: true,
	        confirmButtonClass: "swal2-confirm btn btn-success m-btn m-btn--custom",
	        confirmButtonText: "Sim",
	        cancelButtonText: "Cancelar"
	    }).then(function(isConfirm) {

		  if (isConfirm.value) {
		  	$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/excluirPedidoPS", 
				async: false,
				data: { pedido_id : pedido_id }		
			}).done(function(data) {
				var dados = $.parseJSON(data);

				swal({
		   			title:"Ok!",
		   			text: "Pedido retirado da programação da produção.",
		   			type: 'success'
		    	}).then(function() {
		    		location.reload(); 
		    	}); 

			});	
		  }else{

		  
		  }

		});
	});

	
		
		$( ".area_pedidos" ).droppable({
	      revert: true
	    });
		
		$( ".pedidos" ).draggable({
			start: function( event, ui ) {
				$(this).removeClass('parado');
				$(this).addClass('movido');
				$(this).find('i.primeiro').attr('class','la la-arrows-alt');
			},
			
			revert: function(is_valid_drop,ui){

				if(!is_valid_drop){
					$(this).removeClass('movido');
					$(this).addClass('parado');	
					$(this).find('i.primeiro').attr('class','la la-edit');
				   	return true;

				} else {

				   	var bombas_semana 			= 	$(is_valid_drop).attr('bombas');
					var bicos_semana 			= 	$(is_valid_drop).attr('bicos');
					var bombas_regra 			= 	$(is_valid_drop).attr('total_bombas');	
					var bicos_regra 			= 	$(is_valid_drop).attr('total_bicos');
					var dt_semana_prog 			= 	$(is_valid_drop).attr('semana');
					var bicos_pedido 			= 	$(this).attr('bicos');	
					var bombas_pedido 			= 	$(this).attr('bombas');										
					var pedido_id 				= 	$(this).attr('pedido_id');				
					var programacao_semanal_id 	= 	$(this).attr('programacao_semanal_id');
					var	semana_atual			= 	$('#semana_atual').val();
					
					console.log('dt_semana_prog: '+dt_semana_prog );
					console.log('programacao_semanal_id: '+programacao_semanal_id );

					total_bombas 		= 	parseInt(bombas_semana)	+ 	parseInt(bombas_pedido);
					total_bicos  		= 	parseInt(bicos_semana) 	+ 	parseInt(bicos_pedido);	
						
					if( total_bombas > bombas_regra || total_bicos > bicos_regra  ){

						$(this).removeClass('movido');
						$(this).addClass('parado');			    				
						$(this).find('i.primeiro').attr('class','la la-edit');
						swal({
				   			title:"Atenção!",
				   			text: "Total de bicos ou de bombas maior que o limite permitido.",
				   			type: 'warning'
				    	}).then(function() {}); 	
						return true;	

					}else{
						var validacao = 0;						

						$(this).removeClass('movido');
						$(this).addClass('parado');			    				
						$(this).find('i.primeiro').attr('class','la la-check');
						var obj = $(this);
						if( validacao == 0 ){
							swal({
						        title: 'Tem certeza?',
						        text: 'Deseja realmente alterar a data de previsão de fechamento dessa negociação?',
						        type: 'warning',
						        showCancelButton: true,
						        confirmButtonClass: "swal2-confirm btn btn-success m-btn m-btn--custom",
						        confirmButtonText: "Sim",
						        cancelButtonText: "Cancelar"
						    }).then(function(isConfirm) {

							  if (isConfirm.value) {
							  	$.ajax({
									method: "POST",
									url:  base_url+"AreaAdministrador/atualizarProgramacaoComercial", 
									async: false,
									data: { programacao_semanal_id 	: 	programacao_semanal_id,
											dt_semana_prog			: 	dt_semana_prog	}		
								}).done(function(data) {
									var dados = $.parseJSON(data);

									swal({
							   			title:"Ok!",
							   			text: "Data de produção alterada com sucesso.",
							   			type: 'success'
							    	}).then(function() {
							    		location.reload(); 
							    	}); 
			
								});	
							  }else{

							  	obj.removeClass('movido');
								obj.addClass('parado');			    				
								obj.addClass('borda-erro');
								obj.find('i.primeiro').attr('class','la la-warning');
							  	return true;
							  }

							});
						}				
					}

				}
			},
			stop: function(event, ui){}
		});	
	
	//informação
	$('.la-info-circle').bind('click', function(){
		
		var orcamento_id 			= 	$(this).parents('span.pedidos').attr('orcamento_id');		
		var programacao_semanal_id 	= 	$(this).parents('span.pedidos').attr('programacao_semanal_id');
		var tipo 					= 	$(this).parents('span.pedidos').attr('tipo');

		$('#alterar_dt').attr('orcamento_id',orcamento_id);

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/buscaDadosNegociacao",
			async: true,
			data: { orcamento_id :  programacao_semanal_id,
					tipo 		 : 	tipo	}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			console.log(dados);
			var conteudo = 'Orçamento #'+dados[0].orcamento_id+' - '+dados[0].status;
			if (tipo == 'pedido'){
				conteudo+= ' | Pedido #'+dados[0].pedido_id+' - '+dados[0].status_pedido;
			}
			$('#titulo').text(conteudo);
			$('#cliente').text(dados[0].cliente);
			$('#cidade').text(dados[0].cidade);
			$('#bombas').text(dados[0].qtd);
			$('#bicos').text(dados[0].bicos);
			$('#modelos').empty();			

			for (var i = 0; i < dados.length; i++) {
				$('#modelos').append('<h5>'+dados[i].modelo+': <small class="text-muted">'+dados[i].qtd+'</small></h5>');

			}			

			$('#visualizar_status').attr('orcamento_id', orcamento_id);			
			if( dados[0].status_id == 7 ){
				$('#excluir_pedido').fadeIn('slow');
				$('#excluir_pedido').attr('pedido_id',pedido_id);
			}else{
				$('#excluir_pedido').fadeOut('slow');
			}
			$("#info-pedido").modal({
				show: true
			});

		});
	});

	$('#observacao_pedido').bind('focusout', function(){
		var pedido_id 	= 	$('#visualizar_status').attr('pedido_id');
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/alteraObservacaoPedidoPP",
			async: false,
			data: { pedido_id	:  pedido_id,
					observacao 	: $(this).val()	}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			console.log(dados);
			if(dados.retorno == 'sucesso'){
				swal({
		   			title:"Ok!",
		   			text: "Observação alterada com sucesso.",
		   			type: 'success'
		    	}).then(function() {
		    		
		    		$("#info-pedido").modal('hide');
					$('#observacao_pedido').val('');
		    	}); 
			}

		});	
	});


	$('#alterar_dt').bind('click', function(){
		var dt_semana_prog = $('#dt_programacao').val();
		if( dt_semana_prog == '' ){
			swal({
	   			title:"Atenção!",
	   			text: "Preencha a nova data de alteração.",
	   			type: 'warning'
	    	}).then(function() {
	    		$('#dt_programacao').focus();
	    	}); 				
		}else{

			var orcamento_id 	= 	$(this).attr('orcamento_id');
			dt_programacao 				= 	dt_semana_prog.replace("/", "*");
			dt_programacao 				= 	dt_programacao.replace("/","*");
			
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/alteraDataInexisteOrcamento",
				async: true,
				data: {	orcamento_id 	:  	orcamento_id,
						dt_previsao 	: 	dt_programacao 	}
			}).done(function(data) {
				var dados = $.parseJSON(data);

				if(dados.retorno == 'sucesso'){
					swal({
			   			title:"OK!",
			   			text: "Data de programação alterada.",
			   			type: 'success'
			    	}).then(function() {
			    		location.reload(); 
			    	}); 	
				}else if(dados.retorno == 'data existente'){					
					swal({
			   			title:"Atenção!",
			   			text: "Semana já existente, arraste o pedido para a semana desejada.",
			   			type: 'warning'
			    	}).then(function() {
			    		location.reload(); 
			    	}); 
				}
			
			});
		}

	});

	/*$('.la-cut').bind('click', function(){
		var pedido_id 	= 	$(this).parents('span.pedidos').attr('pedido_id');
		var bicos 		= 	$(this).parents('span.pedidos').attr('bicos');
		var bombas 		= 	$(this).parents('span.pedidos').attr('bombas');
		var cliente 	= 	$(this).parents('span.pedidos').attr('title');
		var dt_semana_pedido 		= $(this).parents('span.pedidos').attr('semana');
		var programacao_semanal_id 	= $(this).parents('span.pedidos').attr('programacao_semanal_id');

		$('#titulo_divisao').empty();
		$('#titulo_divisao').append('Dividir Pedido <b>'+pedido_id+' - '+cliente+'</b>');
		$('#bicos_divisao').text(bicos);
		$('#bombas_divisao').text(bombas);		
		$('#dividir_pedido').attr('programacao_semanal_id',programacao_semanal_id);
		$('#dividir_pedido').attr('pedido_id',pedido_id);
		$('#dividir_pedido').attr('bicos',bicos);
		$('#dividir_pedido').attr('bombas',bombas);
		$('#dividir_pedido').attr('dt_semana_pedido',dt_semana_pedido);
		$("#dividir-pedido").modal({
			show: true
		});	

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/buscaModelosPedido", 
			async: true,
			data: { pedido_id 	: 	pedido_id	}		
		}).done(function(data) {
			var dados = $.parseJSON(data);
			var option = '';
			
			$('.modelo_1, .modelo_2').empty();

			for (var i = dados.length - 1; i >= 0; i--) {
				option+='<option value="'+dados[i].nr_bicos+'">'+dados[i].modelo+'</option>';
				
			}

			$('.modelo_1, .modelo_2').append(option);
			

		});	

	});
	// copia e cola o modelo e a qtd de bombas
	copiaModeloQtd(1);
	copiaModeloQtd(2);*/
	

	$('#visualizar_status').bind('click', function(){

		var pedido_id = $(this).attr('pedido_id');
		$('#pedido_id').val(pedido_id);
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/buscaNrSeriePorPedido",
			async: false,
			data: {	pedido_id :	pedido_id 	}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			$('#nr_serie_id').empty();
			$('#nr_serie_id').append(dados.combo);

		});

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/buscaStatusProducao",
			async: false,
			data: {	pedido_id :	pedido_id 	}			
		}).done(function(data) {
			var dados = $.parseJSON(data);
			$('#status_producao_id').empty();
			$('#status_producao_id').append(dados.combo);

		});

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/buscaStatusPedidosProducao",
			async: false,
			data: {	pedido_id :	pedido_id 	}			
		}).done(function(data) {
			var dados = $.parseJSON(data);
			$('#conteudo_status').empty();			
			$('#conteudo_status').append(dados.html);

		});

		$('#status-pedido').modal('show');
		
	});

	$('#enviar_status').bind('click', function(){
		var pedido_id 	= $('#pedido_id').val();
		var nr_serie_id = $('#nr_serie_id').val();
		var status_producao_id = $('#status_producao_id').val();
		var observacao 			= 	$('#observacao').val();
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/insereAndamentoProducao",
			async: true,
			data: {	pedido_id 			: 	pedido_id,
					nr_serie_id 		: 	nr_serie_id,
					status_producao_id 	: 	status_producao_id,
					observacao 			: 	observacao 	}
		}).done(function(data) {
			var dados = $.parseJSON(data);
			if(dados.retorno == 'sucesso'){
				swal({
		   			title:"Ok!",
		   			text: "Status da Bomba Modificada com sucesso",
		   			type: 'success'
		    	}).then(function() {
		    		window.location.href= base_url+"AreaAdministrador/programacaoProducao";
		    	}); 
			}

		});
	})
});

function verificaBicosBombasSemana( dt_semana, bicos, bombas, dt_semana_pedido ){
	var retorno = '';
	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/verificaBicosBombasDt",
		async: false,
		data: {	dt_semana_prog		: 	dt_semana,
				bicos 				: 	bicos,
				bombas 				: 	bombas,
				dt_semana_pedido 	:  	dt_semana_pedido}
	}).done(function(data) {
		var dados = $.parseJSON(data);
		if( dados.retorno === true){
			retorno = 0;
		}else{			
	    	retorno = 1;	
		}
	});
	console.log(retorno);
	return retorno;
}

function mostra_bombas_bicos(data_semana,semana){
	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministrador/buscaTotalBombasSemana",
		async: true,
		data: { data_semana :  data_semana,
				semana: semana	}
	}).done(function(data) { 
		var dados = $.parseJSON(data);
		console.log(dados);
		$('.modal-title-bibo').empty();
		$('.modal-title-bibo').text('Total de bombas e Bicos semana'+semana);
		$('#bicos_comercial').empty();
		$('#bicos_comercial').text(dados.total_comercial['bicos']);
		$('#bombas_comercial').empty();
		$('#bombas_comercial').text(dados.total_comercial['bombas']);
		$('#bicos_producao').empty();
		$('#bicos_producao').text('0');
		$('#bombas_producao').empty();
		$('#bombas_producao').text('0');
		if(dados.total_producao ){
			console.log('here')
			$('#bicos_producao').empty();
			$('#bicos_producao').text(dados.total_producao['bicos']);
			$('#bombas_producao').empty();
			$('#bombas_producao').text(dados.total_producao['bombas']);
		}
		$('#bombas-bicos').modal({
			show: true
		})
	});
}

function copiaModeloQtd(indice){
	$('#add_div_'+indice).bind('click', function(){
		var indice_atu 	= 	parseInt($(this).attr('indice')) + 1;
		$(this).attr('indice', indice_atu);
		var html = $('#copiar_'+indice).clone();
		$(html).find('select').attr('indice', indice_atu);
		$(html).find('input').attr('indice', indice_atu);
		$(html).find('.la-minus').attr('style','display: block;float: right;margin-right: 20px;margin-top: 10px;color: #ffcc00;font-weight: 600;');	
		$(html).find('.la-minus').attr('onclick','excluir('+indice_atu+')');
		$(html).find('.la-minus').attr('indice',indice_atu);
		var html_copia = '<div class="col-lg-12 col-xs-6" style="display: inline;">'+$(html).html()+'</div>';
		$('#colar_'+indice).append(html_copia);

	});
}

function excluir(indice){
	$('select[indice="'+indice+'"]').fadeOut('slow', function() {
		$(this).remove();
	});

	$('input[indice="'+indice+'"]').fadeOut('slow', function() {
		$(this).remove();
	});

	$('i.la-minus[indice="'+indice+'"]').fadeOut('slow', function() {
		$(this).remove();
	});
}