$(document).ready(function(){
 	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
 	$('.moeda').mask('000.000.000,00', {reverse: true});
 	$('#html_table').DataTable({

    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {

			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },

		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }

		}

	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('table thead').css('background-color', '#f4f3f8');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box; width: 100% !important; padding-right: 0px;')
	$('.no-footer').attr('style','margin:0px;');
	
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	$('#empresa').mask('99.999.999/9999-99');

	$('#salvar').bind('click', function(){
		
		if( $('#empresa_id').val() != '' && $('#empresa_id').val() != undefined){
			$('#solicitarCartao').modal('hide');
			var empresa_id 	= 	$('#empresa_id').val();
			var nome 		= 	$('#nome').val();
			var usuario_id 	= 	$('#usuario_id').val();
			
			$.ajax({
				method: "POST",
				url:  base_url+"AreaRepresentantes/solicitarCartaoFuncionario", 
				async: true,
				data: { nome 		: 	nome,
						usuario_id 	: 	usuario_id,
						empresa_id 	: 	empresa_id }
			}).done(function(data) {
				var dados = $.parseJSON(data);
				
				if(dados.retorno == 'sucesso'){
					swal({
			   			title: "OK!",
			   			text: "Cartão solicitado com sucesso!",
			   			type: 'success'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaRepresentantes/gestaoFuncionarios';
			    	}); 
				}
			});	

		}else{

			swal({
				title: "Atenção!",
				text: 'Selecione um posto cadastrado!',
				type: "warning"
			}).then(function() {
		      	
		   	});
			return false;
		}
	});

	/* autocomplete responsável */
	$( "#empresa" ).autocomplete({

		source: base_url+"AreaRepresentantes/retornaEmpresas",
		minLength: 9,
		select: function( event, ui ) {
			$('#empresa').attr('maxlength','550');
			$('#empresa_id').val(ui.item.id);
		},
		response: function (event, ui) {
	        if (!ui.content.length) {
	            swal({
					title: "Atenção!",
					text: 'Nenhum CNPJ encontrado em nosso banco de dados, verifique os dados e tente novamente',
					type: 'warning'
				}).then(function() {					
					$('#empresa').val('');
				});

	        }

	    }
    });

});

function solicitaCartao(usuario_id, nome){ 
	$('#solicitarCartao').modal({show: true});
	$('#titulo_modal').text(nome);
	$('#nome').val(nome);
	$('#usuario_id').val(usuario_id);
}

function excluirFuncionario(usuario_id){
	
		var id = usuario_id;
		swal({
		  title: 'Excluir?',
		  text: "Deseja realmente excluir esse usuário?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Sim!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					method: "POST",
					url:  base_url+"AreaRepresentantes/excluirFuncionario", 
					async: false,
					data: { id 	: id }
				}).done(function(data) {
					var dados = $.parseJSON(data);
					
					if(dados.retorno == 'sucesso'){
						swal({
				   			title: "OK!",
				   			text: "Funcionário Excluído com sucesso",
				   			type: 'success'
				    	}).then(function() {
				    	   	window.location = base_url+'AreaRepresentantes/gestaoFuncionarios';
				    	}); 
					}
				});	
			}
		})
	
}