var table;
$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    var table = $('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

	/*$(	"#usuarios" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaUsuariosWertco",
		minLength: 3,
		select: function( event, ui ) {
			$('#rel_chamados').slideDown('slow');
			$("#usuario_id").val(ui.item.usuario_id);
	
		}
    });*/


    /*$('#enviar').bind('click', function(){
    	
    	    	
    	var empresa_id = $('#empresa_id').val();
	    $.ajax({
			method: "POST",
			url: base_url+"AreaAdministrador/areaAtuacaoAdmAjax",
			async: true,
			data: { empresa_id : empresa_id	}
		}).done(function( data ) {
			var dados = $.parseJSON(data);
			$('#relatorio').empty();
			$('#relatorio').append(dados.retorno);
			datatable();

		});
	});	*/


	

});


function datatable(){

	table = $('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');
	
	var botao_excluir =  '<a href="#" class="btn btn-outline-danger m-btn btn-sm  m-btn--icon" id="desaprovar_todos">';
		botao_excluir+=  '	<span>';
		botao_excluir+=  '		<i class="la la-warning"></i>';
		botao_excluir+=  '			<span>';
		botao_excluir+=  '				Desaprovar Todas';
		botao_excluir+=  '			</span>';
		botao_excluir+=  '	</span>';
		botao_excluir+=  '</a>';


	var botao_aprovar =  '<a href="#" class="btn btn-outline-success m-btn btn-sm  m-btn--icon" style="margin-right: 20px;" id="aprovar_todos">';
		botao_aprovar+=  '	<span>';
		botao_aprovar+=  '		<i class="la la-check"></i>';
		botao_aprovar+=  '			<span>';
		botao_aprovar+=  '				Aprovar Todas';
		botao_aprovar+=  '			</span>';
		botao_aprovar+=  '	</span>';
		botao_aprovar+=  '</a>';
		botao_aprovar+=  '<img src="'+base_url+'bootstrap/img/loading.gif" width="70" style="display: none;" id="loading" >';	 
	$('#html_table_filter').after(botao_excluir);
	$('#html_table_filter').after(botao_aprovar);

}
