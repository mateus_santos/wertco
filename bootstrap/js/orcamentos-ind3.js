$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 0, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});

	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');

	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');
	$('div.dataTables_scrollHeadInner').attr('style','box-sizing: content-box; width: 100% !important; padding-right: 0px;');
	$('table.dataTable').attr('style','margin-left: 0px;');
	
	$('.valor_unitario').mask('#.##0,00', {reverse: true}); 

	$("#alterar_status").bind('click', function(){

		orcamento_id 		= 	$(this).attr('orcamento_id');
		status_orcamento	= 	$("#status_orcamento").val(); 
		$.ajax({
			method: "POST",
			url:  base_url+"AreaIndicadores3/alteraStatusOrcamento", 
			async: true,
			data: { orcamento_id 		: 	orcamento_id,
					status_orcamento 	: 	status_orcamento}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso')
			{				
				swal({
		   			title: "OK!",
		   			text: "Status do orçamento atualizado com sucesso",
		   			type: 'success'
		    	}).then(function() {
		    	   	window.location = base_url+'AreaIndicadores3/orcamentos';
		    	}); 
			}
		});		
	});

	$("#adicionar_andamento").bind('click', function(){

		orcamento_id 		= 	$(this).attr('orcamento_id');
		status_orcamento_id	= 	$(this).attr('status_orcamento_id'); 
		andamento			= 	$("#andamento_texto").val();
		if( andamento == '')
		{
			swal({
	   			title: "Atenção!",
	   			text: "Insira alguma observação para ser adicionada ao processo desta venda!",
	   			type: 'warning'
		    }); 

		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaIndicadores3/insereAndamentoOrcamento", 
				async: true,
				data: { orcamento_id 		: 	orcamento_id,
						status_orcamento_id	: 	status_orcamento_id,
						andamento 			: 	andamento}
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{				
					swal({
			   			title: "OK!",
			   			text: "Observação adicionada ao andamento do orçamento #"+orcamento_id,
			   			type: 'success'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaIndicadores3/orcamentos';
			    	}); 
				}
			});		

		}
	});

	/* autocomplete responsável */
	$( "#responsavel" ).autocomplete({
		source: base_url+"AreaIndicadores3/retornaRepresentantes",
		minLength: 2,
		select: function( event, ui ) {
			$('#responsavel_id').val(ui.item.id);
		}
    });

    $("#alterar_responsavel").bind('click', function(){
    	orcamento_id 	=	$(this).attr('orcamento_id');
		responsavel_id 	= 	$("#responsavel_id").val();
		orcamento_responsavel_id = $(this).attr('responsavel_id');	
		status_orcamento_id = $(this).attr('status_orcamento_id');	

		if(responsavel_id == "")
		{
			swal({
	   			title: "Atenção!",
	   			text: "Selecione um representante!",
	   			type: 'warning'
		    }); 

		}else{
			
			$.ajax({
				method: "POST",
				url:  base_url+"AreaIndicadores3/alteraResponsavelOrcamento", 
				async: true,
				data: { orcamento_id 				: 	orcamento_id,
						responsavel_id				: 	responsavel_id,
						orcamento_responsavel_id 	: 	orcamento_responsavel_id,
						status_orcamento_id			: 	status_orcamento_id						}
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{				
					swal({
			   			title: "OK!",
			   			text: "Responsável por dar continuidade ao orçamento foi adicionado com sucesso.",
			   			type: 'success'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaIndicadores3/orcamentos';
			    	}); 
				}
			});		
		}	
    });

    $("#tour_virtual").bind('click', function(){
    	
    	if($('.status').length > 0){

	    	var tour;
	  
			tour = new Shepherd.Tour({
			  defaults: {
			    classes: 'shepherd-element shepherd-open shepherd-theme-arrows',
			    scrollTo: true
			  }
			});

			tour.addStep('primeiro-step', {
			  text: 'Área onde se encontram todos os orçamentos de sua responsabilidade.',
			  attachTo: '.m-datatable top',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.status click',
			  showCancelLink: true,
			  buttons: [
			    {
			      text: 'Próximo', 
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('segundo-step', {
			  text: 'Aqui você pode alterar o status do orçamento. Basta clicar no botão e selecionar o próximo passo da negociação.',
			  attachTo: '.status top',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.andamento click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Próximo',
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('terceiro-step', {
			  text: 'Neste botão é possível visualizar o andamento da negociação e adicionar algum comentário/observação (FOLLOW UP) a esta negociação. <br/>Lembrando que apenas você e o responsável Comercial da WERTCO, terão acesso ao andamento do mesmo.',
			  attachTo: '.andamento_tour top',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.responsavel_tour click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Próximo',
			      action: tour.next
			    }
			  ]
			});			

			tour.addStep('quinto-step', {
			  text: 'Aqui pode ser visualizado o orçamento completo.<br/> Além disso, pode-se editar, imprimir, gerar desconto<br/> e emitir o orçamento para o cliente final.',
			  attachTo: '.visualizar left',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.cadastrar_orcamento click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Próximo',
			      action: tour.next
			    }
			  ]
			});

			tour.addStep('sexto-step', {
			  text: 'Clique neste botão para cadastrar um novo orçamento no sistema.',
			  attachTo: '.cadastrar_orcamento top',
			  classes: 'shepherd-theme-arrows',
			  advanceOn: '.cadastrar_orcamento click',
			  showCancelLink: true,
			  buttons: [		    
			    {
			      text: 'Anterior',
			      action: tour.back
			    },
			    {
			      text: 'Finalizar',
			      action: tour.next
			    }
			  ]
			});
			
			tour.start();
		}
    });

});

function status(orcamento_id){
	
	$('.modal-status-title').text('Alterar Status Orçamento #'+ orcamento_id);
	$('#alterar_status').attr('orcamento_id', orcamento_id);
	$("#m_modal_6").modal({
	    show: true
	});
	

	//orçamento dos representantes
	$.ajax({
		method: "POST",
		url:  base_url+"AreaIndicadores3/buscaProximosStatus", 
		async: true,
		data: { orcamento_id 	: 	orcamento_id }
	}).done(function(data) {
		var dados = $.parseJSON(data);		
		var combo = "";

		for(var i=0;i<dados.length;i++){
			if(dados[i].id != 5){
				combo+="<option value='"+dados[i].id+"'>"+dados[i].descricao.charAt(0).toUpperCase() + dados[i].descricao.slice(1)+"</opton>";
			}	
		}

		$('#status_orcamento').append(combo);

	});
}

function andamento(orcamento_id,status_orcamento_id){
	
	$('.modal-andamento-title').text('Andamento orçamento #'+ orcamento_id);
	$('#adicionar_andamento').attr('orcamento_id', orcamento_id);
	$('#adicionar_andamento').attr('status_orcamento_id', status_orcamento_id);

	$("#m_modal_7").modal({
	    show: true
	});

	$.ajax({
		method: "POST",
		url:  base_url+"AreaIndicadores3/buscaAndamentos", 
		async: true,
		data: { orcamento_id 	: 	orcamento_id}
	}).done(function(data) {

		var dados = $.parseJSON(data);	
		$('.m-list-timeline__item').remove();				
		var html="";

		if( dados.length > 0 ){

			for( var i=0;i<dados.length;i++ ){ 
				var tipo = '';
				if( dados[i]['status_orcamento_id'] == 1)
				{
					tipo = 'secondary';
				}else if( dados[i]['status_orcamento_id'] == 2 ){
					tipo = 'success';
				}else if( dados[i]['status_orcamento_id'] == 3 ){
					tipo = 'danger';

				}else if( dados[i]['status_orcamento_id'] == 4 ){
					tipo = 'warning';

				}else if( dados[i]['status_orcamento_id'] == 5 ){
					tipo = 'info';

				}else if( dados[i]['status_orcamento_id'] == 6 ){
					tipo = 'primary';

				}

				var data_hora = dados[i]['dthr_andamento'].split(' ');
				var data = data_hora[0].split('-');
				var hora = data_hora[1];
				var data_andamento = data[2]+'/'+data[1]+'/'+data[0];
				html+= '<div class="m-list-timeline__item">';
                html+='<span class="m-list-timeline__badge m-list-timeline__badge--'+tipo+'"></span>';
                html+=' <span class="m-list-timeline__text">'+dados[i]['andamento']+'</span>';
                html+=' <span class="m-list-timeline__time">'+dados[i]['usuario']+' '+data_andamento+' '+hora+'</span>';
                html+='</div>';
            }

            $('.m-list-timeline__items').append(html);
		}else{
			$('.m-list-timeline__item').remove();
		}
	});

	
}

/*function responsavel(orcamento_id,responsavel_id, status_orcamento_id){
	
	$('.modal-responsavel-title').text('Alterar/Adicionar Responsável pelo Orçamento #'+ orcamento_id);
	$('#alterar_responsavel').attr('orcamento_id', orcamento_id);
	$('#alterar_responsavel').attr('responsavel_id', responsavel_id);
	$('#alterar_responsavel').attr('status_orcamento_id', status_orcamento_id);
	$("#m_modal_8").modal({
	    show: true
	});

}*/


