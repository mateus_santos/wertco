$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

function excluirFuncionario(usuario_id){

	var id = usuario_id;
	swal({
	  title: 'Excluir?',
	  text: "Deseja realmente excluir esse usuário?",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Sim!'
	}).then((result) => {

		if (result.value) {
			$.ajax({
				method: "POST",
				url:  base_url+"AreaRepresentantes/excluirFuncionario", 
				async: false,
				data: { id 	: id }
			}).done(function(data) {
				var dados = $.parseJSON(data);
				
				if(dados.retorno == 'sucesso'){
					swal({
			   			title: "OK!",
			   			text: "Funcionário Excluído com sucesso",
			   			type: 'success'
			    	}).then(function() {
			    	   	window.location = base_url+'AreaRepresentantes/gestaoFuncionarios';
			    	}); 

				}

			});	
		}
	})

}