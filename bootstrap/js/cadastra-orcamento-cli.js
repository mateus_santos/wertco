$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('.add').bind('click', function(){

        var html = $('#modelo').clone();                
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
        $(html).find('.bombas').attr('indice', indice);
        $(html).find('.valor_unitario').attr('indice', indice);
        $(html).find('.valor_unitario').text('');
        $(html).find('b.valor_unitario').addClass('valor_unitario_'+indice);
        $(html).find('input.valor_unitario').attr('id', 'valor_unitario_'+indice);

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();                
        body+="</tr>";                
        $('.table').append(body);                
        $('#modelo').attr('total_indice',indice);

        excluir();
        mostra_valor();        
    });
    
    $('#cnpj').mask('00.000.000/0000-00');
    $('#cpf').mask('000.000.000-00');
    $('#bombas').mask('00000'); 
    $('#bicos').mask('00000');  
    $('.qtd').mask('000');
    $('#cep').mask('00000-000');  

    $('.bombas').bind('change', function(){
        
        $('b.valor_unitario').text($(this).find(':selected').attr('valor_unitario'));  
        $('#valor_unitario_0').val($(this).find(':selected').attr('valor_unitario'));  
        $('.valor_unitario_0').mask('#.##0,00', {reverse: true});         
    }); 
    
    $('#cnpj').bind('focusout', function(){
        var cnpj = $(this).val(); 
        if(cnpj != ""){

            $.ajax({
                method: "POST",
                url: base_url+'clientes/verificaOrcamento',
                async: true,
                data: { cnpj    :   cnpj }
                }).done(function( data ) {
                    var dados = $.parseJSON(data);
                    if( dados.total == 0){

                        $.ajax({
                            method: "POST",
                            url: base_url+'clientes/verifica_cnpj',
                            async: false,
                            data: { cnpj    :   cnpj }
                            }).done(function( data ) {
                                var dados = $.parseJSON(data);                          
                                if(dados.length > 0){
                                    
                                    $('#razao_social').val(dados[0].razao_social);      
                                    $('#razao_social').attr('disabled', true);      
                                    $('#fantasia').val(dados[0].fantasia);      
                                    $('#fantasia').attr('disabled', true);
                                    $('#telefone').val(dados[0].telefone);
                                    $('#telefone').attr('disabled', true);
                                    $('#endereco').val(dados[0].endereco);      
                                    $('#endereco').attr('disabled', true);                                  
                                    $('#email').attr('disabled', true);
                                    $('#email').val(dados[0].email);
                                    $('#cidade').attr('disabled', true);
                                    $('#cidade').val(dados[0].cidade);  
                                    $('#estado').attr('disabled', true);
                                    $('#estado').val(dados[0].estado);                        
                                    $('#pais').attr('disabled', true);
                                    $('#pais').val(dados[0].pais);                        
                                    $('#salvar').val('0');
                                    $('#empresa_id').val(dados[0].id);
                                    
                                }else{

                                    $('#razao_social').attr('disabled', false);     
                                    $('#razao_social').val('');
                                    $('#fantasia').attr('disabled', false);
                                    $('#fantasia').val('');
                                    $('#telefone').attr('disabled', false);
                                    $('#telefone').val('');
                                    $('#endereco').attr('disabled', false); 
                                    $('#endereco').val('');                                
                                    $('#email').attr('disabled', false); 
                                    $('#email').val('');
                                    $('#cidade').attr('disabled', false); 
                                    $('#cidade').val('');  
                                    $('#estado').attr('disabled', false); 
                                    $('#estado').val('');                        
                                    $('#pais').attr('disabled', false);
                                    $('#pais').val('');         
                                    $('#salvar').val('1');

                                }                           
                        });  
                    }else{

                        swal({
                            title: "Atenção!",
                            text: "Já Existe um orçamento em andamento para este cliente, entre em contato com a WERTCO!",
                            type: 'warning'
                        }).then(function() {
                            $('#cnpj').val('');
                        }); 
                   }
                });


        
        }
    });

});
	
	
function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');                
        $('tr[indice="'+indice+'"]').remove(); 
    });    
}

function mostra_valor()
{
    $('.bombas').bind('change', function(){
        indice = $(this).attr('indice');        
        $('.valor_unitario_'+indice).text($(this).find(':selected').attr('valor_unitario'));  
        $('#valor_unitario_0'+indice).val($(this).find(':selected').attr('valor_unitario'));  
        $('.valor_unitario_0'+indice).mask('#.##0,00', {reverse: true});         
        
    }); 

}