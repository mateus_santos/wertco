$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
function excluirFormaPagto(id){

		var id = id;
		swal({
		  title: 'Excluir?',
		  text: "Deseja realmente excluir esse Forma de Pagto?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Sim!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					method: "POST",
					url:  base_url+"AreaAdministrador/excluirFormaPagto", 
					async: false,
					data: { id 	: id }
				}).done(function(data) {
					var dados = $.parseJSON(data);		
					
					if(dados.retorno == 'sucesso'){				
						swal({
				   			title: "OK!",
				   			text: "Forma de Pagto Excluído com sucesso",
				   			type: 'success'
				    	}).then(function() {
				    	   	window.location = base_url+'AreaAdministrador/formaPagto';
				    	}); 
					}
				});	
			}
		})
	
}