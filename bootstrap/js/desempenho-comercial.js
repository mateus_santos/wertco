$(document).ready(function(){

	now = new Date();
	mes = (now.getMonth() > 8 )  ? now.getMonth() : '0' + (now.getMonth() + 1);
	ano = now.getFullYear();
	$.ajax({
        method: "POST",
        url: base_url+'AreaAdministrador/buscaDadosFunilMesAno',
        async: true,
        data: { mes 	: mes,
        		ano 	: ano }
    }).done(function( data ) {
        var dados = $.parseJSON(data);
        funilMesAno(dados.abertos, dados.negociacao, dados.fechados);
	});

	$('#gera_funil').bind('click', function (){
		$('#chart-area').remove();
		html = '<canvas id="chart-area" height=""></canvas>';
		$('#canvas-holder').append(html);

		var mes = $('#mes').val();
		var ano = $('#ano').val();
		$.ajax({
	        method: "POST",
	        url: base_url+'AreaAdministrador/buscaDadosFunilMesAno',
	        async: true,
	        data: { mes 	: mes,
	        		ano 	: ano }
	    }).done(function( data ) {
	        var dados = $.parseJSON(data);
	        
	        funilMesAno(dados.abertos, dados.negociacao, dados.fechados)
		});		

	});

	$.ajax({
        method: "POST",
        url: base_url+'AreaAdministrador/buscaDadosFunilMesAnoOrcamento',
        async: true,
        data: { mes 	: mes,
        		ano 	: ano }
    }).done(function( data ) {
        var dados = $.parseJSON(data);
        funilMesAnoOrcamento(dados.abertos, dados.negociacao, dados.fechados);
	});

	$('#gera_funil_orcamento').bind('click', function (){
		$('#chart-area-orcamento').remove();
		html = '<canvas id="chart-area-orcamento" height=""></canvas>';
		$('#canvas-holder-orcamento').append(html);

		var mes = $('#mes_orcamento').val();
		var ano = $('#ano_orcamento').val();
		$.ajax({
	        method: "POST",
	        url: base_url+'AreaAdministrador/buscaDadosFunilMesAnoOrcamento',
	        async: true,
	        data: { mes 	: mes,
	        		ano 	: ano }
	    }).done(function( data ) {
	        var dados = $.parseJSON(data);
	        
	        funilMesAnoOrcamento(dados.abertos, dados.negociacao, dados.fechados)
		});

	});

	$.ajax({
        method: "POST",
        url: base_url+'AreaAdministrador/buscaDadosFunilMesAnoPedidos',
        async: true,
        data: { mes 	: mes,
        		ano 	: ano }
    }).done(function( data ) {
        var dados = $.parseJSON(data);
        funilMesAnoPedidos(dados.abertos, dados.negociacao, dados.fechados);
	});

	$('#gera_funil_pedidos').bind('click', function (){
		$('#chart-area-pedidos').remove();
		html = '<canvas id="chart-area-pedidos" height=""></canvas>';
		$('#canvas-holder-pedidos').append(html);

		var mes = $('#mes_pedidos').val();
		var ano = $('#ano_pedidos').val();
		$.ajax({
	        method: "POST",
	        url: base_url+'AreaAdministrador/buscaDadosFunilMesAnoPedidos',
	        async: true,
	        data: { mes 	: mes,
	        		ano 	: ano }
	    }).done(function( data ) {
	        var dados = $.parseJSON(data);	        
	        funilMesAnoPedidos(dados.abertos, dados.negociacao, dados.fechados)
		});

	});

});

function funilMesAno(abertos, negociacao, fechados){

	var config = {
		type: 'funnel',
		data: {
			datasets: [{
				data: [fechados, negociacao, abertos],
				backgroundColor: [
					"#28a745",
					"#ffcc00",
					"#c9cbd5"
				],
				hoverBackgroundColor: [
					"#28a745",
					"#ffcc00",
					"#c9cbd5"
				]
			}],
			labels: [
				"Fechados",
				"Em Negociação",
				"Abertos"
			]
		},
		options: {
			responsive: true,
			sort: 'desc',
			maintainAspectRatio: false,			
			legend: {
				position: 'top'
			},
			title: {
				display: false,
				text: 'Funil de vendas'
			},
			animation: {
				animateScale: true,
				animateRotate: true
			}
		}
	};


	var ctx = document.getElementById("chart-area").getContext("2d");
	window.myDoughnut = new Chart(ctx, config);

}

function funilMesAnoOrcamento(abertos, negociacao, fechados){

	var config = {
		type: 'funnel',
		data: {
			datasets: [{
				data: [fechados, negociacao, abertos],
				backgroundColor: [
					"#28a745",
					"#ffcc00",
					"#c9cbd5"
				],
				hoverBackgroundColor: [
					"#28a745",
					"#ffcc00",
					"#c9cbd5"
				]
			}],
			labels: [
				"Fechados",
				"Em Negociação",
				"Abertos"
			]
		},
		options: {
			responsive: true,
			sort: 'desc',
			maintainAspectRatio: false,			
			legend: {
				position: 'top'
			},
			title: {
				display: false,
				text: 'Funil de vendas - Orçamentos'
			},
			animation: {
				animateScale: true,
				animateRotate: true
			}
		}
	};


	var ctx = document.getElementById("chart-area-orcamento").getContext("2d");
	window.myDoughnut = new Chart(ctx, config);

}

function funilMesAnoPedidos(abertos, negociacao, fechados){

	var config = {
		type: 'funnel',
		data: {
			datasets: [{
				data: [fechados, negociacao, abertos],
				backgroundColor: [
					"#28a745",
					"#ffcc00",
					"#c9cbd5"
				],
				hoverBackgroundColor: [
					"#28a745",
					"#ffcc00",
					"#c9cbd5"
				]
			}],
			labels: [
				"Fechados",
				"Em Negociação",
				"Abertos"
			]
		},
		options: {
			responsive: true,
			sort: 'desc',
			maintainAspectRatio: false,			
			legend: {
				position: 'top'
			},
			title: {
				display: false,
				text: 'Funil de vendas - Pedidos'
			},
			animation: {
				animateScale: true,
				animateRotate: true
			}
		}
	};


	var ctx = document.getElementById("chart-area-pedidos").getContext("2d");
	window.myDoughnut = new Chart(ctx, config);

}