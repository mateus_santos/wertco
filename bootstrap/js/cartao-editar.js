$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	/* autocomplete responsável */
	$( "#usuario" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaTecnicos",
		minLength: 1,
		select: function( event, ui ) {
			$('#usuario_id').val(ui.item.id);
		}
    });
});

