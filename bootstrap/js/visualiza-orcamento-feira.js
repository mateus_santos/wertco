$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

	$('.valor_unitario').mask('#.##0,00', {reverse: true}); 
	$('#valor_desconto').mask('00,00', {reverse: true}); 
	$('#valor_desconto_orc').mask('00,00', {reverse: true}); 
	
	if( $('#valor_desconto').val() != "" ){
		$('#valor_desconto').attr('disabled',true);
	}

	$('.editar_produto').bind('click', function(){
		var indice = $(this).attr('indice');
		$('#valor_produto_'+indice).removeAttr('disabled');
	});

	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

	//caso não seja da zona do usuário, bloqueia os campos
	/*if($('#zona_atuacao').val() == 0){
		$('input').attr('disabled', true);
		$('textarea').attr('disabled', true);
		$('select').attr('disabled', true);
		$('.la-edit').remove();
		$('.la-plus').remove();		
		$('.excluir_produto').remove();
		$('#motivo_emissao').attr('disabled', false);
	}*/

	if( $('#status').attr('status') == 'fechado' || $('#status').attr('status') == 'perdido' || $('#status').attr('status') == 'cancelado'){
		$('select').attr('disabled',true);
		$('input').attr('disabled',true);
		$('.excluir_produto').hide();
		$('a[title="Adicionar produto"]').hide();
		$('#emitirOrcamento').hide();
	}

	$('.valor_unitario').mask('#.##0,00', {reverse: true}); 
	$('#valor_desconto').mask('00,00', {reverse: true}); 
	$('#valor_desconto_orc').mask('00,00', {reverse: true}); 
	
	if( $('#valor_desconto').val() != "" ){
		$('#valor_desconto').attr('disabled',true);
	}
	
	if(	$('#comissao').val() == '0.01'){
		$('.valor_produto').attr('type', 'text');
		$('.valor_produto').attr('disabled', false);
		$('.valor_produto').addClass('form-control');
		$('.valor_produto').mask('#.##0,00', {reverse: true}); 
		$('.valor_unitario').hide();
		$('#aprovacao').show();
	}

	$('.bombas').bind('change', function(){

        indice 					= 	$(this).attr('indice');
        produto_id 				= 	$(this).val();
        orcamento_produto_id 	= 	$(this).attr('orcamento_produto_id');
        orcamento_id 			= 	$('#orcamento_id').val();
        status_id				= 	$('#status_id').val();
        produto 				= 	$(this).find('option:selected').text();
        produto_nr 				= 	indice;
        valor_base 				= 	$(this).find('option:selected').attr('valor_unitario');
        comissao 				= 	$("input[name='valor_desconto_orc']:checked").val();
		valor_tributo 			= 	$("#valor_tributo").val();

		 if($(this).val() == '14' || $(this).val() == '68' || $(this).val() == '70' || $(this).val() == '15' || $(this).val() == '16' || $(this).val() == '17' || 
            $(this).val() == '18' || $(this).val() == '19' || $(this).val() == '20' || $(this).val() == '21' || $(this).val() == '22' || $(this).val() == '23' || 
            $(this).val() == '24' || $(this).val() == '170' || $(this).val() == '26' || $(this).val() == '29' || $(this).val() == '34' || $(this).val() == '35' || 
            $(this).val() == '36' || $(this).val() == '37' || $(this).val() == '38' || $(this).val() == '39' || $(this).val() == '45' || $(this).val() == '45' || 
            $(this).val() == '46' || $(this).val() == '47' || $(this).val() == '64' || $(this).val() == '66' || $(this).val() == '69' || $(this).val() == '200' ||         
            $(this).val() == '289' || $(this).val() == '295' || $(this).val() == '296' || $(this).val() == '297' || $(this).val() == '298' || $(this).val() == '308')
        {
            $('#comissao2').fadeOut('slow');
        }else{
            $('#comissao2').fadeIn('slow');
        }


        if( $(this).find('option:selected').attr('tipo') != 'opcionais' && comissao != 'outro' ){
	       
	        if( comissao == '3' && valor_tributo == '7.00' ) {
                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '10.00' ) {
                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11) * 1.12).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 4% de comissão ***********
            // ************************************
            }else if( comissao == '4' && valor_tributo == '7.00' ) {
                
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '10.00' ) {
                
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11) *1.12).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '12.00') {
                
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '18.00') {

               	var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 5% de comissão ***********
            // ************************************
            }else if(comissao == '5' && valor_tributo == '7.00'  ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '10.00'  ) {

                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 2% de comissão ***********
            // ************************************
            }else if(comissao == '2' && valor_tributo == '7.00') {

                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '10.00') {

                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '12.00') {

               
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '18.00') {

               
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 0% de comissão ***********
            // ************************************
            }else if(comissao == '0' && valor_tributo == '7.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '10.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '12.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '18.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            }else if(comissao == 'TB2') {
			    
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())).toFixed(2);
            }
	        
	        // Acréscimo de 20% sobre o valor final quando for para o nordeste
	        /*if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
	            $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
	            $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
	            $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
	        {
	            valor_produto = valor_produto * 1.2;
	            
	        }*/

	        $('b.valor_unitario_'+indice).text(valor_produto);
	        $('#valor_produto_'+indice).val(valor_produto);
			$('b.valor_unitario_'+indice).unmask();
	        $('b.valor_unitario_'+indice).mask('#.##0,00', {reverse: true});

	    }else{

	        
	        var valor_produto = valor_base;
	        // Acréscimo de 20% sobre o valor final quando for para o nordeste
		    /*if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
		        $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
		        $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
		        $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
		    {
		        valor_produto = valor_produto * 1.2;
		
		    }*/


	    	$('b.valor_unitario_'+indice).text(valor_produto);
	        $('#valor_produto_'+indice).val(valor_produto);
			$('b.valor_unitario_'+indice).unmask();
			$('b.valor_unitario_'+indice).mask('#.##0,00', {reverse: true});
	    }
	    
	    
	    
		$.ajax({
			method: "POST",
			url:  base_url+"AreaFeira/alterarProdutoOrcamento", 
			async: false,
			data: { produto_id 				: 	produto_id,
					orcamento_produto_id 	: 	orcamento_produto_id,
					status_orcamento_id 	: 	status_id,
					orcamento_id 			: 	orcamento_id,
					produto_nr 				: 	produto_nr,
					produto 				:  	produto,
					valor_produto 			: 	valor_produto
					}
		}).done(function(data) {

			var dados = $.parseJSON(data);
			
			if(dados.retorno == 'sucesso')
			{			
				atualizacao_dados = 1;	
				swal({
					title: "OK!",
					text: 'Produto alterado com sucesso!',
					type: "success"
				}).then(function() {

					$('#valor_produto_'+indice).css('border','2px solid green');
					$('#valor_produto_'+indice).removeAttr('disabled');
					$('#valor_produto_'+indice).focus();	
					window.location = base_url+'AreaFeira/visualizarOrcamento/'+orcamento_id;				
					
				});
			}
		});
    }); 

	$('.entregaFretePagto').bind('change', function(){
		var orcamento_id 		= 	$(this).attr('orcamento_id');
		var valor 				= 	$(this).val();
		var tipo 				= 	$(this).attr('id');	
		var status_orcamento_id	= 	$(this).attr('status_id');
		
		var url = '';
		if( tipo == 'forma_pagto' ){
			url = 'alterarFormaPagto';
			desc_tipo = 'Forma de Pagamento';
		}else if( tipo == 'entregas' ) {
			url = 'alterarEntrega';
			desc_tipo = 'Forma de Entrega';
		}else{
			url = 'alterarFrete';
			desc_tipo = 'Frete';
		}

		$.ajax({
			method: "POST",
			url:  base_url+"AreaFeira/"+url, 
			async: true,
			data: { valor 				: 	valor,
					orcamento_id 		: 	orcamento_id,
					status_orcamento_id	: 	status_orcamento_id}
		}).done(function(data) {
			var dados = $.parseJSON(data);

			if(dados.retorno == 'sucesso')
			{
				swal({
					title: "OK!",
					text: desc_tipo+' alterado com sucesso!',
					type: "success"
				});
			}
		});
	});

	$('.editar_qtd').bind('click', function(){
		var indice = $(this).attr('indice');
		$('#qtd_'+indice).removeAttr('disabled');
	});

	$('.qtd').bind('focusout', function(){
		
		var indice 					= 	$(this).attr('indice');
		var qtd 					= 	$(this).val();
		var orcamento_produto_id 	= 	$(this).attr('orcamento_produto_id');
		var orcamento_id 			= 	$('#orcamento_id').val();
        var status_id				= 	$('#status_id').val();
		$.ajax({
			method: "POST",
			url:  base_url+"AreaFeira/alterarQtdProduto", 
			async: true,
			data: { qtd 					: 	qtd,
					orcamento_produto_id 	: 	orcamento_produto_id,
					status_orcamento_id 	: 	status_id,
					orcamento_id 			: 	orcamento_id  }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso')
			{				
				$('#valor_produto_'+indice).attr('qtd', qtd);
			}
		});

	});	

	$('#observacao').bind('focusout', function(){

		var observacao	= 	$(this).val();
		var id			= 	$('#orcamento_id').val();
		
		$.ajax({
			method: "POST",
			url:  base_url+"AreaFeira/alterarObservacao",
			async: true,
			data: { observacao	: observacao,
					id 			: id }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso')
			{
				swal({
					title: "OK!",
					text: 'Observação alterada com sucesso!',
					type: "success"
				});
			}
		});

	});	


	$('.valor_produto').bind('focusout', function(){
		var valor_produto 			= 	$(this).val();
		var orcamento_id 			= 	$(this).attr('orcamento_id');
		var orcamento_produto_id 	= 	$(this).attr('orcamento_produto_id');
		var orcamento_id 			= 	$('#orcamento_id').val();
        var status_id				= 	$('#status_id').val();

		$.ajax({
			method: "POST",
			url:  base_url+"AreaFeira/alterarValorProduto", 
			async: true,
			data: { valor_produto 			: 	valor_produto,
					orcamento_produto_id 	: 	orcamento_produto_id,
					status_orcamento_id 	: 	status_id,
					orcamento_id 			: 	orcamento_id }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso')
			{				
				
			}
		});

	});	

	$('.atualiza_valor').bind('click', function(){
		var valor = 0;
		$('.valor_produto').each(function(){
			novo_valor = replaceAll($(this).val(), '.', '');
			novo_valor = replaceAll(novo_valor, ',', '.');
			
			valor = valor + (parseFloat(novo_valor) * parseInt($(this).attr('qtd')));
			
		});
		
		subtotal 	= 	valor.toFixed(2);
		valor_total = 	(valor + (parseFloat(valor.toFixed(2))/100 * parseFloat($('#valor_tributo').val())) + (parseFloat(valor.toFixed(2))/100  * 5)).toFixed(2);
		$('.valor_total').text(valor_total);
		$('.valor_subtotal').text(subtotal);
		$('.valor_total').mask('#.##0,00', {reverse: true}); 
		$('.valor_subtotal').mask('#.##0,00', {reverse: true});
	});


	$('#alterar_valor').bind('click', function(){
		orcamento_id 	= 	$(this).attr('orcamento_id');	
		status_id 		= 	$(this).attr('status_id');	
		valor_orcamento = 	$('#valor_orcamento').val();
			
		$.ajax({
			method: "POST",
			url:  base_url+"AreaFeira/alterarValorOrcamento", 
			async: true,
			data: { orcamento_id 	: 	orcamento_id,
					valor_orcamento : 	valor_orcamento,
					status_id 		: 	status_id}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso')
			{				
				swal({
						title: "OK!",
						text: 'Valor do Orçamento alterado com sucesso!',
						type: "success"
					}).then(function() {
				      	window.location = base_url+'AreaFeira/visualizarOrcamento/'+orcamento_id;
				   	});

			}else{		
				swal({
					title: "OPS!",
					text: 'Alguma inconsistência ocorreu, contate o webmaster!',
					type: "warning"
				}).then(function() {
			      	window.location = base_url+'AreaFeira/orcamentos';
			   	});
			}

		});		

	});

	$("#geraPdf").bind('click', function(){

		orcamento_id 	= 	$(this).attr('orcamento_id');	
		contato 		= 	$('#contato_posto').val();	
		representante	= 	$('#representante').val();

		if( contato != '' && representante != '' ) {

			$.ajax({
				method: "POST",
				url:  base_url+"AreaFeira/geraOrcamento/"+orcamento_id+"/ajax/"+contato+"/"+representante, 
				async: true,
				data: { orcamento_id 	: 	orcamento_id,
						contato 		: 	contato,
						representante	: 	representante			}
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno != 'erro')
				{				
					window.open(base_url+'pdf/'+dados.retorno);
				}

			});		

		}else{
			swal({
				title: "OPS!",
				text: 'Preencha todos os dados para continuar!',
				type: "warning"
			});
		}
	});

	$("#valor_desconto").bind('focusout', function(){
		
		if( $(this).val() != "" )
		{
			$("#motivo_desconto").removeAttr('disabled');
		}else{
			$("#motivo_desconto").attr('disabled',true);
		}
	});

	$("#valor_desconto_orc").bind('focusout', function(){
		
		if( $(this).val() != "" )
		{
			$("#motivo_desconto_orc").removeAttr('disabled');
		}else{
			$("#motivo_desconto_orc").attr('disabled',true);
		}
	});

	$("#emitir_orcamento").bind('click', function(){

		orcamento_id 		= 	$(this).attr('orcamento_id');
		email_destino		= 	$('#email_destino').val();		
		motivo_desconto		= 	$('#motivo_desconto').val();
		valor_tributo		= 	$('#valor_tributo').val();		
		valor_total			= 	$('#valor_total').val();
		status_orcamento_id	= 	$(this).attr('status_id');
		
		var erro = 0;

		if( email_destino == "" ){
			swal(
		  		'Ops!',
		  		'Insira um e-mail para ser enviado o orçamento!',
		  		'warning'
			);
			erro++;
		}

		var emailFilter=/^.+@.+\..{2,}$/;
		var illegalChars= /[\(\)\<\>\,\;\:\\\/\"\[\]]/		
		if(!(emailFilter.test(email_destino)) || email_destino.match(illegalChars))        
        {
	   		swal(
		  		'Ops!',
		  		'Insira um e-mail válido!',
		  		'warning'
			);
			erro++;
        }		

		if( erro == 0 ){
			
			$.ajax({
				method: "POST",
				url:  base_url+"AreaFeira/emitirOrcamento", 
				async: true,
				data: { orcamento_id 		: 	orcamento_id,
						email_destino		: 	email_destino,						
						valor_tributo 		: 	valor_tributo,
						valor_total 		: 	valor_total,
						status_orcamento_id : 	status_orcamento_id } 

			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{			
					
					swal({
						title: "OK!",
						text: 'Orçamento Emitido com sucesso!',
						type: "success"
					}).then(function() {
				      	window.location = base_url+'AreaFeira/orcamentos';
				   	});

				}else{
			
					swal({
						title: "OPS!",
						text: 'Alguma inconsistência ocorreu, contate o webmaster!',
						type: "warning"
					}).then(function() {
				      	window.location = base_url+'AreaFeira/orcamentos';
				   	});
				}

			});		
		}
	});

	$("#adicionar_desconto_orc").bind('click', function(){
		var valor_desconto = 0;
		$('input[name="valor_desconto_orc"]').each(function(){
			if($(this).is(':checked')){
				valor_desconto = $(this).val();
			}
		});
					
		var orcamento_id 		= 	$(this).attr('orcamento_id');
		var erro = 0;
		if(valor_desconto == ""){
			swal(
		  		'Ops!',
		  		'Insira um desconto para o orçamento!',
		  		'warning'
			);
			erro++;	
		}
				
		if( erro == 0 ){

			$.ajax({
				method: "POST",
				url:  base_url+"AreaFeira/insereDescontoOrcamento", 
				async: true,
				data: { orcamento_id 		: 	orcamento_id,
						valor_desconto 		: 	valor_desconto }

			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{				
					swal({
						title: "OK!",
						text: 'Desconto adicionado ao orçamento com sucesso!',
						type: "success"
					}).then(function() {
				      	window.location = base_url+'AreaFeira/visualizarOrcamento/'+orcamento_id;
				   	});
				}else{
					swal({
						title: "OPS!",
						text: 'Alguma inconsistência ocorreu, contate o webmaster!',
						type: "warning"
					}).then(function() {
				      	window.location = base_url+'AreaFeira/visualizarOrcamento/'+orcamento_id;
				   	});
				}

			});		
		}


	});

	$('.add').bind('click', function(){

        var html = $('#modelo').clone();                
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
        $(html).find('.bombas_new').attr('indice', indice);
        $(html).find('.bombas_new').attr('id', 'bombas_new_'+indice);
        $(html).find('.valor_unitarios').attr('indice', indice);        
        $(html).find('.valor_unitarios_text').attr('indice', indice);
        $(html).find('.valor_unitarios_text').text('');
        $(html).find('.valor_unitarios_text').addClass('valor_unitarios_text_'+indice);
        $(html).find('.valor_unitarios_text').removeClass('valor_unitarios_text_0');
        
        $(html).find('.valor_unitarios').addClass('valor_unitarios_'+indice);
        $(html).find('.valor_unitarios').removeClass('valor_unitarios_0');
        $(html).find('.valor_unitarios').attr('indice', indice);
        $(html).find('.valor_unitarios').attr('id', 'valor_unitarios_'+indice);

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();                
        body+="</tr>";                
        $('table.novo').append(body);                
        $('#modelo').attr('total_indice',indice);

        excluir();
        mostra_valor();        
    });
    
    mostra_valor();

    $('#adicionar_produto').bind('click', function(){
    	var dados 			= 	$('#produtos_novos').serializeArray();
    	orcamento_id 		= 	$(this).attr('orcamento_id');
    	status_orcamento_id = 	$(this).attr('status_id');
		var inputVazios = $('.novo').find('input').filter(function () {
        	return this.value.split(' ').join('') == '';
    	});

    	var selectVazios = $('.novo').find('select').filter(function () {
        	return this.value.split(' ').join('') == '';
    	});

		if( selectVazios.length > 0 || inputVazios.length > 0){
			swal({
				title: "Atenção!",
				text: 'Preencha corretamente os campos dos novos produtos!',
				type: "warning"
			});	
		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaFeira/insereProdutosNovosOrcamento", 
				async: true,
				data: { dados				: 	dados,
						orcamento_id 		: 	orcamento_id,
						status_orcamento_id : 	status_orcamento_id }

			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{
					swal({
						title: "OK!",
						text: 'Produto(s) adicionado(s) ao orçamento com sucesso!',
						type: "success"
					}).then(function() {
						cadastraEmissao(orcamento_id);
				      	window.location = base_url+'AreaFeira/visualizarOrcamento/'+orcamento_id;
				   	});
				}else{
					swal({
						title: "OPS!",
						text: 'Alguma inconsistência ocorreu, contate o webmaster!',
						type: "warning"
					}).then(function() {
				      	window.location = base_url+'AreaFeira/visualizarOrcamento/'+orcamento_id;
				   	});
				}
			});	
		}
	});

    if($('input[name="valor_desconto_orc"]:checked').val() == '2'){
            
            $('.bombas option[value="14"]').hide();                               
            $('.bombas option[value="68"]').hide();
            $('.bombas option[value="70"]').hide();
            $('.bombas option[value="15"]').hide();
            $('.bombas option[value="16"]').hide();
            $('.bombas option[value="17"]').hide();
            $('.bombas option[value="18"]').hide();
            $('.bombas option[value="19"]').hide();
            $('.bombas option[value="20"]').hide();
            $('.bombas option[value="21"]').hide();
            $('.bombas option[value="22"]').hide();
            $('.bombas option[value="23"]').hide();
            $('.bombas option[value="24"]').hide();
            $('.bombas option[value="170"]').hide();
            $('.bombas option[value="26"]').hide();
            $('.bombas option[value="29"]').hide();
            $('.bombas option[value="34"]').hide();
            $('.bombas option[value="35"]').hide();
            $('.bombas option[value="36"]').hide();
            $('.bombas option[value="37"]').hide();
            $('.bombas option[value="38"]').hide();
            $('.bombas option[value="39"]').hide();
            $('.bombas option[value="45"]').hide();
            $('.bombas option[value="46"]').hide();
            $('.bombas option[value="47"]').hide();
            $('.bombas option[value="64"]').hide();
            $('.bombas option[value="66"]').hide();
            $('.bombas option[value="69"]').hide();
            $('.bombas option[value="200"]').hide();
            $('.bombas option[value="289"]').hide();
            $('.bombas option[value="295"]').hide();
            $('.bombas option[value="296"]').hide();
            $('.bombas option[value="297"]').hide();
            $('.bombas option[value="298"]').hide();
            $('.bombas option[value="308"]').hide();
        }

	$('.comissao').bind('click', function(){
		console.log($(this).val());
		if($(this).val() == '2' && ($('.bombas').val() == '14' || $('.bombas').val() == "68" || $('.bombas').val() == "70" || $('.bombas').val() == "15" || $('.bombas').val() == "16" ||
            	$('.bombas').val() == "17" || $('.bombas').val() == "18" || $('.bombas').val() =="19" || $('.bombas').val() == "20" || $('.bombas').val() == "21" || 
            	$('.bombas').val() =="22"  || $('.bombas').val() == "23" || $('.bombas').val() =="24" || $('.bombas').val() == "170"|| $('.bombas').val() == "26" ||
            	$('.bombas').val() == "29" || $('.bombas').val() == "34" || $('.bombas').val() == "35" || $('.bombas').val() == "36" || $('.bombas').val() == "37" ||
            	$('.bombas').val() == "38" || $('.bombas').val() == "39" || $('.bombas').val() == 45 || $('.bombas').val() =="46" || $('.bombas').val() == "47" ||
            	$('.bombas').val() == "64" || $('.bombas').val() == "66" || $('.bombas').val() == "69" || $('.bombas').val() == "200" || $('.bombas').val() == "289" ||
                $('.bombas').val() == "295" || $('.bombas').val() =="296" || $('.bombas').val() == "297" || $('.bombas').val() == "298" || $('.bombas').val() =="308") )
        {
            	swal({
					title: "Atenção!",
					text: 'Esse produto está indisponível para essa valor de comissão.',
					type: "warning"
				}).then(function() {
						window.location = base_url+'AreaFeira/visualizarOrcamento/'+$('#orcamento_id').val();
				});
	            
			
        }else if( $(this).val() == 'outro' ){
			
			$('.valor_produto').attr('type', 'text');
			$('.valor_produto').attr('disabled', false);
			$('.valor_produto').addClass('form-control');
			$('.valor_produto').mask('#.##0,00', {reverse: true}); 
			$('.valor_unitario').hide();
			$('#aprovacao').show();
			
		}else{
			
			$('#aprovacao').hide();

			swal({
			  title: 'Alterar tabela?',
			  text: "Deseja realmente alterar a tabela de preços desses produtos?",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Sim!'
			}).then((result) => {
				if (result.value) {
					console.log($(this).val());
					atualiza_valores($(this).val());
					cadastraEmissao(orcamento_id);
				}
			});
		}
	});

	$( "#nome_indicador" ).autocomplete({
		source: base_url+"AreaFeira/retornaIndicadores",
		minLength: 2,
		select: function( event, ui ) {

			$('#nome_indicador').val(ui.item.id);
			var indicador 		= 	ui.item.id;
	    	var orcamento_id 	= 	$('#nome_indicador').attr('orcamento_id');

	    	$.ajax({
				method: "POST",
				url:  base_url+"AreaFeira/alteraIndicadorOrcamento", 
				async: true,
				data: { indicador		: 	indicador,
						orcamento_id 	: 	orcamento_id }

			}).done(function(data) {

				var dados = $.parseJSON(data);
				if(dados.retorno == 'sucesso'){
					swal({
						title: "OK!",
						text: 'Indicador atualizado com sucesso!',
						type: "success"
					});

					$('#alterar_indicador').text($('#nome_indicador').val());

				}else{
					swal({
						title: "OPS!",
						text: 'Acontenceu um erro inesperado, entre em contato com o webmaster!',
						type: "warning"
					});
				}
	    	});
		}
    });

	$('.excluir_indicador').bind('click', function(){
		indicador = '';
        orcamento_id = $(this).attr('orcamento_id');
        
        $.ajax({
			method: "POST",
			url:  base_url+"AreaFeira/alteraIndicadorOrcamento", 
			async: true,
			data: { indicador		: 	indicador,
					orcamento_id 	: 	orcamento_id }

		}).done(function(data) {

			var dados = $.parseJSON(data);
			if(dados.retorno == 'sucesso'){
				swal({
					title: "OK!",
					text: 'Indicador excluído com sucesso!',
					type: "success"
				});

				$("#alterar_indicador").text('NÃO HOUVE INDICAÇÃO PARA ESTE ORÇAMENTO');
			}else{
				swal({
					title: "OPS!",
					text: 'Acontenceu um erro inesperado, entre em contato com o webmaster!',
					type: "warning"
				});
			}
    	});	
	});

	$('.indicacao').bind('click', function(){
        if($(this).is(':checked')){
            $('.nome_indicador').fadeIn('slow');
            $('#nome_indicador').attr('required','required');

        }else{

            $('.nome_indicador').fadeOut('slow');
            $('#nome_indicador').removeAttr('required');
            $('#nome_indicador').val('');

          /* indicador = '';
            orcamento_id = $('#nome_indicador').attr('orcamento_id');
            
            $.ajax({
				method: "POST",
				url:  base_url+"AreaFeira/alteraIndicadorOrcamento", 
				async: true,
				data: { indicador		: 	indicador,
						orcamento_id 	: 	orcamento_id }

			}).done(function(data) {

				var dados = $.parseJSON(data);
				if(dados.retorno == 'sucesso'){
					swal({
						title: "OK!",
						text: 'Indicador atualizado com sucesso!',
						type: "success"
					});
				}else{
					swal({
						title: "OPS!",
						text: 'Acontenceu um erro inesperado, entre em contato com o webmaster!',
						type: "warning"
					});
				}
	    	});*/
        }
    });

    
});

function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');
        $('.novo').find('tr[indice="'+indice+'"]').remove(); 
    });    
}

function mostra_valor()
{
    $('.bombas_new').bind('change', function(){
        indice = $(this).attr('indice');
        var tipo 		= 	$(this).find('option:selected').attr('tipo');
        var valor_base 	= 	$(this).find('option:selected').attr('valor_unitario');
        comissao 		= 	$('input[name="valor_desconto_orc"]:checked').val();        
       	valor_tributo	=	$("#valor_tributo").val();
       	
        if( tipo != 'opcionais' && comissao != 'outro' ){
	       
	        if( comissao == '3' && valor_tributo == '7.00' ) {
                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '10.00' ) {
                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 4% de comissão ***********
            // ************************************
            }else if( comissao == '4' && valor_tributo == '7.00' ) {
                
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '10.00' ) {
                
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '12.00') {
                
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '18.00') {

               	var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 5% de comissão ***********
            // ************************************
            }else if(comissao == '5' && valor_tributo == '7.00'  ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '10.00'  ) {

                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11) *1.12).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 2% de comissão ***********
            // ************************************
            }else if(comissao == '2' && valor_tributo == '7.00') {

                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '10.00') {

                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '12.00') {

               
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '18.00') {

               
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 0% de comissão ***********
            // ************************************
            }else if(comissao == '0' && valor_tributo == '7.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '10.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11) *1.12).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '12.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '18.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            }else if(comissao == 'TB2') {
			    
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())).toFixed(2);
            }

            // Acréscimo de 20% sobre o valor final quando for para o nordeste
           /* if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
                $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
                $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
                $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
            {
                valor_produto = valor_produto * 1.2;
            }
	        */
	        $('.valor_unitarios_text_'+indice).text(valor_produto);
	        $('#valor_unitarios_'+indice).val(valor_produto);
	        $('.valor_unitarios_text_'+indice).unmask();	
	        $('.valor_unitarios_text_'+indice).mask('#.##0,00', {reverse: true});

	    }else{

	    	/*if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
                $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
                $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
                $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
            {
                valor_produto = valor_base * 1.2;
            }*/
            
	    	$('.valor_unitarios_text_'+indice).text(valor_base);
	        $('#valor_unitarios_'+indice).val(valor_base);
	        $('.valor_unitarios_text_'+indice).unmask();	
	        
	    }
        
	}); 

}
   
function excluirProduto(orcamento_produto_id,indice,orcamento_id){ 
	var total_linhas = $('.novo_produto_orc').length;
	if( total_linhas == 1 ){
		swal({
			title: "OPS!",
			text: 'Impossível remover este produto. Altere o status do orçamento para "CANCELADO"!',
			type: "warning"
		});
	}else{

		swal({
		  title: 'Excluir?',
		  text: "Deseja realmente excluir esse produto?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Sim!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					method: "POST",
					url:  base_url+"AreaFeira/excluirProdutoOrcamento", 
					async: true,
					data: { orcamento_produto_id	: 	orcamento_produto_id }

				}).done(function(data) {
					var dados = $.parseJSON(data);		
				
					if(dados.retorno == 'sucesso')
					{				
						swal({
							title: "OK!",
							text: 'Produto(s) excluído(s) do orçamento com sucesso!',
							type: "success"
						}).then(function() {
							cadastraEmissao(orcamento_id);
			      			window.location = base_url+'AreaFeira/visualizarOrcamento/'+orcamento_id;
			   			});

						$('tr[indice="'+indice+'"]').remove();

					}else{
						swal({
							title: "OPS!",
							text: 'Alguma inconsistência ocorreu, contate o webmaster!',
							type: "warning"
						}).then(function() {
				      		window.location = base_url+'AreaFeira/visualizarOrcamento/'+orcamento_id;
				   		});
					}

				});	
			}
		});
	}
}

function replaceAll(str, de, para){
    var pos = str.indexOf(de);
    while (pos > -1){
		str = str.replace(de, para);
		pos = str.indexOf(de);
	}
    return (str);
}

function atualiza_valores(comissao){

	indice = 1;
	erro = 0;

    $('.valor_unitario').each(function(){

    	var valor_base 		= 	$(this).attr('valor_base');
    	var valor_tributo	=	$("#valor_tributo").val();
    	
    	if($(this).attr('tipo') != 'opcionais'){
	        if($(this).attr('valor_base') != '' && $(this).attr('valor_base') != undefined )  {

		        if( comissao == '3' && valor_tributo == '7.00' ) {
	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
	            }else if( comissao == '3' && valor_tributo == '10.00' ) {
	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
	            }else if( comissao == '3' && valor_tributo == '12.00' ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
	            }else if( comissao == '3' && valor_tributo == '18.00' ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
	            // ************************************
	            // ********* 4% de comissão ***********
	            // ************************************
	            }else if( comissao == '4' && valor_tributo == '7.00' ) {
	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
	            }else if( comissao == '4' && valor_tributo == '10.00' ) {
	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
	            }else if( comissao == '4' && valor_tributo == '12.00') {
	                
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
	            }else if( comissao == '4' && valor_tributo == '18.00') {

	               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
	            // ************************************
	            // ********* 5% de comissão ***********
	            // ************************************
	            }else if(comissao == '5' && valor_tributo == '7.00'  ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
	            }else if(comissao == '5' && valor_tributo == '10.00'  ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
	            }else if(comissao == '5' && valor_tributo == '12.00' ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
	            }else if(comissao == '5' && valor_tributo == '18.00' ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
	            // ************************************
	            // ********* 2% de comissão ***********
	            // ************************************
	            }else if(comissao == '2' && valor_tributo == '7.00') {

	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
	            }else if(comissao == '2' && valor_tributo == '10.00') {

	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
	            }else if(comissao == '2' && valor_tributo == '12.00') {

	               
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
	            }else if(comissao == '2' && valor_tributo == '18.00') {

	               
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
	            // ************************************
	            // ********* 0% de comissão ***********
	            // ************************************
	            }else if(comissao == '0' && valor_tributo == '7.00') {

	               //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)).toFixed(2);
	               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
	            }else if(comissao == '0' && valor_tributo == '10.00') {

	               //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)).toFixed(2);
	               var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
	            }else if(comissao == '0' && valor_tributo == '12.00') {
	               
	               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
	            }else if(comissao == '0' && valor_tributo == '18.00') {
	               
	               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
	            }else if(comissao == 'TB2') {
				    
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())).toFixed(2);
	            }          
	            
	        }
	    }else{
	    	valor_produto = $(this).attr('valor_base');
	    }

	    // Acréscimo de 20% sobre o valor final quando for para o nordeste
        /*if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
            $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
            $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
            $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
        {
            valor_produto = valor_produto * 1.2;
        }*/

        orcamento_produto_id 	= 	$(this).attr('orcamento_produto_id');
        orcamento_id 			=	$(this).attr('orcamento_id');

        $.ajax({        	
			method: "POST",
			url:  base_url+"AreaFeira/alterarValorProduto",
			async: true,
			data: { id 				: 	orcamento_produto_id,
					valor 			: 	valor_produto,
					comissao 		: 	comissao,
					orcamento_id 	: 	orcamento_id }

		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso')
			{		
				cadastraEmissao(orcamento_id);
				erro = 0;
				
			}else{
				erro = 1;

			}

		});		
      
		indice++; 
        
    });

    if( erro ==0 ){

	    swal({
			title: "OK!",
			text: 'Valor da Comissão Alterada com Sucesso!',
			type: "success"
		}).then(function() {
      		window.location = base_url+'AreaFeira/visualizarOrcamento/'+orcamento_id;
   		});

	}
    
}

function atualizaTotal(){
	total 		= 0;
	indice_qtd 	= 1;
	$('.valor_produto').each(function(){
		
		total = parseFloat(parseFloat(parseFloat($(this).val())  *  parseFloat($('#qtd_'+indice_qtd).val())) + parseFloat(total));
		indice_qtd++;	
	});
	
	$('.valor_subtotal').text(total);	
	$('.valor_subtotal').unmask();
    $('.valor_subtotal').mask('###.##0,00', {reverse: true}); 

 }

 function geraPdfOrcamento(orcamento_id, contato){

	if( contato != '' ) {

		$.ajax({
			method: "POST",
			url:  base_url+"AreaFeira/geraOrcamento/"+orcamento_id+"/ajax/"+contato, 
			async: true,
			data: { orcamento_id 	: 	orcamento_id,
					contato 		: 	contato			}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno != 'erro')
			{				
				$('#m_modal_10').modal('hide');
				window.open(base_url+'pdf/'+dados.retorno);
			}

		});		

	}else{
		swal({
			title: "OPS!",
			text: 'Preencha todos os dados para continuar!',
			type: "warning"
		});
	}	
	
 }

function cadastraEmissao(orcamento_id){
	var status_orcamento_id = $('#status_id').val();
	
	$.ajax({
		method: "POST",
		url:  base_url+"AreaFeira/insereMotivoReemissao",
		async: true,
		data: { orcamento_id 		: 	orcamento_id,					
				motivo_emissao 		: 	'Alteração no orçamento.',
				status_orcamento_id : 	status_orcamento_id 	}
	}).done(function(data) {
		var dados = $.parseJSON(data);
	});

} 