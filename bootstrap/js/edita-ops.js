$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	$('#cpf_cliente').mask('000.000.000-00');
	$('.porcentagem').mask('000');

	$('.add').bind('click', function(){
		$("#m_modal_7").modal({
		    show: true
		});
       
    });

	$('.add-subitem').bind('click', function(){
		var html = $('#descricao_filho-0').clone();
		var produto_id = $('#produto_id_filho-0').clone();
		var qtd  = $('#qtd_filho-0').clone();      
        var indice = $('#descricao_filho-0').attr('indice');
		var novo_indice = parseInt(indice) + 1;
		var remover = '<i class="la la-minus excluir" style="color: red;font-size: 27px;float: right; cursor: pointer;margin-top: -43px;" subitem_id="descricao_filho-'+novo_indice+'" qtd_id="qtd_filho-'+novo_indice+'"></i>';
		$(html).attr('id','descricao_filho-'+novo_indice);
		$(html).attr('filho','produto_id_filho-'+novo_indice);
		$(html).val('');
		$(qtd).attr('id','qtd_filho-'+novo_indice);
		$(produto_id).attr('id','produto_id_filho-'+novo_indice);
		$(produto_id).val('');
		$(qtd).attr('style','width: 90%;margin-bottom: 10px;');
		$('#descricao_filho-0').attr('indice', novo_indice);
		$('#qtd_filho-0').attr('indice', novo_indice);
			    
		$('#descricao_filho-0').after(html);
		$('#produto_id_filho-0').after(produto_id);
		$('#qtd_filho-0').after(qtd); 
		
		$('#qtd_filho-'+novo_indice).after(remover);	
		excluir();
		autocompletes('#descricao_filho-'+novo_indice);
		
	});

	$( "#itens" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaItens",
		minLength: 3,
		select: function( event, ui ) {
			$('#item_id').val(ui.item.id);
			
		}
    });
	
	$( ".sub" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaProdutosAvulsos",
		minLength: 3,
		select: function( event, ui ) {
			
			$('#produto_id_filho-0').val(ui.item.id);
			$('#produto_id_filho-0').attr('item_id',ui.item.item_id);
			
		}
    });
	
	$('#adicionar_item').bind('click', function(){
		var descricao = [];
		var qtd = [];
		var item_id = [];
		var erro = 0;
		
		$('.subi').each(function(){
			if( $(this).val() != '' || $(this).val() != undefined  ){
				descricao.push($(this).val());
				item_id.push($(this).attr('item_id'));
			}else{
				erro = 1;
			}
		});
		
		$('.qtd').each(function(){
			if( $(this).val() != '' || $(this).val() != undefined  ){
				qtd.push($(this).val());
			}else{
				erro = 1;
			}
		});
		
		if( erro == 0 ){
			var subitens_desc 		= JSON.stringify(descricao);
			var subitens_qtd 		= JSON.stringify(qtd);
			var subitens_item_id 	= JSON.stringify(item_id);
			var item_desc 			= $('#descricao_pai').val();
			
			$.ajax({
				method: "POST",
				url: base_url+"AreaAdministrador/insereItensSubitens",
				async: true,
				data: { item_desc 		:   item_desc,
						sub_descricao 	: 	subitens_desc,
						sub_qtd			: 	subitens_qtd,
						subitens_item_id: 	subitens_item_id
						}
			}).done(function( data ) {
				var dados = $.parseJSON(data);
					if(dados.retorno == 'sucesso'){
						swal({
							title: "OK!",
							text: 'Item cadastrado com sucesso, agora ele já está disponível para criação da op',
							type: 'success'
						}).then(function() {
							$('#m_modal_7').modal('hide');
							$('#m_modal_7').fadeOut('slow');
						});
						
					}else{
						
						swal({
							title: 'Ops!',
							text: 'Não deu',
							type: 'warning'
						}).then(function() {
							$('#m_modal_7').modal('hide');
							$('#m_modal_7').fadeOut('slow');
						});
												
					}
					
				
			});
			
		}else{
				swal({
					title: 	'Ops!',
					text: 	'Preencha todos os campos',
					type: 	'warning'
				}).then(function() {
					$('#m_modal_7').modal('hide');
					$('#m_modal_7').fadeOut('slow');
				});
			
		}
		
	});
	
	

   
});

function excluir()
{
    $('.excluir').bind('click', function(){
        var id = $(this).attr('subitem_id');
		var qtd_id = $(this).attr('qtd_id');		
        $('#'+id).remove();
		$('#'+qtd_id).remove();
		$(this).remove();
    });    
}
function autocompletes($id){
	
	$( $id ).autocomplete({
		source: base_url+"AreaAdministrador/retornaProdutosAvulsos",
		minLength: 3,
		select: function( event, ui ) {
			filho= $(this).attr('filho');
			
			$('#'+filho).val(ui.item.id);
			$('#'+filho).attr('item_id',ui.item.item_id);
			
		}
	});

}