$(document).ready(function(){
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	/* autocomplete responsável */
	$('#tag').bind('focusout', function(){
				
		if( $(this).val().length != 16 ){

			swal({
				title: "Atenção!",
				text: 'O campo TAG deve conter 16 caracteres.',
				type: 'warning'
			}).then(function() {					
				$('#tag').val('');
			});
		}
	});

	$( "#usuario" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaTecnicos",
		minLength: 1,
		select: function( event, ui ) {
			$('#usuario_id').val(ui.item.id);
		},
		response: function (event, ui) {
	        if (!ui.content.length) {
	            swal({
					title: "Atenção!",
					text: 'Selecione um técnico.',
					type: 'warning'
				}).then(function() {					
					$('#usuario').val('');
				});

	        }
	    },
	    change: function(event,ui) {
          	if(ui.item == null){
          		$('#usuario').val('');
          	}
      	}

    });
});

