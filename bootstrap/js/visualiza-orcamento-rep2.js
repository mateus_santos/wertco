$(document).ready(function(){   
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    var atualizacao_dados = 0;
    
    if( $('#status').attr('status') == 'fechado' || $('#status').attr('status') == 'perdido' || $('#status').attr('status') == 'cancelado'  )   {
        $('select').attr('disabled',true);
        $('input').attr('disabled',true);
        $('.excluir_produto').hide();
        $('a[title="Adicionar produto"]').hide();
        $('#emitirOrcamento').hide();
    }

    $('.valor_unitario').mask('#.##0,00', {reverse: true}); 
    $('#valor_desconto').mask('00,00', {reverse: true}); 
    $('#valor_desconto_orc').mask('00,00', {reverse: true}); 
    
    if( $('#valor_desconto').val() != "" ){
        $('#valor_desconto').attr('disabled',true);
    }
    
    $('.editar_produto').bind('click', function(){
        var indice = $(this).attr('indice');
        $('#valor_produto_'+indice).removeAttr('disabled');
    });

    $('.bombas').bind('change', function(){

        indice                  =   $(this).attr('indice');
        produto_id              =   $(this).val();
        orcamento_produto_id    =   $(this).attr('orcamento_produto_id');
        orcamento_id            =   $('#orcamento_id').val();
        status_id               =   $('#status_id').val();
        produto                 =   $(this).find('option:selected').text();
        produto_nr              =   indice;
        valor_base              =   $(this).find('option:selected').attr('valor_unitario');
        comissao                =   $("input[name='valor_desconto_orc']:checked").val();
        valor_tributo           =   $("#valor_tributo").val();
        estado                  =   $('#estado').val();
        $('#reajuste').val($(this).find('option:selected').attr('reajuste'));
        if( $(this).find('option:selected').attr('tipo') != 'opcionais' ){

            if( comissao == '3' && valor_tributo == '7.00' ) {
                
                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val())) * 1.11).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '10.00' ) {
                
                var valor_produto = Math.round((parseFloat(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val())) * 1.11)*1.12).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val())) * 1.09).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val())) * 1.07).toFixed(2);
            // ************************************
            // ********* 4% de comissão ***********
            // ************************************
            }else if( comissao == '4' && valor_tributo == '7.00' ) {
                
                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val())) * 1.11).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '10.00' ) {
                
                var valor_produto = Math.round((parseFloat(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val())) * 1.11)*1.12).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '12.00') {
                
                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val())) * 1.09).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '18.00') {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()))  * 1.07).toFixed(2);
            // ************************************
            // ********* 5% de comissão ***********
            // ************************************
            }else if(comissao == '5' && valor_tributo == '7.00'  ) {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val())) * 1.11).toFixed(2);                
                if( estado == 'AC' || estado == 'AP' || estado == 'AM' || estado == 'PA' || estado == 'RO' || estado == 'RR' || estado == 'TO'){
                    valor_produto = valor_produto / 0.9798;
                }
            }else if(comissao == '5' && valor_tributo == '10.00'  ) {

                var valor_produto = Math.round((parseFloat(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val())) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val())) * 1.09).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val())) * 1.07).toFixed(2);
            // ************************************
            // ********* 2% de comissão ***********
            // ************************************
            }else if(comissao == '2' && valor_tributo == '7.00') {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())) * 1.11).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '10.00') {

                var valor_produto = Math.round((parseFloat(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '12.00') {
               
                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())) * 1.09).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '18.00') {
               
                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())) * 1.07).toFixed(2);
            
            // ************************************
            // ********* 0% de comissão ***********
            // ************************************
            }else if(comissao == '0' && valor_tributo == '7.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '10.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '12.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '18.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            }
            /* Acréscimo de 12% no valor do produto geral 01/08/2022 */
            valor_produto = Math.round(parseFloat(valor_produto) * 1.12);
            /* Inserção do reajuste 30/11/2022 */            
            if( $('#reajuste').val() > 0){
                valor_produto = Math.round(valor_produto * (1 + parseFloat($('#reajuste').val() / 100))).toFixed(2);    
            }else{
                valor_produto = valor_produto.toFixed(2);
            }
            $('b.valor_unitario_'+indice).text(valor_produto);
            $('#valor_produto_'+indice).val(valor_produto);
            $('b.valor_unitario_'+indice).unmask();
            $('b.valor_unitario_'+indice).mask('#.##0,00', {reverse: true});

        }else{

            $('b.valor_unitario_'+indice).text(valor_base);
            $('#valor_produto_'+indice).val(valor_base);
            $('b.valor_unitario_'+indice).unmask();
            //$('b.valor_unitario_'+indice).mask('000.000.000.000.000,00');

            var valor_produto = valor_base;
        }

        swal({
            title: "Atenção!",
            text: 'Para alterar esse produto, exclua a linha e insira novamente o produto',
            type: "warning"
        }).then(function() {

            $('#valor_produto_'+indice).css('border','2px solid green');
            $('#valor_produto_'+indice).removeAttr('disabled');
            $('#valor_produto_'+indice).focus();                    
            
        });   

        $.ajax({
            method: "POST",
            url:  base_url+"AreaRepresentantes2/alterarProdutoOrcamento", 
            async: true,
            data: { produto_id              :   produto_id,
                    orcamento_produto_id    :   orcamento_produto_id,
                    status_orcamento_id     :   status_id,
                    orcamento_id            :   orcamento_id,
                    produto_nr              :   produto_nr,
                    produto                 :   produto,
                    valor_produto           :   valor_produto
                    }
        }).done(function(data) {

            var dados = $.parseJSON(data);
            
            if(dados.retorno == 'sucesso')
            {
                atualizacao_dados = 1;
                swal({
                    title: "OK!",
                    text: 'Produto alterado com sucesso!',
                    type: "success"
                }).then(function() {

                    $('#valor_produto_'+indice).css('border','2px solid green');
                    $('#valor_produto_'+indice).removeAttr('disabled');
                    $('#valor_produto_'+indice).focus();                    
                    
                });
            }

        });
    }); 

    $('.entregaFretePagto').bind('change', function(){
        var orcamento_id        =   $(this).attr('orcamento_id');
        var valor               =   $(this).val();
        var tipo                =   $(this).attr('id'); 
        var status_orcamento_id =   $(this).attr('status_id');
        
        var url = '';
        if( tipo == 'forma_pagto' ){
            url = 'alterarFormaPagto';
            desc_tipo = 'Forma de Pagamento';
        }else if( tipo == 'entregas' ) {
            url = 'alterarEntrega';
            desc_tipo = 'Forma de Entrega';
        }else{
            url = 'alterarFrete';
            desc_tipo = 'Frete';
        }

        $.ajax({
            method: "POST",
            url:  base_url+"AreaRepresentantes2/"+url, 
            async: true,
            data: { valor               :   valor,
                    orcamento_id        :   orcamento_id,
                    status_orcamento_id :   status_orcamento_id}
        }).done(function(data) {
            var dados = $.parseJSON(data);

            if(dados.retorno == 'sucesso')
            {
                atualizacao_dados = 1;
                swal({
                    title: "OK!",
                    text: desc_tipo+' alterado com sucesso!',
                    type: "success"
                });
            }
        });
    });


    $('.editar_qtd').bind('click', function(){
        var indice = $(this).attr('indice');
        $('#qtd_'+indice).removeAttr('disabled');
    });

    $('.qtd').bind('focusout', function(){
        var indice                  =   $(this).attr('indice');
        var qtd                     =   $(this).val();
        var orcamento_produto_id    =   $(this).attr('orcamento_produto_id');
        var orcamento_id            =   $('#orcamento_id').val();
        var status_id               =   $('#status_id').val();
        $.ajax({
            method: "POST",
            url:  base_url+"AreaRepresentantes2/alterarQtdProduto", 
            async: true,
            data: { qtd                     :   qtd,
                    orcamento_produto_id    :   orcamento_produto_id,
                    status_orcamento_id     :   status_id,
                    orcamento_id            :   orcamento_id  }
        }).done(function(data) {
            var dados = $.parseJSON(data);      
            
            if(dados.retorno == 'sucesso')
            {               
                $('#valor_produto_'+indice).attr('qtd', qtd);
                atualizacao_dados = 1;  
                swal({
                    title: "OK!",
                    text: 'Quantidade alterada com sucesso!',
                    type: "success"
                }).then(function() {
                    cadastraEmissao(orcamento_id);
                    window.location = base_url+'AreaRepresentantes2/visualizarOrcamento/'+window.btoa(orcamento_id);
                });
            }
        });

    }); 


    $('.valor_produto').bind('focusout', function(){

        var valor_produto           =   $(this).val();
        var orcamento_id            =   $(this).attr('orcamento_id');
        var orcamento_produto_id    =   $(this).attr('orcamento_produto_id');
        var orcamento_id            =   $('#orcamento_id').val();
        var status_id               =   $('#status_id').val();

        $.ajax({
            method: "POST",
            url:  base_url+"AreaRepresentantes2/alterarValorProduto", 
            async: true,
            data: { valor_produto           :   valor_produto,
                    orcamento_produto_id    :   orcamento_produto_id,
                    status_orcamento_id     :   status_id,
                    orcamento_id            :   orcamento_id }
        }).done(function(data) {
            var dados = $.parseJSON(data);      
            
            if(dados.retorno == 'sucesso')
            {               
                atualizacao_dados = 1;  
            }
        });

    }); 

    $('.atualiza_valor').bind('click', function(){
        var valor = 0;
        $('.valor_produto').each(function(){
            novo_valor = replaceAll($(this).val(), '.', '');
            novo_valor = replaceAll(novo_valor, ',', '.');          
            valor = valor + (parseFloat(novo_valor) * parseInt($(this).attr('qtd')));
            
        });
        
        subtotal                =   valor.toFixed(2);
        valor_total             =   (valor + (parseFloat(valor.toFixed(2))/100 * parseFloat($('#valor_tributo').val())) + (parseFloat(valor.toFixed(2))/100  * 5)).toFixed(2);
        desconto                =   replaceAll($('#valor_desconto').val(), '.', '');
        desconto                =   replaceAll(desconto, ',', '.');             
        valor_total_desconto    =   (valor_total - (valor_total/100*parseFloat(desconto))).toFixed(2);      
        $('.valor_total').text(valor_total);
        $('.valor_subtotal').text(subtotal);
        $('.valor_final_desconto').text(valor_total_desconto);
        $('.valor_total').mask('#.##0,00', {reverse: true}); 
        $('.valor_subtotal').mask('#.##0,00', {reverse: true});
        $('.valor_final_desconto').mask('#.##0,00', {reverse: true});

    });

    $('#alterar_valor').bind('click', function(){

        orcamento_id    =   $(this).attr('orcamento_id');   
        status_id       =   $(this).attr('status_id');  
        valor_orcamento =   $('#valor_orcamento').val();
            
        $.ajax({
            method: "POST",
            url:  base_url+"AreaRepresentantes2/alterarValorOrcamento", 
            async: true,
            data: { orcamento_id    :   orcamento_id,
                    valor_orcamento :   valor_orcamento,
                    status_id       :   status_id}
        }).done(function(data) {
            var dados = $.parseJSON(data);      
            
            if(dados.retorno == 'sucesso')
            {               
                swal({
                        title: "OK!",
                        text: 'Valor do Orçamento alterado com sucesso!',
                        type: "success"
                    }).then(function() {
                        cadastraEmissao(orcamento_id);
                        window.location = base_url+'AreaRepresentantes2/visualizarOrcamento/'+window.btoa(orcamento_id);
                    });

            }else{      
                swal({
                    title: "OPS!",
                    text: 'Alguma inconsistência ocorreu, contate o webmaster!',
                    type: "warning"
                }).then(function() {
                    window.location = base_url+'AreaRepresentantes2/orcamentos';
                });
            }

        });     

    });

    /*$("#geraPdf").bind('click', function(){

        orcamento_id    =   $(this).attr('orcamento_id');   
        contato         =   $('#contato_posto').val();          
        fl_especial     =   $('#fl_especial').val();
        
        $.ajax({
            method: "POST",
            url:  base_url+"AreaRepresentantes2/verificaEmissao",
            async: true,
            data: { orcamento_id    :   orcamento_id    }
        }).done(function(data) {
            var dados = $.parseJSON(data);      

            if(fl_especial == 1 && dados.dias >= 5){
                $('#modal_reemissao').modal('show');

            }else if(fl_especial == 0 && dados.dias >= 15 ){
                $('#modal_reemissao').modal('show');

            }else{
                geraPdfOrcamento(orcamento_id, contato);                

            }
            
        });     
        
    });*/

    $('#adicionar_motivo').bind('click', function(){
        orcamento_id        =   $(this).attr('orcamento_id');
        status_orcamento_id =   $(this).attr('status_orcamento');
        motivo_emissao      =   $('#motivo_emissao').val();
        contato             =   $('#contato_posto').val();

        $.ajax({
            method: "POST",
            url:  base_url+"AreaRepresentantes2/insereMotivoReemissao",
            async: true,
            data: { orcamento_id        :   orcamento_id,                   
                    motivo_emissao      :   motivo_emissao,
                    status_orcamento_id :   status_orcamento_id     }
        }).done(function(data) {
            var dados = $.parseJSON(data);      
            if(dados.retorno == 'sucesso'){
                swal({
                    title: "OK!",
                    text: 'Motivo inserido com sucesso!',
                    type: "success"
                }).then(function() {
                    cadastraEmissao(orcamento_id);
                    window.location = base_url+'AreaRepresentantes2/visualizarOrcamento/'+window.btoa(orcamento_id);
                });
                geraPdfOrcamento(orcamento_id, contato);        
                
            }else{
                swal(
                    'Ops!',
                    'Algum erro aconteceu!',
                    'warning'
                );
            }

        });

    });

    $("#valor_desconto").bind('focusout', function(){
        
        if( $(this).val() != "" )
        {
            $("#motivo_desconto").removeAttr('disabled');
        }else{
            $("#motivo_desconto").attr('disabled',true);
        }
    });

    $("#valor_desconto_orc").bind('focusout', function(){
        
        if( $(this).val() != "" )
        {
            $("#motivo_desconto_orc").removeAttr('disabled');
        }else{
            $("#motivo_desconto_orc").attr('disabled',true);
        }
    });

    $("#emitir_orcamento").bind('click', function(){

        orcamento_id        =   $(this).attr('orcamento_id');
        email_destino       =   $('#email_destino').val();      
        motivo_desconto     =   $('#motivo_desconto').val();
        valor_tributo       =   $('#valor_tributo').val();      
        valor_total         =   $('#valor_total').val();
        status_orcamento_id =   $(this).attr('status_id');
        
        var erro = 0;

        if( email_destino == "" ){
            swal(
                'Ops!',
                'Insira um e-mail para ser enviado o orçamento!',
                'warning'
            );
            erro++;
        }

        var emailFilter=/^.+@.+\..{2,}$/;
        var illegalChars= /[\(\)\<\>\,\;\:\\\/\"\[\]]/      
        if(!(emailFilter.test(email_destino)) || email_destino.match(illegalChars))        
        {
            swal(
                'Ops!',
                'Insira um e-mail válido!',
                'warning'
            );
            erro++;
        }       

        if( erro == 0 ){
            
            $.ajax({
                method: "POST",
                url:  base_url+"AreaRepresentantes2/emitirOrcamento", 
                async: true,
                data: { orcamento_id        :   orcamento_id,
                        email_destino       :   email_destino,                      
                        valor_tributo       :   valor_tributo,
                        valor_total         :   valor_total,
                        status_orcamento_id :   status_orcamento_id } 

            }).done(function(data) {
                var dados = $.parseJSON(data);      
                
                if(dados.retorno == 'sucesso')
                {           
                    
                    swal({
                        title: "OK!",
                        text: 'Orçamento Emitido com sucesso!',
                        type: "success"
                    }).then(function() {
                        window.location = base_url+'AreaRepresentantes2/orcamentos';
                    });

                }else{
            
                    swal({
                        title: "OPS!",
                        text: 'Alguma inconsistência ocorreu, contate o webmaster!',
                        type: "warning"
                    }).then(function() {
                        window.location = base_url+'AreaRepresentantes2/orcamentos';
                    });
                }

            });     
        }
    });

    $("#adicionar_desconto_orc").bind('click', function(){
        var valor_desconto = 0;
        $('input[name="valor_desconto_orc"]').each(function(){
            if($(this).is(':checked')){
                valor_desconto = $(this).val();
            }
        });
                    
        var orcamento_id        =   $(this).attr('orcamento_id');
        var erro = 0;
        if(valor_desconto == ""){
            swal(
                'Ops!',
                'Insira um desconto para o orçamento!',
                'warning'
            );
            erro++; 
        }
                
        if( erro == 0 ){

            $.ajax({
                method: "POST",
                url:  base_url+"AreaRepresentantes2/insereDescontoOrcamento", 
                async: true,
                data: { orcamento_id        :   orcamento_id,
                        valor_desconto      :   valor_desconto }

            }).done(function(data) {
                var dados = $.parseJSON(data);      
                
                if(dados.retorno == 'sucesso')
                {               
                    swal({
                        title: "OK!",
                        text: 'Desconto adicionado ao orçamento com sucesso!',
                        type: "success"
                    }).then(function() {
                        cadastraEmissao(orcamento_id);
                        window.location = base_url+'AreaRepresentantes2/visualizarOrcamento/'+window.btoa(orcamento_id);
                    });
                }else{
                    swal({
                        title: "OPS!",
                        text: 'Alguma inconsistência ocorreu, contate o webmaster!',
                        type: "warning"
                    }).then(function() {
                        cadastraEmissao(orcamento_id);
                        window.location = base_url+'AreaRepresentantes2/visualizarOrcamento/'+window.btoa(orcamento_id);
                    });
                }

            });     
        }


    });

    $('.add').bind('click', function(){

        var html = $('#modelo').clone();                
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
        $(html).find('.bombas_new').attr('indice', indice);
        $(html).find('.bombas_new').attr('id', 'bombas_new_'+indice);
        $(html).find('.valor_unitarios').attr('indice', indice);        
        $(html).find('.valor_unitarios_text').attr('indice', indice);
        $(html).find('.valor_unitarios_text').text('');
        $(html).find('.valor_unitarios_text').addClass('valor_unitarios_text_'+indice);
        $(html).find('.valor_unitarios_text').removeClass('valor_unitarios_text_0');
        
        $(html).find('.valor_unitarios').addClass('valor_unitarios_'+indice);
        $(html).find('.valor_unitarios').removeClass('valor_unitarios_0');
        $(html).find('.valor_unitarios').attr('indice', indice);
        $(html).find('.valor_unitarios').attr('id', 'valor_unitarios_'+indice);

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();                
        body+="</tr>";                
        $('table.novo').append(body);                
        $('#modelo').attr('total_indice',indice);

        excluir();
        mostra_valor();        
    });
    
    mostra_valor();

    $('#adicionar_produto').bind('click', function(){
        var dados           =   $('#produtos_novos').serializeArray();
        orcamento_id        =   $(this).attr('orcamento_id');
        status_orcamento_id =   $(this).attr('status_id');
        var inputVazios = $('.novo').find('input').filter(function () {
            return this.value.split(' ').join('') == '';
        });

        var selectVazios = $('.novo').find('select').filter(function () {
            return this.value.split(' ').join('') == '';
        });

        if( selectVazios.length > 0 || inputVazios.length > 0){
            swal({
                title: "Atenção!",
                text: 'Preencha corretamente os campos dos novos produtos!',
                type: "warning"
            }); 
        }else{
            
            $.ajax({
                method: "POST",
                url:  base_url+"AreaRepresentantes2/insereProdutosNovosOrcamento", 
                async: true,
                data: { dados               :   dados,
                        orcamento_id        :   orcamento_id,
                        status_orcamento_id :   status_orcamento_id }

            }).done(function(data) {
                var dados = $.parseJSON(data);      
                
                if(dados.retorno == 'sucesso')
                {
                    swal({
                        title: "OK!",
                        text: 'Produto(s) adicionado(s) ao orçamento com sucesso!',
                        type: "success"
                    }).then(function() {
                        cadastraEmissao(orcamento_id);
                        window.location = base_url+'AreaRepresentantes2/visualizarOrcamento/'+window.btoa(orcamento_id);
                    });
                }else{
                    swal({
                        title: "OPS!",
                        text: 'Alguma inconsistência ocorreu, contate o webmaster!',
                        type: "warning"
                    }).then(function() {
                        cadastraEmissao(orcamento_id);
                        window.location = base_url+'AreaRepresentantes2/visualizarOrcamento/'+window.btoa(orcamento_id);
                    });
                }
            }); 
        }
    });

    /* Slider */
    $('#valor_desconto_orc').slider({
        formatter: function(value) {
            return 'Desconto: ' + value + '%';
        }
    });

    $('.comissao').bind('click', function(){
       
        console.log($(this).val());        
        swal({
          title: 'Alterar tabela?',
          text: "Deseja realmente alterar a tabela de preços desses produtos?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim!'
        }).then((result) => {
            if (result.value) {

                atualiza_valores($(this).val());
                cadastraEmissao(orcamento_id);
            }
        });
        
        
    });
});

function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');
        $('.novo').find('tr[indice="'+indice+'"]').remove(); 
    });    
}

function mostra_valor()
{
    $('.bombas_new').bind('change', function(){
        indice = $(this).attr('indice');
        var tipo        = $(this).find('option:selected').attr('tipo');
        var valor_base  = $(this).find('option:selected').attr('valor_unitario');
        comissao        = $('input[name="valor_desconto_orc"]:checked').val();
        valor_tributo   =   $("#valor_tributo").val();
        estado = $('#estado').val();
        reajuste        =   $(this).find('option:selected').attr('reajuste');
        if(tipo != 'opcionais'){

            if( comissao == '3' && valor_tributo == '7.00' ) {
                
                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val())) * 1.11).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '10.00' ) {
                
                var valor_produto = Math.round((parseFloat(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val())) * 1.11)*1.12).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val())) * 1.09).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val())) * 1.07).toFixed(2);
            // ************************************
            // ********* 4% de comissão ***********
            // ************************************
            }else if( comissao == '4' && valor_tributo == '7.00' ) {
                
                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val())) * 1.11).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '10.00' ) {
                
                var valor_produto = Math.round((parseFloat(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val())) * 1.11)*1.12).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '12.00') {
                
                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val())) * 1.09).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '18.00') {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()))  * 1.07).toFixed(2);
            // ************************************
            // ********* 5% de comissão ***********
            // ************************************
            }else if(comissao == '5' && valor_tributo == '7.00'  ) {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val())) * 1.11).toFixed(2);
                if( estado == 'AC' || estado == 'AP' || estado == 'AM' || estado == 'PA' || estado == 'RO' || estado == 'RR' || estado == 'TO'){
                    valor_produto = valor_produto / 0.9798;
                }
            }else if(comissao == '5' && valor_tributo == '10.00'  ) {

                var valor_produto = Math.round((parseFloat(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val())) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val())) * 1.09).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val())) * 1.07).toFixed(2);
            // ************************************
            // ********* 2% de comissão ***********
            // ************************************
            }else if(comissao == '2' && valor_tributo == '7.00') {

                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())) * 1.11).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '10.00') {

                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round((parseFloat(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '12.00') {

               
                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())) * 1.09).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '18.00') {

               
                var valor_produto = Math.round((parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())) * 1.07).toFixed(2);          
            // ************************************
            // ********* 0% de comissão ***********
            // ************************************
            }else if(comissao == '0' && valor_tributo == '7.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '10.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '12.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '18.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            }
            /* Acréscimo de 12% no valor do produto geral 01/08/2022 */
            valor_produto = Math.round(parseFloat(valor_produto) * 1.12);
            /* Inserção do reajuste 30/11/2022 */
            
            if( reajuste > 0 ){            
                valor_produto = Math.round(valor_produto * (1 + parseFloat(reajuste / 100))).toFixed(2);    
            }else{
                valor_produto = valor_produto.toFixed(2);
            }
            $('.valor_unitarios_text_'+indice).text(valor_produto);
            $('#valor_unitarios_'+indice).val(valor_produto);
            $('.valor_unitarios_text_'+indice).unmask();    
            $('.valor_unitarios_text_'+indice).mask('#.##0,00', {reverse: true});

        }else{

            $('.valor_unitarios_text_'+indice).text(valor_base);
            $('#valor_unitarios_'+indice).val(valor_base);
            $('.valor_unitarios_text_'+indice).unmask();    
            
        }
        
    }); 

}
   
function excluirProduto(orcamento_produto_id,indice,orcamento_id){
    var total_linhas = $('.novo_produto_orc').length;
    if( total_linhas == 1 ){
        swal({
            title: "OPS!",
            text: 'Impossível remover este produto. Altere o status do orçamento para "CANCELADO"!',
            type: "warning"
        });
    }else{

        swal({
          title: 'Excluir?',
          text: "Deseja realmente excluir esse produto?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sim!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    method: "POST",
                    url:  base_url+"AreaRepresentantes2/excluirProdutoOrcamento", 
                    async: true,
                    data: { orcamento_produto_id    :   orcamento_produto_id }

                }).done(function(data) {
                    var dados = $.parseJSON(data);      
                
                    if(dados.retorno == 'sucesso')
                    {               
                        swal({
                            title: "OK!",
                            text: 'Produto(s) excluído(s) do orçamento com sucesso!',
                            type: "success"
                        }).then(function() {
                            cadastraEmissao(orcamento_id);
                            window.location = base_url+'AreaRepresentantes2/visualizarOrcamento/'+window.btoa(orcamento_id);
                        });

                        $('tr[indice="'+indice+'"]').remove();

                    }else{
                        swal({
                            title: "OPS!",
                            text: 'Alguma inconsistência ocorreu, contate o webmaster!',
                            type: "warning"
                        }).then(function() {
                            cadastraEmissao(orcamento_id);
                            window.location = base_url+'AreaRepresentantes2/visualizarOrcamento/'+window.btoa(orcamento_id);
                        });
                    }

                }); 
            }
        });
    }
}

function replaceAll(str, de, para){
    var pos = str.indexOf(de);
    while (pos > -1){
        str = str.replace(de, para);
        pos = str.indexOf(de);
    }
    return (str);
}

function atualiza_valores(comissao){
    indice = 1;
    erro = 0;
    total = 0.00;
    estado = $('#estado').val();
    $('b.valor_unitario').each(function(){

        var valor_base      =   $(this).attr('valor_base');
        var valor_tributo   =   $("#valor_tributo").val();
        var reajuste        =   $(this).attr('reajuste');
        if($(this).attr('tipo') != 'opcionais'){
            if($(this).attr('valor_base') != '' && $(this).attr('valor_base') != undefined )  {

                if( comissao == '3' && valor_tributo == '7.00' ) {
                    //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
                }else if( comissao == '3' && valor_tributo == '10.00' ) {
                    //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                    var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
                }else if( comissao == '3' && valor_tributo == '12.00' ) {

                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
                }else if( comissao == '3' && valor_tributo == '18.00' ) {

                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
                // ************************************
                // ********* 4% de comissão ***********
                // ************************************
                }else if( comissao == '4' && valor_tributo == '7.00' ) {
                    //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(retorno.fator)).toFixed(2);
                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
                }else if( comissao == '4' && valor_tributo == '10.00' ) {
                    //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(retorno.fator)).toFixed(2);
                    var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
                }else if( comissao == '4' && valor_tributo == '12.00') {
                    
                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
                }else if( comissao == '4' && valor_tributo == '18.00') {

                   var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
                // ************************************
                // ********* 5% de comissão ***********
                // ************************************
                }else if(comissao == '5' && valor_tributo == '7.00'  ) {

                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
                    if( estado == 'AC' || estado == 'AP' || estado == 'AM' || estado == 'PA' || estado == 'RO' || estado == 'RR' || estado == 'TO'){
                        valor_produto = valor_produto / 0.9798;
                    }
                }else if(comissao == '5' && valor_tributo == '10.00'  ) {

                    var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
                }else if(comissao == '5' && valor_tributo == '12.00' ) {

                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
                }else if(comissao == '5' && valor_tributo == '18.00' ) {

                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
                // ************************************
                // ********* 2% de comissão ***********
                // ************************************
                }else if(comissao == '2' && valor_tributo == '7.00') {

                    //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
                }else if(comissao == '2' && valor_tributo == '10.00') {

                    //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                    var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
                }else if(comissao == '2' && valor_tributo == '12.00') {

                   
                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
                }else if(comissao == '2' && valor_tributo == '18.00') {

                   
                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
                // ************************************
                // ********* 0% de comissão ***********
                // ************************************
                }else if(comissao == '0' && valor_tributo == '7.00') {

                   //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)).toFixed(2);
                   var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
                }else if(comissao == '0' && valor_tributo == '10.00') {

                   //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)).toFixed(2);
                   var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
                }else if(comissao == '0' && valor_tributo == '12.00') {
                   
                   var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
                }else if(comissao == '0' && valor_tributo == '18.00') {
                   
                   var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
                }else if(comissao == 'TB2') {
                    
                    var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())).toFixed(2);
                }          
                
            }
            /* Acréscimo de 12% no valor do produto geral 01/08/2022 */
            valor_produto = Math.round(parseFloat(valor_produto) * 1.12);
            /* Inserção do reajuste 30/11/2022 */        
            if( reajuste > 0){
                valor_produto = Math.round(valor_produto * (1 + parseFloat(reajuste / 100))).toFixed(2);    
            }else{
                valor_produto = valor_produto.toFixed(2);
            }

        }else{
            valor_produto = $(this).attr('valor_base');
            valor_produto = Math.round(parseFloat(valor_produto)).toFixed(2);
        }
        
        $(this).text(valor_produto);
        total = total + Number(valor_produto);
        console.log(valor_produto);
        
        orcamento_produto_id    =   $(this).attr('orcamento_produto_id');
        orcamento_id            =   $(this).attr('orcamento_id');
        
        if(comissao == 2 || comissao == 0){
            erro = 1;
            swal({
                title: "Atenção!",
                text: "Para gerar orçamentos com as tabelas de 0% e 2%, entre em contato com a nossa equipe comercial!",
                type: 'warning'
            }).then(function() {
                window.location = base_url+'AreaRepresentantes2/visualizarOrcamento/'+window.btoa(orcamento_id);   
            }); 
        }else{
           
            $.ajax({            
                method: "POST",
                url:  base_url+"AreaRepresentantes2/alterarValorProduto",
                async: true,
                data: { id              :   orcamento_produto_id,
                        valor           :   valor_produto,
                        comissao        :   comissao,
                        orcamento_id    :   orcamento_id }

            }).done(function(data) {
                var dados = $.parseJSON(data);      
                
                if(dados.retorno == 'sucesso')
                {       
                    cadastraEmissao(orcamento_id);
                    erro = 0;
                    
                }else{
                    erro = 1;

                }

            });     
          
            indice++; 
       }
       
    });

    if( erro ==0 ){
        swal({
            title: "OK!",
            text: 'Valor da Comissão alterada com sucesso!',
            type: "success"
        }).then(function() {
            cadastraEmissao(orcamento_id);
            window.location = base_url+'AreaRepresentantes2/visualizarOrcamento/'+window.btoa(orcamento_id);
        });

    }
    console.log(total);
    $('.valor_unitario').unmask().mask('#.##0,00', {reverse: true});
    $('.valor_subtotal').text(total+',00');
    $('.valor_subtotal').unmask().mask('#.##0,00', {reverse: true});
}

function atualizaTotal(){
    total       = 0;
    indice_qtd  = 1;
    $('.valor_unitario').each(function(){
        
        total = parseFloat(parseFloat(parseFloat($(this).val())  *  parseFloat($('#qtd_'+indice_qtd).val())) + parseFloat(total));
        indice_qtd++;   
    });
    
    $('.valor_subtotal').text(total);   
    $('.valor_subtotal').unmask();
    $('.valor_subtotal').mask('###.##0,00', {reverse: true}); 
 }

 function geraPdfOrcamento(orcamento_id, contato){

    if( contato != '' ) {

        $.ajax({
            method: "POST",
            url:  base_url+"AreaRepresentantes2/geraOrcamento/"+orcamento_id+"/ajax/"+contato, 
            async: true,
            data: { orcamento_id    :   orcamento_id,
                    contato         :   contato         }
        }).done(function(data) {
            var dados = $.parseJSON(data);      
            
            if(dados.retorno != 'erro')
            {               
                $('#m_modal_10').modal('hide');
                window.open(base_url+'pdf/'+dados.retorno);
            }

        });     

    }else{
        swal({
            title: "OPS!",
            text: 'Preencha todos os dados para continuar!',
            type: "warning"
        });
    }   
    
 }  

function cadastraEmissao(orcamento_id){
    var status_orcamento_id = $('#status_id').val();
    
    $.ajax({
        method: "POST",
        url:  base_url+"AreaRepresentantes2/insereMotivoReemissao",
        async: true,
        data: { orcamento_id        :   orcamento_id,                   
                motivo_emissao      :   'Alteração no orçamento.',
                status_orcamento_id :   status_orcamento_id     }
    }).done(function(data) {
        var dados = $.parseJSON(data);
    });

}