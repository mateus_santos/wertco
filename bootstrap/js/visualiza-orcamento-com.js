$(document).ready(function(){	

	var atualizacao_dados = 0;
	$('.date').datepicker({
        orientation: 'bottom',
        format: 'dd/mm/yyyy'
    });
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

	if($('#valor_tributo').val() == '7.00'){
		$('#tn').fadeIn('slow');
	}else{
		$('#tn').fadeOut('slow');
	}

	//caso não seja da zona do usuário, bloqueia os campos
	/*if($('#permite_alterar').val() != 1 || $('#status_id').val() == 13 ){
		$('input').attr('disabled', true);
		$('textarea').attr('disabled', true);
		$('select').attr('disabled', true);
		$('.la-edit').remove();
		$('.la-plus').remove();		
		$('.excluir_produto').remove();
		$('#motivo_emissao').attr('disabled', false);
		$('button#aprovacao').remove();
		$('.m-portlet__nav').remove();
		$('.excluir_indicador').remove();
	}*/

	if( $('#status').attr('status') == 'fechado' || $('#status').attr('status') == 'perdido' || $('#status').attr('status') == 'cancelado'){
		$('select').attr('disabled',true);
		$('input').attr('readonly',true);			
		$('input[type="radio"]').attr('disabled',true);			
		$('.excluir_produto').hide();
		$('a[title="Adicionar produto"]').hide();
		$('#emitirOrcamento').hide();			
	}

	$('.valor_unitario').mask('#.##0,00', {reverse: true}); 
	$('#valor_desconto').mask('00,00', {reverse: true}); 
	$('#valor_desconto_orc').mask('00,00', {reverse: true}); 
	
	if( $('#valor_desconto').val() != "" ){
		$('#valor_desconto').attr('disabled',true);
	}
	
	if(	$('#comissao').val() == '0.01'){
		$('.valor_produto').attr('type', 'text');
		$('.valor_produto').attr('disabled', false);
		$('.valor_produto').addClass('form-control');
		$('.valor_produto').mask('#.##0,00', {reverse: true}); 
		$('.valor_unitario').hide();
		$('#aprovacao').show();

	}

	$('#dt_previsao').bind('change', function(){

		var dt_previsao	= 	$(this).val();
		var id			= 	$('#orcamento_id').val();
		if(dt_previsao != ''){
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministradorComercial/alterarDtPrevisao",
				async: true,
				data: { dt_previsao	: dt_previsao,
						id 			: id }
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				atualizacao_dados = 1;	
				if(dados.retorno == 'sucesso')
				{
					swal({
						title: "OK!",
						text: 'Previsão de fechamento alterada com sucesso!',
						type: "success"
					});
				}
			});
		}

	});

	$('#dt_previsao_prod').bind('change', function(){

		var dt_previsao	= 	$(this).val();
		var id			= 	$('#orcamento_id').val();
		if(dt_previsao != ''){
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministradorComercial/alterarDtPrevisaoProd",
				async: true,
				data: { dt_previsao_prod	: dt_previsao,
						id 			: id }
			}).done(function(data) {
				var dados = $.parseJSON(data);		
				atualizacao_dados = 1;	
				if(dados.retorno == 'sucesso')
				{
					swal({
						title: "OK!",
						text: 'Previsão de produção alterada com sucesso!',
						type: "success"
					});
				}
			});
		}

	});

	$('.editar_produto').bind('click', function(){
		var indice = $(this).attr('indice');
		$('#valor_produto_'+indice).removeAttr('disabled');
	});

	$('#observacao').bind('focusout', function(){

		var observacao	= 	$(this).val();
		var id			= 	$('#orcamento_id').val();
		
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/alterarObservacao",
			async: true,
			data: { observacao	: observacao,
					id 			: id }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso')
			{
				swal({
					title: "OK!",
					text: 'Observação alterada com sucesso!',
					type: "success"
				});
			}
		});

	});

	$('.bombas').bind('change', function(){

        indice 					= 	$(this).attr('indice');
        produto_id 				= 	$(this).val();
        orcamento_produto_id 	= 	$(this).attr('orcamento_produto_id');
        orcamento_id 			= 	$('#orcamento_id').val();
        status_id				= 	$('#status_id').val();
        produto 				= 	$(this).find('option:selected').text();
        produto_nr 				= 	indice;
        valor_base 				= 	$(this).find('option:selected').attr('valor_unitario');
        comissao 				= 	$("input[name='valor_desconto_orc']:checked").val();
		valor_tributo 			= 	$("#valor_tributo").val();
		$('#reajuste').val($(this).find('option:selected').attr('reajuste'));
		
        $('#comissao2').fadeIn('slow');

        if( $(this).find('option:selected').attr('tipo') != 'opcionais' && comissao != 'outro' ){
	       
	        if( comissao == '3' && valor_tributo == '7.00' ) {
                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '10.00' ) {
                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11) *1.12).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 4% de comissão ***********
            // ************************************
            }else if( comissao == '4' && valor_tributo == '7.00' ) {
                
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '10.00' ) {
                
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11) *1.12).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '12.00') {
                
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '18.00') {

               	var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 5% de comissão ***********
            // ************************************
            }else if(comissao == '5' && valor_tributo == '7.00'  ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
                if( estado == 'AC' || estado == 'AP' || estado == 'AM' || estado == 'PA' || estado == 'RO' || estado == 'RR' || estado == 'TO'){
                    valor_produto = valor_produto / 0.9798;
                }
            }else if(comissao == '5' && valor_tributo == '10.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 2% de comissão ***********
            // ************************************
            }else if(comissao == '2' && valor_tributo == '7.00') {

                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '10.00') {

                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '12.00') {

               
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '18.00') {

               
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 0% de comissão ***********
            // ************************************
            }else if(comissao == '0' && valor_tributo == '7.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '10.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '12.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '18.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            }else if(comissao == 'TB2') {
			    
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())).toFixed(2);
            }else if(comissao == 'TN' ) {                 
                var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat(retorno.fator)* 1.11).toFixed(2);
                valor_produto = valor_produto / 0.9798;
            }
	        
	        // Acréscimo de 20% sobre o valor final quando for para o nordeste
	        /*if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
	            $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
	            $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
	            $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
	        {
	            valor_produto = valor_produto * 1.2;
	            
	        }*/
	        /* Acréscimo de 12% no valor do produto geral 01/08/2022 */
            //valor_produto = Math.round(valor_produto * 1.12);            
            valor_produto = Math.round(parseFloat(valor_produto) * 1.12);
            /* Inserção do reajuste 30/11/2022 */            
            if( $('#reajuste').val() > 0 && $(this).find('option:selected').attr('tipo') != 'opcionais'){
            	
                valor_produto = Math.round(valor_produto * (1 + parseFloat($('#reajuste').val() / 100))).toFixed(2);    
            }else{
                valor_produto = valor_produto.toFixed(2);
            }
	        $('b.valor_unitario_'+indice).text(valor_produto);
	        $('#valor_produto_'+indice).val(valor_produto);
			$('b.valor_unitario_'+indice).unmask();
	        $('b.valor_unitario_'+indice).mask('#.##0,00', {reverse: true});

	    }else{

	        
	        var valor_produto = valor_base;
	        // Acréscimo de 20% sobre o valor final quando for para o nordeste
		    /*if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
		        $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
		        $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
		        $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
		    {
		        valor_produto = valor_produto * 1.2;
		
		    }*/

		    /* Acréscimo de 12% no valor do produto geral 01/08/2022 */
            //valor_produto = Math.round(valor_produto * 1.12);
            if($(this).find('option:selected').attr('tipo') != 'opcionais'){
            	valor_produto = Math.round(parseFloat(valor_produto) * 1.12).toFixed(2);
            }else{
            	valor_produto = valor_produto.toFixed(2);
            }
	    	$('b.valor_unitario_'+indice).text(valor_produto);
	        $('#valor_produto_'+indice).val(valor_produto);
			$('b.valor_unitario_'+indice).unmask();
			$('b.valor_unitario_'+indice).mask('#.##0,00', {reverse: true});
	    }
	    
	    
	    
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministradorComercial/alterarProdutoOrcamento", 
			async: false,
			data: { produto_id 				: 	produto_id,
					orcamento_produto_id 	: 	orcamento_produto_id,
					status_orcamento_id 	: 	status_id,
					orcamento_id 			: 	orcamento_id,
					produto_nr 				: 	produto_nr,
					produto 				:  	produto,
					valor_produto 			: 	valor_produto
					}
		}).done(function(data) {

			var dados = $.parseJSON(data);
			
			if(dados.retorno == 'sucesso')
			{			
				atualizacao_dados = 1;	
				swal({
					title: "OK!",
					text: 'Produto alterado com sucesso!',
					type: "success"
				}).then(function() {

					$('#valor_produto_'+indice).css('border','2px solid green');
					$('#valor_produto_'+indice).removeAttr('disabled');
					$('#valor_produto_'+indice).focus();	
					window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;				
					
				});
			}
		});
    }); 

	$('.entregaFretePagto').bind('change', function(){
		var orcamento_id 		= 	$(this).attr('orcamento_id');
		var valor 				= 	$(this).val();
		var tipo 				= 	$(this).attr('id');	
		var status_orcamento_id	= 	$(this).attr('status_id');
		
		var url = '';
		if( tipo == 'forma_pagto' ){
			url = 'alterarFormaPagto';
			desc_tipo = 'Forma de Pagamento';
		}else if( tipo == 'entregas' ) {
			url = 'alterarEntrega';
			desc_tipo = 'Forma de Entrega';
		}else if( tipo == 'frete'){
			url = 'alterarFrete';
			desc_tipo = 'Frete';
		}else{
			url = 'alterarGarantia';
			desc_tipo = 'Garantia';
		}

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministradorComercial/"+url, 
			async: true,
			data: { valor 				: 	valor,
					orcamento_id 		: 	orcamento_id,
					status_orcamento_id	: 	status_orcamento_id}
		}).done(function(data) {
			var dados = $.parseJSON(data);

			if(dados.retorno == 'sucesso')
			{
				atualizacao_dados = 1;
				swal({
					title: "OK!",
					text: desc_tipo+' alterado com sucesso!',
					type: "success"
				});
			}
		});
	});


	$('.editar_qtd').bind('click', function(){
		var indice = $(this).attr('indice');
		$('#qtd_'+indice).removeAttr('disabled');
	});

	$('.qtd').bind('focusout', function(){

		var indice 					= 	$(this).attr('indice');
		var qtd 					= 	$(this).val();
		var orcamento_produto_id 	= 	$(this).attr('orcamento_produto_id');
		var orcamento_id 			= 	$('#orcamento_id').val();
        var status_id				= 	$('#status_id').val();
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministradorComercial/alterarQtdProduto", 
			async: true,
			data: { qtd 					: 	qtd,
					orcamento_produto_id 	: 	orcamento_produto_id,
					status_orcamento_id 	: 	status_id,
					orcamento_id 			: 	orcamento_id  }
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso')
			{				
				atualizacao_dados = 1;	
				$('#valor_produto_'+indice).attr('qtd', qtd);
				swal({
					title: "OK!",
					text: 'Quantidade alterada com sucesso!',
					type: "success"
				}).then(function() {
					cadastraEmissao(orcamento_id);
			      	window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;
			   	});
			}

		});

	});	

	
	$('#aprovacao').bind('click', function(){
		var _retorno = 0;
        $('.valor_produto').each(function(){

        	var valor_produto 			= 	$(this).val();
			var orcamento_id 			= 	$(this).attr('orcamento_id');
			var orcamento_produto_id 	= 	$(this).attr('orcamento_produto_id');
			var orcamento_id 			= 	$('#orcamento_id').val();
	        var status_id				= 	$('#status_id').val();
			
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministradorComercial/alterarValorProduto", 
				async: false,
				data: { valor 		 			: 	valor_produto,
						id 						: 	orcamento_produto_id,
						status_orcamento_id 	: 	status_id,
						orcamento_id 			: 	orcamento_id,
						comissao 				: 	'outro' 	}
			}).done(function(data) {
				var dados = $.parseJSON(data);
				if(dados.retorno == 'sucesso')
				{			
					atualizacao_dados = 1;	
					_retorno = 1;
				}
			});

		});

		if( _retorno > 0 ){

			var orcamento_id 	= 	$('#orcamento_id').val();
			
			$.ajax({
				method: "POST",
				url:  	base_url+"AreaAdministradorComercial/alteraStatusOrcamento",
				async: 	true,
				data: {	status_orcamento 	: 	$('#status_id').val(),
						orcamento_id 		: 	orcamento_id }

			}).done(function(data) {
				
				var dados = $.parseJSON(data);				
				
				if(dados.retorno == 'sucesso')
				{
					atualizacao_dados = 1;		
					$.ajax({
						method: "POST",
						url:  base_url+"AreaAdministradorComercial/insereDescontoOrcamento", 
						async: true,
						data: {	valor_desconto 	:  'outro',
								orcamento_id 	: 	orcamento_id }
					}).done(function(data) {
						var dados = $.parseJSON(data);

					});

					swal({
						title: "OK!",
						text: 'Valores alterados com sucesso!',
						type: "success"
					}).then(function() {
						cadastraEmissao(orcamento_id);
				      	window.location = base_url+'AreaAdministradorComercial/orcamentosStatus/1';
				   	});
				}

			});

				
		}

	});
	
	$('.atualiza_valor').bind('click', function(){
		
		var valor = 0;		
		$('.valor_produto').each(function(){
			novo_valor = replaceAll($(this).val(), '.', '');
			novo_valor = replaceAll(novo_valor, ',', '.');
			valor = valor + (parseFloat(novo_valor) * parseInt($(this).attr('qtd')));
			
		});
		
		subtotal 				= 	valor.toFixed(2);
		valor_total 			= 	(valor + (parseFloat(valor.toFixed(2))/100 * parseFloat($('#valor_tributo').val())) + (parseFloat(valor.toFixed(2))/100  * 5)).toFixed(2);
		desconto 				= 	replaceAll($('#valor_desconto').val(), '.', '');
		desconto 				= 	replaceAll(desconto, ',', '.');				
		valor_total_desconto 	= 	(valor_total - (valor_total/100*parseFloat(desconto))).toFixed(2);		
		$('.valor_total').text(valor_total);
		$('.valor_subtotal').text(subtotal);
		$('.valor_final_desconto').text(valor_total_desconto);
		$('.valor_total').mask('#.##0,00', {reverse: true}); 
		$('.valor_subtotal').mask('#.##0,00', {reverse: true});
		$('.valor_final_desconto').mask('#.##0,00', {reverse: true});
	});

	$('#alterar_valor').bind('click', function(){

		orcamento_id 	= 	$(this).attr('orcamento_id');	
		status_id 		= 	$(this).attr('status_id');	
		valor_orcamento = 	$('#valor_orcamento').val();
			
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministradorComercial/alterarValorOrcamento", 
			async: true,
			data: { orcamento_id 	: 	orcamento_id,
					valor_orcamento : 	valor_orcamento,
					status_id 		: 	status_id}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso')
			{				
				atualizacao_dados = 1;	
				swal({
						title: "OK!",
						text: 'Valor do Orçamento alterado com sucesso!', 
						type: "success"
					}).then(function() {
						cadastraEmissao(orcamento_id);
				      	window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;
				   	});

			}else{		
				swal({
					title: "OPS!",
					text: 'Alguma inconsistência ocorreu, contate o webmaster!',
					type: "warning"
				}).then(function() {
			      	window.location = base_url+'AreaAdministradorComercial/orcamentosStatus/1';
			   	});
			}

		});		

	});

	$("#geraPdf").bind('click', function(){

		orcamento_id 	= 	$(this).attr('orcamento_id');	
		contato 		= 	$('#contato_posto').val();			
		fl_especial 	= 	$('#fl_especial').val();
				
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministradorComercial/verificaEmissao",
			async: true,
			data: { orcamento_id 	: 	orcamento_id 	}
		}).done(function(data) {
			var dados = $.parseJSON(data);	
			console.log(dados.dias+' >= 5');	
			if(fl_especial == 1 && dados.dias >= 5 && atualizacao_dados == 0){
				$('#modal_reemissao').modal('show');

			}else if(fl_especial == 0 && dados.dias >= 15 && atualizacao_dados == 0	){
				$('#modal_reemissao').modal('show');

			}else{
				geraPdfOrcamento(orcamento_id, contato);				

			}
			
		});		

		
	});

	$('#adicionar_motivo').bind('click', function(){
		orcamento_id 		= 	$(this).attr('orcamento_id');
		status_orcamento_id = 	$(this).attr('status_orcamento');
		motivo_emissao 		= 	$('#motivo_emissao').val();
		contato 			= 	$('#contato_posto').val();

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministradorComercial/insereMotivoReemissao",
			async: true,
			data: { orcamento_id 		: 	orcamento_id,					
					motivo_emissao 		: 	motivo_emissao,
					status_orcamento_id : 	status_orcamento_id 	}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			if(dados.retorno == 'sucesso'){
				swal({
					title: "OK!",
					text: 'Motivo inserido com sucesso!',
					type: "success"
				}).then(function() {
			      	window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;
			   	});
				geraPdfOrcamento(orcamento_id, contato);		
				
			}else{
				swal(
			  		'Ops!',
			  		'Algum erro aconteceu!',
			  		'warning'
				);
			}

		});

	});

	$("#valor_desconto").bind('focusout', function(){
		
		if( $(this).val() != "" )
		{
			$("#motivo_desconto").removeAttr('disabled');
		}else{
			$("#motivo_desconto").attr('disabled',true);
		}
	});

	$("#valor_desconto_orc").bind('focusout', function(){
		
		if( $(this).val() != "" )
		{
			$("#motivo_desconto_orc").removeAttr('disabled');
		}else{
			$("#motivo_desconto_orc").attr('disabled',true);
		}
	});

	$("#emitir_orcamento").bind('click', function(){

		orcamento_id 		= 	$(this).attr('orcamento_id');
		email_destino		= 	$('#email_destino').val();		
		motivo_desconto		= 	$('#motivo_desconto').val();
		valor_tributo		= 	$('#valor_tributo').val();		
		valor_total			= 	$('#valor_total').val();
		status_orcamento_id	= 	$(this).attr('status_id');
		
		var erro = 0;

		if( email_destino == "" ){
			swal(
		  		'Ops!',
		  		'Insira um e-mail para ser enviado o orçamento!',
		  		'warning'
			);
			erro++;
		}

		var emailFilter=/^.+@.+\..{2,}$/;
		var illegalChars= /[\(\)\<\>\,\;\:\\\/\"\[\]]/		
		if(!(emailFilter.test(email_destino)) || email_destino.match(illegalChars))        
        {
	   		swal(
		  		'Ops!',
		  		'Insira um e-mail válido!',
		  		'warning'
			);
			erro++;
        }		

		if( erro == 0 ){
			
			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministradorComercial/emitirOrcamento", 
				async: true,
				data: { orcamento_id 		: 	orcamento_id,
						email_destino		: 	email_destino,						
						valor_tributo 		: 	valor_tributo,
						valor_total 		: 	valor_total,
						status_orcamento_id : 	status_orcamento_id } 

			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{			
					
					swal({
						title: "OK!",
						text: 'Orçamento Emitido com sucesso!',
						type: "success"
					}).then(function() {
				      	window.location = base_url+'AreaAdministradorComercial/orcamentosStatus/1';
				   	});

				}else{
			
					swal({
						title: "OPS!",
						text: 'Alguma inconsistência ocorreu, contate o webmaster!',
						type: "warning"
					}).then(function() {
				      	window.location = base_url+'AreaAdministradorComercial/orcamentosStatus/1';
				   	});
				}

			});		
		}
	});

	$("#adicionar_desconto_orc").bind('click', function(){
		var valor_desconto = 0;
		$('input[name="valor_desconto_orc"]').each(function(){
			if($(this).is(':checked')){
				valor_desconto = $(this).val();
			}
		});
					
		var orcamento_id 		= 	$(this).attr('orcamento_id');
		var erro = 0;
		if(valor_desconto == ""){
			swal(
		  		'Ops!',
		  		'Insira um desconto para o orçamento!',
		  		'warning'
			);
			erro++;	
		}
				
		if( erro == 0 ){

			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministradorComercial/insereDescontoOrcamento", 
				async: true,
				data: { orcamento_id 		: 	orcamento_id,
						valor_desconto 		: 	valor_desconto }

			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{				
					swal({
						title: "OK!",
						text: 'Desconto adicionado ao orçamento com sucesso!',
						type: "success"
					}).then(function() {
				      	window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;
				   	});
				}else{
					swal({
						title: "OPS!",
						text: 'Alguma inconsistência ocorreu, contate o webmaster!',
						type: "warning"
					}).then(function() {
				      	window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;
				   	});
				}

			});		
		}


	});

	$('.add').bind('click', function(){

        var html = $('#modelo').clone();                
        var indice = parseInt($('#modelo').attr('total_indice')) + 1;
        $(html).find('.remover').css('display', 'block');
        $(html).find('.remover').attr('indice', indice);
        $(html).find('.bombas_new').attr('indice', indice);
        $(html).find('.bombas_new').attr('id', 'bombas_new_'+indice);
        $(html).find('.valor_unitarios').attr('indice', indice);        
        $(html).find('.valor_unitarios_text').attr('indice', indice);
        $(html).find('.valor_unitarios_text').text('');
        $(html).find('.valor_unitarios_text').addClass('valor_unitarios_text_'+indice);
        $(html).find('.valor_unitarios_text').removeClass('valor_unitarios_text_0');
        
        $(html).find('.valor_unitarios').addClass('valor_unitarios_'+indice);
        $(html).find('.valor_unitarios').removeClass('valor_unitarios_0');
        $(html).find('.valor_unitarios').attr('indice', indice);
        $(html).find('.valor_unitarios').attr('id', 'valor_unitarios_'+indice);

        body = "<tr indice='"+indice+"'>";
        body+= $(html).html();                
        body+="</tr>";                
        $('table.novo').append(body);                
        $('#modelo').attr('total_indice',indice);

        excluir();
        mostra_valor();        
    });
    
    mostra_valor();

    $('#adicionar_produto').bind('click', function(){
    	var dados 			= 	$('#produtos_novos').serializeArray();
    	orcamento_id 		= 	$(this).attr('orcamento_id');
    	status_orcamento_id = 	$(this).attr('status_id');
		var inputVazios = $('.novo').find('input').filter(function () {
        	return this.value.split(' ').join('') == '';
    	});

    	var selectVazios = $('.novo').find('select').filter(function () {
        	return this.value.split(' ').join('') == '';
    	});

		if( selectVazios.length > 0 || inputVazios.length > 0){
			swal({
				title: "Atenção!",
				text: 'Preencha corretamente os campos dos novos produtos!',
				type: "warning"
			});	
		}else{

			$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministradorComercial/insereProdutosNovosOrcamento", 
				async: true,
				data: { dados				: 	dados,
						orcamento_id 		: 	orcamento_id,
						status_orcamento_id : 	status_orcamento_id }

			}).done(function(data) {
				var dados = $.parseJSON(data);		
				
				if(dados.retorno == 'sucesso')
				{
					swal({
						title: "OK!",
						text: 'Produto(s) adicionado(s) ao orçamento com sucesso!',
						type: "success"
					}).then(function() {
						cadastraEmissao(orcamento_id);
				      	window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;
				   	});
				}else{
					swal({
						title: "OPS!",
						text: 'Alguma inconsistência ocorreu, contate o webmaster!',
						type: "warning"
					}).then(function() {
				      	window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;
				   	});
				}
			});	
		}
	});

	/* Slider */
	$('#valor_desconto_orc').slider({
		formatter: function(value) {
			return 'Desconto: ' + value + '%';
		}
	});
	console.log($('input[name="valor_desconto_orc"]:checked').val());
	if($('input[name="valor_desconto_orc"]:checked').val() == '2'){
            
            $('.bombas option[value="14"]').hide();                               
            $('.bombas option[value="68"]').hide();
            $('.bombas option[value="70"]').hide();
            $('.bombas option[value="15"]').hide();
            $('.bombas option[value="16"]').hide();
            $('.bombas option[value="17"]').hide();
            $('.bombas option[value="18"]').hide();
            $('.bombas option[value="19"]').hide();
            $('.bombas option[value="20"]').hide();
            $('.bombas option[value="21"]').hide();
            $('.bombas option[value="22"]').hide();
            $('.bombas option[value="23"]').hide();
            $('.bombas option[value="24"]').hide();
            $('.bombas option[value="170"]').hide();
            $('.bombas option[value="26"]').hide();
            $('.bombas option[value="29"]').hide();
            $('.bombas option[value="34"]').hide();
            $('.bombas option[value="35"]').hide();
            $('.bombas option[value="36"]').hide();
            $('.bombas option[value="37"]').hide();
            $('.bombas option[value="38"]').hide();
            $('.bombas option[value="39"]').hide();
            $('.bombas option[value="45"]').hide();
            $('.bombas option[value="46"]').hide();
            $('.bombas option[value="47"]').hide();
            $('.bombas option[value="64"]').hide();
            $('.bombas option[value="66"]').hide();
            $('.bombas option[value="69"]').hide();
            $('.bombas option[value="200"]').hide();
            $('.bombas option[value="289"]').hide();
            $('.bombas option[value="295"]').hide();
            $('.bombas option[value="296"]').hide();
            $('.bombas option[value="297"]').hide();
            $('.bombas option[value="298"]').hide();
            $('.bombas option[value="308"]').hide();
        }

	$('.comissao').bind('click', function(){
		console.log($(this).val());
		
		if( $(this).val() == 'outro' ){
			
			$('.valor_produto').attr('type', 'text');
			$('.valor_produto').attr('disabled', false);
			$('.valor_produto').addClass('form-control');
			$('.valor_produto').mask('#.##0,00', {reverse: true}); 
			$('.valor_unitario').hide();
			$('#aprovacao').show();
			
		}else{
			
			$('#aprovacao').hide();

			swal({
			  title: 'Alterar tabela?',
			  text: "Deseja realmente alterar a tabela de preços desses produtos?",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Sim!'
			}).then((result) => {
				if (result.value) {

					atualiza_valores($(this).val());
					cadastraEmissao(orcamento_id);
				}
			});
		}
	});

	$( "#nome_indicador" ).autocomplete({
		source: base_url+"AreaAdministrador/retornaIndicadores",
		minLength: 2,
		select: function( event, ui ) {

			$('#nome_indicador').val(ui.item.id);
			var indicador 		= 	ui.item.id;
	    	var orcamento_id 	= 	$('#nome_indicador').attr('orcamento_id');
	    	var status_orcamento_id	= 	$('#status_id').val();

	    	$.ajax({
				method: "POST",
				url:  base_url+"AreaAdministrador/alteraIndicadorOrcamento", 
				async: true,
				data: { indicador			: 	indicador,
						orcamento_id 		: 	orcamento_id,
						status_orcamento_id : 	status_orcamento_id }

			}).done(function(data) {

				var dados = $.parseJSON(data);
				if(dados.retorno == 'sucesso'){
					swal({
						title: "OK!",
						text: 'Indicador atualizado com sucesso!',
						type: "success"
					});
					cadastraEmissao(orcamento_id);
					$('#alterar_indicador').text($('#nome_indicador').val());

				}else{
					swal({
						title: "OPS!",
						text: 'Acontenceu um erro inesperado, entre em contato com o webmaster!',
						type: "warning"
					});
				}
	    	});
		}
    });

	$('.excluir_indicador').bind('click', function(){
		indicador = '';
        orcamento_id = $(this).attr('orcamento_id');
        
        $.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/alteraIndicadorOrcamento", 
			async: true,
			data: { indicador		: 	indicador,
					orcamento_id 	: 	orcamento_id }

		}).done(function(data) {

			var dados = $.parseJSON(data);
			if(dados.retorno == 'sucesso'){
				swal({
					title: "OK!",
					text: 'Indicador excluído com sucesso!',
					type: "success"
				});

				$("#alterar_indicador").text('NÃO HOUVE INDICAÇÃO PARA ESTE ORÇAMENTO');
			}else{
				swal({
					title: "OPS!",
					text: 'Acontenceu um erro inesperado, entre em contato com o webmaster!',
					type: "warning"
				});
			}
    	});	
	});

	$('.indicacao').bind('click', function(){
        if($(this).is(':checked')){
            $('.nome_indicador').fadeIn('slow');
            $('#nome_indicador').attr('required','required');

        }else{

            $('.nome_indicador').fadeOut('slow');
            $('#nome_indicador').removeAttr('required');
            $('#nome_indicador').val('');
          
        }
    });
});

function excluir()
{
    $('.remover').bind('click', function(){
        var indice = $(this).attr('indice');
        $('.novo').find('tr[indice="'+indice+'"]').remove(); 
    });    
}

function mostra_valor()
{
    $('.bombas_new').bind('change', function(){
        indice = $(this).attr('indice');
        var tipo 		= 	$(this).find('option:selected').attr('tipo');
        var valor_base 	= 	$(this).find('option:selected').attr('valor_unitario');
        comissao 		= 	$('input[name="valor_desconto_orc"]:checked').val();        
       	valor_tributo	=	$("#valor_tributo").val();
       	reajuste 		= 	$(this).find('option:selected').attr('reajuste');

        if( tipo != 'opcionais' && comissao != 'outro' ){
	       
	        if( comissao == '3' && valor_tributo == '7.00' ) {
                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else  if( comissao == '3' && valor_tributo == '10.00' ) {
                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11) *1.12).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if( comissao == '3' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 4% de comissão ***********
            // ************************************
            }else if( comissao == '4' && valor_tributo == '7.00' ) {
                
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '10.00' ) {
                
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '12.00') {
                
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if( comissao == '4' && valor_tributo == '18.00') {

               	var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 5% de comissão ***********
            // ************************************
            }else if(comissao == '5' && valor_tributo == '7.00'  ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
                if( estado == 'AC' || estado == 'AP' || estado == 'AM' || estado == 'PA' || estado == 'RO' || estado == 'RR' || estado == 'TO'){
                    valor_produto = valor_produto / 0.9798;
                }
            }else if(comissao == '5' && valor_tributo == '10.00'  ) {

                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '12.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '5' && valor_tributo == '18.00' ) {

                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 2% de comissão ***********
            // ************************************
            }else if(comissao == '2' && valor_tributo == '7.00') {

                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '10.00') {

                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '12.00') {

               
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '2' && valor_tributo == '18.00') {

               
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            // ************************************
            // ********* 0% de comissão ***********
            // ************************************
            }else if(comissao == '0' && valor_tributo == '7.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '10.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '12.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
            }else if(comissao == '0' && valor_tributo == '18.00') {
               
               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
            }else if(comissao == 'TB2') {
			    
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())).toFixed(2);
            }else if(comissao == 'TN'  ) {                    
                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val())* 1.11).toFixed(2);
                valor_produto = valor_produto / 0.9798;
            }    

            // Acréscimo de 20% sobre o valor final quando for para o nordeste
           /* if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
                $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
                $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
                $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
            {
                valor_produto = valor_produto * 1.2;
            }
	        */
	        /* Acréscimo de 12% no valor do produto geral 01/08/2022 */            
            valor_produto = Math.round(parseFloat(valor_produto) * 1.12);
            /* Inserção do reajuste 30/11/2022 */
            
            if( reajuste > 0){            
                valor_produto = Math.round(valor_produto * (1 + parseFloat(reajuste / 100))).toFixed(2);    
            }else{
                valor_produto = valor_produto.toFixed(2);
            }
	        $('.valor_unitarios_text_'+indice).text(valor_produto);
	        $('#valor_unitarios_'+indice).val(valor_produto);
	        $('.valor_unitarios_text_'+indice).unmask();	
	        $('.valor_unitarios_text_'+indice).mask('#.##0,00', {reverse: true});

	    }else{

	    	/*if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
                $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
                $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
                $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
            {
                valor_produto = valor_base * 1.2;
            }*/
            /* Acréscimo de 12% no valor do produto geral 01/08/2022 */
            if(tipo != 'opcionais' ){
            	valor_produto = Math.round(parseFloat(valor_base) * 1.12).toFixed(2);            
            }else{
            	valor_produto = Math.round(parseFloat(valor_base)).toFixed(2);
            }
	    	$('.valor_unitarios_text_'+indice).text(valor_produto);
	        $('#valor_unitarios_'+indice).val(valor_produto);
	        $('.valor_unitarios_text_'+indice).unmask();	
	        
	    }
        
	}); 

}
   
function excluirProduto(orcamento_produto_id,indice,orcamento_id){ 
	var total_linhas = $('.novo_produto_orc').length;
	if( total_linhas == 1 ){
		swal({
			title: "OPS!",
			text: 'Impossível remover este produto. Altere o status do orçamento para "CANCELADO"!',
			type: "warning"
		});
	}else{

		swal({
		  title: 'Excluir?',
		  text: "Deseja realmente excluir esse produto?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Sim!'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					method: "POST",
					url:  base_url+"AreaAdministradorComercial/excluirProdutoOrcamento", 
					async: true,
					data: { orcamento_produto_id	: 	orcamento_produto_id }

				}).done(function(data) {
					var dados = $.parseJSON(data);		
				
					if(dados.retorno == 'sucesso')
					{				
						swal({
							title: "OK!",
							text: 'Produto(s) excluído(s) do orçamento com sucesso!',
							type: "success"
						}).then(function() {
							cadastraEmissao(orcamento_id);
			      			window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;
			   			});

						$('tr[indice="'+indice+'"]').remove();

					}else{
						swal({
							title: "OPS!",
							text: 'Alguma inconsistência ocorreu, contate o webmaster!',
							type: "warning"
						}).then(function() {
				      		window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;
				   		});
					}

				});	
			}
		});
	}
}

function replaceAll(str, de, para){
    var pos = str.indexOf(de);
    while (pos > -1){
		str = str.replace(de, para);
		pos = str.indexOf(de);
	}
    return (str);
}

function atualiza_valores(comissao){

	indice = 1;
	erro = 0;

    $('b.valor_unitario').each(function(){

    	var valor_base 		= 	$(this).attr('valor_base');
    	var valor_tributo	=	$("#valor_tributo").val();
    	var reajuste 		= 	$(this).attr('reajuste');
    	if(valor_tributo == '7.00'){
            $('#tn').fadeIn('slow');
        }else{
            $('#tn').fadeOut('slow');
            
        }

    	if($(this).attr('tipo') != 'opcionais'){
	        if($(this).attr('valor_base') != '' && $(this).attr('valor_base') != undefined )  {

		        if( comissao == '3' && valor_tributo == '7.00' ) {
	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
	            }else if( comissao == '3' && valor_tributo == '10.00' ) {
	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6805) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
	            }else if( comissao == '3' && valor_tributo == '12.00' ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
	            }else if( comissao == '3' && valor_tributo == '18.00' ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6805) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
	            // ************************************
	            // ********* 4% de comissão ***********
	            // ************************************
	            }else if( comissao == '4' && valor_tributo == '7.00' ) {
	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
	            }else if( comissao == '4' && valor_tributo == '10.00' ) {
	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.637) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
	            }else if( comissao == '4' && valor_tributo == '12.00') {
	                
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
	            }else if( comissao == '4' && valor_tributo == '18.00') {

	               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.637) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
	            // ************************************
	            // ********* 5% de comissão ***********
	            // ************************************
	            }else if(comissao == '5' && valor_tributo == '7.00'  ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
	                if( estado == 'AC' || estado == 'AP' || estado == 'AM' || estado == 'PA' || estado == 'RO' || estado == 'RR' || estado == 'TO'){
                        valor_produto = valor_produto / 0.9798;
                    }
	            }else if(comissao == '5' && valor_tributo == '10.00'  ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
	            }else if(comissao == '5' && valor_tributo == '12.00' ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
	            }else if(comissao == '5' && valor_tributo == '18.00' ) {

	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.583) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
	            // ************************************
	            // ********* 2% de comissão ***********
	            // ************************************
	            }else if(comissao == '2' && valor_tributo == '7.00') {

	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
	            }else if(comissao == '2' && valor_tributo == '10.00') {

	                //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.6945) * parseFloat(retorno.fator)).toFixed(2);
	                var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
	            }else if(comissao == '2' && valor_tributo == '12.00') {

	               
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
	            }else if(comissao == '2' && valor_tributo == '18.00') {

	               
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
	            // ************************************
	            // ********* 0% de comissão ***********
	            // ************************************
	            }else if(comissao == '0' && valor_tributo == '7.00') {

	               //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)).toFixed(2);
	               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11).toFixed(2);
	            }else if(comissao == '0' && valor_tributo == '10.00') {

	               //var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.7200) * parseFloat(retorno.fator)).toFixed(2);
	               var valor_produto = Math.round(parseFloat(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.11)*1.12).toFixed(2);
	            }else if(comissao == '0' && valor_tributo == '12.00') {
	               
	               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.09).toFixed(2);
	            }else if(comissao == '0' && valor_tributo == '18.00') {
	               
	               var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.7200) * parseFloat($('#fator').val()) * 1.07).toFixed(2);
	            }else if(comissao == 'TB2') {
				    
	                var valor_produto = Math.round(parseFloat(parseFloat(valor_base) / 0.6945) * parseFloat($('#fator').val())).toFixed(2);
	            }else if(comissao == 'TN'  ) {                    
                    var valor_produto = Math.round(parseFloat(parseFloat($(this).attr('valor_base')) / 0.583) * parseFloat($('#fator').val())* 1.11).toFixed(2);
                    valor_produto = valor_produto / 0.9798;
                }              
	            
	        }
	    }else{
	    	valor_produto = $(this).attr('valor_base');	  	
	    	
	    }

	    // Acréscimo de 20% sobre o valor final quando for para o nordeste
        /*if( $('#estado').val() == 'AC' || $('#estado').val() == 'AL' || $('#estado').val() == 'AP' ||
            $('#estado').val() == 'AM' || $('#estado').val() == 'BA' || $('#estado').val() == 'CE' || 
            $('#estado').val() == 'MA' || $('#estado').val() == 'PA' || $('#estado').val() == 'PB' || $('#estado').val() == 'PE' ||
            $('#estado').val() == 'PI' || $('#estado').val() == 'RN' || $('#estado').val() == 'RR' || $('#estado').val() == 'SE' )
        {
            valor_produto = valor_produto * 1.2;
        }*/
        /* Acréscimo de 12% no valor do produto geral 01/08/2022 */
        //valor_produto 			= 	Math.round(valor_produto * 1.12);
        
        /* Inserção do reajuste 30/11/2022 */        
        if( reajuste > 0 && $(this).attr('tipo') != 'opcionais'){
        	valor_produto = Math.round(parseFloat(valor_produto) * 1.12);
            valor_produto = Math.round(valor_produto * (1 + parseFloat(reajuste / 100))).toFixed(2);    
        }else{
            valor_produto = Math.round(parseFloat(valor_produto)).toFixed(2);
        }

        orcamento_produto_id 	= 	$(this).attr('orcamento_produto_id');
        orcamento_id 			=	$(this).attr('orcamento_id');

        $.ajax({        	
			method: "POST",
			url:  base_url+"AreaAdministradorComercial/alterarValorProduto",
			async: true,
			data: { id 				: 	orcamento_produto_id,
					valor 			: 	valor_produto,
					comissao 		: 	comissao,
					orcamento_id 	: 	orcamento_id }

		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno == 'sucesso')
			{		
				cadastraEmissao(orcamento_id);
				erro = 0;
				
			}else{
				erro = 1;

			}

		});		
      
		indice++; 
        
    });

    if( erro ==0 ){

	    swal({
			title: "OK!",
			text: 'Valor da Comissão Alterada com Sucesso!',
			type: "success"
		}).then(function() {
      		window.location = base_url+'AreaAdministradorComercial/visualizarOrcamento/'+orcamento_id;
   		});

	}
    
}

function atualizaTotal(){
	total 		= 0;
	indice_qtd 	= 1;
	$('.valor_produto').each(function(){
		
		total = parseFloat(parseFloat(parseFloat($(this).val())  *  parseFloat($('#qtd_'+indice_qtd).val())) + parseFloat(total));
		indice_qtd++;	
	});
	
	$('.valor_subtotal').text(total);	
	$('.valor_subtotal').unmask();
    $('.valor_subtotal').mask('###.##0,00', {reverse: true}); 

 }

 function geraPdfOrcamento(orcamento_id, contato){

	if( contato != '' ) {

		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministradorComercial/geraOrcamento/"+orcamento_id+"/ajax/"+contato, 
			async: true,
			data: { orcamento_id 	: 	orcamento_id,
					contato 		: 	contato			}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno != 'erro')
			{				
				$('#m_modal_10').modal('hide');
				window.open(base_url+'pdf/'+dados.retorno);
			}

		});		

	}else{
		swal({
			title: "OPS!",
			text: 'Preencha todos os dados para continuar!',
			type: "warning"
		});
	}	
	
 }

function cadastraEmissao(orcamento_id){
	var status_orcamento_id = $('#status_id').val();
	
	$.ajax({
		method: "POST",
		url:  base_url+"AreaAdministradorComercial/insereMotivoReemissao",
		async: true,
		data: { orcamento_id 		: 	orcamento_id,					
				motivo_emissao 		: 	'Alteração no orçamento.',
				status_orcamento_id : 	status_orcamento_id 	}
	}).done(function(data) {
		var dados = $.parseJSON(data);
	});

}