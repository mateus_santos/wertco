$(document).ready(function(){	
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 2, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');

    $('#enviar').bind('click', function(){    	
    	$('#rel_chamados').slideDown('slow');
    	var produto_id	= 	$('#produto_id').val();    	
	    $.ajax({
			method: "POST",
			url: base_url+"AreaAdministrador/previsaoVendasDetalhadoAjax",
			async: true,
			data: { produto_id 	: produto_id 	}
		}).done(function( data ) {
			var dados = $.parseJSON(data);			
			$('#relatorio').empty();
			$('#relatorio').append(dados.retorno);
			datatable();
		
		});
	});	

});

$('.datepicker').mask('99/99/9999');

$('.datepicker').datepicker({
    format: 'dd/mm/yyyy'
    
});

function andamento(produto_id,modelo)
{
	
	$('.m_andamento_titulo').text('Oportunidades refente a '+modelo);

	$.ajax({
		method: "POST",
		url: base_url+"AreaAdministrador/buscaOportunidades",
		async: true,
		data: { produto_id 	: produto_id 	}
	}).done(function(data){
		var dados = $.parseJSON(data);
		$('.andamento').empty();
		$('.andamento').append(dados.retorno);
		$('#m_andamentos').modal({
			show: true
		})
		datatable_andamento();
	});
}

function datatable()
{

	$('#html_table').DataTable({
    	"scrollX": true,
    	"order": [[ 4, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table_wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table_wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');
}

function datatable_andamento()
{

	$('#html_table_').DataTable({
    	"scrollX": true,
    	"order": [[ 3, "desc" ]],
		"language": {
			"sEmptyTable": "Nenhum registro encontrado",
		    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
		    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
		    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
		    "sInfoPostFix": "",
		    "sInfoThousands": ".",
		    "sLengthMenu": "_MENU_    Resultados por página",
		    "sLoadingRecords": "Carregando...",
		    "sProcessing": "Processando...",
		    "sZeroRecords": "Nenhum registro encontrado",
		    "sSearch": "Pesquisar",
		    "oPaginate": {
		        "sNext": '<i class="la la-angle-double-right"></i>',
		        "sPrevious": '<i class="la la-angle-double-left"></i>',
		        "sFirst": "Primeiro",
		        "sLast": "Último"
		    },
		    "oAria": {
		        "sSortAscending": ": Ordenar colunas de forma ascendente",
		        "sSortDescending": ": Ordenar colunas de forma descendente"
		    }
		}
	});
    
 	$('#html_table__wrapper').removeClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
	$('#html_table__wrapper').addClass('m-datatable m-datatable--default m-datatable--brand m-datatable--loaded');
	$('a[aria-controls="html_table"]').addClass('m-datatable__pager-link m-datatable__pager-link');
	$('.dataTables_filter').css('float','right');
	$('.dataTables_scrollHeadInner').attr('style','box-sizing: content-box;width: 100%;padding-right: 0px;');
	$('table.no-footer').attr('style','margin-left: 0px; width: 100%;');
	$('table thead').css('background-color', '#f4f3f8');
	//$('table thead').css('height', '150px');
	$('table tbody tr:odd').addClass('zebraUm');
	$('table tbody tr:even').addClass('zebraDois');
}

function gerarPdf(id,tipo){

	if(tipo=='Pedido'){
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/geraPedidosPdf/"+id+"/ajax/", 
			async: true,
			data: { pedido_id : id}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno != 'erro')
			{				
				window.open(base_url+'pedidos/'+dados.retorno);

			}else{
				swal({
		   			title: "Atenção!",
		   			text: "Erro ao gerar pdf do pedido, entre em contato com o webmaster",
		   			type: 'warning'
		    	}).then(function() {
		    	   	
		    	});
			}

		});		
	}else{
		$.ajax({
			method: "POST",
			url:  base_url+"AreaAdministrador/geraOrcamento/"+id+"/ajax/Wertco", 
			async: true,
			data: { orcamento_id 	: 	id,
					contato 		: 	'Wertco'			}
		}).done(function(data) {
			var dados = $.parseJSON(data);		
			
			if(dados.retorno != 'erro')
			{				
				$('#m_modal_10').modal('hide');
				window.open(base_url+'pdf/'+dados.retorno);
			}

		});
	}
}