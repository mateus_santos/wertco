  -- phpMyAdmin SQL Dump
  -- version 5.1.0
  -- https://www.phpmyadmin.net/
  --
  -- Host: 127.0.0.1
  -- Tempo de geração: 20-Jul-2021 às 22:47
  -- Versão do servidor: 10.4.18-MariaDB
  -- versão do PHP: 8.0.3

  SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
  START TRANSACTION;
  SET time_zone = "+00:00";


  /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
  /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
  /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
  /*!40101 SET NAMES utf8mb4 */;

  --
  -- Banco de dados: `novaandes`
  --

  -- --------------------------------------------------------

  --
  -- Estrutura da tabela `orcamento_responsavel`
  --

  CREATE TABLE `orcamento_responsavel` (
    `id` int(11) NOT NULL,
    `orcamento_id` int(11) NOT NULL,
    `usuario_id` int(11) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Índices para tabelas despejadas
  --

  --
  -- Índices para tabela `orcamento_responsavel`
  --
  ALTER TABLE `orcamento_responsavel`
    ADD PRIMARY KEY (`id`,`orcamento_id`);

  --
  -- AUTO_INCREMENT de tabelas despejadas
  --

  --
  -- AUTO_INCREMENT de tabela `orcamento_responsavel`
  --
  ALTER TABLE `orcamento_responsavel`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  COMMIT;

  /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
  /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
  /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
