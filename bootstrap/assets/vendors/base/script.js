jQuery(function($) {

  var modelo_final;
  
  var modelo_Json;
  var modelo_tipo;
  var industrial;

  var unidade;

  $(document).ready(function(){
            //Check the support for the File API support
            if (window.File && window.FileReader && window.FileList && window.Blob) {
              var fileSelected = document.getElementById('txtfiletoread');
              fileSelected.addEventListener('change', function (e) {
                    //Set the extension for the file
                    //Get the file object
                    var fileTobeRead = fileSelected.files[0];
                    //Check of the extension match
                        //Initialize the FileReader object to read the 2file
                        var fileReader = new FileReader();
                        fileReader.onload = function (e) {
                          var obj = JSON.parse(fileReader.result);
                          seletorModelo(obj);
                          setPrenchimento(obj);
                        }
                        fileReader.readAsText(fileTobeRead);

                      }, false);
            }
            else {
              alert("Arquivo(s) n���o suportado(s)");
            }
          });


  function seletorModelo(object){
    $('#jsonSelect option').each(function() {
              // se localizar a frase, define o atributo selected
              if($(this).attr('value') == object.modelo) {
               $(this).prop('selected', true);

             }
           });
  }

  $( "#jsonSelect1,#jsonSelect2,#jsonSelect3,#jsonSelect4,#jsonSelect5,#jsonSelect6,#jsonSelect7,#jsonSelect8,#jsonSelect9,"+
   "#jsonSelect10,#jsonSelect11,#jsonSelect12,#jsonSelect13,#jsonSelect14,#jsonSelect15,#jsonSelect16,#jsonSelect17,#jsonSelect18,"+
   "#jsonSelect19,#jsonSelect20,#jsonSelect21,#jsonSelect22,#jsonSelect23,#jsonSelect24,#jsonSelect25,#jsonSelect26,#jsonSelect27,"+
   "#jsonSelect28,#jsonSelect29,#jsonSelect30,#jsonSelect31,#jsonSelect32,#jsonSelect33,#jsonSelect34").change(function() {


    $('.jsn').attr('hidden',true);
    $('.nenh').attr('hidden',true);

    var $el = $('#txtfiletoread');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();

    try{
      var rawFile = new XMLHttpRequest();
              //rawFile.open("GET", "/jsons/"+$( "#jsonSelect" ).val()+".jsn", false);
              modelo_Json = $(this).val();
              rawFile.open("GET", "/bootstrap/jsons/"+$(this).val()+".jsn", false);
              rawFile.onreadystatechange = function ()
              {
                if(rawFile.readyState === 4)
                {

                  if(rawFile.status === 200 || rawFile.status == 0)
                  {
                    var allText = rawFile.responseText;
                    var obj = JSON.parse(allText);
                    setPrenchimento(obj);
                    setExplicacao(obj.modelo);
                    $('.nenh').attr('hidden',true);

                  }
                  else if(rawFile.status === 404){
                    $('.nenh').removeAttr('hidden');
                  }
                }
              }
              rawFile.send(null);
            }
            catch(e){
              setPrenchimento(null);
            }
          });


   $("#unidade").change(function() {

    if($("#unidade").val() != unidade){

      unidade = $("#unidade").val();

      if(unidade === 'litros'){
       var a =  ($('#ponto_baixa_vazao').val()*3.1);

       $('#ponto_baixa_vazao').val(a.toFixed(1));
     }else{
      var a =  ($('#ponto_baixa_vazao').val()/3.1);
      $('#ponto_baixa_vazao').val(a.toFixed(1));
    }
  }

});




   $( "#jsonTipoSelect" ).change(function() {

    $('.jsnSelect').attr('hidden',true);

    var tipo = $( "#jsonTipoSelect" ).val();


    switch(tipo){

      case "1":
      $("#jsonSelect1").removeAttr('hidden');
      break;
      case "2":
      $("#jsonSelect2").removeAttr('hidden');
      break;
      case "3":
      $("#jsonSelect3").removeAttr('hidden');
      break;
      case "4":
      $("#jsonSelect4").removeAttr('hidden');
      break;
      case "5":
      $("#jsonSelect5").removeAttr('hidden');
      break;
      case "6":
      $("#jsonSelect6").removeAttr('hidden');
      break;
      case "7":
      $("#jsonSelect7").removeAttr('hidden');
      break;
      case "8":
      $("#jsonSelect8").removeAttr('hidden');
      break;
      case "9":
      $("#jsonSelect9").removeAttr('hidden');
      break;
      case "10":
      $("#jsonSelect10").removeAttr('hidden');
      break;
      case "11":
      $("#jsonSelect11").removeAttr('hidden');
      break;
      case "12":
      $("#jsonSelect12").removeAttr('hidden');
      break;
      case "13":
      $("#jsonSelect13").removeAttr('hidden');
      break;
      case "14":
      $("#jsonSelect14").removeAttr('hidden');
      break;
      case "15":
      $("#jsonSelect15").removeAttr('hidden');
      break;
      case "16":
      $("#jsonSelect16").removeAttr('hidden');
      break;
      case "17":
      $("#jsonSelect17").removeAttr('hidden');
      break;
      case "18":
      $("#jsonSelect18").removeAttr('hidden');
      break;
      case "19":
      $("#jsonSelect19").removeAttr('hidden');
      break;
      case "20":
      $("#jsonSelect20").removeAttr('hidden');
      break;
      case "21":
      $("#jsonSelect21").removeAttr('hidden');
      break;
      case "22":
      $("#jsonSelect22").removeAttr('hidden');
      break;
      case "23":
      $("#jsonSelect23").removeAttr('hidden');
      break;
      case "24":
      $("#jsonSelect24").removeAttr('hidden');
      break;
      case "25":
      $("#jsonSelect25").removeAttr('hidden');
      break;
      case "26":
      $("#jsonSelect26").removeAttr('hidden');
      break;
      case "27":
      $("#jsonSelect27").removeAttr('hidden');
      break;
      case "28":
      $("#jsonSelect28").removeAttr('hidden');
      break;
      case "29":
      $("#jsonSelect29").removeAttr('hidden');
      break;
      case "30":
      $("#jsonSelect30").removeAttr('hidden');
      break;
      case "31":
      $("#jsonSelect31").removeAttr('hidden');
      break;
      case "32":
      $("#jsonSelect32").removeAttr('hidden');
      break;
      case "33":
      $("#jsonSelect33").removeAttr('hidden');
      break;
      case "34":
      $("#jsonSelect34").removeAttr('hidden');
      break;

    }

  });

   $( "#formJsn" ).submit(function( event ) {
    geraJson();
    event.preventDefault();
  });


   function setExplicacao(nome){

    var res = nome.split("");

    switch(res[0]){

      case 'C':
      $("#exAno").text("C - 2017");
      break;
      default:
      $("#exAno").text("e");
      break;

    }

    switch(res[1]){

      case 'I':
      $("#exUtilizacao").text("I - Industrial");
      $("#industrialT").removeAttr('hidden');
      $("#industrial").removeAttr('hidden');
      industrial = true;
      break;
      case 'C':

      $("#industrialT").attr("hidden","true");
      $("#industrial").attr("hidden","true");
      $("#exUtilizacao").text("C - Comercial");
      industrial = false;

      break;
      case 'D':
      $("#exUtilizacao").text("D - Dispenser comercial");
      $("#industrialT").attr("hidden","true");
      $("#industrial").attr("hidden","true");
      industrial = false;
      break;
      case 'E':
      $("#industrialT").removeAttr('hidden');
      $("#industrial").removeAttr('hidden');
      $("#exUtilizacao").text("E - Dispenser industrial");
      industrial = true;
      break;
      default:
      $("#exUtilizacao").text("e");
      break;

    }

    switch(res[2]){

      case 'F':
      $("#exTipo").text("F - Low Hose = Bico Frontal");
      break;
      case 'L':
      $("#exTipo").text("L - Low Hose = Bico Lateral");
      break;
      case 'S':
      $("#exTipo").text("S - High Hose Slim");
      break;
      case 'H':
      $("#exTipo").text("H - High Hose");
      break;
      default:
      $("#exTipo").text("");
      break;

    }

    switch(res[3]){

      case '1':
      $("#exVersao").text("1 - Simples");
      break;
      case 'D':
      $("#exVersao").text("D - Dual");
      break;
      case '2':
      $("#exVersao").text("2 - Dupla");
      break;
      case '4':
      $("#exVersao").text("4 - Quadrupla");
      break;
      case '6':
      $("#exVersao").text("6 - Sextupla");
      break;
      case '8':
      $("#exVersao").text("8 - Octupla");
      break;
      default:
      $("#exVersao").text("");
      break;

    }

    switch(res[4]){

      case '4':
      $("#exNumeroAbast").text("4 - Quatro Abastecimentos Simultaneos");
      break;
      case '2':
      $("#exNumeroAbast").text("2 - Dois Abastecimentos Simultaneos");
      break;
      case '1':
      $("#exNumeroAbast").text("1 - Um Abastecimento");
      break;
      default:
      $("#exNumeroAbast").text("");
      break;
    }

    switch(res[5]){
      case 'P':
      $("#exRotativa").text("P - T75");
      break;
      case 'E':
      $("#exRotativa").text("E - T140");
      break;
      case 'D':
      $("#exRotativa").text("D - Dispenser");
      break;
      case 'J':
      $("#exRotativa").text("J - T75/T140");
      break;
      case 'G':
      $("#exRotativa").text("G - T75/T75");
      break;
      case 'H':
      $("#exRotativa").text("H - T75/T140/T140");
      break;
      case 'I':
      $("#exRotativa").text("I - T140/T140");
      break;
      case 'C':
      $("#exRotativa").text("C - T140/T140/T140/T140");
      break;
      case 'Q':
      $("#exRotativa").text("Q - T75/T75/T75");
      break;
      case 'R':
      $("#exRotativa").text("R - T75/T75/T75/T75");
      break;
      case 'S':
      $("#exRotativa").text("S - T140/T140/T140");
      break;
      case 'U':
      $("#exRotativa").text("U - T75/T75/T140");
      break;
      case 'V':
      $("#exRotativa").text("V - T75/T75/T75/140");
      break;
      default:
      $("#exNumeroAbast").text("");
      break;

    }

    switch(res[6]){

      case 'A':
      $("#exVazao").text("A - 40 L/min");
      break;
      case 'B':
      $("#exVazao").text("B - 75 L/min");
      break;
      case 'C':
      $("#exVazao").text("C - 130 L/min");
      break;
      case 'D':
      $("#exVazao").text("D - 40/75 L/min");
      break;
      case 'E':
      $("#exVazao").text("E - 40/130 L/min");
      break;
      case 'F':
      $("#exVazao").text("F - 75/130 L/min");
      break;
      case 'G':
      $("#exVazao").text("G - 40/75/130 L/min");
      break;
      case 'H':
      $("#exVazao").text("H - 40/40/40/75 L/min");
      break;
      case 'I':
      $("#exVazao").text("I - 40/40/75/75 L/min");
      break;
      case 'J':
      $("#exVazao").text("J - 40/75/75/75 L/min");
      break;
      case 'L':
      $("#exVazao").text("L - 40/130/130 L/min");
      break;
      case 'M':
      $("#exVazao").text("M - 40/40/75 L/min");
      break;
      case 'N':
      $("#exVazao").text("N - 40/75/75 L/min");
      break;
      case 'O':
      $("#exVazao").text("O - 40/40/130 L/min");
      break;
      case 'P':
      $("#exVazao").text("P - 75/75/130 L/min");
      break;
      case 'Q':
      $("#exVazao").text("Q - 75/130/130 L/min");
      break;
      case 'R':
      $("#exVazao").text("R - 130/130/40/40 L/min");
      break;
      case 'S':
      $("#exVazao").text("S - 130/130/40/75 L/min");
      break;
      case 'T':
      $("#exVazao").text("T - 130/130/75/75 L/min");
      break;
      case 'U':
      $("#exVazao").text("U - 40/40/40/130 L/min");
      break;
      case 'V':
      $("#exVazao").text("V - 40/75/75/130 L/min");
      break;
      case '6':
      $("#exVazao").text("6 - 130/40/40");
      break;
      case '7':
      $("#exVazao").text("7 - 130/40/75");
      break;
      case '8':
      $("#exVazao").text("8 - 130/75/75");
      break;
      default:
      $("#exVazao").text("");
      break;


    }

    $(".explic").removeAttr('hidden');
  }


  function setPrenchimento(object){

    console.log(object)

    resetCamposHtml();

    switch(object.modelo){

              //VERDE CLARO
              case "CCL11PA":
              case "CCL11PB":
              case "CIL11PA":
              case "CIL11PB":

              modelo_final = 1;
              modelo_tipo = "T1_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav1').attr('hidden',true);
              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#SLOT2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#POS4').attr('hidden',true);
              $('#altav4').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);


              $("#SLOT2 input,select").removeAttr('required');
              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');
              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#altav1 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCL11PA - CIL11PA - CCL11PB - CIL11PB.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCL11PA - CIL11PA - CCL11PB - CIL11PB.png");

              /****************************************************/

              break;

              //VERDE ESCURO
              case "CCL11EC":
              case "CIL11EC":


              modelo_final = 2;
              modelo_tipo = "T1_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#SLOT2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#POS4').attr('hidden',true);
              $('#altav4').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);


              $("#SLOT2 input,select").removeAttr('required');
              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');
              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');


              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1H(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCL11EC - CIL11EC.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCL11EC - CIL11EC.png");

              /****************************************************/
              break;

              //VERDE ESCURO

              case "CCL11GC":
              case "CIL11GC":

              modelo_final = 2;
              modelo_tipo = "T1_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#SLOT2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#POS4').attr('hidden',true);
              $('#altav4').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);


              $("#SLOT2 input,select").removeAttr('required');
              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');
              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');


              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1H(object);
              

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCL11GC - CIL11GC.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCL11GC - CIL11GC.png");
              
              
              



              

              /****************************************************/
              break;

              //ROSA
              case "CCFD2PA":
              case "CIFD2PA":
              case "CCFD2PB":
              case "CIFD2PB":
              case "CCLD2PA":
              case "CILD2PA":
              case "CCLD2PB":
              case "CILD2PB":
              case "CCL22GA":
              case "CIL22GA":
              case "CCL22GB":
              case "CIL22GB":
              case "CCL22GD":
              case "CIL22GD":
              case "CCF22GA":
              case "CIF22GA":
              case "CCF22GB":
              case "CIF22GB":
              case "CCF22GD":
              case "CIF22GD":
              case "CCSD2PA":
              case "CISD2PA":
              case "CCSD2PB":
              case "CISD2PB":
              case "CCS22GA":
              case "CIS22GA":
              case "CCS22GB":
              case "CIS22GB":
              case "CCS22GD":
              case "CIS22GD":
              case "CCHD2PA":
              case "CCHD2PB":

              modelo_final = 3;
              modelo_tipo = "T2_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav1').attr('hidden',true);
              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);

              $('#altav4').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);

              $('#altav1 input,select').removeAttr('required');
              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#altav4 input,select').removeAttr('required');
              $('#POS5 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');

              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');


              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);
              printJsonSlot2(object);
              printJsonPos4(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCF22GA - CIF22GA - CCF22GB - CIF22GB - CCF22GD - CIF22GD - CCS22GA - CIS22GA - CCS22GB - CIS22GB - .png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCF22GA - CIF22GA - CCF22GB - CIF22GB - CCF22GD - CIF22GD - CCS22GA - CIS22GA - CCS22GB - CIS22GB - .png");

              break;

              //AMARELO CLARO
              case "CCFD2EC":
              case "CIFD2EC":
              case "CCFD2GC":
              case "CIFD2GC":
              case "CCLD2EC":
              case "CILD2EC":
              case "CCLD2GC":
              case "CILD2GC":
              case "CCL22EC":
              case "CIL22EC":
              case "CIF22EC":
              case "CCSD2EC":
              case "CISD2EC":
              case "CCSD2GC":
              case "CISD2GC":
              case "CCS22IC":
              case "CIS22IC":
              case "CCHD2EC":

              modelo_final = 4;
              modelo_tipo = "T2_BICO";


              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);

              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');              
              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS5 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1H(object);
              printJsonSlot2(object);
              printJsonPos4H(object);
              
              if(object.modelo == "CCLD2EC" || object.modelo == "CILD2EC"){

                $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCLD2EC - CILD2EC.png");
                $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCLD2EC - CILD2EC.png");
                
              }else{

                $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCS22IC - CIS22IC.png");
                $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCS22IC - CIS22IC.png");
              }

              break;
              
               //AMARELO CLARO

               case "CCF22EC":

               modelo_final = 4;
               modelo_tipo = "T2_BICO";


               $('.nenh').attr('hidden',true);
               $('#jsn').removeAttr('hidden');

               $('#POS2').attr('hidden',true);
               $('#altav2').attr('hidden',true);
               $('#POS3').attr('hidden',true);
               $('#altav3').attr('hidden',true);
               $('#POS5').attr('hidden',true);
               $('#altav5').attr('hidden',true);
               $('#POS6').attr('hidden',true);
               $('#altav6').attr('hidden',true);
               $('#SLOT3').attr('hidden',true);
               $('#POS5').attr('hidden',true);
               $('#altav5').attr('hidden',true);
               $('#POS6').attr('hidden',true);
               $('#altav6').attr('hidden',true);
               $('#SLOT4').attr('hidden',true);
               $('#POS7').attr('hidden',true);
               $('#altav7').attr('hidden',true);
               $('#POS8').attr('hidden',true);
               $('#altav8').attr('hidden',true);
               $('#POS9').attr('hidden',true);
               $('#altav9').attr('hidden',true);
               $('#POS10').attr('hidden',true);
               $('#altav10').attr('hidden',true);
               $('#POS11').attr('hidden',true);
               $('#altav11').attr('hidden',true);
               $('#POS12').attr('hidden',true);
               $('#altav12').attr('hidden',true);

               $("#SLOT3 input,select").removeAttr('required');
               $("#SLOT4 input,select").removeAttr('required');              
               $('#POS2 input,select').removeAttr('required');
               $('#POS3 input,select').removeAttr('required');
               $('#POS5 input,select').removeAttr('required');
               $('#POS6 input,select').removeAttr('required');

               printJsonP(object);
               printJsonSlot1(object);
               printJsonPos1H(object);
               printJsonSlot2(object);
               printJsonPos4H(object);

               $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCS22IC - CIS22IC.png");
               $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCS22IC - CIS22IC.png");

               break;

              // LARANJA CLARO
              case "CCL22JE":
              case "CIL22JE":
              case "CCL22JF":
              case "CIL22JF":

              modelo_final = 5;
              modelo_tipo = "T2_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav1').attr('hidden',true);
              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);

              $('#altav1 input,select').removeAttr('required');
              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');              
              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS5 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);
              printJsonSlot2(object);
              printJsonPos4H(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCL22JE - CIL22JE - CCL22JF - CIL22JF.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCL22JE - CIL22JE - CCL22JF - CIL22JF.png");

              break;

              // ROXO
              case "CCF22JE":
              case "CIF22JE":
              case "CCR22JF":
              case "CIF22JF":
              case "CCS22JE":
              case "CIS22JE":
              case "CCS22JF":
              case "CIS22JF":

              modelo_final = 6;
              modelo_tipo = "T2_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#altav4').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);

              $('#altav4 input,select').removeAttr('required');
              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');              
              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS5 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1H(object);
              printJsonSlot2(object);
              printJsonPos4(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCF22JE - CIF22JE - CCR22JF - CIF22JF - CCS22JE - CIS22JE - CCS22JF - CIS22JF.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCF22JE - CIF22JE - CCR22JF - CIF22JF - CCS22JE - CIS22JE - CCS22JF - CIS22JF.png");

              break;

              //AZUL CLARO
              case "CCF42GA":
              case "CCF42GB":
              case "CCF42GD":
              case "CCS42GA":
              case "CCS42GB":
              case "CCS42GD":
              case "CCH42GA":
              case "CCH42GB":
              case "CCH42GD":

              modelo_final = 7;
              modelo_tipo = "T4_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav1').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#altav4').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);

              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#altav1 input,select').removeAttr('required');
              $('#altav2 input,select').removeAttr('required');
              $('#altav3 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#altav4 input,select').removeAttr('required');
              $('#altav5 input,select').removeAttr('required');
              $('#altav6 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);
              printJsonPos2(object);
              printJsonSlot2(object);
              printJsonPos4(object);
              printJsonPos5(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCF42GA - CCF42GB - CCF42GD - CCS42GA - CCS42GB-CCS42GD - CCH42GA - CCH42GB - CCH42GD-CCF44GA - CCF4.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCF42GA - CCF42GB - CCF42GD - CCS42GA - CCS42GB-CCS42GD - CCH42GA - CCH42GB - CCH42GD-CCF44GA - CCF4.png");

              break;


              //ROXO CLARO
              case "CCF44GA":
              case "CCF44GB":
              case "CCF44GD":
              case "CCS44GA":
              case "CCS44GB":
              case "CCS44GD":
              case "CCH44GA":
              case "CCH44GB":
              case "CCH44GD":

              modelo_final = 8;
              modelo_tipo = "T4_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav1').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#altav4').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#altav12').attr('hidden',true);

              $('#POS2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#POS12').attr('hidden',true);



              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS5 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#POS8 input,select').removeAttr('required');
              $('#POS9 input,select').removeAttr('required');
              $('#POS11 input,select').removeAttr('required');
              $('#POS12 input,select').removeAttr('required');


              $('#altav1 input,select').removeAttr('required');
              $('#altav4 input,select').removeAttr('required');
              $('#altav7 input,select').removeAttr('required');
              $('#altav10 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);
              printJsonSlot2(object);
              printJsonPos4(object);
              printJsonSlot3(object);
              printJsonPos7(object);
              printJsonSlot4(object);
              printJsonPos10(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCF42GA - CCF42GB - CCF42GD - CCS42GA - CCS42GB-CCS42GD - CCH42GA - CCH42GB - CCH42GD-CCF44GA - CCF4.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCF42GA - CCF42GB - CCF42GD - CCS42GA - CCS42GB-CCS42GD - CCH42GA - CCH42GB - CCH42GD-CCF44GA - CCF4.png");


              break;


              //VERDE
              case "CCH42CC":

              modelo_final = 9;
              modelo_tipo = "T4_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');


              $('#altav3').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#altav12').attr('hidden',true);
              $('#POS3').attr('hidden',true);

              $('#POS6').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#POS12').attr('hidden',true);


              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#altav3 input,select').removeAttr('required');
              $('#altav6 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1H(object);
              printJsonPos2H(object);
              printJsonSlot2(object);
              printJsonPos4H(object);
              printJsonPos5H(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42CC - CCH44CC.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42CC - CCH44CC.png");

              break;

              //VERDE

              case "CCH42IC":

              modelo_final = 9;
              modelo_tipo = "T4_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');


              $('#altav3').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#altav12').attr('hidden',true);
              $('#POS3').attr('hidden',true);

              $('#POS6').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#POS12').attr('hidden',true);


              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#altav3 input,select').removeAttr('required');
              $('#altav6 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1H(object);
              printJsonPos2H(object);
              printJsonSlot2(object);
              printJsonPos4H(object);
              printJsonPos5H(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH44IC - CCH42IC.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH44IC - CCH42IC.png");

              break;


              //AMARELO ESCURO
              case "CCH42JE":
              case "CCH42JF":

              modelo_final = 10;
              modelo_tipo = "T4_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav1').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#altav12').attr('hidden',true);
              $('#POS3').attr('hidden',true);

              $('#POS6').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#POS12').attr('hidden',true);


              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#altav1 input,select').removeAttr('required');
              $('#altav3 input,select').removeAttr('required');
              $('#altav5 input,select').removeAttr('required');
              $('#altav6 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);
              printJsonPos2H(object);
              printJsonSlot2(object);
              printJsonPos4H(object);
              printJsonPos5(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42JE - CCH42JF - CCH44JE - CCH44JF.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42JE - CCH42JF - CCH44JE - CCH44JF.png");

              break;


              //AMARELO ESCURO

              case "CCH42HE":
              case "CCH42HF":

              modelo_final = 10;
              modelo_tipo = "T4_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');


              $('#altav1').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#altav12').attr('hidden',true);
              $('#POS3').attr('hidden',true);

              $('#POS6').attr('hidden',true);
              $('#SLOT3').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#POS12').attr('hidden',true);


              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#altav1 input,select').removeAttr('required');
              $('#altav3 input,select').removeAttr('required');
              $('#altav5 input,select').removeAttr('required');
              $('#altav6 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);
              printJsonPos2H(object);
              printJsonSlot2(object);
              printJsonPos4H(object);
              printJsonPos5(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42HE - CCH42HF - CCH44HE - CCH44HF.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42HE - CCH42HF - CCH44HE - CCH44HF.png");

              break;

              //LARANJA
              case "CCH44CC":

              modelo_final = 11;
              modelo_tipo = "T4_BICO";


              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);

              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);

              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);

              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);

              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS5 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#POS8 input,select').removeAttr('required');
              $('#POS9 input,select').removeAttr('required');
              $('#POS11 input,select').removeAttr('required');
              $('#POS12 input,select').removeAttr('required');


              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1H(object);
              printJsonSlot2(object);
              printJsonPos4H(object);
              printJsonSlot3(object);
              printJsonPos7H(object);
              printJsonSlot4(object);
              printJsonPos10H(object);
              

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42CC - CCH44CC.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42CC - CCH44CC.png");

              break;

              //LARANJA
              case "CCH44IC":

              modelo_final = 11;
              modelo_tipo = "T4_BICO";


              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);

              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);

              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);

              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);

              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS5 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#POS8 input,select').removeAttr('required');
              $('#POS9 input,select').removeAttr('required');
              $('#POS11 input,select').removeAttr('required');
              $('#POS12 input,select').removeAttr('required');


              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1H(object);
              printJsonSlot2(object);
              printJsonPos4H(object);
              printJsonSlot3(object);
              printJsonPos7H(object);
              printJsonSlot4(object);
              printJsonPos10H(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH44IC - CCH42IC.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH44IC - CCH42IC.png");


              break;


              //AZUL MARINHO
              case "CCH44JE":
              case "CCH44JF":


              modelo_final = 12;
              modelo_tipo = "T4_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav1').attr('hidden',true);
              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);

              $('#altav4').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);

              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);

              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);


              $('#altav1 input,select').removeAttr('required');
              $('#altav4 input,select').removeAttr('required');
              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS5 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#POS8 input,select').removeAttr('required');
              $('#POS9 input,select').removeAttr('required');
              $('#POS11 input,select').removeAttr('required');
              $('#POS12 input,select').removeAttr('required');


              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);
              printJsonSlot2(object);
              printJsonPos4(object);
              printJsonSlot3(object);
              printJsonPos7H(object);
              printJsonSlot4(object);
              printJsonPos10H(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42JE - CCH42JF - CCH44JE - CCH44JF.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42JE - CCH42JF - CCH44JE - CCH44JF.png");
              
              break;

              //AZUL MARINHO

              case "CCH44HE":
              case "CCH44HF":

              modelo_final = 12;
              modelo_tipo = "T4_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav1').attr('hidden',true);
              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);

              $('#altav4').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);

              $('#POS8').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);

              $('#POS11').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);


              $('#altav1 input,select').removeAttr('required');
              $('#altav4 input,select').removeAttr('required');
              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS5 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#POS8 input,select').removeAttr('required');
              $('#POS9 input,select').removeAttr('required');
              $('#POS11 input,select').removeAttr('required');
              $('#POS12 input,select').removeAttr('required');


              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);
              printJsonSlot2(object);
              printJsonPos4(object);
              printJsonSlot3(object);
              printJsonPos7H(object);
              printJsonSlot4(object);
              printJsonPos10H(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42HE - CCH42HF - CCH44HE - CCH44HF.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH42HE - CCH42HF - CCH44HE - CCH44HF.png");
              
              break;



              // VERMELHO
              case "CCH62QA":
              case "CCH62QB":
              case "CCH62SC":
              case "CCH62QM":
              case "CCH62QN":
              case "CCH62UG":
              case "CCH62UP":
              case "CCH62HQ":

              modelo_final = 13;
              modelo_tipo = "T6_BICO";


              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav1').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#altav4').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#altav12').attr('hidden',true);

              $('#SLOT3').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);

              $('#POS7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#POS12').attr('hidden',true);


              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');


              $('#altav1 input,select').removeAttr('required');
              $('#altav2 input,select').removeAttr('required');
              $('#altav3 input,select').removeAttr('required');
              $('#altav4 input,select').removeAttr('required');
              $('#altav5 input,select').removeAttr('required');
              $('#altav6 input,select').removeAttr('required');
              $('#altav7 input,select').removeAttr('required');
              $('#altav8 input,select').removeAttr('required');
              $('#altav9 input,select').removeAttr('required');
              $('#altav10 input,select').removeAttr('required');
              $('#altav11 input,select').removeAttr('required');
              $('#altav12 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);
              printJsonPos2(object);
              printJsonPos3(object);
              printJsonSlot2(object);
              printJsonPos4(object);
              printJsonPos5(object);
              printJsonPos6(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH62QA - CCH62QB - CCH62SC - CCH62QM - CCH62QN - CCH62UG - CCH62UP - CCH62HQ.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH62QA - CCH62QB - CCH62SC - CCH62QM - CCH62QN - CCH62UG - CCH62UP - CCH62HQ.png");

              break;


              //CINZA
              case "CCH62H6":
              case "CCH62H7":
              case "CCH62H8":

              modelo_final = 14;
              modelo_tipo = "T6_BICO";


              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav2').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#altav4').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#altav12').attr('hidden',true);

              $('#SLOT3').attr('hidden',true);
              $('#POS7').attr('hidden',true);
              $('#POS8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#SLOT4').attr('hidden',true);
              $('#POS10').attr('hidden',true);
              $('#POS11').attr('hidden',true);
              $('#POS12').attr('hidden',true);


              $("#SLOT3 input,select").removeAttr('required');
              $("#SLOT4 input,select").removeAttr('required');

              $('#altav2 input,select').removeAttr('required');
              $('#altav3 input,select').removeAttr('required');
              $('#altav4 input,select').removeAttr('required');
              $('#altav5 input,select').removeAttr('required');

              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1H(object);
              printJsonPos2(object);
              printJsonPos3(object);
              printJsonSlot2(object);
              printJsonPos4(object);
              printJsonPos5(object);
              printJsonPos6H(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH62H6 - CCH62H7 - CCH62H8.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH62H6 - CCH62H7 - CCH62H8.png");

              break;

              //VERDE LIMAO
              case "CCH64QA":
              case "CCH64QB":
              case "CCH64SC":
              case "CCH64QM":
              case "CCH64QN":
              case "CCH64UG":
              case "CCH64UP":
              case "CCH64HQ":

              modelo_final = 15;
              modelo_tipo = "T6_BICO";


              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav1').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#altav3').attr('hidden',true);
              $('#altav4').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#altav7').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#altav10').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#altav12').attr('hidden',true);

              $('#POS2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#POS5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#POS12').attr('hidden',true);


              $('#altav1 input,select').removeAttr('required');
              $('#altav4 input,select').removeAttr('required');
              $('#altav7 input,select').removeAttr('required');
              $('#altav8 input,select').removeAttr('required');
              $('#altav10 input,select').removeAttr('required');
              $('#altav11 input,select').removeAttr('required');

              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');

              $('#POS5 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');

              $('#POS9 input,select').removeAttr('required');
              $('#POS12 input,select').removeAttr('required');


              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);
              printJsonSlot2(object);
              printJsonPos4(object);
              printJsonSlot3(object);
              printJsonPos7(object);
              printJsonPos8(object);
              printJsonSlot4(object);
              printJsonPos10(object);
              printJsonPos11(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH62QA - CCH62QB - CCH62SC - CCH62QM - CCH62QN - CCH62UG - CCH62UP - CCH62HQ - CCH64QA - CCH64QB - CCH64SC - CCH64QM -CCH64QN - CCH64UG - CCH64UP - CCH64HQ.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH62QA - CCH62QB - CCH62SC - CCH62QM - CCH62QN - CCH62UG - CCH62UP - CCH62HQ - CCH64QA - CCH64QB - CCH64SC - CCH64QM -CCH64QN - CCH64UG - CCH64UP - CCH64HQ.png");

              break;


              //AMARELO OVO
              case "CCH64H6":
              case "CCH64H7":
              case "CCH64H8":

              modelo_final = 16;
              modelo_tipo = "T6_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#POS2').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);


              $('#POS5').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);

              $('#altav7').attr('hidden',true);
              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);

              $('#altav10').attr('hidden',true);
              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);


              $('#altav2 input,select').removeAttr('required');
              $('#altav3 input,select').removeAttr('required');

              $('#altav5 input,select').removeAttr('required');
              $('#altav6 input,select').removeAttr('required');
              $('#altav7 input,select').removeAttr('required');
              $('#altav8 input,select').removeAttr('required');
              $('#altav9 input,select').removeAttr('required');
              $('#altav10 input,select').removeAttr('required');
              $('#altav11 input,select').removeAttr('required');
              $('#altav12 input,select').removeAttr('required');

              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS5 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#POS9 input,select').removeAttr('required');
              $('#POS12 input,select').removeAttr('required');


              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1H(object);
              printJsonSlot2(object);
              printJsonPos4H(object);
              printJsonSlot3(object);
              printJsonPos7(object);
              printJsonPos8(object);
              printJsonSlot4(object);
              printJsonPos10(object);
              printJsonPos11(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH62H6 - CCH62H7 - CCH62H8.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH62H6 - CCH62H7 - CCH62H8.png");

              break;

              //VINHO
              case "CCH84RA":
              case "CCH84RB":
              case "CCH84CC":
              case "CCH84RH":
              case "CCH84RI":
              case "CCH84RJ":
              case "CCH84VU":
              case "CCH84VV":

              modelo_final = 17;
              modelo_tipo = "T8_BICO";

              $('.nenh').attr('hidden',true);
              $('#jsn').removeAttr('hidden');

              $('#altav1').attr('hidden',true);
              $('#altav2').attr('hidden',true);
              $('#POS3').attr('hidden',true);
              $('#altav3').attr('hidden',true);

              $('#altav4').attr('hidden',true);
              $('#altav5').attr('hidden',true);
              $('#POS6').attr('hidden',true);
              $('#altav6').attr('hidden',true);
              $('#altav7').attr('hidden',true);

              $('#altav8').attr('hidden',true);
              $('#POS9').attr('hidden',true);
              $('#altav9').attr('hidden',true);
              $('#altav10').attr('hidden',true);

              $('#altav11').attr('hidden',true);
              $('#POS12').attr('hidden',true);
              $('#altav12').attr('hidden',true);


              $('#altav1 input,select').removeAttr('required');
              $('#altav2 input,select').removeAttr('required');
              $('#altav4 input,select').removeAttr('required');
              $('#altav5 input,select').removeAttr('required');
              $('#altav7 input,select').removeAttr('required');
              $('#altav8 input,select').removeAttr('required');
              $('#altav10 input,select').removeAttr('required');
              $('#altav11 input,select').removeAttr('required');

              $('#POS2 input,select').removeAttr('required');
              $('#POS3 input,select').removeAttr('required');
              $('#POS6 input,select').removeAttr('required');
              $('#POS9 input,select').removeAttr('required');
              $('#POS12 input,select').removeAttr('required');


              printJsonP(object);
              printJsonSlot1(object);
              printJsonPos1(object);
              printJsonPos2(object);
              printJsonSlot2(object);
              printJsonPos4(object);
              printJsonPos5(object);
              printJsonSlot3(object);
              printJsonPos7(object);
              printJsonPos8(object);
              printJsonSlot4(object);
              printJsonPos10(object);
              printJsonPos11(object);

              $("#img-esquema-bomba").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH84RA - CCH84RB - CCH84CC - CCH84RH - CCH84RI - CCH84RJ - CCH84VU - CCH84VV.png");
              $("#img-esquema-bomba-2").attr("src","http://wertco.com.br/bootstrap/img/ligacao-bomba/CCH84RA - CCH84RB - CCH84CC - CCH84RH - CCH84RI - CCH84RJ - CCH84VU - CCH84VV.png");
              break;

              default:
              $('.jsn').attr('hidden',true);
              break;

            }
          }


          function geraJson(){

            var json = {};

            json.titulo = $('#titulo').val();
            json.versao = $('#versao').val();
            json.unidade = $('#unidade').val();
            json.modelo = $('#modelo').val();
            json.cnpj = $('#cnpj').val();
            json.ip = $('#ip').val();
            json.gw = $('#gw').val();
            json.Mask = $('#Mask').val();
            json.dns = $('#dns').val();
            json.dhcp = $('#dhcp').val();
            json.hostname = $('#hostname').val();
            if(industrial){
              json.industrial = $('#industrial').val();
            }
            json.ble_id = $('#ble_id').val();
            json.protocolo1 = $('#protocolo1').val();
            json.protocolo2 = $('#protocolo2').val();
            json.Automacao = $('#Automacao').val();
            json.Identfid = $('#Identfid').val();
            json.valvula = $('#valvula').val();
            json.falta_de_pulsos = Number($('#falta_de_pulsos').val());
            json.tempo_de_start = Number($('#tempo_de_start').val());
            json.beep = $('#beep').val();
            json.Tempo_Desliga = Number($('#Tempo_Desliga').val());
            json.Timer_Ligar = $('#Timer_Ligar').val();
            json.ponto_baixa_vazao = Number($('#ponto_baixa_vazao').val());
            json.Limite_Volume = Number($('#Limite_Volume').val());
            json.total_Decimal = Number($('#total_Decimal').val());
            json.volume_Decimal = Number($('#volume_Decimal').val());
            json.preco_Decimal = Number($('#preco_Decimal').val());
            json.encerrante_dec = Number($('#encerrante_dec').val());
            json.encerrante_calc = Number($('#encerrante_calc').val());
            json.preset_P1 = $('#preset_P1').val();
            json.preset_P2 = $('#preset_P2').val();
            json.Arredonda = Number($('#Arredonda').val()); 
            json.texto_1 = $('#texto_1').val();
            json.texto_2 = $('#texto_2').val();
            json.texto_3 = $('#texto_3').val();



            switch(modelo_final){

              case 1:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();
              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }

              break;

              case 2:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();
              json.slot1.pos1.motorB = Number($('#motorB1').val());
              json.slot1.pos1.valvB = Number($('#valvB1').val());
              json.slot1.pos1.pulserB = $('#pulserB1').val();
              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }

              break;

              case 3:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();
              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }

              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();
              json.slot2.pos1  = {};

              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }

              break;

              case 4:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();
              json.slot1.pos1.motorB = Number($('#motorB1').val());
              json.slot1.pos1.valvB = Number($('#valvB1').val());
              json.slot1.pos1.pulserB = $('#pulserB1').val();
              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }

              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();
              json.slot2.pos1  = {};

              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.motorB = Number($('#motorB4').val());
              json.slot2.pos1.valvB = Number($('#valvB4').val());
              json.slot2.pos1.pulserB = $('#pulserB4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }

              break;

              case 5:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();
              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }

              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();
              json.slot2.pos1  = {};

              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.motorB = Number($('#motorB4').val());
              json.slot2.pos1.valvB = Number($('#valvB4').val());
              json.slot2.pos1.pulserB = $('#pulserB4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }

              break;

              case 6:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();
              json.slot1.pos1.motorB = Number($('#motorB1').val());
              json.slot1.pos1.valvB = Number($('#valvB1').val());
              json.slot1.pos1.pulserB = $('#pulserB1').val();
              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }

              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();
              json.slot2.pos1  = {};

              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }


              break;

              case 7:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();
              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }

              json.slot1.pos2  = {};
              json.slot1.pos2.bico = Number($('#bico2').val());
              json.slot1.pos2.mode =  $('#mode2').val();
              json.slot1.pos2.motorA = Number($('#motorA2').val());
              json.slot1.pos2.valvA = Number($('#valvA2').val());
              json.slot1.pos2.pulserA = $('#pulserA2').val();
              json.slot1.pos2.eltmCnl = Number($('#eltmCnl2').val());
              json.slot1.pos2.ppl_V = Number($('#ppl_V2').val());
              json.slot1.pos2.ppl_C = Number($('#ppl_C2').val());
              json.slot1.pos2.ppl_P = Number($('#ppl_P2').val());
              json.slot1.pos2.rv_motor = Number($('#rv_P2').val());

              if($('#mode2').val().includes("H")){
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
                json.slot1.pos2.pwm_b = Number($('#pwm_b_P2').val());
              }else{
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
              }


              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();

              json.slot2.pos1  = {};
              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }


              json.slot2.pos2  = {};
              json.slot2.pos2.bico = Number($('#bico5').val());
              json.slot2.pos2.mode =  $('#mode5').val();
              json.slot2.pos2.motorA = Number($('#motorA5').val());
              json.slot2.pos2.valvA = Number($('#valvA5').val());
              json.slot2.pos2.pulserA = $('#pulserA5').val();

              json.slot2.pos2.eltmCnl = Number($('#eltmCnl5').val());
              json.slot2.pos2.ppl_V = Number($('#ppl_V5').val());
              json.slot2.pos2.ppl_C = Number($('#ppl_C5').val());
              json.slot2.pos2.ppl_P = Number($('#ppl_P5').val());
              json.slot2.pos2.rv_motor = Number($('#rv_P5').val());

              if($('#mode5').val().includes("H")){
                json.slot2.pos2.pwm_a = Number($('#pwm_a_P5').val());
                json.slot2.pos2.pwm_b = Number($('#pwm_b_P5').val());
              }else{
                json.slot2.pos2.pwm_a = Number($('#pwm_a_P5').val());
              }

              break;

              case 8:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();
              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }


              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();
              json.slot2.pos1  = {};

              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }


              json.slot3 = {};

              json.slot3.ladoInmetro = $('#ladoInmetro3').val();
              json.slot3.logico = Number($('#logico3').val());
              json.slot3.dspA = $('#dspA3').val();
              json.slot3.dspB = $('#dspB3').val();   
              json.slot3.eltm = $('#eltm3').val();
              json.slot3.LcdOrdem = $('#LcdOrdem3').val();
              json.slot3.pos1  = {};

              json.slot3.pos1.bico = Number($('#bico7').val());
              json.slot3.pos1.mode =  $('#mode7').val();
              json.slot3.pos1.motorA = Number($('#motorA7').val());
              json.slot3.pos1.valvA = Number($('#valvA7').val());
              json.slot3.pos1.pulserA = $('#pulserA7').val();

              json.slot3.pos1.eltmCnl = Number($('#eltmCnl7').val());
              json.slot3.pos1.ppl_V = Number($('#ppl_V7').val());
              json.slot3.pos1.ppl_C = Number($('#ppl_C7').val());
              json.slot3.pos1.ppl_P = Number($('#ppl_P7').val());
              json.slot3.pos1.rv_motor = Number($('#rv_P7').val());

              if($('#mode7').val().includes("H")){
                json.slot3.pos1.pwm_a = Number($('#pwm_a_P7').val());
                json.slot3.pos1.pwm_b = Number($('#pwm_b_P7').val());
              }else{
                json.slot3.pos1.pwm_a = Number($('#pwm_a_P7').val());
              }

              json.slot4 = {};

              json.slot4.ladoInmetro = $('#ladoInmetro4').val();
              json.slot4.logico = Number($('#logico4').val());
              json.slot4.dspA = $('#dspA4').val();
              json.slot4.dspB = $('#dspB4').val();   
              json.slot4.eltm = $('#eltm4').val();
              json.slot4.LcdOrdem = $('#LcdOrdem4').val();
              json.slot4.pos1  = {};

              json.slot4.pos1.bico = Number($('#bico10').val());
              json.slot4.pos1.mode =  $('#mode10').val();
              json.slot4.pos1.motorA = Number($('#motorA10').val());
              json.slot4.pos1.valvA = Number($('#valvA10').val());
              json.slot4.pos1.pulserA = $('#pulserA10').val();

              json.slot4.pos1.eltmCnl = Number($('#eltmCnl10').val());
              json.slot4.pos1.ppl_V = Number($('#ppl_V10').val());
              json.slot4.pos1.ppl_C = Number($('#ppl_C10').val());
              json.slot4.pos1.ppl_P = Number($('#ppl_P10').val());
              json.slot4.pos1.rv_motor = Number($('#rv_P10').val());

              if($('#mode10').val().includes("H")){
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
                json.slot4.pos1.pwm_b = Number($('#pwm_b_P10').val());
              }else{
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
              }


              break;

              case 9:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();

              json.slot1.pos1.motorB = Number($('#motorB1').val());
              json.slot1.pos1.valvB = Number($('#valvB1').val());
              json.slot1.pos1.pulserB = $('#pulserB1').val();


              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());


              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }

              json.slot1.pos2  = {};
              json.slot1.pos2.bico = Number($('#bico2').val());
              json.slot1.pos2.mode =  $('#mode2').val();
              json.slot1.pos2.motorA = Number($('#motorA2').val());
              json.slot1.pos2.valvA = Number($('#valvA2').val());
              json.slot1.pos2.pulserA = $('#pulserA2').val();

              json.slot1.pos2.motorB = Number($('#motorB2').val());
              json.slot1.pos2.valvB = Number($('#valvB2').val());
              json.slot1.pos2.pulserB = $('#pulserB2').val();

              json.slot1.pos2.eltmCnl = Number($('#eltmCnl2').val());
              json.slot1.pos2.ppl_V = Number($('#ppl_V2').val());
              json.slot1.pos2.ppl_C = Number($('#ppl_C2').val());
              json.slot1.pos2.ppl_P = Number($('#ppl_P2').val());
              json.slot1.pos2.rv_motor = Number($('#rv_P2').val());

              if($('#mode2').val().includes("H")){
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
                json.slot1.pos2.pwm_b = Number($('#pwm_b_P2').val());
              }else{
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
              }

              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();

              json.slot2.pos1  = {};
              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.motorB = Number($('#motorB4').val());
              json.slot2.pos1.valvB = Number($('#valvB4').val());
              json.slot2.pos1.pulserB = $('#pulserB4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }


              json.slot2.pos2  = {};
              json.slot2.pos2.bico = Number($('#bico5').val());
              json.slot2.pos2.mode =  $('#mode5').val();
              json.slot2.pos2.motorA = Number($('#motorA5').val());
              json.slot2.pos2.valvA = Number($('#valvA5').val());
              json.slot2.pos2.pulserA = $('#pulserA5').val();

              json.slot2.pos2.motorB = Number($('#motorB5').val());
              json.slot2.pos2.valvB = Number($('#valvB5').val());
              json.slot2.pos2.pulserB = $('#pulserB5').val();

              json.slot2.pos2.eltmCnl = Number($('#eltmCnl5').val());
              json.slot2.pos2.ppl_V = Number($('#ppl_V5').val());
              json.slot2.pos2.ppl_C = Number($('#ppl_C5').val());
              json.slot2.pos2.ppl_P = Number($('#ppl_P5').val());
              json.slot2.pos2.rv_motor = Number($('#rv_P5').val());

              if($('#mode5').val().includes("H")){
                json.slot2.pos2.pwm_a = Number($('#pwm_a_P5').val());
                json.slot2.pos2.pwm_b = Number($('#pwm_b_P5').val());
              }else{
                json.slot2.pos2.pwm_a = Number($('#pwm_a_P5').val());
              }


              break;

              case 10:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();


              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }

              json.slot1.pos2  = {};
              json.slot1.pos2.bico = Number($('#bico2').val());
              json.slot1.pos2.mode =  $('#mode2').val();
              json.slot1.pos2.motorA = Number($('#motorA2').val());
              json.slot1.pos2.valvA = Number($('#valvA2').val());
              json.slot1.pos2.pulserA = $('#pulserA2').val();

              json.slot1.pos2.motorB = Number($('#motorB2').val());
              json.slot1.pos2.valvB = Number($('#valvB2').val());
              json.slot1.pos2.pulserB = $('#pulserB2').val();

              json.slot1.pos2.eltmCnl = Number($('#eltmCnl2').val());
              json.slot1.pos2.ppl_V = Number($('#ppl_V2').val());
              json.slot1.pos2.ppl_C = Number($('#ppl_C2').val());
              json.slot1.pos2.ppl_P = Number($('#ppl_P2').val());
              json.slot1.pos2.rv_motor = Number($('#rv_P2').val());

              if($('#mode2').val().includes("H")){
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
                json.slot1.pos2.pwm_b = Number($('#pwm_b_P2').val());
              }else{
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
              }


              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();

              json.slot2.pos1  = {};
              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.motorB = Number($('#motorB4').val());
              json.slot2.pos1.valvB = Number($('#valvB4').val());
              json.slot2.pos1.pulserB = $('#pulserB4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }

              json.slot2.pos2  = {};
              json.slot2.pos2.bico = Number($('#bico5').val());
              json.slot2.pos2.mode =  $('#mode5').val();
              json.slot2.pos2.motorA = Number($('#motorA5').val());
              json.slot2.pos2.valvA = Number($('#valvA5').val());
              json.slot2.pos2.pulserA = $('#pulserA5').val();

              json.slot2.pos2.eltmCnl = Number($('#eltmCnl5').val());
              json.slot2.pos2.ppl_V = Number($('#ppl_V5').val());
              json.slot2.pos2.ppl_C = Number($('#ppl_C5').val());
              json.slot2.pos2.ppl_P = Number($('#ppl_P5').val());
              json.slot2.pos2.rv_motor = Number($('#rv_P5').val());

              if($('#mode5').val().includes("H")){
                json.slot2.pos2.pwm_a = Number($('#pwm_a_P5').val());
                json.slot2.pos2.pwm_b = Number($('#pwm_b_P5').val());
              }else{
                json.slot2.pos2.pwm_a = Number($('#pwm_a_P5').val());
              }

              break;

              case 11:


              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();

              json.slot1.pos1.motorB = Number($('#motorB1').val());
              json.slot1.pos1.valvB = Number($('#valvB1').val());
              json.slot1.pos1.pulserB = $('#pulserB1').val();

              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }


              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();
              json.slot2.pos1  = {};

              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.motorB = Number($('#motorB4').val());
              json.slot2.pos1.valvB = Number($('#valvB4').val());
              json.slot2.pos1.pulserB = $('#pulserB4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }


              json.slot3 = {};

              json.slot3.ladoInmetro = $('#ladoInmetro3').val();
              json.slot3.logico = Number($('#logico3').val());
              json.slot3.dspA = $('#dspA3').val();
              json.slot3.dspB = $('#dspB3').val();   
              json.slot3.eltm = $('#eltm3').val();
              json.slot3.LcdOrdem = $('#LcdOrdem3').val();
              json.slot3.pos1  = {};

              json.slot3.pos1.bico = Number($('#bico7').val());
              json.slot3.pos1.mode =  $('#mode7').val();
              json.slot3.pos1.motorA = Number($('#motorA7').val());
              json.slot3.pos1.valvA = Number($('#valvA7').val());
              json.slot3.pos1.pulserA = $('#pulserA7').val();

              json.slot3.pos1.motorB = Number($('#motorB7').val());
              json.slot3.pos1.valvB = Number($('#valvB7').val());
              json.slot3.pos1.pulserB = $('#pulserB7').val();

              json.slot3.pos1.eltmCnl = Number($('#eltmCnl7').val());
              json.slot3.pos1.ppl_V = Number($('#ppl_V7').val());
              json.slot3.pos1.ppl_C = Number($('#ppl_C7').val());
              json.slot3.pos1.ppl_P = Number($('#ppl_P7').val());
              json.slot3.pos1.rv_motor = Number($('#rv_P7').val());

              if($('#mode7').val().includes("H")){
                json.slot3.pos1.pwm_a = Number($('#pwm_a_P7').val());
                json.slot3.pos1.pwm_b = Number($('#pwm_b_P7').val());
              }else{
                json.slot3.pos1.pwm_a = Number($('#pwm_a_P7').val());
              }

              json.slot4 = {};

              json.slot4.ladoInmetro = $('#ladoInmetro4').val();
              json.slot4.logico = Number($('#logico4').val());
              json.slot4.dspA = $('#dspA4').val();
              json.slot4.dspB = $('#dspB4').val();   
              json.slot4.eltm = $('#eltm4').val();
              json.slot4.LcdOrdem = $('#LcdOrdem4').val();
              json.slot4.pos1  = {};

              json.slot4.pos1.bico = Number($('#bico10').val());
              json.slot4.pos1.mode =  $('#mode10').val();
              json.slot4.pos1.motorA = Number($('#motorA10').val());
              json.slot4.pos1.valvA = Number($('#valvA10').val());
              json.slot4.pos1.pulserA = $('#pulserA10').val();
              
              
              json.slot4.pos1.motorB = Number($('#motorB10').val());
              json.slot4.pos1.valvB = Number($('#valvB10').val());
              json.slot4.pos1.pulserB = $('#pulserB10').val();
              


              json.slot4.pos1.eltmCnl = Number($('#eltmCnl10').val());
              json.slot4.pos1.ppl_V = Number($('#ppl_V10').val());
              json.slot4.pos1.ppl_C = Number($('#ppl_C10').val());
              json.slot4.pos1.ppl_P = Number($('#ppl_P10').val());
              json.slot4.pos1.rv_motor = Number($('#rv_P10').val());

              if($('#mode10').val().includes("H")){
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
                json.slot4.pos1.pwm_b = Number($('#pwm_b_P10').val());
              }else{
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
              }

              break;

              case 12:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();

              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }


              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();
              json.slot2.pos1  = {};

              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }

              json.slot3 = {};

              json.slot3.ladoInmetro = $('#ladoInmetro3').val();
              json.slot3.logico = Number($('#logico3').val());
              json.slot3.dspA = $('#dspA3').val();
              json.slot3.dspB = $('#dspB3').val();   
              json.slot3.eltm = $('#eltm3').val();
              json.slot3.LcdOrdem = $('#LcdOrdem3').val();
              json.slot3.pos1  = {};

              json.slot3.pos1.bico = Number($('#bico7').val());
              json.slot3.pos1.mode =  $('#mode7').val();
              json.slot3.pos1.motorA = Number($('#motorA7').val());
              json.slot3.pos1.valvA = Number($('#valvA7').val());
              json.slot3.pos1.pulserA = $('#pulserA7').val();

              json.slot3.pos1.motorB = Number($('#motorB7').val());
              json.slot3.pos1.valvB = Number($('#valvB7').val());
              json.slot3.pos1.pulserB = $('#pulserB7').val();

              json.slot3.pos1.eltmCnl = Number($('#eltmCnl7').val());
              json.slot3.pos1.ppl_V = Number($('#ppl_V7').val());
              json.slot3.pos1.ppl_C = Number($('#ppl_C7').val());
              json.slot3.pos1.ppl_P = Number($('#ppl_P7').val());
              json.slot3.pos1.rv_motor = Number($('#rv_P7').val());

              if($('#mode7').val().includes("H")){
                json.slot3.pos1.pwm_a = Number($('#pwm_a_P7').val());
                json.slot3.pos1.pwm_b = Number($('#pwm_b_P7').val());
              }else{
                json.slot3.pos1.pwm_a = Number($('#pwm_a_P7').val());
              }


              json.slot4 = {};

              json.slot4.ladoInmetro = $('#ladoInmetro4').val();
              json.slot4.logico = Number($('#logico4').val());
              json.slot4.dspA = $('#dspA4').val();
              json.slot4.dspB = $('#dspB4').val();   
              json.slot4.eltm = $('#eltm4').val();
              json.slot4.LcdOrdem = $('#LcdOrdem4').val();
              json.slot4.pos1  = {};

              json.slot4.pos1.bico = Number($('#bico10').val());
              json.slot4.pos1.mode =  $('#mode10').val();
              json.slot4.pos1.motorA = Number($('#motorA10').val());
              json.slot4.pos1.valvA = Number($('#valvA10').val());
              json.slot4.pos1.pulserA = $('#pulserA10').val();

              json.slot4.pos1.motorB = Number($('#motorB10').val());
              json.slot4.pos1.valvB = Number($('#valvB10').val());
              json.slot4.pos1.pulserB = $('#pulserB10').val();

              json.slot4.pos1.eltmCnl = Number($('#eltmCnl10').val());
              json.slot4.pos1.ppl_V = Number($('#ppl_V10').val());
              json.slot4.pos1.ppl_C = Number($('#ppl_C10').val());
              json.slot4.pos1.ppl_P = Number($('#ppl_P10').val());
              json.slot4.pos1.rv_motor = Number($('#rv_P10').val());

              if($('#mode10').val().includes("H")){
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
                json.slot4.pos1.pwm_b = Number($('#pwm_b_P10').val());
              }else{
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
              }


              break;

              case 13:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();
              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }

              json.slot1.pos2  = {};
              json.slot1.pos2.bico = Number($('#bico2').val());
              json.slot1.pos2.mode =  $('#mode2').val();
              json.slot1.pos2.motorA = Number($('#motorA2').val());
              json.slot1.pos2.valvA = Number($('#valvA2').val());
              json.slot1.pos2.pulserA = $('#pulserA2').val();
              json.slot1.pos2.eltmCnl = Number($('#eltmCnl2').val());
              json.slot1.pos2.ppl_V = Number($('#ppl_V2').val());
              json.slot1.pos2.ppl_C = Number($('#ppl_C2').val());
              json.slot1.pos2.ppl_P = Number($('#ppl_P2').val());
              json.slot1.pos2.rv_motor = Number($('#rv_P2').val());

              if($('#mode2').val().includes("H")){
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
                json.slot1.pos2.pwm_b = Number($('#pwm_b_P2').val());
              }else{
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
              }


              json.slot1.pos3  = {};
              json.slot1.pos3.bico = Number($('#bico3').val());
              json.slot1.pos3.mode =  $('#mode3').val();
              json.slot1.pos3.motorA = Number($('#motorA3').val());
              json.slot1.pos3.valvA = Number($('#valvA3').val());
              json.slot1.pos3.pulserA = $('#pulserA3').val();
              json.slot1.pos3.eltmCnl = Number($('#eltmCnl3').val());
              json.slot1.pos3.ppl_V = Number($('#ppl_V3').val());
              json.slot1.pos3.ppl_C = Number($('#ppl_C3').val());
              json.slot1.pos3.ppl_P = Number($('#ppl_P3').val());
              json.slot1.pos3.rv_motor = Number($('#rv_P3').val());

              if($('#mode3').val().includes("H")){
                json.slot1.pos3.pwm_a = Number($('#pwm_a_P3').val());
                json.slot1.pos3.pwm_b = Number($('#pwm_b_P3').val());
              }else{
                json.slot1.pos3.pwm_a = Number($('#pwm_a_P3').val());
              }


              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();

              json.slot2.pos1  = {};
              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }


              json.slot2.pos2  = {};
              json.slot2.pos2.bico = Number($('#bico5').val());
              json.slot2.pos2.mode =  $('#mode5').val();
              json.slot2.pos2.motorA = Number($('#motorA5').val());
              json.slot2.pos2.valvA = Number($('#valvA5').val());
              json.slot2.pos2.pulserA = $('#pulserA5').val();

              json.slot2.pos2.eltmCnl = Number($('#eltmCnl5').val());
              json.slot2.pos2.ppl_V = Number($('#ppl_V5').val());
              json.slot2.pos2.ppl_C = Number($('#ppl_C5').val());
              json.slot2.pos2.ppl_P = Number($('#ppl_P5').val());
              json.slot2.pos2.rv_motor = Number($('#rv_P5').val());

              json.slot2.pos3  = {};
              json.slot2.pos3.bico = Number($('#bico6').val());
              json.slot2.pos3.mode =  $('#mode6').val();
              json.slot2.pos3.motorA = Number($('#motorA6').val());
              json.slot2.pos3.valvA = Number($('#valvA6').val());
              json.slot2.pos3.pulserA = $('#pulserA6').val();

              json.slot2.pos3.eltmCnl = Number($('#eltmCnl6').val());
              json.slot2.pos3.ppl_V = Number($('#ppl_V6').val());
              json.slot2.pos3.ppl_C = Number($('#ppl_C6').val());
              json.slot2.pos3.ppl_P = Number($('#ppl_P6').val());
              json.slot2.pos3.rv_motor = Number($('#rv_P6').val());

              if($('#mode5').val().includes("H")){
                json.slot2.pos3.pwm_a = Number($('#pwm_a_P6').val());
                json.slot2.pos3.pwm_b = Number($('#pwm_b_P6').val());
              }else{
                json.slot2.pos3.pwm_a = Number($('#pwm_a_P6').val());
              }

              break;

              case 14:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();

              json.slot1.pos1.motorB = Number($('#motorB1').val());
              json.slot1.pos1.valvB = Number($('#valvB1').val());
              json.slot1.pos1.pulserB = $('#pulserB1').val();

              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              json.slot1.pos2  = {};
              json.slot1.pos2.bico = Number($('#bico2').val());
              json.slot1.pos2.mode =  $('#mode2').val();
              json.slot1.pos2.motorA = Number($('#motorA2').val());
              json.slot1.pos2.valvA = Number($('#valvA2').val());
              json.slot1.pos2.pulserA = $('#pulserA2').val();
              json.slot1.pos2.eltmCnl = Number($('#eltmCnl2').val());
              json.slot1.pos2.ppl_V = Number($('#ppl_V2').val());
              json.slot1.pos2.ppl_C = Number($('#ppl_C2').val());
              json.slot1.pos2.ppl_P = Number($('#ppl_P2').val());
              json.slot1.pos2.rv_motor = Number($('#rv_P2').val());

              if($('#mode2').val().includes("H")){
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
                json.slot1.pos2.pwm_b = Number($('#pwm_b_P2').val());
              }else{
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
              }


              json.slot1.pos3  = {};
              json.slot1.pos3.bico = Number($('#bico3').val());
              json.slot1.pos3.mode =  $('#mode3').val();
              json.slot1.pos3.motorA = Number($('#motorA3').val());
              json.slot1.pos3.valvA = Number($('#valvA3').val());
              json.slot1.pos3.pulserA = $('#pulserA3').val();
              json.slot1.pos3.eltmCnl = Number($('#eltmCnl3').val());
              json.slot1.pos3.ppl_V = Number($('#ppl_V3').val());
              json.slot1.pos3.ppl_C = Number($('#ppl_C3').val());
              json.slot1.pos3.ppl_P = Number($('#ppl_P3').val());
              json.slot1.pos3.rv_motor = Number($('#rv_P3').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }


              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();

              json.slot2.pos1  = {};
              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }


              json.slot2.pos2  = {};
              json.slot2.pos2.bico = Number($('#bico5').val());
              json.slot2.pos2.mode =  $('#mode5').val();
              json.slot2.pos2.motorA = Number($('#motorA5').val());
              json.slot2.pos2.valvA = Number($('#valvA5').val());
              json.slot2.pos2.pulserA = $('#pulserA5').val();

              json.slot2.pos2.eltmCnl = Number($('#eltmCnl5').val());
              json.slot2.pos2.ppl_V = Number($('#ppl_V5').val());
              json.slot2.pos2.ppl_C = Number($('#ppl_C5').val());
              json.slot2.pos2.ppl_P = Number($('#ppl_P5').val());
              json.slot2.pos2.rv_motor = Number($('#rv_P5').val());


              if($('#mode5').val().includes("H")){
                json.slot2.pos2.pwm_a = Number($('#pwm_a_P5').val());
                json.slot2.pos2.pwm_b = Number($('#pwm_b_P5').val());
              }else{
                json.slot2.pos2.pwm_a = Number($('#pwm_a_P5').val());
              }

              json.slot2.pos3  = {};
              json.slot2.pos3.bico = Number($('#bico6').val());
              json.slot2.pos3.mode =  $('#mode6').val();
              json.slot2.pos3.motorA = Number($('#motorA6').val());
              json.slot2.pos3.valvA = Number($('#valvA6').val());
              json.slot2.pos3.pulserA = $('#pulserA6').val();

              json.slot2.pos3.motorB = Number($('#motorB6').val());
              json.slot2.pos3.valvB = Number($('#valvB6').val());
              json.slot2.pos3.pulserB = $('#pulserB6').val();

              json.slot2.pos3.eltmCnl = Number($('#eltmCnl6').val());
              json.slot2.pos3.ppl_V = Number($('#ppl_V6').val());
              json.slot2.pos3.ppl_C = Number($('#ppl_C6').val());
              json.slot2.pos3.ppl_P = Number($('#ppl_P6').val());
              json.slot2.pos3.rv_motor = Number($('#rv_P6').val());

              if($('#mode6').val().includes("H")){
                json.slot2.pos3.pwm_a = Number($('#pwm_a_P6').val());
                json.slot2.pos3.pwm_b = Number($('#pwm_b_P6').val());
              }else{
                json.slot2.pos3.pwm_a = Number($('#pwm_a_P6').val());
              }


              break;

              case 15:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();

              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }


              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();
              json.slot2.pos1  = {};

              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }


              json.slot3 = {};

              json.slot3.ladoInmetro = $('#ladoInmetro3').val();
              json.slot3.logico = Number($('#logico3').val());
              json.slot3.dspA = $('#dspA3').val();
              json.slot3.dspB = $('#dspB3').val();   
              json.slot3.eltm = $('#eltm3').val();
              json.slot3.LcdOrdem = $('#LcdOrdem3').val();

              json.slot3.pos1  = {};
              json.slot3.pos1.bico = Number($('#bico7').val());
              json.slot3.pos1.mode =  $('#mode7').val();
              json.slot3.pos1.motorA = Number($('#motorA7').val());
              json.slot3.pos1.valvA = Number($('#valvA7').val());
              json.slot3.pos1.pulserA = $('#pulserA7').val();

              json.slot3.pos1.eltmCnl = Number($('#eltmCnl7').val());
              json.slot3.pos1.ppl_V = Number($('#ppl_V7').val());
              json.slot3.pos1.ppl_C = Number($('#ppl_C7').val());
              json.slot3.pos1.ppl_P = Number($('#ppl_P7').val());
              json.slot3.pos1.rv_motor = Number($('#rv_P7').val());


              if($('#mode7').val().includes("H")){
                json.slot3.pos1.pwm_a = Number($('#pwm_a_P7').val());
                json.slot3.pos1.pwm_b = Number($('#pwm_b_P7').val());
              }else{
                json.slot3.pos1.pwm_a = Number($('#pwm_a_P7').val());
              }

              json.slot3.pos2  = {};
              json.slot3.pos2.bico = Number($('#bico8').val());
              json.slot3.pos2.mode =  $('#mode8').val();
              json.slot3.pos2.motorA = Number($('#motorA8').val());
              json.slot3.pos2.valvA = Number($('#valvA8').val());
              json.slot3.pos2.pulserA = $('#pulserA8').val();

              json.slot3.pos2.eltmCnl = Number($('#eltmCnl8').val());
              json.slot3.pos2.ppl_V = Number($('#ppl_V8').val());
              json.slot3.pos2.ppl_C = Number($('#ppl_C8').val());
              json.slot3.pos2.ppl_P = Number($('#ppl_P8').val());
              json.slot3.pos2.rv_motor = Number($('#rv_P8').val());

              if($('#mode8').val().includes("H")){
                json.slot3.pos2.pwm_a = Number($('#pwm_a_P8').val());
                json.slot3.pos2.pwm_b = Number($('#pwm_b_P8').val());
              }else{
                json.slot3.pos2.pwm_a = Number($('#pwm_a_P8').val());
              }

              json.slot4 = {};

              json.slot4.ladoInmetro = $('#ladoInmetro4').val();
              json.slot4.logico = Number($('#logico4').val());
              json.slot4.dspA = $('#dspA4').val();
              json.slot4.dspB = $('#dspB4').val();   
              json.slot4.eltm = $('#eltm4').val();
              json.slot4.LcdOrdem = $('#LcdOrdem4').val();

              json.slot4.pos1  = {};
              json.slot4.pos1.bico = Number($('#bico10').val());
              json.slot4.pos1.mode =  $('#mode10').val();
              json.slot4.pos1.motorA = Number($('#motorA10').val());
              json.slot4.pos1.valvA = Number($('#valvA10').val());
              json.slot4.pos1.pulserA = $('#pulserA10').val();

              json.slot4.pos1.eltmCnl = Number($('#eltmCnl10').val());
              json.slot4.pos1.ppl_V = Number($('#ppl_V10').val());
              json.slot4.pos1.ppl_C = Number($('#ppl_C10').val());
              json.slot4.pos1.ppl_P = Number($('#ppl_P10').val());
              json.slot4.pos1.rv_motor = Number($('#rv_P10').val());

              if($('#mode10').val().includes("H")){
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
                json.slot4.pos1.pwm_b = Number($('#pwm_b_P10').val());
              }else{
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
              }

              json.slot4.pos2  = {};
              json.slot4.pos2.bico = Number($('#bico11').val());
              json.slot4.pos2.mode =  $('#mode11').val();
              json.slot4.pos2.motorA = Number($('#motorA11').val());
              json.slot4.pos2.valvA = Number($('#valvA11').val());
              json.slot4.pos2.pulserA = $('#pulserA11').val();

              json.slot4.pos2.eltmCnl = Number($('#eltmCnl11').val());
              json.slot4.pos2.ppl_V = Number($('#ppl_V11').val());
              json.slot4.pos2.ppl_C = Number($('#ppl_C11').val());
              json.slot4.pos2.ppl_P = Number($('#ppl_P11').val());
              json.slot4.pos2.rv_motor = Number($('#rv_P11').val());

              if($('#mode11').val().includes("H")){
                json.slot4.pos2.pwm_a = Number($('#pwm_a_P11').val());
                json.slot4.pos2.pwm_b = Number($('#pwm_b_P11').val());
              }else{
                json.slot4.pos2.pwm_a = Number($('#pwm_a_P11').val());
              }

              break;

              case 16:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();

              json.slot1.pos1.motorB = Number($('#motorB1').val());
              json.slot1.pos1.valvB = Number($('#valvB1').val());
              json.slot1.pos1.pulserB = $('#pulserB1').val();

              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }


              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();
              json.slot2.pos1  = {};

              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.motorB = Number($('#motorB4').val());
              json.slot2.pos1.valvB = Number($('#valvB4').val());
              json.slot2.pos1.pulserB = $('#pulserB4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }


              json.slot3 = {};

              json.slot3.ladoInmetro = $('#ladoInmetro3').val();
              json.slot3.logico = Number($('#logico3').val());
              json.slot3.dspA = $('#dspA3').val();
              json.slot3.dspB = $('#dspB3').val();   
              json.slot3.eltm = $('#eltm3').val();
              json.slot3.LcdOrdem = $('#LcdOrdem3').val();

              json.slot3.pos1  = {};
              json.slot3.pos1.bico = Number($('#bico7').val());
              json.slot3.pos1.mode =  $('#mode7').val();
              json.slot3.pos1.motorA = Number($('#motorA7').val());
              json.slot3.pos1.valvA = Number($('#valvA7').val());
              json.slot3.pos1.pulserA = $('#pulserA7').val();

              json.slot3.pos1.eltmCnl = Number($('#eltmCnl7').val());
              json.slot3.pos1.ppl_V = Number($('#ppl_V7').val());
              json.slot3.pos1.ppl_C = Number($('#ppl_C7').val());
              json.slot3.pos1.ppl_P = Number($('#ppl_P7').val());
              json.slot3.pos1.rv_motor = Number($('#rv_P7').val());

              json.slot3.pos2  = {};
              json.slot3.pos2.bico = Number($('#bico8').val());
              json.slot3.pos2.mode =  $('#mode8').val();
              json.slot3.pos2.motorA = Number($('#motorA8').val());
              json.slot3.pos2.valvA = Number($('#valvA8').val());
              json.slot3.pos2.pulserA = $('#pulserA8').val();

              json.slot3.pos2.eltmCnl = Number($('#eltmCnl8').val());
              json.slot3.pos2.ppl_V = Number($('#ppl_V8').val());
              json.slot3.pos2.ppl_C = Number($('#ppl_C8').val());
              json.slot3.pos2.ppl_P = Number($('#ppl_P8').val());
              json.slot3.pos1.rv_motor = Number($('#rv_P8').val());

              if($('#mode8').val().includes("H")){
                json.slot3.pos2.pwm_a = Number($('#pwm_a_P8').val());
                json.slot3.pos2.pwm_b = Number($('#pwm_b_P8').val());
              }else{
                json.slot3.pos2.pwm_a = Number($('#pwm_a_P8').val());
              }


              json.slot4 = {};

              json.slot4.ladoInmetro = $('#ladoInmetro4').val();
              json.slot4.logico = Number($('#logico4').val());
              json.slot4.dspA = $('#dspA4').val();
              json.slot4.dspB = $('#dspB4').val();   
              json.slot4.eltm = $('#eltm4').val();
              json.slot4.LcdOrdem = $('#LcdOrdem4').val();

              json.slot4.pos1  = {};
              json.slot4.pos1.bico = Number($('#bico10').val());
              json.slot4.pos1.mode =  $('#mode10').val();
              json.slot4.pos1.motorA = Number($('#motorA10').val());
              json.slot4.pos1.valvA = Number($('#valvA10').val());
              json.slot4.pos1.pulserA = $('#pulserA10').val();

              json.slot4.pos1.eltmCnl = Number($('#eltmCnl10').val());
              json.slot4.pos1.ppl_V = Number($('#ppl_V10').val());
              json.slot4.pos1.ppl_C = Number($('#ppl_C10').val());
              json.slot4.pos1.ppl_P = Number($('#ppl_P10').val());
              json.slot4.pos1.rv_motor = Number($('#rv_P10').val());

              if($('#mode10').val().includes("H")){
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
                json.slot4.pos1.pwm_b = Number($('#pwm_b_P10').val());
              }else{
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
              }

              json.slot4.pos2  = {};
              json.slot4.pos2.bico = Number($('#bico11').val());
              json.slot4.pos2.mode =  $('#mode11').val();
              json.slot4.pos2.motorA = Number($('#motorA11').val());
              json.slot4.pos2.valvA = Number($('#valvA11').val());
              json.slot4.pos2.pulserA = $('#pulserA11').val();

              json.slot4.pos2.eltmCnl = Number($('#eltmCnl11').val());
              json.slot4.pos2.ppl_V = Number($('#ppl_V11').val());
              json.slot4.pos2.ppl_C = Number($('#ppl_C11').val());
              json.slot4.pos2.ppl_P = Number($('#ppl_P11').val());
              json.slot4.pos2.rv_motor = Number($('#rv_P11').val());

              if($('#mode11').val().includes("H")){
                json.slot4.pos2.pwm_a = Number($('#pwm_a_P11').val());
                json.slot4.pos2.pwm_b = Number($('#pwm_b_P11').val());
              }else{
                json.slot4.pos2.pwm_a = Number($('#pwm_a_P11').val());
              }

              break;

              case 17:

              json.slot1 = {};

              json.slot1.ladoInmetro = $('#ladoInmetro1').val();
              json.slot1.logico = Number($('#logico1').val());
              json.slot1.dspA = $('#dspA1').val();
              json.slot1.dspB = $('#dspB1').val();   
              json.slot1.eltm = $('#eltm1').val();
              json.slot1.LcdOrdem = $('#LcdOrdem1').val();

              json.slot1.pos1  = {};
              json.slot1.pos1.bico = Number($('#bico1').val());
              json.slot1.pos1.mode =  $('#mode1').val();
              json.slot1.pos1.motorA = Number($('#motorA1').val());
              json.slot1.pos1.valvA = Number($('#valvA1').val());
              json.slot1.pos1.pulserA = $('#pulserA1').val();

              json.slot1.pos1.eltmCnl = Number($('#eltmCnl1').val());
              json.slot1.pos1.ppl_V = Number($('#ppl_V1').val());
              json.slot1.pos1.ppl_C = Number($('#ppl_C1').val());
              json.slot1.pos1.ppl_P = Number($('#ppl_P1').val());
              json.slot1.pos1.rv_motor = Number($('#rv_P1').val());

              if($('#mode1').val().includes("H")){
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
                json.slot1.pos1.pwm_b = Number($('#pwm_b_P1').val());
              }else{
                json.slot1.pos1.pwm_a = Number($('#pwm_a_P1').val());
              }


              json.slot1.pos2  = {};
              json.slot1.pos2.bico = Number($('#bico2').val());
              json.slot1.pos2.mode =  $('#mode2').val();
              json.slot1.pos2.motorA = Number($('#motorA2').val());
              json.slot1.pos2.valvA = Number($('#valvA2').val());
              json.slot1.pos2.pulserA = $('#pulserA2').val();

              json.slot1.pos2.eltmCnl = Number($('#eltmCnl2').val());
              json.slot1.pos2.ppl_V = Number($('#ppl_V2').val());
              json.slot1.pos2.ppl_C = Number($('#ppl_C2').val());
              json.slot1.pos2.ppl_P = Number($('#ppl_P2').val());
              json.slot1.pos2.rv_motor = Number($('#rv_P2').val());

              if($('#mode2').val().includes("H")){
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
                json.slot1.pos2.pwm_b = Number($('#pwm_b_P2').val());
              }else{
                json.slot1.pos2.pwm_a = Number($('#pwm_a_P2').val());
              }


              json.slot2 = {};

              json.slot2.ladoInmetro = $('#ladoInmetro2').val();
              json.slot2.logico = Number($('#logico2').val());
              json.slot2.dspA = $('#dspA2').val();
              json.slot2.dspB = $('#dspB2').val();   
              json.slot2.eltm = $('#eltm2').val();
              json.slot2.LcdOrdem = $('#LcdOrdem2').val();

              json.slot2.pos1  = {};
              json.slot2.pos1.bico = Number($('#bico4').val());
              json.slot2.pos1.mode =  $('#mode4').val();
              json.slot2.pos1.motorA = Number($('#motorA4').val());
              json.slot2.pos1.valvA = Number($('#valvA4').val());
              json.slot2.pos1.pulserA = $('#pulserA4').val();

              json.slot2.pos1.eltmCnl = Number($('#eltmCnl4').val());
              json.slot2.pos1.ppl_V = Number($('#ppl_V4').val());
              json.slot2.pos1.ppl_C = Number($('#ppl_C4').val());
              json.slot2.pos1.ppl_P = Number($('#ppl_P4').val());
              json.slot2.pos1.rv_motor = Number($('#rv_P4').val());

              if($('#mode4').val().includes("H")){
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
                json.slot2.pos1.pwm_b = Number($('#pwm_b_P4').val());
              }else{
                json.slot2.pos1.pwm_a = Number($('#pwm_a_P4').val());
              }

              json.slot2.pos2  = {};
              json.slot2.pos2.bico = Number($('#bico5').val());
              json.slot2.pos2.mode =  $('#mode5').val();
              json.slot2.pos2.motorA = Number($('#motorA5').val());
              json.slot2.pos2.valvA = Number($('#valvA5').val());
              json.slot2.pos2.pulserA = $('#pulserA5').val();

              json.slot2.pos2.eltmCnl = Number($('#eltmCnl5').val());
              json.slot2.pos2.ppl_V = Number($('#ppl_V5').val());
              json.slot2.pos2.ppl_C = Number($('#ppl_C5').val());
              json.slot2.pos2.ppl_P = Number($('#ppl_P5').val());
              json.slot2.pos2.rv_motor = Number($('#rv_P5').val());

              if($('#mode5').val().includes("H")){
                json.slot2.pos2.pwm_a = Number($('#pwm_a_P5').val());
                json.slot2.pos2.pwm_b = Number($('#pwm_b_P5').val());
              }else{
                json.slot2.pos2.pwm_a = Number($('#pwm_a_P5').val());
              }


              json.slot3 = {};

              json.slot3.ladoInmetro = $('#ladoInmetro3').val();
              json.slot3.logico = Number($('#logico3').val());
              json.slot3.dspA = $('#dspA3').val();
              json.slot3.dspB = $('#dspB3').val();   
              json.slot3.eltm = $('#eltm3').val();
              json.slot3.LcdOrdem = $('#LcdOrdem3').val();

              json.slot3.pos1  = {};
              json.slot3.pos1.bico = Number($('#bico7').val());
              json.slot3.pos1.mode =  $('#mode7').val();
              json.slot3.pos1.motorA = Number($('#motorA7').val());
              json.slot3.pos1.valvA = Number($('#valvA7').val());
              json.slot3.pos1.pulserA = $('#pulserA7').val();

              json.slot3.pos1.eltmCnl = Number($('#eltmCnl7').val());
              json.slot3.pos1.ppl_V = Number($('#ppl_V7').val());
              json.slot3.pos1.ppl_C = Number($('#ppl_C7').val());
              json.slot3.pos1.ppl_P = Number($('#ppl_P7').val());
              json.slot3.pos1.rv_motor = Number($('#rv_P7').val());

              if($('#mode7').val().includes("H")){
                json.slot3.pos1.pwm_a = Number($('#pwm_a_P7').val());
                json.slot3.pos1.pwm_b = Number($('#pwm_b_P7').val());
              }else{
                json.slot3.pos1.pwm_a = Number($('#pwm_a_P7').val());
              }

              json.slot3.pos2  = {};
              json.slot3.pos2.bico = Number($('#bico8').val());
              json.slot3.pos2.mode =  $('#mode8').val();
              json.slot3.pos2.motorA = Number($('#motorA8').val());
              json.slot3.pos2.valvA = Number($('#valvA8').val());
              json.slot3.pos2.pulserA = $('#pulserA8').val();

              json.slot3.pos2.eltmCnl = Number($('#eltmCnl8').val());
              json.slot3.pos2.ppl_V = Number($('#ppl_V8').val());
              json.slot3.pos2.ppl_C = Number($('#ppl_C8').val());
              json.slot3.pos2.ppl_P = Number($('#ppl_P8').val());
              json.slot3.pos2.rv_motor = Number($('#rv_P8').val());

              if($('#mode8').val().includes("H")){
                json.slot3.pos2.pwm_a = Number($('#pwm_a_P8').val());
                json.slot3.pos2.pwm_b = Number($('#pwm_b_P8').val());
              }else{
                json.slot3.pos2.pwm_a = Number($('#pwm_a_P8').val());
              }


              json.slot4 = {};

              json.slot4.ladoInmetro = $('#ladoInmetro4').val();
              json.slot4.logico = Number($('#logico4').val());
              json.slot4.dspA = $('#dspA4').val();
              json.slot4.dspB = $('#dspB4').val();   
              json.slot4.eltm = $('#eltm4').val();
              json.slot4.LcdOrdem = $('#LcdOrdem4').val();

              json.slot4.pos1  = {};
              json.slot4.pos1.bico = Number($('#bico10').val());
              json.slot4.pos1.mode =  $('#mode10').val();
              json.slot4.pos1.motorA = Number($('#motorA10').val());
              json.slot4.pos1.valvA = Number($('#valvA10').val());
              json.slot4.pos1.pulserA = $('#pulserA10').val();

              json.slot4.pos1.eltmCnl = Number($('#eltmCnl10').val());
              json.slot4.pos1.ppl_V = Number($('#ppl_V10').val());
              json.slot4.pos1.ppl_C = Number($('#ppl_C10').val());
              json.slot4.pos1.ppl_P = Number($('#ppl_P10').val());
              json.slot4.pos1.rv_motor = Number($('#rv_P10').val());

              if($('#mode10').val().includes("H")){
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
                json.slot4.pos1.pwm_b = Number($('#pwm_b_P10').val());
              }else{
                json.slot4.pos1.pwm_a = Number($('#pwm_a_P10').val());
              }

              json.slot4.pos2  = {};
              json.slot4.pos2.bico = Number($('#bico11').val());
              json.slot4.pos2.mode =  $('#mode11').val();
              json.slot4.pos2.motorA = Number($('#motorA11').val());
              json.slot4.pos2.valvA = Number($('#valvA11').val());
              json.slot4.pos2.pulserA = $('#pulserA11').val();

              json.slot4.pos2.eltmCnl = Number($('#eltmCnl11').val());
              json.slot4.pos2.ppl_V = Number($('#ppl_V11').val());
              json.slot4.pos2.ppl_C = Number($('#ppl_C11').val());
              json.slot4.pos2.ppl_P = Number($('#ppl_P11').val());
              json.slot4.pos2.rv_motor = Number($('#rv_P11').val());

              if($('#mode11').val().includes("H")){
                json.slot4.pos2.pwm_a = Number($('#pwm_a_P11').val());
                json.slot4.pos2.pwm_b = Number($('#pwm_b_P11').val());
              }else{
                json.slot4.pos2.pwm_a = Number($('#pwm_a_P11').val());
              }

              break;


            }


            var jsonse = JSON.stringify(json,null,4);
          /*  var a = document.createElement('a');
            var file = new Blob([jsonse], {type: 'application/json'});
            a.href = URL.createObjectURL(file);
            a.download = modelo_Json+'.jsn';
            a.click();*/

            var zip = new JSZip();
            var f1 = zip.folder(modelo_tipo);
            var f2 = f1.folder(modelo_Json);
            f2.file("config.jsn",jsonse);

            zip.generateAsync({type:"blob"})
            .then(function(content) {
                // see FileSaver.js
                saveAs(content, "wertco.zip");
              });


          }


          function resetCamposHtml(){

            modelo_final = 0;

            $('#jsn').attr('hidden',true);
            $('.nenh').removeAttr('hidden');

            $('#SLOT1').removeAttr('hidden');
            $('#POS1').removeAttr('hidden');
            $('#altav1').removeAttr('hidden');
            $('#POS2').removeAttr('hidden');
            $('#altav2').removeAttr('hidden');
            $('#POS3').removeAttr('hidden');
            $('#altav3').removeAttr('hidden');


            $('#SLOT2').removeAttr('hidden');
            $('#POS4').removeAttr('hidden');
            $('#altav4').removeAttr('hidden');
            $('#POS5').removeAttr('hidden');
            $('#altav5').removeAttr('hidden');
            $('#POS6').removeAttr('hidden');
            $('#altav6').removeAttr('hidden');


            $('#SLOT3').removeAttr('hidden');
            $('#POS7').removeAttr('hidden');
            $('#altav7').removeAttr('hidden');
            $('#POS8').removeAttr('hidden');
            $('#altav8').removeAttr('hidden');
            $('#POS9').removeAttr('hidden');
            $('#altav9').removeAttr('hidden');


            $('#SLOT4').removeAttr('hidden');
            $('#POS10').removeAttr('hidden');
            $('#altav10').removeAttr('hidden');
            $('#POS11').removeAttr('hidden');
            $('#altav11').removeAttr('hidden');
            $('#POS12').removeAttr('hidden');
            $('#altav12').removeAttr('hidden');

            $("#SLOT1 input,select").attr('required', true);
            $("#SLOT2 input,select").attr('required', true);
            $("#SLOT3 input,select").attr('required', true);
            $("#SLOT4 input,select").attr('required', true);

          }


          function printJsonP(object){


            $('#titulo').val(object.titulo);
            $('#versao').val(object.versao);
            $('#unidade').val(object.unidade);
            $('#modelo').val(object.modelo);
            $('#cnpj').val(object.cnpj);
            $('#ip').val(object.ip);
            $('#gw').val(object.gw);
            $('#Mask').val(object.Mask);
            $('#dns').val(object.dns);
            $('#dhcp').val(object.dhcp);
            $('#hostname').val(object.hostname);
            if(object.industrial){
              $('#industrial').val(object.industrial);
            }
            $('#ble_id').val(object.ble_id);
            $('#protocolo1').val(object.protocolo1);
            $('#protocolo2').val(object.protocolo2);
            $('#Automacao').val(object.Automacao);
            $('#Identfid').val(object.Identfid);
            $('#valvula').val(object.valvula);
            $('#falta_de_pulsos').val(object.falta_de_pulsos);
            $('#tempo_de_start').val(object.tempo_de_start);
            $('#beep').val(object.beep);
            $('#Tempo_Desliga').val(object.Tempo_Desliga);
            $('#Timer_Ligar').val(object.Timer_Ligar);


            if(object.unidade === 'litros'){
              $('#ponto_baixa_vazao').val(object.ponto_baixa_vazao);
            }else{
              $('#ponto_baixa_vazao').val((object.ponto_baixa_vazao/3.1).toFixed(1));
            }

            
            $('#Limite_Volume').val(object.Limite_Volume);
            $('#total_Decimal').val(object.total_Decimal);
            $('#volume_Decimal').val(object.volume_Decimal);
            $('#preco_Decimal').val(object.preco_Decimal);
            $('#encerrante_dec').val(object.encerrante_dec);
            $('#encerrante_calc').val(object.encerrante_calc);
            $('#preset_P1').val(object.preset_P1);
            $('#preset_P2').val(object.preset_P2);
            $('#Arredonda').val(object.Arredonda);
            $('#texto_1').val(object.texto_1);
            $('#texto_2').val(object.texto_2);
            $('#texto_3').val(object.texto_3);


            $('#ini_dom').val(insert(object.Timer_Ligar.substring(0,4),2,':'));
            $('#fim_dom').val(insert(object.Timer_Ligar.substring(4,8),2,':'));

            $('#ini_seg').val(insert(object.Timer_Ligar.substring(8,12),2,':'));
            $('#fim_seg').val(insert(object.Timer_Ligar.substring(12,16),2,':'));

            $('#ini_ter').val(insert(object.Timer_Ligar.substring(16,20),2,':'));
            $('#fim_ter').val(insert(object.Timer_Ligar.substring(20,24),2,':'));

            $('#ini_qua').val(insert(object.Timer_Ligar.substring(24,28),2,':'));
            $('#fim_qua').val(insert(object.Timer_Ligar.substring(28,32),2,':'));

            $('#ini_qui').val(insert(object.Timer_Ligar.substring(32,36),2,':'));
            $('#fim_qui').val(insert(object.Timer_Ligar.substring(36,40),2,':'));

            $('#ini_sex').val(insert(object.Timer_Ligar.substring(40,44),2,':'));
            $('#fim_sex').val(insert(object.Timer_Ligar.substring(44,48),2,':'));

            $('#ini_sab').val(insert(object.Timer_Ligar.substring(48,52),2,':'));
            $('#fim_sab').val(insert(object.Timer_Ligar.substring(52,56),2,':'));


          }

          function printJsonSlot1(object){
            /********* SLOT 1 ***********************************/
            $('#ladoInmetro1').val(object.slot1.ladoInmetro);
            $('#logico1').val(object.slot1.logico);
            $('#dspA1').val(object.slot1.dspA);
            $('#dspB1').val(object.slot1.dspB);
            $('#eltm1').val(object.slot1.eltm);
            $('#LcdOrdem1').val(object.slot1.LcdOrdem);
          }

          function printJsonPos1(object){

            /********* POS 1 ***********************************/
            $('#bico1').val(object.slot1.pos1.bico);
            $('#mode1').val(object.slot1.pos1.mode);
            $('#motorA1').val(object.slot1.pos1.motorA);
            $('#valvA1').val(object.slot1.pos1.valvA);
            $('#pulserA1').val(object.slot1.pos1.pulserA);
            $('#eltmCnl1').val(object.slot1.pos1.eltmCnl);
            $('#ppl_V1').val(object.slot1.pos1.ppl_V);
            $('#ppl_C1').val(object.slot1.pos1.ppl_C);
            $('#ppl_P1').val(object.slot1.pos1.ppl_P);
            $('#rv_P1').val(object.slot1.pos1.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P1').prop('hidden', false)
              $('#pwm_b_P1').prop('hidden', false)
              $('#labelpwm1').prop('hidden', false)
              $('#pwm_a_P1').val(object.slot1.pos1.pwm_a);
              $('#pwm_b_P1').val(object.slot1.pos1.pwm_b);

            }else{
              $('#pwm_a_P1').prop('hidden', false)
              $('#pwm_b_P1').prop('hidden', true)
              $('#labelpwm1').prop('hidden', true)
              $('#pwm_a_P1').val(object.slot1.pos1.pwm_a);
            }
            
            console.log(object);

          }


          function printJsonPos1H(object){

            /********* POS 1 ***********************************/
            $('#bico1').val(object.slot1.pos1.bico);
            $('#mode1').val(object.slot1.pos1.mode);
            $('#motorA1').val(object.slot1.pos1.motorA);
            $('#valvA1').val(object.slot1.pos1.valvA);
            $('#pulserA1').val(object.slot1.pos1.pulserA);
            $('#motorB1').val(object.slot1.pos1.motorB);
            $('#valvB1').val(object.slot1.pos1.valvB);
            $('#pulserB1').val(object.slot1.pos1.pulserB);
            $('#eltmCnl1').val(object.slot1.pos1.eltmCnl);
            $('#ppl_V1').val(object.slot1.pos1.ppl_V);
            $('#ppl_C1').val(object.slot1.pos1.ppl_C);
            $('#ppl_P1').val(object.slot1.pos1.ppl_P);
            $('#rv_P1').val(object.slot1.pos1.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P1').prop('hidden', false)
              $('#pwm_b_P1').prop('hidden', false)
              $('#labelpwm1').prop('hidden', false)
              $('#pwm_a_P1').val(object.slot1.pos1.pwm_a);
              $('#pwm_b_P1').val(object.slot1.pos1.pwm_b);
            }else{
              $('#pwm_a_P1').prop('hidden', false)
              $('#pwm_b_P1').prop('hidden', true)
              $('#labelpwm1').prop('hidden', true)
              $('#pwm_a_P1').val(object.slot1.pos1.pwm_a);
            }

            console.log(object);

          }

          function printJsonPos2(object){

            /********* POS 2 ************************************/
            $('#bico2').val(object.slot1.pos2.bico);
            $('#mode2').val(object.slot1.pos2.mode);
            $('#motorA2').val(object.slot1.pos2.motorA);
            $('#valvA2').val(object.slot1.pos2.valvA);
            $('#pulserA2').val(object.slot1.pos2.pulserA);
            $('#eltmCnl2').val(object.slot1.pos2.eltmCnl);
            $('#ppl_V2').val(object.slot1.pos2.ppl_V);
            $('#ppl_C2').val(object.slot1.pos2.ppl_C);
            $('#ppl_P2').val(object.slot1.pos2.ppl_P);
            $('#rv_P2').val(object.slot1.pos2.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P2').prop('hidden', false)
              $('#pwm_b_P2').prop('hidden', false)
              $('#labelpwm2').prop('hidden', false)
              $('#pwm_a_P2').val(object.slot1.pos2.pwm_a);
              $('#pwm_b_P2').val(object.slot1.pos2.pwm_b);
            }else{
              $('#pwm_a_P2').prop('hidden', false)
              $('#pwm_b_P2').prop('hidden', true)
              $('#labelpwm2').prop('hidden', true)
              $('#pwm_a_P2').val(object.slot1.pos2.pwm_a);
            }

          }

          function printJsonPos2H(object){

            /********* POS 2 ************************************/
            $('#bico2').val(object.slot1.pos2.bico);
            $('#mode2').val(object.slot1.pos2.mode);
            $('#motorA2').val(object.slot1.pos2.motorA);
            $('#valvA2').val(object.slot1.pos2.valvA);
            $('#pulserA2').val(object.slot1.pos2.pulserA);
            $('#motorB2').val(object.slot1.pos2.motorB);
            $('#valvB2').val(object.slot1.pos2.valvB);
            $('#pulserB2').val(object.slot1.pos2.pulserB);
            $('#eltmCnl2').val(object.slot1.pos2.eltmCnl);
            $('#ppl_V2').val(object.slot1.pos2.ppl_V);
            $('#ppl_C2').val(object.slot1.pos2.ppl_C);
            $('#ppl_P2').val(object.slot1.pos2.ppl_P);
            $('#rv_P2').val(object.slot1.pos2.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P2').prop('hidden', false)
              $('#pwm_b_P2').prop('hidden', false)
              $('#labelpwm2').prop('hidden', false)
              $('#pwm_a_P2').val(object.slot1.pos2.pwm_a);
              $('#pwm_b_P2').val(object.slot1.pos2.pwm_b);
            }else{
              $('#pwm_a_P2').prop('hidden', false)
              $('#pwm_b_P2').prop('hidden', true)
              $('#labelpwm2').prop('hidden', true)
              $('#pwm_a_P2').val(object.slot1.pos2.pwm_a);
            }

          }

          function printJsonPos3(object){
            /********* POS 1 ***********************************/
            $('#bico3').val(object.slot1.pos3.bico);
            $('#mode3').val(object.slot1.pos3.mode);
            $('#motorA3').val(object.slot1.pos3.motorA);
            $('#valvA3').val(object.slot1.pos3.valvA);
            $('#pulserA3').val(object.slot1.pos3.pulserA);
            $('#eltmCnl3').val(object.slot1.pos3.eltmCnl);
            $('#ppl_V3').val(object.slot1.pos3.ppl_V);
            $('#ppl_C3').val(object.slot1.pos3.ppl_C);
            $('#ppl_P3').val(object.slot1.pos3.ppl_P);
            $('#rv_P3').val(object.slot1.pos3.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P3').prop('hidden', false)
              $('#pwm_b_P3').prop('hidden', false)
              $('#labelpwm3').prop('hidden', false)
              $('#pwm_a_P3').val(object.slot1.pos3.pwm_a);
              $('#pwm_b_P3').val(object.slot1.pos3.pwm_b);
            }else{
              $('#pwm_a_P3').prop('hidden', false)
              $('#pwm_b_P3').prop('hidden', true)
              $('#labelpwm3').prop('hidden', true)
              $('#pwm_a_P3').val(object.slot1.pos3.pwm_a);
            }
          }

          function printJsonPos3H(object){
            /********* POS 1 ***********************************/
            $('#bico3').val(object.slot1.pos3.bico);
            $('#mode3').val(object.slot1.pos3.mode);
            $('#motorA3').val(object.slot1.pos3.motorA);
            $('#valvA3').val(object.slot1.pos3.valvA);
            $('#pulserA3').val(object.slot1.pos3.pulserA);
            $('#motorB3').val(object.slot1.pos3.motorB);
            $('#valvB3').val(object.slot1.pos3.valvB);
            $('#pulserB3').val(object.slot1.pos3.pulserB);
            $('#eltmCnl3').val(object.slot1.pos3.eltmCnl);
            $('#ppl_V3').val(object.slot1.pos3.ppl_V);
            $('#ppl_C3').val(object.slot1.pos3.ppl_C);
            $('#ppl_P3').val(object.slot1.pos3.ppl_P);
            $('#rv_P3').val(object.slot1.pos3.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P3').prop('hidden', false)
              $('#pwm_b_P3').prop('hidden', false)
              $('#labelpwm3').prop('hidden', false)
              $('#pwm_a_P3').val(object.slot1.pos3.pwm_a);
              $('#pwm_b_P3').val(object.slot1.pos3.pwm_b);
            }else{
              $('#pwm_a_P3').prop('hidden', false)
              $('#pwm_b_P3').prop('hidden', true)
              $('#labelpwm3').prop('hidden', true)
              $('#pwm_a_P3').val(object.slot1.pos3.pwm_a);
            }
          }

          function printJsonSlot2(object){
            /********* SLOT 2 ***********************************/
            $('#ladoInmetro2').val(object.slot2.ladoInmetro);
            $('#logico2').val(object.slot2.logico);
            $('#dspA2').val(object.slot2.dspA);
            $('#dspB2').val(object.slot2.dspB);
            $('#eltm2').val(object.slot2.eltm);
            $('#LcdOrdem2').val(object.slot2.LcdOrdem);
          }



          function printJsonPos4(object){
            /********* POS 2 ***********************************/
            $('#bico4').val(object.slot2.pos1.bico);
            $('#mode4').val(object.slot2.pos1.mode);
            $('#motorA4').val(object.slot2.pos1.motorA);
            $('#valvA4').val(object.slot2.pos1.valvA);
            $('#pulserA4').val(object.slot2.pos1.pulserA);
            $('#eltmCnl4').val(object.slot2.pos1.eltmCnl);
            $('#ppl_V4').val(object.slot2.pos1.ppl_V);
            $('#ppl_C4').val(object.slot2.pos1.ppl_C);
            $('#ppl_P4').val(object.slot2.pos1.ppl_P);
            $('#rv_P4').val(object.slot2.pos1.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P4').prop('hidden', false)
              $('#pwm_b_P4').prop('hidden', false)
              $('#labelpwm4').prop('hidden', false)
              $('#pwm_a_P4').val(object.slot2.pos1.pwm_a);
              $('#pwm_b_P4').val(object.slot2.pos1.pwm_b);
            }else{
              $('#pwm_a_P4').prop('hidden', false)
              $('#pwm_b_P4').prop('hidden', true)
              $('#labelpwm4').prop('hidden', true)
              $('#pwm_a_P4').val(object.slot2.pos1.pwm_a);
            }
          }

          function printJsonPos4H(object){
            /********* POS 2 ***********************************/
            $('#bico4').val(object.slot2.pos1.bico);
            $('#mode4').val(object.slot2.pos1.mode);
            $('#motorA4').val(object.slot2.pos1.motorA);
            $('#valvA4').val(object.slot2.pos1.valvA);
            $('#pulserA4').val(object.slot2.pos1.pulserA);
            $('#motorB4').val(object.slot2.pos1.motorB);
            $('#valvB4').val(object.slot2.pos1.valvB);
            $('#pulserB4').val(object.slot2.pos1.pulserB);
            $('#eltmCnl4').val(object.slot2.pos1.eltmCnl);
            $('#ppl_V4').val(object.slot2.pos1.ppl_V);
            $('#ppl_C4').val(object.slot2.pos1.ppl_C);
            $('#ppl_P4').val(object.slot2.pos1.ppl_P);
            $('#rv_P4').val(object.slot2.pos1.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P4').prop('hidden', false)
              $('#pwm_b_P4').prop('hidden', false)
              $('#labelpwm4').prop('hidden', false)
              $('#pwm_a_P4').val(object.slot2.pos1.pwm_a);
              $('#pwm_b_P4').val(object.slot2.pos1.pwm_b);
            }else{
              $('#pwm_a_P4').prop('hidden', false)
              $('#pwm_b_P4').prop('hidden', true)
              $('#labelpwm4').prop('hidden', true)
              $('#pwm_a_P4').val(object.slot2.pos1.pwm_a);
            }
          }

          function printJsonPos5(object){

            /********* POS 1 ***********************************/
            $('#bico5').val(object.slot2.pos2.bico);
            $('#mode5').val(object.slot2.pos2.mode);
            $('#motorA5').val(object.slot2.pos2.motorA);
            $('#valvA5').val(object.slot2.pos2.valvA);
            $('#pulserA5').val(object.slot2.pos2.pulserA);
            $('#eltmCnl5').val(object.slot2.pos2.eltmCnl);
            $('#ppl_V5').val(object.slot2.pos2.ppl_V);
            $('#ppl_C5').val(object.slot2.pos2.ppl_C);
            $('#ppl_P5').val(object.slot2.pos2.ppl_P);
            $('#rv_P5').val(object.slot2.pos2.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P5').prop('hidden', false)
              $('#pwm_b_P5').prop('hidden', false)
              $('#labelpwm5').prop('hidden', false)
              $('#pwm_a_P5').val(object.slot2.pos2.pwm_a);
              $('#pwm_b_P5').val(object.slot2.pos2.pwm_b);
            }else{
              $('#pwm_a_P5').prop('hidden', false)
              $('#pwm_b_P5').prop('hidden', true)
              $('#labelpwm5').prop('hidden', true)
              $('#pwm_a_P5').val(object.slot2.pos2.pwm_a);
            }
          }

          function printJsonPos5H(object){

            /********* POS 1 ***********************************/
            $('#bico5').val(object.slot2.pos2.bico);
            $('#mode5').val(object.slot2.pos2.mode);
            $('#motorA5').val(object.slot2.pos2.motorA);
            $('#valvA5').val(object.slot2.pos2.valvA);
            $('#pulserA5').val(object.slot2.pos2.pulserA);
            $('#motorB5').val(object.slot2.pos2.motorB);
            $('#valvB5').val(object.slot2.pos2.valvB);
            $('#pulserB5').val(object.slot2.pos2.pulserB);
            $('#eltmCnl5').val(object.slot2.pos2.eltmCnl);
            $('#ppl_V5').val(object.slot2.pos2.ppl_V);
            $('#ppl_C5').val(object.slot2.pos2.ppl_C);
            $('#ppl_P5').val(object.slot2.pos2.ppl_P);
            $('#rv_P5').val(object.slot2.pos2.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P5').prop('hidden', false)
              $('#pwm_b_P5').prop('hidden', false)
              $('#labelpwm5').prop('hidden', false)
              $('#pwm_a_P5').val(object.slot2.pos2.pwm_a);
              $('#pwm_b_P5').val(object.slot2.pos2.pwm_b);
            }else{
              $('#pwm_a_P5').prop('hidden', false)
              $('#pwm_b_P5').prop('hidden', true)
              $('#labelpwm5').prop('hidden', true)
              $('#pwm_a_P5').val(object.slot2.pos2.pwm_a);
            }
          }

          function printJsonPos6(object){

            /********* POS 2 ***********************************/
            $('#bico6').val(object.slot2.pos3.bico);
            $('#mode6').val(object.slot2.pos3.mode);
            $('#motorA6').val(object.slot2.pos3.motorA);
            $('#valvA6').val(object.slot2.pos3.valvA);
            $('#pulserA6').val(object.slot2.pos3.pulserA);
            $('#eltmCnl6').val(object.slot2.pos3.eltmCnl);
            $('#ppl_V6').val(object.slot2.pos3.ppl_V);
            $('#ppl_C6').val(object.slot2.pos3.ppl_C);
            $('#ppl_P6').val(object.slot2.pos3.ppl_P);
            $('#rv_P6').val(object.slot2.pos3.rv_motor); 

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P6').prop('hidden', false)
              $('#pwm_b_P6').prop('hidden', false)
              $('#labelpwm6').prop('hidden', false)
              $('#pwm_a_P6').val(object.slot2.pos3.pwm_a);
              $('#pwm_b_P6').val(object.slot2.pos3.pwm_b);
            }else{
              $('#pwm_a_P6').prop('hidden', false)
              $('#pwm_b_P6').prop('hidden', true)
              $('#labelpwm6').prop('hidden', true)
              $('#pwm_a_P6').val(object.slot2.pos3.pwm_a);
            }
          }

          function printJsonPos6H(object){

            /********* POS 2 ***********************************/
            $('#bico6').val(object.slot2.pos3.bico);
            $('#mode6').val(object.slot2.pos3.mode);
            $('#motorA6').val(object.slot2.pos3.motorA);
            $('#valvA6').val(object.slot2.pos3.valvA);
            $('#pulserA6').val(object.slot2.pos3.pulserA);
            $('#motorB6').val(object.slot2.pos3.motorB);
            $('#valvB6').val(object.slot2.pos3.valvB);
            $('#pulserB6').val(object.slot2.pos3.pulserB);
            $('#eltmCnl6').val(object.slot2.pos3.eltmCnl);
            $('#ppl_V6').val(object.slot2.pos3.ppl_V);
            $('#ppl_C6').val(object.slot2.pos3.ppl_C);
            $('#ppl_P6').val(object.slot2.pos3.ppl_P);
            $('#rv_P6').val(object.slot2.pos3.rv_motor); 

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P6').prop('hidden', false)
              $('#pwm_b_P6').prop('hidden', false)
              $('#labelpwm6').prop('hidden', false)
              $('#pwm_a_P6').val(object.slot2.pos3.pwm_a);
              $('#pwm_b_P6').val(object.slot2.pos3.pwm_b);
            }else{
              $('#pwm_a_P6').prop('hidden', false)
              $('#pwm_b_P6').prop('hidden', true)
              $('#labelpwm6').prop('hidden', true)
              $('#pwm_a_P6').val(object.slot2.pos3.pwm_a);
            } 
          }

          function printJsonSlot3(object){
            /********* SLOT 3 ***********************************/
            $('#ladoInmetro3').val(object.slot3.ladoInmetro);
            $('#logico3').val(object.slot3.logico);
            $('#dspA3').val(object.slot3.dspA);
            $('#dspB3').val(object.slot3.dspB);
            $('#eltm3').val(object.slot3.eltm);
            $('#LcdOrdem3').val(object.slot3.LcdOrdem);
          }

          function printJsonPos7(object){
            /********* POS 1 ***********************************/
            $('#bico7').val(object.slot3.pos1.bico);
            $('#mode7').val(object.slot3.pos1.mode);
            $('#motorA7').val(object.slot3.pos1.motorA);
            $('#valvA7').val(object.slot3.pos1.valvA);
            $('#pulserA7').val(object.slot3.pos1.pulserA);
            $('#eltmCnl7').val(object.slot3.pos1.eltmCnl);
            $('#ppl_V7').val(object.slot3.pos1.ppl_V);
            $('#ppl_C7').val(object.slot3.pos1.ppl_C);
            $('#ppl_P7').val(object.slot3.pos1.ppl_P);
            $('#rv_P7').val(object.slot3.pos1.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P7').prop('hidden', false)
              $('#pwm_b_P7').prop('hidden', false)
              $('#labelpwm7').prop('hidden', false)
              $('#pwm_a_P7').val(object.slot3.pos1.pwm_a);
              $('#pwm_b_P7').val(object.slot3.pos1.pwm_b);
            }else{
              $('#pwm_a_P7').prop('hidden', false)
              $('#pwm_b_P7').prop('hidden', true)
              $('#labelpwm7').prop('hidden', true)
              $('#pwm_a_P7').val(object.slot3.pos1.pwm_a);            
            }
          }

          function printJsonPos7H(object){
            /********* POS 1 ***********************************/
            $('#bico7').val(object.slot3.pos1.bico);
            $('#mode7').val(object.slot3.pos1.mode);
            $('#motorA7').val(object.slot3.pos1.motorA);
            $('#valvA7').val(object.slot3.pos1.valvA);
            $('#pulserA7').val(object.slot3.pos1.pulserA);
            $('#motorB7').val(object.slot3.pos1.motorB);
            $('#valvB7').val(object.slot3.pos1.valvB);
            $('#pulserB7').val(object.slot3.pos1.pulserB);
            $('#eltmCnl7').val(object.slot3.pos1.eltmCnl);
            $('#ppl_V7').val(object.slot3.pos1.ppl_V);
            $('#ppl_C7').val(object.slot3.pos1.ppl_C);
            $('#ppl_P7').val(object.slot3.pos1.ppl_P);
            $('#rv_P7').val(object.slot3.pos1.rv_motor);

            if(object.slot1.pos1.mode.includes("H")){
              $('#pwm_a_P7').prop('hidden', false)
              $('#pwm_b_P7').prop('hidden', false)
              $('#labelpwm7').prop('hidden', false)
              $('#pwm_a_P7').val(object.slot3.pos1.pwm_a);
              $('#pwm_b_P7').val(object.slot3.pos1.pwm_b);
            }else{
              $('#pwm_a_P7').prop('hidden', false)
              $('#pwm_b_P7').prop('hidden', true)
              $('#labelpwm7').prop('hidden', true)
              $('#pwm_a_P7').val(object.slot3.pos1.pwm_a);
            }
          }

          function printJsonPos8(object){
           /********* POS 2 ***********************************/
           $('#bico8').val(object.slot3.pos2.bico);
           $('#mode8').val(object.slot3.pos2.mode);
           $('#motorA8').val(object.slot3.pos2.motorA);
           $('#valvA8').val(object.slot3.pos2.valvA);
           $('#pulserA8').val(object.slot3.pos2.pulserA);
           $('#eltmCnl8').val(object.slot3.pos2.eltmCnl);
           $('#ppl_V8').val(object.slot3.pos2.ppl_V);
           $('#ppl_C8').val(object.slot3.pos2.ppl_C);
           $('#ppl_P8').val(object.slot3.pos2.ppl_P);
           $('#rv_P8').val(object.slot3.pos2.rv_motor);

           if(object.slot1.pos1.mode.includes("H")){
            $('#pwm_a_P8').prop('hidden', false)
            $('#pwm_b_P8').prop('hidden', false)
            $('#labelpwm8').prop('hidden', false)
            $('#pwm_a_P8').val(object.slot3.pos2.pwm_a);
            $('#pwm_b_P8').val(object.slot3.pos2.pwm_b);
          }else{
            $('#pwm_a_P8').prop('hidden', false)
            $('#pwm_b_P8').prop('hidden', true)
            $('#labelpwm8').prop('hidden', true)
            $('#pwm_a_P8').val(object.slot3.pos2.pwm_a);
          }
        }

        function printJsonPos8H(object){
         /********* POS 2 ***********************************/
         $('#bico8').val(object.slot3.pos2.bico);
         $('#mode8').val(object.slot3.pos2.mode);
         $('#motorA8').val(object.slot3.pos2.motorA);
         $('#valvA8').val(object.slot3.pos2.valvA);
         $('#pulserA8').val(object.slot3.pos2.pulserA);
         $('#motorB8').val(object.slot3.pos2.motorB);
         $('#valvB8').val(object.slot3.pos2.valvB);
         $('#pulserB8').val(object.slot3.pos2.pulserB);
         $('#eltmCnl8').val(object.slot3.pos2.eltmCnl);
         $('#ppl_V8').val(object.slot3.pos2.ppl_V);
         $('#ppl_C8').val(object.slot3.pos2.ppl_C);
         $('#ppl_P8').val(object.slot3.pos2.ppl_P);
         $('#rv_P8').val(object.slot3.pos2.rv_motor);

         if(object.slot1.pos1.mode.includes("H")){
          $('#pwm_a_P8').prop('hidden', false)
          $('#pwm_b_P8').prop('hidden', false)
          $('#labelpwm8').prop('hidden', false)
          $('#pwm_a_P8').val(object.slot3.pos2.pwm_a);
          $('#pwm_b_P8').val(object.slot3.pos2.pwm_b);
        }else{
          $('#pwm_a_P8').prop('hidden', false)
          $('#pwm_b_P8').prop('hidden', true)
          $('#labelpwm8').prop('hidden', true)
          $('#pwm_a_P8').val(object.slot3.pos2.pwm_a);
        }
      }


      function printJsonPos9(object){
       /********* POS 2 ***********************************/
       $('#bico9').val(object.slot3.pos3.bico);
       $('#mode9').val(object.slot3.pos3.mode);
       $('#motorA9').val(object.slot3.pos3.motorA);
       $('#valvA9').val(object.slot3.pos3.valvA);
       $('#pulserA9').val(object.slot3.pos3.pulserA);
       $('#eltmCnl9').val(object.slot3.pos3.eltmCnl);
       $('#ppl_V9').val(object.slot3.pos3.ppl_V);
       $('#ppl_C9').val(object.slot3.pos3.ppl_C);
       $('#ppl_P9').val(object.slot3.pos3.ppl_P);
       $('#rv_P9').val(object.slot3.pos3.rv_motor);

       if(object.slot1.pos1.mode.includes("H")){
        $('#pwm_a_P9').prop('hidden', false)
        $('#pwm_b_P9').prop('hidden', false)
        $('#labelpwm9').prop('hidden', false)
        $('#pwm_a_P9').val(object.slot3.pos3.pwm_a);
        $('#pwm_b_P9').val(object.slot3.pos3.pwm_b);
      }else{
        $('#pwm_a_P9').prop('hidden', false)
        $('#pwm_b_P9').prop('hidden', true)
        $('#labelpwm9').prop('hidden', true)
        $('#pwm_a_P9').val(object.slot3.pos3.pwm_a);
      }
    }

    function printJsonPos9H(object){
     /********* POS 2 ***********************************/
     $('#bico9').val(object.slot3.pos3.bico);
     $('#mode9').val(object.slot3.pos3.mode);
     $('#motorA9').val(object.slot3.pos3.motorA);
     $('#valvA9').val(object.slot3.pos3.valvA);
     $('#pulserA9').val(object.slot3.pos3.pulserA);
     $('#motorB9').val(object.slot3.pos3.motorB);
     $('#valvB9').val(object.slot3.pos3.valvB);
     $('#pulserB9').val(object.slot3.pos3.pulserB);
     $('#eltmCnl9').val(object.slot3.pos3.eltmCnl);
     $('#ppl_V9').val(object.slot3.pos3.ppl_V);
     $('#ppl_C9').val(object.slot3.pos3.ppl_C);
     $('#ppl_P9').val(object.slot3.pos3.ppl_P);
     $('#rv_P9').val(object.slot3.pos3.rv_motor);

     if(object.slot1.pos1.mode.includes("H")){
      $('#pwm_a_P9').prop('hidden', false)
      $('#pwm_b_P9').prop('hidden', false)
      $('#labelpwm9').prop('hidden', false)
      $('#pwm_a_P9').val(object.slot3.pos3.pwm_a);
      $('#pwm_b_P9').val(object.slot3.pos3.pwm_b);
    }else{
      $('#pwm_a_P9').prop('hidden', false)
      $('#pwm_b_P9').prop('hidden', true)
      $('#labelpwm9').prop('hidden', true)
      $('#pwm_a_P9').val(object.slot3.pos3.pwm_a);
    }
  }

  function printJsonSlot4(object){
    /********* SLOT 4 ***********************************/
    $('#ladoInmetro4').val(object.slot4.ladoInmetro);
    $('#logico4').val(object.slot4.logico);
    $('#dspA4').val(object.slot4.dspA);
    $('#dspB4').val(object.slot4.dspB);
    $('#eltm4').val(object.slot4.eltm);
    $('#LcdOrdem4').val(object.slot4.LcdOrdem);

  }


  function printJsonPos10(object){
   /********* POS 2 ***********************************/
   $('#bico10').val(object.slot4.pos1.bico);
   $('#mode10').val(object.slot4.pos1.mode);
   $('#motorA10').val(object.slot4.pos1.motorA);
   $('#valvA10').val(object.slot4.pos1.valvA);
   $('#pulserA10').val(object.slot4.pos1.pulserA);
   $('#eltmCnl10').val(object.slot4.pos1.eltmCnl);
   $('#ppl_V10').val(object.slot4.pos1.ppl_V);
   $('#ppl_C10').val(object.slot4.pos1.ppl_C);
   $('#ppl_P10').val(object.slot4.pos1.ppl_P);
   $('#rv_P10').val(object.slot4.pos1.rv_motor);

   if(object.slot1.pos1.mode.includes("H")){
    $('#pwm_a_P10').prop('hidden', false)
    $('#pwm_b_P10').prop('hidden', false)
    $('#labelpwm10').prop('hidden', false)
    $('#pwm_a_P10').val(object.slot4.pos1.pwm_a);
    $('#pwm_b_P10').val(object.slot4.pos1.pwm_b);
  }else{
    $('#pwm_a_P10').prop('hidden', false)
    $('#pwm_b_P10').prop('hidden', true)
    $('#labelpwm10').prop('hidden', true)
    $('#pwm_a_P10').val(object.slot4.pos1.pwm_a);
  }
}

function printJsonPos10H(object){
 /********* POS 2 ***********************************/
 $('#bico10').val(object.slot4.pos1.bico);
 $('#mode10').val(object.slot4.pos1.mode);
 $('#motorA10').val(object.slot4.pos1.motorA);
 $('#valvA10').val(object.slot4.pos1.valvA);
 $('#pulserA10').val(object.slot4.pos1.pulserA);
 $('#motorB10').val(object.slot4.pos1.motorB);
 $('#valvB10').val(object.slot4.pos1.valvB);
 $('#pulserB10').val(object.slot4.pos1.pulserB);
 $('#eltmCnl10').val(object.slot4.pos1.eltmCnl);
 $('#ppl_V10').val(object.slot4.pos1.ppl_V);
 $('#ppl_C10').val(object.slot4.pos1.ppl_C);
 $('#ppl_P10').val(object.slot4.pos1.ppl_P);
 $('#rv_P10').val(object.slot4.pos1.rv_motor);

 if(object.slot1.pos1.mode.includes("H")){
  $('#pwm_a_P10').prop('hidden', false)
  $('#pwm_b_P10').prop('hidden', false)
  $('#labelpwm10').prop('hidden', false)
  $('#pwm_a_P10').val(object.slot4.pos1.pwm_a);
  $('#pwm_b_P10').val(object.slot4.pos1.pwm_b);
}else{
  $('#pwm_a_P10').prop('hidden', false)
  $('#pwm_b_P10').prop('hidden', true)
  $('#labelpwm10').prop('hidden', true)
  $('#pwm_a_P10').val(object.slot4.pos1.pwm_a);
}
}

function printJsonPos11(object){
 /********* POS 2 ***********************************/
 $('#bico11').val(object.slot4.pos2.bico);
 $('#mode11').val(object.slot4.pos2.mode);
 $('#motorA11').val(object.slot4.pos2.motorA);
 $('#valvA11').val(object.slot4.pos2.valvA);
 $('#pulserA11').val(object.slot4.pos2.pulserA);
 $('#eltmCnl11').val(object.slot4.pos2.eltmCnl);
 $('#ppl_V11').val(object.slot4.pos2.ppl_V);
 $('#ppl_C11').val(object.slot4.pos2.ppl_C);
 $('#ppl_P11').val(object.slot4.pos2.ppl_P);
 $('#rv_P11').val(object.slot4.pos2.rv_motor);

 if(object.slot1.pos1.mode.includes("H")){
  $('#pwm_a_P11').prop('hidden', false)
  $('#pwm_b_P11').prop('hidden', false)
  $('#labelpwm11').prop('hidden', false)
  $('#pwm_a_P11').val(object.slot4.pos2.pwm_a);
  $('#pwm_b_P11').val(object.slot4.pos2.pwm_b);
}else{
  $('#pwm_a_P11').prop('hidden', false)
  $('#pwm_b_P11').prop('hidden', true)
  $('#labelpwm11').prop('hidden', true)
  $('#pwm_a_P11').val(object.slot4.pos2.pwm_a);
}
}

function printJsonPos11H(object){
 /********* POS 2 ***********************************/
 $('#bico11').val(object.slot4.pos2.bico);
 $('#mode11').val(object.slot4.pos2.mode);
 $('#motorA11').val(object.slot4.pos2.motorA);
 $('#valvA11').val(object.slot4.pos2.valvA);
 $('#pulserA11').val(object.slot4.pos2.pulserA);
 $('#motorB11').val(object.slot4.pos2.motorB);
 $('#valvB11').val(object.slot4.pos2.valvB);
 $('#pulserB11').val(object.slot4.pos2.pulserB);
 $('#eltmCnl11').val(object.slot4.pos2.eltmCnl);
 $('#ppl_V11').val(object.slot4.pos2.ppl_V);
 $('#ppl_C11').val(object.slot4.pos2.ppl_C);
 $('#ppl_P11').val(object.slot4.pos2.ppl_P);
 $('#rv_P11').val(object.slot4.pos2.rv_motor);

 if(object.slot1.pos1.mode.includes("H")){
  $('#pwm_a_P11').prop('hidden', false)
  $('#pwm_b_P11').prop('hidden', false)
  $('#labelpwm11').prop('hidden', false)
  $('#pwm_a_P11').val(object.slot4.pos2.pwm_a);
  $('#pwm_b_P11').val(object.slot4.pos2.pwm_b);
}else{
  $('#pwm_a_P11').prop('hidden', false)
  $('#pwm_b_P11').prop('hidden', true)
  $('#labelpwm11').prop('hidden', true)
  $('#pwm_a_P11').val(object.slot4.pos2.pwm_a);
}
}

function printJsonPos12(object){
 /********* POS 2 ***********************************/
 $('#bico12').val(object.slot4.pos3.bico);
 $('#mode12').val(object.slot4.pos3.mode);
 $('#motorA12').val(object.slot4.pos3.motorA);
 $('#valvA12').val(object.slot4.pos3.valvA);
 $('#pulserA12').val(object.slot4.pos3.pulserA);
 $('#eltmCnl12').val(object.slot4.pos3.eltmCnl);
 $('#ppl_V12').val(object.slot4.pos3.ppl_V);
 $('#ppl_C12').val(object.slot4.pos3.ppl_C);
 $('#ppl_P12').val(object.slot4.pos3.ppl_P);
 $('#rv_P12').val(object.slot4.pos3.rv_motor);

 if(object.slot1.pos1.mode.includes("H")){
  $('#pwm_a_P12').prop('hidden', false)
  $('#pwm_b_P12').prop('hidden', false)
  $('#labelpwm12').prop('hidden', false)
  $('#pwm_a_P12').val(object.slot4.pos3.pwm_a);
  $('#pwm_b_P12').val(object.slot4.pos3.pwm_b);
}else{
  $('#pwm_a_P12').prop('hidden', false)
  $('#pwm_b_P12').prop('hidden', true)
  $('#labelpwm12').prop('hidden', true)
  $('#pwm_a_P12').val(object.slot4.pos3.pwm_a);
}
}

function printJsonPos12H(object){
 /********* POS 2 ***********************************/
 $('#bico12').val(object.slot4.pos3.bico);
 $('#mode12').val(object.slot4.pos3.mode);
 $('#motorA12').val(object.slot4.pos3.motorA);
 $('#valvA12').val(object.slot4.pos3.valvA);
 $('#pulserA12').val(object.slot4.pos3.pulserA);
 $('#motorB12').val(object.slot4.pos3.motorB);
 $('#valvB12').val(object.slot4.pos3.valvB);
 $('#pulserB12').val(object.slot4.pos3.pulserB);
 $('#eltmCnl12').val(object.slot4.pos3.eltmCnl);
 $('#ppl_V12').val(object.slot4.pos3.ppl_V);
 $('#ppl_C12').val(object.slot4.pos3.ppl_C);
 $('#ppl_P12').val(object.slot4.pos3.ppl_P);
 $('#rv_P12').val(object.slot4.pos3.rv_motor);

 if(object.slot1.pos1.mode.includes("H")){
  $('#pwm_a_P12').prop('hidden', false)
  $('#pwm_b_P12').prop('hidden', false)
  $('#labelpwm12').prop('hidden', false)
  $('#pwm_a_P12').val(object.slot4.pos3.pwm_a);
  $('#pwm_b_P12').val(object.slot4.pos3.pwm_b);
}else{
  $('#pwm_a_P12').prop('hidden', false)
  $('#pwm_b_P12').prop('hidden', true)
  $('#labelpwm12').prop('hidden', true)
  $('#pwm_a_P12').val(object.slot4.pos3.pwm_a);
}
}

function insert(str, index, value) {
  return str.substr(0, index) + value + str.substr(index);
}

});