<?php
	header('Content-Type: text/html; charset=UTF-8');

	require_once('PHPMailer/src/Exception.php');
	require_once('PHPMailer/src/PHPMailer.php');
	require_once('PHPMailer/src/SMTP.php');				
	
	class enviaEmail{		
	
		private $destinatario;
		private $titulo;
		private $conteudo;		
		
		public function sendEmail($dados){			
			
			//$mail = new PHPMailer();
			$mail = new PHPMailer\PHPMailer\PHPMailer(true);
			try {
				//Server settings
				//$mail->SMTPDebug = 2;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP
				$mail->Host = 'smtp.emailarray.com';  				  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = 'mateus@companytec.com.br';                 // SMTP username
				$mail->Password = 'webmaster2017';                           // SMTP password
				//$mail->SMTPSecure = '';                            // Enable TLS encryption, `ssl` also accepted
				$mail->Port = 587;                                    // TCP port to connect to
				$mail->CharSet = 'UTF-8';
				
				//Recipients
				$mail->setFrom('webmaster@wertco.com.br', 'WERTCO');
				$mail->addAddress($dados['destinatario']);     			// Add a recipient
				$mail->addAddress('webmaster@wertco.com.br');               // Name is optional
				//$mail->addReplyTo('info@example.com', 'Information');
				//$mail->addCC('cc@example.com');
				//$mail->addBCC('bcc@example.com');

				//Attachments
				if(isset($dados['anexo'])){
					$mail->addAttachment($dados['anexo']);         // Add attachments
				}
				//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
				
				$mensagem = "
				<html>
					<head>
						<title>".$dados['titulo']."</title>
					</head>
				<body background='#ffcc00'> 						
					<img src='http://www.wertco.com.br/site_novo/img/styleswitcher/logos/logos-dark/wertcofundoescuro.png' />
					<h2>".$dados['titulo']."</h2>
					<div>
						".$dados['conteudo']."
					</div>
					</body>
					</html>";
				
				//Content
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = $dados['titulo'];
				$mail->Body    = $mensagem;
				//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

				$mail->send();
				$retorno = true;
			} catch (Exception $e) {				
				//echo 'Mailer Error: ' . $mail->ErrorInfo;
				$retorno = false;
			}
			
			return $retorno;						
		}
		
	}
	
?>