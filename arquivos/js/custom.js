(function($) {
	
    "use strict";
		
	$(window).on("load", function() {
		
		// PRELOADER
		var d = new Date();
		var n = d.getTime();;
		(function($, window, document, undefined) {
			var s = document.body || document.documentElement,
				s = s.style,
				prefixTransition = "";

			if (s.WebkitTransition === "") prefixTransition = "-webkit-";
			if (s.MozTransition === "") prefixTransition = "-moz-";
			if (s.OTransition === "") prefixTransition = "-o-";

			$.fn.extend({
				onCSSTransitionEnd: function(callback) {
					var $this = $(this).eq(0);
					$this.one("webkitTransitionEnd mozTransitionEnd oTransitionEnd otransitionend transitionend", callback);
					if ((prefixTransition == "" && !("transition" in s)) || $this.css(prefixTransition + "transition-duration") == "0s") {
						callback();
					} else {
						var arr_1 = $this.css(prefixTransition + "transition-duration").split(", ");
						var arr_2 = $this.css(prefixTransition + "transition-delay").split(", ");
						length = 0;
						for (var i = 0; i < arr_1.length; i++) {
							length = parseFloat(arr_1[i]) + parseFloat(arr_2[i]) > length ? parseFloat(arr_1[i]) + parseFloat(arr_2[i]) : length;
						};
						var d = new Date();
						var l = d.getTime();
						if ((l - n) > length * 1000) {
							callback();
						}
					}
					return this;
				}
			});
		})(jQuery, window, document);
		$("#preloader").addClass("loading");
		$("#preloader #loader").onCSSTransitionEnd(function() {
			$("#preloader").addClass("ended");
		});
		
		//FILTERABLE PORTFOLIO
		$(".simplefilter li").on("click", function() {
			$(".simplefilter li").removeClass("active");
			$(this).addClass("active");
		});
		var options = {
			animationDuration: 0.6,
			filter: "all",
			callbacks: {
				onFilteringStart: function() {},
				onFilteringEnd: function() {}
			},
			delay: 0,
			delayMode: "alternate",
			easing: "ease-out",
			layout: "sameSize",
			selector: ".filtr-container",
			setupControls: true
		}
		/*var filterizd = $(".filtr-container").filterizr(options);
		filterizd.filterizr("setOptions", options);*/

	});	
			
	$(document).ready(function () {
			
		$("#bombas_ico").bind('click', function(){
			$('.bombas_ico').attr('src','img/bombaamarelaM.png');	
		});		
		$(".bombas_menu").bind('click', function(){
			$('.bombas_ico').attr('src','img/bombabranca.png');
		});
			
		// REMOVE # FROM URL
		$("a[href='#']").on("click", (function(e) {
			e.preventDefault();
		}));
		
		// LOGOS SLIDER
		$("#bxslider").bxSlider({
			minSlides: 1,
			maxSlides: 6,
			slideWidth: 200,
			slideMargin: 30,
			ticker: true,
			speed: 30000
		});	

		// INITIALIZING MAGNIFIC POPUP
		jQuery(".magnific-popup-gallery").each(function() {
			magnific_popup_init(jQuery(this))
		});
		
		
			
		jQuery(".mfp-youtube").magnificPopup({
			type: "iframe",
			mainClass: "mfp-fade",
			removalDelay: 0,
			preloader: false,
			fixedContentPos: false,
			iframe: {
				patterns: {
					youtube: {
						src: "https://youtube.com/embed/%id%?autoplay=1&rel=0"
					},
				}
			}
		});
		jQuery(".mfp-vimeo").magnificPopup({
			type: "iframe",
			mainClass: "mfp-fade",
			removalDelay: 0,
			preloader: false,
			fixedContentPos: false,
			iframe: {
				patterns: {
					vimeo: {
						src: "https://player.vimeo.com/video/%id%?autoplay=1"
					}
				}
			}
		});
		function magnific_popup_init(item) {
			item.magnificPopup({
				delegate: "a[data-gal^='magnific-pop-up']",
				type: "image",
				removalDelay: 500,
				mainClass: "mfp-zoom-in",
				fixedContentPos: false,
				callbacks: {
					beforeOpen: function() {
						// just a hack that adds mfp-anim class to markup 
						this.st.image.markup = this.st.image.markup.replace("mfp-figure", "mfp-figure mfp-with-anim");
					}
				},
				gallery: {
					enabled: true
				}
			});
					
		}
		
		// BACK TO TOP
		$("#back-top a").on("click", function() {
			$("body,html").stop(false, false).animate({
				scrollTop: 0
			}, 800);
			return false;
		});

		// TESTIMONIAL CAROUSEL
		$("#quote-carousel").carousel({
			wrap: true,
			interval: 6000
		});
		
		// TESTIMONIAL CAROUSEL TOUCH OPTIMIZED
		var cr = $("#quote-carousel");
		cr.on("touchstart", function(event){
			var xClick = event.originalEvent.touches[0].pageX;
			$(this).one("touchmove", function(event){
				var xMove = event.originalEvent.touches[0].pageX;
				if( Math.floor(xClick - xMove) > 5 ){
					cr.carousel('next');
				}
				else if( Math.floor(xClick - xMove) < -5 ){
					cr.carousel('prev');
				}
			});
			cr.on("touchend", function(){
					$(this).off("touchmove");
			});
		});

		// BUTTON ICON ANIMATION
		var btn_hover = "";
		$(".custom-button").each(function() {
			var btn_text = $(this).text();
			$(this).addClass(btn_hover).empty().append("<span data-hover='" + btn_text + "'>" + btn_text + "</span>");
		});

		// SINGLE PAGE SCROLL
		$("#singlepage-nav").singlePageNav({
			offset: 0,
			filter: ":not(.nav-external)",
			updateHash: 0,
			currentClass: "current",
			easing: "swing",
			speed: 750
		});
		
		// LOCAL SCROLL
		$(".scroll-to-target[href^='#']").on("click", function(scroll_to_target) {
			scroll_to_target.preventDefault();
			var a = this.hash,
				i = $(a);
			$("html, body").stop().animate({
				scrollTop: i.offset().top
			}, 900, "swing", function() {})
		})
		
		// HAMBURGER ICON ANIMATION
		$(".link-menu").on("click", function(){
			if( $(this).hasClass('nav-link') ){

				window.open('area-restrita.html');
			} 
			$("#icon-toggler").removeClass("open");
		});

		$("#icon-toggler").on("click", function(){
			$(this).toggleClass("open");
		});
		
		
		
		/********************************************************************************
		*********************************************************************************
		**************************	Início Cadastro de Newletter	*********************
		*********************************************************************************
		*********************************************************************************/
		
		$("#enviar_newsletter").bind('click', function(){
			var email = $("#email_newsletter").val();
			
			if(email != "" && valida_email(email) == true){
				$.ajax({
					method: "POST",
					url: "/site_novo/php/cadastraNewsletter.php",
					async: true,
					data: { email	:	email }
					}).success(function( data ) {
						var dados = $.parseJSON(data);		
						if(dados.retorno == "sucesso"){
							$("#output_message_newsletter").text('Obrigado pelo interesse. Em breve você receberá as novidades da Wertco!');
							$("#output_message_newsletter").css('color','#6cd015');
						}else if(dados.retorno == "E-mail já cadastrado"){
							$("#output_message_newsletter").text(dados.retorno);
							$("#output_message_newsletter").css('color','#f14343');
						}else{
							$("#output_message_newsletter").text("Erro, entre em contato com a Wertco");
							$("#output_message_newsletter").css('color','#6cd015');
						}
					});	
			}else{
				$("#output_message_newsletter").text('Insira um e-mail corretamente');
				$("#output_message_newsletter").css('color','#f14343');
			}
		});
				
		/********************************************************************************
		*********************************************************************************
		**************************	Fim Cadastro de Newletter	*************************
		*********************************************************************************
		*********************************************************************************/
		
		/********************************************************************************
		*********************************************************************************
		**************************	Início Fale Conosco		*****************************
		*********************************************************************************
		*********************************************************************************/
		
		$("#form-submit").bind('click', function(){
			var email = $("#email_contact").val();
			var firstname = $("#firstname").val();
			var lastname = $("#lastname").val();
			var message = $("#message").val();
			
			
			
			if((email != "" && valida_email(email) == true) && message != "" ){ 
				$(this).after('<img src="img/loading.gif" id="loading" />');
				$(this).unbind('click');
				$.ajax({
					method: "POST",
					url: "/site_novo/php/enviaContato.php",
					async: true,
					data: { email	 	:	email,
							firstname 	: 	firstname,
							lastname	:	lastname,
							message		:	message}
					}).success(function( data ) {
						var dados = $.parseJSON(data);		 
						if(dados.retorno == "sucesso"){
							$("#contato").text('Obrigado pelo interesse. Em breve você receberá as novidades da Wertco!');
							$("#contato").css('color','#6cd015');
						}else if(dados.retorno == "Mensagem já enviada."){
							$("#contato").text(dados.retorno);
							$("#contato").css('color','#f14343');
						}else{
							$("#contato").text("Erro, entre em contato com a Wertco");
							$("#contato").css('color','#6cd015');
						}
						
						$("#loading").remove();
						$(this).bind('click');
					});	
			}else{
				$("#contato").text('Preencha todos os campos corretamente');
				$("#contato").css('color','#f14343');
			}
		});
				
		/********************************************************************************
		*********************************************************************************
		**************************	Fim Cadastro de Newletter	*************************
		*********************************************************************************
		*********************************************************************************/
		
		
		/********************************************************************************
		*********************************************************************************
		**************************	INÍCIO BOMBA DIFERENCIAIS   *************************
		*********************************************************************************
		*********************************************************************************/
		
		$(".portico").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				$(this).next('i').attr('class','fa fa-minus');
				$("#altera_img").attr('src','img/bombas/portico.png'); 
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		$(".bicos").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/bicos.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		$(".base").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/baserobusta.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		$(".mostrador").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/painelfrontal.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		$(".multimidia").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/multimidia.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		$(".indicadores").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/indicadordeprodutos.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		$(".teclado").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/teclado.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});		
		
		$(".display").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/mostrador.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		$(".smc").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/caracteristica2.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		$(".revestimentos").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/caracteristica2.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		$(".eletronica").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/painelfrontalcompanyteccinza.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		$(".conectividade").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/painelfrontalblueethercinza.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		$(".antifraude").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/baserobustaokpulser.png');
			}else{
				 $('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		$(".totalizador").bind('click', function(){
			$('#altera_img').show();
			var expanded = $(this).attr('aria-expanded');			
			if( expanded === 'false' ){
				
				$("#altera_img").attr('src','img/bombas/totalizador.png');
			}else{
				$('#altera_img').hide();	
				$("#altera_img").attr('src','');
			}
		});
		
		
		
		/*$("#portico").bind('click', function(){
			$("#altera_img").attr('src','portico.png');
		});*/
		
		/********************************************************************************
		*********************************************************************************
		**************************	FIM BOMBA DIFERENCIAIS   ****************************
		*********************************************************************************
		*********************************************************************************/
	});
	
	function valida_email(email)
	{
		var x=email;
		var atpos=x.indexOf("@");
		var dotpos=x.lastIndexOf(".");
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
		{		
		return false;
		}
		return true;
	}
	$('[data-toggle="tooltip"]').tooltip(); 

	$(window).on("scroll", function() {
		
		// FIX HEADER ON SCROLL
		var e = $(window).scrollTop();
		$(window).height();
		e > 1 ? $(".header").addClass("header-fixed") : $(".header").removeClass("header-fixed");
		
		// BACK TO TOP
		if ($(this).scrollTop() > 100) {
			$("#back-top").fadeIn();
		} else {
			$("#back-top").fadeOut();
		}
		
	});
	
	$("#mostra_desc_portico").bind('click', function(){
		var ativo = $(this).attr('ativo');

		if( ativo == 0 ){
			
			$('#descricao_portico').fadeIn('slow');
			$(this).attr('src','img/bombas/desc_portico-.png');
			$(this).attr('ativo',1);
			/***************** ***** *****************/
			$('#mostra_desc_acabamentos').attr('src','img/bombas/desc_acabamentos+.png');
			$('#mostra_desc_acabamentos').attr('ativo',0);
			$('#descricao_acabamentos').fadeOut('slow');
			/***************** ***** *****************/
			$('#descricao_midia').fadeOut('slow');
			$('#mostra_desc_acabamentos').attr('src','img/bombas/desc_midia+.png');
			$('#mostra_desc_acabamentos').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_bico').fadeOut('slow');
			$('#mostra_desc_bico').attr('src','img/bombas/desc_bico+.png');
			$('#mostra_desc_bico').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_teclado').fadeOut('slow');
			$('#mostra_desc_teclado').attr('src','img/bombas/desc_teclado+.png');
			$('#mostra_desc_teclado').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_mostrador').fadeOut('slow');
			$('#mostra_desc_mostrador').attr('src','img/bombas/desc_mostrador+.png');
			$('#mostra_desc_mostrador').attr('ativo',0);
			/***************** ***** *****************/			
		}else{

			$('#descricao_portico').fadeOut('slow');
			$(this).attr('src','img/bombas/desc_portico+.png');
			$(this).attr('ativo',0);
		}	
	});

	$("#mostra_desc_mostrador").bind('click', function(){
		var ativo = $(this).attr('ativo');
		if( ativo == 0 ){
			$('#descricao_mostrador').fadeIn('slow');
			$(this).attr('src','img/bombas/desc_mostrador-.png');
			$(this).attr('ativo',1);
			/***************** ***** *****************/
			$('#mostra_desc_acabamentos').attr('src','img/bombas/desc_acabamentos+.png');
			$('#mostra_desc_acabamentos').attr('ativo',0);
			$('#descricao_acabamentos').fadeOut('slow');
			/***************** ***** *****************/
			$('#descricao_midia').fadeOut('slow');
			$('#mostra_desc_acabamentos').attr('src','img/bombas/desc_midia+.png');
			$('#mostra_desc_acabamentos').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_bico').fadeOut('slow');
			$('#mostra_desc_bico').attr('src','img/bombas/desc_bico+.png');
			$('#mostra_desc_bico').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_teclado').fadeOut('slow');
			$('#mostra_desc_teclado').attr('src','img/bombas/desc_teclado+.png');
			$('#mostra_desc_teclado').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_portico').fadeOut('slow');
			$('#mostra_desc_portico').attr('src','img/bombas/desc_portico+.png');
			$('#mostra_desc_portico').attr('ativo',0);
			/***************** ***** *****************/
		}else{
			$('#descricao_mostrador').fadeOut('slow');
			$(this).attr('src','img/bombas/desc_mostrador+.png');
			$(this).attr('ativo',0);
		}	
	});

	$("#mostra_desc_teclado").bind('click', function(){
		var ativo = $(this).attr('ativo');
		if( ativo == 0 ){
			$('#descricao_teclado').fadeIn('slow');
			$(this).attr('src','img/bombas/desc_teclado-.png');
			$(this).attr('ativo',1);
			/***************** ***** *****************/
			$('#mostra_desc_acabamentos').attr('src','img/bombas/desc_acabamentos+.png');
			$('#mostra_desc_acabamentos').attr('ativo',0);
			$('#descricao_acabamentos').fadeOut('slow');
			/***************** ***** *****************/
			$('#descricao_midia').fadeOut('slow');
			$('#mostra_desc_acabamentos').attr('src','img/bombas/desc_midia+.png');
			$('#mostra_desc_acabamentos').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_bico').fadeOut('slow');
			$('#mostra_desc_bico').attr('src','img/bombas/desc_bico+.png');
			$('#mostra_desc_bico').attr('ativo',0);			
			/***************** ***** *****************/
			$('#descricao_mostrador').fadeOut('slow');
			$('#mostra_desc_mostrador').attr('src','img/bombas/desc_mostrador+.png');
			$('#mostra_desc_mostrador').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_portico').fadeOut('slow');
			$('#mostra_desc_portico').attr('src','img/bombas/desc_portico+.png');
			$('#mostra_desc_portico').attr('ativo',0);
			/***************** ***** *****************/
		}else{
			$('#descricao_teclado').fadeOut('slow');
			$(this).attr('src','img/bombas/desc_teclado+.png');
			$(this).attr('ativo',0);
		}	
	});

	$("#mostra_desc_bico").bind('click', function(){
		var ativo = $(this).attr('ativo');
		if( ativo == 0 ){
			$('#descricao_bico').fadeIn('slow');
			$(this).attr('src','img/bombas/desc_bico-.png');
			$(this).attr('ativo',1);
			/***************** ***** *****************/
			$('#mostra_desc_acabamentos').attr('src','img/bombas/desc_acabamentos+.png');
			$('#mostra_desc_acabamentos').attr('ativo',0);
			$('#descricao_acabamentos').fadeOut('slow');
			/***************** ***** *****************/
			$('#descricao_midia').fadeOut('slow');
			$('#mostra_desc_acabamentos').attr('src','img/bombas/desc_midia+.png');
			$('#mostra_desc_acabamentos').attr('ativo',0);			
			/***************** ***** *****************/
			$('#descricao_teclado').fadeOut('slow');
			$('#mostra_desc_teclado').attr('src','img/bombas/desc_teclado+.png');
			$('#mostra_desc_teclado').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_mostrador').fadeOut('slow');
			$('#mostra_desc_mostrador').attr('src','img/bombas/desc_mostrador+.png');
			$('#mostra_desc_mostrador').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_portico').fadeOut('slow');
			$('#mostra_desc_portico').attr('src','img/bombas/desc_portico+.png');
			$('#mostra_desc_portico').attr('ativo',0);
			/***************** ***** *****************/
		}else{
			$('#descricao_bico').fadeOut('slow');
			$(this).attr('src','img/bombas/desc_bico+.png');
			$(this).attr('ativo',0);
		}	
	});

	$("#mostra_desc_midia").bind('click', function(){
		var ativo = $(this).attr('ativo');
		if( ativo == 0 ){
			$('#descricao_midia').fadeIn('slow');
			$(this).attr('src','img/bombas/desc_midia-.png');
			$(this).attr('ativo',1);
			/***************** ***** *****************/
			$('#mostra_desc_acabamentos').attr('src','img/bombas/desc_acabamentos+.png');
			$('#mostra_desc_acabamentos').attr('ativo',0);
			$('#descricao_acabamentos').fadeOut('slow');			
			/***************** ***** *****************/
			$('#descricao_bico').fadeOut('slow');
			$('#mostra_desc_bico').attr('src','img/bombas/desc_bico+.png');
			$('#mostra_desc_bico').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_teclado').fadeOut('slow');
			$('#mostra_desc_teclado').attr('src','img/bombas/desc_teclado+.png');
			$('#mostra_desc_teclado').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_mostrador').fadeOut('slow');
			$('#mostra_desc_mostrador').attr('src','img/bombas/desc_mostrador+.png');
			$('#mostra_desc_mostrador').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_portico').fadeOut('slow');
			$('#mostra_desc_portico').attr('src','img/bombas/desc_portico+.png');
			$('#mostra_desc_portico').attr('ativo',0);
			/***************** ***** *****************/
		}else{
			$('#descricao_midia').fadeOut('slow');
			$(this).attr('src','img/bombas/desc_midia+.png');
			$(this).attr('ativo',0);
		}	
	});

	$("#mostra_desc_acabamentos").bind('click', function(){
		var ativo = $(this).attr('ativo');
		if( ativo == 0 ){
			$('#descricao_acabamentos').fadeIn('slow');
			$(this).attr('src','img/bombas/desc_acabamentos-.png');
			$(this).attr('ativo',1);
			/***************** ***** *****************/
			$('#descricao_midia').fadeOut('slow');
			$('#mostra_desc_midia').attr('src','img/bombas/desc_midia+.png');
			$('#mostra_desc_midia').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_bico').fadeOut('slow');
			$('#mostra_desc_bico').attr('src','img/bombas/desc_bico+.png');
			$('#mostra_desc_bico').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_teclado').fadeOut('slow');
			$('#mostra_desc_teclado').attr('src','img/bombas/desc_teclado+.png');
			$('#mostra_desc_teclado').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_mostrador').fadeOut('slow');
			$('#mostra_desc_mostrador').attr('src','img/bombas/desc_mostrador+.png');
			$('#mostra_desc_mostrador').attr('ativo',0);
			/***************** ***** *****************/
			$('#descricao_portico').fadeOut('slow');
			$('#mostra_desc_portico').attr('src','img/bombas/desc_portico+.png');
			$('#mostra_desc_portico').attr('ativo',0);
			/***************** ***** *****************/
		}else{
			$('#descricao_acabamentos').fadeOut('slow');
			$(this).attr('src','img/bombas/desc_acabamentos+.png');
			$(this).attr('ativo',0);
		}	
	});

	$("#accordion").bind('click', function(){
		
		/***************** ***** *****************/
		$('#mostra_desc_acabamentos').attr('src','img/bombas/desc_acabamentos+.png');
		$('#mostra_desc_acabamentos').attr('ativo',0);
		$('#descricao_acabamentos').fadeOut('slow');
		/***************** ***** *****************/
		$('#descricao_midia').fadeOut('slow');
		$('#mostra_desc_midia').attr('src','img/bombas/desc_midia+.png');
		$('#mostra_desc_midia').attr('ativo',0);
		/***************** ***** *****************/
		$('#descricao_bico').fadeOut('slow');
		$('#mostra_desc_bico').attr('src','img/bombas/desc_bico+.png');
		$('#mostra_desc_bico').attr('ativo',0);
		/***************** ***** *****************/
		$('#descricao_teclado').fadeOut('slow');
		$('#mostra_desc_teclado').attr('src','img/bombas/desc_teclado+.png');
		$('#mostra_desc_teclado').attr('ativo',0);
		/***************** ***** *****************/
		$('#descricao_mostrador').fadeOut('slow');
		$('#mostra_desc_mostrador').attr('src','img/bombas/desc_mostrador+.png');
		$('#mostra_desc_mostrador').attr('ativo',0);
		/***************** ***** *****************/
		$('#descricao_portico').fadeOut('slow');
		$('#mostra_desc_portico').attr('src','img/bombas/desc_portico+.png');
		$('#mostra_desc_portico').attr('ativo',0);
		/***************** ***** *****************/
	});

})(jQuery);